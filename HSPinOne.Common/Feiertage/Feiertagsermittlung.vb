﻿Imports HSPinOne.DAL


Public Class Feiertagsermittlung



    ' Aufzählung für Feiertage
    Public Enum Feiertage
        ftNeujahr = 1
        ftErscheinungsfest = 2
        ftKarfreitag = 3
        ftOstersonntag = 4
        ftOstermontag = 5
        ftMaifeiertag = 6
        ftChristiHimmelfahrt = 7
        ftPfingstmontag = 8
        ftFronleichnam = 9
        ftMariaeHimmelfahrt = 10
        ftTagDerEinheit = 11
        ftReformationstag = 12
        ftAllerheiligen = 13
        ftBussUndBettag = 14
        ftWeihnachtsfeiertag1 = 15
        ftWeihnachtsfeiertag2 = 16
    End Enum

    ' Aufzählung für Bundesländer
    Public Enum Bundeslaender
        blBadenWuerttemberg = 2 ^ 0
        blBayern = 2 ^ 1
        blBerlin = 2 ^ 2
        blBrandenburg = 2 ^ 3
        blBremen = 2 ^ 4
        blHamburg = 2 ^ 5
        blHessen = 2 ^ 6
        blMecklenburgVorpommern = 2 ^ 7
        blNiedersachsen = 2 ^ 8
        blNordrheinWestfalen = 2 ^ 9
        blRheinlandPfalz = 2 ^ 10
        blSaarland = 2 ^ 11
        blSachsen = 2 ^ 12
        blSachsenAnhalt = 2 ^ 13
        blSchleswigHolstein = 2 ^ 14
        blThueringen = 2 ^ 15
    End Enum





    Public Function Ostersonntag(Optional ByVal Jahr As Long = 0) As Date
        'Osterfunktion nach Carl Friedrich Gauß (1800). Rückgabewert
        ' ist das Datum des Ostersonntags im angegebenen (ersatzweise:
        ' aktuellen) Jahr. Gültigkeitsbereich: 1583 - 8702 (auf das
        ' Auslösen von Laufzeitfehlern bei Unter- oder Überschreitung
        ' dieses Gültigkeitsbereichs wird hier verzichtet)

        Dim a As Long, b As Long, c As Long, d As Long, e As Long, f As Long

        ' Wurde kein Jahr angegeben, wird das aktuelle Jahr verwendet:
        If Jahr = 0 Then
            Jahr = Year(Now)
        End If

        ' Die "magische" Gauss-Formel anwenden:
        a = Jahr Mod 19
        b = Jahr \ 100
        c = (8 * b + 13) \ 25 - 2
        d = b - (Jahr \ 400) - 2
        e = (19 * (Jahr Mod 19) + ((15 - c + d) Mod 30)) Mod 30
        If e = 28 Then
            If a > 10 Then
                e = 27
            End If
        ElseIf e = 29 Then
            e = 28
        End If
        f = (d + 6 * e + 2 * (Jahr Mod 4) + 4 * (Jahr Mod 7) + 6) Mod 7

        ' Rückgabewert als Datum bereitstellen
        Ostersonntag = DateSerial(Jahr, 3, e + f + 22)

    End Function


    Public Function Feiertagsdatum(ByVal Feiertag As Feiertage, _
                                   Optional ByVal Jahr As Long = 0 _
                                   ) As Date
        ' Gibt das Datum eines datumsfesten oder beweglichen Feiertags zurück.

        ' Wurde kein Jahr angegeben, wird das aktuelle Jahr verwendet:
        If Jahr = 0 Then
            Jahr = Year(Now)
        End If

        ' Feiertage ermitteln:

        Select Case Feiertag

            Case Feiertage.ftNeujahr
                Return DateSerial(Jahr, 1, 1)

            Case Feiertage.ftErscheinungsfest
                Return DateSerial(Jahr, 1, 6)

            Case Feiertage.ftKarfreitag
                Return DateAdd("d", -2, Ostersonntag(Jahr))

            Case Feiertage.ftOstersonntag
                Return Ostersonntag(Jahr)

            Case Feiertage.ftOstermontag
                Return DateAdd("d", 1, Ostersonntag(Jahr))

            Case Feiertage.ftMaifeiertag
                Return DateSerial(Jahr, 5, 1)

            Case Feiertage.ftChristiHimmelfahrt
                Return DateAdd("d", 39, Ostersonntag(Jahr))

            Case Feiertage.ftPfingstmontag
                Return DateAdd("d", 50, Ostersonntag(Jahr))

            Case Feiertage.ftFronleichnam
                Return DateAdd("d", 60, Ostersonntag(Jahr))

            Case Feiertage.ftMariaeHimmelfahrt
                Return DateSerial(Jahr, 8, 15)

            Case Feiertage.ftTagDerEinheit
                Return DateSerial(Jahr, 10, 3)

            Case Feiertage.ftReformationstag
                Return DateSerial(Jahr, 10, 31)

            Case Feiertage.ftAllerheiligen
                Return DateSerial(Jahr, 11, 1)

            Case Feiertage.ftBussUndBettag
                Return DateAdd(DateInterval.Day, _
                                         Weekday(DateSerial(Jahr, 12, 25), vbMonday) - 4 * 7 - vbWednesday, _
                                         DateSerial(Jahr, 12, 25))


            Case Feiertage.ftWeihnachtsfeiertag1
                Return DateSerial(Jahr, 12, 25)

            Case Feiertage.ftWeihnachtsfeiertag2
                Return DateSerial(Jahr, 12, 26)

        End Select
        Return DateSerial(1900, 1, 1)

    End Function


    Public Function IstFeiertagIn(ByVal Feiertag As Feiertage, _
                                  Optional ByVal Bundesland As Bundeslaender = 0 _
                                  ) As Boolean
        ' Gibt zurück, ob der im Parameter Feiertag angegebene Feiertag in
        ' einem angegebenen Bundesland begangen wird (True) oder nicht (False).
        ' Wird für den Parameter Bundesland kein Wert übergeben, wird zurückgegeben,
        ' ob der Feiertag ein bundesweit einheitlicher Feiertag ist.
        '
        ' HINWEIS: Feiertage, die nur in einzelnen Gemeinden eines Bundeslandes
        '          begangen werden (Fronleichnam: Sachsen, Thüringen; Mariä
        '          Himmelfahrt: Bayern), werden von dieser Funktion als in diesen
        '          Bundesländern als NICHT begangen behandelt! Hinweise, wie Sie
        '          dieses Verhalten bei Bedarf ändern können, finden Sie in diesem
        '          Sourcecode.
        '
        ' TIPP:    Bei Übergabe durch den Or- oder +-Operator kumulierter Werte
        '          für den Parameter Bundesland können Sie feststellen, ob dieser
        '          Feiertag in mehreren Bundesländern begangen wird. Beispiel:
        '          IstFeiertagInBayernUndInHessen = _
        '            IstFeiertagIn(ftFronleichnam, ftBayern + ftHessen)

        ' Bundesweit einheitliche Feiertage
        Select Case Feiertag

            Case Feiertage.ftNeujahr, Feiertage.ftKarfreitag, Feiertage.ftOstersonntag, Feiertage.ftOstermontag, _
                 Feiertage.ftMaifeiertag, Feiertage.ftChristiHimmelfahrt, Feiertage.ftPfingstmontag, _
                 Feiertage.ftTagDerEinheit, Feiertage.ftWeihnachtsfeiertag1, Feiertage.ftWeihnachtsfeiertag2
                Return True

        End Select

        ' Wurde kein Wert für Bundesland übergeben, wird die Funktion hier verlassen
        If Bundesland = 0 Then
            Return False
        End If

        ' Bundesweit nicht einheitliche Feiertage
        Select Case Feiertag

            Case Feiertage.ftErscheinungsfest
                Return (Bundesland And _
                                 (Bundeslaender.blBadenWuerttemberg Or Bundeslaender.blBayern Or Bundeslaender.blSachsenAnhalt) _
                                                ) = Bundesland

            Case Feiertage.ftFronleichnam
                ' HINWEIS: In Sachsen-Anhalt und Thüringen wird Fronleichnam
                ' nur vereinzelt als Feiertag begangen. Für diese Bundesländer
                ' liefert diese Funktion generell FALSE zurück. Ändern Sie
                ' dieses Verhalten bei Bedarf in diesem Select Case-Zweig.
                Return (Bundesland And ( _
                                 Bundeslaender.blBadenWuerttemberg Or Bundeslaender.blBayern Or Bundeslaender.blHessen Or _
                                 Bundeslaender.blNordrheinWestfalen Or Bundeslaender.blRheinlandPfalz Or Bundeslaender.blSaarland) _
                                ) = Bundesland

            Case Feiertage.ftMariaeHimmelfahrt
                ' HINWEIS: In Bayern wird Mariä Himmelfahrt nur in einzelnen
                ' Gemeinden als Feiertag begangen. Für dieses Bundesland
                ' liefert diese Funktion generell FALSE zurück. Ändern Sie
                ' dieses Verhalten bei Bedarf in diesem Select Case-Zweig.
                Return (Bundesland And ( _
                                 Bundeslaender.blSaarland) _
                                ) = Bundesland

            Case Feiertage.ftReformationstag
                Return (Bundesland And ( _
                                 Bundeslaender.blBrandenburg Or Bundeslaender.blMecklenburgVorpommern Or _
                                Bundeslaender.blSachsen Or Bundeslaender.blSachsenAnhalt Or Bundeslaender.blThueringen) _
                                ) = Bundesland

            Case Feiertage.ftAllerheiligen
                Return (Bundesland And ( _
                                 Bundeslaender.blBadenWuerttemberg Or Bundeslaender.blBayern Or Bundeslaender.blNordrheinWestfalen _
                                 Or Bundeslaender.blRheinlandPfalz Or Bundeslaender.blSaarland) _
                                ) = Bundesland

            Case Feiertage.ftBussUndBettag
                Return (Bundesland And ( _
                                 Bundeslaender.blSachsen) _
                                ) = Bundesland

        End Select
        Return False
    End Function

    Public Function GetAll(ByVal bundesland As String, ByVal Jahr As Long) As Dictionary(Of DateTime, String)


        Dim cls As New HSPinOne.DAL.Bundeslaender
        Dim blid = cls.GetBundeslandbyName(bundesland)
        Dim ret As New Dictionary(Of DateTime, String)

        If blid <> 0 Then

            For Each C As Feiertage In [Enum].GetValues(GetType(Feiertage))
                If IstFeiertagIn(C, blid) = True Then
                    ret.Add(Feiertagsdatum(C, Jahr), Mid(C.ToString, 3))
                End If
            Next
        End If
        Return ret
    End Function

    Public Sub New()

    End Sub
End Class
