﻿Public Enum SettingsSections
    Allgemein
    Buchhaltung
    Clientnutzer
    Warenverwaltung
    Kurse
    Ferien
    Venue
    Sport
    Studio
    Vorlagen
    Kunden
    WebService
    Vermietung
    EinzelplatzVermietung
End Enum

Public Enum StatisticsSections
    Hochschulsport
    Studio
End Enum