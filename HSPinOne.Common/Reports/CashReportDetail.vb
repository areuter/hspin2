﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports MigraDoc.DocumentObjectModel



Namespace Templates




    Public Class CashReportDetail



        Public Sub CreateDocument(ByVal Datum As Date, ByVal items As List(Of Billing), ByVal Kassenname As String, ByVal Titel As String)

            Dim mt As New MigradocTools
            Dim doc = mt.CreateDocument

            Dim sums As New Dictionary(Of String, Double)
            Dim customerstates As List(Of CustomerState)
            Dim packages As List(Of Package)

            Using con As New in1Entities
                packages = (From c In con.Packages Select c).ToList
                customerstates = (From c In con.CustomerStates Select c).ToList
            End Using


            '
            'Dim result = From c In OCLSV
            '             Group c By c.CustomerID, c.Customer.Kontoinhaber, c.Customer.Kontonummer, c.Customer.BLZ Into Group
            '             Select CustomerID, Kontoinhaber, Kontonummer, BLZ, Summe = Group.Sum(Function(o) o.Brutto), VWZ1 = String.Join(" ", Group.Select(Function(a) a.Verwendungszweck))

            'Stornierte und - Buchungen raus

            Dim result = From c In items Where c.BillingStatus = BillingStates.Bezahlt And c.IsStorno = False
                         Group c By c.CustomerStateID, Verwendungszweck = Trim(c.Verwendungszweck) Into Group
                         Select CustomerStateID, Verwendungszweck, Anzahl = Group.Count, Summe = Group.Sum(Function(o) o.Brutto)
                         Order By Verwendungszweck, CustomerStateID




            doc.LastSection.AddParagraph(Titel, "Heading1")
            doc.LastSection.AddParagraph(Kassenname, "Heading2")







            Dim table = doc.LastSection.AddTable()
            table.Style = "Table"
            Dim col = table.AddColumn(Unit.FromCentimeter(8))
            col = table.AddColumn(Unit.FromCentimeter(4))
            col = table.AddColumn(Unit.FromCentimeter(2))
            col.Format.Alignment = ParagraphAlignment.Right
            col = table.AddColumn(Unit.FromCentimeter(3))
            col.Format.Alignment = ParagraphAlignment.Right

            Dim row = table.AddRow
            row.HeadingFormat = True
            row.Format.Font.Bold = True
            row.Format.Font.Color = Colors.White
            row.Shading.Color = Colors.Black

            row.Cells(0).AddParagraph("Artikel")
            row.Cells(1).AddParagraph("Statusgruppe")
            row.Cells(2).AddParagraph("Anzahl")
            row.Cells(3).AddParagraph("Summe")

            For Each itm In result
                row = table.AddRow
                Dim csid = itm.CustomerStateID
                Dim pt = itm.Verwendungszweck
                Dim cs = customerstates.Where(Function(o) o.CustomerStateID = csid).First.CustomerStatename
                row.Cells(0).AddParagraph(pt)
                row.Cells(1).AddParagraph(cs)
                row.Cells(2).AddParagraph(itm.Anzahl)
                row.Cells(3).AddParagraph((itm.Summe).ToString("c"))
            Next


            row = table.AddRow()

            row.HeadingFormat = True
            row.Format.Font.Bold = True
            row.Format.Font.Color = Colors.White
            row.Shading.Color = Colors.Black
            row.Cells(0).AddParagraph("Tagessumme")
            row.Cells(3).AddParagraph((result.Sum(Function(o) o.Summe)).ToString("c"))




            mt.Printout(doc)


        End Sub
    End Class


End Namespace
