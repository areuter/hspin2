﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports MigraDoc.DocumentObjectModel



Namespace Templates




    Public Class CashReport



        Public Sub CreateDocument(ByVal Datum As Date, ByVal items As List(Of Billing), ByVal Kassenname As String, ByVal Titel As String, Optional ByVal state As String = BillingStates.Bezahlt)

            Dim mt As New MigradocTools
            Dim doc = mt.CreateDocument

            Dim sums As New Dictionary(Of String, Double)
            Dim customerstates As List(Of CustomerState)
            Dim producttypes As List(Of ProductType)


            Using con As New in1Entities
                producttypes = (From c In con.ProductTypes Select c).ToList
                customerstates = (From c In con.CustomerStates Select c).ToList
            End Using


            '
            'Dim result = From c In OCLSV
            '             Group c By c.CustomerID, c.Customer.Kontoinhaber, c.Customer.Kontonummer, c.Customer.BLZ Into Group
            '             Select CustomerID, Kontoinhaber, Kontonummer, BLZ, Summe = Group.Sum(Function(o) o.Brutto), VWZ1 = String.Join(" ", Group.Select(Function(a) a.Verwendungszweck))

            Dim result = From c In items
                         Where c.BillingStatus = state
                         Group c By c.CustomerStateID, c.ProductTypeID Into Group
                         Select CustomerStateID, ProductTypeID, Summe = Group.Sum(Function(o) o.Brutto)
                         Order By ProductTypeID, CustomerStateID




            doc.LastSection.AddParagraph(Titel, "Heading1")
            doc.LastSection.AddParagraph(Kassenname, "Heading2")







            Dim table = doc.LastSection.AddTable()
            table.Style = "Table"
            table.AddColumn(Unit.FromCentimeter(5))
            table.AddColumn(Unit.FromCentimeter(5))
            Dim col = table.AddColumn(Unit.FromCentimeter(5))
            col.Format.Alignment = ParagraphAlignment.Right

            Dim row = table.AddRow
            row.HeadingFormat = True
            row.Format.Font.Bold = True
            row.Format.Font.Color = Colors.White
            row.Shading.Color = Colors.Black

            row.Cells(0).AddParagraph("Warengruppe")
            row.Cells(1).AddParagraph("Statusgruppe")
            row.Cells(2).AddParagraph("Summe")

            For Each itm In result
                row = table.AddRow
                Dim csid = itm.CustomerStateID
                Dim ptid = itm.ProductTypeID
                Dim cs = customerstates.Where(Function(o) o.CustomerStateID = csid).First.CustomerStatename
                Dim pt = producttypes.Where(Function(o) o.ProductTypeID = ptid).First.ProductTypeName
                row.Cells(0).AddParagraph(pt)
                row.Cells(1).AddParagraph(cs)
                row.Cells(2).AddParagraph((itm.Summe).ToString("c"))


            Next


            row = table.AddRow()

            row.HeadingFormat = True
            row.Format.Font.Bold = True
            row.Format.Font.Color = Colors.White
            row.Shading.Color = Colors.Black
            row.Cells(0).AddParagraph("Tagessumme")
            row.Cells(2).AddParagraph((result.Sum(Function(o) o.Summe)).ToString("c"))



            ' Aufteilung nach Zahlungsarten
            ' Zahlungsarten
            row = table.AddRow()
            row = table.AddRow()
            row.Format.Font.Size = Unit.FromPoint(10)
            row.Cells(0).AddParagraph("Zahlungsarten")
            row.Cells(1).AddParagraph("Anzahl")
            row.Cells(2).AddParagraph("Summe")
            row.HeadingFormat = True
            row.Format.Font.Bold = True
            row.Format.Font.Color = Colors.White
            row.Shading.Color = Colors.Black

            Dim zarten = From c In items
                         Where c.BillingStatus = BillingStates.Bezahlt
                         Group c By c.PaymentMethodID, c.PaymentMethod.PaymentMethodname Into Group
                         Select PaymentMethodID, PaymentMethodname, Summe = Group.Sum(Function(o) o.Brutto),
                             Anzahl = Group.Count
                         Order By PaymentMethodID

            For Each itm In zarten
                'ZList.Add(New ZBonItem With {.Bezeichnung = " -" & itm.PaymentMethodname, .Anzahl = itm.Anzahl, .Summe = itm.Summe})
                row = table.AddRow()
                row.Cells(0).AddParagraph(itm.PaymentMethodname)
                row.Cells(1).AddParagraph(itm.Anzahl)
                row.Cells(2).AddParagraph(itm.Summe.ToString("c"))

            Next

            'Informativ noch die Aufteilung nach CustomerstateID
            row = table.AddRow()
            row = table.AddRow()
            row.Format.Font.Size = Unit.FromPoint(10)
            row.Cells(0).AddParagraph("Zusatzinformationen")


            For Each itm In customerstates
                row = table.AddRow()
                row.Cells(0).AddParagraph("Summe " & itm.CustomerStatename)
                row.Cells(2).AddParagraph((result.Where(Function(o) o.CustomerStateID = itm.CustomerStateID).Sum(Function(o) o.Summe)).ToString("c"))
            Next


            mt.Printout(doc)


        End Sub
    End Class

    Public Class CashRepitem
        Public Property Warengruppe As Integer
        Public Property Status As String
        Public Property Betrag As Decimal

    End Class

End Namespace