﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports MigraDoc.DocumentObjectModel



Namespace Templates
    Public Class CashReportText


        Public Shared Sub CreateReport(ByVal textdata As String)
            Dim mt As New MigradocTools
            Dim doc = mt.CreateDocument
            Dim style As Style = doc.Styles("Normal")

            style = style.Document.Styles.AddStyle("fixed", "Normal")
            style.Font.Name = "Courier New"

            ' Replace alls Spaces by Non breaking spaces
            textdata = textdata.Replace(" ", Chr(160))

            doc.LastSection.AddParagraph(textdata, "fixed")
            mt.Printout(doc)
        End Sub

    End Class
End Namespace