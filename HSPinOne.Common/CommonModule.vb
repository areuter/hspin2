﻿
Imports HSPinOne.Infrastructure
Imports Prism.Ioc
Imports Prism.Modularity
Imports System.Windows.Input




Public Class CommonModule
    Implements IModule

    Public Sub RegisterTypes(containerRegistry As IContainerRegistry) Implements IModule.RegisterTypes
        containerRegistry.RegisterInstance(Of INewsFeedService)(New NewsFeedService())
        containerRegistry.RegisterInstance(Of ISearchCustomer)(New SearchCustomer())
        containerRegistry.RegisterInstance(Of IImageEditor)(New ImageEditor())
        containerRegistry.RegisterInstance(Of IHistoryViewer)(New HistoryViewer())
        containerRegistry.RegisterInstance(Of IStorno)(New Storno())
    End Sub

    Public Sub OnInitialized(containerProvider As IContainerProvider) Implements IModule.OnInitialized
        'Throw New NotImplementedException()

    End Sub
End Class
