﻿Imports System.Windows.Data
Imports System.Globalization

Public Class CollectionViewGroupConverter
    Implements IValueConverter



    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object Implements IValueConverter.Convert
        If Not IsNothing(value) Then
            Dim output As SettingsNavigation = New SettingsNavigation()
            output.MenuItemName = value.Name
            Return output
        End If
        Return False
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class
