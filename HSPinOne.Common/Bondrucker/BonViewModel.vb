﻿Imports System.Drawing
Imports System.IO
Imports System.Threading.Tasks
Imports System.Windows.Input
Imports HSPinOne.Common
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Public Class BonViewModel
    Inherits ViewModelBase

    Private _Bontext As String = "Mustertext auf Bon"
    Public Property Bontext() As String
        Get
            Return _Bontext
        End Get
        Set(ByVal value As String)
            _Bontext = value
            RaisePropertyChanged("Bontext")
        End Set
    End Property

    Private _Email As String
    Public Property Email() As String
        Get
            Return _Email
        End Get
        Set(ByVal value As String)
            _Email = value
            RaisePropertyChanged("Email")
        End Set
    End Property

    Public Event RequestClose As EventHandler

    Private _lastbon As Guid
    Private _lastbonid As Integer
    Private _customerid As Integer = 0
    Private _duplikat As Boolean = False

    Private _canprint As Boolean = False

    Public Property BonmailCommand As New RelayCommand(AddressOf Bonmail, AddressOf CanBonmail)
    Public Property BonprintCommand As New RelayCommand(AddressOf Bonprint, AddressOf CanBonprint)
    Public Property CancelCommand As New RelayCommand(AddressOf CancelAction)

    Public Sub New(ByVal bonid As Guid, ByVal duplikat As Boolean, ByVal customerid As Integer)
        _lastbon = bonid
        _customerid = customerid
        _duplikat = duplikat
        LoadData()

    End Sub

    Public Sub New(ByVal bonid As Integer, ByVal duplikat As Boolean, ByVal customerid As Integer)
        _lastbonid = bonid
        _customerid = customerid
        _duplikat = duplikat
        LoadData()

    End Sub

    Private Async Sub LoadData()
        Dim bon As New Bondruck
        Await Task.Run(Sub() bon.LastBon(_lastbonid, _duplikat, True))
        Bontext = bon.Preview

        ' Suche mail Adressen
        Using con As New in1Entities
            Dim cust = con.Customers.Find(_customerid)
            Dim address = ""
            If Not IsNothing(cust) Then
                Email = cust.Mail1
            End If

        End Using

        If MachineSettingService.GetMachineSetting(MachineSettingService.in1MaschineSettings.BondruckerAktiv) = 1 Then
            _canprint = True
        End If
        CommandManager.InvalidateRequerySuggested()

    End Sub


    Private Sub Bonmail()
        If Not IsNothing(_lastbon) Then

            If Not RegExUtils.IsValidEmail(Email) Then
                Return
            End If

            Dim mail As New clsMail
            Dim body As String = "<html><body><p>Anbei ihr Kassenbon als Bild.</p><img src=""cid:bonimage.jpg""></body></html>"
            Dim font As New Font("Courier New", 10.0F)


            Dim img = DrawText(Bontext, font, Color.Black, Color.White)
            Dim bon As New MemoryStream
            img.Save(bon, Imaging.ImageFormat.Jpeg)
            bon.Position = 0

            mail.AddLinkedResource("bonimage.jpg", bon)

            mail.SendSMTP(Email, "Kassenbon", body)

            'mail.MailWindow(Email, "Bon vom " & Now.ToString(), Bontext)

            CancelAction()

        End If
    End Sub

    Public Function DrawText(ByVal text As String, ByRef font As Font, ByRef textColor As Color, ByRef backColor As Color) As Image
        ' first, create a dummy bitmap just to get a graphics object
        Dim img As Image = New Bitmap(1, 1)
        Dim drawing As Graphics = Graphics.FromImage(img)

        ' measure the string to see how big the image needs to be
        Dim textSize As SizeF = drawing.MeasureString(text, font)

        ' free up the dummy image and old graphics object
        img.Dispose()
        drawing.Dispose()

        ' create a new image of the right size
        img = New Bitmap(CType(textSize.Width, Integer), CType(textSize.Height, Integer))

        drawing = Graphics.FromImage(img)

        ' paint the background
        drawing.Clear(backColor)

        ' create a brush for the text
        Dim textBrush As Brush = New SolidBrush(textColor)

        drawing.DrawString(text, font, textBrush, 0, 0)

        drawing.Save()

        textBrush.Dispose()
        drawing.Dispose()

        Return img

    End Function

    Private Function CanBonmail() As Boolean
        If RegExUtils.IsValidEmail(Email) Then Return True
        Return False
    End Function

    Private Sub Bonprint()
        If Not IsNothing(_lastbon) Then
            Dim bon As New Bondruck
            bon.LastBon(_lastbon, _duplikat)
            CancelAction()
        End If
    End Sub

    Private Function CanBonprint() As Boolean
        Return _canprint
    End Function

    Private Sub CancelAction()
        Dim e As New CustomEventArgs
        RaiseEvent RequestClose(Me, e)
    End Sub




End Class
