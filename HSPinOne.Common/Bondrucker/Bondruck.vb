﻿
Imports System.IO.Ports
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Text
Imports System.Net.Sockets
Imports System.ComponentModel
Imports System.Threading
Imports System.Linq
Imports System.Data.Entity

Public Enum Printertype As Integer
    Serial
    Network
    Web
    None
End Enum

Public Enum Alignment As Integer
    Left
    Center
    Right
End Enum

Public Class Bondruck




    Private _escpos As Escpos
    Private _epos As Epos

    Private WithEvents _bgw As New BackgroundWorker


    Private Const MAXWIDTH As Integer = 42
    Private Const PRICEWIDTH As Integer = 10

    Private _preview As New StringBuilder


    Private _Kunde As String = ""
    Public Property Kunde As String
        Get
            Return _Kunde
        End Get
        Set(value As String)
            _Kunde = value
        End Set
    End Property

    Private _BonGuid As Guid
    Public Property BonGuid() As Guid
        Get
            Return _BonGuid
        End Get
        Set(ByVal value As Guid)
            _BonGuid = value
        End Set
    End Property


    Private _port As String = "COM1"
    Private _printertype As Printertype = Printertype.Serial
    Private _aktiv As Boolean = False
    Private _ipaddress As String
    Private _ipport As Integer = 9100
    Private _customhead As String = String.Empty
    Private _customfooter As String = String.Empty
    Private _connected As Boolean = False

    Private _data As StringBuilder = New StringBuilder()

    Public Sub New()

        Using context As New in1Entities
            Dim query = From c In context.MachineConfigs Where c.Machinename = MyGlobals.Fingerprint Select c

            For Each itm In query
                Select Case itm.Configname
                    Case "BondruckerAktiv"
                        If Not IsNothing(itm.Machinesetting) Then
                            If itm.Machinesetting = 1 Then _aktiv = True
                        End If
                    Case "BondruckerPort"
                        If Not IsNothing(itm.Machinesetting) Then
                            If itm.Machinesetting.StartsWith("COM") Then
                                _port = itm.Machinesetting
                                _printertype = Printertype.Serial
                            End If
                            If itm.Machinesetting.Equals("LAN") Then
                                _port = "Network"
                                _printertype = Printertype.Network
                            End If
                            If itm.Machinesetting.Equals("Web") Then
                                _port = "Web"
                                _printertype = Printertype.Web
                            End If
                        End If
                    Case "BondruckerIP"
                        If Not IsNothing(itm.Machinesetting) Then _ipaddress = itm.Machinesetting
                    Case "BondruckerIPPort"
                        If Not IsNothing(itm.Machinesetting) Then _ipport = CType(itm.Machinesetting, Integer)
                    Case "BondruckerHead"
                        If Not IsNothing(itm.Machinesetting) Then _customhead = itm.Machinesetting
                    Case "Bontext"
                        If Not IsNothing(itm.Machinesetting) Then _customfooter = itm.Machinesetting
                End Select
            Next
        End Using

        If _printertype = Printertype.Serial Then
            _escpos = New Escpos(MAXWIDTH, PRICEWIDTH, _port)
        ElseIf _printertype = Printertype.Network Then
            _escpos = New Escpos(MAXWIDTH, PRICEWIDTH, _ipaddress, _ipport)
        Else
            _escpos = New Escpos()
        End If

        _epos = New Epos(_ipaddress, MAXWIDTH)



    End Sub

    Public Sub LastBon(ByVal bonid As Integer, Optional ByVal duplikat As Boolean = False, Optional preview As Boolean = False)

        Try


            Dim summe As Double = 0
            Dim method As Integer = 0
            Dim origdate As DateTime = Now

            Kunde = "Tagesgast"

            Using con As New in1Entities
                Dim query = con.BillingReceipts.Include(Function(o) o.Billings).Where(Function(o) o.BillingReceiptID = bonid).Single()






                If Not IsNothing(query.Billings.First.Customer) Then
                    Kunde = query.Billings.First.Customer.Suchname
                End If



                PrintHeader()
                    Feed()

                    If duplikat Then
                        PrintTextLine("- DUPLIKAT -", Alignment.Center)
                        Feed()
                    End If


                    PrintTextLine("Kunde: " & Kunde)
                    PrintFullLineRepeat("-")

                    Dim Steuerliste As New Steuersummen()
                Dim taxes = From c In query.Billings
                            Group c By c.Steuerprozent
                                Into Group
                            Select Steuerprozent
                            Order By Steuerprozent


                For i = 0 To taxes.Count - 1
                        Steuerliste.AddTax(taxes(i), Chr(i + 65))
                    Next



                For Each itm In query.Billings
                    summe = summe + itm.Brutto
                    method = itm.PaymentMethodID
                    If Not IsNothing(itm.Buchungsdatum) Then
                        origdate = itm.Buchungsdatum
                    End If
                    Dim sc = Steuerliste.Add(itm)
                    PrintLineItemTax(itm.Verwendungszweck, itm.Brutto, sc)
                Next


                'Summe drucken
                PrintFullLineRepeat("-")
                    PrintLineItemBold("Summe", summe)

                    Feed()

                    Dim paymentmethod = con.PaymentMethods.Find(method)
                    Feed()
                    PrintTextLine("Zahlungsart: " & paymentmethod.PaymentMethodname)


                    'Print Tax
                    Feed()

                    PrintTextLine("               Netto    MwSt     Brutto")
                    For Each itm In Steuerliste.GetList()
                        Dim satz = String.Format("{0,4:#0.0}%", itm.Steuersatz)
                        Dim mwst = String.Format("{0,8:#0.00}", itm.Steuerbetrag)
                        Dim netto = String.Format("{0,8:#0.00}", itm.Netto)
                        Dim brutto = String.Format("{0,8:#0.00}", itm.Brutto)
                        PrintTextLine(itm.Shortcode & " " & satz & "     " & netto & "   " & mwst & "   " & brutto)
                    Next

                    Feed()

                PrintTextLine(origdate.ToString() & " " & query.Creator, Alignment.Left)
                PrintTextLine(query.KasseNr & "-" & query.KasseBelegNr, Alignment.Center)

                PrintFooter()

                    If Not preview Then
                        Cut()
                        Pulse()
                        Print()
                    End If

            End Using

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Sub LastBon(ByVal bonguid As Guid, Optional ByVal duplikat As Boolean = False, Optional preview As Boolean = False)

        Try


            Dim summe As Double = 0
            Dim method As Integer = 0
            Dim origdate As DateTime = Now

            Kunde = "Tagesgast"

            Using con As New in1Entities
                Dim query = (From c In con.Billings Where c.Vorgang = bonguid Select c).ToList

                If query.Count > 0 Then



                    If Not IsNothing(query.First.Customer) Then
                        Kunde = query.First.Customer.Suchname
                    End If



                    PrintHeader()
                    Feed()

                    If duplikat Then
                        PrintTextLine("- DUPLIKAT -", Alignment.Center)
                        Feed()
                    End If


                    PrintTextLine("Kunde: " & Kunde)
                    PrintFullLineRepeat("-")

                    Dim Steuerliste As New Steuersummen()
                    Dim taxes = From c In query
                                Group c By c.Steuerprozent
                                Into Group
                                Select Steuerprozent
                                Order By Steuerprozent


                    For i = 0 To taxes.Count - 1
                        Steuerliste.AddTax(taxes(i), Chr(i + 65))
                    Next



                    For Each itm In query
                        summe = summe + itm.Brutto
                        method = itm.PaymentMethodID
                        If Not IsNothing(itm.Buchungsdatum) Then
                            origdate = itm.Buchungsdatum
                        End If
                        Dim sc = Steuerliste.Add(itm)
                        PrintLineItemTax(itm.Verwendungszweck, itm.Brutto, sc)
                    Next


                    'Summe drucken
                    PrintFullLineRepeat("-")
                    PrintLineItemBold("Summe", summe)

                    Feed()

                    Dim paymentmethod = con.PaymentMethods.Find(method)
                    Feed()
                    PrintTextLine("Zahlungsart: " & paymentmethod.PaymentMethodname)


                    'Print Tax
                    Feed()

                    PrintTextLine("               Netto    MwSt     Brutto")
                    For Each itm In Steuerliste.GetList()
                        Dim satz = String.Format("{0,4:#0.0}%", itm.Steuersatz)
                        Dim mwst = String.Format("{0,8:#0.00}", itm.Steuerbetrag)
                        Dim netto = String.Format("{0,8:#0.00}", itm.Netto)
                        Dim brutto = String.Format("{0,8:#0.00}", itm.Brutto)
                        PrintTextLine(itm.Shortcode & " " & satz & "     " & netto & "   " & mwst & "   " & brutto)
                    Next

                    Feed()

                    PrintTextLine(origdate.ToString() & " " & query.First.LastModifiedBy, Alignment.Left)
                    PrintTextLine(bonguid.ToString(), Alignment.Center)

                    PrintFooter()

                    If Not preview Then
                        Cut()
                        Pulse()
                        Print()
                    End If

                End If
            End Using

        Catch ex As Exception
            Throw ex
        End Try

    End Sub


    Public Sub PrintReceipt(ByVal receipt As BillingReceipt, Optional ByVal duplikat As Boolean = False, Optional preview As Boolean = False)

        Try


            Dim summe As Double = 0
            Dim method As Integer = 0
            Dim origdate As DateTime = Now

            Kunde = "Tagesgast"

            Using con As New in1Entities
                Dim query = (From c In con.Billings Where c.BillingReceiptID = receipt.BillingReceiptID Select c).ToList

                If query.Count > 0 Then



                    If Not IsNothing(query.First.Customer) Then
                        Kunde = query.First.Customer.Suchname
                    End If



                    PrintHeader()
                    Feed()

                    If duplikat Then
                        PrintTextLine("- DUPLIKAT -", Alignment.Center)
                        Feed()
                    End If


                    PrintTextLine("Kunde: " & Kunde)
                    PrintFullLineRepeat("-")

                    Dim Steuerliste As New Steuersummen()
                    Dim taxes = From c In query
                                Group c By c.Steuerprozent
                                Into Group
                                Select Steuerprozent
                                Order By Steuerprozent


                    For i = 0 To taxes.Count - 1
                        Steuerliste.AddTax(taxes(i), Chr(i + 65))
                    Next



                    For Each itm In query
                        summe = summe + itm.Brutto
                        method = itm.PaymentMethodID
                        If Not IsNothing(itm.Buchungsdatum) Then
                            origdate = itm.Buchungsdatum
                        End If
                        Dim sc = Steuerliste.Add(itm)
                        PrintLineItemTax(itm.Verwendungszweck, itm.Brutto, sc)
                    Next


                    'Summe drucken
                    PrintFullLineRepeat("-")
                    PrintLineItemBold("Summe", summe)

                    Feed()

                    Dim paymentmethod = con.PaymentMethods.Find(method)
                    Feed()
                    PrintTextLine("Zahlungsart: " & paymentmethod.PaymentMethodname)


                    'Print Tax
                    Feed()

                    PrintTextLine("               Netto    MwSt     Brutto")
                    For Each itm In Steuerliste.GetList()
                        Dim satz = String.Format("{0,4:#0.0}%", itm.Steuersatz)
                        Dim mwst = String.Format("{0,8:#0.00}", itm.Steuerbetrag)
                        Dim netto = String.Format("{0,8:#0.00}", itm.Netto)
                        Dim brutto = String.Format("{0,8:#0.00}", itm.Brutto)
                        PrintTextLine(itm.Shortcode & " " & satz & "     " & netto & "   " & mwst & "   " & brutto)
                    Next

                    Feed()

                    PrintTextLine(origdate.ToString() & " " & query.First.LastModifiedBy, Alignment.Left)
                    PrintTextLine(BonGuid.ToString(), Alignment.Center)

                    PrintFooter()

                    If Not preview Then
                        Cut()
                        Pulse()
                        Print()
                    End If

                End If
            End Using

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Function Preview() As String
        Return _preview.ToString()
    End Function

    Public Sub InitPrinter()
        If _aktiv Then _escpos.InitPrinter()
    End Sub



    Public Sub ClosePrinter()
        If _aktiv Then _escpos.ClosePrinter()
    End Sub

    Public Sub Write(ByVal strinput As String)
        Me._data.Append(strinput)
    End Sub

    Public Sub PrintHeader(Optional ByVal oDate As DateTime = Nothing)


        'MachineHeader
        If Not Trim(_customhead) = "" Then
            Dim lins = Split(_customhead, "\n")
            For Each itm In lins
                If Not Trim(itm) = "" Then PrintTextLine(TruncateAt(itm, MAXWIDTH))
            Next
        Else
            Dim hspname As String = GlobalSettingService.HoleEinstellung("hspname")
            Dim hspstr As String = GlobalSettingService.HoleEinstellung("hspstr")
            Dim hsport As String = GlobalSettingService.HoleEinstellung("hsport")
            Dim hspphone As String = GlobalSettingService.HoleEinstellung("hspphone")



            If Len(hspname) > 0 Then PrintTextLine(hspname, Alignment.Center)
            If Len(hspstr) > 0 Then PrintTextLine(hspstr, Alignment.Center)
            If Len(hsport) > 0 Then PrintTextLine(hsport, Alignment.Center)
            If Len(hspphone) > 0 Then PrintTextLine(hspphone, Alignment.Center)

        End If


    End Sub

    Public Sub PrintFooter(Optional ByVal footertext As String = "")
        Feed()
        If footertext = "" Then
            If Not Trim(_customfooter) = "" Then
                footertext = _customfooter
            End If
        End If

        If Trim(footertext) <> "" Then
            For i = 1 To footertext.Length Step MAXWIDTH
                PrintTextLine(Mid(footertext, i, MAXWIDTH), Alignment.Center)
            Next
        End If
    End Sub

    Public Sub PrintCorrectness()

        Feed()

        PrintTextLine("Für die Richtigkeit:")

        Feed()

        PrintTextLine("Kassenbestand")
        Feed(2)
        PrintFullLineRepeat(".")
        Feed(2)

        PrintTextLine("Fehlbetrag oder Überschuss")
        Feed(2)
        PrintFullLineRepeat(".")
        Feed(2)

        PrintTextLine("Name 1 + Unterschrift")
        Feed(2)
        PrintFullLineRepeat(".")
        Feed(2)

        PrintTextLine("Name 2 + Unterschrift")
        Feed(2)
        PrintFullLineRepeat(".")
        Feed(2)
    End Sub


    Public Sub Print()
        If Not _bgw.IsBusy Then
            _bgw.RunWorkerAsync()
        End If


    End Sub

    Private Sub Print_Worker(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles _bgw.DoWork

        If _aktiv Then


            If _printertype = Printertype.Web Then

                _epos.Print()
            Else
                _escpos.InitPrinter()
                _escpos.Print()
                _escpos.ClosePrinter()
            End If
        End If

    End Sub

    Private Sub Print_Completed(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles _bgw.RunWorkerCompleted
        If Not IsNothing(e.Error) Then
            Dim dlg As New DialogService
            dlg.ShowAlert("Bondrucker", "Es ist ein Problem mit dem Bondrucker aufgetreten: " + e.Error.ToString())
        End If
    End Sub

    Public Sub Printout(Optional zbon As Boolean = False)

        If _aktiv = False Then Return

        Dim summe As Decimal = 0

        Try

            InitPrinter()

            PrintHeader()

            Feed(2)

            'Kunde drucken
            If Len(Kunde) > 1 Then
                PrintTextLine("Kunde: " & Kunde)
            End If

            PrintFullLineRepeat("-")



            'For Each itm In lines
            '    PrintLineItem(itm.Artikel, itm.Preis)
            '    summe = summe + itm.Preis
            'Next

            'Summe drucken
            PrintTextLine(New String("-", MAXWIDTH))
            Feed(1)

            PrintLineItemBold("Summe", summe)

            Feed(1)
            PrintLeftandRight("Datum", Now.ToString())

            If Not IsNothing(BonGuid) Then
                PrintLeftandRight("Vorgang", BonGuid.ToString())
            End If

            PrintFooter()

            'Leerzeilen
            Feed(6)

            Cut()

            Print()

            ClosePrinter()


        Catch ex As Exception

            Dim dlg As New DialogService
            dlg.ShowError("Fehler", ex.Message)

        Finally
            ClosePrinter()

        End Try

    End Sub


    Public Sub Feed(Optional lines As Integer = 1)
        If _aktiv = False Then Return
        _escpos.Feed(lines)
        _epos.Feed(lines)
        _preview.AppendLine("")
    End Sub

    Public Sub Cut()
        If _aktiv = False Then Return
        _escpos.Cut()
        _epos.Cut()
    End Sub

    Public Sub Pulse()
        If _aktiv = False Then Return
        _escpos.Pulse()
        _epos.Pulse()
    End Sub



    Public Sub PrintLineItem(itemCode As String, unitPrice As Double)
        Dim line As String = ""
        line = TruncateAt(itemCode.PadRight(MAXWIDTH - PRICEWIDTH - 1), MAXWIDTH - PRICEWIDTH - 1)
        line = line & " " & GetPrice(unitPrice)
        _escpos.AddLine(line)
        _epos.AddLine(line)
        _preview.AppendLine(line)
    End Sub

    Public Sub PrintLineItemTax(itemCode As String, unitPrice As Double, tax As String)
        Dim line As String = ""
        line = TruncateAt(itemCode.PadRight(MAXWIDTH - PRICEWIDTH - 1), MAXWIDTH - PRICEWIDTH - 1 - 2)
        line = line & " " & GetPrice(unitPrice) & " " & tax
        _escpos.AddLine(line)
        _epos.AddLine(line)
        _preview.AppendLine(line)
    End Sub


    Public Sub PrintLineItemBold(itemCode As String, unitPrice As Double)
        Dim line As String = ""
        line = TruncateAt(itemCode.PadRight(MAXWIDTH - PRICEWIDTH - 1), MAXWIDTH - PRICEWIDTH - 1)
        line = line & " " & GetPrice(unitPrice)
        _escpos.AddLine(line, dh:=True)
        _epos.AddLine(line, dh:=True)
        _preview.AppendLine(line)
    End Sub

    Private Function GetPrice(price As Double)
        Dim newprice As String
        newprice = price.ToString("#0.00") & " €"
        Return newprice.PadLeft(10)
    End Function

    Public Sub PrintTextLine(ByVal text As String, Optional ByVal justification As Alignment = Alignment.Left, Optional ByVal ds As Boolean = False)
        _escpos.AddLine(text, justification, False, ds, ds)
        _epos.AddLine(text, justification, False, ds, ds)
        _preview.AppendLine(text)
    End Sub

    Public Sub PrintFullLineRepeat(ByVal text As String)
        If Len(text) = 0 Then Return
        Dim newtext As String = ""
        Do While Len(newtext) < MAXWIDTH
            newtext = newtext & text
        Loop
        Dim line As String = ""
        line = TruncateAt(newtext, MAXWIDTH)
        _escpos.AddLine(line)
        _epos.AddLine(line)
        _preview.AppendLine(line)
    End Sub

    Public Sub PrintFullLine(ByVal text As String)
        If Len(text) = 0 Then Return
        Dim newtext As String = ""
        newtext = text.PadRight(MAXWIDTH)
        _escpos.AddLine(newtext)
        _epos.AddLine(newtext)
        _preview.AppendLine(newtext)
    End Sub

    Public Sub PrintLeftandRight(ByVal left As String, ByVal right As String, Optional ByVal ul As Boolean = False)
        Dim newtext As String = ""
        If Len(left & " " & right) >= MAXWIDTH Then
            newtext = left & " " & right
        Else
            newtext = left & Space(MAXWIDTH - Len(left) - Len(right)) & right
        End If
        _epos.AddLine(newtext, ul:=ul)
        _escpos.AddLine(newtext, ul:=ul)
        _preview.AppendLine(newtext)
    End Sub


    Private Function TruncateAt(text As String, maxWidth As Integer) As String

        Dim retVal As String = text
        If (text.Length > maxWidth) Then retVal = text.Substring(0, maxWidth)
        Return retVal
    End Function







End Class

Public Class Bondruckerzeile
    Property Artikel As String
    Property Preis As Double
End Class

Friend Class Steuersummen
    Private _liste As New List(Of Steuersumme)

    Public Sub AddTax(ByVal Steuersatz As Double, ByVal shortcode As String)
        Dim item As New Steuersumme
        item.Steuersatz = Steuersatz
        item.Shortcode = shortcode
        item.Brutto = 0
        item.Netto = 0
        item.Steuerbetrag = 0
        _liste.Add(item)
    End Sub

    Public Function Add(ByVal bill As Billing) As String

        Dim shortcode As String = ""

        Dim found = _liste.FindAll(Function(o) o.Steuersatz = bill.Steuerprozent)

        If Not IsNothing(found) AndAlso found.Count > 0 Then
            shortcode = found.First.Shortcode
            found.First.Brutto = found.First.Brutto + bill.Brutto
            Dim netto = Math.Round(bill.Brutto / (1 + (bill.Steuerprozent / 100)), 2, MidpointRounding.AwayFromZero)
            found.First.Netto = found.First.Netto + netto
            found.First.Steuerbetrag = found.First.Steuerbetrag + (bill.Brutto - netto)
        End If
        Return shortcode
    End Function

    Public Function GetList() As List(Of Steuersumme)
        Return _liste
    End Function


End Class

Friend Class Steuersumme
    Property Steuersatz As Double
    Property Netto As Double
    Property Brutto As Double
    Property Steuerbetrag As Double
    Property Shortcode As String
End Class

