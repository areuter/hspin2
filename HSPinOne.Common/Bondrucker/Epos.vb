﻿Imports System.Net



Public Class Epos


    ' URL of ePOS-Print supported TM printer
    Private address As String = "http://{0}/cgi-bin/epos/service.cgi?devid=local_printer&timeout=10000"

    ' XML namespace
    Private soap As XNamespace = "http://schemas.xmlsoap.org/soap/envelope/"
    Private epos As XNamespace = "http://www.epson-pos.com/schemas/2011/03/epos-print"

    Private _body As New XElement(epos + "epos-print")
    Private _printwidth As Integer = 42


    Public Sub New(ByVal ipaddress As String, ByVal printwidth As Integer)
        address = String.Format(address, ipaddress)
        printwidth = printwidth
    End Sub

    Public Sub Print()


        Dim req As XElement = New XElement(soap + "Envelope",
                                           New XElement(soap + "Body", _body))


        ' Send print document
        Dim client As WebClient = New WebClient()
        client.Encoding = System.Text.Encoding.UTF8
        client.Headers.Set("Content-Type", "text/xml; charset=utf-8")
        client.Headers.Set("SOAPAction", """""")
        AddHandler client.UploadStringCompleted, AddressOf UploadStringCompletedEventHandler
        client.UploadStringAsync(New Uri(address, UriKind.Absolute), req.ToString())
        Debug.Print(req.ToString())

    End Sub

    ' Receive response document
    Private Sub UploadStringCompletedEventHandler(sender As Object, e As UploadStringCompletedEventArgs)

        If (e.Error IsNot Nothing) Then
            'MessageBox.Show(e.Error.Message)
        Else
            'Parse response document
            Dim res As XElement = XElement.Parse(e.Result)
            Dim c = From el In res.Descendants(epos + "response") Select el.Attribute("success")
            'MessageBox.Show(c.First().Value)
        End If

    End Sub

    Public Sub AddLine(ByVal text As String, Optional alignment As Alignment = Alignment.left, Optional ul As Boolean = False, Optional dw As Boolean = False, Optional dh As Boolean = False)

        _body.Add(New XElement(epos + "text", text & Environment.NewLine,
                            New XAttribute("lang", "de"),
                            New XAttribute("smooth", "true"),
                            New XAttribute("dw", dw.ToString().ToLower()),
                            New XAttribute("dh", dh.ToString().ToLower()),
                            New XAttribute("ul", ul.ToString().ToLower()),
                            New XAttribute("align", alignment.ToString().ToLower())
                            )
                            )

    End Sub

    Public Sub Feed(Optional lines As Integer = 1)

        _body.Add(New XElement(epos + "feed",
                            New XAttribute("line", lines.ToString())
                            ))
    End Sub



    Public Sub Line()

    End Sub


    Public Sub Cut()
        Feed(6)
        _body.Add(New XElement(epos + "cut"))
    End Sub

    Public Sub Pulse()

        _body.Add(New XElement(epos + "pulse"))
    End Sub


End Class
