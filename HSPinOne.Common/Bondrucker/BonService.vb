﻿Imports HSPinOne.Infrastructure

Public Class BonService


    Public Sub New(ByVal bonguid As Guid, Optional ByVal duplikat As Boolean = False, Optional ByVal customerid As Integer = 0)
        Dim win As New WindowController
        Dim vm As New BonViewModel(bonguid, duplikat, customerid)
        Dim uc As New BonView()
        uc.DataContext = vm
        AddHandler vm.RequestClose, AddressOf win.CloseWindow
        win.ShowWindow(uc, "Bondruck")
        RemoveHandler vm.RequestClose, AddressOf win.CloseWindow
    End Sub

    Public Sub New(ByVal bonid As Integer, Optional ByVal duplikat As Boolean = False, Optional ByVal customerid As Integer = 0)
        Dim win As New WindowController
        Dim vm As New BonViewModel(bonid, duplikat, customerid)
        Dim uc As New BonView()
        uc.DataContext = vm
        AddHandler vm.RequestClose, AddressOf win.CloseWindow
        win.ShowWindow(uc, "Bondruck")
        RemoveHandler vm.RequestClose, AddressOf win.CloseWindow
    End Sub
End Class
