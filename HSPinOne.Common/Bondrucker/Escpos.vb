﻿Imports System.IO.Ports
Imports System.Net.Sockets
Imports System.Text

Public Enum PrintMode
    Normal = 0
    Emphasized = 8
    DoubleHeight = 16
    EmphDoubleHeight = 24
    DoubleWidth = 32
    EmphDoubleWidth = 40
    DoubleSize = 48
    EmphDouble = 56
    Underline = 128
End Enum

Public Enum Underline
    None = 0
    Thin = 1
    Thick = 2
End Enum

Public Enum Justification
    Left
    Center
    Right
End Enum
Public Class Escpos

    Private _data As StringBuilder = New StringBuilder
    Public ReadOnly Property Data() As StringBuilder
        Get
            Return _data
        End Get
    End Property

    Private Const ESC As String = Chr(27)
    Private Const LF As String = Chr(10)
    Private Const NUL As String = Chr(0)
    Private Const FS As String = Chr(28)
    Private Const SOH As String = Chr(1)
    Private Const SP As String = Chr(32)
    Private Const GS As String = Chr(29)

    Private _maxWidth As Integer = 42
    Private _priceWidth As Integer = 10
    Private printer As SerialPort
    Private _printertype As Printertype = Printertype.Serial
    Private _port As String = "COM1"
    Private _connected As Boolean = False
    Private client As New TcpClient()
    Private _ipaddress As String
    Private _ipport As String

    Private _active As Boolean = False

    Dim stream As NetworkStream

    Public Sub New()
        _printertype = Printertype.None
    End Sub

    Public Sub New(ByVal width As Integer, ByVal pricewidth As Integer, ByVal port As String)
        _maxWidth = width
        _priceWidth = pricewidth
        _port = port
        _active = True
        _printertype = Printertype.Serial
    End Sub

    Public Sub New(ByVal width As Integer, ByVal pricewidth As Integer, ByVal ipaddress As String, ByVal ipport As String)
        _maxWidth = width
        _priceWidth = pricewidth
        _ipaddress = ipaddress
        _ipport = ipport
        _printertype = Printertype.Network
        _active = True
    End Sub

    Public Sub InitPrinter()
        If Not _active Then Return
        Try

            If _printertype = Printertype.Serial Then
                'Port öffnen
                If Not IsNothing(printer) Then
                    If printer.IsOpen Then printer.Close()
                End If

                printer = New SerialPort(_port)
                printer.Encoding = Encoding.GetEncoding(1252)


                'printer.PortName = _port.ToUpper
                printer.BaudRate = 9600
                printer.Parity = Parity.None ' DirectCast([Enum].Parse(GetType(Parity), sett(1)), Parity)
                printer.DataBits = 8 ' Integer.Parse(sett(2))
                printer.StopBits = StopBits.One ' DirectCast([Enum].Parse(GetType(StopBits), sett(3)), StopBits)
                printer.Open()
                _connected = True
                InitPrinterDefaults()
            ElseIf _printertype = Printertype.Network Then
                client = New TcpClient(_ipaddress, _ipport)
                stream = client.GetStream()
                _connected = True
                InitPrinterDefaults()
            Else
                Dim dlg As New HSPinOne.Infrastructure.DialogService
                dlg.ShowError("Bondrucker", "BondruckerPort muss mit COM beginnen oder Network oder Web sein.")
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub


    Private Sub InitPrinterDefaults()

        'Drucker initialisieren
        Write(ESC & "@")

        'Codepage PC437 USA Standard Europe = 0, Euro PC858 = 19, ISO = 40
        Write(ESC & "t" & Chr(19))

        'CharacterSet 0 USA
        Write(ESC & "R" & Chr(0))

        ' Set Print Mode to normal
        Write(ESC & "!" & Chr(0))

        'Paper Near End Sensor ausschalten
        'printer.Write(ESC & "c4")
    End Sub

    Public Sub ClosePrinter()
        _connected = False
        If Not IsNothing(printer) Then
            If printer.IsOpen Then printer.Close()
        End If
        If _printertype = Printertype.Network Then
            stream.Close()
            client.Close()
        End If
    End Sub

    Public Sub Print()
        If _connected = True Then

            If _printertype = Printertype.Serial Then
                printer.Write(Me._data.ToString())
            ElseIf _printertype = Printertype.Network Then
                ' Translate the passed message into ASCII and store it as a Byte array.
                'Dim enc As Encoding = Encoding.GetEncoding("IBM437")
                Dim enc As Encoding = Encoding.GetEncoding("IBM00858")
                Dim data As [Byte]() = enc.GetBytes(Me._data.ToString())
                stream.Write(data, 0, data.Length)
            End If
        End If
    End Sub

    Public Sub AddLine(ByVal text As String, Optional alignment As Alignment = Alignment.left, Optional ul As Boolean = False, Optional dw As Boolean = False, Optional dh As Boolean = False)



        SetJustification(alignment)
        If ul = True Then SetUnderline(Underline.Thin)
        If dh = True And dw = False Then SetPrintMode(PrintMode.DoubleHeight)
        If dh = False And dw = True Then SetPrintMode(PrintMode.DoubleWidth)
        If dw = True And dh = True Then SetPrintMode(PrintMode.DoubleSize)


        'If _printertype = Printertype.Serial Then
        text = Umlauteersetzen(text)
        'End If

        Write(text + LF)

        SetJustification()
        SetUnderline()
        SetPrintMode(PrintMode.Normal)


    End Sub

    Public Sub Write(ByVal strinput As String)
        Me._data.Append(strinput)
    End Sub


    ''' <summary>
    ''' Cut paper full
    ''' </summary>
    Public Sub Cut()
        'Bon auswerfen und abschneiden
        Feed(6)
        Write(GS + "V" + NUL)
    End Sub
    ''' <summary>
    ''' Open cash drawer
    ''' </summary>
    ''' <param name="pin"></param>
    ''' <param name="on_ms"></param>
    ''' <param name="off_ms"></param>
    Public Sub Pulse(Optional ByVal pin As Integer = 0, Optional ByVal on_ms As Integer = 120, Optional ByVal off_ms As Integer = 240)
        Write(ESC & "p" & Chr(pin + 48) & Chr(on_ms / 2) & Chr(off_ms / 2))
    End Sub

    Public Sub SetPrintMode(ByVal mode As PrintMode)
        ' Set Print Mode
        Write(ESC & "!" & Chr(mode))
    End Sub

    Public Sub SetUnderline(Optional ByVal thickness As Underline = Underline.None)
        Write(ESC & "-" & thickness)
    End Sub

    Public Sub SetJustification(Optional just As Alignment = Alignment.Left)
        Write(ESC & "a" & just)
    End Sub


    Public Sub Feed(Optional ByVal n As Integer = 1)
        If n <= 1 Then
            Write(LF)
        Else
            Write(ESC & "d" & Chr(n))
        End If

    End Sub

    Private Function Umlauteersetzen(ByVal text As String) As String

        ' Wenn Netzwerkdrucker, dann wird Codierung in GetBytes richtig übersetzt
        If _printertype = Printertype.Network Then
            Return text
        End If

        Dim strText As String = text
        'Umlaute ersetzen
        strText = Replace(strText, "ä", Chr(132))
        strText = Replace(strText, "Ä", Chr(142))
        strText = Replace(strText, "ö", Chr(148))
        strText = Replace(strText, "Ö", Chr(153))
        strText = Replace(strText, "ü", Chr(129))
        strText = Replace(strText, "Ü", Chr(154))
        strText = Replace(strText, "ß", Chr(225))
        strText = Replace(strText, "€", Chr(213))
        Return strText

    End Function
End Class
