﻿Imports System.ComponentModel
Imports System.Windows.Media
Imports System.Windows.Input
Imports HSPinOne.Infrastructure


Public Interface INavigationItem
    ReadOnly Property IsExpanded As Boolean
    Property MenuItemName As String
    Property MenuItemImageSource As ImageSource
    Property MenuItemCommand
    Property Tag1 As String
    Property Tag2 As String
End Interface

Public Class NavigationItem
    Inherits ViewModelBase
    Implements INavigationItem

    Public ReadOnly Property IsExpanded As Boolean Implements INavigationItem.IsExpanded
        Get
            Return False
        End Get
    End Property

    Private _menuitemname As String

    Public Property MenuItemName As String Implements INavigationItem.MenuItemName
        Get
            Return _menuitemname
        End Get
        Set(value As String)
            _menuitemname = value
            RaisePropertyChanged("MenuItemName")
        End Set
    End Property

    Private _menuitemimagesource As ImageSource

    Public Property MenuItemImageSource As ImageSource Implements INavigationItem.MenuItemImageSource
        Get
            Return _menuitemimagesource
        End Get
        Set(value As ImageSource)
            _menuitemimagesource = value
            RaisePropertyChanged("MenuItemImageSource")
        End Set
    End Property

    Private _menuItemCommand As ICommand

    Public Property MenuItemCommand Implements INavigationItem.MenuItemCommand
        Get
            Return _menuItemCommand
        End Get
        Set(value)
            _menuItemCommand = value
            RaisePropertyChanged("MenuItemcommand")
        End Set
    End Property

    Private _Tag1 As String

    Public Property Tag1 As String Implements INavigationItem.Tag1
        Get
            Return _Tag1
        End Get
        Set(value As String)
            _Tag1 = value
            RaisePropertyChanged("Tag1")
        End Set
    End Property

    Private _Tag2 As String

    Public Property Tag2 As String Implements INavigationItem.Tag2
        Get
            Return _Tag2
        End Get
        Set(value As String)
            _Tag2 = value
            RaisePropertyChanged("Tag2")
        End Set
    End Property


    Public Sub New()

    End Sub
    Public Sub New(ByVal menuitemname As String)
        _menuitemname = menuitemname
    End Sub
    Public Sub New(ByVal menuitemname As String, ByVal tag1 As String)
        _menuitemname = menuitemname
        Me.Tag1 = tag1
    End Sub



End Class



