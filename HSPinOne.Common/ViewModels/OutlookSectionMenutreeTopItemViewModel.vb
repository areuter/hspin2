﻿Imports System.ComponentModel
Imports System.Windows.Media
Imports System.Collections.ObjectModel
Imports System.Windows.Input
Imports System.Windows


Public Class OutlookSectionMenuTreeTopItemViewModel
    Implements INotifyPropertyChanged

    Private _menuitemname As String

    Public Property MenuItemName As String
        Get
            Return _menuitemname
        End Get
        Set(value As String)
            _menuitemname = value
            RaisePropertyChanged("MenuItemName")
        End Set
    End Property

    Private _menuitemimagesource As ImageSource

    Public Property MenuItemImageSource As ImageSource
        Get
            Return _menuitemimagesource
        End Get
        Set(value As ImageSource)
            _menuitemimagesource = value
            RaisePropertyChanged("MenuItemImageSource")
        End Set
    End Property

    Private _children As ObservableCollection(Of OutlookSectionMenuTreeItemViewModel)

    Public Property Children As ObservableCollection(Of OutlookSectionMenuTreeItemViewModel)
        Get
            Return _children
        End Get
        Set(value As ObservableCollection(Of OutlookSectionMenuTreeItemViewModel))
            _children = value
        End Set
    End Property

    Private _isExpanded As Boolean

    Public Property IsExpanded As Boolean
        Get
            Return _isExpanded
        End Get
        Set(value As Boolean)
            _isExpanded = value
            RaisePropertyChanged("IsExpanded")
        End Set
    End Property

    Private _menuItemCommand As ICommand

    Public Property MenuItemCommand
        Get
            Return _menuItemCommand
        End Get
        Set(value)
            _menuItemCommand = value
            RaisePropertyChanged("MenuItemcommand")
        End Set
    End Property

    Public Shared ReadOnly IsExpandedProperty As DependencyProperty =
    DependencyProperty.Register("IsExpanded",
                                GetType(Boolean),
                                GetType(OutlookSectionMenuTreeTopItemViewModel))



    Public Event PropertyChanged(sender As Object, e As PropertyChangedEventArgs) _
        Implements INotifyPropertyChanged.PropertyChanged

    Public Sub RaisePropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub

    Public Sub New()
        Me.Children = New ObservableCollection(Of OutlookSectionMenuTreeItemViewModel)
    End Sub
End Class


