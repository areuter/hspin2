﻿Imports System.ComponentModel
Imports System.Windows.Input
Imports System.Windows.Media
Imports System.Windows.Controls


Public Class OutlookSectionMenuListBoxItemViewModel
    Inherits ListBoxItem



    Private _menuItemCommand As String

    Public Property MenuItemCommand
        Get
            Return _menuItemCommand
        End Get
        Set(ByVal value)
            _menuItemCommand = value
            'RaisePropertyChanged("MenuItemcommand")
        End Set
    End Property

    Private _menuItemView As String
    Public Property MenuItemView As String
        Get
            Return _menuItemView
        End Get
        Set(ByVal value As String)
            _menuItemView = value
            'RaisePropertyChanged("MenuItemView")
        End Set
    End Property


    Private _ribbonTab As String
    Public Property RibbonTab As String
        Get
            Return _ribbonTab
        End Get
        Set(ByVal value As String)
            _ribbonTab = value
            'RaisePropertyChanged("RibbonTab")
        End Set
    End Property


    'Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged
    'Public Sub RaisePropertyChanged(ByVal info As String)
    '    RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    'End Sub
End Class
