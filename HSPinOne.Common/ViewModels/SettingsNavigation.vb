﻿
Imports System.Windows.Input

Public Class SettingsNavigation
    Inherits NavigationItem
    Public Sub New()
    End Sub

    Public Sub New(category As SettingsSections, name As String)
        Me.Name = name
        Me.Category = category
    End Sub

    Public Sub New(category As SettingsSections, name As String, tag1 As String)
        MyBase.New(name, tag1)
        Me.Name = name
        Me.Category = category
    End Sub

    Public Property Name() As String
        Get
            Return m_Name
        End Get
        Set(value As String)
            m_Name = value
        End Set
    End Property
    Private m_Name As String
    Public Property Category() As SettingsSections
        Get
            Return m_Category
        End Get
        Set(value As SettingsSections)
            m_Category = value
        End Set
    End Property



    Private m_Category As SettingsSections
    Public ReadOnly Property SettingsNavigationList() As IEnumerable(Of SettingsNavigation)
        Get
            Dim itemList As New List(Of SettingsNavigation)()





            'Accounting
            itemList.Add(New SettingsNavigation(SettingsSections.Buchhaltung, "Allgemein", "AccountingSettingsView"))
            itemList.Add(New SettingsNavigation(SettingsSections.Buchhaltung, "Bankkonten", "BankAccountView"))
            itemList.Add(New SettingsNavigation(SettingsSections.Buchhaltung, "Kostenstellen", "CostCenterView"))

            'AdministrationUser
            itemList.Add(New SettingsNavigation(SettingsSections.Clientnutzer, "Nutzer", "AdministrationUserView"))

            'Commodity
            itemList.Add(New SettingsNavigation(SettingsSections.Warenverwaltung, "Artikelverwaltung", "CommodityOverviewView"))


            itemList.Add(New SettingsNavigation(SettingsSections.Kurse, "Kurskategorien", "CourseCategoryView"))
            itemList.Add(New SettingsNavigation(SettingsSections.Kurse, "Buchungsinfos", "BookingInformationView"))
            itemList.Add(New SettingsNavigation(SettingsSections.Kurse, "Kosteninformation", "CostInformationView"))
            itemList.Add(New SettingsNavigation(SettingsSections.Kurse, "Zielgruppen", "AudienceView"))
            itemList.Add(New SettingsNavigation(SettingsSections.Kurse, "Geschlecht", "GenderView"))

            itemList.Add(New SettingsNavigation(SettingsSections.Allgemein, "Hochschulsport", "GlobalView"))
            itemList.Add(New SettingsNavigation(SettingsSections.Allgemein, "Email Konten", "EmailAccountView"))
            itemList.Add(New SettingsNavigation(SettingsSections.Allgemein, "Admin Zutritte", "AdminAccessView"))
            itemList.Add(New SettingsNavigation(SettingsSections.Allgemein, "Statusgruppen", "StatusGroupView"))
            itemList.Add(New SettingsNavigation(SettingsSections.Allgemein, "Semester", "TermView"))

            itemList.Add(New SettingsNavigation(SettingsSections.Ferien, "Feiertage", "HolidayView"))


            itemList.Add(New SettingsNavigation(SettingsSections.Venue, "Räume", "LocationView"))
            itemList.Add(New SettingsNavigation(SettingsSections.Venue, "Raumgruppen", "LocationResourceView"))
            'itemList.Add(New SettingsNavigation(SettingsSections.Venue, "Raumausstattung", "VenueEquipmentView"))


            itemList.Add(New SettingsNavigation(SettingsSections.Sport, "Sportarten", "SportView"))
            itemList.Add(New SettingsNavigation(SettingsSections.Sport, "Sport Kategorien", "SportCategoryView"))


            itemList.Add(New SettingsNavigation(SettingsSections.Studio, "Allgemein", "StudioGeneralView"))


            itemList.Add(New SettingsNavigation(SettingsSections.Vorlagen, "Word", "TemplateWordView"))
            itemList.Add(New SettingsNavigation(SettingsSections.Vorlagen, "Pdf", "TemplatePdfView"))


            'itemList.Add(New SettingsNavigation(SettingsSections.Kunden, "", ""))


            'itemList.Add(New SettingsNavigation(SettingsSections.WebService, "Allgemein", "WebServiceGeneralView"))


            'itemList.Add(New SettingsNavigation(SettingsSections.Vermietung, "Allgemein", "RentalGeneralView"))


            'itemList.Add(New SettingsNavigation(SettingsSections.EinzelplatzVermietung, "Allgemein", "VenueRentalGeneralView"))


            Return itemList.AsEnumerable()
        End Get
    End Property
End Class

