﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Input
Imports System.ComponentModel
Imports System.Windows.Data
Imports System.Data.Entity
Imports System.Data.Entity.Infrastructure

'Imports System.Linq.Dynamic

Namespace ViewModel




    Public Class SearchViewModel
        Inherits ViewModelBase


        Public Event RequestClose As EventHandler
        Public Event SetFocus As EventHandler


        Private _context As in1Entities

        Dim WithEvents BGW As New BackgroundWorker




        Private _term0 As String = ""

        Public Property Term0 As String
            Get
                Return _term0
            End Get
            Set(ByVal value As String)
                _term0 = value
            End Set
        End Property

        Private _feld0 As String = "Suchname"
        Public Property Feld0 As String
            Get
                Return _feld0
            End Get
            Set(ByVal value As String)
                _feld0 = value
                RaisePropertyChanged("Feld0")
            End Set
        End Property

        Private _operator0 As String = "startet mit"
        Public Property Operator0 As String
            Get
                Return _operator0
            End Get
            Set(ByVal value As String)
                _operator0 = value
                RaisePropertyChanged("Operator0")
            End Set
        End Property


        Private _selectedcustomer As Customer
        Public Property SelectedCustomer As Customer
            Get
                Return _selectedcustomer
            End Get
            Set(ByVal value As Customer)
                _selectedcustomer = value
            End Set
        End Property

        Private _searchresults As IEnumerable(Of Customer)
        Public Property SearchResults As IEnumerable(Of Customer)
            Get
                Return _searchresults
            End Get
            Set(ByVal value As IEnumerable(Of Customer))
                _searchresults = value
            End Set
        End Property

        Private _lv As ListCollectionView
        Public Property LV As ListCollectionView
            Get
                Return _lv
            End Get
            Set(ByVal value As ListCollectionView)
                _lv = value
            End Set
        End Property

        Private _dialogresult As Boolean
        Public Property DialogResult As Boolean
            Get
                Return _dialogresult
            End Get
            Set(ByVal value As Boolean)
                _dialogresult = value
            End Set
        End Property

        Private _working As Boolean
        Public Property Working As Boolean
            Get
                Return _working
            End Get
            Set(ByVal value As Boolean)
                _working = value
                RaisePropertyChanged("Working")
            End Set
        End Property

        Private _operators As List(Of String)
        Public Property Operators As List(Of String)
            Get
                Return _operators
            End Get
            Set(ByVal value As List(Of String))
                _operators = value
            End Set
        End Property

        Private _felder As Dictionary(Of String, String)
        Public Property Felder As Dictionary(Of String, String)
            Get
                Return _felder
            End Get
            Set(ByVal value As Dictionary(Of String, String))
                _felder = value
            End Set
        End Property

        Private _feld1 As String = ""
        Public Property Feld1 As String
            Get
                Return _feld1
            End Get
            Set(ByVal value As String)
                _feld1 = value
            End Set
        End Property

        Private _term1 As String = ""
        Public Property Term1 As String
            Get
                Return _term1
            End Get
            Set(ByVal value As String)
                _term1 = value
                RaisePropertyChanged("Term1")
            End Set
        End Property

        Private _operator1 As String = "startet mit"
        Public Property Operator1 As String
            Get
                Return _operator1
            End Get
            Set(ByVal value As String)
                _operator1 = value
            End Set
        End Property

        Private _Searchinactice As Boolean = False
        Public Property Searchinactive As Boolean
            Get
                Return _Searchinactice
            End Get
            Set(value As Boolean)
                _Searchinactice = value
            End Set
        End Property



        Public Property OKCommand As ICommand
        Public Property KeyDownCommand As ICommand
        Public Property SearchCommand As ICommand
        Public Property CancelCommand As ICommand
        Public Property Del1Command As ICommand





        Public Sub New()
            Me._context = New in1Entities

            Dim c As New Customer
            Dim t As Type = c.GetType
            Dim fi = t.GetProperties
            Felder = New Dictionary(Of String, String)
            For Each fld In fi
                If fld.PropertyType = GetType(String) _
                    Or fld.PropertyType = GetType(Nullable(Of Date)) _
                    Or fld.PropertyType = GetType(Integer) _
                    Or fld.PropertyType = GetType(Long) _
                    Or fld.PropertyType = GetType(Nullable(Of Long)) Then
                    Felder.Add(fld.Name, fld.PropertyType.ToString)
                End If
            Next

            Operators = New List(Of String)
            Operators.Add("startet mit")
            Operators.Add("enthält")
            Operators.Add("=")
            Operators.Add("Datum")



            OKCommand = New RelayCommand(AddressOf OK, AddressOf CanOK)
            KeyDownCommand = New RelayCommand(AddressOf KeyDownAction)
            SearchCommand = New RelayCommand(AddressOf Search, AddressOf CanSearch)
            CancelCommand = New RelayCommand(AddressOf CancelAction)
        End Sub

        Public Sub CloseButtonClicked()
            RaiseEvent RequestClose(Me, Nothing)
        End Sub

        Private Sub OK()
            SelectedCustomer = LV.CurrentItem
            DialogResult = True
            RaiseEvent RequestClose(Me, Nothing)
        End Sub

        Private Function CanOK() As Boolean
            If Not IsNothing(LV) Then
                If Not IsNothing(LV.CurrentItem) Then Return True
            End If

            Return False

        End Function

        Private Sub CancelAction()
            DialogResult = False
            RaiseEvent RequestClose(Me, Nothing)
        End Sub

        Private Sub KeyDownAction(ByVal param As Object)
            If CanOK() Then OK()
        End Sub

        Private Sub Search()
            If Not CanSearch() Then Return
            If Trim(Term0) = "" Then Return
            If Not IsNothing(_context) Then
                Me._context.Dispose()
            End If
            Me._context = New in1Entities

            Dim strquery = "SELECT * FROM Customer WHERE 1=1 "

            If Searchinactive = False Then
                strquery = strquery & "AND Aktiv = 1 "

            End If

            If Feld0 = "Suchname" Then
                If IsNumeric(Term0) Then
                    Feld0 = "CustomerID"
                    Operator0 = "startet mit"
                End If
            End If

            If Not IsNothing(Term0) And Not Trim(Term0) = "" Then
                strquery = strquery & " AND " & GetSQL(Feld0, Operator0, Term0)
            End If

            If Not IsNothing(Term1) And Not Trim(Term1) = "" Then
                strquery = strquery & " AND " & GetSQL(Feld1, Operator1, Term1)
            End If

            strquery = strquery & " ORDER BY Suchname"

            Dim query = _context.Customers.SqlQuery(strquery).AsNoTracking()
            ' Dim query = _context.Database.SqlQuery(Of Customer)(strquery)

            'Dim query = _context.Customers.SqlQuery(strquery)






            Try

                SearchResults = query.ToList

            Catch ex As Exception
                Dim dlg As New DialogService
                dlg.ShowError("Fehler", "Der Suchbegriff oder der Vergleichsoperator versucht so einen Fehler. Bitte ändern Sie ihre Suche")
            End Try



            If SearchResults.Count > 0 Then
                SelectedCustomer = SearchResults.First
                RaiseEvent SetFocus(Me, Nothing)
            End If
            _context.Dispose()
            LV = CType(CollectionViewSource.GetDefaultView(SearchResults), ListCollectionView)
            RaisePropertyChanged("LV")
        End Sub

        Private Function GetSQL(ByVal fld As String, ByVal op As String, ByVal term As String) As String
            Dim qry As String = ""
            term = term.Replace("'", "''")

            ' Check ob Datumsformat
            Dim res = (Felder.Where(Function(o) o.Key = fld)).FirstOrDefault

            If Not IsNothing(res) Then
                If res.Value.Contains("Date") Then
                    op = "Datum"
                End If
            End If

            If op = "startet mit" Then
                qry = fld & " LIKE '" & term & "%'"
            ElseIf op = "enthält" Then
                qry = fld & " LIKE '%" & term & "%'"
            ElseIf op = "=" Then
                qry = fld & " = '" & term & "'"
            ElseIf op = "Datum" Then
                'qry = "DATEDIFF(day," & fld & ",'" & term & "')=0"
                qry = "CONVERT(VARCHAR(10), " & fld & ", 104) LIKE '" & term & "%'"
            End If
            Return qry
        End Function

        Private Function CanSearch() As Boolean
            If Not IsNothing(Term0) And Len(Term0) > 0 Then
                Return True
            End If
            Return False
        End Function

    End Class


End Namespace