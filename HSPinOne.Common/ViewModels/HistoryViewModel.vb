﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.ComponentModel
Imports System.Collections.ObjectModel


Namespace ViewModel


    Public Class HistoryViewModel
        Inherits ViewModelBase


        Public Property OCHistory As New ObservableCollection(Of AuditLog)

        Private WithEvents bgw As New BackgroundWorker
        Private _tablename As String
        Private _recordid As String

        Public Sub New(ByVal tablename As String, ByVal recordid As String)
            _tablename = tablename
            _recordid = recordid

            If Not bgw.IsBusy Then
                IsBusy = True
                bgw.WorkerSupportsCancellation = False
                bgw.RunWorkerAsync()
            End If





        End Sub

        Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw.DoWork
            Using con As New in1Entities
                con.Database.CommandTimeout = 60
                Dim query = From c In con.AuditLogs Where c.TableName = _tablename And c.RecordID = _recordid Order By c.EventDateUTC Descending Select c

                e.Result = query.ToList
            End Using
        End Sub


        Private Sub bgw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles bgw.RunWorkerCompleted

            For Each itm In e.Result
                OCHistory.Add(itm)
            Next
            IsBusy = False
        End Sub
    End Class
End Namespace