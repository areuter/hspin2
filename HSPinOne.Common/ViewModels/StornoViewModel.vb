﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Namespace ViewModel


    Public Class StornoViewModel
        Inherits ViewModelBase


        Private _Reason As String
        Public Property Reason() As String
            Get
                Return _Reason
            End Get
            Set(ByVal value As String)
                _Reason = value
            End Set
        End Property

        Private _Reasons As List(Of String)



        Public Property Reasons() As List(Of String)
            Get
                Return _Reasons
            End Get
            Set(ByVal value As List(Of String))
                _Reasons = value
            End Set
        End Property

        Public Property Result As Boolean = False

        Public Property OKCommand As New RelayCommand(AddressOf OKAction)
        Public Property CancelCommand As New RelayCommand(AddressOf CancelAction)

        Public Event RequestClose As EventHandler
        Public Sub New()
            Dim reasons = GlobalSettingService.HoleEinstellung("client.stornogrund")
            Me.Reasons = reasons.Split(",").Select(Function(o) o.Trim()).ToList()

        End Sub
        Private Sub OkAction()
            RaiseEvent RequestClose(Me, Nothing)
        End Sub

        Private Sub CancelAction()
            RaiseEvent RequestClose(Me, Nothing)
        End Sub

    End Class
End Namespace