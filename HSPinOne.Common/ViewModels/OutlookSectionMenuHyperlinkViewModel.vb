﻿
Imports System.ComponentModel
Imports System.Windows.Input
Imports System.Windows.Media
Imports HSPinOne.Infrastructure




Public Class OutlookSectionMenuHyperlinkViewModel
    Inherits ViewModelBase





    Private _menuitemname As String

    Public Property MenuItemName As String
        Get
            Return _menuitemname
        End Get
        Set(value As String)
            _menuitemname = value
            RaisePropertyChanged("MenuItemName")
        End Set
    End Property

    Private _menuitemimagesource As ImageSource

    Public Property MenuItemImageSource As ImageSource
        Get
            Return _menuitemimagesource
        End Get
        Set(value As ImageSource)
            _menuitemimagesource = value
            RaisePropertyChanged("MenuItemImageSource")
        End Set
    End Property

    Private _menuItemCommand As ICommand

    Public Property MenuItemCommand
        Get
            Return _menuItemCommand
        End Get
        Set(value)
            _menuItemCommand = value
            RaisePropertyChanged("MenuItemcommand")
        End Set
    End Property

    Private _menuitemroles As String

    Public Property MenuItemRoles As String
        Get
            Return _menuitemroles
        End Get
        Set(ByVal value As String)
            _menuitemroles = value
            RaisePropertyChanged("MenuItemRoles")
        End Set
    End Property











End Class
