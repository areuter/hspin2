﻿Imports System.ComponentModel
Imports System.Windows.Media
Imports System.Windows.Input


Public Class OutlookSectionMenuTreeItemViewModel
    Implements INotifyPropertyChanged

    Public ReadOnly Property IsExpanded As Boolean
        Get
            Return False
        End Get
    End Property

    Private _menuitemname As String

    Public Property MenuItemName As String
        Get
            Return _menuitemname
        End Get
        Set(value As String)
            _menuitemname = value
            RaisePropertyChanged("MenuItemName")
        End Set
    End Property

    Private _menuitemimagesource As ImageSource

    Public Property MenuItemImageSource As ImageSource
        Get
            Return _menuitemimagesource
        End Get
        Set(value As ImageSource)
            _menuitemimagesource = value
            RaisePropertyChanged("MenuItemImageSource")
        End Set
    End Property

    Private _menuItemCommand As ICommand

    Public Property MenuItemCommand
        Get
            Return _menuItemCommand
        End Get
        Set(value)
            _menuItemCommand = value
            RaisePropertyChanged("MenuItemcommand")
        End Set
    End Property

    Private _Tag1 As String

    Public Property Tag1 As String
        Get
            Return _Tag1
        End Get
        Set(value As String)
            _Tag1 = value
            RaisePropertyChanged("Tag1")
        End Set
    End Property

    Private _Tag2 As String

    Public Property Tag2 As String
        Get
            Return _Tag2
        End Get
        Set(value As String)
            _Tag2 = value
            RaisePropertyChanged("Tag2")
        End Set
    End Property


    Public Event PropertyChanged(sender As Object, e As PropertyChangedEventArgs) _
        Implements INotifyPropertyChanged.PropertyChanged

    Public Sub RaisePropertyChanged(ByVal info As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(info))
    End Sub
End Class


