﻿


Public Class StatisticsNavigation
    Inherits NavigationItem

    Public Sub New()
    End Sub

    Public Sub New(category As StatisticsSections, name As String)
        Me.Name = name
        Me.Category = category
    End Sub

    Public Sub New(category As StatisticsSections, name As String, tag1 As String)
        MyBase.New(name, tag1)
        Me.Name = name
        Me.Category = category
    End Sub

    Public Property Name() As String
        Get
            Return m_Name
        End Get
        Set(value As String)
            m_Name = value
        End Set
    End Property

    Private m_Name As String

    Public Property Category() As StatisticsSections
        Get
            Return m_Category
        End Get
        Set(value As StatisticsSections)
            m_Category = value
        End Set
    End Property


    Private m_Category As StatisticsSections

    Public ReadOnly Property StatisticsNavigationList() As IEnumerable(Of StatisticsNavigation)
        Get
            Dim itemList As New List(Of StatisticsNavigation)()

            'Hochschulsport
            itemList.Add(New StatisticsNavigation(StatisticsSections.Hochschulsport, "Übersicht",
                                                  "HspStatisticCourseOverviewView"))
            itemList.Add(New StatisticsNavigation(StatisticsSections.Hochschulsport, "Kursstatistik",
                                                  "HspStatisticCourseView"))
            itemList.Add(New StatisticsNavigation(StatisticsSections.Hochschulsport, "Kurs Genderverteilung",
                                                  "HspStatisticCourseGenderView"))
            itemList.Add(New StatisticsNavigation(StatisticsSections.Hochschulsport, "Kursbilanz",
                                                  "HspStatisticCourseBilanzView"))
            itemList.Add(New StatisticsNavigation(StatisticsSections.Hochschulsport, "Anzahl Kunden",
                                                  "HspStatisticCustomerInDbView"))
            itemList.Add(New StatisticsNavigation(StatisticsSections.Hochschulsport, "Kurse je Anbieter",
                                                  "HspStatisticCoursePartyView"))
            itemList.Add(New StatisticsNavigation(StatisticsSections.Hochschulsport, "Kurse je Kategorie",
                                                  "HspStatisticCourseCategoryView"))

            'Studio
            itemList.Add(New StatisticsNavigation(StatisticsSections.Studio, "Checkins", "StudioStatisticCheckinView"))
            itemList.Add(New StatisticsNavigation(StatisticsSections.Studio, "Vertragsabschlüsse/Tag", "StudioStatisticContractDayView"))
            itemList.Add(New StatisticsNavigation(StatisticsSections.Studio, "Verteilung Verträge", "StudioStatisticContractGenderView"))
            itemList.Add(New StatisticsNavigation(StatisticsSections.Studio, "Auslaufende Verträge", "StudioStatisticContractEndingView"))


            ' animalList__1.Add(New SettingsNavigation("Cheetah", TestCategory.BigCats))
            Return itemList.AsEnumerable()
        End Get
    End Property
End Class

