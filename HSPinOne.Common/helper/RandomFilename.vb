﻿Imports System.Text

Public Class RandomFilename
    ''' <summary>
    ''' Generiert Dateinamen im Wimdows Temp Pfad
    ''' </summary>
    ''' <param name="inp">Dateiname</param>
    ''' <param name="ext">Endung ohne Punkt</param>
    ''' <returns>Pfad+Dateiname</returns>
    ''' <remarks></remarks>
    Public Shared Function GetFilename(ByVal inp As String, ByVal ext As String) As String
        If Len(inp) = 0 Then
            Return GetTempFile() & "." & ext
        Else
            Dim san = SanitizeFilename(inp)
            If Len(san) > 0 Then
                Return GetTempPath() & san & "_" & Now.ToString("yyyyMMddHHmmss") & "." & ext
            Else
                Return GetTempFile() & "." & ext
            End If
        End If
    End Function

    Public Shared Function GetTempFile() As String
        Return System.IO.Path.GetTempPath & RandomString(20, True)
    End Function

    Public Shared Function GetTempPath() As String
        Return System.IO.Path.GetTempPath
    End Function


    Private Function RandomNumber(min As Integer, max As Integer) As Integer
        Dim random As New Random()
        Return random.Next(min, max)
    End Function 'RandomNumber


    '/ '/ Generates a random string with the given length 
    '/ '/ Size of the string '/ If true, generate lowercase string 
    '/ Random string 
    Public Shared Function RandomString(ByVal size As Integer, ByVal lowerCase As Boolean) As String
        Dim builder As New StringBuilder()
        Dim random As New Random()
        Dim ch As Char
        Dim i As Integer
        For i = 0 To size - 1
            ch = Convert.ToChar(Convert.ToInt32((26 * random.NextDouble() + 65)))
            builder.Append(ch)
        Next
        If lowerCase Then
            Return builder.ToString().ToLower()
        End If
        Return builder.ToString()
    End Function 'RandomString

    Public Shared Function SanitizeFilename(ByVal fn As String) As String
        'Clean just a filename
        Dim allowedChars As String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_"
        Dim filename As String = String.Empty
        For Each c In fn
            If allowedChars.Contains(c) Then
                filename = filename & c
            Else
                If c <> Chr(32) Then filename = filename & "_"
            End If
        Next
        Return filename.ToLower
    End Function

End Class
