﻿Imports HSPinOne.DAL

Public Class BookingReplaceObject
    Inherits ReplaceObject

    Private ReadOnly _booking As Booking

    Sub New(ByVal b As Booking)
        Prefx = "buchung."
        _booking = b
        Add("bookingid", b.BookingID.ToString())
        Add("buchungsnummer", b.BookingID.ToString())
        Add("name", b.Subject)
        Add("beschreibung", b.Description)
        
    End Sub

End Class
