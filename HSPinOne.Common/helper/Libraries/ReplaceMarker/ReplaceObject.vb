﻿Public Class ReplaceObject

    Private _table As List(Of String) = New List(Of String)
    Public Property Table() As List(Of String)
        Get
            Return _table
        End Get
        Set(ByVal value As List(Of String))
            _table = value
        End Set
    End Property

    Public Output As Dictionary(Of String, String) = New Dictionary(Of String, String)()
    Protected Prefx As String = ""
    Protected Function NullableToString(ByVal i As Nullable(Of Long))
        If i Is Nothing Then
            Return ""
        End If
        Return i
    End Function

    Protected Function NullableToString(ByVal i As Nullable(Of Integer))
        If i Is Nothing Then
            Return ""
        End If
        Return i
    End Function

    Protected Function NullableToString(ByVal i As Nullable(Of DateTime))
        If i Is Nothing Then
            Return ""
        End If
        Return i.Value.ToShortDateString()
    End Function

    Public Sub Add(key As String, value As String)
        If Not Output.ContainsKey(key) Then
            Output.Add(String.Format("{0}{1}", Prefx, key), value)
        End If

    End Sub

    Public Sub AddTableLine(ByVal str As String)
        Table.Add(str)
    End Sub
End Class
