﻿Imports HSPinOne.DAL

Public Class CourseReplaceObject
    Inherits ReplaceObject

    Private _course As Course

    Sub New(ByVal c As Course)
        Prefx = "kurs."
        _course = c
        Add("kursid", c.CourseID.ToString())
        Add("veranstaltungsnummer", c.CourseID.ToString())
        Add("kursname", Me.MakeKursname(c.Zusatzinfo))
        Add("semester", c.Term.Termname)
        Add("semesterkurz", c.Term.Termshortname)
        Add("zeit", MakeZeittext(c.Zeittext))
        Add("block", MakeBlock())
        Add("start", c.DTStart.ToShortDateString() & " " & c.DTStart.ToShortTimeString)
        Add("ende", c.DTEnd.ToShortDateString & " " & c.DTEnd.ToShortTimeString)
        Add("bis", c.DTUntil.ToShortDateString & " " & c.DTUntil.ToShortTimeString)

        Me.GetLocation(c.Location)
        Me.GetAnsprechpartner(c.AnsprechpartnerID)
    End Sub

    Private Function MakeBlock() As String
        If Not IsNothing(_course.Appointments) And _course.Appointments.Count > 0 Then
            Dim apps = _course.Appointments.OrderBy(Function(o) o.Start)
            Return _
                String.Format("{0} - {1}", apps.First().Start.ToShortDateString(),
                              apps.Last().Start.ToShortDateString())
        End If
        Return ""
    End Function

    Private Function MakeZeittext(ByVal zt As String) As String
        If String.IsNullOrEmpty(zt) Then
            Return _
                _course.DTStart.ToString("ddd") & ". " & _course.DTStart.ToShortTimeString() & " - " &
                _course.DTEnd.ToShortTimeString()
        End If
        Return zt
    End Function

    Private Function MakeKursname(ByVal zi As String) As String
        If String.IsNullOrEmpty(zi) Then
            Return _course.Sport.Sportname
        End If
        Return _course.Sport.Sportname & " " & zi
    End Function

    Private Sub GetLocation(ByVal l As Location)
        If l Is Nothing Then
            Add("ortsname", "")
            Add("strasse", "")
            Add("plz", "")
            Add("ort", "")
        Else
            Add("ortsname", l.LocationName)
            Add("strasse", l.Strasse)
            Add("plz", l.PLZ)
            Add("ort", l.Ort)
        End If
    End Sub

    Private Sub GetAnsprechpartner(ByVal a As Nullable(Of Integer))
        If a Is Nothing Then
            Add("ansprechpartner", "N.N.")
        Else
            Add("ansprechpartner", _course.Ansprechpartner.Suchname)
        End If
    End Sub
End Class
