﻿Imports System.Text
Imports HSPinOne.DAL

Public Class BillingReplaceObject
    Inherits ReplaceObject




    Sub New(ByVal b As Billing)
        Prefx = "rechnung."

        Add("billingid", b.BillingID.ToString())
        Add("rechnungsnummer", b.BillingID.ToString())
        Add("preis", b.Brutto)
        Add("preisbrutto", b.Brutto.ToString("c"))
        Add("preisnetto", MakeNettoPrice(b.Brutto, b.Steuerprozent))
        Add("steuerbetrag", MakeSteuerbetrag(b.Brutto, b.Steuerprozent))
        Add("status", MakeBillingStatus(b.BillingStatus))
        Add("faelligkeit", b.Faelligkeit.ToShortDateString())
        Add("rechnungsdatum", b.Rechnungsdatum.ToShortDateString())
        Add("verwendungszweck", b.Verwendungszweck)
        Add("bemerkung", b.Bemerkung)
        Add("steuerprozent", b.Steuerprozent)
        Add("mandatsreferenz", String.Format("{0}-{1}", b.BillingID, b.CustomerID))
        Add("kstextern", b.KSTextern)
        Add("innenauftrag", b.Innenauftrag)
        Add("sachkonto", b.Sachkonto)



    End Sub

    Sub New()
        Prefx = "rechnung."
        Table.Add("Verwendungszweck;Netto;Steuerbetrag;Brutto")
    End Sub

    Public Sub AddTableBillingLine(ByVal b As Billing)
        Table.Add(b.Verwendungszweck & ";" &
        MakeNettoPrice(b.Brutto, b.Steuerprozent) & ";" &
        MakeSteuerbetrag(b.Brutto, b.Steuerprozent) & ";" &
        b.Brutto.ToString("c") & "")
    End Sub

    Public Sub AddTableSum(ByVal vwz As String, ByVal brutto As Double, ByVal steuerbetrag As Double, ByVal steuerprozent As Double, ByVal netto As Double)
        Table.Add(vwz & ";" & netto.ToString("c") & ";" & steuerbetrag.ToString("c") & ";" & brutto.ToString("c"))
    End Sub



    Private Function MakeNettoPrice(ByVal price As Decimal, ByVal percent As Nullable(Of Decimal)) As String
        If percent Is Nothing Or percent = 0 Then
            Return price.ToString("c")
        End If
        Return (price - (price * percent.Value / 100)).ToString("c")
    End Function

    Private Function MakeSteuerbetrag(ByVal price As Decimal, ByVal percent As Nullable(Of Decimal)) As String
        If percent Is Nothing Or percent = 0 Then
            Return 0.ToString("c")
        End If
        Return (price / (percent.Value + 100) * 19).ToString("c")
    End Function

    Private Function MakeBillingStatus(ByVal bs As String) As String
        Dim status = New Billingstatuslist().Liste
        Return status.FirstOrDefault(Function(o) o.StatusID = bs).Statusname
    End Function
End Class


