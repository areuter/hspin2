﻿'-------------------------------------------
'Helper im in einem String definierte Marker zu ersetzen
'
'
'    .AddReplacement(Suchbegriff, Ersetzung)
'
'    .Show()   Zeigt das Dokument an
'
'    Ein Vorlagenpfad kann mittels des Stringproperty Vorlage gesetzt werden.
'    Ist keine gesetzt, dann wird der Vorlagenauswahldialog aufgerufen
'
'------------------------------------------------AR 2012

Public Class ReplaceMarker
    Public Property Replacement As Dictionary(Of String,String)
    Public Property DataProvider As Object
    Public Property Template As String
    Public Property FinalString As String

    Public Sub New(ByVal vorlage As String)
        Try

            Replacement = New Dictionary(Of String, String)
            FinalString = ""
            Template = vorlage
            AddDefaults()
        Catch ex As Exception
            MsgBox("Es ist ein Fehler beim Ersetzen der Marker aufgetreten." & Environment.NewLine & ex.Message)
        End Try
    End Sub

    Public Sub New(ByVal vorlage As String, list As Dictionary(Of String, String))
        Try
            Replacement = list
            FinalString = ""
            Template = vorlage
            AddDefaults()
        Catch ex As Exception
            MsgBox("Es ist ein Fehler beim Ersetzen der Marker aufgetreten." & Environment.NewLine & ex.Message)
        End Try
    End Sub

    Private Sub AddDefaults()
        Replacement.Add("heute.zeit", DateTime.Now.ToShortTimeString())
        Replacement.Add("heute.datum", DateTime.Now.ToShortDateString())
        Replacement.Add("heute.datumzeit", DateTime.Now)
    End Sub

    Public Sub AddReplacement(ByVal search As String, ByVal replaceWith As String)
        Replacement.Add(search, ReplaceWith)
    End Sub

    Public Sub DoReplacement()
        FinalString = Template
        For Each itm In Replacement
            ReplaceText(itm.Key, itm.Value)
        Next
    End Sub


    Private Sub ReplaceText(ByVal find As String, ByVal replace As String)
        FinalString = FinalString.Replace("{" + find + "}", replace)
    End Sub

End Class
