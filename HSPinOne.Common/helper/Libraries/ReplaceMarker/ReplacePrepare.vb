﻿Imports HSPinOne.DAL

Public Class xReplacePrepare
    Public Function Prepare(ByVal obj As Customer)
        obj.BLZ = CheckForNull(obj.BLZ)


        Return obj
    End Function

    Private Function CheckForNull(ByVal i As Nullable(Of Integer))
        If i Is Nothing Then
            Return ""
        End If
        Return i
    End Function

End Class
