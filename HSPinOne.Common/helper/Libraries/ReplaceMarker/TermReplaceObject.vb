﻿Imports HSPinOne.DAL

Public Class TermReplaceObject
    Inherits ReplaceObject

    Sub New(ByVal t As Term)
        Prefx = "semester."
        Add("termid", t.TermID.ToString())
        Add("semesternummer", t.TermID.ToString())
        Add("semester", t.Termname)
        Add("semesterkurz", t.Termshortname)
        Add("start", NullableToString(t.Semesterbeginn))
        Add("ende", NullableToString(t.Semesterende))

    End Sub

End Class
