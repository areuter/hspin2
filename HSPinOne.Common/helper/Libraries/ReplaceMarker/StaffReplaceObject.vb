﻿Imports HSPinOne.DAL
Imports System.Text
Imports System.Data.Entity

Public Class StaffReplaceObject
    Inherits ReplaceObject

    Private _staff As StaffContract
    Private Const Lprefx As String = "staff."
    Public StundenList As List(Of String) = New List(Of String)()

    Sub New(ByVal s As StaffContract)
        Prefx = Lprefx
        _staff = s
        Add("contractid", s.StaffContractID.ToString())
        Add("vertragsnummer", s.StaffContractID.ToString())
        Add("beginn", s.Beginn.ToShortDateString())
        Add("ende", s.Ende.ToShortDateString())
        Add("kurse", Me.GetCourses(s, "sztlw")) 'sport,zeittext,term,location,wage
        Add("kursesws", Me.GetCourses(s, "ch")) 'course,hours
        Add("sportart", Me.GetCourses(s, "s")) ' nur Sportart
        Add("sportartgehalt", Me.GetCourses(s, "sw")) ' Sportart und Gehalt

    End Sub

    Sub New(ByVal courseid As Integer, ByVal customerid As Integer)
        Prefx = Lprefx
        'StundenList.Add("Datum;Uhrzeit;Min.;TZ;Unterschrift")
        'Dim oldstart As DateTime
        'For Each app In a
        '    If app.Canceled = False And app.Start <> oldstart Then
        '        If app.AppointmentStaffs.Count = 0 Then
        '            Dim lohn As Decimal = DateDiff(DateInterval.Minute, app.Start, app.Ende) * stdLohn / 60
        '            StundenList.Add(
        '                app.Start.ToShortDateString & ";" & app.Start.ToShortTimeString & "-" &
        '                app.Ende.ToShortTimeString & ";" &
        '                DateDiff(DateInterval.Minute, app.Start, app.Ende) & ";;") ' & lohn.ToString("c"))

        '        Else
        '            For Each slot In app.AppointmentStaffs
        '                If slot.CustomerID = customerId Then
        '                    Dim lohn As Decimal = DateDiff(DateInterval.Minute, app.Start, app.Ende) * slot.StdLohn / 60
        '                    StundenList.Add(
        '                        app.Start.ToShortDateString & ";" & app.Start.ToShortTimeString & "-" &
        '                        app.Ende.ToShortTimeString & ";" &
        '                        DateDiff(DateInterval.Minute, app.Start, app.Ende) & ";;") ' & lohn.ToString("c"))
        '                End If
        '            Next
        '        End If
        '    End If
        '    oldstart = app.Start
        'Next
        ''Summenzeile
        'StundenList.Add("Summen:")
        GetAppointments(courseid, customerid)
    End Sub

    Private Sub GetAppointments(ByVal courseid As Integer, ByVal customerid As Integer)
        Using con As New in1Entities

            StundenList.Add("Datum;Uhrzeit;Min.;Gehalt;TZ;Unterschrift")
            Dim query = From c In con.AppointmentStaffs.Include(Function(o) o.Appointment)
                        Where c.CustomerID = customerid AndAlso
                         c.Appointment.CourseID = courseid AndAlso
                            c.Appointment.Canceled = False
                        Order By c.Appointment.Start

            Dim result = query.ToList()
            Dim olddate As DateTime = New DateTime
            For Each itm In query
                Dim line As String

                line = itm.Appointment.Start.ToShortDateString & ";" &
                    itm.Appointment.Start.ToShortTimeString() & ";" &
                    (itm.Appointment.Ende - itm.Appointment.Start).TotalMinutes & ";" &
                    (itm.Appointment.Ende - itm.Appointment.Start).TotalMinutes * itm.StdLohn / 60 & ";;"

                If Not olddate = itm.Appointment.Start Then
                    StundenList.Add(line)
                End If
                olddate = itm.Appointment.Start

            Next
        End Using
        StundenList.Add("Summen:")
    End Sub

    Private Function GetCourses(ByVal sc As StaffContract, ByVal param As String) As String

        Dim sb As New StringBuilder()
        Select Case param
            Case "sztlw"
                For Each itm As CourseStaff In sc.CourseStaffs
                    sb.AppendLine(String.Format("{0} {1} {2}{3}{4}{3}{5}", itm.Course.Sport.Sportname, itm.Course.Zeittext,
                                                itm.Course.Term.Termname, vbTab, itm.Course.Location.LocationName,
                                                itm.StdLohn.ToString("c")))
                Next

            Case "ch"
                For Each itm As CourseStaff In sc.CourseStaffs
                    Dim termine = itm.Course.Appointments.OrderBy(Function(o) o.Start).ThenBy(Function(o) o.LocationID).ToList()
                    Dim minutes As Decimal = 0
                    Dim diff As TimeSpan = New TimeSpan()
                    For Each appointment As Appointment In termine
                        diff = appointment.Ende - appointment.Start
                        minutes += diff.Ticks
                    Next
                    sb.AppendLine(String.Format("{0} {1} {2}", itm.Course.Sport.Sportname & " " & itm.Course.Zusatzinfo, vbTab, "EStd. " & Math.Round((TimeSpan.FromTicks(minutes).TotalMinutes / 45))))
                Next

            Case "sw"
                For Each itm In sc.CourseStaffs
                    sb.AppendLine(itm.Course.Sport.Sportname & vbTab & itm.StdLohn.ToString("c"))
                Next

            Case "s"
                For Each itm In sc.CourseStaffs
                    sb.AppendLine(itm.Course.Sport.Sportname)
                Next

        End Select
        Dim str = sb.ToString()
        sb.Clear()
        Return str
    End Function
End Class
