﻿Imports HSPinOne.DAL

Public Class CustomerReplaceObject
    Inherits ReplaceObject

    Sub New(ByVal c As Customer)
        Prefx = "kunde."
        Add("kundenid", c.CustomerID.ToString())
        Add("kundennummer", c.CustomerID.ToString())
        Add("nachname", c.Nachname)
        Add("vorname", c.Vorname)
        Add("name", c.Suchname)
        Add("anrede", MakeAnrede(c.Gender))
        Add("geburtstag", NullableToString(c.Geburtstag))
        Add("titel", MakeTitel(c.Titel))
        Add("strasse", c.Strasse)
        Add("plz", c.PLZ)
        Add("ort", c.Ort)
        Add("briefanrede", c.Briefanrede)
        Add("kontoinhaber", c.Kontoinhaber)
        Add("iban", c.IBAN)
        Add("bic", c.BIC)
        'Add("mandatsreferenz", c.CustomerID.ToString & "-" & c.Mandatscounter.ToString)
        Add("mandatsreferenz", String.Format("{0}-{1}", c.CustomerID, c.Mandatscounter))
        Add("mref", String.Format("{0}-{1}", c.CustomerID, c.Mandatscounter))
        'obsolete
        'Add("kontonummer", NullableToString(c.Kontonummer))
        'Add("blz", NullableToString(c.BLZ))

        Add("matrikelnummer", c.Matrikelnummer)

        Add("mail1", c.Mail1)
        Add("mail", c.Mail1)
        Add("telefon1", c.Telefon1)
        Add("telefon", c.Telefon1)

        Add("status", GetCustomerState(c.CustomerStateID))

        Add("firma", c.Company)

        'Metas hinzufügen
        Using con As New in1Entities
            Dim metas = con.CustomerMetas.Where(Function(o) o.CustomerID = c.CustomerID)
            If Not IsNothing(metas) Then
                For Each meta In metas
                    Add("meta." & meta.MetaKey, meta.MetaValue)
                Next

            End If
        End Using


    End Sub

    Private Function GetCustomerState(ByVal csid As Integer) As String
        Using con As New in1Entities
            Dim cs = (From c In con.CustomerStates Where c.CustomerStateID = csid Select c).FirstOrDefault

            If cs Is Nothing Then
                Return "unbekannter Status"
            End If
            Return cs.CustomerStatename

        End Using

    End Function

    Private Function MakeAnrede(ByVal g As String)
        Select Case g
            Case "m"
                Return "Herrn"
            Case Else
                Return "Frau"
        End Select
    End Function

    Private Function MakeTitel(ByVal g As String) As String
        If Not String.IsNullOrEmpty(g) Then
            Return g + " "
        End If
        Return g
    End Function
End Class
