﻿Imports HSPinOne.DAL
Imports System.Text

Public Class AppointmentReplaceObject
    Inherits ReplaceObject

    ReadOnly _appointmentList As List(Of Appointment)

    Sub New(ByVal a As List(Of Appointment))
        Prefx = "termin."
        _appointmentList = a
    End Sub

    ''' <summary>
    ''' Appointment formatiert zurückgeben in einer Zeile, Kommagetrennt
    ''' Default {0} {1}-{3} {4}
    ''' Parameterreihenfolge für String.Format()
    ''' Start.ToShortDateString
    ''' Start.ToShortTimeString
    ''' Ende.ToShortDateString
    ''' Ende.ToShortTimeString
    ''' Location.LocationName
    ''' </summary>
    ''' <param name="format"></param>
    ''' <remarks></remarks>
    Public Sub ListToString(Optional ByVal format As String = "{0} {1}-{3} {4}, ")
        Dim sb As New StringBuilder()
        For Each app As Appointment In _appointmentList
            sb.Append(String.Format(format, app.Start.ToShortDateString, app.Start.ToShortTimeString,
                                    app.Ende.ToShortDateString,
                                    app.Ende.ToShortTimeString, app.Location.LocationName))
        Next

        Dim str = sb.ToString().Trim().TrimEnd(",")
        sb.Clear()
        Add("liste", str)
    End Sub

    ''' <summary>
    ''' Appointment formatiert zurückgeben mit Zeilenumbruch
    ''' Default {0} {1}-{3} {4}
    ''' Parameterreihenfolge für String.Format()
    ''' Start.ToShortDateString
    ''' Start.ToShortTimeString
    ''' Ende.ToShortDateString
    ''' Ende.ToShortTimeString
    ''' Location.LocationName
    ''' </summary>
    ''' <param name="format"></param>
    ''' <remarks></remarks>
    Public Sub ListToStringWithNewLine(Optional ByVal format As String = "{0} {1}-{3} {4}")
        Dim sb As New StringBuilder()
        For Each app As Appointment In _appointmentList
            sb.AppendLine(String.Format(format, app.Start.ToShortDateString, app.Start.ToShortTimeString,
                                    app.Ende.ToShortDateString,
                                    app.Ende.ToShortTimeString, app.Location.LocationName))
        Next

        Dim str = sb.ToString()
        sb.Clear()
        Add("liste", str)
    End Sub
End Class
