﻿Imports HSPinOne.Common
Imports HSPinOne.Infrastructure
Imports HSPinOne.DAL
Public Class Dokviewer

    Public Shared Sub Dokumentanzeigen(ByVal strDokument)

        Try



            Dim Archivpfad = GlobalSettingService.in1DataFolder(in1Folder.documentsarchive)


            Dim p As New System.Diagnostics.Process
            Dim s As New System.Diagnostics.ProcessStartInfo(Archivpfad & strDokument)
            s.UseShellExecute = True
            s.WindowStyle = ProcessWindowStyle.Normal
            p.StartInfo = s
            p.Start()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Shared Sub ShowFilestream(ByVal strDok As Document)



        If Not IsNothing(strDok) Then
            Dim fileName = System.IO.Path.GetTempPath() & strDok.Dateiname & Guid.NewGuid().ToString() & "." & strDok.Extension

            System.IO.File.WriteAllBytes(fileName, strDok.DocumentData)
            Dim p As New System.Diagnostics.Process
            Dim s As New System.Diagnostics.ProcessStartInfo(fileName)
            s.UseShellExecute = True
            s.WindowStyle = ProcessWindowStyle.Normal
            p.StartInfo = s
            p.Start()

        End If

    End Sub

    Public Shared Function GetFilestreamFilePath(ByVal strDok As Document) As String


        If Not IsNothing(strDok) Then
            Dim fileName = System.IO.Path.GetTempPath() & "in1" & strDok.Dateiname & Guid.NewGuid().ToString() & "." & strDok.Extension

            System.IO.File.WriteAllBytes(fileName, strDok.DocumentData)
            Return fileName
        End If
        Return String.Empty
    End Function

End Class
