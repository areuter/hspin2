﻿Imports HSPinOne.Infrastructure

Public Class MailViewModel

    Public Event RequestClose As EventHandler

    Private _Subject As String
    Public Property Subject() As String
        Get
            Return _Subject
        End Get
        Set(ByVal value As String)
            _Subject = value
        End Set
    End Property

    Private _Message As String
    Public Property Message() As String
        Get
            Return _Message
        End Get
        Set(ByVal value As String)
            _Message = value
        End Set
    End Property

    Private _Address As String
    Public Property Address() As String
        Get
            Return _Address
        End Get
        Set(ByVal value As String)
            _Address = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal address As String, ByVal subject As String, ByVal message As String)
        _Address = address
        _Subject = subject
        _Message = message
    End Sub
    Public Property SendCommand = New RelayCommand(AddressOf Send)
    Private Sub Send()
        Dim mail As New clsMail
        mail.SendSMTP(_Address, _Subject, _Message)
        RaiseEvent RequestClose(Nothing, Nothing)
    End Sub

End Class
