﻿Imports System.Threading
Imports HSPinOne.Common.SendFileTo
Imports System.Text.RegularExpressions
Imports System.Windows
Imports System.ComponentModel
Imports HSPinOne.Infrastructure
Imports System.Net.Mail
Imports HSPinOne.Common
Imports System.IO

Public Class clsMail



    Private Property Recipients As List(Of String)

    Private Property BccRecipients As List(Of String)

    Public Property Subject As String

    Public Property Body As String = ""



    Public Property ID As Long

    Public Property Errormessage As String

    Public Property Attachments As List(Of String)

    Public Property LinkedResources As List(Of LinkedResource)

    Private WithEvents bgw As New BackgroundWorker

    Private plainView As AlternateView
    Private htmlView As AlternateView




    Public Sub AddRecipient(ByVal strRec As String)
        If IsNothing(Recipients) Then
            Recipients = New List(Of String)
        End If
        If Not IsNothing(strRec) AndAlso Not Trim(strRec) = "" AndAlso strRec.Contains("@") = True Then
            ' Nur eintragen, wenn noch nicht in Liste
            If Not Recipients.Contains(strRec.ToLower) Then
                Recipients.Add(strRec.ToLower)
            End If
        End If
    End Sub

    Public Sub AddBccRecipient(ByVal strRec As String)
        If IsNothing(BccRecipients) Then
            BccRecipients = New List(Of String)
        End If
        If Not IsNothing(strRec) AndAlso Not LTrim(strRec) = "" AndAlso strRec.Contains("@") = True Then
            If Not BccRecipients.Contains(strRec.ToLower) Then BccRecipients.Add(strRec)
        End If
    End Sub

    Public Sub AddAttachment(ByVal filename As String)
        If IsNothing(Attachments) Then Attachments = New List(Of String)
        If Not IsNothing(filename) Then
            Attachments.Add(filename)
        End If
    End Sub

    Public Sub AddLinkedResource(ByVal fname As String, ByVal ms As MemoryStream)
        If IsNothing(LinkedResources) Then LinkedResources = New List(Of LinkedResource)
        Dim inline As LinkedResource = New LinkedResource(ms, "image/jpg")
        inline.ContentId = fname

        LinkedResources.Add(inline)
    End Sub


    Public Sub Mailto(ByVal sTo As String, sSubject As String, sBody As String, Optional lngID As Long = 0)

        AddRecipient(sTo)
        Me.Subject = sSubject
        Me.Body = ScrubHtml(sBody)
        SendinBackground()

        'Dim mapi As New SendFileTo.MAPI
        'mapi.AddRecipientTo(sTo)
        'mapi.SendMailPopup(sSubject, sBody)
    End Sub

    Public Sub SendSMTP(ByVal sTo As String, sSubject As String, sBody As String)
        Using con As New HSPinOne.DAL.in1Entities
            Dim settings = con.GlobalSettings.ToList()
            Dim dic As New Dictionary(Of String, String)
            For Each itm In settings
                dic.Add(itm.Einstellung, itm.Wert)
            Next
            Dim client As SmtpClient = New SmtpClient(dic.Item("smtphost"), Int32.Parse(dic.Item("smtpport")))

            client.Credentials = New System.Net.NetworkCredential(dic.Item("smtplogin"), dic.Item("smtppw"))
            '//smtpClient.UseDefaultCredentials = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network
            client.EnableSsl = True
            Dim mail = New MailMessage()

            '//Setting From , To And CC
            mail.From = New MailAddress(dic.Item("smtpfrom"))
            mail.To.Add(New MailAddress(sTo))

            mail.Subject = sSubject
            'mail.IsBodyHtml = True



            plainView = AlternateView.CreateAlternateViewFromString("Please use HTML Viewer", Nothing, "text/plain")
            htmlView = AlternateView.CreateAlternateViewFromString(sBody, Nothing, "text/html")
            mail.AlternateViews.Add(plainView)
            mail.AlternateViews.Add(htmlView)

            For Each itm In LinkedResources
                htmlView.LinkedResources.Add(itm)
            Next

            ' Replace \r\n by <br />

            'sBody = sBody.Replace("\r\n\r\n", "<br />")

            'mail.Body = sBody




            Dim info As New EmailMessageInfo
            info.RecipientName = sTo

            AddHandler client.SendCompleted, AddressOf SmtpClientSendCompleted
            client.SendAsync(mail, New SendAsyncState(info))
        End Using
    End Sub

    Private Sub SmtpClientSendCompleted(ByVal sender As Object, ByVal e As AsyncCompletedEventArgs)
        Dim smtpClient = CType(sender, SmtpClient)
        Dim userAsyncState = CType(e.UserState, SendAsyncState)
        RemoveHandler smtpClient.SendCompleted, AddressOf SmtpClientSendCompleted

        If e.Error IsNot Nothing Then
            Errormessage = String.Format("Message sending for ""{0}"" failed.", userAsyncState.EmailMessageInfo.RecipientName)
            Throw e.Error
        End If
    End Sub

    Public Sub MailWindow(ByVal sTo As String, sSubject As String, sBody As String)

        Dim win As New WindowController()
        Dim vm As New MailViewModel(sTo, sSubject, sBody)
        Dim view As New MailView()
        view.DataContext = vm
        AddHandler vm.RequestClose, AddressOf win.CloseWindow
        win.ShowWindow(view, "Neue E-Mail")
        RemoveHandler vm.RequestClose, AddressOf win.CloseWindow

    End Sub
    Public Sub Send()

        Dim config = HSPinOne.Infrastructure.MachineSettingService.GetMachineSetting(Infrastructure.MachineSettingService.in1MaschineSettings.Mailer)

        If config = "Outlook" Then
            SendOutlook()
        Else
            Dim mapi As New MAPI
            If Not IsNothing(Recipients) Then

                For Each reci In Recipients
                    mapi.AddRecipientTo(reci)
                Next
            End If
            If Not IsNothing(BccRecipients) Then

                For Each bcc In BccRecipients
                    mapi.AddRecipientBCC(bcc)
                Next
            End If

            If Not IsNothing(Attachments) Then
                For Each itm In Attachments
                    mapi.AddAttachment(itm)
                Next
            End If

            Me.Body = ScrubHtml(Me.Body)
            mapi.SendMailPopup(Subject, Body)
        End If
    End Sub

    Public Sub SendSilentMapi()
        Dim mapi As New MAPI
        If Not IsNothing(Recipients) Then
            For Each reci In Recipients
                mapi.AddRecipientTo(reci)
            Next
        End If

        If Not IsNothing(BccRecipients) Then

            For Each bcc In BccRecipients
                mapi.AddRecipientBCC(bcc)
            Next
        End If

        If Not IsNothing(Attachments) Then
            For Each itm In Attachments
                mapi.AddAttachment(itm)
            Next
        End If
        Me.Body = ScrubHtml(Me.Body)
        mapi.SendMailDirect(Subject, Body)
    End Sub

    Private Function ScrubHtml(value As String) As String
        Dim step1 = Regex.Replace(value, "<[^>]+>|&nbsp;", "").Trim()
        'Dim step2 = Regex.Replace(step1, "\s{2,}", " ")
        Return step1
    End Function

    Private Sub SendOutlook()

        Dim objOutlook As Object
        Dim olMsg As Object

        objOutlook = CreateObject("Outlook.Application")
        olMsg = objOutlook.CreateItem(0)

        If Not IsNothing(Recipients) Then
            For Each reci In Recipients
                Dim recip = olMsg.Recipients.Add(reci)
                recip.Type = 1
            Next
        End If

        If Not IsNothing(BccRecipients) Then

            For Each bcc In BccRecipients
                Dim recip = olMsg.Recipients.Add(bcc)
                recip.Type = 3
            Next
        End If

        If Not IsNothing(Attachments) Then
            For Each itm In Attachments
                olMsg.Attachments.Add(itm)
            Next
        End If

        Me.Body = ScrubHtml(Me.Body)

        olMsg.Subject = Subject
        olMsg.Body = Body

        olMsg.Display()


        olMsg = Nothing
        objOutlook = Nothing


    End Sub

    'Public Sub SendOutlook()

    '    Dim oMailMessage As MailMessage = Nothing
    '    Dim oSmtpClient As SmtpClient = Nothing

    '    Try

    '        oMailMessage = New MailMessage()
    '        oMailMessage.From = New MailAddress(SmtpFrom)
    '        If Not IsNothing(Recipients) Then
    '            For Each reci In Recipients
    '                oMailMessage.[To].Add(reci)
    '            Next
    '        End If

    '        If Not IsNothing(BccRecipients) Then

    '            For Each bcc In BccRecipients
    '                oMailMessage.[Bcc].Add(bcc)
    '            Next
    '        End If

    '        oMailMessage.Subject = Subject
    '        oMailMessage.Body = Body
    '        oMailMessage.IsBodyHtml = True

    '        oSmtpClient = New SmtpClient(SmtpHost, SmtpPort)
    '        oSmtpClient.Credentials = New NetworkCredential(SmtpLogin, SmtpPw)
    '        'oSmtpClient.EnableSsl = True

    '        oSmtpClient.Send(oMailMessage)

    '    Catch ex As Exception
    '        MsgBox(Err.Number & ex.Message & ex.StackTrace.ToString) 'Falls ein Fehler auftritt wird eine MsgBox angezeigt
    '    End Try
    'End Sub

    Public Sub SendinBackground()

        'Dim th As New Thread(AddressOf Send)
        'th.SetApartmentState(ApartmentState.STA)
        'th.Start()
        If Not bgw.IsBusy Then
            bgw.RunWorkerAsync()
        End If
    End Sub

    Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw.DoWork
        Send()
    End Sub

    Private Sub bgw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles bgw.RunWorkerCompleted
        If Not IsNothing(e.Error) Then
            Throw New Exception(e.Error.Message)
        End If
    End Sub


    'Public Sub SendinBackgroundSmtp()

    '    Dim th As New Thread(AddressOf SendSilentSmtp)
    '    th.SetApartmentState(ApartmentState.STA)
    '    th.Start()
    'End Sub
End Class
Public Interface IEmailMessageInfo
    Property RecipientName As String
End Interface

Public Class EmailMessageInfo
    Implements IEmailMessageInfo

    Private _recipientname As String
    Public Property RecipientName As String Implements IEmailMessageInfo.RecipientName
        Get
            Return _recipientname
        End Get
        Set(value As String)
            _recipientname = value
        End Set
    End Property
End Class

Public Class SendAsyncState
    Public Property EmailMessageInfo As IEmailMessageInfo
    Public Sub New(ByVal info As IEmailMessageInfo)
        EmailMessageInfo = info

    End Sub

    Public Sub New()

    End Sub
End Class