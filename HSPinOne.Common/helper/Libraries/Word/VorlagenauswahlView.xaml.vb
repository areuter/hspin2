﻿Imports HSPinOne.Infrastructure
Imports System.Windows.Input
Imports HSPinOne.Common
Imports HSPinOne.DAL


Public Class VorlagenauswahlView


    Private _files As List(Of String)
    Private _selectedfile As String

    Public Property Files As List(Of String)
        Get
            Return _files
        End Get
        Set(value As List(Of String))
            _files = value
        End Set
    End Property

    Public Property SelectedFile As String
        Get
            Return _selectedfile
        End Get
        Set(value As String)
            _selectedfile = value
        End Set
    End Property
    Private _Docs As New Dictionary(Of Guid, String)
    Public Property Docs As Dictionary(Of Guid, String)
        Get
            Return _Docs
        End Get
        Set(value As Dictionary(Of Guid, String))
            _Docs = value
        End Set
    End Property

    Private _SelectedGuid As Guid
    Public Property SelectedGuid As Guid
        Get
            Return _SelectedGuid
        End Get
        Set(value As Guid)
            _SelectedGuid = value
        End Set
    End Property

    Public Property OKCommand As ICommand
    Public Property CancelCommand As ICommand


    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        OKCommand = New RelayCommand(AddressOf OK, AddressOf CanOK)
        CancelCommand = New RelayCommand(AddressOf Cancel)


        Using con As New in1Entities

            Dim query = From c In con.Documents Where c.DocumentType = 4 Select c.DocumentUID, c.Dateiname, c.Extension


            For Each itm In query.OrderBy(Function(o) o.Dateiname)
                Docs.Add(itm.DocumentUID, itm.Dateiname & "." & itm.Extension)
            Next

        End Using



        'Dim pfad As String = GlobalSettingService.in1DataFolder(in1Folder.doctemplates)
        'Dim di As New IO.DirectoryInfo(pfad)
        'Dim diar1 As IO.FileInfo() = di.GetFiles()
        'Dim dra As IO.FileInfo

        ''list the names of all files in the specified directory
        'Files = New List(Of String)
        'For Each dra In diar1
        '    If Not Mid(dra.Name, 1, 1) = "~" Then Files.Add(dra.Name)
        'Next

        Me.DataContext = Me
    End Sub


    Private Sub OK()
        Dim doc As Document
        Using con As New in1Entities
            doc = (From c In con.Documents Where c.DocumentUID = SelectedGuid).SingleOrDefault
        End Using
        If Not IsNothing(doc) Then
            Me.SelectedFile = Dokviewer.GetFilestreamFilePath(doc)
            Me.DialogResult = True
            Me.Close()
        Else
            Dim dlg As New DialogService
            dlg.ShowAlert("Fehler", "Das Dokument konnte nicht zwischengespeichert werden")
        End If


    End Sub

    Private Function CanOK(ByVal param As Object) As Boolean
        If Not IsNothing(SelectedGuid) Then Return True
        Return False

    End Function

    Private Sub Cancel()
        Me.Close()
    End Sub

End Class
