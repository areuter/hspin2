﻿'-------------------------------------------
'Suchen und Ersetzen in Word Dokumenten eines Vorlagenordners
'
'
'    .AddReplacement(Suchbegriff, Ersetzung)
'
'    .Show()   Zeigt das Dokument an
'
'    Ein Vorlagenpfad kann mittels des Stringproperty Vorlage gesetzt werden.
'    Ist keine gesetzt, dann wird der Vorlagenauswahldialog aufgerufen
'
'------------------------------------------------AR 2012
Imports HSPinOne.DAL



Public Class WordAutomation
    Private Const wdGoToLine = 3
    Private Const wdGoToAbsolute = 1
    Private Const wdFindContinue = 1
    Private Const wdReplaceAll = 2

    Public Property Replacement As Dictionary(Of String, String) = New Dictionary(Of String, String)

    Public Property Vorlage As String

    Public Property Vorschau As Boolean

    Public Property Customer As Customer

    Public Property Course As Course

    Private _table As List(Of String)
    Private _tablepos As String
    Private _tablealignment As String
    Private _tablewidths As String


    Public Sub New()
        Replacement = New Dictionary(Of String, String)
        Me.AddDefaults()
    End Sub


    Public Sub New(ByVal dict As Dictionary(Of String, String))
        Replacement = dict
        Me.AddDefaults()
    End Sub

    Private Sub AddDefaults()
        Replacement.Add("heute.zeit", DateTime.Now.ToShortTimeString())
        Replacement.Add("heute.datum", DateTime.Now.ToShortDateString())
        Replacement.Add("heute.datumzeit", DateTime.Now)
    End Sub

    Public Sub AddReplacement(ByVal search As String, ByVal replaceWith As Object)
        If Not IsNothing(replaceWith) Then
            Replacement.Add(search, replaceWith)
        Else
            Replacement.Add(search, "")
        End If
    End Sub

    Public Sub SetReplacementList(ByVal dict As Dictionary(Of String, String))
        Replacement = dict
    End Sub

    ''' <summary>
    ''' Tabelle einfügen
    ''' </summary>
    ''' <param name="position">Markername</param>
    ''' <param name="tablelist">Tabelle als List, Spalten mit Semikolon getrennt</param>
    ''' <param name="align">String mit Ausrichtung der Spalten z.B. lcr</param>
    Public Sub AddTable(ByVal position As String, ByVal tablelist As List(Of String), Optional ByVal align As String = "l", Optional widths As String = "")
        _tablepos = position
        _table = tablelist
        _tablealignment = align.ToLower()
        _tablewidths = widths
    End Sub


    Public Sub Show()
        Dim oWord As Object
        Dim oDoc As Object
        
        Try
            If IsNothing(Vorlage) Then
                Dim dlg As New VorlagenauswahlView
                If dlg.ShowDialog Then
                    Vorlage = (dlg.SelectedFile).ToString
                Else
                    Return
                End If
            End If

            If IsNothing(Vorlage) Then
                MsgBox("Keine Vorlage")
                Return
            End If

            'Dokumentvorlagenpfad holen

            'Dim Pfad As String = HSPinOne.Infrastructure.GlobalSettingService.in1DataFolder(in1Folder.doctemplates)

            'Kopieren nach Lokal um den HRESULT Fehler zu umgehen
            'System.IO.File.Copy(Pfad & Vorlage, System.IO.Path.GetTempPath & Vorlage, True)

            'Dim resourceInfo = System.Windows.Application.GetResourceStream(
            '                   New Uri(Pfad & Vorlage, UriKind.Relative))
            'Dim fileName = CopyStreamToTempFile(resourceInfo.Stream, ".docx")

            oWord = CreateObject("Word.Application")
            If IsNothing(oWord) Then
                MsgBox("Word kann nicht gestartet werden")
                Return
            End If


            'Defaultfelder hinzufügen
            'AddReplacement("Heute", Now.ToShortDateString)

            ''Customerfelder hinzufügen
            'If Not IsNothing(Customer) Then
            '    AddReplacement("Vorname", Customer.Vorname)
            '    AddReplacement("Nachname", Customer.Nachname)
            '    AddReplacement("Anrede", IIf(Customer.Gender = "m", "Herrn", "Frau"))
            '    AddReplacement("Geburtstag", Customer.Geburtstag)
            '    AddReplacement("Titel", IIf(Customer.Titel = "", "", Customer.Titel & " "))
            '    AddReplacement("Strasse", Customer.Strasse)
            '    AddReplacement("PLZ", Customer.PLZ)
            '    AddReplacement("Ort", Customer.Ort)
            '    AddReplacement("Status", Customer.CustomerState.CustomerStatename)
            '    AddReplacement("Briefanrede", Customer.Briefanrede)
            '    AddReplacement("Kontoinhaber", Customer.Kontoinhaber)
            '    AddReplacement("Kontonummer", Customer.Kontonummer)
            '    AddReplacement("BLZ", Customer.BLZ)
            '    AddReplacement("IBAN", Customer.IBAN)
            '    AddReplacement("BIC", Customer.BIC)
            '    AddReplacement("Telefon1", Customer.Telefon1)
            '    AddReplacement("Telefon2", Customer.Telefon2)
            '    AddReplacement("Mail1", Customer.Mail1)
            '    AddReplacement("Mail2", Customer.Mail2)
            'End If

            ''Coursefelder hinzufügen
            'If Not IsNothing(Course) Then
            '    AddReplacement("Kursname", Course.Sport.Sportname & " " & Course.Zusatzinfo)
            '    AddReplacement("Sportart", Course.Sport.Sportname)
            '    AddReplacement("Zusatzinfo", Course.Zusatzinfo)
            '    AddReplacement("Zeittext", Course.Zeittext)
            'End If


            With oWord
                oDoc = .Documents.Add(Template := Vorlage, NewTemplate := False)
                .Selection.Find.ClearFormatting()
                .Selection.Find.Replacement.ClearFormatting()
                .Visible = True
                .Activate()
                Dim oRange = oDoc.Range()

                'Wenn vorhanden Tabelle einfügen
                PrintTable(oDoc, oRange)


                'Suchen und Ersetzen
                For Each itm In Replacement
                    ReplaceText(oDoc, oRange, itm.Key, itm.Value)
                Next
            End With

            oWord = Nothing

        Catch ex As Exception
            MsgBox("Es ist ein Fehler beim Erstellen des Word Dokuments aufgetreten." & vbNewLine & ex.Message)
        End Try
    End Sub

    Private Sub PrintTable(ByVal oDoc As Object, ByVal oRange As Object)

        Dim rows As Long
        Dim cols As Long

        Dim headercols As Integer = 0

        If Not IsNothing(_table) Then
            cols = UBound(Split(_table(0), ";")) + 1
            rows = _table.Count

            'Tabellenposition festlegen
            oRange.Find.Text = "{" & _tablepos & "}"
            oRange.Find.Replacement.Text = ""
            oRange.Find.Execute(Replace := 1)

            'Tabelle einfügen
            Dim tbl = oDoc.Tables.Add(oRange, rows, cols)

            'Style definieren für Rahmen
            Dim styl As Object = oDoc.Styles.Add("tt", 3)
            With styl.Table
                'Apply borders around table 
                .Borders(- 1).LineStyle = 1
                .Borders(- 2).LineStyle = 1
                .Borders(- 3).LineStyle = 1
                .Borders(- 4).LineStyle = 1
                .Borders(- 5).LineStyle = 1
                .Borders(- 6).LineStyle = 1
            End With

            tbl.Range.Style = "tt"


            'Tabelle füllen
            Dim allowedalign As String() = {"l", "c", "r"}
            Dim line As Array
            For i = 1 To rows
                line = Split(_table(i - 1), ";")
                If i = 1 Then
                    headercols = line.Length
                End If
                Dim maxcol = UBound(line) + 1
                If maxcol > headercols Then maxcol = headercols
                For j = 1 To maxcol
                    Dim align = "l"
                    If j <= Len(_tablealignment) Then
                        align = Mid(_tablealignment, j, 1)
                        If Not allowedalign.Contains(align) Then
                            align = "l"
                        End If
                    End If

                    tbl.Cell(i, j).Range.Text = line(j - 1)

                    'Set alignment
                    Dim intalign As Integer = 0
                    Select Case align
                        Case "l" : intalign = 0
                        Case "c" : intalign = 1
                        Case "r" : intalign = 2
                    End Select

                    tbl.Cell(i, j).Range.ParagraphFormat.Alignment = intalign

                    'First Row Bold
                    If i = 1 Then
                        tbl.Cell(i, j).Range.Font.Bold = 1
                    End If

                    ' Weiten setzen
                    If Not Trim(_tablewidths) = "" Then
                        Dim ww = _tablewidths.Split(";")
                        If j <= ww.Count Then
                            tbl.Cell(i, j).SetWidth(CType(ww(j - 1), Integer), 1)
                        End If
                    End If
                Next
            Next

        End If
    End Sub

    Private Sub ReplaceText(ByVal oDoc As Object, ByVal oRange As Object, ByVal find As String, ByVal replace As String)
        With oRange.Find
            .Text = "{" + find + "}"
            .Forward = True
            .Wrap = wdFindContinue
            .Format = False
            .MatchCase = False
            .MatchWholeWord = True
            .MatchWildcards = False
            .MatchSoundsLike = False
            .MatchAllWordForms = False
            Do While .Execute = True
                oRange.Text = replace
            Loop


        End With
    End Sub
End Class
