﻿Imports System.IO
Imports HSPinOne.Infrastructure


Public Class CSVExport

    Private items As New List(Of String)

    Private _Dateiname As String = String.Empty
    Public ReadOnly Property Dateiname As String
        Get
            Return _Dateiname
        End Get
    End Property
    ''' <summary>
    ''' Fügt eine Zeile zur Datei hinzu
    ''' </summary>
    ''' <param name="Felder">Liste von Werten</param>
    ''' <remarks></remarks>
    Public Sub AddLine(ByVal ParamArray Felder() As Object)
        Dim line As String = String.Empty
        For i = 0 To UBound(Felder)
            line = line & CSVEncode(Felder(i)) & ";"
        Next
        items.Add(Left(line, Len(line) - 1))

    End Sub


    Public Sub Export()
        Dim writer As StreamWriter = Nothing
        Dim writeropen As Boolean
        Dim strSep As String = ";"


        Try


            If dateiname Is Nothing OrElse Trim(dateiname) = "" Then
                Exit Sub
            End If

            ' IsBusy = True

            writer = New StreamWriter(dateiname, False, Text.Encoding.UTF8)
            writeropen = True




            Dim i As Long = 1
            For Each itm In items
                writer.WriteLine(itm)
            Next
            If writeropen Then writer.Close()
            Dim dlg As New DialogService
            dlg.ShowInfo("Export", "Es wurden " & items.Count & " Datensätze exportiert")
            dlg = Nothing

        Catch ex As Exception
            MsgBox(ex.Message)
            If writeropen Then
                writer.Close()

            End If

        End Try
    End Sub


    ' encode function preserves commas in data fields
    Private Function CSVEncode(ByVal value As Object) As String

        

        If Not IsNothing(value) Then
            'Linebreaks ersetzen
            If TypeOf value Is String Then
                value = value.Replace(vbCrLf, "")
                value = value.Replace(vbCr, "")
                value = value.Replace(vbLf, "")
            End If


            Return """" & value & """"
        Else
            Return """" & " " & """"
        End If

    End Function

    Public Sub New()
        _Dateiname = "Export_" & Year(Now).ToString & Month(Now).ToString & Day(Now).ToString & Hour(Now).ToString & Minute(Now).ToString & Second(Now).ToString

        Dim dlg As New DialogService
        _Dateiname = dlg.SaveFile("CSV-Dateien (*.csv)|*.csv", Dateiname, ".csv")
    End Sub
End Class
