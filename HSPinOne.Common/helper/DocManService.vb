﻿Imports HSPinOne.DAL
Imports System.Data.Entity


Public Class DocManService

    ''' <summary>
    ''' Filter by filetype (Empty = All, comma-separated type: jpg,jpeg,gif
    ''' </summary>
    ''' <remarks></remarks>
    Private _Filetypes As String = String.Empty
    Public Property Filetypes() As String
        Get
            Return _Filetypes
        End Get
        Set(ByVal value As String)
            _Filetypes = value
        End Set
    End Property


    Public Shared Function GetDocument() As Object

        Dim result = Nothing
        Dim uc As New DocManView
        Dim vm = New ViewModel.DocManViewModel
        uc.DataContext = vm

        Dim win As New HSPinOne.Infrastructure.WindowController

        AddHandler vm.RequestClose, AddressOf win.CloseWindow

        win.ShowWindow(uc, "Dokumentenmanager")
        If Not IsNothing(vm.SelectedItem) Then
            result = vm.SelectedItem
        End If

        win = Nothing
        uc = Nothing
        vm = Nothing

        Return result
    End Function


    Public Shared Function GetImage() As String

        Dim result As String = String.Empty
        Dim uc As New DocManView
        Dim vm = New ViewModel.DocManViewModel
        uc.DataContext = vm

        Dim win As New HSPinOne.Infrastructure.WindowController

        AddHandler vm.RequestClose, AddressOf win.CloseWindow

        win.ShowWindow(uc, "Dokumentenmanager")
        If Not IsNothing(vm.SelectedItem) Then
            Dim itm = CType(vm.SelectedItem, Guid)
            If Not IsNothing(itm) AndAlso Not itm = Guid.Empty Then
                result = "[" & itm.ToString & "]"
            End If

        End If

        win = Nothing
        uc = Nothing
        vm = Nothing

        Return result
    End Function

    Public Shared Function GetFullDocument() As Document

        Dim result As Document = Nothing
        Dim uc As New DocManView
        Dim vm = New ViewModel.DocManViewModel
        uc.DataContext = vm

        Dim win As New HSPinOne.Infrastructure.WindowController

        AddHandler vm.RequestClose, AddressOf win.CloseWindow

        win.ShowWindow(uc, "Dokumentenmanager")
        If Not IsNothing(vm.SelectedItem) Then
            Dim itm = CType(vm.SelectedItem, Guid)
            If Not IsNothing(itm) AndAlso Not itm = Guid.Empty Then
                Using conn As New in1Entities
                    result = conn.Documents.Find(itm)
                End Using
            End If

        End If

        win = Nothing
        uc = Nothing
        vm = Nothing

        Return result
    End Function

End Class
