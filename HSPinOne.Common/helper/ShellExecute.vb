﻿Public Class ShellExecute

    Public Shared Sub RunAssociatedProgram(ByVal strDokument As Object)

        Try


            Dim p As New System.Diagnostics.Process
            Dim s As New System.Diagnostics.ProcessStartInfo(strDokument)
            s.UseShellExecute = True
            s.WindowStyle = ProcessWindowStyle.Normal
            p.StartInfo = s
            p.Start()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

End Class
