﻿Imports System.Text.RegularExpressions

Namespace IBAN


    Public Class IbanConverter



        Public Shared Function getIban(ByVal sBLZ As String, ByVal sKtoNr As String) As String


            Dim pz As Object
            Dim sIBAN As String
            Dim sChar As String
            Dim sTemp As String, sTmp As String
            Dim i As Integer

            '  sIBAN = sLand & "00" & Format$(sBLZ, String$(nBLZMaxLen, "0")) & _
            '     Format$(sKtoNr, String$(nKTOMaxLen, "0"))
            '  sIBAN = Format$(sBLZ, String$(nBLZMaxLen, "0")) & Format$(sKtoNr, String$(nKTOMaxLen, "0"))
            sIBAN = sBLZ.ToString.PadLeft(8, "0") & sKtoNr.ToString.PadLeft(10, "0") 'String$(nBLZMaxLen - Len(sBLZ), "0") & sBLZ & String$(nKTOMaxLen - Len(sKtoNr), "0") & sKtoNr

            sTemp = ""

            '  sIBAN = Right$(sIBAN, Len(sIBAN) - 4) & Left$(sIBAN, 4)
            sTmp = sIBAN & "DE" & "00"

            ' *** Alle Buchstaben in Zahlen wandeln ***
            For i = 1 To Len(sTmp)   '(sIBAN)
                sChar = Mid$(sTmp, i, 1) ' sIBAN
                If Asc(sChar) > 64 And Asc(sChar) < 91 Then
                    sChar = CStr(Asc(sChar) - 55)
                End If
                sTemp = sTemp & sChar   ' Ergebnis in sTemp
            Next i

            pz = 98 - modDecimal(CDec(sTemp), 97)

            '  sTemp = Left$(sTemp, Len(sTemp) - 2) & Trim$(Str$(pz))
            '  sTemp = Right$(sTemp, 6) & Left$(sTemp, Len(sTemp) - 6)

            '  getIban = Chr$(Val(Left$(sTemp, 2)) + 55) & _
            '    Chr$(Val(Mid$(sTemp, 3, 2)) + 55) & _
            '    Right$(sTemp, Len(sTemp) - 4)
            getIban = "DE" & Format$(pz, "00") & sIBAN

        End Function

        Public Shared Function modDecimal(Dividend, Divisor) As Object
            If Divisor = 0 Then
                modDecimal = -1
            Else
                modDecimal = Dividend - Divisor * (Math.Round(Dividend / Divisor))
                If modDecimal < 0 Then modDecimal = Divisor + modDecimal
            End If
        End Function

        Public Shared Function checkIban(ByVal sIban As String) As Boolean

            'Check auf richtige Länge
            If Len(sIban) <> 22 Then Return False

            'Deutsche Iban
            If Mid(sIban, 1, 2) <> "DE" Then Return False

            'Aufsplitten
            Dim blz As String = Mid(sIban, 5, 8)
            Dim konto As String = Mid(sIban, 13, 10)

            If getIban(blz, konto) = sIban Then Return True


            Return False

        End Function


    End Class

    Public Class IBAN_validieren
        Private IBAN As String

        '
        '  * CSharp IBAN validieren
        '  * Der Konstruktor erwartet die Übergabe der zu testenden IBAN
        '  

        Public Sub New(sIBAN As String)
            IBAN = sIBAN
        End Sub

        '
        '  * CSharp ISIBAN
        '  * Liefert True für korrekte IBAN, sonst false
        '  

        Public Function ISIBAN() As Boolean
            'Leerzeichen entfernen
            Dim mysIBAN As String = IBAN.Replace(" ", "")
            'Eine IBAN hat maximal 34 Stellen
            If mysIBAN.Length > 34 OrElse mysIBAN.Length < 5 Then
                Return False
            Else
                'IBAN ist zu lang oder viel zu lang
                Dim LaenderCode As String = mysIBAN.Substring(0, 2).ToUpper()
                Dim Pruefsumme As String = mysIBAN.Substring(2, 2).ToUpper()
                Dim BLZ_Konto As String = mysIBAN.Substring(4).ToUpper()

                If Not IsNumeric(Pruefsumme) Then
                    Return False
                End If
                'Prüfsumme ist nicht numerisch
                If Not ISLaendercode(LaenderCode) Then
                    Return False
                End If
                'Ländercode ist ungültig
                'Pruefsumme validieren
                Dim Umstellung As String = BLZ_Konto & LaenderCode & "00"
                Dim Modulus As String = IBANCleaner(Umstellung)
                If 98 - Modulo(Modulus, 97) <> Integer.Parse(Pruefsumme) Then
                    Return False
                    'Prüfsumme ist fehlerhaft 
                End If
            End If
            Return True
        End Function

        '
        '   * CSharp Ländercode
        '   * Test auf korrekten Ländercode nach ISO 3166-1
        '   

        Private Function ISLaendercode(code As String) As Boolean
            ' Der Code muss laut ISO 3166-1 ein 2-stelliger Ländercode aus Buchstaben sein.
            If code.Length <> 2 Then
                Return False
            Else
                code = code.ToUpper()
                Dim Laendercodes As String() = {"AA", "AB", "AC", "AD", "AE", "AF", _
                 "AG", "AH", "AI", "AJ", "AK", "AL", _
                 "AM", "AN", "AO", "AP", "AQ", "AR", _
                 "AS", "AT", "AU", "AV", "AW", "AX", _
                 "AY", "AZ", "BA", "BB", "BC", "BD", _
                 "BE", "BF", "BG", "BH", "BI", "BJ", _
                 "BK", "BL", "BM", "BN", "BO", "BP", _
                 "BQ", "BR", "BS", "BT", "BU", "BV", _
                 "BW", "BX", "BY", "BZ", "CA", "CB", _
                 "CC", "CD", "CE", "CF", "CG", "CH", _
                 "CI", "CJ", "CK", "CL", "CM", "CN", _
                 "CO", "CP", "CQ", "CR", "CS", "CT", _
                 "CU", "CV", "CW", "CX", "CY", "CZ", _
                 "DA", "DB", "DC", "DD", "DE", "DF", _
                 "DG", "DH", "DI", "DJ", "DK", "DL", _
                 "DM", "DN", "DO", "DP", "DQ", "DR", _
                 "DS", "DT", "DU", "DV", "DW", "DX", _
                 "DY", "DZ", "EA", "EB", "EC", "ED", _
                 "EE", "EF", "EG", "EH", "EI", "EJ", _
                 "EK", "EL", "EM", "EN", "EO", "EP", _
                 "EQ", "ER", "ES", "ET", "EU", "EV", _
                 "EW", "EX", "EY", "EZ", "FA", "FB", _
                 "FC", "FD", "FE", "FF", "FG", "FH", _
                 "FI", "FJ", "FK", "FL", "FM", "FN", _
                 "FO", "FP", "FQ", "FR", "FS", "FT", _
                 "FU", "FV", "FW", "FX", "FY", "FZ", _
                 "GA", "GB", "GC", "GD", "GE", "GF", _
                 "GG", "GH", "GI", "GJ", "GK", "GL", _
                 "GM", "GN", "GO", "GP", "GQ", "GR", _
                 "GS", "GT", "GU", "GV", "GW", "GX", _
                 "GY", "GZ", "HA", "HB", "HC", "HD", _
                 "HE", "HF", "HG", "HH", "HI", "HJ", _
                 "HK", "HL", "HM", "HN", "HO", "HP", _
                 "HQ", "HR", "HS", "HT", "HU", "HV", _
                 "HW", "HX", "HY", "HZ", "IA", "IB", _
                 "IC", "ID", "IE", "IF", "IG", "IH", _
                 "II", "IJ", "IK", "IL", "IM", "IN", _
                 "IO", "IP", "IQ", "IR", "IS", "IT", _
                 "IU", "IV", "IW", "IX", "IY", "IZ", _
                 "JA", "JB", "JC", "JD", "JE", "JF", _
                 "JG", "JH", "JI", "JJ", "JK", "JL", _
                 "JM", "JN", "JO", "JP", "JQ", "JR", _
                 "JS", "JT", "JU", "JV", "JW", "JX", _
                 "JY", "JZ", "KA", "KB", "KC", "KD", _
                 "KE", "KF", "KG", "KH", "KI", "KJ", _
                 "KK", "KL", "KM", "KN", "KO", "KP", _
                 "KQ", "KR", "KS", "KT", "KU", "KV", _
                 "KW", "KX", "KY", "KZ", "LA", "LB", _
                 "LC", "LD", "LE", "LF", "LG", "LH", _
                 "LI", "LJ", "LK", "LL", "LM", "LN", _
                 "LO", "LP", "LQ", "LR", "LS", "LT", _
                 "LU", "LV", "LW", "LX", "LY", "LZ", _
                 "MA", "MB", "MC", "MD", "ME", "MF", _
                 "MG", "MH", "MI", "MJ", "MK", "ML", _
                 "MM", "MN", "MO", "MP", "MQ", "MR", _
                 "MS", "MT", "MU", "MV", "MW", "MX", _
                 "MY", "MZ", "NA", "NB", "NC", "ND", _
                 "NE", "NF", "NG", "NH", "NI", "NJ", _
                 "NK", "NL", "NM", "NN", "NO", "NP", _
                 "NQ", "NR", "NS", "NT", "NU", "NV", _
                 "NW", "NX", "NY", "NZ", "OA", "OB", _
                 "OC", "OD", "OE", "OF", "OG", "OH", _
                 "OI", "OJ", "OK", "OL", "OM", "ON", _
                 "OO", "OP", "OQ", "OR", "OS", "OT", _
                 "OU", "OV", "OW", "OX", "OY", "OZ", _
                 "PA", "PB", "PC", "PD", "PE", "PF", _
                 "PG", "PH", "PI", "PJ", "PK", "PL", _
                 "PM", "PN", "PO", "PP", "PQ", "PR", _
                 "PS", "PT", "PU", "PV", "PW", "PX", _
                 "PY", "PZ", "QA", "QB", "QC", "QD", _
                 "QE", "QF", "QG", "QH", "QI", "QJ", _
                 "QK", "QL", "QM", "QN", "QO", "QP", _
                 "QQ", "QR", "QS", "QT", "QU", "QV", _
                 "QW", "QX", "QY", "QZ", "RA", "RB", _
                 "RC", "RD", "RE", "RF", "RG", "RH", _
                 "RI", "RJ", "RK", "RL", "RM", "RN", _
                 "RO", "RP", "RQ", "RR", "RS", "RT", _
                 "RU", "RV", "RW", "RX", "RY", "RZ", _
                 "SA", "SB", "SC", "SD", "SE", "SF", _
                 "SG", "SH", "SI", "SJ", "SK", "SL", _
                 "SM", "SN", "SO", "SP", "SQ", "SR", _
                 "SS", "ST", "SU", "SV", "SW", "SX", _
                 "SY", "SZ", "TA", "TB", "TC", "TD", _
                 "TE", "TF", "TG", "TH", "TI", "TJ", _
                 "TK", "TL", "TM", "TN", "TO", "TP", _
                 "TQ", "TR", "TS", "TT", "TU", "TV", _
                 "TW", "TX", "TY", "TZ", "UA", "UB", _
                 "UC", "UD", "UE", "UF", "UG", "UH", _
                 "UI", "UJ", "UK", "UL", "UM", "UN", _
                 "UO", "UP", "UQ", "UR", "US", "UT", _
                 "UU", "UV", "UW", "UX", "UY", "UZ", _
                 "VA", "VB", "VC", "VD", "VE", "VF", _
                 "VG", "VH", "VI", "VJ", "VK", "VL", _
                 "VM", "VN", "VO", "VP", "VQ", "VR", _
                 "VS", "VT", "VU", "VV", "VW", "VX", _
                 "VY", "VZ", "WA", "WB", "WC", "WD", _
                 "WE", "WF", "WG", "WH", "WI", "WJ", _
                 "WK", "WL", "WM", "WN", "WO", "WP", _
                 "WQ", "WR", "WS", "WT", "WU", "WV", _
                 "WW", "WX", "WY", "WZ", "XA", "XB", _
                 "XC", "XD", "XE", "XF", "XG", "XH", _
                 "XI", "XJ", "XK", "XL", "XM", "XN", _
                 "XO", "XP", "XQ", "XR", "XS", "XT", _
                 "XU", "XV", "XW", "XX", "XY", "XZ", _
                 "YA", "YB", "YC", "YD", "YE", "YF", _
                 "YG", "YH", "YI", "YJ", "YK", "YL", _
                 "YM", "YN", "YO", "YP", "YQ", "YR", _
                 "YS", "YT", "YU", "YV", "YW", "YX", _
                 "YY", "YZ", "ZA", "ZB", "ZC", "ZD", _
                 "ZE", "ZF", "ZG", "ZH", "ZI", "ZJ", _
                 "ZK", "ZL", "ZM", "ZN", "ZO", "ZP", _
                 "ZQ", "ZR", "ZS", "ZT", "ZU", "ZV", _
                 "ZW", "ZX", "ZY", "ZZ"}
                If Array.IndexOf(Laendercodes, code) = -1 Then
                    Return False
                Else
                    Return True
                End If
            End If
        End Function

        '
        '   * CSharp IBAN Cleaner
        '   * Buchstaben duch Zahlen ersetzen
        '   

        Private Function IBANCleaner(sIBAN As String) As String
            For x As Integer = 65 To 90
                Dim replacewith As Integer = x - 64 + 9
                Dim replace As String = (ChrW(x)).ToString()
                sIBAN = sIBAN.Replace(replace, replacewith.ToString())
            Next
            Return sIBAN
        End Function

        '
        '   * CSharp Modulo
        '   * Es war mir bei diesen großen Zahlen mit C# nicht möglich anderes 
        '   * an eine Mod Ergebnis zu kommen.
        '   

        Private Function Modulo(sModulus As String, iTeiler As Integer) As Integer
            Dim iStart As Integer, iEnde As Integer, iErgebniss As Integer, iRestTmp As Integer, iBuffer As Integer
            Dim iRest As String = "", sErg As String = ""

            iStart = 0
            iEnde = 0

            While iEnde <= sModulus.Length - 1
                iBuffer = Integer.Parse(iRest & sModulus.Substring(iStart, iEnde - iStart + 1))

                If iBuffer >= iTeiler Then
                    iErgebniss = iBuffer \ iTeiler
                    iRestTmp = iBuffer - iErgebniss * iTeiler
                    iRest = iRestTmp.ToString()

                    sErg = sErg & iErgebniss.ToString()

                    iStart = iEnde + 1
                    iEnde = iStart
                Else
                    If sErg <> "" Then
                        sErg = sErg & "0"
                    End If

                    iEnde = iEnde + 1
                End If
            End While

            If iStart <= sModulus.Length Then
                iRest = iRest & sModulus.Substring(iStart)
            End If

            Return Integer.Parse(iRest)
        End Function

        '
        '   * Csharp ISNUMERIC
        '   * Da C# ISNUMERIC als Methode nicht kennt müssen wir selbst ran.
        '   

        Private Function IsNumeric(value As String) As Boolean
            Try
                Integer.Parse(value)
                Return (True)
            Catch
                Return (False)
            End Try
        End Function



    End Class

    Public Class BICvalidieren

        Private _bic As String

        Private _errormsg As String = String.Empty
        Public ReadOnly Property Errormsg As String
            Get
                Return _errormsg
            End Get
        End Property
        Public Function IsBic() As Boolean
            If IsNothing(_bic) OrElse Trim(_bic) = "" Then
                _errormsg = "Fehlende BIC"
                Return False
            End If


            Dim regex As Regex = New Regex("^[A-Z]{6}[A-Z0-9]{2}([A-Z0-9]{3})?$")
            Dim match As Match = regex.Match(_bic)
            If match.Success Then
                Return True
            Else
                _errormsg = "BIC Format ist falsch"
                Return False
            End If

            Return False
        End Function

        Public Sub New(ByVal strbic As String)
            _bic = strbic
        End Sub


    End Class


End Namespace

