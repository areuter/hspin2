﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Windows.Input
Imports System.Windows.Data
Imports System.ComponentModel


Namespace ViewModel



    Public Class DocManViewModel
        Inherits ViewModelBase

        Public Event RequestClose As EventHandler



        Private _SelectedFilter As Integer
        Public Property SelectedFilter As Integer
            Get
                Return _SelectedFilter
            End Get
            Set(value As Integer)
                _SelectedFilter = value
                RaisePropertyChanged("SelectedFilter")
                If Not IsNothing(LV) Then
                    LV.Refresh()
                End If
            End Set
        End Property


        Private _SelectedItem As Guid
        Public Property SelectedItem As Guid
            Get
                Return _SelectedItem
            End Get
            Set(value As Guid)
                _SelectedItem = value
                RaisePropertyChanged("SelectedItem")
            End Set
        End Property

        Private _Doctype As Integer
        Public Property Doctype As Integer
            Get
                Return _Doctype
            End Get
            Set(value As Integer)
                _Doctype = value
                RaisePropertyChanged("Doctype")
            End Set
        End Property

        Private _Doctypes As New Dictionary(Of Integer, String)
        Public ReadOnly Property Doctypes As Dictionary(Of Integer, String)
            Get
                Return _Doctypes
            End Get
        End Property

        Private _SelectedUpload As String
        Public Property SelectedUpload As String
            Get
                Return _SelectedUpload
            End Get
            Set(value As String)
                _SelectedUpload = value
                RaisePropertyChanged("SelectedUpload")
            End Set
        End Property

        Private _LV As ListCollectionView
        Public Property LV As ListCollectionView
            Get
                Return _LV
            End Get
            Set(value As ListCollectionView)
                _LV = value
                RaisePropertyChanged("LV")
            End Set
        End Property


        Private WithEvents bgw As New BackgroundWorker


        Private _context As in1Entities

        Public Property UploadCommand As ICommand
        Public Property SelectUploadCommand As ICommand
        Public Property SelectCommand As ICommand
        Public Property PreviewCommand As ICommand
        Public Property CancelCommand As ICommand
        Public Property DelCommand As ICommand




        Public Sub New()
            _context = New in1Entities


            SelectedFilter = 3



            Dim dt As Array
            dt = System.Enum.GetValues(GetType(Document.Doctype))

            _Doctypes.Add(0, "Bitte auswählen")
            For i = 2 To dt.Length - 1
                _Doctypes.Add(i, dt(i).ToString)
            Next
            RaisePropertyChanged("Doctypes")
            Doctype = 0

            UploadCommand = New RelayCommand(AddressOf Upload, AddressOf CanUpload)

            SelectUploadCommand = New RelayCommand(AddressOf SelectUpload)
            PreviewCommand = New RelayCommand(AddressOf Preview)
            SelectCommand = New RelayCommand(AddressOf SelectAction, AddressOf CanSelectorPreview)
            CancelCommand = New RelayCommand(AddressOf CancelAction, AddressOf CanSelectorPreview)
            DelCommand = New RelayCommand(AddressOf DeleteAction, AddressOf CanSelectorPreview)



            Load()

        End Sub

        Private Sub Load()

            bgw.WorkerReportsProgress = False
            bgw.WorkerSupportsCancellation = False
            If Not bgw.IsBusy Then
                bgw.RunWorkerAsync()
            End If
        End Sub

        Private Sub SelectAction()
            RaiseEvent RequestClose(Me, Nothing)
        End Sub

        Private Function CanSelectorPreview() As Boolean
            If Not IsNothing(SelectedItem) Then
                Return True
            End If
            Return False
        End Function

        Private Sub DeleteAction()
            Dim dlg As New DialogService
            If dlg.AskCancelConfirmation("Löschen", "Soll die Datei wirklich gelöscht werden") = System.Windows.MessageBoxResult.Yes Then
                If Not IsNothing(SelectedItem) Then

                    Dim dok = (From c In _context.Documents Where c.DocumentUID = SelectedItem).SingleOrDefault

                    If Not IsNothing(dok) Then
                        _context.Documents.Remove(dok)
                        _context.SaveChanges()
                        Load()
                    End If

                End If
            End If
        End Sub

        Private Sub CancelAction()
            SelectedItem = Nothing
            RaiseEvent RequestClose(Me, Nothing)
        End Sub

        Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw.DoWork
            Using con As New in1Entities
                Dim query = From c In _context.Documents Where c.DocumentType >= 2 Order By c.Dateiname Select c.DocumentUID, c.Dateiname, c.Extension, c.Erstelldatum, c.DocumentType

                e.Result = query.ToList
            End Using
        End Sub

        Private Sub bgw_RunWorkerCompleted(ByVal sender As Object, ByVal es As RunWorkerCompletedEventArgs) Handles bgw.RunWorkerCompleted
            LV = CType(CollectionViewSource.GetDefaultView(es.Result), ListCollectionView)


            If Not IsNothing(LV) Then
                LV.Filter = New Predicate(Of Object)(AddressOf Filter)
            End If
            LV.Refresh()
        End Sub


        Private Function Filter(ByVal itm As Object) As Boolean
            Dim itmtype = CType(itm.DocumentType, Integer)

            If itmtype = SelectedFilter Then Return True



            Return False
        End Function

        Private Sub Preview()
            If Not IsNothing(SelectedItem) Then
                Using con As New in1Entities
                    Dim doc = (From c In con.Documents Where c.DocumentUID = SelectedItem Select c).First

                    If Not IsNothing(doc) AndAlso Not IsNothing(doc.DocumentData) Then

                        Dokviewer.ShowFilestream(doc)
                    End If


                End Using

            End If
        End Sub

        Private Sub Upload()


            If System.IO.File.Exists(SelectedUpload) Then
                Dim doc As New Document("", Now, SelectedUpload, Doctype)
                _context.Documents.Add(doc)
                _context.SaveChanges()
                SelectedUpload = ""
                Doctype = 0
                Load()
            End If

        End Sub

        Private Sub SelectUpload()
            Dim dlg As New DialogService
            Dim fn = dlg.GetFile("*.*")
            If Not IsNothing(fn) Then
                SelectedUpload = fn
            End If
        End Sub

        Private Function CanUpload()

            If Not IsNothing(SelectedUpload) AndAlso Not Trim(SelectedUpload) = "" AndAlso Not IsNothing(Doctype) AndAlso Not Doctype = 0 Then Return True
            Return False

        End Function
    End Class
End Namespace