﻿
' CapStructures v1.1
'
' This software is released into the public domain.  You are free to use it
' in any way you like, except that you may not sell this source code.
'
' This software is provided "as is" with no expressed or implied warranty.
' I accept no liability for any damage or loss of business that this software
' may cause.
' 
' This source code is originally written by Tamir Khason (see http://blogs.microsoft.co.il/blogs/tamir
' or http://www.codeplex.com/wpfcap).
' 
' Modifications are made by Geert van Horrik (CatenaLogic, see http://blog.catenalogic.com) 


Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.InteropServices

Namespace WpfCap
    <ComVisible(False)> _
    Friend Enum PinDirection
        Input
        Output
    End Enum

    <ComVisible(False), StructLayout(LayoutKind.Sequential)> _
    Friend Class AMMediaType
        Implements IDisposable
        Public MajorType As Guid

        Public SubType As Guid

        <MarshalAs(UnmanagedType.Bool)> _
        Public FixedSizeSamples As Boolean = True

        <MarshalAs(UnmanagedType.Bool)> _
        Public TemporalCompression As Boolean

        Public SampleSize As Integer = 1

        Public FormatType As Guid

        Public unkPtr As IntPtr

        Public FormatSize As Integer

        Public FormatPtr As IntPtr

        Protected Overrides Sub Finalize()
            Try
                Dispose(False)
            Finally
                MyBase.Finalize()
            End Try
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            ' remove me from the Finalization queue 
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overridable Sub Dispose(disposing As Boolean)
            If FormatSize <> 0 Then
                Marshal.FreeCoTaskMem(FormatPtr)
            End If
            If unkPtr <> IntPtr.Zero Then
                Marshal.Release(unkPtr)
            End If
        End Sub
    End Class

    <ComVisible(False), StructLayout(LayoutKind.Sequential, Pack:=1, CharSet:=CharSet.Unicode)> _
    Friend Class PinInfo
        Public Filter As IBaseFilter

        Public Direction As PinDirection

        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=128)> _
        Public Name As String
    End Class

    <ComVisible(False), StructLayout(LayoutKind.Sequential)> _
    Friend Structure VideoInfoHeader
        Public SrcRect As RECT

        Public TargetRect As RECT

        Public BitRate As Integer

        Public BitErrorRate As Integer

        Public AverageTimePerFrame As Long

        Public BmiHeader As BitmapInfoHeader
    End Structure

    <ComVisible(False), StructLayout(LayoutKind.Sequential, Pack:=2)> _
    Friend Structure BitmapInfoHeader
        Public Size As Integer

        Public Width As Integer

        Public Height As Integer

        Public Planes As Short

        Public BitCount As Short

        Public Compression As Integer

        Public ImageSize As Integer

        Public XPelsPerMeter As Integer

        Public YPelsPerMeter As Integer

        Public ColorsUsed As Integer

        Public ColorsImportant As Integer
    End Structure

    <ComVisible(False), StructLayout(LayoutKind.Sequential)> _
    Friend Structure RECT
        Public Left As Integer

        Public Top As Integer

        Public Right As Integer

        Public Bottom As Integer
    End Structure
End Namespace
