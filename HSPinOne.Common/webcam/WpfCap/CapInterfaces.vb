﻿
' CapInterfaces v1.1
'
' This software is released into the public domain.  You are free to use it
' in any way you like, except that you may not sell this source code.
'
' This software is provided "as is" with no expressed or implied warranty.
' I accept no liability for any damage or loss of business that this software
' may cause.
' 
' This source code is originally written by Tamir Khason (see http://blogs.microsoft.co.il/blogs/tamir
' or http://www.codeplex.com/wpfcap).
' 
' Modifications are made by Geert van Horrik (CatenaLogic, see http://blog.catenalogic.com) 
'


Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.InteropServices
Imports System.Runtime.InteropServices.ComTypes

Namespace WpfCap
    <ComImport(), Guid("56A868A9-0AD4-11CE-B03A-0020AF0BA770"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)> _
    Friend Interface IGraphBuilder
        <PreserveSig()> _
        Function AddFilter(<[In]()> filter As IBaseFilter, <[In](), MarshalAs(UnmanagedType.LPWStr)> name As String) As Integer

        <PreserveSig()> _
        Function RemoveFilter(<[In]()> filter As IBaseFilter) As Integer

        <PreserveSig()> _
        Function EnumFilters(<Out()> ByRef enumerator As IntPtr) As Integer

        <PreserveSig()> _
        Function FindFilterByName(<[In](), MarshalAs(UnmanagedType.LPWStr)> name As String, <Out()> ByRef filter As IBaseFilter) As Integer

        <PreserveSig()> _
        Function ConnectDirect(<[In]()> pinOut As IPin, <[In]()> pinIn As IPin, <[In](), MarshalAs(UnmanagedType.LPStruct)> mediaType As AMMediaType) As Integer

        <PreserveSig()> _
        Function Reconnect(<[In]()> pin As IPin) As Integer

        <PreserveSig()> _
        Function Disconnect(<[In]()> pin As IPin) As Integer

        <PreserveSig()> _
        Function SetDefaultSyncSource() As Integer

        <PreserveSig()> _
        Function Connect(<[In]()> pinOut As IPin, <[In]()> pinIn As IPin) As Integer

        <PreserveSig()> _
        Function Render(<[In]()> pinOut As IPin) As Integer

        <PreserveSig()> _
        Function RenderFile(<[In](), MarshalAs(UnmanagedType.LPWStr)> file As String, <[In](), MarshalAs(UnmanagedType.LPWStr)> playList As String) As Integer

        <PreserveSig()> _
        Function AddSourceFilter(<[In](), MarshalAs(UnmanagedType.LPWStr)> fileName As String, <[In](), MarshalAs(UnmanagedType.LPWStr)> filterName As String, <Out()> ByRef filter As IBaseFilter) As Integer

        <PreserveSig()> _
        Function SetLogFile(hFile As IntPtr) As Integer

        <PreserveSig()> _
        Function Abort() As Integer

        <PreserveSig()> _
        Function ShouldOperationContinue() As Integer
    End Interface

    <ComImport(), Guid("56A86895-0AD4-11CE-B03A-0020AF0BA770"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)> _
    Friend Interface IBaseFilter
        <PreserveSig()> _
        Function GetClassID(<Out()> ByRef ClassID As Guid) As Integer

        <PreserveSig()> _
        Function [Stop]() As Integer

        <PreserveSig()> _
        Function Pause() As Integer

        <PreserveSig()> _
        Function Run(start As Long) As Integer

        <PreserveSig()> _
        Function GetState(milliSecsTimeout As Integer, <Out()> ByRef filterState As Integer) As Integer

        <PreserveSig()> _
        Function SetSyncSource(<[In]()> clock As IntPtr) As Integer

        <PreserveSig()> _
        Function GetSyncSource(<Out()> ByRef clock As IntPtr) As Integer

        <PreserveSig()> _
        Function EnumPins(<Out()> ByRef enumPins__1 As IEnumPins) As Integer

        <PreserveSig()> _
        Function FindPin(<[In](), MarshalAs(UnmanagedType.LPWStr)> id As String, <Out()> ByRef pin As IPin) As Integer

        <PreserveSig()> _
        Function QueryFilterInfo(<Out()> filterInfo As FilterInfo) As Integer

        <PreserveSig()> _
        Function JoinFilterGraph(<[In]()> graph As IFilterGraph, <[In](), MarshalAs(UnmanagedType.LPWStr)> name As String) As Integer

        <PreserveSig()> _
        Function QueryVendorInfo(<Out(), MarshalAs(UnmanagedType.LPWStr)> ByRef vendorInfo As String) As Integer
    End Interface

    <ComImport(), Guid("56A86891-0AD4-11CE-B03A-0020AF0BA770"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)> _
    Friend Interface IPin
        <PreserveSig()> _
        Function Connect(<[In]()> receivePin As IPin, <[In](), MarshalAs(UnmanagedType.LPStruct)> mediaType As AMMediaType) As Integer

        <PreserveSig()> _
        Function ReceiveConnection(<[In]()> receivePin As IPin, <[In](), MarshalAs(UnmanagedType.LPStruct)> mediaType As AMMediaType) As Integer

        <PreserveSig()> _
        Function Disconnect() As Integer

        <PreserveSig()> _
        Function ConnectedTo(<Out()> ByRef pin As IPin) As Integer

        <PreserveSig()> _
        Function ConnectionMediaType(<Out(), MarshalAs(UnmanagedType.LPStruct)> mediaType As AMMediaType) As Integer

        <PreserveSig()> _
        Function QueryPinInfo(<Out(), MarshalAs(UnmanagedType.LPStruct)> pinInfo As PinInfo) As Integer

        <PreserveSig()> _
        Function QueryDirection(ByRef pinDirection As PinDirection) As Integer

        <PreserveSig()> _
        Function QueryId(<Out(), MarshalAs(UnmanagedType.LPWStr)> ByRef id As String) As Integer

        <PreserveSig()> _
        Function QueryAccept(<[In](), MarshalAs(UnmanagedType.LPStruct)> mediaType As AMMediaType) As Integer

        <PreserveSig()> _
        Function EnumMediaTypes(enumerator As IntPtr) As Integer

        <PreserveSig()> _
        Function QueryInternalConnections(apPin As IntPtr, <[In](), Out()> ByRef nPin As Integer) As Integer

        <PreserveSig()> _
        Function EndOfStream() As Integer

        <PreserveSig()> _
        Function BeginFlush() As Integer

        <PreserveSig()> _
        Function EndFlush() As Integer

        <PreserveSig()> _
        Function NewSegment(start As Long, [stop] As Long, rate As Double) As Integer
    End Interface

    <ComImport(), Guid("56A86892-0AD4-11CE-B03A-0020AF0BA770"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)> _
    Friend Interface IEnumPins
        <PreserveSig()> _
        Function [Next](<[In]()> cPins As Integer, <Out(), MarshalAs(UnmanagedType.LPArray, SizeParamIndex:=0)> pins As IPin(), <Out()> ByRef pinsFetched As Integer) As Integer

        <PreserveSig()> _
        Function Skip(<[In]()> cPins As Integer) As Integer

        <PreserveSig()> _
        Function Reset() As Integer

        <PreserveSig()> _
        Function Clone(<Out()> ByRef enumPins As IEnumPins) As Integer
    End Interface

    <ComImport(), Guid("56A8689F-0AD4-11CE-B03A-0020AF0BA770"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)> _
    Friend Interface IFilterGraph
        <PreserveSig()> _
        Function AddFilter(<[In]()> filter As IBaseFilter, <[In](), MarshalAs(UnmanagedType.LPWStr)> name As String) As Integer

        <PreserveSig()> _
        Function RemoveFilter(<[In]()> filter As IBaseFilter) As Integer

        <PreserveSig()> _
        Function EnumFilters(<Out()> ByRef enumerator As IntPtr) As Integer

        <PreserveSig()> _
        Function FindFilterByName(<[In](), MarshalAs(UnmanagedType.LPWStr)> name As String, <Out()> ByRef filter As IBaseFilter) As Integer

        <PreserveSig()> _
        Function ConnectDirect(<[In]()> pinOut As IPin, <[In]()> pinIn As IPin, <[In](), MarshalAs(UnmanagedType.LPStruct)> mediaType As AMMediaType) As Integer

        <PreserveSig()> _
        Function Reconnect(<[In]()> pin As IPin) As Integer

        <PreserveSig()> _
        Function Disconnect(<[In]()> pin As IPin) As Integer

        <PreserveSig()> _
        Function SetDefaultSyncSource() As Integer
    End Interface

    <ComImport(), Guid("55272A00-42CB-11CE-8135-00AA004BB851"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)> _
    Friend Interface IPropertyBag
        <PreserveSig()> _
        Function Read(<[In](), MarshalAs(UnmanagedType.LPWStr)> propertyName As String, <[In](), Out(), MarshalAs(UnmanagedType.Struct)> ByRef pVar As Object, <[In]()> pErrorLog As IntPtr) As Integer

        <PreserveSig()> _
        Function Write(<[In](), MarshalAs(UnmanagedType.LPWStr)> propertyName As String, <[In](), MarshalAs(UnmanagedType.Struct)> ByRef pVar As Object) As Integer
    End Interface

    <ComImport(), Guid("6B652FFF-11FE-4FCE-92AD-0266B5D7C78F"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)> _
    Friend Interface ISampleGrabber
        <PreserveSig()> _
        Function SetOneShot(<[In](), MarshalAs(UnmanagedType.Bool)> oneShot As Boolean) As Integer

        <PreserveSig()> _
        Function SetMediaType(<[In](), MarshalAs(UnmanagedType.LPStruct)> mediaType As AMMediaType) As Integer

        <PreserveSig()> _
        Function GetConnectedMediaType(<Out(), MarshalAs(UnmanagedType.LPStruct)> mediaType As AMMediaType) As Integer

        <PreserveSig()> _
        Function SetBufferSamples(<[In](), MarshalAs(UnmanagedType.Bool)> bufferThem As Boolean) As Integer

        <PreserveSig()> _
        Function GetCurrentBuffer(ByRef bufferSize As Integer, buffer As IntPtr) As Integer

        <PreserveSig()> _
        Function GetCurrentSample(sample As IntPtr) As Integer

        <PreserveSig()> _
        Function SetCallback(callback As ISampleGrabberCB, whichMethodToCallback As Integer) As Integer
    End Interface

    <ComImport(), Guid("0579154A-2B53-4994-B0D0-E773148EFF85"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)> _
    Friend Interface ISampleGrabberCB
        <PreserveSig()> _
        Function SampleCB(sampleTime As Double, sample As IntPtr) As Integer

        <PreserveSig()> _
        Function BufferCB(sampleTime As Double, buffer As IntPtr, bufferLen As Integer) As Integer
    End Interface

    <ComImport(), Guid("29840822-5B84-11D0-BD3B-00A0C911CE86"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)> _
    Friend Interface ICreateDevEnum
        <PreserveSig()> _
        Function CreateClassEnumerator(<[In]()> ByRef type As Guid, <Out()> ByRef enumMoniker As IEnumMoniker, <[In]()> flags As Integer) As Integer
    End Interface

    <ComImport(), Guid("56A868B4-0AD4-11CE-B03A-0020AF0BA770"), InterfaceType(ComInterfaceType.InterfaceIsDual)> _
    Friend Interface IVideoWindow
        <PreserveSig()> _
        Function put_Caption(caption As String) As Integer

        <PreserveSig()> _
        Function get_Caption(<Out()> ByRef caption As String) As Integer

        <PreserveSig()> _
        Function put_WindowStyle(windowStyle As Integer) As Integer

        <PreserveSig()> _
        Function get_WindowStyle(ByRef windowStyle As Integer) As Integer

        <PreserveSig()> _
        Function put_WindowStyleEx(windowStyleEx As Integer) As Integer

        <PreserveSig()> _
        Function get_WindowStyleEx(ByRef windowStyleEx As Integer) As Integer

        <PreserveSig()> _
        Function put_AutoShow(<[In](), MarshalAs(UnmanagedType.Bool)> autoShow As Boolean) As Integer

        <PreserveSig()> _
        Function get_AutoShow(<Out(), MarshalAs(UnmanagedType.Bool)> ByRef autoShow As Boolean) As Integer

        <PreserveSig()> _
        Function put_WindowState(windowState As Integer) As Integer

        <PreserveSig()> _
        Function get_WindowState(ByRef windowState As Integer) As Integer

        <PreserveSig()> _
        Function put_BackgroundPalette(<[In](), MarshalAs(UnmanagedType.Bool)> backgroundPalette As Boolean) As Integer

        <PreserveSig()> _
        Function get_BackgroundPalette(<Out(), MarshalAs(UnmanagedType.Bool)> ByRef backgroundPalette As Boolean) As Integer

        <PreserveSig()> _
        Function put_Visible(<[In](), MarshalAs(UnmanagedType.Bool)> visible As Boolean) As Integer

        <PreserveSig()> _
        Function get_Visible(<Out(), MarshalAs(UnmanagedType.Bool)> ByRef visible As Boolean) As Integer

        <PreserveSig()> _
        Function put_Left(left As Integer) As Integer

        <PreserveSig()> _
        Function get_Left(ByRef left As Integer) As Integer

        <PreserveSig()> _
        Function put_Width(width As Integer) As Integer

        <PreserveSig()> _
        Function get_Width(ByRef width As Integer) As Integer

        <PreserveSig()> _
        Function put_Top(top As Integer) As Integer

        <PreserveSig()> _
        Function get_Top(ByRef top As Integer) As Integer

        <PreserveSig()> _
        Function put_Height(height As Integer) As Integer

        <PreserveSig()> _
        Function get_Height(ByRef height As Integer) As Integer

        <PreserveSig()> _
        Function put_Owner(owner As IntPtr) As Integer

        <PreserveSig()> _
        Function get_Owner(ByRef owner As IntPtr) As Integer

        <PreserveSig()> _
        Function put_MessageDrain(drain As IntPtr) As Integer

        <PreserveSig()> _
        Function get_MessageDrain(ByRef drain As IntPtr) As Integer

        <PreserveSig()> _
        Function get_BorderColor(ByRef color As Integer) As Integer

        <PreserveSig()> _
        Function put_BorderColor(color As Integer) As Integer

        <PreserveSig()> _
        Function get_FullScreenMode(<Out(), MarshalAs(UnmanagedType.Bool)> ByRef fullScreenMode As Boolean) As Integer

        <PreserveSig()> _
        Function put_FullScreenMode(<[In](), MarshalAs(UnmanagedType.Bool)> fullScreenMode As Boolean) As Integer

        <PreserveSig()> _
        Function SetWindowForeground(focus As Integer) As Integer

        <PreserveSig()> _
        Function NotifyOwnerMessage(hwnd As IntPtr, msg As Integer, wParam As IntPtr, lParam As IntPtr) As Integer

        <PreserveSig()> _
        Function SetWindowPosition(left As Integer, top As Integer, width As Integer, height As Integer) As Integer

        <PreserveSig()> _
        Function GetWindowPosition(ByRef left As Integer, ByRef top As Integer, ByRef width As Integer, ByRef height As Integer) As Integer

        <PreserveSig()> _
        Function GetMinIdealImageSize(ByRef width As Integer, ByRef height As Integer) As Integer

        <PreserveSig()> _
        Function GetMaxIdealImageSize(ByRef width As Integer, ByRef height As Integer) As Integer

        <PreserveSig()> _
        Function GetRestorePosition(ByRef left As Integer, ByRef top As Integer, ByRef width As Integer, ByRef height As Integer) As Integer

        <PreserveSig()> _
        Function HideCursor(<[In](), MarshalAs(UnmanagedType.Bool)> hideCursor__1 As Boolean) As Integer

        <PreserveSig()> _
        Function IsCursorHidden(<Out(), MarshalAs(UnmanagedType.Bool)> ByRef hideCursor As Boolean) As Integer
    End Interface

    <ComImport(), Guid("56A868B1-0AD4-11CE-B03A-0020AF0BA770"), InterfaceType(ComInterfaceType.InterfaceIsDual)> _
    Friend Interface IMediaControl
        <PreserveSig()> _
        Function Run() As Integer

        <PreserveSig()> _
        Function Pause() As Integer

        <PreserveSig()> _
        Function [Stop]() As Integer

        <PreserveSig()> _
        Function GetState(timeout As Integer, ByRef filterState As Integer) As Integer

        <PreserveSig()> _
        Function RenderFile(fileName As String) As Integer

        <PreserveSig()> _
        Function AddSourceFilter(<[In]()> fileName As String, <Out(), MarshalAs(UnmanagedType.IDispatch)> ByRef filterInfo As Object) As Integer

        <PreserveSig()> _
        Function get_FilterCollection(<Out(), MarshalAs(UnmanagedType.IDispatch)> ByRef collection As Object) As Integer

        <PreserveSig()> _
        Function get_RegFilterCollection(<Out(), MarshalAs(UnmanagedType.IDispatch)> ByRef collection As Object) As Integer

        <PreserveSig()> _
        Function StopWhenReady() As Integer
    End Interface
End Namespace
