﻿
' CapPlayer v1.1
'
' This software is released into the public domain.  You are free to use it
' in any way you like, except that you may not sell this source code.
'
' This software is provided "as is" with no expressed or implied warranty.
' I accept no liability for any damage or loss of business that this software
' may cause.
' 
' This source code is originally written by Tamir Khason (see http://blogs.microsoft.co.il/blogs/tamir
' or http://www.codeplex.com/wpfcap).
' 
' Modifications are made by Geert van Horrik (CatenaLogic, see http://blog.catenalogic.com) 
'


Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Windows.Interop
Imports System.Windows.Media.Imaging
Imports System.Windows.Media
Imports System.Runtime.InteropServices
Imports System.Windows.Media.Animation
Imports System.Windows.Controls
Imports System.Windows
Imports System.Windows.Data
Imports System.Diagnostics

Namespace WpfCap
    Public Class CapPlayer
        Inherits Image
        Implements IDisposable
#Region "Variables"
#End Region

#Region "Constructor & destructor"
        Public Sub New()
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            ' Check whether we have a valid device
            If Device IsNot Nothing Then
                ' Yes, dispose it
                Device.Dispose()

                ' Clear device
                Device = Nothing
            End If
        End Sub
#End Region

#Region "Properties"
        ''' <summary>
        ''' Wrapper for the Device dependency property
        ''' </summary>
        Public Property Device() As CapDevice
            Get
                Return DirectCast(GetValue(DeviceProperty), CapDevice)
            End Get
            Set(value As CapDevice)
                SetValue(DeviceProperty, value)
            End Set
        End Property

        ' Using a DependencyProperty as the backing store for Device.  This enables animation, styling, binding, etc...
        Public Shared ReadOnly DeviceProperty As DependencyProperty = DependencyProperty.Register("Device", GetType(CapDevice), GetType(CapPlayer), New UIPropertyMetadata(Nothing, New PropertyChangedCallback(AddressOf DeviceProperty_Changed)))

        ''' <summary>
        ''' Wrapper for the Rotation dependency property
        ''' </summary>
        Public Property Rotation() As Double
            Get
                Return CDbl(GetValue(RotationProperty))
            End Get
            Set(value As Double)
                SetValue(RotationProperty, value)
            End Set
        End Property

        ' Using a DependencyProperty as the backing store for Rotation.  This enables animation, styling, binding, etc...
        Public Shared ReadOnly RotationProperty As DependencyProperty = DependencyProperty.Register("Rotation", GetType(Double), GetType(CapPlayer), New UIPropertyMetadata(0.0, New PropertyChangedCallback(AddressOf RotationProperty_Changed)))

        ''' <summary>
        ''' Wrapper for the framerate dependency property
        ''' </summary>
        Public Property Framerate() As Single
            Get
                Return CSng(GetValue(FramerateProperty))
            End Get
            Set(value As Single)
                SetValue(FramerateProperty, value)
            End Set
        End Property

        Public Shared ReadOnly FramerateProperty As DependencyProperty = DependencyProperty.Register("Framerate", GetType(Single), GetType(CapPlayer), New UIPropertyMetadata(CSng(0)))

        ''' <summary>
        ''' Gets the current bitmap
        ''' </summary>
        Public ReadOnly Property CurrentBitmap() As BitmapSource
            Get
                ' Return right value
                Return If((Device IsNot Nothing), New TransformedBitmap(Device.BitmapSource.Clone(), New RotateTransform(Rotation)), Nothing)
            End Get
        End Property
#End Region

#Region "Methods"
        Private Shared Sub DeviceProperty_Changed(sender As DependencyObject, e As DependencyPropertyChangedEventArgs)
            ' Get the sender
            Dim typedSender As CapPlayer = TryCast(sender, CapPlayer)
            If (typedSender IsNot Nothing) AndAlso (e.NewValue IsNot Nothing) Then
                ' Make sure that we are not in design mode
                If System.ComponentModel.DesignerProperties.GetIsInDesignMode(typedSender) Then
                    Return
                End If

                ' Unsubscribe from previous device
                Dim oldDevice As CapDevice = TryCast(e.OldValue, CapDevice)
                If oldDevice IsNot Nothing Then
                    ' Clean up
                    typedSender.CleanUpDevice(oldDevice)
                End If

                ' Subscribe to new one
                Dim newDevice As CapDevice = TryCast(e.NewValue, CapDevice)
                If newDevice IsNot Nothing Then
                    ' Subscribe
                    AddHandler newDevice.NewBitmapReady, AddressOf typedSender.device_OnNewBitmapReady
                End If
            End If
        End Sub

        Private Shared Sub RotationProperty_Changed(sender As DependencyObject, e As DependencyPropertyChangedEventArgs)
            ' Get the sender
            Dim typedSender As CapPlayer = TryCast(sender, CapPlayer)
            If typedSender IsNot Nothing Then
                ' Rotate
                typedSender.LayoutTransform = New RotateTransform(CDbl(e.NewValue))
            End If
        End Sub

        ''' <summary>
        ''' Cleans up a specific device
        ''' </summary>
        ''' <param name="device">Device to clean up</param>
        Private Sub CleanUpDevice(device As CapDevice)
            ' Check if there even is a device
            If device Is Nothing Then
                Return
            End If

            ' Stop
            device.StopCam()

            ' Unsubscribe
            RemoveHandler device.NewBitmapReady, AddressOf device_OnNewBitmapReady
        End Sub

        ''' <summary>
        ''' Invoked when a new bitmap is ready
        ''' </summary>
        ''' <param name="sender">Sender</param>
        ''' <param name="e">EventArgs</param>
        Private Sub device_OnNewBitmapReady(sender As Object, e As EventArgs)
            ' Create new binding for the framerate
            Dim b As New Binding()
            b.Source = Device
            b.Path = New PropertyPath(CapDevice.FramerateProperty)
            SetBinding(CapPlayer.FramerateProperty, b)

            ' Get the sender
            Dim typedSender As CapDevice = TryCast(sender, CapDevice)
            If typedSender IsNot Nothing Then
                ' Set the source of the image
                Source = typedSender.BitmapSource
            End If
        End Sub
#End Region

 
    End Class
End Namespace
