﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading
Imports System.Runtime.InteropServices
Imports System.Runtime.InteropServices.ComTypes
Imports System.Windows.Media
Imports System.Windows.Interop
Imports System.Windows

Namespace WpfCap
    Public Class CapDevice
        Inherits DependencyObject
        Implements IDisposable
#Region "Win32"
        Shared ReadOnly FilterGraph As New Guid(&HE436EBB3UI, &H524F, &H11CE, &H9F, &H53, &H0, _
         &H20, &HAF, &HB, &HA7, &H70)

        Shared ReadOnly SampleGrabber As New Guid(&HC1F400A0UI, &H3F08, &H11D3, &H9F, &HB, &H0, _
         &H60, &H8, &H3, &H9E, &H37)

        Public Shared ReadOnly SystemDeviceEnum As New Guid(&H62BE5D10, &H60EB, &H11D0, &HBD, &H3B, &H0, _
         &HA0, &HC9, &H11, &HCE, &H86)

        Public Shared ReadOnly VideoInputDevice As New Guid(&H860BB310UI, &H5D01, &H11D0, &HBD, &H3B, &H0, _
         &HA0, &HC9, &H11, &HCE, &H86)

        <ComVisible(False)> _
        Friend Class MediaTypes
            Public Shared ReadOnly Video As New Guid(&H73646976, &H0, &H10, &H80, &H0, &H0, _
             &HAA, &H0, &H38, &H9B, &H71)

            Public Shared ReadOnly Interleaved As New Guid(&H73766169, &H0, &H10, &H80, &H0, &H0, _
             &HAA, &H0, &H38, &H9B, &H71)

            Public Shared ReadOnly Audio As New Guid(&H73647561, &H0, &H10, &H80, &H0, &H0, _
             &HAA, &H0, &H38, &H9B, &H71)

            Public Shared ReadOnly Text As New Guid(&H73747874, &H0, &H10, &H80, &H0, &H0, _
             &HAA, &H0, &H38, &H9B, &H71)

            Public Shared ReadOnly Stream As New Guid(&HE436EB83UI, &H524F, &H11CE, &H9F, &H53, &H0, _
             &H20, &HAF, &HB, &HA7, &H70)
        End Class

        <ComVisible(False)> _
        Friend Class MediaSubTypes
            Public Shared ReadOnly YUYV As New Guid(&H56595559, &H0, &H10, &H80, &H0, &H0, _
             &HAA, &H0, &H38, &H9B, &H71)

            Public Shared ReadOnly IYUV As New Guid(&H56555949, &H0, &H10, &H80, &H0, &H0, _
             &HAA, &H0, &H38, &H9B, &H71)

            Public Shared ReadOnly DVSD As New Guid(&H44535644, &H0, &H10, &H80, &H0, &H0, _
             &HAA, &H0, &H38, &H9B, &H71)

            Public Shared ReadOnly RGB1 As New Guid(&HE436EB78UI, &H524F, &H11CE, &H9F, &H53, &H0, _
             &H20, &HAF, &HB, &HA7, &H70)

            Public Shared ReadOnly RGB4 As New Guid(&HE436EB79UI, &H524F, &H11CE, &H9F, &H53, &H0, _
             &H20, &HAF, &HB, &HA7, &H70)

            Public Shared ReadOnly RGB8 As New Guid(&HE436EB7AUI, &H524F, &H11CE, &H9F, &H53, &H0, _
             &H20, &HAF, &HB, &HA7, &H70)

            Public Shared ReadOnly RGB565 As New Guid(&HE436EB7BUI, &H524F, &H11CE, &H9F, &H53, &H0, _
             &H20, &HAF, &HB, &HA7, &H70)

            Public Shared ReadOnly RGB555 As New Guid(&HE436EB7CUI, &H524F, &H11CE, &H9F, &H53, &H0, _
             &H20, &HAF, &HB, &HA7, &H70)

            Public Shared ReadOnly RGB24 As New Guid(&HE436EB7DUI, &H524F, &H11CE, &H9F, &H53, &H0, _
             &H20, &HAF, &HB, &HA7, &H70)

            Public Shared ReadOnly RGB32 As New Guid(&HE436EB7EUI, &H524F, &H11CE, &H9F, &H53, &H0, _
             &H20, &HAF, &HB, &HA7, &H70)

            Public Shared ReadOnly Avi As New Guid(&HE436EB88UI, &H524F, &H11CE, &H9F, &H53, &H0, _
             &H20, &HAF, &HB, &HA7, &H70)

            Public Shared ReadOnly Asf As New Guid(&H3DB80F90, &H9412, &H11D1, &HAD, &HED, &H0, _
             &H0, &HF8, &H75, &H4B, &H99)
        End Class

        <DllImport("kernel32.dll", SetLastError:=True)> _
        Private Shared Function CreateFileMapping(hFile As IntPtr, lpFileMappingAttributes As IntPtr, flProtect As UInteger, dwMaximumSizeHigh As UInteger, dwMaximumSizeLow As UInteger, lpName As String) As IntPtr
        End Function

        <DllImport("kernel32.dll", SetLastError:=True)> _
        Private Shared Function MapViewOfFile(hFileMappingObject As IntPtr, dwDesiredAccess As UInteger, dwFileOffsetHigh As UInteger, dwFileOffsetLow As UInteger, dwNumberOfBytesToMap As UInteger) As IntPtr
        End Function
#End Region

#Region "Variables"
        Private _stopSignal As AutoResetEvent = Nothing
        Private _worker As Thread = Nothing
        Private _capGrabber As CapGrabber = Nothing

        Private _timer As Stopwatch = Stopwatch.StartNew()
        Private _frames As Double = 0.0
        Private _monikerString As String = ""
#End Region

#Region "Constructor & destructor"
        ''' <summary>
        ''' Initializes the default capture device
        ''' </summary>
        Public Sub New()
            Me.New("")
        End Sub

        ''' <summary>
        ''' Initializes a specific capture device
        ''' </summary>
        ''' <param name="moniker">Moniker string that represents a specific device</param>
        Public Sub New(moniker As String)
            ' Store moniker string
            MonikerString = moniker

            ' Check if this code is invoked by an application or as a user control
            If Application.Current IsNot Nothing Then
                ' Application, subscribe to exit event so we can shut down
                AddHandler Application.Current.Exit, AddressOf CurrentApplication_Exit
            End If
        End Sub

        ''' <summary>
        ''' Disposes the object
        ''' </summary>
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Stop
            StopCam()

            If Application.Current IsNot Nothing AndAlso Not IsRunning Then
                RemoveHandler Application.Current.[Exit], AddressOf CurrentApplication_Exit
            End If
        End Sub
#End Region

#Region "Events"
        ''' <summary>
        ''' Event that is invoked when a new bitmap is ready
        ''' </summary>
        Public Event NewBitmapReady As EventHandler
#End Region

#Region "Properties"
        ''' <summary>
        ''' Gets the device monikers
        ''' </summary>
        Public Shared ReadOnly Property DeviceMonikers() As FilterInfo()
            Get
                Dim filters As New List(Of FilterInfo)()
                Dim ms As IMoniker() = New IMoniker(0) {}
                Dim enumD As ICreateDevEnum = TryCast(Activator.CreateInstance(Type.GetTypeFromCLSID(SystemDeviceEnum)), ICreateDevEnum)
                Dim moniker As IEnumMoniker = Nothing
                Dim g As Guid = VideoInputDevice
                If enumD.CreateClassEnumerator(g, moniker, 0) = 0 Then
                    While True
                        Dim r As Integer = moniker.[Next](1, ms, IntPtr.Zero)
                        If r <> 0 OrElse ms(0) Is Nothing Then
                            Exit While
                        End If
                        Dim lFilterInfo As New FilterInfo(ms(0))
                        'Add if Name Not Empty
                        If Not String.IsNullOrEmpty(lFilterInfo.Name) Then
                            filters.Add(lFilterInfo)
                        End If
                        Marshal.ReleaseComObject(ms(0))
                        ms(0) = Nothing
                    End While
                End If

                Return filters.ToArray()
            End Get
        End Property

        ''' <summary>
        ''' Gets the available devices
        ''' </summary>
        Public Shared ReadOnly Property Devices() As CapDevice()
            Get
                ' Declare variables
                Dim devices__1 As New List(Of CapDevice)()

                ' Loop all monikers
                For Each moniker As FilterInfo In DeviceMonikers
                    devices__1.Add(New CapDevice(moniker.MonikerString))
                Next

                ' Return result
                Return devices__1.ToArray()
            End Get
        End Property

        ''' <summary>
        ''' Wrapper for the BitmapSource dependency property
        ''' </summary>
        Public Property BitmapSource() As InteropBitmap
            Get
                Return DirectCast(GetValue(BitmapSourceProperty), InteropBitmap)
            End Get
            Private Set(value As InteropBitmap)
                SetValue(BitmapSourcePropertyKey, value)
            End Set
        End Property

        Private Shared ReadOnly BitmapSourcePropertyKey As DependencyPropertyKey = DependencyProperty.RegisterReadOnly("BitmapSource", GetType(InteropBitmap), GetType(CapDevice), New UIPropertyMetadata(Nothing))

        Public Shared ReadOnly BitmapSourceProperty As DependencyProperty = BitmapSourcePropertyKey.DependencyProperty

        ''' <summary>
        ''' Wrapper for the Name dependency property
        ''' </summary>
        Public Property Name() As String
            Get
                Return DirectCast(GetValue(NameProperty), String)
            End Get
            Set(value As String)
                SetValue(NameProperty, value)
            End Set
        End Property

        ' Using a DependencyProperty as the backing store for Name.  This enables animation, styling, binding, etc...
        Public Shared ReadOnly NameProperty As DependencyProperty = DependencyProperty.Register("Name", GetType(String), GetType(CapDevice), New UIPropertyMetadata(""))

        ''' <summary>
        ''' Wrapper for the MonikerString dependency property
        ''' </summary>
        Public Property MonikerString() As String
            Get
                Return DirectCast(GetValue(MonikerStringProperty), String)
            End Get
            Set(value As String)
                SetValue(MonikerStringProperty, value)
            End Set
        End Property

        ' Using a DependencyProperty as the backing store for MonikerString.  This enables animation, styling, binding, etc...
        Public Shared ReadOnly MonikerStringProperty As DependencyProperty = DependencyProperty.Register("MonikerString", GetType(String), GetType(CapDevice), New UIPropertyMetadata("", New PropertyChangedCallback(AddressOf MonikerString_Changed)))

        ''' <summary>
        ''' Wrapper for the Framerate dependency property
        ''' </summary>
        Public Property Framerate() As Single
            Get
                Return CSng(GetValue(FramerateProperty))
            End Get
            Set(value As Single)
                SetValue(FramerateProperty, value)
            End Set
        End Property

        Public Shared ReadOnly FramerateProperty As DependencyProperty = DependencyProperty.Register("Framerate", GetType(Single), GetType(CapDevice), New UIPropertyMetadata(CSng(0)))

        ''' <summary>
        ''' Gets whether the capture device is currently running
        ''' </summary>
        Public ReadOnly Property IsRunning() As Boolean
            Get
                ' Check if we have a worker thread
                If _worker Is Nothing Then
                    Return False
                End If

                ' Check if we can join the thread
                If _worker.Join(0) = False Then
                    Return True
                End If

                ' Release
                Release()

                ' Not running
                Return False
            End Get
        End Property
#End Region

#Region "Methods"
        ''' <summary>
        ''' Invoked when the application exits
        ''' </summary>
        ''' <param name="sender">Sender</param>
        ''' <param name="e">EventArgs</param>
        Private Sub CurrentApplication_Exit(sender As Object, e As ExitEventArgs)
            ' Dispose
            Dispose()
        End Sub

        ''' <summary>
        ''' Invoked when a new frame arrived
        ''' </summary>
        ''' <param name="sender">Sender</param>
        ''' <param name="e">EventArgs</param>
        Private Sub capGrabber_NewFrameArrived(sender As Object, e As EventArgs)
            ' Make sure to be thread safe
            If Dispatcher IsNot Nothing Then
                Me.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Render, DirectCast(Sub()


                                                                                                        If BitmapSource IsNot Nothing Then
                                                                                                            BitmapSource.Invalidate()
                                                                                                            UpdateFramerate()
                                                                                                        End If
                                                                                                    End Sub _
            , SendOrPostCallback), Nothing)
            End If
        End Sub

        ''' <summary>
        ''' Invoked when the MonikerString dependency property has changed
        ''' </summary>
        ''' <param name="sender">Sender</param>
        ''' <param name="e">EventArgs</param>
        Private Shared Sub MonikerString_Changed(sender As DependencyObject, e As DependencyPropertyChangedEventArgs)
            ' Get typed sender
            Dim typedSender As CapDevice = TryCast(sender, CapDevice)
            If typedSender IsNot Nothing Then
                ' Always stop the device
                typedSender.StopCam()

                ' Get the new value
                Dim newMonikerString As String = TryCast(e.NewValue, String)

                ' Check if we have a valid moniker string
                If Not String.IsNullOrEmpty(newMonikerString) Then
                    ' Initialize device
                    typedSender.InitializeDeviceForMoniker(newMonikerString)

                    ' Start
                    typedSender.Start()
                End If
            End If
        End Sub

        ''' <summary>
        ''' Invoked when a property of the CapGrabber object has changed
        ''' </summary>
        ''' <param name="sender">Sender</param>
        ''' <param name="e">PropertyChangedEventArgs</param>
        Private Sub capGrabber_PropertyChanged(sender As Object, e As System.ComponentModel.PropertyChangedEventArgs)
            Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.DataBind, DirectCast(Sub()


                                                                                                   Try
                                                                                                       Dim _map As IntPtr = IntPtr.Zero
                                                                                                       Dim _section As IntPtr = IntPtr.Zero

                                                                                                       If (_capGrabber.Width <> 0) AndAlso (_capGrabber.Height <> 0) Then
                                                                                                           ' Get the pixel count
                                                                                                           Dim pcount As UInteger = CUInt(_capGrabber.Width * _capGrabber.Height * PixelFormats.Bgr32.BitsPerPixel / 8)

                                                                                                           ' Create a file mapping
                                                                                                           _section = CreateFileMapping(New IntPtr(-1), IntPtr.Zero, &H4, 0, pcount, Nothing)
                                                                                                           _map = MapViewOfFile(_section, &HF001F, 0, 0, pcount)

                                                                                                           ' Get the bitmap
                                                                                                           BitmapSource = TryCast(Imaging.CreateBitmapSourceFromMemorySection(_section, _capGrabber.Width, _capGrabber.Height, PixelFormats.Bgr32, _capGrabber.Width * PixelFormats.Bgr32.BitsPerPixel / 8, 0), InteropBitmap)
                                                                                                           _capGrabber.Map = _map

                                                                                                           ' Invoke event
                                                                                                           RaiseEvent NewBitmapReady(Me, Nothing)
                                                                                                       End If
                                                                                                   Catch ex As Exception
                                                                                                       ' Trace
                                                                                                       Trace.TraceError(ex.Message)
                                                                                                   End Try
                                                                                               End Sub _
             , SendOrPostCallback), Nothing)
        End Sub

        ''' <summary>
        ''' Updates the framerate
        ''' </summary>
        Private Sub UpdateFramerate()
            ' Increase the frames
            _frames += 1

            ' Check the timer
            If _timer.ElapsedMilliseconds >= 1000 Then
                ' Set the framerate
                Framerate = CSng(Math.Round(_frames * 1000 / _timer.ElapsedMilliseconds))

                ' Reset the timer again so we can count the framerate again
                _timer.Reset()
                _timer.Start()
                _frames = 0
            End If
        End Sub

        ''' <summary>
        ''' Initialize the device for a specific moniker
        ''' </summary>
        ''' <param name="moniker">Moniker to initialize the device for</param>
        Private Sub InitializeDeviceForMoniker(moniker As String)
            ' Store moniker (since dependency properties are not thread-safe, store it locally as well)
            _monikerString = moniker

            ' Find the name
            For Each filterInfo As FilterInfo In DeviceMonikers
                If filterInfo.MonikerString = moniker Then
                    Name = filterInfo.Name
                    Exit For
                End If
            Next
        End Sub

        ''' <summary>;
        ''' Starts grabbing images from the capture device
        ''' </summary>
        Public Sub Start()
            ' First check if we have a valid moniker string
            If String.IsNullOrEmpty(_monikerString) Then
                Return
            End If

            ' Check if we are already running
            If IsRunning Then
                ' Yes, stop it first
                StopCam()
            End If

            ' Create new grabber
            _capGrabber = New CapGrabber()
            AddHandler _capGrabber.PropertyChanged, AddressOf capGrabber_PropertyChanged
            AddHandler _capGrabber.NewFrameArrived, AddressOf capGrabber_NewFrameArrived

            ' Create manual reset event
            _stopSignal = New AutoResetEvent(False)

            ' Start the thread
            _worker = New Thread(AddressOf RunWorker)
            _worker.Start()
        End Sub

        ''' <summary>
        ''' Stops grabbing images from the capture device
        ''' </summary>
        Public Sub StopCam()
            Try
                ' Check if the capture device is even running
                If IsRunning Then
                    ' Yes, stop via the event
                    _stopSignal.Set()

                    ' Abort the thread (Mauvaise idée empèche la caméra de s'arreter)
                    '_worker.Abort();

                    If _worker IsNot Nothing Then
                        ' Join
                        _worker.Join(500)

                        ' Release
                        Release()
                    End If
                End If
            Catch ex As Exception
                ' Trace
                Trace.TraceError(ex.Message)

                ' Release
                Release()
            End Try
        End Sub

        ''' <summary>
        ''' Releases the capture device
        ''' </summary>
        Private Sub Release()
            ' Stop the thread
            _worker = Nothing

            ' Clear the event
            If _stopSignal IsNot Nothing Then
                _stopSignal.Set()
                '_Signal.Close();
                _stopSignal = Nothing
            End If

            ' Clean up
            '_graph = null;
            '_sourceObject = null;
            '_grabberObject = null;
            '_grabber = null;
            _capGrabber = Nothing
            '_control = null;
        End Sub

        ''' <summary>
        ''' Worker thread that captures the images
        ''' </summary>
        Private Sub RunWorker()
            Try
                Dim _graph As IGraphBuilder = Nothing
                Dim _grabber As ISampleGrabber = Nothing
                Dim _sourceObject As IBaseFilter = Nothing
                Dim _grabberObject As IBaseFilter = Nothing
                Dim _control As IMediaControl = Nothing

                ' Create the main graph
                _graph = TryCast(Activator.CreateInstance(Type.GetTypeFromCLSID(FilterGraph)), IGraphBuilder)

                ' Create the webcam source
                _sourceObject = FilterInfo.CreateFilter(_monikerString)

                ' Create the grabber
                _grabber = TryCast(Activator.CreateInstance(Type.GetTypeFromCLSID(SampleGrabber)), ISampleGrabber)
                _grabberObject = TryCast(_grabber, IBaseFilter)

                ' Add the source and grabber to the main graph
                _graph.AddFilter(_sourceObject, "source")
                _graph.AddFilter(_grabberObject, "grabber")

                Using mediaType As New AMMediaType()
                    mediaType.MajorType = MediaTypes.Video
                    mediaType.SubType = MediaSubTypes.RGB32
                    _grabber.SetMediaType(mediaType)

                    If _graph.Connect(_sourceObject.GetPin(PinDirection.Output, 0), _grabberObject.GetPin(PinDirection.Input, 0)) >= 0 Then
                        If _grabber.GetConnectedMediaType(mediaType) = 0 Then
                            ' During startup, this code can be too fast, so try at least 3 times
                            Dim retryCount As Integer = 0
                            Dim succeeded As Boolean = False
                            While (retryCount < 3) AndAlso Not succeeded
                                ' Tried again
                                retryCount += 1

                                Try
                                    ' Retrieve the grabber information
                                    Dim header As VideoInfoHeader = DirectCast(Marshal.PtrToStructure(mediaType.FormatPtr, GetType(VideoInfoHeader)), VideoInfoHeader)
                                    _capGrabber.Width = header.BmiHeader.Width
                                    _capGrabber.Height = header.BmiHeader.Height

                                    ' Succeeded
                                    succeeded = True
                                Catch generatedExceptionName As Exception
                                    ' Trace
                                    Trace.TraceInformation("Failed to retrieve the grabber information, tried {0} time(s)", retryCount)

                                    ' Sleep
                                    Thread.Sleep(50)
                                End Try
                            End While
                        End If
                    End If
                    _graph.Render(_grabberObject.GetPin(PinDirection.Output, 0))
                    _grabber.SetBufferSamples(False)
                    _grabber.SetOneShot(False)
                    _grabber.SetCallback(_capGrabber, 1)

                    ' Get the video window
                    Dim wnd As IVideoWindow = DirectCast(_graph, IVideoWindow)
                    wnd.put_AutoShow(False)
                    wnd = Nothing

                    ' Create the control and run
                    _control = DirectCast(_graph, IMediaControl)
                    _control.Run()

                    ' Wait for the stop signal
                    _stopSignal.WaitOne()

                    ' Stop when ready
                    '_control.Stop();
                    _control.StopWhenReady()
                End Using
            Catch ex As Exception
                ' Trace
                Trace.WriteLine(ex)
            Finally
                ' Clean up
                Release()
            End Try
        End Sub
#End Region
    End Class
End Namespace
