﻿
' FilterInfo v1.1
'
' This software is released into the public domain.  You are free to use it
' in any way you like, except that you may not sell this source code.
'
' This software is provided "as is" with no expressed or implied warranty.
' I accept no liability for any damage or loss of business that this software
' may cause.
' 
' This source code is originally written by Tamir Khason (see http://blogs.microsoft.co.il/blogs/tamir
' or http://www.codeplex.com/wpfcap).
' 
' Modifications are made by Geert van Horrik (CatenaLogic, see http://blog.catenalogic.com) 
'


Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.InteropServices.ComTypes
Imports System.Runtime.InteropServices

Namespace WpfCap
    ''' <summary>
    ''' FilterInfo class
    ''' </summary>
    Public Class FilterInfo
        Implements IComparable
#Region "Win32"
        <DllImport("ole32.dll")> _
        Public Shared Function CreateBindCtx(reserved As Integer, ByRef ppbc As IBindCtx) As Integer
        End Function

        <DllImport("ole32.dll", CharSet:=CharSet.Unicode)> _
        Public Shared Function MkParseDisplayName(pbc As IBindCtx, szUserName As String, ByRef pchEaten As Integer, ByRef ppmk As IMoniker) As Integer
        End Function
#End Region

#Region "Variables"
        Private ReadOnly _name As String
        Private ReadOnly _monikerString As String
#End Region

#Region "Constructor & destructor"
        ''' <summary>
        ''' Initializes a new filter info object
        ''' </summary>
        ''' <param name="monikerString">Moniker string to base the filter on</param>
        Public Sub New(monikerString As String)
            ' Store values
            _monikerString = monikerString
            _name = GetName(monikerString)
        End Sub

        ''' <summary>
        ''' Initializes a new filter info object
        ''' </summary>
        ''' <param name="moniker">Moniker to base the filter on</param>
        Friend Sub New(moniker As IMoniker)
            Me.New(GetMonikerString(moniker))
        End Sub
#End Region

#Region "Properties"
        ''' <summary>
        ''' Gets the name
        ''' </summary>
        Public ReadOnly Property Name() As String
            Get
                Return _name
            End Get
        End Property

        ''' <summary>
        ''' Gets the Moniker String
        ''' </summary>
        Public ReadOnly Property MonikerString() As String
            Get
                Return _monikerString
            End Get
        End Property
#End Region

#Region "Methods"
        ''' <summary>
        ''' Creates a specific filter based on the moniker
        ''' </summary>
        ''' <param name="filterMoniker">FilterMoniker to create the </param>
        ''' <returns>Filter or null</returns>
        Friend Shared Function CreateFilter(filterMoniker As String) As IBaseFilter
            ' Declare variables
            Dim filterObject As Object = Nothing
            Dim bindCtx As IBindCtx = Nothing
            Dim moniker As IMoniker = Nothing
            Dim n As Integer = 0

            ' Create binding context
            If CreateBindCtx(0, bindCtx) = 0 Then
                ' Parse the display name
                If MkParseDisplayName(bindCtx, filterMoniker, n, moniker) = 0 Then
                    ' Bind to the object
                    Dim filterId As Guid = GetType(IBaseFilter).GUID
                    moniker.BindToObject(Nothing, Nothing, filterId, filterObject)

                    ' Clean up
                    Marshal.ReleaseComObject(moniker)
                End If

                ' Clean up
                Marshal.ReleaseComObject(bindCtx)
            End If

            ' Return the filter
            Return TryCast(filterObject, IBaseFilter)
        End Function

        ''' <summary>
        ''' Gets the moniker string for a specific moniker
        ''' </summary>
        ''' <param name="moniker">Moniker to retrieve the moniker string of</param>
        ''' <returns>Moniker string</returns>
        Private Shared Function GetMonikerString(moniker As IMoniker) As String
            ' Declare variables
            Dim result As String = ""

            ' Get the display name of the moniker
            moniker.GetDisplayName(Nothing, Nothing, result)

            ' Return result
            Return result
        End Function

        ''' <summary>
        ''' Gets the name of a specific moniker
        ''' </summary>
        ''' <param name="moniker">Moniker object to get the name of</param>
        ''' <returns>Name of a specific moniker</returns>
        Private Shared Function GetName(moniker As IMoniker) As String
            ' Declare variables
            Dim bagObj As [Object] = Nothing
            Dim bag As IPropertyBag = Nothing

            Try
                ' Bind the moniker to storage
                Dim bagId As Guid = GetType(IPropertyBag).GUID
                moniker.BindToStorage(Nothing, Nothing, bagId, bagObj)
                bag = DirectCast(bagObj, IPropertyBag)

                ' Try to retrieve the friendly name
                Dim val As Object = ""
                Dim hr As Integer = bag.Read("FriendlyName", val, IntPtr.Zero)
                If hr <> 0 Then
                    Marshal.ThrowExceptionForHR(hr)
                End If

                ' Convert to string & validate
                Dim result As String = DirectCast(val, String)
                If String.IsNullOrEmpty(result) Then
                    Throw New ApplicationException()
                End If

                ' Return result
                Return result
            Catch generatedExceptionName As Exception
                ' Return empty string
                Return String.Empty
            Finally
                ' Clean up
                bag = Nothing
                If bagObj IsNot Nothing Then
                    Marshal.ReleaseComObject(bagObj)
                    bagObj = Nothing
                End If
            End Try
        End Function

        ''' <summary>
        ''' Gets the name of a specific moniker
        ''' </summary>
        ''' <param name="monikerString">Moniker string to get the name of</param>
        ''' <returns>Name of a specific moniker</returns>
        Private Shared Function GetName(monikerString As String) As String
            ' Declare variables
            Dim bindCtx As IBindCtx = Nothing
            Dim moniker As IMoniker = Nothing
            Dim name As String = ""
            Dim n As Integer = 0

            ' Create binding context
            If CreateBindCtx(0, bindCtx) = 0 Then
                ' Parse the display name
                If MkParseDisplayName(bindCtx, monikerString, n, moniker) = 0 Then
                    ' Get the name
                    name = GetName(moniker)

                    ' Clean up
                    Marshal.ReleaseComObject(moniker)
                    moniker = Nothing
                End If

                ' Clean up
                Marshal.ReleaseComObject(bindCtx)
                bindCtx = Nothing
            End If

            ' Return the name
            Return name
        End Function

        ''' <summary>
        ''' Compares the current object to another object
        ''' </summary>
        ''' <param name="value">Value to compare the current object to</param>
        ''' <returns>If 0, the values are equal</returns>
        Public Function CompareTo(value As Object) As Integer Implements IComparable.CompareTo
            ' Get the object as filter info
            Dim f As FilterInfo = DirectCast(value, FilterInfo)

            ' Check if we have a valid object
            If f Is Nothing Then
                ' No, so different
                Return 1
            End If

            ' Valid object, compare the names
            Return (Name.CompareTo(f.Name))
        End Function
#End Region
    End Class
End Namespace
