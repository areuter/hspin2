﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.InteropServices
Imports System.Runtime.CompilerServices

Namespace WpfCap
    Friend Module CapHelper

        <Extension()> _
        Public Function GetPin(filter As IBaseFilter, dir As PinDirection, num As Integer) As IPin
            Dim pin As IPin() = New IPin(0) {}
            Dim pinsEnum As IEnumPins = Nothing

            If filter.EnumPins(pinsEnum) = 0 Then
                Dim pinDir As PinDirection
                Dim n As Integer

                While pinsEnum.[Next](1, pin, n) = 0
                    pin(0).QueryDirection(pinDir)

                    If pinDir = dir Then
                        If num = 0 Then
                            Return pin(0)
                        End If
                        num -= 1
                    End If

                    Marshal.ReleaseComObject(pin(0))
                    pin(0) = Nothing
                End While
            End If
            Return Nothing
        End Function
    End Module
End Namespace


