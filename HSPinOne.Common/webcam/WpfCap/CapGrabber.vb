﻿
' CapGrabber v1.1
'
' This software is released into the public domain.  You are free to use it
' in any way you like, except that you may not sell this source code.
'
' This software is provided "as is" with no expressed or implied warranty.
' I accept no liability for any damage or loss of business that this software
' may cause.
' 
' This source code is originally written by Tamir Khason (see http://blogs.microsoft.co.il/blogs/tamir
' or http://www.codeplex.com/wpfcap).
' 
' Modifications are made by Geert van Horrik (CatenaLogic, see http://blog.catenalogic.com) 
'


Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Runtime.InteropServices
Imports System.Windows.Media
Imports System.ComponentModel

Namespace WpfCap
    Friend Class CapGrabber
        Implements ISampleGrabberCB
        Implements INotifyPropertyChanged

#Region "Win32 imports"
        <DllImport("Kernel32.dll", EntryPoint:="RtlMoveMemory")> _
        Private Shared Sub CopyMemory(Destination As IntPtr, Source As IntPtr, Length As Integer)
        End Sub
#End Region

#Region "Variables"
        Private _height As Integer = 0
        Private _width As Integer = 0
#End Region

#Region "Constructor & destructor"
        Public Sub New()
        End Sub
#End Region

#Region "Events"

        Public Event PropertyChanged(sender As Object, e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

        Public Event NewFrameArrived As EventHandler
#End Region

#Region "Properties"
        Public Property Map() As IntPtr
            Get
                Return m_Map
            End Get
            Set(value As IntPtr)
                m_Map = value
            End Set
        End Property
        Private m_Map As IntPtr

        ''' <summary>
        ''' Gets or sets the width of the grabber
        ''' </summary>
        Public Property Width() As Integer
            Get
                Return _width
            End Get
            Set(value As Integer)
                _width = value
                RaisePropertyChanged("Width")
            End Set
        End Property

        ''' <summary>
        ''' Gets or sets the height of the grabber
        ''' </summary>
        Public Property Height() As Integer
            Get
                Return _height
            End Get
            Set(value As Integer)
                _height = value
                RaisePropertyChanged("Height")
            End Set
        End Property
#End Region

#Region "Methods"
        Public Function SampleCB(sampleTime As Double, sample As IntPtr) As Integer Implements ISampleGrabberCB.SampleCB
            Return 0
        End Function

        Public Function BufferCB(sampleTime As Double, buffer As IntPtr, bufferLen As Integer) As Integer Implements ISampleGrabberCB.BufferCB
            If Map <> IntPtr.Zero Then
                CopyMemory(Map, buffer, bufferLen)
                OnNewFrameArrived()
            End If
            Return 0
        End Function

        Private Sub OnNewFrameArrived()
            RaiseEvent NewFrameArrived(Me, Nothing)
        End Sub

        Private Sub RaisePropertyChanged(name As String)
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(name))
        End Sub
#End Region


    End Class
End Namespace
