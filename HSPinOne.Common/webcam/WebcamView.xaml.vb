﻿Imports System.Windows.Data
Imports System.Windows
Imports System.Collections.ObjectModel
Imports System.Windows.Media.Imaging
Imports HSPinOne.Common.WpfCap

Public Class WebcamView

    Public Event RequestClose As EventHandler

    Private _DialogResult As Boolean = False
    Public ReadOnly Property DialogResult As Boolean
        Get
            Return _DialogResult
        End Get

    End Property

#Region "Properties"


    ''' <summary>
    ''' Wrapper for the WebcamRotation dependency property
    ''' </summary>
    Public Property WebcamRotation() As Double
        Get

            Return CType(GetValue(WebcamRotationProperty), Double)
        End Get
        Set(value As Double)
            SetValue(WebcamRotationProperty, value)
        End Set
    End Property

    ' Using a DependencyProperty as the backing store for WebcamRotation.  This enables animation, styling, binding, etc...
    Public Shared ReadOnly WebcamRotationProperty As DependencyProperty = DependencyProperty.Register("WebcamRotation", GetType(Double), GetType(WebcamView), New UIPropertyMetadata(CDbl(180)))

    ''' <summary>
    ''' Wrapper for the SelectedWebcam dependency property
    ''' </summary>
    Public Property SelectedWebcam() As CapDevice
        Get
            Return DirectCast(GetValue(SelectedWebcamProperty), CapDevice)
        End Get
        Set(value As CapDevice)
            SetValue(SelectedWebcamProperty, value)
        End Set
    End Property

    ' Using a DependencyProperty as the backing store for SelectedWebcam.  This enables animation, styling, binding, etc...
    Public Shared ReadOnly SelectedWebcamProperty As DependencyProperty = DependencyProperty.Register("SelectedWebcam", GetType(CapDevice), GetType(WebcamView), New UIPropertyMetadata(Nothing))

    ''' <summary>
    ''' Wrapper for the SelectedWebcamMonikerString dependency property
    ''' </summary>
    Public Property SelectedWebcamMonikerString() As String
        Get
            Return DirectCast(GetValue(SelectedWebcamMonikerStringProperty), String)
        End Get
        Set(value As String)
            SetValue(SelectedWebcamMonikerStringProperty, value)
        End Set
    End Property

    ' Using a DependencyProperty as the backing store for SelectedWebcamMonikerString.  This enables animation, styling, binding, etc...
    Public Shared ReadOnly SelectedWebcamMonikerStringProperty As DependencyProperty = DependencyProperty.Register("SelectedWebcamMonikerString", GetType(String), GetType(WebcamView), New UIPropertyMetadata("", New PropertyChangedCallback(AddressOf SelectedWebcamMonikerString_Changed)))

    ''' <summary>
    ''' Wrapper for the SelectedImages dependency property
    ''' </summary>
    Public Property SelectedImages() As ObservableCollection(Of BitmapSource)
        Get
            Return DirectCast(GetValue(SelectedImagesProperty), ObservableCollection(Of BitmapSource))
        End Get
        Set(value As ObservableCollection(Of BitmapSource))
            SetValue(SelectedImagesProperty, value)
        End Set
    End Property

    ' Using a DependencyProperty as the backing store for SelectedImages.  This enables animation, styling, binding, etc...
    Public Shared ReadOnly SelectedImagesProperty As DependencyProperty = DependencyProperty.Register("SelectedImages", GetType(ObservableCollection(Of BitmapSource)), GetType(WebcamView), New UIPropertyMetadata(New ObservableCollection(Of BitmapSource)()))


    Public Property SelectedImage As BitmapSource
        Get
            Return CType(GetValue(SelectedImageProperty), BitmapSource)
        End Get
        Set(value As BitmapSource)
            SetValue(SelectedImageProperty, value)
        End Set
    End Property

    Public Shared ReadOnly SelectedImageProperty As DependencyProperty = DependencyProperty.Register("SelectedImage", GetType(BitmapSource), GetType(WebcamView), New UIPropertyMetadata(Nothing))

#End Region

#Region "Methods"
    ''' <summary>
    ''' Invoked when the SelectedWebcamMonikerString dependency property has changed
    ''' </summary>
    ''' <param name="sender">Sender</param>
    ''' <param name="e">EventArgs</param>
    Private Shared Sub SelectedWebcamMonikerString_Changed(sender As DependencyObject, e As DependencyPropertyChangedEventArgs)
        ' Get typed sender
        Dim typedSender As WebcamView = TryCast(sender, WebcamView)
        If typedSender IsNot Nothing Then
            ' Get new value
            Dim newMonikerString As String = TryCast(e.NewValue, String)

            ' Update the device
            If typedSender.SelectedWebcam Is Nothing Then
                ' Create it
                typedSender.SelectedWebcam = New CapDevice("")
            End If

            ' Now set the moniker string
            typedSender.SelectedWebcam.MonikerString = newMonikerString
        End If
    End Sub
#End Region



    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        ' Create default device
        SelectedWebcamMonikerString = If((CapDevice.DeviceMonikers.Length > 0), CapDevice.DeviceMonikers(0).MonikerString, "")

    End Sub

    Private Sub captureImageNowButton_Click(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles captureImageNowButton.Click
        If Not IsNothing(SelectedWebcam) Then
            Dim bm = webcamPlayer.CurrentBitmap
            If Not IsNothing(bm) Then
                Me.SelectedImage = bm
            End If

        End If
    End Sub

    Public Sub CloseAction()
        Me.webcamPlayer.Dispose()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles Button1.Click
        If Not IsNothing(Me.SelectedImage) Then
            _DialogResult = True
            RaiseEvent RequestClose(Me, Nothing)
        End If
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class

Public Class ScaleConverter
    Implements IValueConverter




#Region "IMultiValueConverter Members"


    Public Function Convert(value As Object, targetType As System.Type, parameter As Object, culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.Convert
        ' Declare variables
        Dim width As Double = 0

        Try
            ' Get value
            width = CDbl(value)
        Catch generatedExceptionName As Exception
            ' Trace
            Trace.TraceError("Failed to cast '{0}' to Double", value)

            ' Return 0
            Return 0
        End Try

        ' Convert
        Return If((width > 0), (width / 4) * 3, 0)
    End Function


    Public Function ConvertBack(value As Object, targetType As System.Type, parameter As Object, culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
#End Region


   
End Class
