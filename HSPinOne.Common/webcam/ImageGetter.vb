﻿Imports WIA
Imports HSPinOne.Infrastructure
Imports System.Runtime.InteropServices
Imports System.Windows.Media
Imports System.Windows.Controls
Imports System.Windows.Interop
Imports System.Windows
Imports System.Windows.Media.Imaging
Imports System.IO

Public Class ImageGetter


    Private _ImageData As Byte()
    Public ReadOnly Property ImageData As Byte()
        Get
            Return _ImageData
        End Get
    End Property


    'Private twain As TwainLib.Twain
    Private ImageDisplay As Image

    Public Sub New()



    End Sub


    Public Function LoadImageFromDisk() As Boolean
        Dim dlg As New DialogService
        Dim fn As Object

        Try


            fn = dlg.GetFile("Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png")

            If Not IsNothing(fn) Then


                Dim imageData = File.ReadAllBytes(fn)
                Dim resizedImage As New BitmapImage()
                resizedImage.BeginInit()  ' Needed only so we can call EndInit()
                With resizedImage
                    .StreamSource = New MemoryStream(imageData)
                    .CreateOptions = BitmapCreateOptions.IgnoreColorProfile
                    '.DecodePixelHeight = 1024
                    .DecodePixelWidth = 1024
                End With
                resizedImage.EndInit()    ' This does the actual loading and resizing
                Dim encoder As JpegBitmapEncoder = New JpegBitmapEncoder()
                encoder.Frames.Add(BitmapFrame.Create(resizedImage))


                Using ms As MemoryStream = New MemoryStream
                    encoder.Save(ms)
                    _ImageData = ms.ToArray()
                    Return True
                End Using

            End If

        Catch ex As Exception
            dlg.ShowAlert("Fehler", ex.Message)
        End Try

        Return False
    End Function

    Public Function GetImageWia() As Boolean

        Try


            Dim CD As New WIA.CommonDialog
            Dim dev = CD.ShowSelectDevice(WiaDeviceType.UnspecifiedDeviceType, True, False)
            Dim F As WIA.ImageFile = CD.ShowAcquireImage
            If Not IsNothing(F) Then


                _ImageData = F.FileData.BinaryData
                Return True
            End If


        Catch ex As Exception
            Dim dlg As New DialogService
            dlg.ShowError("WIA", "Es ist ein Problem mit der Webcam Schnittstelle aufgetreten. stellen Sie sicher, dass die Webcam das WIA-Treibermodell unterstützt.")
        End Try
        Return False
    End Function

    Public Function GetImage() As Boolean
        Dim win As New WindowController

        Dim uc As New WebcamView
        AddHandler uc.RequestClose, AddressOf win.CloseWindow
        win.ShowWindow(uc, "Webcam")
        If Not IsNothing(uc.SelectedImage) And uc.DialogResult = True Then
            Dim encoder As New JpegBitmapEncoder
            encoder.Frames.Add(BitmapFrame.Create(uc.SelectedImage))
            encoder.QualityLevel = 100
            Dim bit As Byte()
            Using stream As New IO.MemoryStream
                'encoder.Frames.Add(BitmapFrame.Create(uc.SelectedImage))
                encoder.Save(stream)
                bit = stream.ToArray()
                stream.Close()
            End Using

            _ImageData = bit
            uc.CloseAction()
            uc = Nothing
            win = Nothing
            Return True
        Else
            uc.CloseAction()
            uc = Nothing
            win = Nothing
        End If

        Return False
    End Function


End Class
