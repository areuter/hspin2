﻿Imports MigraDoc.DocumentObjectModel
Imports MigraDoc.DocumentObjectModel.Shapes
Imports MigraDoc.Rendering

Public Class MigradocTools

    Private _headertext As String = ""
    Public Property Headertext As String
        Get
            Return _headertext
        End Get
        Set(value As String)
            _headertext = value
        End Set
    End Property

    Private _landscape As Boolean = False

    Private _filename As String = String.Empty



    Public Function CreateDocument() As Document

        Dim _document As New Document
        If _landscape = True Then
            _document.DefaultPageSetup.PageFormat = 4
            _document.DefaultPageSetup.Orientation = Orientation.Landscape
            _document.DefaultPageSetup.TopMargin = "1.8cm"
            _document.DefaultPageSetup.BottomMargin = "2cm"
            _document.DefaultPageSetup.LeftMargin = "1.8cm"
            _document.DefaultPageSetup.RightMargin = "1.8cm"
        Else
            _document.DefaultPageSetup.PageFormat = 4
            _document.DefaultPageSetup.Orientation = Orientation.Portrait
            _document.DefaultPageSetup.TopMargin = "2cm"
            _document.DefaultPageSetup.BottomMargin = "1.4cm"
            _document.DefaultPageSetup.LeftMargin = "1.8cm"
            _document.DefaultPageSetup.RightMargin = "1.8cm"
        End If

        DefineStyles(_document)

        DefineContentSection(_document)
        Return _document

    End Function

    Public Function CreateDocument(ByVal landscape As Boolean) As Document
        Me._landscape = landscape
        Return CreateDocument()


    End Function

    Public Function CreateDocument(ByVal landscape As Boolean, ByVal fn As String) As Document
        Me._landscape = landscape
        Me._filename = fn
        Return CreateDocument()


    End Function

    Public Sub Printout(ByVal document As Document)

        Dim pdfRenderer As MigraDoc.Rendering.PdfDocumentRenderer = New MigraDoc.Rendering.PdfDocumentRenderer()
        pdfRenderer.Document = document
        pdfRenderer.RenderDocument()
        Dim filename As String = RandomFilename.GetFilename(_filename, "pdf")
        pdfRenderer.Save(filename)
        'pdfRenderer.PdfDocument.Save(filename)
        ShellExecute.RunAssociatedProgram(filename)



    End Sub

    Public Sub SendasAttachment(ByVal document As Document, ByVal recipients As List(Of String), ByVal subject As String)
        Dim pdfRenderer As MigraDoc.Rendering.PdfDocumentRenderer = New MigraDoc.Rendering.PdfDocumentRenderer()
        pdfRenderer.Document = document
        pdfRenderer.RenderDocument()
        'pdfRenderer.PdfDocument.Save(filename)
        Dim fn = RandomFilename.GetFilename(_filename, "pdf")
        pdfRenderer.Save(fn)
        Dim cls As New clsMail
        cls.Subject = subject
        If Not IsNothing(recipients) Then
            For Each itm In recipients
                cls.AddRecipient(itm)
            Next
        End If

        cls.AddAttachment(fn)
        cls.SendinBackground()
    End Sub


 

    Private Sub DefineContentSection(ByVal document As Document)
        Dim section As Section = document.AddSection()
        section.PageSetup.OddAndEvenPagesHeaderFooter = True
        section.PageSetup.StartingNumber = 1

        Dim header As HeaderFooter = section.Headers.Primary
        Dim kopfzeile = header.AddParagraph

        kopfzeile.AddText(Headertext)
        kopfzeile.AddTab()
        kopfzeile.AddDateField("dd.MM.yyyy")



        header = section.Headers.EvenPage
        header.AddParagraph(Headertext)

        'Create a paragraph with centered page number. See definition of style "Footer".
        Dim footer As Paragraph = New Paragraph()
        footer.AddText("Seite ")
        footer.AddPageField()
        footer.AddTab()
        footer.AddText("Gedruckt am: " & Now.ToString() & " von " & HSPinOne.Infrastructure.MyGlobals.CurrentUsername)


        'Add paragraph to footer for odd pages.
        section.Footers.Primary.Add(footer)
        ' Add clone of paragraph to footer for odd pages. Cloning is necessary because an object must
        ' not belong to more than one other object. If you forget cloning an exception is thrown.

        section.Footers.EvenPage.Add(footer.Clone())

    End Sub


    '''<summary>Only supports horizontal and vertical lines.  Assumes single width, no dashes.</summary>
    Public Shared Sub DrawLine(frameContainer As TextFrame, color As Color, x1 As Single, x2 As Single)

        Dim frameRect As New TextFrame()
        Dim frame As New TextFrame()
        'nearly as big as frameContainer.  Its margins will position the line.
        frameRect.LineFormat.Color = color

        'horizontal
        frameRect.Height = Unit.FromPoint(0.01)
        frame.MarginTop = Unit.FromInch(0)
        If x2 > x1 Then
            'right
            frameRect.Width = Unit.FromCentimeter((x2 - x1))
            frame.MarginLeft = Unit.FromCentimeter(x1)
        End If

        frame.Elements.Add(frameRect)
        frame.RelativeVertical = RelativeVertical.Page
        frame.RelativeHorizontal = RelativeHorizontal.Page
        frame.Top = TopPosition.Parse("0 in")
        frame.Left = LeftPosition.Parse("0 in")
        frame.Width = frameContainer.Width
        Dim bottom As Unit = Unit.Zero

        bottom = Unit.FromInch(0)

        'If frameContainer.Height < bottom Then
        '    frameContainer.Height = bottom
        'End If
        frame.Height = frameContainer.Height
        frameContainer.Elements.Add(frame)
    End Sub

    Private Sub DefineStyles(ByVal document As Document)
        ' Get the predefined style Normal.

        Dim style As Style = document.Styles("Normal")


        'Because all styles are derived from Normal, the next line changes the 
        ' font of the whole document. Or, more exactly, it changes the font of
        ' all styles and paragraphs that do not redefine the font.

        style.Font.Name = "Tahoma"

        'Heading1 to Heading9 are predefined styles with an outline level. An outline level
        ' other than OutlineLevel.BodyText automatically creates the outline (or bookmarks) 
        ' in PDF.

        style = document.Styles("Heading1")
        style.Font.Name = "Tahoma"
        style.Font.Size = 14
        style.Font.Bold = True
        style.Font.Color = Colors.DarkBlue
        style.ParagraphFormat.PageBreakBefore = True
        style.ParagraphFormat.SpaceAfter = 12

        style = document.Styles("Heading2")
        style.Font.Size = 12
        style.Font.Bold = True
        style.ParagraphFormat.PageBreakBefore = False
        style.ParagraphFormat.SpaceBefore = 12
        style.ParagraphFormat.SpaceAfter = 12

        style = document.Styles("Heading3")
        style.Font.Size = 10
        style.Font.Bold = True
        style.Font.Italic = True
        style.ParagraphFormat.SpaceBefore = 6
        style.ParagraphFormat.SpaceAfter = 3

        style = document.Styles(StyleNames.Header)
        style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right)
        style.Font.Name = "Tahoma"

        style = document.Styles(StyleNames.Footer)
        style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center)
        style.Font.Name = "Tahoma"
        style.Font.Size = 8


        'Create a new style called TextBox based on style Normal
        style = document.Styles.AddStyle("TextBox", "Normal")
        style.ParagraphFormat.Alignment = ParagraphAlignment.Justify
        style.ParagraphFormat.Borders.Width = 2.5
        style.ParagraphFormat.Borders.Distance = "3pt"
        style.ParagraphFormat.Shading.Color = Colors.SkyBlue

        'Create a new style called Table based on style Normal
        style = document.Styles.AddStyle("Table", "Normal")
        style.ParagraphFormat.Alignment = ParagraphAlignment.Left
        style.ParagraphFormat.SpaceBefore = "1mm"
        style.ParagraphFormat.SpaceAfter = "1mm"
        style.ParagraphFormat.LineSpacing = "1mm"

        'Create a new style called TOC based on style Normal
        style = document.Styles.AddStyle("TOC", "Normal")
        style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right, TabLeader.Dots)
        style.ParagraphFormat.Font.Color = Colors.Blue
    End Sub

End Class
