﻿Imports MigraDoc.DocumentObjectModel
Imports MigraDoc.DocumentObjectModel.Shapes


Public Class In1Document

    Private _Document As Document
    Public Property Document() As Document
        Get
            Return _Document
        End Get
        Set(ByVal value As Document)
            _Document = value
        End Set
    End Property

    Private _Orientation As Orientation
    Public Property Orientation() As Orientation
        Get
            Return _Orientation
        End Get
        Set(ByVal value As Orientation)
            _Orientation = value
        End Set
    End Property

    Private _Filename As String
    Public Property Filename() As String
        Get
            Return _Filename
        End Get
        Set(ByVal value As String)
            _Filename = value
        End Set
    End Property


    Private _table As Object
    Private _alternationCounter As Integer = 0

    Public Sub New(Optional ByVal landscape As Boolean = False)
        Dim mt As New MigradocTools
        _Document = mt.CreateDocument(landscape)

    End Sub


    Public Sub AddParagraph(ByVal content As String, Optional ByVal style As String = "")
        _Document.LastSection.AddParagraph(content, style)
    End Sub

    Public Sub AddTable(ByVal cols() As Double, Optional ByVal hasborders As Boolean = False)
        _table = _Document.LastSection.AddTable()
        _table.Style = "Table"

        If hasborders Then
            _table.Borders.Color = Colors.Black
            _table.Borders.Width = 0.75
        End If

        For i As Integer = 0 To UBound(cols, 1)
            _table.AddColumn(Unit.FromCentimeter(cols(i)))
        Next i



    End Sub

    Public Sub AddHeadRow(ByVal rowdata As String)

        Dim args = rowdata.Split("|")
        Dim row = _table.AddRow
        row.HeadingFormat = True
        row.Format.Font.Bold = True
        row.Format.Font.Color = Colors.White
        row.Shading.Color = Colors.Black
        For i As Integer = 0 To UBound(args, 1)
            row.Cells(i).AddParagraph(args(i))
        Next i
    End Sub

    Public Sub AddRow(ByVal rowdata As String)
        Dim args = rowdata.Split("|")
        Dim row = _table.AddRow
        row.HeadingFormat = False
        row.Format.Font.Bold = False
        row.Format.Font.Color = Colors.Black
        If _alternationCounter Mod 2 = 0 Then
            row.Shading.Color = Colors.White
        Else
            row.Shading.Color = Colors.LightGray
        End If
        _alternationCounter += 1
        For i As Integer = 0 To UBound(args, 1)
            row.Cells(i).AddParagraph(args(i))
        Next i
    End Sub

    Public Sub Preview()
        Dim pdfRenderer As MigraDoc.Rendering.PdfDocumentRenderer = New MigraDoc.Rendering.PdfDocumentRenderer()
        pdfRenderer.Document = Document
        pdfRenderer.RenderDocument()
        Dim fn As String
        If _Filename = "" Then
            fn = RandomFilename.GetFilename(_Filename, "pdf")
        Else
            fn = _Filename
        End If

        pdfRenderer.Save(fn)
        'pdfRenderer.PdfDocument.Save(filename)
        ShellExecute.RunAssociatedProgram(fn)
    End Sub








End Class
