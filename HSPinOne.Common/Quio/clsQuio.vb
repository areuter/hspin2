﻿Imports System.Runtime.InteropServices
Imports System.Linq



Public Class QuioCV6600

    '**********************************
    ' Quio CV-6600 CVAPIV01.dll Code zum Auslesen der MifareID
    ' Copyright 2012 Alex Reuter
    '
    '***********************************

    Private _errortext As String = ""
    Public ReadOnly Property Errortext As String
        Get
            Return _errortext
        End Get

    End Property

    Public Declare Function CV_SetCommunicationType Lib "CVAPIV01.dll" (ByVal aType As Integer) As Integer 'aType 0--RS232, 1--USB, 2--UDP
    Public Declare Function SearchAndOpenUSBPorts Lib "CVAPIV01.dll" () As Integer 'return the count of readers connected to PC, the usb port have the rang from 0 to (count-1)
    Public Declare Function UsingUSBPort Lib "CVAPIV01.dll" (ByVal USBPort As Integer) As Integer 'with which usb port do you want to communicate ? the usb port: 0,1,2...(count-1) 
    Public Declare Sub CloseUSBPorts Lib "CVAPIV01.dll" ()
    Public Declare Function GetVersionNum Lib "CVAPIV01.dll" (ByVal DeviceAddress As Integer, ByVal aVer As String) As Integer
    Public Declare Function MF_Request Lib "CVAPIV01.dll" (ByVal DeviceAddress As Integer, ByVal inf_mode As Byte, ByVal buffer() As Byte) As Integer
    Public Declare Function MF_Anticoll Lib "CVAPIV01.dll" (ByVal DeviceAddress As Integer, ByVal snr() As Byte, ByVal status() As Byte) As Integer
    Public Declare Function ActiveLED Lib "CVAPIV01.dll" (ByVal DeviceAddress As Integer, ByVal NumLED As Byte, ByVal ontime As Byte, ByVal cycle As Byte) As Integer
    Public Declare Function ActiveBuzzer Lib "CVAPIV01.dll" (ByVal DeviceAddress As Integer, ByVal mode As Byte, ByRef pattern As Byte) As Integer
    Public Declare Function GetVersionAPI Lib "CVAPIV01.dll" (ByVal VersionAPI() As Byte) As Integer
    Public Declare Function GetSerialNum Lib "CVAPIV01.dll" (ByVal DeviceAddress As Integer, ByRef CurrentAddress As Integer, ByRef snr As Byte) As Integer
    Public Declare Function ActiveWiegandMode Lib "CVAPIV01.dll" (ByVal DeviceAddress As Integer, ByVal status As Byte) As Integer
    Public Declare Function CloseComm Lib "CVAPIV01.dll" () As Integer
    Public Declare Function WiegandMode Lib "CVAPIV01.dll" (ByVal DeviceAddress As Integer, ByVal data() As Byte) As Integer
    Public Declare Function MF_HLRequest Lib "CVAPIV01.dll" (ByVal DeviceAddress As Integer, ByVal mode As Byte, ByRef aLen As Integer, ByRef snr As Byte) As Integer

    'you can add other APIs here, which you want to use, according to our API reference
    Public Declare Function SearchUSBReadersCallBack Lib "CVAPIV01.dll" (ByVal fct As aOnSearchUSBReaders) As Integer

    Dim ReaderNumber As Integer

    <DllImport("msvcr70.dll", CallingConvention:=CallingConvention.Cdecl, CharSet:=CharSet.Auto)> _
    Shared Function _controlfp(ByVal n As Integer, ByVal mask As Integer) As Integer
    End Function



    Public Sub ResetFPCR()
        'reset the FPC Register (Word) to its default value,
        'as expected by .Net framework

        _controlfp(&H9001F, &HFFFFF)

    End Sub


    Public Function Karte_lesen() As Decimal
        Try

 
        Dim decCard As Decimal = 0
            'decCard = ReadCV6600()
            decCard = SerialNumber()

            Return decCard

        Catch ex As Exception
            Dim dlg As New HSPinOne.Infrastructure.DialogService
            dlg.ShowError("Kartenleser", "Es liegt ein Problem mit dem Kartenleser vor. Bitte informieren Sie den Administrator.")
        End Try
        Return 0

    End Function

    Public Function Hex2Long(ByVal inp As String) As Long

        Try

            Return Int64.Parse(inp, Globalization.NumberStyles.HexNumber)

        Catch ex As Exception

        End Try


        Return 0

    End Function

    Delegate Function aOnSearchUSBReaders(ByVal port As Integer) As Integer

    Public Function OnSearchUSBReaders(ByVal port As Integer) As Integer
        'System.Diagnostics.Debug.Print(port)
        Return True
    End Function

    Private Function SerialNumber() As Decimal
        Dim SN(16) As Byte
        Dim SNStr As String
        Dim aLen As Integer
        Dim i As Integer
        Dim soundOK(4) As Byte
        Dim soundErr(4) As Byte
        soundOK(0) = 2
        soundOK(1) = 0
        soundOK(2) = 0
        soundOK(3) = 0
        soundOK(4) = 1

        soundErr(0) = 1
        soundErr(1) = 0
        soundErr(2) = 1
        soundErr(3) = 0
        soundErr(4) = 1

        SNStr = ""

        Try

            CV_SetCommunicationType(1) 'use the usb interface

            'Dim handler As aOnSearchUSBReaders = AddressOf OnSearchUSBReaders
            'SearchUSBReadersCallback(handler)

            ReaderNumber = SearchAndOpenUSBPorts() 'find how many readers connectecd PC
            If ReaderNumber > 0 Then
                If UsingUSBPort(0) = 0 Then  ' using the first reader, you can call the API to use other reader at any time, but the port number must be < ReaderNumber
                Else
                    _errortext = "Leser nicht gefunden"
                    Return 0
                End If
            Else
                _errortext = "Keine Quio Leser angeschlossen"
                Return 0

            End If
            Dim tmp As String
            Dim res = MF_HLRequest(0, 1, aLen, SN(0))
            ResetFPCR()

            If res = 0 Then
                For i = 0 To (aLen - 1)
                    tmp = Hex(SN(i))
                    If Len(tmp) < 2 Then tmp = "0" & tmp
                    SNStr = tmp & SNStr
                    'If Len(Hex(SN(i))) = 1 Then
                    '    SNStr = SNStr + "" + ("0" & Hex(SN(i)))
                    'Else
                    '    SNStr = SNStr + Hex(SN(i))
                    'End If
                Next i
                Dim decResult As Decimal = 0
                decResult = Int64.Parse(SNStr, Globalization.NumberStyles.HexNumber)
                'decResult = CDec("&H" & SNStr)
                If decResult < 0 Then
                    decResult = decResult + 4294967296.0#
                End If
                If ActiveBuzzer(0, 4, soundOK(0)) <> 0 Then MsgBox("Fehler beim Buzzer")
                ResetFPCR()
                Return decResult
            ElseIf res = 4 Then
                ResetFPCR()
                CloseUSBPorts()
                _errortext = "Kartentimeout. Bitte erneut versuchen (4)"
                Return 0
            ElseIf res = 7 Then   'Leser nicht gefunden
                _errortext = "Kein Leser angeschlossen oder Leser nicht gefunden (7)"
                Return 0
            ElseIf res = 17 Then  'Keine Karte im Feld
                _errortext = "Keine Karte im Lesefeld gefunden (17)"
                If ActiveBuzzer(0, 4, soundErr(0)) <> 0 Then MsgBox("Fehler beim Buzzer")
                ResetFPCR()
                Return 0

            Else
                _errortext = "Fehler beim Lesen der Karte Errorcode: " & res
                Return 0
            End If

        Catch ex As Exception
            'Me.ListBox1.Items.Add(ex.Message)
            Return 0
        End Try
        Return 0
    End Function

    Private Function ReadCV6600() As Decimal

        Dim ATQ(1) As Byte ' String * 2
        Dim UID(3) As Byte 'String * 4
        Dim Collision(1) As Byte
        Dim i As Integer
        Dim Key(5) As Byte
        Dim strResult As String
        Dim buffer(63) As Byte
        Dim sector As Byte
        Dim soundOK(4) As Byte
        Dim soundErr(4) As Byte
        Dim Erg As Integer = 0
        Dim tmp As String
        Dim APIVer(63) As Byte

        Dim decResult As Decimal = 0
        Dim ReaderNumber As Integer

        strResult = ""
        _errortext = ""

        soundOK(0) = 2
        soundOK(1) = 0
        soundOK(2) = 0
        soundOK(3) = 0
        soundOK(4) = 1

        soundErr(0) = 1
        soundErr(1) = 0
        soundErr(2) = 1
        soundErr(3) = 0
        soundErr(4) = 1

        Try

            'If GetVersionAPI(APIVer) = 0 Then
            '    ResetFPCR()
            '    If APIVer(3) = Asc("3") Then
            '        CV_SetCommunicationType(0)
            '        ResetFPCR()
            '        CV_SetCommunicationType(1)
            '        ResetFPCR()
            '    End If
            'End If

            CV_SetCommunicationType(1) 'use the usb interface 
            Dim sta = CloseComm()
            ReaderNumber = SearchAndOpenUSBPorts() 'find how many readers connectecd PC
            If ReaderNumber > 0 Then
                If UsingUSBPort(0) = 0 Then  ' using the first reader, you can call the API to use other reader at any time, but the port number must be < ReaderNumber

                    Dim version As String
                    version = Strings.Space(50)
                    Dim ret = GetVersionNum(0, version)

                    sector = 0

                    Erg = MF_Request(0, 1, ATQ)
                    ResetFPCR()
                    If Erg = 0 Then
                        If MF_Anticoll(0, UID, Collision) = 0 Then
                            ResetFPCR()
                            For i = 0 To 3
                                tmp = Hex(UID(i))
                                If Len(tmp) < 2 Then tmp = "0" & tmp
                                strResult = tmp & strResult
                            Next i
                            decResult = CDec("&H" & strResult)
                            If decResult < 0 Then
                                decResult = decResult + 4294967296.0#
                            End If
                            If ActiveBuzzer(0, 4, soundOK(0)) <> 0 Then MsgBox("Fehler beim Buzzer")
                            ResetFPCR()
                            'CloseComm()

                            'decResult = Hex2Long(strResult)
                            Return decResult
                        End If
                    ElseIf Erg = 4 Then 'Timeout
                        'MsgBox "Timeout. Bitte Karte entfernen und erneut versuchen", vbOKOnly, "Fehler"
                        'Erg = OpenComm("COM1", 9600)
                        'ResetFPCR()
                        'Erg = CloseComm()
                        ResetFPCR()
                        CloseUSBPorts()
                        _errortext = "Kartentimeout. Bitte erneut versuchen"
                    ElseIf Erg = 7 Then   'Leser nicht gefunden
                        _errortext = "Kein Leser angeschlossen oder Leser nicht gefunden"
                        Return 7
                    ElseIf Erg = 17 Then  'Keine Karte im Feld
                        _errortext = "Keine Karte im Lesefeld gefunden"
                        'If ActiveLED(0, 2, 40, 1) <> 0 Then MsgBox "Fehler bei LED"
                        If ActiveBuzzer(0, 4, soundErr(0)) <> 0 Then MsgBox("Fehler beim Buzzer")
                        ResetFPCR()
                        Return 17
                    Else
                        _errortext = "Unbekannter Fehler mit Fehlercode " & Erg
                        CloseUSBPorts()
                        Return Erg
                    End If
                    ResetFPCR()
                End If
            End If
            Return 0
        Catch ex As Exception

            MsgBox(ex.Message)

        End Try

        Return 0
    End Function

End Class

