﻿Imports Newtonsoft.Json
Imports HSPinOne.Infrastructure
Imports System.Net
Imports System.IO
Imports System.Text


Public Class Kontocheck

    Private _checked As Boolean
    Private _errormessage As String
    Private _konto As String
    Private _blz As String
    Private _bankname As String
    Private _iban As String

    Public Property Checked
        Get
            Return _checked
        End Get
        Set(value)
            _checked = value
        End Set
    End Property

    Public Property Errormessage As String
        Get
            Return _errormessage
        End Get
        Set(value As String)
            _errormessage = value
        End Set
    End Property

    Public Property Bankname As String
        Get
            Return _bankname
        End Get
        Set(value As String)
            _bankname = value
        End Set
    End Property

    Private _BIC As String
    Public Property BIC As String
        Get
            Return _BIC
        End Get
        Set(value As String)
            _BIC = value
        End Set
    End Property



    Public Sub New(strKonto As String, strBLZ As String)
        _konto = strKonto
        _blz = strBLZ
        Checkit()
    End Sub

    Public Sub New(strIBAN As String)
        _iban = strIBAN

        Checkiban()
    End Sub

    Private Sub Checkit()
        If Not IsNothing(_konto) And Not IsNothing(_blz) Then


            Try



                'Dim XMLHttpRequest As Object

                Dim URL As String
                Dim strParams As String

                Dim res As String = ""



                URL = GlobalSettingService.HoleEinstellung("kontocheckurl")
                If IsNothing(URL) OrElse Trim(URL) = "" Then
                    Errormessage = "Keine Kontocheckurl angegeben"
                    Return
                End If

                strParams = "?konto=" & _konto & "&blz=" & _blz & "&bic=1"
                URL = URL & strParams

                Dim req = New WebBase
                res = req.GetJSON(URL)

                If Not req.HasError Then
                    Dim ret As Bank = JsonConvert.DeserializeObject(Of Bank)(res)

                    If Not ret.Bankname = "Error" Then
                        Bankname = ret.Bankname
                        Checked = ret.valid
                        BIC = ret.BIC
                    End If
                Else
                    Errormessage = req.Errormessage
                End If


            Catch ex As Exception
                Errormessage = ex.Message
            End Try
        End If
    End Sub

    Private Sub Checkiban()
        If Not IsNothing(_iban) Then


            Try



                'Dim XMLHttpRequest As Object

                Dim URL As String
                Dim strParams As String

                Dim res As String = ""



                URL = GlobalSettingService.HoleEinstellung("kontocheckurl")
                If IsNothing(URL) OrElse Trim(URL) = "" Then
                    Errormessage = "Keine Kontocheckurl angegeben"
                    Return
                End If

                strParams = "?iban=" & _iban
                URL = URL & strParams

                Dim req = New WebBase
                res = req.GetJSON(URL)

                If Not req.HasError Then
                    Dim ret As Bank = JsonConvert.DeserializeObject(Of Bank)(res)

                    If Not ret.Bankname = "Error" Then
                        Bankname = ret.Bankname
                        Checked = ret.valid
                        BIC = ret.BIC
                    End If
                Else
                    Errormessage = req.Errormessage
                End If


            Catch ex As Exception
                Errormessage = ex.Message
            End Try
        End If
    End Sub

End Class

Public Structure Bank
    Dim Bankname As String
    Dim Ort As String
    Dim valid As Boolean
    Dim BIC As String
End Structure

