﻿Imports Newtonsoft.Json
Imports System.Net



Public Class ExternalCustomerSource


    Public Function GetData(ByVal url As String, ByVal param As String)

        Using w As New WebClient
            Dim json As String = String.Empty
            Try
                json = w.DownloadString(url & "?" & param)
            Catch ex As Exception
                Return ex.Message
            End Try
            If Not IsNothing(json) Then
                If Not Trim(json) = "" Then
                    Dim data = JsonConvert.DeserializeObject(Of SourceFormat)(json)
                    If Not IsNothing(data.Message) Then
                        Return data.Message
                    End If

                End If
            End If
        End Using
        Return Nothing

    End Function

End Class

Public Class SourceFormat
    Public Message As String
End Class
