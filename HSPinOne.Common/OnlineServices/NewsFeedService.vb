﻿Imports Newtonsoft.Json
Imports System.Net
Imports System.IO
Imports System.Xml
Imports System.ComponentModel.Composition


Public Class NewsFeedService
    Implements INewsFeedService


    Public Function GetNews(ByVal url As String) As System.Collections.Generic.IList(Of NewsArticle) Implements INewsFeedService.GetNews
        Return ProcessRSS(url)
    End Function

    Public Function HasNews() As Boolean Implements INewsFeedService.HasNews
        Return True
    End Function

    Public Event Updated(ByVal sender As Object, ByVal e As System.EventArgs) Implements INewsFeedService.Updated

    Private Function ProcessRSS(ByVal rssURL As String) As IList(Of NewsArticle)

        Dim articles As New List(Of NewsArticle)
        Try



            Dim request As WebRequest = WebRequest.Create(rssURL)
            Dim response As WebResponse = request.GetResponse
            Dim rssStream As Stream = response.GetResponseStream
            Dim rssDoc As New XmlDocument
            rssDoc.Load(rssStream)
            Dim rssItems As XmlNodeList = rssDoc.SelectNodes("rss/channel/item")



            Dim title As String = ""
            Dim link As String = ""
            Dim body As String = ""

            Dim upperLimit As Integer = rssItems.Count
            If upperLimit > 5 Then
                upperLimit = 5
            End If
            If upperLimit > 0 Then
                Dim i As Integer = 0

                While i < upperLimit
                    Dim article As New NewsArticle
                    Dim rssDetail As XmlNode
                    rssDetail = rssItems.Item(i).SelectSingleNode("title")
                    If Not IsNothing(rssDetail) Then
                        article.Title = rssDetail.InnerText
                    Else
                        article.Title = ""
                    End If

                    rssDetail = rssItems.Item(i).SelectSingleNode("link")
                    If Not IsNothing(rssDetail) Then
                        article.Link = rssDetail.InnerText
                    Else
                        article.Link = ""
                    End If

                    rssDetail = rssItems.Item(i).SelectSingleNode("description")
                    If Not IsNothing(rssDetail) Then
                        article.Body = rssDetail.InnerText
                    Else
                        article.Body = ""
                    End If

                    rssDetail = rssItems.Item(i).SelectSingleNode("pubdate")
                    If Not IsNothing(rssDetail) Then
                        article.PublishedDate = CDate(rssDetail.InnerText)
                    End If
                    articles.Add(article)
                    i = i + 1
                End While

            End If

        Catch ex As Exception
            Dim eart As New NewsArticle
            eart.Title = "Fehler beim Download"
            eart.Body = ex.Message
            articles.Add(eart)


        End Try
        Return articles
    End Function

End Class
