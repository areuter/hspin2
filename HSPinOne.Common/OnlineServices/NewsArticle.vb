
Public Class NewsArticle
    Private _PublishedDate As Date

    Public Property PublishedDate() As Date
        Get
            Return _PublishedDate
        End Get
        Set(ByVal value As Date)
            _PublishedDate = value
        End Set
    End Property

    Private _Title As String

    Public Property Title() As String
        Get
            Return _Title
        End Get
        Set(ByVal value As String)
            _Title = value
        End Set
    End Property

    Private _Body As String

    Public Property Body() As String
        Get
            Return _Body
        End Get
        Set(ByVal value As String)
            _Body = value
        End Set
    End Property

    Private _link As String

    Public Property Link() As String
        Get
            Return _link
        End Get
        Set(ByVal value As String)
            _link = value
        End Set
    End Property
End Class

