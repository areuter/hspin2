﻿Imports Newtonsoft.Json
Imports HSPinOne.Infrastructure


Public Class PlzCheck

    Private _checked As Boolean
    Private _errormessage As String


    Public Property Checked
        Get
            Return _checked
        End Get
        Set(value)
            _checked = value
        End Set
    End Property

    Public Property Errormessage As String
        Get
            Return _errormessage
        End Get
        Set(value As String)
            _errormessage = value
        End Set
    End Property

    Private _city As String = String.Empty
    Public Property City As String
        Get
            Return _city
        End Get
        Set(value As String)
            _city = value
        End Set
    End Property

    Private _plz As String



    Public Sub New(plz As String)
        _plz = plz
        Checkit()
    End Sub

    Private Sub Checkit()
        If Not IsNothing(_plz) Then


            Try



                Dim URL As String
                Dim strParams As String

                Dim res As String



                URL = GlobalSettingService.HoleEinstellung("plzcheckurl")
                If IsNothing(URL) OrElse Trim(URL) = "" Then
                    Errormessage = "Keine PLZ Check URL angegeben"
                    Return
                End If
                strParams = "?plz=" & _plz
                URL = URL & strParams

                Dim req = New WebBase
                res = req.GetJSON(URL)

                If Not req.HasError Then

                    Dim ret As Postleitzahl = JsonConvert.DeserializeObject(Of Postleitzahl)(res)

                    If Not ret.City = "" Then
                        City = ret.City
                    End If
                Else
                    Errormessage = req.Errormessage
                End If


            Catch ex As Exception
                Errormessage = ex.Message
            End Try
        End If
    End Sub


End Class

Public Structure Postleitzahl
    Dim PLZ As String
    Dim City As String

End Structure


