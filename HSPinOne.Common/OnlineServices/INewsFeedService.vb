﻿Public Interface INewsFeedService
    Function GetNews(ByVal url As String) As IList(Of NewsArticle)
    Function HasNews() As Boolean
    Event Updated As EventHandler
End Interface
