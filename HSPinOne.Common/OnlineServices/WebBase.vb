﻿Imports System.Net
Imports System.IO
Imports System.Text


Public Class WebBase

    '    // Returns JSON string
    'string GET(url) 
    '{
    '    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
    '    try {
    '        WebResponse response = request.GetResponse();
    '        using (Stream responseStream = response.GetResponseStream()) {
    '            StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
    '            return reader.ReadToEnd();
    '        }
    '    }
    '    catch (WebException ex) {
    '        WebResponse errorResponse = ex.Response;
    '        using (Stream responseStream = errorResponse.GetResponseStream())
    '        {
    '            StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
    '            String errorText = reader.ReadToEnd();
    '            // log errorText
    '        }
    '        throw;
    '    }
    '}
    Private _errormessage As String = ""
    Public Property Errormessage As String
        Get
            Return _errormessage
        End Get
        Set(value As String)
            _errormessage = value
        End Set
    End Property

    Public Property HasError As Boolean = False



    Public Function GetJSON(url As String) As String
        Dim request As HttpWebRequest = WebRequest.Create(url)
        request.ContentType = "application/json"
        request.ServicePoint.Expect100Continue = False
        Try
            Dim response = request.GetResponse
            Using responseStream = response.GetResponseStream
                Dim reader = New StreamReader(responseStream, Encoding.UTF8)
                Return reader.ReadToEnd
            End Using
        Catch ex As WebException
            Dim errorResponse As WebResponse = ex.Response
            Using responseStream = errorResponse.GetResponseStream
                Dim reader = New StreamReader(responseStream, Encoding.GetEncoding("utf-8"))
                Errormessage = ex.Message.ToString
                HasError = True
            End Using
            Throw
        End Try

    End Function
End Class
