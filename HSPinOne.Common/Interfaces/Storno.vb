﻿Imports HSPinOne.Infrastructure
Imports System.ComponentModel.Composition


Public Class Storno
    Implements IStorno

    Public Function GetReason() As String Implements IStorno.GetReason

        Dim uc As New StornoView

        Dim vm = New ViewModel.StornoViewModel
        uc.DataContext = vm



        'AddHandler vm.SetFocus, AddressOf uc.SetFocus
        Dim rad As New WindowController
        Dim myeventhandler As New EventHandler(AddressOf rad.CloseWindow)

        AddHandler vm.RequestClose, myeventhandler
        AddHandler rad.CloseButtonClicked, AddressOf rad.CloseWindow
        rad.ShowWindow(uc, "Stornogrund")

        If Not IsNothing(vm.Reason) And Not Trim(vm.Reason) = "" Then
            Return vm.Reason
        End If
        Return ""

    End Function
End Class
