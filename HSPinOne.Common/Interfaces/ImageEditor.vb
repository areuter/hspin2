﻿Imports HSPinOne.Infrastructure
Imports System.ComponentModel.Composition
Imports System.Windows
Imports HSPinOne.DAL




Public Class ImageEditor
    Implements IImageEditor



    Public Function GetImage() As DAL.Customer Implements IImageEditor.GetImage

        Dim uc As New SearchView

        Dim vm = New ViewModel.SearchViewModel
        uc.DataContext = vm

        AddHandler vm.SetFocus, AddressOf uc.SetFocus
        Dim shell = Application.Current.MainWindow
        Dim rad As New WindowController
        AddHandler vm.RequestClose, AddressOf rad.CloseWindow
        rad.ShowWindow(uc, "Suche")
        If vm.DialogResult = True Then
            Return vm.SelectedCustomer
        End If
        RemoveHandler vm.SetFocus, AddressOf uc.SetFocus
        Return Nothing

    End Function
End Class

