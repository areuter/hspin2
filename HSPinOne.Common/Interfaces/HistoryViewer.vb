﻿Imports HSPinOne.Infrastructure
Imports System.ComponentModel.Composition
Imports System.Windows
Imports HSPinOne.DAL

Public Class HistoryViewer
    Implements IHistoryViewer


    Public Sub ShowHistory(tablename As String, recordid As Integer) Implements IHistoryViewer.ShowHistory

        If recordid > 0 Then
            Dim uc As New HistoryView

            Dim vm = New ViewModel.HistoryViewModel(tablename, recordid)
            uc.DataContext = vm



            'AddHandler vm.SetFocus, AddressOf uc.SetFocus
            Dim shell = Application.Current.MainWindow
            Dim rad As New WindowController
            Dim myeventhandler As New EventHandler(AddressOf rad.CloseWindow)

            'AddHandler vm.RequestClose, myeventhandler
            AddHandler rad.CloseButtonClicked, AddressOf rad.CloseWindow
            rad.ShowWindow(uc, "History")
        End If

    End Sub
End Class


