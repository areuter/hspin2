﻿Imports HSPinOne.Infrastructure
Imports System.ComponentModel.Composition
Imports System.Windows
Imports HSPinOne.DAL

<Export(GetType(ISearchCustomer)), PartCreationPolicy(CreationPolicy.Shared)> _
Public Class SearchCustomer
    Implements ISearchCustomer



    Public Function GetCustomer() As DAL.Customer Implements ISearchCustomer.GetCustomer

        Dim uc As New SearchView

        Dim vm = New ViewModel.SearchViewModel
        uc.DataContext = vm



        AddHandler vm.SetFocus, AddressOf uc.SetFocus
        Dim shell = Application.Current.MainWindow
        Dim rad As New WindowController
        Dim myeventhandler As New EventHandler(AddressOf rad.CloseWindow)

        AddHandler vm.RequestClose, myeventhandler
        AddHandler rad.CloseButtonClicked, AddressOf vm.CloseButtonClicked
        rad.ShowWindow(uc, "Suche")
        If vm.DialogResult = True Then
            Return vm.SelectedCustomer
        End If
        RemoveHandler vm.SetFocus, AddressOf uc.SetFocus
        RemoveHandler vm.RequestClose, myeventhandler
        Return Nothing

    End Function
End Class

