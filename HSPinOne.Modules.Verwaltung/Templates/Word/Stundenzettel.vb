﻿Imports HSPinOne.Common
Imports HSPinOne.DAL


Public Class Stundenzettel
    Private ReadOnly _cs As CourseStaff


    Public Sub Print()

        Using con As New in1Entities

            Dim rpCustomer = New CustomerReplaceObject(_cs.Customer).Output
            Dim rpCourse = New CourseReplaceObject(_cs.Course).Output
            Dim rpTerm = New TermReplaceObject(_cs.Course.Term).Output

            Dim itm = con.CourseStaffs.Find(_cs.CourseStaffID)
            Dim termine = itm.Course.Appointments.OrderBy(Function(o) o.Start).ThenBy(Function(o) o.LocationID).ToList()

            Dim rpStaff = New StaffReplaceObject(_cs.CourseID, _cs.CustomerID)


            'Die Replacerlsiten zu einer zusammen fügen
            Dim list = rpCustomer.Concat(rpStaff.Output).Concat(rpCourse).Concat(rpTerm).ToDictionary(Function(kvp) kvp.Key,
                                                                                       Function(kvp) kvp.Value)

            Dim w As New WordAutomation(list)
            w.AddTable("staff.kurstermine", rpStaff.StundenList)
            'w.AddTable("staff.kursterminegehalt", rpStaff.StundenList)
            w.Show()

            itm.Stdzetteldruck = Now
            con.SaveChanges()
        End Using
    End Sub


    Public Sub New(ByVal cs As CourseStaff)
        Me._cs = cs
    End Sub
End Class
