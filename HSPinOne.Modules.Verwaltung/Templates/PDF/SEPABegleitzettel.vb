﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports HSPinOne.Common
Imports System.Xml



Namespace Templates




    Public Class SEPABegleitzettel


        Private _datei As String


        Public Sub New(datei As String)
            _datei = datei

        End Sub


        Public Sub CreateDocument()

            Dim docx As New System.Xml.XmlDocument
            docx.Load(_datei)

            Dim msgid = docx.GetElementsByTagName("MsgId")(0).InnerText
            Dim erstelldatum = docx.GetElementsByTagName("CreDtTm")(0).InnerText
            Dim agname = docx.GetElementsByTagName("Nm")(0).InnerText


            '                    Case "Nm"
            'agname = reader.ReadInnerXml
            '        Case "PmtInfId"
            'sammler = reader.ReadInnerXml
            '        Case "CtrlSum"
            'summe = reader.ReadInnerXml
            '        Case "Cd"
            'cd = reader.ReadInnerXml
            '        Case "ReqdColltnDt"
            'ausfdatum = reader.ReadInnerXml
            '        Case "IBAN"
            'IBAN = reader.ReadInnerXml
            '        Case "BIC"
            'bic = reader.ReadInnerXml

            ' Einlesen der Sammler
            Dim pmtinf As New Dictionary(Of String, String)

            Dim node = docx.GetElementsByTagName("PmtInf")

            For Each itm As XmlNode In node

                Dim pmtinfid = itm("PmtInfId").InnerText
                Dim nbtxs = itm("NbOfTxs").InnerText
                Dim ctrlsum = itm("CtrlSum").InnerText
                Dim pmttpinf = itm("PmtTpInf").ChildNodes
                Dim lclInstrm = pmttpinf(1).InnerText
                Dim seqtype = pmttpinf(2).InnerText

                Dim reqdt = itm("ReqdColltnDt").InnerText
                Dim iban = itm("CdtrAcct").InnerText
                Dim bic = itm("CdtrAgt").InnerText

                pmtinf.Add(pmtinfid.ToString, "IBAN " & iban & " " & nbtxs.ToString & " Datensätze " & ctrlsum.ToString & " EUR " & lclInstrm.ToString & " " & seqtype & " Fällig: " & reqdt)

            Next


            Dim _doc = New In1Document()


            _doc.AddParagraph("SEPA - Begleitschein", "Heading1")


            _doc.AddTable(New Double() {8, 8})


            _doc.AddRow("Erstellungsdatum" & "|" & Now.ToShortDateString)

            _doc.AddRow("Auftraggeber" & "|" & agname)

            _doc.AddRow("Datei-ID" & "|" & msgid)

            _doc.AddRow("Dateiname" & "|" & _datei)


            For Each itm In pmtinf
                _doc.AddRow("Sammler: " & itm.Key & "|" & itm.Value)
            Next

            _doc.AddParagraph("Unterschrift des Auftraggebers")



            _doc.Preview()


        End Sub

        Public Sub CreateDocument2()


            Dim reader As Xml.XmlReader = New Xml.XmlTextReader(_datei)

            Dim msgid As String = ""
            Dim erstelldatum As DateTime
            Dim anz As Integer = 0
            Dim agname As String = ""
            Dim sammler As String = ""
            Dim summe As String = ""
            Dim cd As String = ""
            Dim ausfdatum As DateTime
            Dim iban As String = ""
            Dim bic As String = ""




            Do While reader.Read
                Select Case reader.Name
                    Case "MsgId"
                        msgid = reader.ReadInnerXml
                    Case "CreDtTm"
                        erstelldatum = reader.ReadInnerXml
                    Case "NbOfTxs"
                        anz = reader.ReadInnerXml
                    Case "Nm"
                        agname = reader.ReadInnerXml
                    Case "PmtInfId"
                        sammler = reader.ReadInnerXml
                    Case "CtrlSum"
                        summe = reader.ReadInnerXml
                    Case "Cd"
                        cd = reader.ReadInnerXml
                    Case "ReqdColltnDt"
                        ausfdatum = reader.ReadInnerXml
                    Case "IBAN"
                        iban = reader.ReadInnerXml
                    Case "BIC"
                        bic = reader.ReadInnerXml


                End Select
            Loop



            Dim doc As New In1Document


            doc.AddParagraph("SEPA - Begleitschein", "Heading1")



            doc.AddTable(New Double() {8, 8})

            doc.AddRow("Erstellungsdatum" & "|" & erstelldatum.ToShortDateString)

            doc.AddRow("Auftraggeber" & "|" & agname)

            doc.AddRow("Auftraggeber IBAN.|" & iban)

            doc.AddRow("Auftraggeber BIC|" & bic)

            doc.AddRow("Datei-ID|" & msgid)
            doc.AddRow("Dateiname|" & _datei)

            doc.AddRow("Anzahl Datensätze|" & anz)

            doc.AddRow("Summe der €-Beträge|" & summe)

            doc.AddRow("Fälligkeitsdatum|" & ausfdatum.ToShortDateString)




            doc.AddParagraph("Unterschrift des Auftraggebers")



            doc.Preview()


        End Sub

        Public Sub CreateReportUeberweisung(ByVal agname As String, ByVal agiban As String, ByVal erstelldatum As DateTime, ByVal msgid As String, ByVal summe As Decimal, ByVal anz As Decimal)

            Dim doc = New In1Document


            doc.AddParagraph("SEPA - Begleitschein", "Heading1")
            doc.AddParagraph("Überweisungen", "Heading2")


            doc.AddTable(New Double() {8, 8})

            doc.AddRow("Erstellungsdatum|" & erstelldatum.ToShortDateString)

            doc.AddRow("Auftraggeber|" & agname)

            doc.AddRow("Auftraggeber IBAN.|" & agiban)

            doc.AddRow("Datei-ID|" & msgid)

            doc.AddRow("Dateiname|" & _datei)

            doc.AddRow("Anzahl Datensätze| " & anz)

            doc.AddRow("Summe der €-Beträge|" & summe)



            doc.AddParagraph("Unterschrift des Auftraggebers")



            doc.Preview()
        End Sub

    End Class



End Namespace


