﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports HSPinOne.Common
Imports MigraDoc.DocumentObjectModel



Namespace Templates




    Public Class Dtausbegleitzettel


        Private _agname As String
        Private _agkonto As Double
        Private _agblz As Double
        Private _datei As String
        Private _anz As Integer
        Private _summe As Double
        Private _sumkonto As Double
        Private _sumblz As Double




        Public Sub New(agname As String, agkonto As Double, agblz As Double, datei As String, anz As Integer, summe As Double, sumkonto As Double, sumblz As Double)
            _agname = agname
            _agkonto = agkonto
            _agblz = agblz
            _datei = datei
            _anz = anz
            _summe = summe
            _sumkonto = sumkonto
            _sumblz = sumblz

        End Sub


        Public Sub CreateDocument()

            Dim mt As New MigradocTools
            'mt.Headertext = GlobalSettingService.GetInstitution
            Dim doc = mt.CreateDocument(False)


            doc.LastSection.AddParagraph("DTAUS - Begleitschein", "Heading1")

            Dim par = doc.LastSection.AddParagraph()
            Dim tf = doc.LastSection.AddTextFrame()
            MigradocTools.DrawLine(tf, Colors.Black, 0, 16)

            doc.LastSection.AddParagraph()



            Dim table = doc.LastSection.AddTable()
            table.AddColumn(Unit.FromCentimeter(8))
            table.AddColumn(Unit.FromCentimeter(8))

            table.Rows.Height = 45


            Dim row = table.AddRow
            row.Cells(0).AddParagraph("Erstellungsdatum")
            row.Cells(1).AddParagraph(Now.ToShortDateString)

            row = table.AddRow
            row.Cells(0).AddParagraph("Auftraggeber")
            row.Cells(1).AddParagraph(_agname)

            row = table.AddRow
            row.Cells(0).AddParagraph("Auftraggeber Konto-Nr.")
            row.Cells(1).AddParagraph(_agkonto)

            row = table.AddRow
            row.Cells(0).AddParagraph("Auftraggeber Bankleitzahl")
            row.Cells(1).AddParagraph(_agblz)

            row = table.AddRow
            row.Cells(0).AddParagraph("Dateiname")
            row.Cells(1).AddParagraph(_datei)

            row = table.AddRow
            row.Cells(0).AddParagraph("Anzahl Datensätze")
            row.Cells(1).AddParagraph(_anz)

            row = table.AddRow
            row.Cells(0).AddParagraph("Summe der €-Beträge")
            row.Cells(1).AddParagraph(_summe.ToString("N2"))

            row = table.AddRow
            row.Cells(0).AddParagraph("Summe der Kontonummern")
            row.Cells(1).AddParagraph(_sumkonto.ToString("N0"))

            row = table.AddRow
            row.Cells(0).AddParagraph("Summe der Bankleitzahlen")
            row.Cells(1).AddParagraph(_sumblz.ToString("N0"))

            tf = doc.LastSection.AddTextFrame
            MigradocTools.DrawLine(tf, Colors.Black, 0, 16)




            tf = doc.LastSection.AddTextFrame()
            tf.AddParagraph("Unterschrift des Auftraggebers")



            mt.Printout(doc)


        End Sub
    End Class

End Namespace

