﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports HSPinOne.Common
Imports System.Text
Imports System.Collections.ObjectModel

Namespace Templates
    Public Class BillingsTable
        Private _doc As In1Document

        Public Sub CreateDocument(ByVal cust As Customer, ByVal billings As ObservableCollection(Of Billing))

            _doc = New In1Document(True)
            _doc.AddParagraph("Rechnungen Auswahl - " + cust.Suchname, "Heading1")

            Dim items = HSPinOne.Infrastructure.EnumConstants.GetStringsFromClassConstants(GetType(BillingStates))

            Dim cols() = New Double() {5, 2.5, 2.5, 2.5, 2.5, 1, 1.2, 1.2, 1.2, 1.2, 1.2, 1.2}
            _doc.AddTable(cols)

            Dim headrow As String = "VWZ|Fälligkeit|Buchungsdatum|Betrag|Rückbelastet|Status|Konto|KST|IA|SK|Steuer|USt"
            _doc.AddHeadRow(headrow)

            Dim liste = From c In billings Order By c.Faelligkeit Select c
            Dim summe As Double = 0
            For Each bill In liste
                Dim sb As New List(Of String)

                sb.Add(IIf(Not IsNothing(bill.Verwendungszweck), bill.Verwendungszweck, ""))
                sb.Add(bill.Faelligkeit.ToShortDateString)
                If Not IsNothing(bill.Buchungsdatum) Then
                    sb.Add(CType(bill.Buchungsdatum, DateTime).ToShortDateString)
                Else
                    sb.Add("")
                End If

                sb.Add(bill.Brutto.ToString("c"))
                summe = summe + bill.Brutto
                If Not IsNothing(bill.Rueckbuchungsdatum) Then
                    sb.Add(CType(bill.Rueckbuchungsdatum, DateTime).ToShortDateString)
                Else
                    sb.Add("")
                End If
                sb.Add(bill.BillingStatus)
                sb.Add(bill.AccountID)

                sb.Add(bill.KSTextern)
                sb.Add(bill.Innenauftrag)
                sb.Add(bill.Sachkonto)
                sb.Add(bill.Steuerkennzeichen)
                sb.Add(bill.Steuerprozent)
                _doc.AddRow(String.Join("|", sb))

            Next

            'Summenzeile
            _doc.AddHeadRow(" | | " + summe.ToString("c"))

            _doc.Preview()


        End Sub
    End Class
End Namespace
