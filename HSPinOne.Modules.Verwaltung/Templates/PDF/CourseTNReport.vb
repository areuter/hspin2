﻿Imports HSPinOne.DAL
Imports HSPinOne.Common
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Text

Namespace Templates



    Public Class CourseTNReport

        Private _recipients As List(Of String)

        Private _mt As MigradocTools
        Private doc As In1Document

        Private _currentva As Course
        Private _billings As New ObservableCollection(Of Billing)


        Public Sub AddRecipient(ByVal address As String)
            If IsNothing(_recipients) Then _recipients = New List(Of String)
            _recipients.Add(address)
        End Sub

        Public Sub CreateDocument()

            doc = New In1Document
            '_doc = _mt.CreateDocument(True, "Teilnehmerliste_" & _currentva.Sport.Sportname & "_" & IIf(IsNothing(_currentva.Zeittext), "", _currentva.Zeittext))
            doc.AddParagraph("Teilnehmerliste", "Heading1")

            doc.AddParagraph("Kurs: " & _currentva.Sport.Sportname & " " & IIf(IsNothing(_currentva.Zusatzinfo), "", _currentva.Zusatzinfo) & " " & IIf(IsNothing(_currentva.Zeittext), "", _currentva.Zeittext))

            doc.AddParagraph("Semester: " & _currentva.Term.Termname)

            doc.AddParagraph("Ort: " & _currentva.Location.LocationName)

            If _currentva.CourseStaffs.Any Then
                Dim staff As String = ""
                For Each itm In _currentva.CourseStaffs
                    If Not IsNothing(itm.Customer.Mail1) Then
                        staff = staff & itm.Customer.Suchname & " "
                    End If
                Next
                doc.AddParagraph("Leitung: " & staff)
            Else
                doc.AddParagraph("Leitung: ")
            End If


            doc.AddTable(New Double() {0.8, 4, 2.5, 5}, True)


            Dim i As Long = 0

            doc.AddRow("Nr.|Name|Status|E-Mail")


            'Sortieren nach Nachname
            Dim ol = (From c In _billings Where (c.IsStorno = False) Order By c.Customer.Suchname).ToList

            i = 1


            For Each person In ol
                Dim sb As New StringBuilder()
                sb.Append(i & ".|")
                sb.Append(IIf(Not IsNothing(person.Customer.Suchname), person.Customer.Suchname, ""))
                sb.Append("|")
                sb.Append(IIf(Not IsNothing(person.Customer), person.Customer.CustomerState.CustomerStatename, ""))
                sb.Append("|")
                sb.Append(IIf(Not IsNothing(person.Customer.Mail1), person.Customer.Mail1, ""))
                doc.AddRow(sb.ToString())
                i = i + 1
            Next



        End Sub

        Public Sub Printout()
            doc.Preview()
        End Sub

        Public Sub Send(ByVal subject As String)
            Try


                If Not IsNothing(_recipients) Then
                    '_mt.SendasAttachment(_doc, _recipients, subject)
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End Sub


        Public Sub New(ByVal va As Course, ByVal bi As ObservableCollection(Of Billing))
            Me._currentva = va
            Me._billings = bi
        End Sub
    End Class
End Namespace