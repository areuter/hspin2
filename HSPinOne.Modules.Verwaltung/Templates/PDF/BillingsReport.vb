﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports HSPinOne.Common
Imports System.Text

Namespace Templates




    Public Class BillingsReport
        Private _cust As Customer

        Public Sub New(ByVal cust As Customer)
            Me._cust = cust
        End Sub

        Private _doc As In1Document

        Public Sub CreateDocument()

            _doc = New In1Document()
            _doc.AddParagraph("Rechnungsübersicht - " + _cust.Suchname, "Heading1")

            Dim items = HSPinOne.Infrastructure.EnumConstants.GetStringsFromClassConstants(GetType(BillingStates))

            For Each itm In items
                Dim bs = itm
                Dim filter As List(Of Billing) = (_cust.Billings.Where(Function(o) o.BillingStatus = bs)).ToList
                PrintGroup(filter, itm)

            Next


            _doc.Preview()


        End Sub

        Private Sub PrintGroup(ByVal inliste As List(Of Billing), ByVal status As String)

            Dim statustext = BillingStates.GetName(status)

            _doc.AddParagraph("Status: " + statustext, "Heading2")

            Dim cols() = New Double() {5, 2.5, 2.5, 2.5, 2.5, 1, 2}
            _doc.AddTable(cols)

            Dim headrow As String = "VWZ|Fälligkeit|Buchungsdatum|Betrag|Rückbelastet|Status|Konto"
            _doc.AddHeadRow(headrow)

            Dim liste = From c In inliste Order By c.Faelligkeit Select c
            Dim summe As Double = 0
            For Each bill In liste
                Dim sb As New List(Of String)

                sb.Add(IIf(Not IsNothing(bill.Verwendungszweck), bill.Verwendungszweck, ""))
                sb.Add(bill.Faelligkeit.ToShortDateString)
                If Not IsNothing(bill.Buchungsdatum) Then
                    sb.Add(CType(bill.Buchungsdatum, DateTime).ToShortDateString)
                Else
                    sb.Add("")
                End If

                sb.Add(bill.Brutto.ToString("c"))
                summe = summe + bill.Brutto
                If Not IsNothing(bill.Rueckbuchungsdatum) Then
                    sb.Add(CType(bill.Rueckbuchungsdatum, DateTime).ToShortDateString)
                Else
                    sb.Add("")
                End If
                sb.Add(bill.BillingStatus)
                sb.Add(bill.AccountID)
                _doc.AddRow(String.Join("|", sb))

            Next

            'Summenzeile
            _doc.AddHeadRow(" | | " + summe.ToString("c"))



        End Sub

    End Class

End Namespace