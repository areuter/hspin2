﻿Imports MigraDoc.DocumentObjectModel
Imports HSPinOne.Common
Imports HSPinOne.DAL
Imports Document = MigraDoc.DocumentObjectModel.Document


Namespace Templates
    Public Class RestrictedCustomerReport
        Private ReadOnly _state As String

        Public Sub New(ByVal state As String)
            _state = state
        End Sub


        Public Sub CreateDocument(ByVal list As IList(Of Customer))

            Dim mt As New MigradocTools
            Dim doc = mt.CreateDocument
            doc.DefaultPageSetup.BottomMargin = 40

            doc.LastSection.AddParagraph("Übersicht - '" + _state + "' Kunden", "Heading1")
            PrintGroup(doc, list)
            mt.Printout(doc)
        End Sub

        Private Sub PrintGroup(ByVal doc As Document, ByVal list As List(Of Customer))


            Dim table = doc.LastSection.AddTable()
            table.Style = "Table"
            table.AddColumn(Unit.FromCentimeter(2))
            table.AddColumn(Unit.FromCentimeter(5))
            table.AddColumn(Unit.FromCentimeter(2.5))
            table.AddColumn(Unit.FromCentimeter(2.5))
            table.AddColumn(Unit.FromCentimeter(6))



            Dim row = table.AddRow
            row.HeadingFormat = True
            row.Format.Font.Bold = True
            row.Format.Font.Size = Unit.FromPoint(8)
            row.Format.Font.Color = Colors.White
            row.Shading.Color = Colors.Black

            row.Cells(0).AddParagraph("#")
            row.Cells(1).AddParagraph("Kunde")
            row.Cells(2).AddParagraph("Status")
            row.Cells(3).AddParagraph("Bearbeitet am")
            row.Cells(4).AddParagraph("Bemerkung")


            For Each bill In list
                row = table.AddRow
                If ((table.Rows.Count) Mod 2) = 1 Then
                    row.Shading.Color = Colors.LightGray
                End If
                row.Cells(0).AddParagraph(bill.CustomerID)
                row.Cells(1).AddParagraph(IIf(Not IsNothing(bill.Suchname), bill.Suchname, ""))
                row.Cells(2).AddParagraph(IIf(Not IsNothing(bill.CustomerStateID), bill.CustomerState.CustomerStatename,
                                              ""))

                row.Cells(3).AddParagraph(IIf(Not IsNothing(bill.Bearbeitetam), bill.Bearbeitetam.GetValueOrDefault().ToShortDateString(),
                                              ""))
                row.Cells(4).AddParagraph(IIf(Not IsNothing(bill.Checkinmessage), bill.Checkinmessage,
                                             ""))
            Next

            ''Summenzeile
            'row = table.AddRow
            'row.Format.Font.Color = Colors.White
            'row.Shading.Color = Colors.Black
            'row.Cells(3).AddParagraph(list.Count)
        End Sub
    End Class
End Namespace