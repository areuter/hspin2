﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports HSPinOne.Common
Imports System.Data.Entity


Namespace Templates


    Public Class BillingDetailReport

        Private _dic As New Lineitem()


        Public Sub New(ByVal billid As Integer)

            Using con As New in1Entities

                Dim bills = From c In con.Billings.Include(Function(o) o.Customer).Include(Function(o) o.Account).Include(Function(o) o.PaymentMethod) Where c.BillingID = billid Select c

                Dim bill = bills.First()


                Dim doc = New In1Document()

                doc.AddParagraph("Rechnungsdetail", "Heading1")



                doc.AddTable(New Double() {8, 8})



                _dic.Add("Kundendaten", "", 1)
                _dic.Add("Rechnungsdetail:", billid.ToString())
                _dic.Add("Kunde:", bill.Customer.Suchname)
                _dic.Add("Straße:", bill.Customer.Strasse)
                _dic.Add("Ort:", bill.Customer.PLZ & " " & bill.Customer.Ort)
                _dic.Add("Zahlungsdaten", "", 1)
                _dic.Add("Brutto: ", bill.Brutto.ToString("c"))
                _dic.Add("Fällig: ", bill.Faelligkeit.ToShortDateString())
                Dim BDatum As String = ""
                If Not IsNothing(bill.Buchungsdatum) Then BDatum = CType(bill.Buchungsdatum, DateTime).ToString()
                _dic.Add("Gebucht am: ", BDatum)
                _dic.Add("Zahlungsmethode: ", bill.PaymentMethod.PaymentMethodname)
                _dic.Add("Zahlungsinfo: ", bill.PaymentInformation)
                _dic.Add("Status: ", BillingStates.GetName(bill.BillingStatus))
                _dic.Add("Account:", bill.AccountID & " " & bill.Account.AccountName)
                _dic.Add("Kasse", IIf(IsNothing(bill.KasseNr), "-", bill.KasseNr))

                _dic.Add("SAP", "", 1)
                _dic.Add("Kostenstelle", bill.KSTextern)
                _dic.Add("Innenauftrag", bill.Innenauftrag)
                _dic.Add("Sachkonto", bill.Sachkonto)
                _dic.Add("Steuerkennzeichen", bill.Steuerkennzeichen)
                _dic.Add("Artikeldaten", "", 1)
                _dic.Add("VWZ: ", bill.Verwendungszweck)
                _dic.Add("Bemerkung: ", bill.Bemerkung)
                _dic.Add("Meta: ", bill.Comment)

                For Each itm In _dic.Items

                    Dim data As String = itm.col1
                    If Not IsNothing(itm.col2) Then data = data & "|" & itm.col2
                    If itm.style = 1 Then
                        doc.AddHeadRow(data)
                    Else
                        doc.AddRow(data)
                    End If

                Next



                doc.Preview()


            End Using


        End Sub


    End Class

    Public Class Lineitem
        Dim _dic As New List(Of Lineitemdetail)
        Public Sub Add(ByVal fi As String, ByVal sec As String)
            If IsNothing(fi) Then fi = ""
            If IsNothing(sec) Then sec = ""
            _dic.Add(New Lineitemdetail With {.col1 = fi, .col2 = sec})
        End Sub

        Public Sub Add(ByVal fi As String, ByVal sec As String, ByVal style As Integer)
            If IsNothing(fi) Then fi = ""
            If IsNothing(sec) Then sec = ""
            _dic.Add(New Lineitemdetail With {.col1 = fi, .col2 = sec, .style = style})
        End Sub

        Public Function Items() As List(Of Lineitemdetail)
            Return _dic
        End Function
    End Class

    Public Class Lineitemdetail
        Property col1 As String
        Property col2 As String
        Property style As Integer = 0
    End Class

End Namespace