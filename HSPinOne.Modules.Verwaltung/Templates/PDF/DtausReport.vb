﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports HSPinOne.Common
Imports System.Text

Namespace Templates




    Public Class DtausReport

        Private _lastschriften As List(Of Billing)
        Private _faelligkeit As DateTime
        Private _title As String


        Public Sub New(ByVal lastschriften As List(Of Billing), ByVal faelligkeit As DateTime, ByVal title As String)
            Me._lastschriften = lastschriften
            Me._faelligkeit = faelligkeit
            Me._title = title
        End Sub


        Public Sub CreateDocument()

            Dim doc = New In1Document


            doc.AddParagraph(_title & " " & _faelligkeit.ToShortDateString, "Heading1")

            doc.AddTable(New Double() {4, 4.3, 2, 5, 1.7})



            doc.AddRow("Kunde|IBAN|Betrag|VWZ|Datum")


            Dim sortedlist = _lastschriften.OrderBy(Function(o) o.Customer.Suchname).ThenBy(Function(o) o.Faelligkeit)

            For Each bill In sortedlist

                Dim row As New List(Of String)
                row.Add(bill.Customer.Suchname)
                row.Add(IIf(IsNothing(bill.Customer.IBAN), "", bill.Customer.IBAN))
                'row.Cells(2).AddParagraph(bill.Customer.BLZ)
                row.Add(bill.Brutto.ToString("c"))
                row.Add(IIf(Not IsNothing(bill.Verwendungszweck), bill.Verwendungszweck, ""))
                If Not IsNothing(bill.Rechnungsdatum) Then
                    row.Add(CType(bill.Rechnungsdatum, Date).ToShortDateString)
                End If
                doc.AddRow(String.Join("|", row))
            Next

            doc.Preview()


        End Sub
    End Class

End Namespace
