﻿Imports HSPinOne.DAL
Imports HSPinOne.Common



Namespace Templates
    Public Class BookingReport

        Private _book As Booking


        Private _doc As In1Document




        Public Sub CreateDocument()
            Dim _doc As New In1Document

            _doc.AddParagraph("Buchungsdetails", "Heading1")

            _doc.AddParagraph("Titel: " & _book.Subject, "Heading2")

            If Not IsNothing(_book.Description) Then
                _doc.AddParagraph(_book.Description)
            End If

            If Not IsNothing(_book.CustomerID) Then
                _doc.AddParagraph("Zugeordnete Person: " & _book.Customer.Suchname)
            End If

            _doc.AddParagraph("Termine", "Heading2")

            Dim apps As New List(Of String)
            If _book.Appointments.Any Then
                For Each app In _book.Appointments
                    apps.Add(app.Start.ToShortDateString & " " & app.Start.ToShortTimeString & "-" & app.Ende.ToShortDateString & " " & app.Ende.ToShortTimeString & " " & app.Location.LocationName)
                Next
            End If
            _doc.AddParagraph(String.Join(vbNewLine, apps.ToArray))

            _doc.Preview()




        End Sub


        Public Sub New(ByVal book As Booking)
            Me._book = book
        End Sub
    End Class
End Namespace
