﻿Imports HSPinOne.DAL
Imports HSPinOne.Common



Namespace Templates
    Public Class CustomerReport

        Private _cust As Customer
        Private _doc As In1Document


        Public Sub CreateDocument()

            _doc = New In1Document()
            _doc.AddParagraph(_cust.Suchname, "Heading1")

            _doc.AddTable(New Double() {6, 11})

            Dim list As New List(Of String)(New String() {"Vorname", "Nachname", "Strasse", "PLZ", "Ort", "Geburtstag", "Telefon1", "Telefon2", "Mail1", "Mail2", "Kontonummer", "BLZ", "IBAN", "BIC"})

            For Each itm In list
                Try
                    _doc.AddRow(itm.ToString & "|" & _cust.GetType.GetProperty(itm).GetValue(_cust, Nothing))
                Catch ex As Exception

                End Try
            Next
            _doc.Preview()




        End Sub


        Public Sub New(ByVal cust As Customer)
            Me._cust = cust
        End Sub
    End Class
End Namespace

