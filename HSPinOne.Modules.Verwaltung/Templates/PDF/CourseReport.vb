﻿Imports HSPinOne.DAL
Imports HSPinOne.Common




Namespace Templates
    Public Class CourseReport

        Private _currentva As Course
        Private _doc As In1Document


        Public Sub CreateDocument()

            _doc = New In1Document()

            _doc.AddParagraph("Veranstaltungsdetails", "Heading1")

            _doc.AddParagraph(_currentva.Sport.Sportname, "Heading2")


            If Not IsNothing(_currentva.Zeittext) Then
                _doc.AddParagraph(" " & _currentva.Zeittext)
            End If

            If Not IsNothing(_currentva.Zusatzinfo) Then
                _doc.AddParagraph(_currentva.Zusatzinfo)
            End If

            _doc.AddParagraph("Semester: " & _currentva.Term.Termname)
            _doc.AddParagraph("Termine", "Heading2")


            _doc.AddTable(New Double() {6, 3, 5, 3})

            _doc.AddHeadRow("Von - Bis|Uhrzeit|Ort| ")

            Dim itm = _currentva


            _doc.AddRow(itm.DTStart.ToShortDateString & " - " & itm.DTUntil.ToShortDateString & "|" &
                        itm.DTStart.ToShortTimeString & " - " & itm.DTEnd.ToShortTimeString & "|" &
                        itm.Location.LocationName)

            If itm.Appointments.Any Then
                _doc.AddRow("Einzeltermine")
                Dim apps As String = ""
                For Each app In itm.Appointments
                    apps = apps & ", " & app.Start.ToShortDateString
                Next
                _doc.AddRow(Mid(apps, 3))
            End If



            If _currentva.CourseStaffs.Any Then

                _doc.AddParagraph("Übungsleiter", "Heading2")

                For Each st In _currentva.CourseStaffs
                    _doc.AddParagraph(st.Customer.Suchname & " Stundenlohn: " & st.StdLohn)
                Next
            End If
            If Not IsNothing(_currentva.Bemerkung) Then

                _doc.AddParagraph("Bemerkung", "Heading2")

                _doc.AddParagraph(_currentva.Bemerkung)
            End If

            If _currentva.Billings.Any Then
                _doc.AddParagraph("Teilnehmerliste", "Heading2")
                Dim sortList = _currentva.Billings.OrderBy(Function(x) x.Customer.Nachname).ToList()
                For Each tn In sortList
                    If IsNothing(tn.IsStorno) Or Not tn.IsStorno Then
                        _doc.AddParagraph(tn.Customer.Nachname & ", " & tn.Customer.Vorname)
                    End If

                Next
            End If


            _doc.Preview()




        End Sub


        Public Sub New(ByVal va As Course)
            Me._currentva = va
        End Sub
    End Class
End Namespace