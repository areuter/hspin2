﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Data.Entity

Namespace ViewModel


    Public Class AccountingSelectorViewModel
        Inherits ViewModelBase


        Public Event RequestClose As EventHandler

        Public Property Result As Boolean = False

        Private _atlist As ObservableCollection(Of AccountingTemplate)
        Public Property Atlist() As ObservableCollection(Of AccountingTemplate)
            Get
                Return _atlist
            End Get
            Set(ByVal value As ObservableCollection(Of AccountingTemplate))
                _atlist = value
            End Set
        End Property

        Private _Selected As AccountingTemplate
        Public Property Selected() As AccountingTemplate
            Get
                Return _Selected
            End Get
            Set(ByVal value As AccountingTemplate)
                _Selected = value
            End Set
        End Property

        Public Property OKCommand As New RelayCommand(AddressOf OKAction)
        Public Property CancelCommand As New RelayCommand(AddressOf CancelAction)


        Public Sub New()
            Using con As New in1Entities
                con.AccountingTemplates.Load()
                _atlist = con.AccountingTemplates.Local
                Selected = _atlist.FirstOrDefault()
            End Using
        End Sub

        Private Sub OKAction()
            Dim args As New CustomEventArgs
            args.Result = True
            Result = True
            RaiseEvent RequestClose(Me, args)
        End Sub

        Public Sub CancelAction()
            Dim args As New CustomEventArgs
            args.Result = False
            RaiseEvent RequestClose(Me, args)
        End Sub

    End Class
End Namespace