﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Input

Namespace ViewModel



    Public Class ActivityViewModel
        Inherits ViewModelBase


        Private _Act As Activity
        Public Property Act As Activity
            Get
                Return _Act
            End Get
            Set(value As Activity)
                _Act = value
                RaisePropertyChanged("Act")
            End Set
        End Property

        Public Property ActivityTypes As Dictionary(Of Integer, String)

        Public Property Directions As Dictionary(Of Integer, String)

        Private _DialogResult As Boolean = False
        Public Property DialogResult As Boolean
            Get
                Return _DialogResult
            End Get
            Set(value As Boolean)
                _DialogResult = value
            End Set
        End Property

        Public Event RequestClose As EventHandler

        Public Property ShowDokCommand As ICommand
        Public Property OKCommand As ICommand
        Public Property CancelCommand As ICommand


        Private _context As in1Entities

        Public Sub New(ByVal con As in1Entities, ByVal act As Activity)
            '0=Dokument, 1=Email, 2=Telefon, 3=Fax, 4=Persönlich 
            ActivityTypes = New Dictionary(Of Integer, String)
            ActivityTypes.Add(0, "Dokument")
            ActivityTypes.Add(1, "E-Mail")
            ActivityTypes.Add(2, "Telefon")
            ActivityTypes.Add(3, "Fax")
            ActivityTypes.Add(4, "Persönlich")

            Directions = New Dictionary(Of Integer, String)
            Directions.Add(1, "Eingehend")
            Directions.Add(2, "Ausgehend")

            _Act = act
            _context = con

            ShowDokCommand = New RelayCommand(AddressOf ShowDokAction)
            OKCommand = New RelayCommand(AddressOf OKAction)
            CancelCommand = New RelayCommand(AddressOf CancelAction)
        End Sub


        Private Sub ShowDokAction()
            If Not IsNothing(Act.Document) Then
                HSPinOne.Common.Dokviewer.ShowFilestream(Act.Document)
            End If
        End Sub

        Private Sub OKAction()
            DialogResult = True
            RaiseEvent RequestClose(Me, Nothing)
        End Sub

        Private Sub CancelAction()

            RaiseEvent RequestClose(Me, Nothing)
        End Sub




    End Class
End Namespace