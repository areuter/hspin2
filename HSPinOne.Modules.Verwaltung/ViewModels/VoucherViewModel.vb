﻿Imports HSPinOne.Infrastructure
Imports HSPinOne.DAL
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.Security.Cryptography
Imports Prism.Regions

Public Class VoucherViewModel
    Inherits ViewModelBase
    Implements INavigationAware



    Private Const Chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

    Dim _context As in1Entities
    Public Property Vouchers As ObservableCollection(Of Voucher)
    'Public ReadOnly Property Vouchertypes() As Dictionary(Of Integer, String) =
    '   New Dictionary(Of Integer, String) From {{0, "Kein"}, {1, "Prozent"}, {2, "Betrag"}}

    Public ReadOnly Property Vouchertypes As New List(Of String) From {"Kein", "Prozent", "Betrag"}



    Private _Voucher As Voucher
    Public Property Voucher() As Voucher
        Get
            Return _Voucher
        End Get
        Set(ByVal value As Voucher)
            _Voucher = value
            RaisePropertyChanged("Voucher")
        End Set
    End Property

    Private _Anzahl As Integer
    Public Property Anzahl() As Integer
        Get
            Return _Anzahl
        End Get
        Set(ByVal value As Integer)
            _Anzahl = value
            RaisePropertyChanged("Anzahl")
        End Set
    End Property

    Public Property AddCommand = New RelayCommand(AddressOf AddAction)
    Public Property GenerateCommand = New RelayCommand(AddressOf Generate, AddressOf CanGenerate)

    Public Property ExportCommand = New RelayCommand(AddressOf Export, AddressOf CanExport)
    Public Sub New()
        _context = New in1Entities
    End Sub


    Private Sub List()

        _context.Vouchers.Load()
        Vouchers = _context.Vouchers.Local

    End Sub

    Private Sub AddAction()

        Voucher = New Voucher()
        Voucher.Title = "Neue Aktion"
        Voucher.Validfrom = DateTime.Now
        Voucher.Validtill = Voucher.Validfrom.AddDays(30)
        Voucher.Actiontype = Vouchertype.Kein
        Voucher.Username = MyGlobals.CurrentUsername
        _context.Vouchers.Add(Voucher)
    End Sub

    Private Sub Generate()

        _context.SaveChanges()

        For i = 1 To Anzahl
            Dim a = Encode(Voucher.VoucherID)
            Dim r = GetRandomString(9)

            Dim b = Mid(r, 1, 6)
            Dim c = Mid(r, 7, 3)
            Dim s = String.Format("{0}-{1}-{2}", a, b, c)

            Dim v As New Vouchercode
            v.Code = s
            v.Created = Now

            Voucher.Vouchercodes.Add(v)
        Next
        _context.SaveChanges()



    End Sub

    Private Function CanGenerate() As Boolean
        If Not IsNothing(Voucher) AndAlso Len(Voucher.Title) > 3 AndAlso Voucher.Title <> "Neue Aktion" Then
            Return True
        End If
        Return False
    End Function

    Private Function Export()
        _context.SaveChanges()
        Dim csvexport As New HSPinOne.Common.CSVExport()
        csvexport.AddLine("Code", "Gültig vom", "Gültig bis", "Aktion", "Erstellt am", "Eingelöst am")
        For Each itm In Voucher.Vouchercodes
            csvexport.AddLine(itm.Code, itm.Voucher.Validfrom, itm.Voucher.Validtill, itm.Voucher.Title, itm.Created, itm.Usedat)
        Next
        csvexport.Export()

    End Function

    Private Function CanExport() As Boolean
        If Not IsNothing(Voucher) AndAlso Voucher.Vouchercodes.Count > 0 Then
            Return True
        End If
        Return False
    End Function

    Public Function Encode(arg As Long) As String
        Dim charArray As Char() = Chars.ToCharArray()
        Dim result = New Stack(Of Char)()
        While arg <> 0
            result.Push(charArray(arg Mod 36))
            arg \= 36
        End While
        Return New String(result.ToArray())
    End Function

    Private Function GetRandomString(ByVal length As Integer) As String

        Dim s = ""
        Using provider As New RNGCryptoServiceProvider()
            Do While s.Length <> length

                Dim oneByte(1) As Byte
                provider.GetBytes(oneByte)
                Dim character As Char = ChrW(oneByte(0))
                If Chars.Contains(character) Then
                    s += character
                End If

            Loop
        End Using
        Return s
    End Function

    Public Overrides Sub OnNavigatedTo(navigationContext As NavigationContext) Implements INavigationAware.OnNavigatedTo
        List()

    End Sub
End Class
