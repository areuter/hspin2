﻿Imports HSPinOne.Common
Imports HSPinOne.DAL
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports HSPinOne.Infrastructure
Imports System.Windows.Data
Imports System.Windows.Input
Imports System.ComponentModel.Composition
Imports Prism.Regions
Imports System.Threading.Tasks

Namespace ViewModel

    Public Class DienstvertraegeViewModel
        Inherits ViewModelBase
        Implements INavigationAware






        Private _DVView As ListCollectionView


        Private _years As List(Of Integer)
        Private _selectedyear As Integer

        Private _context As in1Entities
        Private _semester As IEnumerable(Of Term)
        Private _selectedsemester As Integer




#Region "   Properties"


        Public ReadOnly Property Context() As in1Entities
            Get
                Return _context
            End Get
        End Property

        Public Property Semester As IEnumerable(Of Term)
            Get
                Return _semester
            End Get
            Set(value As IEnumerable(Of Term))
                _semester = value
            End Set
        End Property





        Public Property Years As List(Of Integer)
            Get
                Return _years
            End Get
            Set(value As List(Of Integer))
                _years = value
            End Set
        End Property

        Public Property SelectedYear As Integer
            Get
                Return _selectedyear
            End Get
            Set(value As Integer)
                _selectedyear = value
                LadeDV()
            End Set
        End Property

        Private _SelectedDate As DateTime
        Public Property SelectedDate() As DateTime
            Get
                Return _SelectedDate
            End Get
            Set(ByVal value As DateTime)
                _SelectedDate = value
                LadeDV()
            End Set
        End Property


        Private _ocDV As ObservableCollection(Of StaffContract) = New ObservableCollection(Of StaffContract)

        Public Property ocDV As ObservableCollection(Of StaffContract)
            Get
                Return _ocDV
            End Get
            Set(value As ObservableCollection(Of StaffContract))
                _ocDV = value
                'RaisePropertyChanged("ocDV")
            End Set
        End Property


        Private _selectedDV As StaffContract
        Public Property SelectedDV As StaffContract
            Get
                Return _selectedDV
            End Get
            Set(value As StaffContract)
                _selectedDV = value
            End Set
        End Property



        Public Property DVView As ListCollectionView
            Get
                Return _DVView
            End Get
            Set(value As ListCollectionView)
                _DVView = value
                RaisePropertyChanged("DVView")
            End Set
        End Property


        Private _dvcheckall As Boolean = False
        Public Property DVCheckall As Boolean
            Get
                Return _dvcheckall
            End Get
            Set(value As Boolean)
                _dvcheckall = value
                RaisePropertyChanged("DVCheckall")
                pDVCheckall(value)
            End Set
        End Property

        Private _SearchText As String
        Public Property SearchText As String
            Get
                Return _SearchText
            End Get
            Set(value As String)
                _SearchText = value
            End Set
        End Property


        Public Property DVErhaltenCommand As ICommand
        Public Property DVErhaltenDelCommand As ICommand
        Public Property DVMailCommand As ICommand

        Public Property OpenDVCommand As ICommand
        Public Property SearchCommand As ICommand

        Private _firstLoad As Boolean = True



#End Region

#Region "   Constructor"



        Public Sub New()

            _context = New in1Entities

            Dim i As Integer = 2012
            Years = New List(Of Integer)
            Do While i <= Year(Now) + 2
                Years.Add(i)
                i = i + 1
            Loop
            'SelectedYear = Year(Now)
            SelectedDate = DateTime.Today().Date()




            DVErhaltenCommand = New RelayCommand(AddressOf DVErhalten, AddressOf CanDVErhalten)
            DVErhaltenDelCommand = New RelayCommand(AddressOf DVErhaltenDel, AddressOf CanDVErhalten)
            DVMailCommand = New RelayCommand(AddressOf DVMail, AddressOf CanDVMail)
            OpenDVCommand = New RelayCommand(AddressOf OpenDV)
            SearchCommand = New RelayCommand(AddressOf RefreshFilter)




            _firstLoad = False

        End Sub

        Private Sub OnInit()
            LadeDV()
        End Sub

        Private Sub LadeDV()
            If _firstLoad = True Then Return

            Try


                IsBusy = True
                Dim dt As DateTime = SelectedDate.AddDays(1).AddSeconds(-1)

                _context.StaffContracts.Where(Function(d) d.Beginn <= SelectedDate And d.Ende >= SelectedDate).Include(Function(o) o.Customer).Load()
                ocDV = _context.StaffContracts.Local

                For Each itm In ocDV
                    itm.IsSelected = False
                Next

                DVView = CType(CollectionViewSource.GetDefaultView(ocDV), ListCollectionView)
                DVView.Filter = New Predicate(Of Object)(AddressOf DVFilter)

                If Not IsNothing(DVView) Then

                    DVView.Refresh()
                    DVCheckall = False
                End If

                IsBusy = False

            Catch ex As Exception
                IsBusy = False
                Throw ex

            End Try
        End Sub


        Private Sub RefreshFilter()
            If Not IsNothing(DVView) Then DVView.Refresh()
        End Sub



#End Region

#Region "   Filter"
        Private Function DVFilter(ByVal item As Object) As Boolean
            Dim itm = TryCast(item, StaffContract)
            If Not IsNothing(itm) Then
                If itm.Beginn <= SelectedDate And itm.Ende >= SelectedDate Then
                    If SearchText Is Nothing OrElse Trim(SearchText) = "" Then Return True
                    If Not IsNothing(itm.Customer) AndAlso Not IsNothing(itm.Customer.Suchname) Then
                        If itm.Customer.Suchname.ToLower.StartsWith(SearchText.ToLower) Then Return True
                    End If
                End If

            End If
            Return False
        End Function



#End Region




        Private Sub pDVCheckall(ByVal value As Boolean)
            If value = False Then
                For Each itm In ocDV
                    itm.IsSelected = False
                Next

            Else
                For Each item In DVView

                    Dim itm = TryCast(item, StaffContract)
                    itm.IsSelected = value

                Next
            End If
        End Sub


#Region "   Commands"

        Private Sub DVErhalten()
            SelectedDV.Vertragzurueck = Now
            _context.SaveChanges()
        End Sub

        Private Sub DVErhaltenDel()
            SelectedDV.Vertragzurueck = Nothing
            _context.SaveChanges()
        End Sub

        Private Function CanDVErhalten(ByVal param As Object) As Boolean
            If Not IsNothing(SelectedDV) Then
                Return True
            End If
            Return False
        End Function

        Private Sub DVMail()
            Dim objMail As New clsMail
            For Each itm In From itm1 In ocDV Where itm1.IsSelected = True
                objMail.AddRecipient(itm.Customer.Mail1)
            Next
            objMail.Send()
        End Sub

        Private Function CanDVMail(ByVal param As Object) As Boolean
            If Not IsNothing(ocDV) Then
                Return ocDV.Any(Function(itm) Not IsNothing(itm.IsSelected) AndAlso itm.IsSelected)
            End If
            Return False
        End Function


        Private Sub OpenDV()


            Dim cs As New CustomerService
            If cs.ShowStaffContract(_context, SelectedDV) = True Then
                _context.SaveChanges()
            Else
                _context.Entry(SelectedDV).Reload()
            End If


        End Sub


#End Region

    End Class
End Namespace