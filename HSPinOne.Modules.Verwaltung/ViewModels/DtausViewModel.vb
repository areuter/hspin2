﻿Imports HSPinOne.DAL
Imports System.Data.Entity
Imports System.Collections.ObjectModel
Imports System.IO
Imports HSPinOne.Infrastructure
Imports System.Windows.Data
Imports System.Windows
Imports System.Windows.Input
Imports System.ComponentModel.Composition
Imports HSPinOne.Common
Imports Prism.Regions
Imports System.ComponentModel
Imports System.Text
Imports System.Data.Entity.SqlServer


Namespace ViewModel


    Public Class DtausViewModel
        Inherits HSPinOne.Infrastructure.ViewModelBase
        Implements INavigationAware


        Private _lsvview As ListCollectionView

        Private _oclsv As ObservableCollection(Of Billing)



        Private _context As in1Entities

        Private _firstload As Boolean = True

        Private _datenok As Boolean = False
        Private _sepamode As Integer = 0

        Private _errorlist As List(Of Erroritem)


        Private WithEvents bgw As New BackgroundWorker

        Private WithEvents bgwprint As New BackgroundWorker


#Region "   Properties"

        Private _ZTypes As Dictionary(Of Integer, String)
        Public ReadOnly Property ZTypes As Dictionary(Of Integer, String)
            Get
                Return New Dictionary(Of Integer, String) From {{0, "Lastschriften"}, {1, "Gutschriften"}, {2, "Überweisungen"}}
            End Get

        End Property

        Private _SelectedType As Integer = 0
        Public Property SelectedType As Integer
            Get
                Return _SelectedType
            End Get
            Set(value As Integer)
                _SelectedType = value
                RaisePropertyChanged("SelectedType")
            End Set
        End Property


        Public Property OCLSV As ObservableCollection(Of Billing)
            Get
                Return _oclsv
            End Get
            Set(value As ObservableCollection(Of Billing))
                _oclsv = value
            End Set
        End Property

        Public Property LSVView As ListCollectionView
            Get
                Return _lsvview
            End Get
            Set(value As ListCollectionView)
                _lsvview = value
                RaisePropertyChanged("LSVView")
            End Set
        End Property

        Private _faelligkeit As DateTime
        Public Property Faelligkeit As DateTime
            Get
                Return _faelligkeit
            End Get
            Set(value As DateTime)
                _faelligkeit = New DateTime(Year(value), Month(value), Day(value), 23, 59, 59)
            End Set
        End Property

        Private _expanded As Boolean = False
        Public Property Expanded As Boolean
            Get
                Return _expanded
            End Get
            Set(value As Boolean)
                _expanded = value
                RaisePropertyChanged("Expanded")
            End Set
        End Property

        Private _visible As Visibility = Visibility.Collapsed
        Public Property Visible As Visibility
            Get
                Return _visible
            End Get
            Set(value As Visibility)
                _visible = value
                RaisePropertyChanged("Visible")
            End Set
        End Property

        Private _errorliste As ObservableCollection(Of Erroritem)
        Public Property Errorliste As ObservableCollection(Of Erroritem)
            Get
                Return _errorliste
            End Get
            Set(value As ObservableCollection(Of Erroritem))
                _errorliste = value

            End Set
        End Property

        Private _accountslookup As List(Of Account)
        Public Property AccountsLookup As List(Of Account)
            Get
                Return _accountslookup
            End Get
            Set(value As List(Of Account))
                _accountslookup = value
            End Set
        End Property

        Private _selectedaccount As Account
        Public Property SelectedAccount As Account
            Get
                Return _selectedaccount
            End Get
            Set(ByVal value As Account)
                _selectedaccount = value
                RaisePropertyChanged("SelectedAccount")
            End Set
        End Property

        Private _CostcenterLookup As List(Of Costcenter)
        Public Property CostcenterLookup As List(Of Costcenter)
            Get
                Return _CostcenterLookup
            End Get
            Set(value As List(Of Costcenter))
                _CostcenterLookup = value
            End Set
        End Property

        Private _selectedcostcenter As Costcenter
        Public Property SelectedCostcenter As Costcenter
            Get
                Return _selectedcostcenter
            End Get
            Set(value As Costcenter)
                _selectedcostcenter = value
            End Set
        End Property


        Private _PrintBusy As Boolean = False
        Public Property PrintBusy As Boolean
            Get
                Return _PrintBusy
            End Get
            Set(value As Boolean)
                _PrintBusy = value
                RaisePropertyChanged("PrintBusy")
            End Set
        End Property

        Private _summe As Decimal = 0
        Public Property Summe As Decimal
            Get
                Return _summe
            End Get
            Set(value As Decimal)
                _summe = value

            End Set
        End Property

        Private _VWZText As String
        Public Property VWZText() As String
            Get
                Return _VWZText
            End Get
            Set(ByVal value As String)
                RaisePropertyChanged("VWZText")
                _VWZText = value
            End Set
        End Property




        Public Property DtausCommand As ICommand
        Public Property PrintCommand As ICommand
        Public Property PrintSumCommand As ICommand
        Public Property ExportSumCommand As ICommand = New RelayCommand(AddressOf ExportSum, AddressOf CanListedrucken)
        Public Property CSVCommand As ICommand
        Public Property SepaCommand As ICommand
        Public Property RefreshCommand As ICommand
        Public Property PrintErrorsCommand As ICommand


#End Region


        Public Sub New()


            'DtausCommand = New RelayCommand(AddressOf Dtaus_erstellen, AddressOf CanDtaus_erstellen)
            PrintCommand = New RelayCommand(AddressOf Listedrucken, AddressOf CanListedrucken)
            PrintSumCommand = New RelayCommand(AddressOf PrintSum, AddressOf CanListedrucken)
            CSVCommand = New RelayCommand(AddressOf CSVExport, AddressOf CanListedrucken)
            SepaCommand = New RelayCommand(AddressOf SEPAExport, AddressOf CanSepaExport)
            RefreshCommand = New RelayCommand(AddressOf LadeListe)
            PrintErrorsCommand = New RelayCommand(AddressOf PrintErrors)
            Dim _context As New in1Entities

            Errorliste = New ObservableCollection(Of Erroritem)

            Dim query = From c In _context.Accounts Select c

            AccountsLookup = New List(Of Account)(query.ToList)
            If AccountsLookup.Any Then
                SelectedAccount = AccountsLookup.FirstOrDefault
            End If

            Dim query2 = From c In _context.Costcenters Select c

            CostcenterLookup = query2.ToList
            If CostcenterLookup.Any Then
                SelectedCostcenter = CostcenterLookup.FirstOrDefault
            End If

            _firstload = False
            Faelligkeit = Now

            Dim sepamodus = GlobalSettingService.HoleEinstellung("SEPA")
            If Not IsNothing(sepamodus) Then
                Dim sm = CType(sepamodus, Integer)
                If Not IsNothing(sm) AndAlso sm > 0 Then _sepamode = sm
            End If



        End Sub

        Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw.DoWork
            If Not _firstload Then


                _datenok = False
                If Not IsNothing(_context) Then _context.Dispose()
                _context = New in1Entities


                ' Nur bei Bankkonten und Lastschriften und Gutschriften auf LSV Enabled checken
                Dim bLSVEnabled As Boolean = False
                If SelectedType < 2 AndAlso Not IsNothing(SelectedAccount.IBAN) Then
                    bLSVEnabled = True
                End If

                Faelligkeit = Faelligkeit.Date.AddHours(23).AddMinutes(59).AddSeconds(59)

                If SelectedType = 0 Then

                    _context.Billings.Include("Customer").Where(Function(b) _
                                                                    b.Faelligkeit <= Me.Faelligkeit _
                                                                    And b.BillingStatus = "O" _
                                                                    And b.AccountID = SelectedAccount.AccountID And b.Brutto > 0 _
                                                                    And b.CustomerID > 0 _
                                                                    And b.Customer.LSVEnabled = bLSVEnabled _
                                                                    And b.CostcenterID = SelectedCostcenter.CostcenterID _
                                                                    And b.PaymentMethodID = 2
                                                                ).OrderBy(Function(o) o.CustomerID).Load()

                    '_context.Billings.Include("Customer").Where(Function(b) b.DtausID = 8).OrderBy(Function(o) o.Faelligkeit).Load()


                ElseIf SelectedType = 1 Then
                    _context.Billings.Include("Customer").Where(Function(b) _
                                                                    b.Faelligkeit <= Me.Faelligkeit _
                                                                    And b.BillingStatus = "O" _
                                                                    And b.AccountID = SelectedAccount.AccountID And b.Brutto < 0 _
                                                                    And b.CustomerID > 0 _
                                                                    And b.Customer.LSVEnabled = bLSVEnabled _
                                                                    And b.CostcenterID = SelectedCostcenter.CostcenterID
                                                                ).OrderBy(Function(o) o.CustomerID).Load()

                ElseIf SelectedType = 2 Then
                    _context.Billings.Include("Customer").Where(Function(b) _
                                                                    b.Faelligkeit <= Me.Faelligkeit _
                                                                    And b.BillingStatus = "O" _
                                                                    And b.PaymentMethodID = 3 _
                                                                    And b.Brutto > 0 _
                                                                    And b.CustomerID > 0 _
                                                                    And b.CostcenterID = SelectedCostcenter.CostcenterID
                                                                ).OrderBy(Function(o) o.CustomerID).Load()
                End If

                OCLSV = _context.Billings.Local

                Dim s1 = (From c In OCLSV Select c.Brutto).Sum
                Summe = s1



                'Überprüfung der Daten
                _errorlist = New List(Of Erroritem)
                For Each itm In OCLSV

                    Dim errors As New List(Of String)

                    If IsNothing(itm.Customer.Suchname) Or Trim(itm.Customer.Suchname) = "" Then errors.Add("Fehlender Suchname")

                    If IsNothing(itm.Customer.Kontoinhaber) Or Trim(itm.Customer.Kontoinhaber) = "" Then errors.Add("Fehlender Kontoinhaber")


                    If SelectedType < 2 Then 'Validierung von IABN und BIC nur bei Lastschriften und Gutschriften
                        If IsNothing(itm.Customer.IBAN) Then
                            errors.Add("Fehlende IBAN")
                        Else

                            Dim chk As New IBAN.IBAN_validieren(itm.Customer.IBAN)
                            If Not chk.ISIBAN Then errors.Add("Fehlerhafte IBAN")
                        End If

                        If IsNothing(itm.Customer.BIC) OrElse Trim(itm.Customer.BIC) = "" Then
                            errors.Add("Fehlende BIC")
                        Else
                            Dim mybic = itm.Customer.BIC.Replace(" ", "")
                            If Not (mybic.Length = 8 Or mybic.Length = 11) Then
                                errors.Add("BIC zu kurz oder zu lang")
                            Else
                                Dim vbic As New IBAN.BICvalidieren(mybic)
                                If Not vbic.IsBic Then errors.Add(vbic.Errormsg)
                            End If
                        End If

                        ' Wenn Ausland, dann Adresse checken
                        If Not IsNothing(itm.Customer.IBAN) Then
                            If Not itm.Customer.IBAN.StartsWith("DE") Then
                                If Trim(itm.Customer.Strasse) = "" Or Trim(itm.Customer.PLZ) = "" Or Trim(itm.Customer.Ort) = "" Then
                                    errors.Add("Fehlende Adresse bei Auslandskonto")
                                End If
                            End If
                        End If
                    End If



                    If Not errors.Count = 0 Then
                        Dim erritm As New Erroritem
                        erritm.Errorbezeichnung = "Kunde: " & itm.Customer.Suchname & " [" & itm.CustomerID & "] : " & String.Join(", ", errors)
                        _errorlist.Add(erritm)
                    End If

                Next

            End If
        End Sub

        Private Sub bgw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles bgw.RunWorkerCompleted

            LSVView = CType(CollectionViewSource.GetDefaultView(OCLSV), ListCollectionView)
            Errorliste = New ObservableCollection(Of Erroritem)(_errorlist)
            If _errorlist.Count > 0 Then
                Expanded = True
                Visible = Visibility.Visible
            Else
                Expanded = False
                Visible = Visibility.Collapsed
                _datenok = True
            End If
            IsBusy = False
            RaisePropertyChanged("Errorliste")
            RaisePropertyChanged("Summe")
            'RaisePropertyChanged("OCLSV")
            CommandManager.InvalidateRequerySuggested()

        End Sub

        Private Sub LadeListe()

            If Not bgw.IsBusy Then
                IsBusy = True
                bgw.RunWorkerAsync()
            End If
        End Sub


        Protected Overrides Sub Finalize()
            If Not IsNothing(_context) Then _context.Dispose()

            MyBase.Finalize()
        End Sub


        Private Sub bgwprint_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgwprint.DoWork

            Dim rep As New Templates.DtausReport(OCLSV.ToList, Faelligkeit, ZTypes(SelectedType).ToString)
            rep.CreateDocument()
            rep = Nothing
        End Sub

        Private Sub bgwprint_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles bgwprint.RunWorkerCompleted
            IsBusy = False
        End Sub

        Private Sub Listedrucken()

            If Not bgwprint.IsBusy Then
                IsBusy = True
                bgwprint.RunWorkerAsync()
            End If



        End Sub

        Private Function CanListedrucken() As Boolean
            If Not IsNothing(OCLSV) Then
                If OCLSV.Count > 0 And _datenok Then Return True

            End If
            Return False

        End Function

        Private Sub PrintSum()
            Dim wc As New WaitCursor
            Try


                Dim rep As New HSPinOne.Common.Templates.CashReport
                rep.CreateDocument(Faelligkeit, OCLSV.ToList, SelectedAccount.AccountName & " Kostenstelle: " & SelectedCostcenter.Costcentername, ZTypes(SelectedType).ToString & " zum " & Faelligkeit.ToShortDateString, BillingStates.Offen)
                rep = Nothing
            Catch ex As Exception

            Finally
                wc.Dispose()

            End Try

        End Sub

        Private Sub ExportSum()

            Dim writer As StreamWriter = Nothing
            Dim writeropen As Boolean
            Dim strSep As String = ";"

            Dim sums As New Dictionary(Of String, Double)
            Dim customerstates As List(Of CustomerState)
            Dim producttypes As List(Of ProductType)

            Using con As New in1Entities
                producttypes = (From c In con.ProductTypes Select c).ToList
                customerstates = (From c In con.CustomerStates Select c).ToList
            End Using

            Dim items = OCLSV.ToList()
            '
            'Dim result = From c In OCLSV
            '             Group c By c.CustomerID, c.Customer.Kontoinhaber, c.Customer.Kontonummer, c.Customer.BLZ Into Group
            '             Select CustomerID, Kontoinhaber, Kontonummer, BLZ, Summe = Group.Sum(Function(o) o.Brutto), VWZ1 = String.Join(" ", Group.Select(Function(a) a.Verwendungszweck))

            Dim result = From c In items
                         Group c By c.CustomerStateID, c.ProductTypeID Into Group
                         Select CustomerStateID, ProductTypeID, Summe = Group.Sum(Function(o) o.Brutto)
                         Order By ProductTypeID, CustomerStateID

            Dim myfn As String = "Export_" & Year(Now).ToString & Month(Now).ToString & Day(Now).ToString



            Dim dlg As New DialogService
            Dim fn = dlg.SaveFile("CSV-Dateien (*.csv)|*.csv", myfn, ".csv")
            If fn Is Nothing Then
                Exit Sub
            End If

            IsBusy = True
            writer = New StreamWriter(fn, False, System.Text.Encoding.UTF8)
            writeropen = True

            'Zusammenstellen der Buchungen
            'OCLSV.OrderBy(Function(o) o.CustomerID)

            Dim header As String = CSVEncode("Datum") & strSep & CSVEncode("Warengruppe") & strSep &
                CSVEncode("Statusgruppe") & strSep & CSVEncode("Summe") & strSep &
                CSVEncode("Kostenstelle")
            writer.WriteLine(header)

            For Each itm In result
                Dim line As String = String.Empty
                Dim cs = customerstates.Where(Function(o) o.CustomerStateID = itm.CustomerStateID).First.CustomerStatename
                Dim pt = producttypes.Where(Function(o) o.ProductTypeID = itm.ProductTypeID).First.ProductTypeName
                line = CSVEncode(Now.ToString) & strSep & CSVEncode(pt) & strSep &
                        CSVEncode(cs) & strSep & CSVEncode(itm.Summe) & strSep & CSVEncode(SelectedCostcenter.Costcentername)
                writer.WriteLine(line)
            Next
            If writeropen Then writer.Close()
            IsBusy = False

        End Sub

        Private Sub PrintErrors()

            Dim doc As New In1Document
            doc.AddParagraph("Fehlerliste", "Heading1")

            For Each itm In Errorliste
                doc.AddParagraph(itm.Errorbezeichnung)
            Next
            doc.Preview()
            doc = Nothing
        End Sub



        Private Sub Verbuchen(ByVal Empfaengername As String, ByVal Empfaengerkontonummer As String, ByVal EmpfaengerBLZ As String,
                              ByVal intAnzCDatensaetze As Integer, ByVal dblSumKonto As Double, ByVal dblSumBLZ As Double,
                              ByVal decSumBetrag As Decimal, Optional msgid As String = "")


            Dim wc As WaitCursor = Nothing
            Try


                Dim dlg As New DialogService
                If dlg.AskConfirmation("Verbuchen", "Die Datei wurde erstellt. Wollen Sie die Rechnungen verbuchen?") = True Then

                    wc = New WaitCursor

                    'Eintrag in der DTAUS Tabelle
                    Dim dta As New Dtaus
                    dta.Agname = Empfaengername
                    dta.Agkonto = Empfaengerkontonummer
                    dta.Agblz = EmpfaengerBLZ
                    dta.SumC = intAnzCDatensaetze
                    dta.Sumkonto = dblSumKonto
                    dta.Sumblz = dblSumBLZ
                    dta.Sumbetrag = decSumBetrag
                    dta.Datum = Now
                    dta.Sicherungsdatei = msgid
                    _context.Dtauss.Add(dta)
                    _context.SaveChanges()

                    ' Rechnungen verbuchen
                    For Each bill In OCLSV
                        bill.BillingStatus = BillingStates.Bezahlt
                        bill.DtausID = dta.DtausID
                        bill.Buchungsdatum = Now

                        ' Wenn Lastschrift, dann Mandat bearbeiten
                        If SelectedType = 0 Then
                            bill.Customer.LetzterEinzug = Now
                            If bill.Customer.Mandatstyp = 0 Then bill.Customer.Mandatstyp = 1
                            bill.PaymentMethodID = 2
                        End If
                        ' Wenn Überweisung
                        If SelectedType = 1 Then
                            bill.PaymentMethodID = 3
                        End If

                        If Not IsNothing(bill.Customer.IBAN) AndAlso Not IsNothing(bill.Customer.BIC) Then
                            If IsNothing(bill.PaymentInformation) OrElse Trim(bill.PaymentInformation) = "" Then
                                bill.PaymentInformation = bill.Customer.IBAN & ";" & bill.Customer.BIC
                            End If
                        End If
                    Next
                    _context.SaveChanges()
                    wc.Dispose()

                    LadeListe()
                End If
            Catch ex As Exception
                MsgBox("Fehler beim Verbuchen" & ex.Message)
                If Not IsNothing(wc) Then wc.Dispose()
            End Try
        End Sub

        Private Sub CSVExport()
            Dim writer As StreamWriter = Nothing
            Dim writeropen As Boolean
            Dim strSep As String = ";"



            Try


                Dim myfn As String = "Export_" & Year(Now).ToString & Month(Now).ToString & Day(Now).ToString



                Dim dlg As New DialogService
                Dim fn = dlg.SaveFile("CSV-Dateien (*.csv)|*.csv", myfn, ".csv")
                If fn Is Nothing Then
                    Exit Sub
                End If

                IsBusy = True
                writer = New StreamWriter(fn, False, System.Text.Encoding.UTF8)
                writeropen = True

                'Zusammenstellen der Buchungen
                'OCLSV.OrderBy(Function(o) o.CustomerID)

                Dim header As String = CSVEncode("Datum") & strSep & CSVEncode("Buchungsnummer") & strSep &
                    CSVEncode("Betrag") & strSep & CSVEncode("Nachname") & strSep & CSVEncode("Vorname") & strSep &
                    CSVEncode("Straße") & strSep & CSVEncode("Hausnummer") & strSep &
                    CSVEncode("PLZ") & strSep & CSVEncode("Ort") & strSep &
                    CSVEncode("Buchungstext") & strSep &
                    CSVEncode("Kontoinhaber") & strSep &
                    CSVEncode("IBAN") & strSep &
                    CSVEncode("BIC") & strSep &
                    CSVEncode("Kostenstelle")

                writer.WriteLine(header)
                Dim i As Long = 1
                'Dim dblSumKonto As Double
                'Dim dblSumBLZ As Double
                Dim decSumBetrag As Decimal
                For Each itm In OCLSV
                    Dim line As String = ""
                    Dim vwz = itm.Verwendungszweck
                    If Not Trim(VWZText) = "" Then vwz = vwz & " " & Trim(VWZText)

                    line = CSVEncode(itm.Faelligkeit.ToShortDateString) & strSep &
                    CSVEncode(itm.BillingID & "-" & itm.CustomerID) & strSep &
                    CSVEncode(Decimal.Round(itm.Brutto, 2, MidpointRounding.AwayFromZero)) & strSep &
                    CSVEncode(itm.Customer.Nachname) & strSep &
                    CSVEncode(itm.Customer.Vorname) & strSep &
                    CSVEncode(itm.Customer.Strasse) & strSep &
                    " " & strSep &
                    CSVEncode(itm.Customer.PLZ) & strSep &
                    CSVEncode(itm.Customer.Ort) & strSep &
                    CSVEncode(vwz) & strSep &
                    CSVEncode(itm.Customer.Kontoinhaber) & strSep &
                    CSVEncode(Trim(itm.Customer.IBAN)) & strSep &
                    CSVEncode(Trim(itm.Customer.BIC)) & strSep &
                    CSVEncode(itm.Costcenter.Costcentername)

                    'dblSumKonto = dblSumKonto + itm.Customer.Kontonummer
                    'dblSumBLZ = dblSumBLZ + itm.Customer.BLZ
                    decSumBetrag = decSumBetrag + itm.Brutto
                    i = i + 1
                    writer.WriteLine(line)
                Next
                If writeropen Then writer.Close()
                IsBusy = False

                Verbuchen("", "0", "0", i - 1, 0, 0, decSumBetrag)


            Catch ex As Exception
                MsgBox(ex.Message)
                If writeropen Then writer.Close()
            End Try


        End Sub

        ' encode function preserves commas in data fields
        Function CSVEncode(ByVal value As Object) As String
            If Not IsNothing(value) Then
                Return """" & CStr(value) & """"
            Else
                Return """" & " " & """"
            End If

        End Function


        '' SEPA erstellen

        Private Sub SEPAExport()
            If SelectedType = 0 Then
                SEPAExportLS()
            ElseIf SelectedType = 1 Then
                SepaUeberweisung()
            End If
        End Sub

        Private Sub SEPAExportLS()

            Dim Empfaengername As String = ""
            Dim EmpfaengerIBAN As String = ""
            Dim EmpfaengerBIC As String = ""
            Dim EmpfaengerGlaeubigerId As String = ""
            Dim prefix = String.Empty

            prefix = GlobalSettingService.HoleEinstellung("client.erefprefix")


            ' Dateiname festlegen und Datei erstellen
            Dim myfn As String = "Export_" & Now.ToString("yyyyMMdd")



            Dim dlg As New DialogService
            Dim fn = dlg.SaveFile("XML-Dateien (*.xml)|*.xml", myfn, ".xml")
            If fn Is Nothing Then
                Return
            End If

            Empfaengername = dtaTestUmlaute(SelectedAccount.Kontoinhaber)
            EmpfaengerIBAN = SelectedAccount.IBAN
            EmpfaengerBIC = SelectedAccount.BIC
            EmpfaengerGlaeubigerId = SelectedAccount.GlaeubigerID

            If Not IBAN.IbanConverter.checkIban(EmpfaengerIBAN) Then
                dlg.ShowAlert("IBAN", "Ungültige Empfänger IBAN in der Accounts Tabelle")
                Return
            End If
            If Len(EmpfaengerBIC) < 8 Then
                dlg.ShowAlert("BIC", "Ungültige Empfänger BIC in der Accounts Tabelle")
                Return
            End If

            If Len(EmpfaengerGlaeubigerId) <> 18 Then
                dlg.ShowAlert("Gläubiger ID", "Ungültige Gläubiger ID in der Accounts Tabelle")
                Return
            End If

            Dim sepa As New in1SEPA.Sepa
            Dim sepamode = GlobalSettingService.HoleEinstellung("SepaMode", False)
            If IsNothing(sepamode) Then
                Return
            End If


            Dim ausfuehrungsdatum As DateTime = Faelligkeit
            Dim days As Long
            Dim vorlaufdays As Long
            If CInt(sepamode) = 2 Then
                vorlaufdays = 2
            Else
                vorlaufdays = 5
            End If

            'Neu ab November 2016
            '---------------------------------------------------------------
            ' Keine COR1 Lastschriften mehr
            ' FRST nur noch optional, kann immer als RCUR gesendet werden
            ' Vorlauffrist nur noch 1 Tag
            ' Einstellungen aus den Settings werden überschrieben
            vorlaufdays = 1
            sepamode = "0"
            Dim intMandatstyp As Integer = 1 'RCUR


            '---------------------------------------------------------------

            days = 0

            Dim earliestDate As Date
            earliestDate = Now

            Do While days < vorlaufdays
                If Weekday(earliestDate, vbMonday) < 6 Then
                    days = days + 1
                End If
                earliestDate = DateAdd("d", 1, earliestDate)
            Loop

            If DateDiff("d", earliestDate, ausfuehrungsdatum) < 0 Then
                ausfuehrungsdatum = earliestDate
            End If
            sepa.Init(Empfaengername, EmpfaengerIBAN, EmpfaengerBIC, EmpfaengerGlaeubigerId, fn, Val(sepamode))

            'Zusammenstellen der Buchungen
            ' OCLSV.OrderBy(Function(o) o.CustomerID)

            'Gleiche Rechnungen zu einer gemeinsamen EREF
            Dim oldid As Integer = 0
            Dim oldendtoendid As String = String.Empty

            For Each itm In OCLSV.OrderBy(Function(o) o.CustomerID)
                If Not oldid = itm.CustomerID Then
                    oldendtoendid = prefix & "-" & itm.CustomerID & "-" & itm.BillingID
                End If
                itm.EndToEnd = oldendtoendid
                oldid = itm.CustomerID
            Next

            Dim result = From c In OCLSV
                         Group c By c.CustomerID, c.Customer.Kontoinhaber, c.Customer.Strasse, c.Customer.PLZ, c.Customer.Ort, c.Customer.IBAN, c.Customer.BIC, c.EndToEnd, c.Customer.Mandatsdatum, c.Customer.LetzterEinzug, c.Customer.Mandatstyp, c.Customer.Mandatscounter Into Group
                         Select CustomerID, Kontoinhaber, Strasse, PLZ, Ort, IBAN, BIC, EndToEnd, Mandatsdatum, LetzterEinzug, Mandatstyp, Mandatscounter, Summe = Group.Sum(Function(o) o.Brutto), VWZ1 = String.Join(" ", Group.Select(Function(a) a.Verwendungszweck))




            'Dim result = OCLSV

            Dim sumC As Double = 0
            Dim anzC As Integer = 0
            Dim mdatum As Date


            For Each itm In result
                If itm.Mandatsdatum Is Nothing OrElse DateDiff(DateInterval.Day, CType(itm.Mandatsdatum, Date), Now) < 0 Then
                    mdatum = DateAdd(DateInterval.Day, -5, Now)
                Else
                    mdatum = itm.Mandatsdatum
                End If



                ' Zahlungsinformationen aus dem Customer
                Dim myiban = itm.IBAN.Trim.Replace(" ", "").ToUpper
                Dim mybic = itm.BIC.Trim.Replace(" ", "").ToUpper

                ' Evtl. andere Zahlungsinfo bei Rechnung hinterlegt?
                'If Not IsNothing(itm.PaymentInformation) AndAlso Not Trim(itm.PaymentInformation) = "" Then
                '    'Versuch ob IBAN und BIC
                '    Dim pi = Split(itm.PaymentInformation, ";")
                '    If UBound(pi) = 1 Then
                '        Dim ibancheck As New HSPinOne.Common.IBAN.IBAN_validieren(pi(0))
                '        If ibancheck.ISIBAN Then
                '            'Noch auf BIC prüfen
                '            If pi(1).ToString.Length = 8 Or pi(1).ToString.Length = 11 Then
                '                myiban = pi(0).ToString.Replace(" ", "").ToUpper
                '                mybic = pi(1).ToString.Replace(" ", "").ToUpper
                '            End If

                '        End If
                '    End If
                'End If

                Dim mref As String = itm.CustomerID.ToString & "-" & itm.Mandatscounter.ToString("D3")
                Dim vwz = itm.VWZ1
                If Not Trim(VWZText) = "" Then vwz = vwz & " " & Trim(VWZText)

                ' Änderung November 2016 Mandatstyp immer 1 RCUR
                'sepa.AddLS(itm.EndToEnd, itm.Summe, itm.Kontoinhaber, mybic, myiban, mref, mdatum, vwz, ausfuehrungsdatum, itm.Mandatstyp)
                '---------

                Dim adrline1 = ""
                Dim adrline2 = ""
                If Not myiban.StartsWith("DE") Then
                    adrline1 = itm.Strasse
                    adrline2 = itm.PLZ & " " & itm.Ort
                End If
                sepa.AddLS(itm.EndToEnd, itm.Summe, itm.Kontoinhaber, mybic, myiban, mref, mdatum, vwz, ausfuehrungsdatum, intMandatstyp, adrline1, adrline2)
                anzC = anzC + 1
                sumC = sumC + itm.Summe
            Next
            sepa.CreateXML()
            Dim msgid As String = sepa.MessageID

            ' Ausdruck Begleitzettel
            Dim doc = New Templates.SEPABegleitzettel(fn)
            doc.CreateDocument()
            doc = Nothing

            Verbuchen(Empfaengername, EmpfaengerIBAN, EmpfaengerBIC, anzC, 0, 0, sumC, msgid)



        End Sub

        Private Sub SepaUeberweisung()
            Dim Empfaengername As String = ""
            Dim EmpfaengerIBAN As String = ""
            Dim EmpfaengerBIC As String = ""
            Dim EmpfaengerGlaeubigerId As String = ""

            Empfaengername = dtaTestUmlaute(SelectedAccount.Kontoinhaber)
            EmpfaengerIBAN = SelectedAccount.IBAN
            EmpfaengerBIC = SelectedAccount.BIC
            EmpfaengerGlaeubigerId = SelectedAccount.GlaeubigerID

            Dim dlg As New DialogService

            If Not IBAN.IbanConverter.checkIban(EmpfaengerIBAN) Then
                dlg.ShowAlert("IBAN", "Ungültige Empfänger IBAN in der Accounts Tabelle")
                Return
            End If
            If Len(EmpfaengerBIC) < 8 Then
                dlg.ShowAlert("BIC", "Ungültige Empfänger BIC in der Accounts Tabelle")
                Return
            End If


            ' Dateiname festlegen und Datei erstellen
            Dim myfn As String = "Export_" & Now.ToString("yyyyMMdd")
            Dim fn = dlg.SaveFile("XML-Dateien (*.xml)|*.xml", myfn, ".xml")
            If fn Is Nothing Then
                Return
            End If

            Dim exectndate As DateTime = DateTime.Now

            Dim sepa As New in1SEPA.SCT
            sepa.Init(Empfaengername, EmpfaengerIBAN, EmpfaengerBIC, exectndate, fn)

            Dim sumC As Double = 0
            Dim anzC As Integer = 0
            For Each itm In OCLSV
                Dim myiban = itm.Customer.IBAN.Trim.Replace(" ", "").ToUpper
                Dim mybic = itm.Customer.BIC.Trim.Replace(" ", "").ToUpper
                Dim betrag = itm.Brutto * -1
                sepa.AddGS(itm.BillingID.ToString(), betrag, itm.Customer.Kontoinhaber, mybic, myiban, itm.Verwendungszweck)
                anzC = anzC + 1
                sumC = sumC + betrag
            Next

            sepa.CreateXML()


            Verbuchen("Sammelüberweisung", EmpfaengerIBAN, EmpfaengerBIC, anzC, 0, 0, sumC)

            Dim rep As New Templates.SEPABegleitzettel(myfn)
            rep.CreateReportUeberweisung(Empfaengername, EmpfaengerIBAN, exectndate, sepa.GetMsgId(), sumC, anzC)
            rep = Nothing

        End Sub

        Private Function CanSepaExport()
            If Not _sepamode = 1 Then Return False
            If Not IsNothing(OCLSV) Then
                If _datenok And OCLSV.Count > 0 Then Return True
            End If

            Return False
        End Function

        Private Function Ktonrformat(strKonto As Long) As String
            Dim Laenge As Integer
            Laenge = Len(CStr(strKonto))
            Dim neu As String = ""
            If Laenge < 10 Then
                Return New String("0", 10 - Laenge) & CStr(strKonto)
            Else
                Return strKonto
            End If


        End Function

        Private Function Nullen(anz As Integer, Wert As Object, Typ As Integer) As Object
            Dim Laenge As Integer
            Dim Betrag As Object
            If Typ = 1 Then
                Betrag = CLng(Wert * 100)
            Else
                Betrag = CLng(Wert)
            End If
            Laenge = Len(CStr(Betrag))
            If Laenge < anz Then
                Return New String("0", anz - Laenge) & Betrag
            Else
                Return Betrag
            End If

        End Function


        Private Function dtaTestUmlaute(Testtext As Object) As String
            Dim i%, dc%, ZeichenAlt$, ZeichenNeu$, nc%
            Dim erlaubt = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 .,ÄÖÜß-"
            If IsNothing(Testtext) Or Trim(Testtext) = "" Then
                dtaTestUmlaute = Space(27)
                Exit Function
            End If
            dc = Len(Testtext)
            ZeichenNeu = ""
            For i = 1 To dc
                ZeichenAlt = UCase(Mid(Testtext, i, 1))
                If erlaubt.Contains(ZeichenAlt) Then
                    If ZeichenAlt = "ß" Then ZeichenAlt = "SS"
                    If ZeichenAlt = "Ä" Then ZeichenAlt = "AE"
                    If ZeichenAlt = "Ö" Then ZeichenAlt = "OE"
                    If ZeichenAlt = "Ü" Then ZeichenAlt = "UE"


                    ZeichenNeu = ZeichenNeu & ZeichenAlt
                Else
                    ZeichenNeu = ZeichenNeu & "."
                End If
            Next i
            nc = Len(ZeichenNeu)
            If nc > 27 Then
                Return Left(ZeichenNeu, 27)
            Else
                Return ZeichenNeu & Space(27 - nc)
            End If
        End Function

    End Class

    Public Class Erroritem
        Public Property Errorbezeichnung As String
    End Class


End Namespace