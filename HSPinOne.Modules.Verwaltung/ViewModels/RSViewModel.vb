﻿Imports System.Collections.ObjectModel
Imports System.ComponentModel
Imports System.Windows.Input
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Data.Entity
Imports System.Linq
Imports Prism.Regions

Namespace ViewModel


    Public Class RSViewModel
        Inherits ViewModelBase
        Implements INavigationAware


        Private _Customer As Customer
        Public Property Customer() As Customer
            Get
                Return _Customer
            End Get
            Set(ByVal value As Customer)
                _Customer = value
                RaisePropertyChanged("Customer")
            End Set
        End Property

        Private _Billings As ObservableCollection(Of Billing)
        Public Property Billings() As ObservableCollection(Of Billing)
            Get
                Return _Billings
            End Get
            Set(ByVal value As ObservableCollection(Of Billing))
                _Billings = value
                RaisePropertyChanged("Billings")
            End Set
        End Property

        Private _Eref As String
        Public Property Eref() As String
            Get
                Return _Eref
            End Get
            Set(ByVal value As String)
                _Eref = value
                RaisePropertyChanged("Eref")
            End Set
        End Property

        Private _Oamt As Double
        Public Property Oamt() As Double
            Get
                Return _Oamt
            End Get
            Set(ByVal value As Double)
                _Oamt = value
                RaisePropertyChanged("Oamt")
            End Set
        End Property

        Private _Rsdatum As Date
        Public Property Rsdatum() As Date
            Get
                Return _Rsdatum
            End Get
            Set(ByVal value As Date)
                _Rsdatum = value
                RaisePropertyChanged("Rsdatum")
            End Set
        End Property

        Private _Coamt As Double
        Public Property Coamt() As Double
            Get
                Return _Coamt
            End Get
            Set(ByVal value As Double)
                _Coamt = value
                RaisePropertyChanged("Coamt")
            End Set
        End Property

        Private _Svwz As String
        Public Property Svwz() As String
            Get
                Return _Svwz
            End Get
            Set(ByVal value As String)
                _Svwz = value
                RaisePropertyChanged("Svwz")
            End Set
        End Property

        Private _Errormsg As String
        Public Property Errormsg() As String
            Get
                Return _Errormsg
            End Get
            Set(ByVal value As String)
                _Errormsg = value
                RaisePropertyChanged("Errormsg")
            End Set
        End Property


        Public Property SearchCommand As ICommand
        Public Property BookCommand As ICommand
        Public Property OpenCustomerCommand As ICommand

        Private _context As in1Entities

        Private WithEvents _bgw As BackgroundWorker

        Private _nkgeldpackageid As Integer

        Private _prefix As String


        Public Sub New()

            SearchCommand = New RelayCommand(AddressOf Search)
            BookCommand = New RelayCommand(AddressOf BookRs, AddressOf CanBookit)
            OpenCustomerCommand = New RelayCommand(AddressOf OpenCustomerAction, AddressOf CanOpenCustomer)



        End Sub

        Public Sub Init()
            Rsdatum = Now

            Dim packageid = GlobalSettingService.HoleEinstellung("packagenkgeld")
            If Trim(packageid) = "" Then
                Throw New Exception("Keine Artikelnummer für Rücklastgebühren hinterlegt")
            Else
                _nkgeldpackageid = CType(packageid, Integer)
            End If

            _prefix = ""
            _prefix = GlobalSettingService.HoleEinstellung("client.erefprefix")

            Clear()
        End Sub

        Public Sub Clear()
            Errormsg = ""
            Eref = ""
            Oamt = 0
            Coamt = 0
            Svwz = ""
            Customer = Nothing
            Billings = Nothing
            If Not IsNothing(_context) Then _context.Dispose()
        End Sub


        Private Sub Search()
            Errormsg = ""
            IsBusy = True
            _context = New in1Entities
            If IsNothing(_bgw) Or (Not IsNothing(_bgw) AndAlso Not _bgw.IsBusy) Then
                _bgw = New BackgroundWorker()
                'AddHandler _bgw.DoWork, AddressOf Search_DoWork
                'AddHandler _bgw.RunWorkerCompleted, AddressOf Search_RunWorkerCompleted
                _bgw.RunWorkerAsync()
            End If

        End Sub

        Private Sub Search_DoWork(sender As Object, e As DoWorkEventArgs) Handles _bgw.DoWork
            Try


                If Not IsNothing(Eref) AndAlso Not Trim(Eref) = "" Then



                    Eref = Trim(Replace(Eref, "EREF+", ""))
                    'Eref = Trim(Replace(Eref, _prefix, ""))

                    Dim query = From c In _context.Billings Where c.EndToEnd = Eref _
                                                                And c.BillingStatus = BillingStates.Bezahlt _
                                                                And c.Brutto > 0 _
                                                                And Not c.Rueckbuchungsdatum.HasValue Select c

                    Billings = New ObservableCollection(Of Billing)(query)
                    If Billings.Count = 0 Then
                        Errormsg = "Zahlungen nicht gefunden"
                        Return
                    End If

                    Oamt = Billings.Sum(Function(o) o.Brutto)

                    Customer = _context.Customers.Find(Billings.First.CustomerID)

                End If
            Catch ex As Exception
                Debug.Print(ex.Message.ToString())
            End Try
        End Sub

        Private Sub Search_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles _bgw.RunWorkerCompleted
            IsBusy = False
        End Sub



        Private Sub BookRs()
            If Not IsNothing(Coamt) AndAlso Not IsNothing(Rsdatum) AndAlso Not IsNothing(Svwz) AndAlso Not IsNothing(Billings) AndAlso Billings.Count > 0 Then
                Dim bal As New Billing_Bal(MyGlobals.CurrentUsername)

                ' Kopie der ersten Rechnung für Stornogebühren
                Dim bill As New Billing
                If Not IsNothing(Billings) AndAlso Billings.Count > 0 Then
                    bill = Billings.First
                End If

                For Each itm In Billings
                    bal.Ruecklastschrift(itm.BillingID, Rsdatum, Svwz)

                Next

                If Coamt > 0 Then
                    'bal.AddBillingFromPackage(bill.CustomerID, _nkgeldpackageid, bill.AccountID, bill.PaymentMethodID, 0, Rsdatum)
                    bal.Ruecklastgebuehr(bill, _nkgeldpackageid, Coamt, Rsdatum)
                End If
                Clear()

            End If
        End Sub

        Private Sub Bookit()
            If Not IsNothing(Coamt) AndAlso Not IsNothing(Rsdatum) AndAlso Not IsNothing(Svwz) AndAlso Not IsNothing(Billings) AndAlso Billings.Count > 0 Then
                Dim repo As New Repositories.BillingRepository(_context)
                Dim bal As New Billing_Bal(MyGlobals.CurrentUsername)

                Dim bill As New Billing

                ' Kopie der ersten Rechnung für Stornogebühren
                If Not IsNothing(Billings) AndAlso Billings.Count > 0 Then
                    bill = Billings.First
                End If

                For Each itm In Billings

                    ' Neue Rechnung aus Original anlegen
                    Dim neuoffen = repo.GetClone(itm.BillingID)
                    'Buchungsdatum löschen
                    neuoffen.Buchungsdatum = Nothing
                    'Status setzen
                    neuoffen.BillingStatus = BillingStates.Offen
                    neuoffen.DtausID = Nothing
                    neuoffen.EndToEnd = Nothing

                    'Zum Context
                    _context.Billings.Add(neuoffen)


                    ' Minusrechnung erstellen
                    Dim minus = repo.GetClone(itm.BillingID)
                    minus.Brutto = minus.Brutto * -1
                    minus.Buchungsdatum = Rsdatum
                    minus.BillingStatus = BillingStates.Bezahlt
                    minus.DtausID = Nothing
                    minus.IsStorno = True
                    minus.EndToEnd = Nothing
                    minus.Verwendungszweck = "RS: " & minus.Verwendungszweck
                    minus.Bemerkung = "RS-Grund: " & Svwz
                    _context.Billings.Add(minus)


                    'RS Datum setzen auf Original
                    itm.Rueckbuchungsdatum = Rsdatum
                    itm.IsStorno = True
                Next

                ' Wenn Gebühren, dann Minusrechnung Gebühren
                ' Neue Rechnung anlegen mit Stornogebühr

                If Coamt > 0 AndAlso Not IsNothing(bill) Then

                    ' Artikel und Preise holen
                    Dim packageprice = _context.PackagePrices.Include(Function(o) o.Package).Where(Function(o) o.PackageID = _nkgeldpackageid And o.CustomerStateID = bill.CustomerStateID).FirstOrDefault()
                    If IsNothing(packageprice) Then
                        Throw New Exception("Preis für die Bankgebühren nicht zu ermitteln")
                        Return
                    End If

                    'Offene Gebührenrechnung beim Kunden anlegen
                    Dim rs2 As New Billing
                    rs2.CustomerID = bill.CustomerID
                    rs2.IsStorno = False
                    rs2.CustomerStateID = bill.CustomerStateID
                    rs2.ProductTypeID = packageprice.Package.ProductTypeID
                    rs2.PackageID = packageprice.PackageID
                    rs2.CostcenterID = bill.CostcenterID
                    rs2.PaymentMethodID = bill.PaymentMethodID
                    rs2.AccountID = bill.AccountID
                    'rs.AccountID = SelectedItem.AccountID
                    rs2.BillingStatus = BillingStates.Offen
                    rs2.Verwendungszweck = packageprice.Package.PackageName
                    rs2.Rechnungsdatum = Now
                    rs2.Faelligkeit = bill.Faelligkeit
                    rs2.Brutto = Coamt
                    rs2.Steuerprozent = packageprice.TaxPercent

                    rs2.KSTextern = packageprice.KSTextern
                    rs2.Innenauftrag = packageprice.Innenauftrag
                    rs2.Sachkonto = packageprice.Sachkonto

                    _context.Billings.Add(rs2)
                End If

                _context.SaveChanges(MyGlobals.CurrentUsername)
                Clear()

            End If
        End Sub

        Public Function CanBookit() As Boolean
            If Not IsNothing(Coamt) AndAlso Not IsNothing(Rsdatum) AndAlso Not IsNothing(Svwz) AndAlso Not IsNothing(Billings) AndAlso Billings.Count > 0 Then
                Return True
            End If
            Return False
        End Function

        Private Sub OpenCustomerAction()
            If Not IsNothing(Customer) Then
                Dim svc = New CustomerService
                svc.ShowCustomer(Customer)
            End If
        End Sub

        Private Function CanOpenCustomer() As Boolean
            If Not IsNothing(Customer) Then Return True
            Return False
        End Function


        Public Overrides Sub OnNavigatedTo(navigationContext As NavigationContext)
            MyBase.OnNavigatedTo(navigationContext)
            Init()
        End Sub
    End Class
End Namespace
