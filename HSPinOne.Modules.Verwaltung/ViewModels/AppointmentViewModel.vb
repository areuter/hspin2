﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Input
Imports System.Data.Entity

Namespace ViewModel


    Public Class AppointmentViewModel
        Inherits ViewModelBase

        Private _context As in1Entities


        Public Event RequestClose As EventHandler
        Public DialogResult As Boolean = False

        Private _locations As IEnumerable(Of Location)
        Public Property Locations As IEnumerable(Of Location)
            Get
                Return _locations
            End Get
            Set(value As IEnumerable(Of Location))
                _locations = value
            End Set
        End Property

        Private _SelectedAppointment As Appointment
        Public Property SelectedAppointment As Appointment
            Get
                Return _SelectedAppointment
            End Get
            Set(value As Appointment)
                _SelectedAppointment = value
                RaisePropertyChanged("SelectedAppointment")
            End Set
        End Property

        Private _SelectedStaff As AppointmentStaff
        Public Property SelectedStaff As AppointmentStaff
            Get
                Return _SelectedStaff
            End Get
            Set(value As AppointmentStaff)
                _SelectedStaff = value
                RaisePropertyChanged("SelectedStaff")
            End Set
        End Property


        Public Property OKCommand As ICommand
        Public Property CancelCommand As ICommand
        Public Property AddStaffCommand As ICommand
        Public Property DeleteStaffCommand As ICommand


        Private _lcontext As in1Entities


        Private _newrecord As Boolean = False
        Private _shouldsave As Boolean = False

        Public Sub CloseButtonClicked()
            If Not _shouldsave Then Cancel()
        End Sub

        Public Sub New(app As Appointment, context As in1Entities, newrecord As Boolean)

            _context = context


            SelectedAppointment = app
            _newrecord = newrecord

            Locations = _context.Locations.Where(Function(o) o.Active = True).ToList

            ' Add any initialization after the InitializeComponent() call.



            OKCommand = New RelayCommand(AddressOf OK, AddressOf CanOK)
            CancelCommand = New RelayCommand(AddressOf Cancel)
            DeleteStaffCommand = New RelayCommand(AddressOf DeleteStaff)
            AddStaffCommand = New RelayCommand(AddressOf AddStaff)

        End Sub

        Private Sub DeleteStaff()
            If Not IsNothing(SelectedStaff) Then
                Dim dlg As New DialogService
                If dlg.AskConfirmation("Löschen", "Wollen Sie wirklich den Übungsleiter entfernen?") Then
                    _context.AppointmentStaffs.Remove(SelectedStaff)
                End If
            End If
        End Sub

        Private Sub AddStaff()
            Dim cs As New HSPinOne.Common.SearchCustomer
            Dim cust = cs.GetCustomer
            If Not IsNothing(cust) Then
                Dim ncust = _context.Customers.Find(cust.CustomerID)
                '_context.Customers.Attach(cust)
                Dim staff As New AppointmentStaff
                staff.Customer = ncust
                staff.StdLohn = 0
                SelectedAppointment.AppointmentStaffs.Add(staff)

                ' Check if Customer already exists in CourseStaff
                Dim staffcount = (From c In SelectedAppointment.Course.CourseStaffs Where c.CourseID = SelectedAppointment.CourseID And c.CustomerID = ncust.CustomerID).Count

                If staffcount = 0 Then
                    Dim cstaff As New CourseStaff
                    cstaff.CourseID = SelectedAppointment.CourseID
                    cstaff.Customer = ncust
                    cstaff.IsDeputy = True
                    cstaff.StdLohn = 0
                    SelectedAppointment.Course.CourseStaffs.Add(cstaff)
                End If

            End If
        End Sub


        Private Sub OK()


            _shouldsave = True

            DialogResult = True
            RaiseEvent RequestClose(Me, Nothing)

        End Sub


        Private Function CanOK() As Boolean
            If Not SelectedAppointment.HasErrors Then Return True
            Return False
        End Function

        Private Sub Cancel()


            Dim entries = _context.ChangeTracker.Entries.Where(Function(o) o.State = Entity.EntityState.Added Or o.State = Entity.EntityState.Deleted Or o.State = Entity.EntityState.Modified)

           


            If Not IsNothing(entries) Then
                For Each entry In entries
                    Dim currentEntry = _context.Entry(entry.Entity)

                    Dim ap = TryCast(entry.Entity, Appointment)
                    If Not IsNothing(ap) Then
                        If ap.AppointmentID = SelectedAppointment.AppointmentID Then Undo(currentEntry)
                    End If

                    Dim st = TryCast(entry.Entity, AppointmentStaff)
                    If Not IsNothing(st) Then
                        If st.AppointmentID = SelectedAppointment.AppointmentID Then Undo(currentEntry)
                    End If


           

                Next
            End If

            RaiseEvent RequestClose(Me, Nothing)
        End Sub


        Private Sub Undo(ByVal obj As System.Data.Entity.Infrastructure.DbEntityEntry)

            Dim entry = obj
            Dim currentEntry = _context.Entry(obj.Entity)
            If (entry.State = EntityState.Modified) Then
                currentEntry.CurrentValues.SetValues(entry.OriginalValues)
                Dim dbValue = currentEntry.GetDatabaseValues()
                currentEntry.OriginalValues.SetValues(dbValue)
                currentEntry.State = EntityState.Unchanged
            ElseIf (entry.State = EntityState.Deleted) Then
                Dim dbValue = currentEntry.GetDatabaseValues()
                currentEntry.OriginalValues.SetValues(dbValue)
                currentEntry.State = EntityState.Unchanged
            ElseIf (entry.State = EntityState.Added) Then
                currentEntry.State = EntityState.Detached
            End If
        End Sub

        Protected Overrides Sub Finalize()

            MyBase.Finalize()

        End Sub
    End Class
End Namespace