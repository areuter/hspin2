﻿Imports System.Windows
Imports HSPinOne.DAL
Imports System.Data.Entity
Imports System.Collections.ObjectModel
Imports HSPinOne.Infrastructure
Imports HSPinOne.Common
Imports System.Windows.Data
Imports System.Windows.Input

Namespace ViewModel




    Public Class ContractViewModel
        Inherits ViewModelBase


        Dim vertrag As New Contract
        Dim myView As ListCollectionView
        Dim _context As in1Entities

        Public Event RequestClose As EventHandler


        Private _tarife As ObservableCollection(Of ContractRate)
        Private _paymentmethods As ObservableCollection(Of PaymentMethod)
        Private _contract As Contract
        Private _dialogresult As Boolean


        Public Property Tarife As ObservableCollection(Of ContractRate)
            Get
                Return _tarife
            End Get
            Set(ByVal value As ObservableCollection(Of ContractRate))
                _tarife = value
            End Set
        End Property

        Private _selectedtarif As ContractRate
        Public Property SelectedTarif As ContractRate
            Get
                Return _selectedtarif
            End Get
            Set(ByVal value As ContractRate)
                _selectedtarif = value
                RaisePropertyChanged("SelectedTarif")
                TarifChanged()
            End Set
        End Property

        Public Property PaymentMethods As ObservableCollection(Of PaymentMethod)
            Get
                Return _paymentmethods
            End Get
            Set(ByVal value As ObservableCollection(Of PaymentMethod))
                _paymentmethods = value
            End Set
        End Property

        Public Property Contract As Contract
            Get
                Return _contract
            End Get
            Set(ByVal value As Contract)
                _contract = value
                RaisePropertyChanged("Contract")
            End Set
        End Property

        Public Property DialogResult As Boolean
            Get
                Return _dialogresult
            End Get
            Set(ByVal value As Boolean)
                _dialogresult = value
            End Set
        End Property

        Private _canedit As Boolean = False
        Public Property CanEdit As Boolean
            Get
                Return _canedit
            End Get
            Set(ByVal value As Boolean)
                _canedit = value
                RaisePropertyChanged("CanEdit")
            End Set
        End Property

        Private _CostcenterLookup As IEnumerable(Of Costcenter)
        Public Property CostcenterLookup As IEnumerable(Of Costcenter)
            Get
                Return _CostcenterLookup
            End Get
            Set(value As IEnumerable(Of Costcenter))
                _CostcenterLookup = value
            End Set
        End Property

        Private _Start As DateTime = Now
        Public Property Start As DateTime
            Get
                Return _Start
            End Get
            Set(value As DateTime)
                _Start = value
                CalculateTSEnd()
                RaisePropertyChanged("Start")
            End Set
        End Property

        Private _Monate As Integer = 1
        Public Property Monate As Integer
            Get
                Return _Monate
            End Get
            Set(value As Integer)
                _Monate = value
                CalculateTSEnd()
                RaisePropertyChanged("Monate")
            End Set
        End Property

        Private _Ende As DateTime
        Public Property Ende As DateTime
            Get
                Return _Ende
            End Get
            Set(value As DateTime)
                _Ende = value
                RaisePropertyChanged("Ende")
            End Set
        End Property

        Private _Grund As String
        Public Property Grund As String
            Get
                Return _Grund
            End Get
            Set(value As String)
                _Grund = value
                RaisePropertyChanged("Grund")


            End Set
        End Property

        Private _SelectedTimestop As ContractTimestop
        Public Property SelectedTimestop As ContractTimestop
            Get
                Return _SelectedTimestop
            End Get
            Set(value As ContractTimestop)
                _SelectedTimestop = value
            End Set
        End Property





        Public Property CancelCommand As ICommand
        Public Property SaveContractCommand As ICommand
        Public Property KuendigenCommand As ICommand
        Public Property TarifCommand As ICommand
        Public Property KuendMailCommand As ICommand
        Public Property AddTimestopCommand As ICommand
        Public Property DelTimestopCommand As ICommand
        Public Property MailTimestopCommand As ICommand
        Public Property HistoryCommand As ICommand



        Private _copy As Contract

        Private _gekuendigt As Boolean = False
        Private _customerstateid As Integer



        Public Sub New(ByVal v As Contract, ByVal customerStateID As Integer, ByVal _c As in1Entities)

            _context = _c
            _context.ContractRates().Load()
            Dim cr = From c In _context.ContractRates.Include(Function(o) o.ContractRatePrices) Where c.IsActive = True Select c





            _context.PaymentMethods().Load()
            PaymentMethods = _context.PaymentMethods.Local

            CostcenterLookup = (From c In _context.Costcenters).ToList

            _customerstateid = customerStateID

            Contract = v
            Tarife = New ObservableCollection(Of ContractRate)(cr.ToList())

            If Contract.NewRecord Then
                CanEdit = True
                Contract.CostcenterID = 0
                Contract.ContractRate = Tarife.FirstOrDefault
                Contract.CostcenterID = CostcenterLookup.First.CostcenterID
            End If



            _copy = _context.Entry(v).CurrentValues.ToObject


            If Not IsNothing(Contract.ContractRate) Then
                SelectedTarif = Contract.ContractRate
            End If

            Start = Now


            CancelCommand = New RelayCommand(AddressOf Cancel)
            SaveContractCommand = New RelayCommand(AddressOf SaveContract, AddressOf CanSaveContract)
            KuendigenCommand = New RelayCommand(AddressOf Kuendigen, AddressOf CanKuendigen)
            TarifCommand = New RelayCommand(AddressOf TarifChanged)
            KuendMailCommand = New RelayCommand(AddressOf KuendMail, AddressOf CanKuendMail)
            KuendigenCommand = New RelayCommand(AddressOf Kuendigen, AddressOf CanKuendigen)
            AddTimestopCommand = New RelayCommand(AddressOf AddTimestop, AddressOf CanAddTimestop)
            DelTimestopCommand = New RelayCommand(AddressOf DelTimestop, AddressOf CanDelTimestop)
            MailTimestopCommand = New RelayCommand(AddressOf MailTimestop, AddressOf CanDelTimestop)
            HistoryCommand = New RelayCommand(AddressOf ShowHistory)

            If Not IsNothing(Contract.Kuendigungzum) AndAlso IsDate(Contract.Kuendigungzum) Then
                _gekuendigt = True
            End If

        End Sub



        Private Sub TarifChanged()
            Contract.ContractRate = SelectedTarif
            If Contract.NewRecord Then


                Contract.Monate = SelectedTarif.Monate
                Contract.Verlaengerungsmonate = SelectedTarif.Velaengerungsmonate
                'Contract.Ratenbetrag = SelectedTarif.Ratenbetrag
                Contract.CostcenterID = SelectedTarif.CostcenterID
                Contract.Startgebuehr = SelectedTarif.Startgebuehr
                Contract.Kennzeichen = SelectedTarif.Kennzeichen

                Dim state As Integer
                state = Contract.Customer.CustomerStateID

                Dim rate = From c In SelectedTarif.ContractRatePrices Where c.CustomerStateID = state Select c

                Contract.Ratenbetrag = rate.FirstOrDefault.Preis


            End If
        End Sub



        Private Sub SaveContract()
            If Contract.NewRecord Then
                Rechnungen()
            End If
            If Not _gekuendigt Then
                Contract.Kuendigungzum = Nothing
                Contract.Kuendigungseingang = Nothing
            End If
            'If Not IsNothing(Contract.Kuendigungzum) AndAlso IsDate(Contract.Kuendigungzum) Then
            '    Kuendigen()
            'End If
            DialogResult = True
            CloseWindow()

        End Sub

        Private Function CanSaveContract() As Boolean
            If Not Contract.HasErrors Then
                Return True
            End If
            Return False
        End Function



        Private Sub Kuendigen()
            _gekuendigt = True

            Contract.Vertragsende = Contract.Kuendigungzum

            Dim bills = Contract.Billings.Where(Function(o) o.BillingStatus = BillingStates.Offen)

            For i = bills.Count - 1 To 0 Step -1
                If bills(i).Faelligkeit >= Contract.Kuendigungzum Then
                    If bills(i).BillingStatus = BillingStates.Offen Then
                        '_context.Billings.Remove(bills(i))
                        bills(i).BillingStatus = BillingStates.Storniert
                    End If
                End If
            Next

            Dim dlg As New DialogService
            dlg.ShowInfo("Kündigung", "Die Kündigung wurde aktiviert")
            dlg = Nothing

        End Sub

        Private Function CanKuendigen() As Boolean
            If Not IsNothing(Contract.Kuendigungzum) Then
                Return True
            End If
            Return False
        End Function

        Private Sub Cancel()

            Dim entry = _context.Entry(Contract)

            entry.CurrentValues.SetValues(_copy)

            CloseWindow()
        End Sub

        Private Sub CloseWindow()
            RaiseEvent RequestClose(Me, Nothing)
        End Sub



        Private Sub Rechnungen()
            'Rechnungen erstellen

            Dim i As Integer
            Dim Tarif = SelectedTarif 'CType((From c In Tarife Where c.ContractRateID = Contract.ContractRateID Select c).First, ContractRate)

            Dim rate = From c In SelectedTarif.ContractRatePrices Where c.CustomerStateID = _customerstateid Select c

            Dim acctid As Nullable(Of Integer) = rate.FirstOrDefault.AccountingTemplateID
            'Dim accounting = (From c In _context.AccountingTemplates Where c.AccountingTemplateID = rate.FirstOrDefault.AccountingTemplateID Select c).FirstOrDefault()
            Dim acctemplate = _context.AccountingTemplates.Include(Function(o) o.Tax).Where(Function(o) o.AccountingTemplateID = acctid).FirstOrDefault()

            Dim bal As New Billing_Bal(MyGlobals.CurrentUsername)

            Dim accountid = Integer.Parse(GlobalSettingService.HoleEinstellung("defaultaccount"))

            For i = 1 To Contract.Monate
                If Contract.Ratenbetrag > 0 Then  'Nur bei Verträgen mit Ratenbetrag > 0
                    'Dim rech As New Billing
                    'rech.IsStorno = False
                    'rech.Customer = Contract.Customer
                    'rech.CustomerStateID = _customerstateid
                    'rech.ProductTypeID = Contract.ContractRate.ProductTypeID
                    'rech.Contract = Contract
                    'rech.Verwendungszweck = Tarif.ContractRateName
                    'rech.Brutto = Contract.Ratenbetrag
                    'rech.CostcenterID = Contract.CostcenterID
                    'rech.Rechnungsdatum = Now
                    'rech.Faelligkeit = DateAdd(DateInterval.Month, CDbl(1 * (i - 1)), CDate(Contract.Zahlungsbeginn))
                    'rech.AccountID = GlobalSettingService.HoleEinstellung("defaultaccount")
                    'rech.PaymentMethodID = Contract.PaymentMethodID
                    'rech.BillingStatus = BillingStates.Offen

                    'rech.Sachkonto = acctemplate.Sachkonto ' rate.FirstOrDefault.Sachkonto
                    'rech.Innenauftrag = acctemplate.Innenauftrag ' rate.FirstOrDefault.Innenauftrag
                    'rech.KSTextern = acctemplate.Kostenstelle ' rate.FirstOrDefault.KSTextern
                    'rech.Steuerkennzeichen = acctemplate.SteuerkennzeichenAR ' rate.FirstOrDefault.Steuerkennzeichen
                    'rech.Steuerprozent = acctemplate.Tax.TaxPercent
                    'rech.AccountingTemplateID = acctid
                    Dim faellig As DateTime = DateAdd(DateInterval.Month, CDbl(1 * (i - 1)), CDate(Contract.Zahlungsbeginn))
                    Dim rech = bal.AddBillingFromContract(Contract, faellig, rate.FirstOrDefault, Tarif.ContractRateName, acctemplate, accountid)
                    _context.Billings.Add(rech)


                    'Contract.Customer.Billings.Add(rech)
                    'Contract.Billings.Add(rech)
                End If
            Next

            If Not IsNothing(Contract.Startgebuehr) AndAlso Contract.Startgebuehr > 0 Then
                Dim rech As New Billing
                rech.IsStorno = False
                rech.Customer = Contract.Customer
                rech.CustomerStateID = _customerstateid
                rech.ProductTypeID = Contract.ContractRate.ProductTypeID
                rech.Contract = Contract
                rech.Verwendungszweck = "Startpaket"
                rech.Brutto = Contract.Startgebuehr
                rech.BruttoOriginal = rech.Brutto
                rech.CostcenterID = Contract.CostcenterID
                rech.Rechnungsdatum = Now
                rech.Faelligkeit = CDate(Contract.Zahlungsbeginn)
                rech.AccountID = GlobalSettingService.HoleEinstellung("defaultaccount")
                rech.PaymentMethodID = Contract.PaymentMethodID
                rech.BillingStatus = BillingStates.Offen
                'Contract.Customer.Billings.Add(rech)
                'Contract.Billings.Add(rech)

                rech.Sachkonto = acctemplate.Sachkonto ' rate.FirstOrDefault.Sachkonto
                rech.Innenauftrag = acctemplate.Innenauftrag ' rate.FirstOrDefault.Innenauftrag
                rech.KSTextern = acctemplate.Kostenstelle ' rate.FirstOrDefault.KSTextern
                rech.Steuerkennzeichen = acctemplate.SteuerkennzeichenAR ' rate.FirstOrDefault.Steuerkennzeichen
                rech.Steuerprozent = acctemplate.Tax.TaxPercent
                rech.AccountingTemplateID = acctid

                _context.Billings.Add(rech)
            End If
            'vertrag.Rechnungserstellt = Now

        End Sub

        Private Sub KuendMail()
            Dim mail As New clsMail
            Dim Templatename As String = "Kuendigung"
            Dim template As Template = _context.Templates.First(Function(o) o.TemplatetypeID = 5)
            If IsNothing(template) Then
                Dim dlg As New DialogService
                dlg.ShowError("Fehler", "Eine E-Mailvorlage >>Kuendigung<< existiert nicht. Bitte benachrichtigen Sie den Administrator.")
                Return
            End If
            Dim replacer As ReplaceMarker
            If (Not IsNothing(Contract.Customer.Mail1)) Then


                Dim text As String
                If template.IsHtml Then
                    text = template.TemplateText.Replace(Environment.NewLine, "<br>")
                Else
                    text = template.TemplateText
                End If

                Dim rpContract = New ContractReplaceObject(Contract).Output
                Dim rpCustomer = New CustomerReplaceObject(Contract.Customer).Output

                'Die Replacerlsiten zu einer zusammen fügen
                Dim list = rpContract.Concat(rpCustomer).ToDictionary(Function(kvp) kvp.Key, Function(kvp) kvp.Value)

                replacer = New ReplaceMarker(text, list)

                'Template.TemplateText = Template.TemplateText.Replace(Environment.NewLine, "<br>")
                'replacer = New ReplaceMarker(template.TemplateText)
                'With replacer
                '    .AddReplacement("briefanrede", Contract.Customer.Briefanrede)
                '    .AddReplacement("termin", Contract.Kuendigungzum)
                'End With
                replacer.DoReplacement()
                mail.Body = replacer.FinalString

                replacer.Template = template.Betreff
                replacer.DoReplacement()
                mail.Subject = replacer.FinalString

                mail.AddRecipient(Contract.Customer.Mail1)
                mail.SendinBackground()
            Else
                Dim dlg As New DialogService
                dlg.ShowError("Fehler", "Für den Kunden ist keine Email Adresse gesetzt. Eine Email kann daher nicht verschickt werden.")
                Return
            End If

        End Sub

        Private Function CanKuendMail() As Boolean
            If Not IsNothing(Contract.Kuendigungzum) Then Return True
            Return False
        End Function

        Private Sub CalculateTSEnd()
            If Not IsNothing(Start) And Not IsNothing(Monate) Then
                If Monate > 0 And IsDate(Start) Then Ende = DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, Monate, Start))
            End If
        End Sub

        Private Sub AddTimestop()
            Dim ts As New ContractTimestop
            ts.TStart = Start
            ts.TEnd = Ende
            ts.Reason = Left(Grund, 150)

            ' Checken
            If ts.TStart < Contract.Vertragsbeginn Then
                Dim dlg As New DialogService
                dlg.ShowInfo("Problem", "Der Stundungsbeginn darf nicht vor dem Vertragsbeginn liegen")
                Return
            End If

            'Rechnungen in diesem Zeitraum auf Timestop
            For Each bill In Contract.Billings
                If DateDiff(DateInterval.Day, ts.TStart, bill.Faelligkeit) >= 0 And DateDiff(DateInterval.Day, bill.Faelligkeit, ts.TEnd) > 0 Then
                    If bill.BillingStatus = BillingStates.Offen Then bill.BillingStatus = BillingStates.Storniert
                End If

            Next
            Contract.ContractTimestops.Add(ts)
            Start = Nothing
            Monate = Nothing
            Ende = Nothing
            Grund = Nothing

        End Sub

        Private Function CanAddTimestop() As Boolean
            If Not IsNothing(Start) And Not IsNothing(Ende) And Not IsNothing(Grund) Then
                If Trim(Grund) <> "" Then Return True
            End If
            Return False
        End Function

        Private Sub DelTimestop()
            If Not IsNothing(SelectedTimestop) Then

                Dim dlg As New DialogService
                If Not dlg.AskCancelConfirmation("Löschen", "Soll die Ruhezeit wirklich gelöscht werden?") = MessageBoxResult.Yes Then
                    dlg = Nothing
                    Return
                End If

                Dim ts = SelectedTimestop
                For Each bill In Contract.Billings
                    If DateDiff(DateInterval.Day, ts.TStart, bill.Faelligkeit) >= 0 And DateDiff(DateInterval.Day, bill.Faelligkeit, ts.TEnd) > 0 Then
                        If bill.BillingStatus = BillingStates.Storniert Then bill.BillingStatus = BillingStates.Offen
                    End If
                Next
                'Contract.ContractTimestops.Remove(SelectedTimestop)
                _context.ContractTimestops.Remove(ts)
                dlg = Nothing
            End If
        End Sub

        Private Function CanDelTimestop() As Boolean
            If Not IsNothing(SelectedTimestop) Then Return True
            Return False
        End Function

        Private Sub MailTimestop()
            Dim mail As New clsMail
            Dim Templatename As String = "Ruhezeit"
            Dim Template As Object = _context.Templates.Where(Function(o) o.TemplateName = Templatename).First()
            If IsNothing(Template) Then
                Dim dlg As New DialogService
                dlg.ShowError("Fehler", "Eine E-Mailvorlage >>Ruhezeit<< existiert nicht. Bitte benachrichtigen Sie den Administrator.")
                Return
            End If
            Dim body As String = ""
            Dim Replacer As ReplaceMarker

            'Template.TemplateText = Template.TemplateText.Replace(Environment.NewLine, "<br>")
            Replacer = New ReplaceMarker(Template.TemplateText)
            With Replacer
                .AddReplacement("briefanrede", Contract.Customer.Briefanrede)
                .AddReplacement("von", SelectedTimestop.TStart.ToShortDateString)
                .AddReplacement("bis", SelectedTimestop.TEnd.ToShortDateString)
            End With
            Replacer.DoReplacement()
            mail.Body = Replacer.FinalString

            Replacer.Template = Template.Betreff
            Replacer.DoReplacement()
            mail.Subject = Replacer.FinalString

            mail.AddRecipient(Contract.Customer.Mail1)
            mail.SendinBackground()


        End Sub

        Private Sub ShowHistory()
            Dim hv = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IHistoryViewer)
            hv.ShowHistory("Contract", Contract.ContractID)
        End Sub



    End Class
End Namespace