﻿Imports HSPinOne.Common
Imports HSPinOne.DAL
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports HSPinOne.Infrastructure
Imports System.Windows.Data
Imports System.Windows.Input



Namespace ViewModel


    Public Class StaffContractViewModel
        Inherits ViewModelBase

        Public Event RequestRefresh As EventHandler
        Public Event RequestClose As EventHandler


        Private _staffcontract As StaffContract
        Private _context As in1Entities
        Private _occoursestaff As ObservableCollection(Of CourseStaff)
        Private _selectedz As CourseStaff
        Private _selectednz As CourseStaff
        Private _customer As Customer

        Dim zviewsource As CollectionViewSource
        Dim nzviewsource As CollectionViewSource
        Private _zview As ListCollectionView
        Private _nzview2 As ListCollectionView

        Private _dialogresult As Boolean
        Public Property DialogResult As Boolean
            Get
                Return _dialogresult
            End Get
            Set(ByVal value As Boolean)
                _dialogresult = value
            End Set
        End Property


        Public Property StaffContract As StaffContract
            Get
                Return _staffcontract
            End Get
            Set(value As StaffContract)
                _staffcontract = value
                RaisePropertyChanged("StaffContract")
            End Set
        End Property

        Public Property ZView As ListCollectionView
            Get
                Return _zview
            End Get
            Set(value As ListCollectionView)
                _zview = value
                RaisePropertyChanged("ZView")
            End Set
        End Property

        Public Property NZView As ListCollectionView
            Get
                Return _nzview2
            End Get
            Set(value As ListCollectionView)
                _nzview2 = value
                RaisePropertyChanged("NZView")
            End Set
        End Property


        Public Property OCCourseStaff As ObservableCollection(Of CourseStaff)
            Get
                Return _occoursestaff
            End Get
            Set(value As ObservableCollection(Of CourseStaff))
                _occoursestaff = value
            End Set
        End Property

        Public Property SelectedZ As CourseStaff
            Get
                Return _selectedz
            End Get
            Set(value As CourseStaff)
                _selectedz = value
                RaisePropertyChanged("SelectedZ")
            End Set
        End Property

        Public Property SelectedNZ As CourseStaff
            Get
                Return _selectednz
            End Get
            Set(value As CourseStaff)
                _selectednz = value
                RaisePropertyChanged("SelectedNZ")
            End Set
        End Property


        Public Property AddCommand As ICommand
        Public Property DelCommand As ICommand
        Public Property SaveCommand As ICommand
        Public Property CancelCommand As ICommand
        Public Property PrintDVCommand As ICommand
        Public Property PrintBillingCommand As ICommand


        Public Sub New(ByVal context As in1Entities, ByVal dv As StaffContract)




            _context = context
            StaffContract = dv


            AfterNew()




        End Sub

        Private Sub AfterNew()
            'Laden der noch verfügbaren VAs



            _context.CourseStaffs.Where(Function(c) c.CustomerID = StaffContract.Customer.CustomerID).Load()

            OCCourseStaff = _context.CourseStaffs.Local
            _customer = StaffContract.Customer
            zviewsource = New CollectionViewSource
            nzviewsource = New CollectionViewSource
            zviewsource.Source = OCCourseStaff
            nzviewsource.Source = OCCourseStaff
            ZView = zviewsource.View
            NZView = nzviewsource.View


            ZView.Filter = New Predicate(Of Object)(AddressOf Zugeordnete)
            NZView.Filter = New Predicate(Of Object)(AddressOf NichtZugeordnete)

            AddCommand = New RelayCommand(AddressOf Add, AddressOf CanAdd)
            DelCommand = New RelayCommand(AddressOf Del, AddressOf CanDel)
            CancelCommand = New RelayCommand(AddressOf CancelAction)
            SaveCommand = New RelayCommand(AddressOf Save)
            PrintBillingCommand = New RelayCommand(AddressOf PrintBilling, AddressOf CanPrintBilling)
            PrintDVCommand = New RelayCommand(AddressOf PrintDV)
        End Sub

        Private Function Zugeordnete(ByVal item As Object) As Boolean

            Dim itm = TryCast(item, CourseStaff)
            If itm IsNot Nothing Then
                If Not IsNothing(itm.StaffContractID) Then
                    If itm.StaffContractID = StaffContract.StaffContractID Then Return True
                End If

            End If
            Return False
        End Function

        Private Function NichtZugeordnete(ByVal item As Object) As Boolean
            Dim itm = TryCast(item, CourseStaff)
            If itm IsNot Nothing Then
                If IsNothing(itm.StaffContractID) Then Return True
            End If
            Return False
        End Function

        Private Sub Add()

            If Not IsNothing(NZView.CurrentItem) Then
                Dim item = CType(NZView.CurrentItem, CourseStaff)
                StaffContract.CourseStaffs.Add(item)
                item.StaffContractID = StaffContract.StaffContractID
                ZView.Refresh()
                NZView.Refresh()
            End If

        End Sub

        Private Function CanAdd(ByVal param As Object) As Boolean
            If Not IsNothing(NZView.CurrentItem) Then Return True
            Return False
        End Function

        Private Sub Del()
            If Not IsNothing(ZView.CurrentItem) Then
                Dim item = CType(ZView.CurrentItem, CourseStaff)
                item.StaffContractID = Nothing
                ZView.Refresh()
                NZView.Refresh()
            End If

        End Sub

        Private Function CanDel(ByVal param As Object) As Boolean
            If Not IsNothing(ZView.CurrentItem) Then Return True
            Return False
        End Function

        Private Sub Save()
            DialogResult = True
            RaiseEvent RequestClose(Me, Nothing)
        End Sub
        Private Sub CancelAction()
            RaiseEvent RequestClose(Me, Nothing)
        End Sub

        Private Sub PrintDV()
            Dim rpCustomer = New CustomerReplaceObject(_customer).Output
            Dim rpStaff = New StaffReplaceObject(StaffContract).Output


            'Die Replacerlsiten zu einer zusammen fügen
            Dim list = rpCustomer.Concat(rpStaff).ToDictionary(Function(kvp) kvp.Key, Function(kvp) kvp.Value)

            Dim w As New WordAutomation(list)
            w.Show()
            StaffContract.Vertragsdruck = DateTime.Now

        End Sub

        Private Sub PrintBilling()
            Dim itm = CType(ZView.CurrentItem, CourseStaff)

            Dim st As New Stundenzettel(itm)
            st.Print()

            Dim dlg As New DialogService
            If dlg.AskCancelConfirmation("Druckmarkierung", "Soll der Stundenzettel als >>Gedruckt<< vermerkt werden?") = System.Windows.MessageBoxResult.Yes Then
                itm.Stdzetteldruck = Now
            End If

        End Sub

        Private Function CanPrintBilling(ByVal param As Object) As Boolean
            If Not IsNothing(ZView.CurrentItem) Then Return True
            Return False
        End Function

    End Class
End Namespace