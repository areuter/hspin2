﻿Imports System.ComponentModel
Imports System.ComponentModel.Composition
Imports System.Data.Entity
Imports System.Windows
Imports System.Windows.Data
Imports System.Windows.Input
Imports System.Windows.Media
Imports HSPinOne.Common
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Media.Imaging
Imports HSPinOne.Common.IBAN
Imports Prism.Regions
Imports System.Collections.ObjectModel
Imports System.Threading
Imports HSPinOne.Modules.Verwaltung.Templates


Namespace ViewModel


    <Export(GetType(CustomerViewModel)), PartCreationPolicy(CreationPolicy.Shared)> _
    Public Class CustomerViewModel
        Inherits Infrastructure.ViewModelBase


        Private _customerstatelookup As IEnumerable(Of CustomerState)
        Private _countrylookup As IEnumerable(Of Country)
        Private _genderlookup As Dictionary(Of String, String)
        Private _view As ListCollectionView
        Private _id As Long = 0
        Private _selectedvertrag As Contract
        Private _selecteddok As Document
        Private _selecteddv As StaffContract
        Private _canclose As Boolean
        Private _dialogresult As Boolean
        Private _kontocheck As Brush
        Private _sepacheck As Brush
        Private _kontoworking As Boolean
        Private _sepaworking As Boolean
        Private _statusmessage As String
        Private _bank As String
        Private _showIban As String = "Visible"
        Private _showBlz As String = "Visible"



        Dim WithEvents BGW As New BackgroundWorker
        Private WithEvents pbgw As New BackgroundWorker


        Public Event RequestClose As EventHandler


        Private _regionmanager As IRegionManager


#Region "   Properties"


        Private _context As in1Entities
        Public Property Context() As in1Entities
            Get
                Return _context
            End Get
            Set(value As in1Entities)
                _context = value
            End Set

        End Property

        Public Property CustomerStateLookup As IEnumerable(Of CustomerState)
            Get
                Return _customerstatelookup
            End Get
            Set(ByVal value As IEnumerable(Of CustomerState))
                _customerstatelookup = value
            End Set
        End Property

        Private _CustomerInstitutionLookup As List(Of CustomerInstitution)
        Public Property CustomerInstitutionLookup() As List(Of CustomerInstitution)
            Get
                'Return _CustomerInstitutionLookup
                Return HSPinOne.DAL.Listitems.GetCustomerInstitutionList()
            End Get
            Set(ByVal value As List(Of CustomerInstitution))
                _CustomerInstitutionLookup = value
            End Set
        End Property



        Public Property CountryLookup As IEnumerable(Of Country)
            Get
                Return _countrylookup
            End Get
            Set(ByVal value As IEnumerable(Of Country))
                _countrylookup = value
            End Set
        End Property

        Public Property GenderLookup As Dictionary(Of String, String)
            Get
                Return _genderlookup
            End Get
            Set(ByVal value As Dictionary(Of String, String))
                _genderlookup = value
            End Set
        End Property

        Private _RoleLookup As IEnumerable(Of Role)
        Public Property RoleLookup() As IEnumerable(Of Role)
            Get
                Return _RoleLookup
            End Get
            Set(ByVal value As IEnumerable(Of Role))
                _RoleLookup = value
            End Set
        End Property



        Public Property View As ListCollectionView
            Get
                Return _view
            End Get
            Set(ByVal value As ListCollectionView)
                _view = value
                RaisePropertyChanged("View")
            End Set
        End Property

        Private _customer As Customer
        Public Property Customer As Customer
            Get
                Return _customer
            End Get
            Set(ByVal value As Customer)
                _customer = value
                LoadCustomer()
                RaisePropertyChanged("Customer")
            End Set
        End Property

        Public Property SelectedVertrag As Contract
            Get
                Return _selectedvertrag
            End Get
            Set(ByVal value As Contract)
                _selectedvertrag = value
                RaisePropertyChanged("SelectedVertrag")
            End Set
        End Property

        Public Property SelectedDok As Document
            Get
                Return _selecteddok
            End Get
            Set(ByVal value As Document)
                _selecteddok = value
                RaisePropertyChanged("SelectedDok")
            End Set
        End Property

        Public Property SelectedDV As StaffContract
            Get
                Return _selecteddv
            End Get
            Set(ByVal value As StaffContract)
                _selecteddv = value
                RaisePropertyChanged("SelectedDV")
            End Set
        End Property

        Public Property CanClose As Boolean
            Get
                Return _canclose
            End Get
            Set(ByVal value As Boolean)
                _canclose = value
                RaisePropertyChanged("CanClose")
            End Set
        End Property

        Public Property DialogResult As Boolean
            Get
                Return _dialogresult
            End Get
            Set(ByVal value As Boolean)
                _dialogresult = value
            End Set
        End Property

        Public Property Kontocheck As Brush
            Get
                Return _kontocheck
            End Get
            Set(ByVal value As Brush)
                _kontocheck = value
                RaisePropertyChanged("Kontocheck")
            End Set
        End Property

        Public Property Sepacheck As Brush
            Get
                Return _sepacheck
            End Get
            Set(ByVal value As Brush)
                _sepacheck = value
                RaisePropertyChanged("Sepacheck")
            End Set
        End Property

        Private _IBANCheck As Brush = Brushes.Orange
        Public Property IBANCheck As Brush
            Get
                Return _IBANCheck
            End Get
            Set(value As Brush)
                _IBANCheck = value
                RaisePropertyChanged("IBANCheck")
            End Set
        End Property

        Private _IBANCheckicon As String = "Checkmark"
        Public Property IBANCheckicon As String
            Get
                Return _IBANCheckicon
            End Get
            Set(value As String)
                _IBANCheckicon = value
                RaisePropertyChanged("IBANCheckicon")
            End Set
        End Property

        Private _BICCheckicon As String = "Checkmark"
        Public Property BICCheckicon() As String
            Get
                Return _BICCheckicon
            End Get
            Set(ByVal value As String)
                _BICCheckicon = value
                RaisePropertyChanged("BICCheckicon")
            End Set
        End Property



        Private _KNCheck As Brush = Brushes.Orange
        Public Property KNCheck As Brush
            Get
                Return _KNCheck
            End Get
            Set(value As Brush)
                _KNCheck = value
                RaisePropertyChanged("KNCheck")
            End Set
        End Property

        Private _KNCheckicon As String = "Checkmark"
        Public Property KNCheckicon As String
            Get
                Return _KNCheckicon
            End Get
            Set(value As String)
                _KNCheckicon = value
                RaisePropertyChanged("KNCheckicon")
            End Set
        End Property

        Public Property KontoWorking As Boolean
            Get
                Return _kontoworking
            End Get
            Set(ByVal value As Boolean)
                _kontoworking = value
                RaisePropertyChanged("KontoWorking")
            End Set
        End Property

        Public Property SepaWorking As Boolean
            Get
                Return _sepaworking
            End Get
            Set(ByVal value As Boolean)
                _sepaworking = value
                RaisePropertyChanged("SepaWorking")
            End Set
        End Property

        Public Property Statusmessage As String
            Get
                Return _statusmessage
            End Get
            Set(ByVal value As String)
                _statusmessage = value
                RaisePropertyChanged("Statusmessage")
            End Set
        End Property

        Public Property Bank As String
            Get
                Return _bank
            End Get
            Set(ByVal value As String)
                _bank = value
                RaisePropertyChanged("Bank")
            End Set
        End Property

        Private _teilnahmeview As ListCollectionView
        Public Property TeilnahmeView As ListCollectionView
            Get
                Return _teilnahmeview
            End Get
            Set(ByVal value As ListCollectionView)
                _teilnahmeview = value
                RaisePropertyChanged("TeilnahmeView")
            End Set
        End Property

        Private _packageview As ListCollectionView
        Public Property PackageView As ListCollectionView
            Get
                Return _packageview
            End Get
            Set(value As ListCollectionView)
                _packageview = value
                RaisePropertyChanged("PackageView")
            End Set
        End Property



        Private _FotoSource As BitmapSource
        Public Property FotoSource As BitmapSource
            Get
                Return _FotoSource
            End Get
            Set(ByVal value As BitmapSource)
                _FotoSource = value
                RaisePropertyChanged("Foto")
            End Set
        End Property

        Private _imag As BitmapSource
        Public Property Imag As BitmapSource
            Get
                Return _imag
            End Get
            Set(ByVal value As BitmapSource)
                _imag = value
                RaisePropertyChanged("Img")
            End Set
        End Property

        Private _SelectedActivity As Activity
        Public Property SelectedActivity As Activity
            Get
                Return _SelectedActivity
            End Get
            Set(value As Activity)
                _SelectedActivity = value
                RaisePropertyChanged("SelectedActivity")
            End Set
        End Property

        Private _IsActivityOpen As Boolean = False
        Public Property IsActivityOpen As Boolean
            Get
                Return _IsActivityOpen
            End Get
            Set(value As Boolean)
                _IsActivityOpen = value
                RaisePropertyChanged("IsActivityOpen")
            End Set
        End Property

        Private _Creator As User
        Public Property Creator As User
            Get
                Return _Creator
            End Get
            Set(value As User)
                _Creator = value
                RaisePropertyChanged("Creator")
            End Set
        End Property

        Private _Editor As User
        Public Property Editor As User
            Get
                Return _Editor
            End Get
            Set(value As User)
                _Editor = value
                RaisePropertyChanged("Editor")
            End Set
        End Property

        Public Property ShowIban As String
            Get
                Return _showIban
            End Get
            Set(value As String)
                _showIban = value
            End Set
        End Property

        Public Property ShowBlz As String
            Get
                Return _showBlz
            End Get
            Set(value As String)
                _showBlz = value
            End Set
        End Property

        Private _sepaType As Integer = 0
        Public Property SepaType As Integer
            Get
                Return _sepaType
            End Get
            Set(value As Integer)
                _sepaType = value
                RaisePropertyChanged("Editor")
            End Set
        End Property

        Private _Flags As ObservableCollection(Of Flag)
        Public Property Flags As ObservableCollection(Of Flag)
            Get
                Return _Flags
            End Get
            Set(value As ObservableCollection(Of Flag))
                _Flags = value
            End Set
        End Property


        Private _SelectedMeta As CustomerMeta
        Public Property SelectedMeta() As CustomerMeta
            Get
                Return _SelectedMeta
            End Get
            Set(ByVal value As CustomerMeta)
                _SelectedMeta = value
            End Set
        End Property


        Public ReadOnly Property Mandatstypen As Dictionary(Of Byte, String)
            Get
                Return New Dictionary(Of Byte, String) From {{0, "Erstlastschrift"}, {1, "Folgelastschrift"}}
            End Get
        End Property

        Private _Ausweisneu As String
        Public Property Ausweisneu() As String
            Get
                Return _Ausweisneu
            End Get
            Set(ByVal value As String)
                _Ausweisneu = value
                RaisePropertyChanged("Ausweisneu")
            End Set
        End Property


#End Region

#Region "  Command Properties"


        'Public Property DokShowCommand As ICommand
        Public Property VertragEditCommand As ICommand
        Public Property VertragNewCommand As ICommand
        Public Property VertragDeleteCommand As ICommand
        Public Property ReadCardCommand As ICommand
        'Public Property SearchCommand As ICommand
        Public Property MailCommand As ICommand
        Public Property WordCommand As ICommand
        Public Property PrintCommand As ICommand
        Public Property RechnungenCommand As ICommand
        Public Property NeuCustomerCommand As ICommand
        Public Property NachnameLostFocusCommand As ICommand
        Public Property PLZCommand As ICommand
        Public Property SaveCommand As ICommand
        Public Property SaveCloseCommand As ICommand
        Public Property CloseCommand As ICommand
        Public Property DVNewCommand As ICommand
        Public Property DVEditCommand As ICommand
        Public Property DVDelCommand As ICommand
        Public Property KontocheckCommand As ICommand
        Public Property SepacheckCommand As ICommand
        Public Property DeleteCommand As ICommand

        Public Property ImageGetCommand As ICommand

        Public Property ImageLoadCommand As ICommand

        Public Property ImageDelCommand As ICommand

        Public Property OpenActivityCommand As ICommand

        Public Property NewActivityCommand As ICommand
        Public Property DelActivityCommand As ICommand

        Public Property ShowHistoryCommand As ICommand
        Public Property NewMandateCommand As ICommand
        Public Property AddMetaCommand As ICommand
        Public Property RemoveMetaCommand As ICommand
        Public Property ValidateCardCommand As ICommand

        'Public Property AddCardCommand As New RelayCommand(AddressOf AddCard, AddressOf CanAddCard)
        'Public Property DelCardCommand As New RelayCommand(AddressOf DelCard)


        Private _Billings As ObservableCollection(Of Billing) = New ObservableCollection(Of Billing)
        Public Property Billings() As ObservableCollection(Of Billing)
            Get
                Return _Billings
            End Get
            Set(ByVal value As ObservableCollection(Of Billing))
                _Billings = value
                RaisePropertyChanged("Billings")
            End Set
        End Property

        Private WithEvents bgwB As New BackgroundWorker
#End Region

        Public Sub New()

            RegisterCommands()



        End Sub

        Private Sub LoadCustomer()


            _context.CustomerStates.Load()
            CustomerStateLookup = _context.CustomerStates.Local

            _context.Countries.Load()
            CountryLookup = _context.Countries.Where(Function(o) o.Active = 1).ToList()

            _context.Roles.Load()
            RoleLookup = _context.Roles.Local

            Creator = _context.Users.Where(Function(o) o.UserID = Customer.Erstelltvon).SingleOrDefault
            Editor = _context.Users.Where(Function(o) o.UserID = Customer.Bearbeitetvon).SingleOrDefault


            SepaType = GlobalSettingService.HoleEinstellung("SEPA")
            If Not IsNothing(SepaType) Then
                If SepaType = 0 Then
                    ShowBlz = "Visible"
                    ShowIban = "Collapsed"
                ElseIf SepaType = 1 Then
                    ShowBlz = "Collapsed"
                    ShowIban = "Visible"
                End If
            End If

            ' Init Flags
            Me.Flags = Flagsmanager.GetFlagList

            Dim sel = Flagsmanager.GetSelected(Customer.Flags)
            For Each itm In Me.Flags
                For Each el In sel
                    If itm.Flagname = el.Flagname Then itm.IsSelected = True
                Next
                AddHandler itm.OnSelectedChanged, AddressOf SyncFlags
            Next



            LoadImage()
            LoadBillings()

        End Sub

        Private Sub LoadBillings()
            If Not bgwB.IsBusy Then
                bgwB.RunWorkerAsync()
            End If
        End Sub

        Private Sub bgwB_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgwB.DoWork
            Dim repo As New Repositories.BillingRepository(_context)
            Dim data = repo.GetActiveBillings(Customer.CustomerID, 300)
            e.Result = data
        End Sub

        Private Sub bgwB_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles bgwB.RunWorkerCompleted
            If Not IsNothing(e.Result) Then
                Billings = e.Result

                TeilnahmeView = New ListCollectionView(Billings)
                TeilnahmeView.Filter = New Predicate(Of Object)(AddressOf Billingfilter)
                PackageView = New ListCollectionView(Billings)
                PackageView.Filter = New Predicate(Of Object)(AddressOf Packagefilter)

                '' Register Views
                'Dim TNSource As New CollectionViewSource
                'TNSource.Source = Billings

                'Dim PackageSource As New CollectionViewSource
                'PackageSource.Source = Billings

                'TeilnahmeView = TNSource.View


                'PackageView = PackageSource.View

                'TeilnahmeView.Refresh()
                'PackageView.Refresh()

            End If
        End Sub

        Private Sub LoadImage()




        End Sub

        Private Sub SyncFlags()
            ' Flags setzen
            Customer.Flags = Flagsmanager.SetSelected(Me.Flags)
        End Sub

        Private Function Billingfilter(ByVal item As Object) As Boolean
            Dim itm = CType(item, Billing)
            If Not IsNothing(itm) AndAlso Not IsNothing(itm.CourseID) Then
                If IsNothing(itm.IsStorno) OrElse itm.IsStorno = False Then Return True
            End If
            Return False
        End Function

        Private Function Packagefilter(ByVal item As Object) As Boolean
            Dim itm = CType(item, Billing)
            If Not IsNothing(itm) AndAlso Not IsNothing(itm.PackageID) AndAlso itm.Packagecredit > 0 Then
                If itm.IsStorno = False Then Return True
            End If
            Return False
        End Function

        Private Sub RegisterCommands()

            SaveCommand = New RelayCommand(AddressOf Save, AddressOf CanSaveExecute)
            SaveCloseCommand = New RelayCommand(AddressOf SaveClose, AddressOf CanSaveExecute)

            VertragEditCommand = New RelayCommand(AddressOf VertragEdit, AddressOf CanVertragEditorDelete)
            VertragNewCommand = New RelayCommand(AddressOf VertragNew, AddressOf CanVertragNew)
            VertragDeleteCommand = New RelayCommand(AddressOf VertragDelete, AddressOf CanVertragEditorDelete)
            ReadCardCommand = New RelayCommand(AddressOf ReadCard, AddressOf CanReadCard)
            'SearchCommand = New RelayCommand(AddressOf Search, AddressOf CanSearch)
            MailCommand = New RelayCommand(AddressOf Mail, AddressOf CanMail)
            WordCommand = New RelayCommand(AddressOf Word, AddressOf CanWord)
            PrintCommand = New RelayCommand(AddressOf Print, AddressOf CanPrint)
            RechnungenCommand = New RelayCommand(AddressOf Rechnungen, AddressOf CanRechnungen)
            PLZCommand = New RelayCommand(AddressOf PLZLostFocus)
            DVNewCommand = New RelayCommand(AddressOf DVNew)
            DVEditCommand = New RelayCommand(AddressOf DVEdit, AddressOf CanDVEdit)
            DVDelCommand = New RelayCommand(AddressOf DVDel, AddressOf CanDVDel)
            KontocheckCommand = New RelayCommand(AddressOf Kontochecken, AddressOf Cankontochecken)
            SepacheckCommand = New RelayCommand(AddressOf Sepachecken, AddressOf Cansepachecken)
            DeleteCommand = New RelayCommand(AddressOf DeleteAction, AddressOf CanDelete)

            ImageGetCommand = New RelayCommand(AddressOf ImageGet)
            ImageLoadCommand = New RelayCommand(AddressOf ImageGetFromDisk)
            ImageDelCommand = New RelayCommand(AddressOf ImageDel)

            OpenActivityCommand = New RelayCommand(AddressOf OpenActivity, AddressOf CanOpenActivity)

            NewActivityCommand = New RelayCommand(AddressOf NewActivity)
            DelActivityCommand = New RelayCommand(AddressOf DelActivity, AddressOf CanOpenActivity)

            ShowHistoryCommand = New RelayCommand(AddressOf ShowHistory)

            NewMandateCommand = New RelayCommand(AddressOf NewMandate)

            AddMetaCommand = New RelayCommand(AddressOf AddMetaAction)
            RemoveMetaCommand = New RelayCommand(AddressOf DelMetaAction)

            ValidateCardCommand = New RelayCommand(AddressOf ValidateCard)

            GenderLookup = New Dictionary(Of String, String)
            GenderLookup.Add("m", "männlich")
            GenderLookup.Add("w", "weiblich")
            GenderLookup.Add("d", "divers")

        End Sub



        Private Sub DeleteAction()

            Dim candel As Boolean = True
            Dim dlg As New DialogService
            'Hat der Kunde noch Verträge?
            If Customer.Contracts.Any Then
                dlg.ShowInfo("Verträge", "Diesem Kunden sind noch Verträge zugeordnet. Löschen nicht möglich")
                candel = False
            End If

            'Hat der Kunde Rechnungen
            If Customer.Billings.Any Then
                dlg.ShowInfo("Rechnungen", "Diesem Kunden sind Rechnungen zugeordent. Löschen nicht möglich")
                candel = False
            End If



            If candel Then
                If dlg.AskConfirmation("Löschen", "Wirklich Kunde löschen") = True Then
                    'Adressenview.Remove(Adressenview.CurrentItem)
                    Customer.Aktiv = False
                    SaveClose()
                End If
            End If
        End Sub

        Private Function CanDelete() As Boolean
            If Not Customer.HasErrors Then
                Return True
            End If
            Return False
        End Function



#Region "   Commands"
        Private Function Save() As Boolean

            Try
                UpdateFocusedField.Update()


                If Not Customer.HasErrors Then
                    'Editor = _context.Users.Where(Function(o) o.UserID = MyGlobals.CurrentUserID).SingleOrDefault

                    'Customer.Bearbeitetam = Now
                    'Customer.Bearbeitetvon = MyGlobals.CurrentUserID
                    _context.SaveChanges(MyGlobals.CurrentUsername)
                    Events.EventInstance.EventAggregator.GetEvent(Of RefreshCustomerEvent).Publish(Nothing)
                    Customer.NewRecord = False
                End If
                Return True
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            Return False

        End Function

        Private Sub SaveClose()

            Try
                If Save() = True Then
                    CloseWindow()
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try


        End Sub

        Public Sub CloseButtonClicked()
            If Not Thread.CurrentPrincipal.IsInRole("Infopoint") Then
                Dim dlg As New DialogService
                dlg.ShowAlert("Speichern", "Sie sind nicht berechtigt Änderungen an Kunden vorzunehmen. Die Änderungen werden nicht gespeichert.")
                CloseWindow()

            ElseIf _context.HasChanges Then
                Dim dlg As New DialogService
                Dim res = dlg.AskCancelConfirmation("Speichern", "Möchten Sie die Änderungen speichern?")
                Select Case res
                    Case MessageBoxResult.Yes
                        SaveClose()
                    Case MessageBoxResult.No
                        CloseWindow()
                End Select
            Else
                CloseWindow()
            End If
        End Sub

        Private Sub CloseWindow()
            CanClose = True
            DialogResult = True
            RaiseEvent RequestClose(Me, Nothing)
        End Sub


        Private Function CanSaveExecute(ByVal param As Object) As Boolean
            If Not Customer.HasErrors Then
                Return True
            End If
            Return False
        End Function

        Private Sub VertragEdit()

            'Dim Vertragview = CType(CollectionViewSource.GetDefaultView(Me.LVVertrag.ItemsSource), ListCollectionView)
            'Dim Vertragview = CollectionViewSource.GetDefaultView(Me.LVVertrag.ItemsSource)

            If SelectedVertrag IsNot Nothing Then


                Dim cs As New CustomerService
                If cs.ShowContract(_context, SelectedVertrag, Customer.CustomerStateID) = True Then
                    'SelectedVertrag.Bearbeitetam = Now
                End If



            End If
        End Sub

        Private Function CanVertragEditorDelete(ByVal param As Object) As Boolean
            If Not IsNothing(SelectedVertrag) Then
                Return True
            End If
            Return False
        End Function

        Private Sub VertragNew()


            Dim v As New Contract
            v.CustomerID = Customer.CustomerID
            v.Customer = Customer
            v.Erstelltam = DateTime.Now

            v.Vertragsbeginn = Now.Date


            v.PaymentMethodID = 2
            v.NewRecord = True
            Customer.Contracts.Add(v)
            Dim cs As New CustomerService
            If cs.ShowContract(_context, v, Customer.CustomerStateID) = True Then
                v.NewRecord = False
            Else
                If v.NewRecord Then
                    'Customer.Contracts.Remove(v)
                    Do While v.Billings.Any
                        _context.Billings.Remove(v.Billings.First)
                    Loop
                    _context.Contracts.Remove(v)
                End If

            End If



        End Sub

        Private Function CanVertragNew(ByVal param As Object) As Boolean
            If Not IsNothing(Customer.CustomerID) Then Return True
            Return False
        End Function


        Private Sub VertragDelete()


            If MessageBox.Show("Sind sie sicher, dass sie diesen Vertrag löschen möchten?", "Löschen", MessageBoxButton.YesNo) = MessageBoxResult.Yes Then
                Dim v As Contract = SelectedVertrag
                If v IsNot Nothing Then
                    'Prüfen, ob schon bezahlte rechnungen
                    Dim bez As Boolean = False
                    For Each bill In v.Billings
                        If bill.BillingStatus <> BillingStates.Offen Then bez = True
                    Next
                    If bez Then
                        MsgBox("Zu diesem Vertrag gehören schon bezahlte Rechnungen. Löschen nicht möglich. Bitte kündigen sie den Vertrag ordnungsgemäß")
                    Else
                        'Do While v.Billings.Any
                        '_context.Billings.Remove(v.Billings.First)

                        'Loop
                        For Each bill In v.Billings
                            If bill.BillingStatus = BillingStates.Offen Then
                                bill.BillingStatus = BillingStates.Storniert
                                bill.Contract = Nothing
                                bill.ContractID = Nothing
                            End If
                        Next
                        _context.Contracts.Remove(v)

                    End If
                End If
            End If

        End Sub


        'Private Sub DokumentShow(ByVal param As Object)

        '    If Not IsNothing(SelectedDok) Then
        '        'Dokviewer.Dokumentanzeigen(SelectedDok.Dateiname)
        '        Dokviewer.ShowFilestream(SelectedDok.DocumentID)
        '    End If

        'End Sub

        'Private Function CanDokumentShow(ByVal param As Object) As Boolean
        '    Return True
        'End Function

        Private Sub ReadCard(ByVal param As Object)


            Dim cls As New QuioCV6600

            Dim booUpdate As Boolean = False

            Try
                Dim decCard = cls.Karte_lesen
                If decCard > 100 Then
                    Dim dlg As New DialogService
                    Dim seccontext As New in1Entities
                    Dim Personen = From p In seccontext.Customers Where p.Kartennummer = decCard Select p

                    For Each strPerson In Personen
                        If Not strPerson.CustomerID = Customer.CustomerID Then
                            Dim res = dlg.AskCancelConfirmation("Doppelt", "Diese Karte ist bereits " & strPerson.Suchname & " zugeordnet. Soll die Karte dort gelöscht werden?")
                            If res = vbYes Then
                                strPerson.Kartennummer = vbEmpty
                                booUpdate = True
                            ElseIf res = vbCancel Then
                                dlg = Nothing
                                Return
                            End If
                        End If
                    Next
                    If booUpdate Then seccontext.SaveChanges()
                    seccontext.Dispose()
                    If IsNothing(param) Then
                        Customer.Kartennummer = decCard
                    Else
                        If param = "Multicard" Then
                            Ausweisneu = decCard
                        End If
                    End If
                    dlg = Nothing
                    End If

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

        End Sub

        Private Function CanReadCard(ByVal param As Object) As Boolean

            Return MyGlobals.QuioEnabled
        End Function

        Private Sub ValidateCard()
            If Not Customer.Kartennummer Is Nothing AndAlso Not Trim(Customer.Kartennummer) = "" Then
                Using con As New in1Entities
                    Dim Personen = From p In con.Customers Where p.Kartennummer = Customer.Kartennummer Select p.CustomerID, p.Suchname
                    If Personen.Count > 0 Then
                        Dim dlg As New DialogService
                        For Each strPerson In Personen
                            If Not strPerson.CustomerID = Customer.CustomerID Then
                                dlg.ShowError("Kartennummer", "Karte bereits vergeben an " & strPerson.Suchname & " (" & strPerson.CustomerID & ")")
                                Customer.Kartennummer = Nothing
                            End If
                        Next
                    End If
                End Using
            End If
        End Sub


        Private Sub Mail()

            Dim mail As New clsMail
            mail.Mailto(Customer.Mail1, "Hochschulsport", "Kunde: " & Customer.CustomerID & vbNewLine, Customer.CustomerID)
        End Sub

        Private Function CanMail(ByVal param As Object) As Boolean
            If Not IsNothing(Customer.Mail1) And InStr(Customer.Mail1, "@") Then
                Return True
            End If
            Return False
        End Function

        Private Sub Word()
            Dim rpCustomer = New CustomerReplaceObject(Customer).Output
            
            Dim doc As New WordAutomation(rpCustomer)
            '       doc.Customer = Customer
            doc.Show()
            doc = Nothing
        End Sub

        Private Function CanWord(ByVal param As Object) As Boolean
            If Not Customer Is Nothing Then
                Return True

            End If
            Return False
        End Function

        Private Sub Print()
            If Not IsNothing(Me.Customer) Then
                Dim rep As New CustomerReport(Me.Customer)
                rep.CreateDocument()
            End If

            'Dim printDialog As New PrintDialog
            'If printDialog.ShowDialog() = True Then
            '    printDialog.PrintVisual(Grid_Adresse, "Adresse")
            'End If

        End Sub

        Private Function CanPrint(ByVal param As Object) As Boolean
            If Not IsNothing(Me.Customer) Then Return True
            Return False
        End Function

        Private Sub Rechnungen()
            If Not IsNothing(Customer) Then
                Dim cs As New CustomerService
                cs.ShowBillings(_context, Customer)
            End If
        End Sub

        Private Function CanRechnungen(ByVal param As Object) As Boolean
            Return True
        End Function







        Private Sub DVNew()

            Dim cs As New CustomerService

            Dim dv As New StaffContract
            dv.Beginn = Now
            dv.Ende = Now
            dv.Customer = Customer
            dv.NewRecord = True
            Customer.StaffContracts.Add(dv)

            If cs.ShowStaffContract(_context, dv) = True Then
                dv.NewRecord = False
            Else
                _context.StaffContracts.Remove(dv)
            End If



        End Sub

        Private Sub DVEdit()

            If Not IsNothing(SelectedDV) Then

                Dim cs As New CustomerService
                Dim _copy As New StaffContract
                _copy = _context.Entry(SelectedDV).CurrentValues.ToObject
                If cs.ShowStaffContract(_context, SelectedDV) = True Then

                Else
                    _context.Entry(SelectedDV).CurrentValues.SetValues(_copy)

                End If

            End If

        End Sub

        Private Function CanDVEdit() As Boolean
            If Not IsNothing(SelectedDV) Then Return True
            Return False

        End Function

        Private Sub DVDel()
            _context.StaffContracts.Remove(SelectedDV)


        End Sub

        Private Function CanDVDel() As Boolean
            If Not IsNothing(SelectedDV) Then
                If Not SelectedDV.CourseStaffs.Any Then
                    Return True
                End If
            End If
            Return False
        End Function

        Private Sub ImageGet()
            If Customer.CustomerID > 0 Then
                Dim wc As New ImageGetter
                If wc.GetImage Then
                    'Customer.HasImage = True
                    Dim doc As New Document
                    doc.Dateiname = "Foto"
                    doc.DocumentType = 1
                    doc.Erstelldatum = Now
                    doc.Extension = "jpg"
                    doc.DocumentData = wc.ImageData
                    Customer.Document = doc
                End If
                wc = Nothing
            End If


        End Sub

        Private Sub ImageGetFromDisk()
            If Customer.CustomerID > 0 Then
                Dim wc As New ImageGetter
                If wc.LoadImageFromDisk Then
                    'Customer.HasImage = True
                    Dim doc As New Document
                    doc.Dateiname = "Foto"
                    doc.DocumentType = 1
                    doc.Erstelldatum = Now
                    doc.Extension = "jpg"
                    doc.DocumentData = wc.ImageData
                    Customer.Document = doc
                End If
                wc = Nothing
            End If
        End Sub

        Private Sub ImageDel()
            Customer.Document = Nothing
        End Sub

        Private Sub ShowHistory()
            If Not IsNothing(Customer.CustomerID) Then
                If Customer.CustomerID > 0 Then
                    Dim hv = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IHistoryViewer)
                    hv.ShowHistory("Customer", Customer.CustomerID)
                End If
            End If
        End Sub

        Private Sub OpenActivity()
            If Not IsNothing(SelectedActivity) Then
                Dim cs As New CustomerService
                If cs.ShowActivity(_context, SelectedActivity) = False Then
                    _context.Entry(SelectedActivity).Reload()
                End If
            End If
        End Sub

        Private Function CanOpenActivity() As Boolean
            If Not IsNothing(SelectedActivity) Then Return True
            Return False
        End Function



        Private Sub NewActivity()
            Dim act As New Activity
            act.Zeit = Now
            act.ActivityType = 4
            act.Direction = 1
            act.Customer = Customer
            act.Comment = "Hier Kommentar eintragen"
            act.UserID = MyGlobals.CurrentUserID
            act.NewRecord = True
            Customer.Activities.Add(act)
            Dim cs As New CustomerService
            If cs.ShowActivity(_context, act) = True Then
                act.NewRecord = False
            Else
                _context.Activities.Remove(act)
            End If



        End Sub

        Private Sub DelActivity()
            If Not IsNothing(SelectedActivity) Then
                _context.Activities.Remove(SelectedActivity)
            End If
        End Sub

        Private Sub Kontochecken()
            If Not IsNothing(Customer.Kontonummer) And Not IsNothing(Customer.BLZ) Then
                If Not BGW.IsBusy Then
                    Statusmessage = "Überprüfe Bankverbindung"
                    KontoWorking = True
                    BGW.RunWorkerAsync("konto")
                End If

            End If
        End Sub

        Private Sub Sepachecken()
            If Not IsNothing(Customer.IBAN) Then Customer.IBAN = Customer.IBAN.ToUpper
            If Not IsNothing(Customer.BIC) Then Customer.BIC = Customer.BIC.ToUpper

            If Not IsNothing(Customer.IBAN) Then
                If Not Trim(Customer.IBAN) = "" Then
                    Dim ciban = New IBAN_validieren(Customer.IBAN)
                    If ciban.ISIBAN() Then
                        IBANCheck = Brushes.Green
                        IBANCheckicon = "CheckmarkCheck"
                        If Not BGW.IsBusy Then
                            Statusmessage = "Überprüfe Bankverbindung"
                            SepaWorking = True
                            BGW.RunWorkerAsync("sepa")
                        End If
                        'IBANCheck = Brushes.LightGreen
                    Else
                        IBANCheck = Brushes.DarkRed
                        IBANCheckicon = "CheckmarkCross"
                    End If
                End If
            End If

        End Sub

        Private Function Cankontochecken() As Boolean
            If Not IsNothing(Customer.BLZ) And Not IsNothing(Customer.Kontonummer) Then
                Return True
            End If
            Return False

        End Function

        Private Function Cansepachecken() As Boolean
            If Not IsNothing(Customer.IBAN) Then
                Return True
            End If
            Return False

        End Function

        Private Sub GetIBAN()
            If Not IsNothing(Customer) Then
                If Not IsNothing(Customer.Kontonummer) And Not IsNothing(Customer.BLZ) Then

                    Customer.IBAN = IbanConverter.getIban(Customer.BLZ, Customer.Kontonummer)

                End If

            End If
        End Sub

        Private Sub NewMandate()

            Dim dlg As New DialogService
            If dlg.AskCancelConfirmation("SEPA-Lastschriftmandat", "Möchten sie ein neues Mandat erzeugen?") = MessageBoxResult.Yes Then
                Dim cnt As Integer = 0
                If Not IsNothing(Customer.Mandatscounter) Then
                    cnt = Customer.Mandatscounter
                End If
                Customer.Mandatscounter = cnt + 1
                Customer.Mandatstyp = 0
                Customer.LetzterEinzug = Nothing
                Customer.Mandatsdatum = Now.ToShortDateString
            End If

        End Sub

        Private Sub AddMetaAction()
            Dim meta As New CustomerMeta
            meta.Customer = Customer
            meta.MetaKey = "neuer Key"
            meta.MetaValue = ""
            Customer.CustomerMetas.Add(meta)
        End Sub

        Private Sub DelMetaAction()
            If Not IsNothing(SelectedMeta) Then
                _context.CustomerMetas.Remove(SelectedMeta)
            End If
        End Sub



        Private Sub BGW_DoWork(sender As Object, e As DoWorkEventArgs) Handles BGW.DoWork

            Dim clsi As New Kontocheck(Customer.IBAN)

            If Not IsNothing(clsi.Bankname) Then Bank = clsi.Bankname
            If clsi.Checked Then
                BICCheckicon = "CheckmarkCheck"
            Else
                BICCheckicon = "CheckmarkCross"
            End If

            If Not IsNothing(clsi.BIC) AndAlso Not Trim(clsi.BIC) = "" Then
                Customer.BIC = clsi.BIC
                'GetIBAN()
            End If
            If Not IsNothing(clsi.Errormessage) Then
                Statusmessage = clsi.Errormessage
            Else
                Statusmessage = ""
            End If
            e.Result = e.Argument


        End Sub

        Private Sub BGW_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles BGW.ProgressChanged

        End Sub

        Private Sub BGW_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles BGW.RunWorkerCompleted
            If e.Result = "sepa" Then
                SepaWorking = False
            ElseIf e.Result = "konto" Then
                KontoWorking = False
            Else
                KontoWorking = False
                SepaWorking = False
            End If
        End Sub


        Private Sub PLZLostFocus()
            If Not IsNothing(Customer.PLZ) And Len(Customer.PLZ) = 5 And IsNumeric(Customer.PLZ) Then

                If Not pbgw.IsBusy Then
                    pbgw.RunWorkerAsync()
                End If

            End If
        End Sub

        Private Sub pbgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles pbgw.DoWork
            If Not IsNothing(Customer.PLZ) Then
                Dim cls As New PlzCheck(Customer.PLZ)
                If Not IsNothing(cls.Errormessage) Then
                    Statusmessage = cls.Errormessage
                Else
                    Statusmessage = ""
                End If
                e.Result = cls.City
            End If
        End Sub

        Private Sub pbgw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles pbgw.RunWorkerCompleted
            If Not IsNothing(e.Result) Then
                Customer.Ort = e.Result
            End If
        End Sub

        'Private Sub AddCard()
        '    If Not IsNothing(Ausweisneu) And Not Trim(Ausweisneu) = "" Then
        '        If IsNumeric(Ausweisneu) Then
        '            'Check duplicate
        '            Using con As New in1Entities
        '                Dim res = con.CustomerCards.Where(Function(o) o.Kartennummer = Ausweisneu)
        '                If Not IsNothing(res) AndAlso res.Count > 0 Then
        '                    Dim dlg As New DialogService
        '                    dlg.ShowAlert("Ausweis", "Dieser Ausweis ist bereits einem anderen Kunden zugeordnet")
        '                    Return
        '                End If
        '            End Using

        '            If Customer.CustomerCards.Where(Function(o) o.Kartennummer = Ausweisneu).Count > 0 Then
        '                Return
        '            End If

        '            Dim card As New CustomerCard
        '            card.Kartennummer = Ausweisneu
        '            card.Created = Now
        '            Customer.CustomerCards.Add(card)
        '            Ausweisneu = ""
        '        End If
        '    End If
        'End Sub

        'Private Function CanAddCard()
        '    If Not IsNothing(Ausweisneu) And Not Trim(Ausweisneu) = "" Then
        '        Return True
        '    End If
        '    Return False
        'End Function

        'Private Sub DelCard(ByVal param As Object)
        '    If Not IsNothing(param) Then
        '        Dim card = TryCast(param, CustomerCard)
        '        If Not IsNothing(card) Then
        '            _context.CustomerCards.Remove(card)
        '            'Customer.CustomerCards.Remove(card)
        '        End If
        '    End If
        'End Sub

#End Region
    End Class
End Namespace
