﻿Imports System.ComponentModel.Composition
Imports System.Windows.Data
Imports System.ComponentModel
Imports HSPinOne.Common
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Input
Imports Prism.Regions
Imports System.Data.Entity
Imports System.Collections.ObjectModel
Imports CommonServiceLocator

Namespace ViewModel

    Public Class CustomersViewModel
        Inherits ViewModelBase

        Private _context As in1Entities

        Private _regionmanager As IRegionManager


#Region "   Properties"

        Private _results As IEnumerable

        Public Property Results As IEnumerable
            Get
                Return _results
            End Get
            Set(value As IEnumerable)
                _results = value
                RaisePropertyChanged("Results")
            End Set
        End Property

        Private _results2 As ObservableCollection(Of Billing)

        Public Property Results2 As ObservableCollection(Of Billing)
            Get
                Return _results2
            End Get
            Set(value As ObservableCollection(Of Billing))
                _results2 = value
                RaisePropertyChanged("Results2")
            End Set
        End Property

        Private _view As ListCollectionView

        Public Property View As ListCollectionView
            Get
                Return _view
            End Get
            Set(value As ListCollectionView)
                _view = value
                RaisePropertyChanged("View")
            End Set
        End Property

        Private _selected As Customer

        Public Property Selected As Customer
            Get
                Return _selected
            End Get
            Set(ByVal value As Customer)
                _selected = value
                If Not IsNothing(value) Then
                    GetCourses()
                    GetUebungsleiter()
                End If
                RaisePropertyChanged("Selected")
            End Set
        End Property

        Private _searchterm As String = ""

        Public Property SearchTerm As String
            Get
                Return _searchterm
            End Get
            Set(value As String)
                _searchterm = value
            End Set
        End Property

        Private _cust As Customer

        Public Property Cust As Customer
            Get
                Return _cust
            End Get
            Set(value As Customer)
                _cust = value
            End Set
        End Property

        Private _bookedcourses As ListCollectionView

        Public Property BookedCourses As ListCollectionView
            Get
                Return _bookedcourses
            End Get
            Set(value As ListCollectionView)
                _bookedcourses = value
                RaisePropertyChanged("BookedCourses")
            End Set
        End Property

        Private _Uebungsleiter As String

        Public Property Uebungsleiter As String
            Get
                Return _Uebungsleiter
            End Get
            Set(value As String)
                _Uebungsleiter = value
                RaisePropertyChanged("Uebungsleiter")
            End Set
        End Property

        Public Property SearchCommand As ICommand
        Public Property OpenCustomerCommand As ICommand
        Public Property ESearchCommand As ICommand
        Public Property NavigateCommand As ICommand


        Public Property AddCustomerCommand As ICommand
        Public Property DuplikateCommand As ICommand
        Public Property RestrictedCustomerCommand As ICommand

#End Region

        Private WithEvents bgw As New BackgroundWorker

        Private WithEvents bgw2 As New BackgroundWorker


        Private Sub Search()
            Suche()
        End Sub


        Private Sub Suche()
            If Len(SearchTerm) > 0 Then
                IsBusy = True

                If Not bgw.IsBusy Then

                    bgw.WorkerSupportsCancellation = False
                    bgw.RunWorkerAsync()
                End If
            End If
        End Sub

        Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw.DoWork
            'Dim _context As New in1Entities

            Dim intSearchterm = Val(SearchTerm)


            Dim TblAdressenQuery =
                    From a In _context.Customers
                    Where (Trim(a.Suchname).StartsWith(SearchTerm) Or a.CustomerID = intSearchterm) And a.Aktiv = True
                    Order By a.Suchname Select a

            e.Result = TblAdressenQuery.ToList

            ' _context.Dispose()
        End Sub

        Private Sub RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) _
            Handles bgw.RunWorkerCompleted
            If Not IsNothing(e.Result) Then
                View = CType(CollectionViewSource.GetDefaultView(e.Result), ListCollectionView)
            End If
            IsBusy = False
        End Sub

        Private Sub ESuche()
            Dim sc As New SearchCustomer
            Dim cust = sc.GetCustomer

            If Not IsNothing(cust) Then
                Dim cs = ServiceLocator.Current.GetInstance(Of ICustomerService)()
                cs.ShowCustomer(cust)
            End If
        End Sub

        Private Sub OpenCustomer()
            If Not IsNothing(View.CurrentItem) Then
                Cust = CType(View.CurrentItem, Customer)

                Dim cs = ServiceLocator.Current.GetInstance(Of ICustomerService)()
                cs.ShowCustomer(Cust)
            End If
        End Sub

        Public Sub RefreshCustomers()
            'Suche()
            If Not IsNothing(Selected) Then
                _context.Entry(Selected).Reload()
            End If
        End Sub

        Public Sub GetCourses()
            If Not bgw2.IsBusy Then
                bgw2.RunWorkerAsync()
            End If
            'BookedCourses.Filter = New Predicate(Of Object)(AddressOf Billingsfilter)
        End Sub

        Private Sub bgw2_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw2.DoWork
            Using con As New in1Entities
                Dim query =
                    con.Billings.Where(
                        Function(o) o.CustomerID = Selected.CustomerID AndAlso o.IsStorno = False AndAlso (o.CourseID.HasValue Or (o.PackageID.HasValue And o.Packagecredit > 0))).
                    OrderByDescending(Function(o) o.Rechnungsdatum).Take(50)
                query.Load
                e.Result = con.Billings.Local
            End Using
        End Sub

        Private Sub bgw2_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles bgw2.RunWorkerCompleted
            If IsNothing(e.Error) Then
                If Not IsNothing(e.Result) Then
                    Results2 = e.Result
                    BookedCourses = CType(CollectionViewSource.GetDefaultView(Results2), ListCollectionView)
                End If
            Else
                Results2 = New ObservableCollection(Of Billing)
                Throw New Exception(e.Error.Message)
            End If

        End Sub


        Private Function Billingsfilter(ByVal item As Object) As Boolean
            Dim itm = CType(item, Billing)

            'If Not IsNothing(itm.CourseID) Or Not IsNothing(itm.PackageID) Or Not IsNothing(itm.ProductID) Then
            If Not IsNothing(itm.IsStorno) Then
                If itm.IsStorno Then Return False
            End If
            If Not IsNothing(itm.CourseID) Or Not IsNothing(itm.PackageID) Then

                If Not IsNothing(itm.PackageID) Then
                    If itm.Packagecredit > 0 Then
                        Return True
                    Else
                        Return False
                    End If
                End If
                Return True
            End If
            Return False
        End Function

        Private Sub GetUebungsleiter()
            If Not IsNothing(Selected) Then
                Uebungsleiter = ""

                Dim dtfrom = Now


                Dim query =
                        From c In _context.StaffContracts
                        Where
                        c.CustomerID = Selected.CustomerID And c.Beginn <= dtfrom And c.Ende > dtfrom Select c

                If Not IsNothing(query) Then
                    If query.Any Then
                        Uebungsleiter = "Übungsleiter (Dienstvertrag vom " &
                                        query.FirstOrDefault.Beginn.ToShortDateString & " - " &
                                        query.FirstOrDefault.Ende.ToShortDateString & ")"
                    End If
                End If
            End If
        End Sub

        Private Sub Duplikate()


            'Dim vm As New DuplicateViewModel(_regionmanager)
            'Dim uc As New DuplicateView

            Dim vm As New MergeDuplicatesViewModel()
            Dim uc As New MergeDuplicatesView


            uc.DataContext = vm

            Dim win As New WindowController
            AddHandler vm.RequestClose, AddressOf win.CloseWindow
            win.ShowWindow(uc, "Duplikate zusammenführen")
        End Sub
        'Private Sub RestrictedCustomer()


        '    Dim vm As New RestrictedCustomerViewModel(_regionmanager)
        '    Dim uc As New RestrictedCustomerView

        '    uc.DataContext = vm

        '    Dim win As New WindowController
        '    AddHandler vm.RequestClose, AddressOf win.CloseWindow
        '    win.ShowWindow(uc, "Geblockte und Inaktive Kunden")
        'End Sub

        Private Sub AddCustomer()

            Events.EventInstance.EventAggregator.GetEvent(Of NewCustomerEvent).Publish(Nothing)
        End Sub

        Private Sub Navigate(ByVal param As Object)
            If Not IsNothing(param) Then
                Process.Start(New ProcessStartInfo(New Uri(param).AbsoluteUri))
            End If
        End Sub


        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub

        Public Sub New()
            _context = New in1Entities
            Events.EventInstance.EventAggregator.GetEvent(Of RefreshCustomerEvent).Subscribe(AddressOf RefreshCustomers)

            SearchCommand = New RelayCommand(AddressOf Search)
            OpenCustomerCommand = New RelayCommand(AddressOf OpenCustomer)
            ESearchCommand = New RelayCommand(AddressOf ESuche)

            AddCustomerCommand = New RelayCommand(AddressOf AddCustomer)
            DuplikateCommand = New RelayCommand(AddressOf Duplikate)
            'RestrictedCustomerCommand = New RelayCommand(AddressOf RestrictedCustomer)
        End Sub
    End Class
End Namespace