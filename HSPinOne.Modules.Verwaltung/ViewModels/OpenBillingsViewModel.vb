﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Data
Imports System.Windows.Input
Imports System.ComponentModel.Composition
Imports Prism.Regions
Imports System.ComponentModel
Imports System.Data.Entity.SqlServer
Imports System.Collections.ObjectModel
Imports System.Data.Entity

Namespace ViewModel

    Public Class OpenBillingsViewModel
        Inherits HSPinOne.Infrastructure.ViewModelBase
        Implements INavigationAware


        Private WithEvents bgw As New BackgroundWorker


#Region "   Properties"
        Private _OpenBillingsView As ListCollectionView
        Public Property OpenBillingsView As ListCollectionView
            Get
                Return _OpenBillingsView
            End Get
            Set(value As ListCollectionView)
                _OpenBillingsView = value

            End Set
        End Property

        Private _SelectedBilling As Object
        Public Property SelectedBilling As Object
            Get
                Return _SelectedBilling
            End Get
            Set(value As Object)
                _SelectedBilling = value
            End Set
        End Property


        Private _Datum As DateTime
        Public Property Datum As DateTime
            Get
                Return _Datum
            End Get
            Set(value As DateTime)
                _Datum = value
                RaisePropertyChanged("Datum")
            End Set
        End Property

        Private _PaymentMethods As ObservableCollection(Of PaymentMethod)
        Public Property PaymentMethods() As ObservableCollection(Of PaymentMethod)
            Get
                Return _PaymentMethods
            End Get
            Set(ByVal value As ObservableCollection(Of PaymentMethod))
                _PaymentMethods = value
            End Set
        End Property

        Private _SelectedPaymentMethod As Integer
        Public Property SelectedPaymentMethod() As Integer
            Get
                Return _SelectedPaymentMethod
            End Get
            Set(ByVal value As Integer)
                _SelectedPaymentMethod = value
                RaisePropertyChanged("SelectedPaymentMethod")
            End Set
        End Property

        Private _Summe As Double
        Public Property Summe() As Double
            Get
                Return _Summe
            End Get
            Set(ByVal value As Double)
                _Summe = value
                RaisePropertyChanged("Summe")
            End Set
        End Property

        Private _Anzahl As Integer
        Public Property Anzahl() As Integer
            Get
                Return _Anzahl
            End Get
            Set(ByVal value As Integer)
                _Anzahl = value
                RaisePropertyChanged("Anzahl")
            End Set
        End Property




        Public Property RefreshCommand As ICommand
        Public Property MouseDoubleClickCommand As ICommand

        Public Property CustomerCommand As ICommand
        Public Property EditCommand As ICommand
        Public Property BookCommand As ICommand

#End Region

        Public Sub New()
            Datum = Now
            RefreshCommand = New RelayCommand(AddressOf Refresh)
            MouseDoubleClickCommand = New RelayCommand(AddressOf OpenCustomer)
            CustomerCommand = New RelayCommand(AddressOf CustomerAction)
            EditCommand = New RelayCommand(AddressOf EditAction)
            BookCommand = New RelayCommand(AddressOf BookAction)


            Using con As New in1Entities
                con.PaymentMethods.Load()
                PaymentMethods = con.PaymentMethods.Local()
                SelectedPaymentMethod = PaymentMethods.FirstOrDefault.PaymentMethodID
            End Using
        End Sub

        Private Sub Refresh()
            IsBusy = True
            If Not bgw.IsBusy Then
                bgw.RunWorkerAsync()
            End If
        End Sub

        Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw.DoWork
            Dim dttill = Datum.Date.AddHours(23).AddMinutes(59).AddSeconds(59)
            Using con As New in1Entities

                Dim query = From c In con.Billings Where c.Faelligkeit <= dttill And c.BillingStatus = BillingStates.Offen _
                            And c.CustomerID > 0 And c.PaymentMethodID = SelectedPaymentMethod Select c.CustomerID, c.Customer.Suchname, c.Faelligkeit, c.Brutto, c.Verwendungszweck, c.Mahnstufe, c.BillingID

                Dim result = query.ToList()

                OpenBillingsView = CType(CollectionViewSource.GetDefaultView(result), ListCollectionView)
                Anzahl = result.Count
                Summe = result.Sum(Function(o) o.Brutto)

            End Using

        End Sub

        Private Sub bgw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles bgw.RunWorkerCompleted
            IsBusy = False
            RaisePropertyChanged("OpenBillingsView")

        End Sub

        Private Sub OpenCustomer()
            If Not IsNothing(SelectedBilling) Then
                Dim cid = CType(SelectedBilling.CustomerID, Integer)
                If Not IsNothing(cid) Then

                    Using con As New in1Entities
                        Dim cust = con.Customers.Find(cid)
                        If Not IsNothing(cust) Then
                            Dim cs As New CustomerService
                            cs.ShowCustomer(cust)
                            Refresh()
                        End If
                    End Using


                End If

            End If

        End Sub

        Private Sub CustomerAction(ByVal param As Object)
            If Not IsNothing(param) Then
                Dim cid = CType(param, Integer)
                If Not IsNothing(cid) Then

                    Using con As New in1Entities
                        Dim cust = con.Customers.Find(cid)
                        If Not IsNothing(cust) Then
                            Dim cs As New CustomerService
                            cs.ShowCustomer(cust)
                            Refresh()
                        End If
                    End Using


                End If
            End If
        End Sub

        Private Sub EditAction(ByVal param As Object, Optional ByVal bookit As Boolean = False)
            If Not IsNothing(param) Then
                Dim billid = CType(param, Integer)
                If Not IsNothing(billid) Then
                    Dim cs As New CustomerService
                    Using con As New in1Entities
                        Dim bill = con.Billings.Find(billid)
                        Dim editable = IIf(bill.BillingStatus = BillingStates.Offen, True, False)
                        If bookit Then
                            bill.Buchungsdatum = Now
                            bill.BillingStatus = BillingStates.Bezahlt
                        End If


                        If cs.ShowBilling(con, bill, editable) Then con.SaveChanges(MyGlobals.CurrentUsername)
                        Refresh()
                    End Using

                End If
            End If
        End Sub

        Private Sub BookAction(ByVal param As Object)
            EditAction(param, True)
        End Sub



    End Class
End Namespace
