﻿Imports System.ComponentModel.Composition
Imports System.Windows.Data
Imports System.ComponentModel
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Input
Imports Prism.Regions
Imports System.Collections.ObjectModel
Imports CommonServiceLocator

Namespace ViewModel
    <Export(GetType(RestrictedCustomerViewModel)), PartCreationPolicy(CreationPolicy.NonShared)>
    Public Class RestrictedCustomerViewModel
        Inherits ViewModelBase

        Public Event RequestClose As EventHandler

        Private _regionmanager As IRegionManager
        Private _servicelocator As IServiceLocator

        Private _selectedCustomer As Customer
        Private _uelview As ListCollectionView
        Private _context As in1Entities
        Private _selectedMode As Integer


#Region "   Properties"


        Public ReadOnly Property Context() As in1Entities
            Get
                Return _context
            End Get
        End Property

        Public Property ModeLookup As Dictionary(Of Integer, String)

        Private _SearchText As String

        Public Property SearchText As String
            Get
                Return _SearchText
            End Get
            Set(value As String)
                _SearchText = value
            End Set
        End Property

        Public Property SelectedMode As Integer
            Get
                Return _selectedMode
            End Get
            Set(value As Integer)
                If value <> _selectedMode Then
                    _selectedMode = value
                    LadeCustomer()
                End If
            End Set
        End Property


        Public Property OcCustomer As ObservableCollection(Of Customer)


        Public Property SelectedCustomer As Customer
            Get
                Return _selectedCustomer
            End Get
            Set(value As Customer)
                _selectedCustomer = value
                RaisePropertyChanged("SelectedCustomer")
            End Set
        End Property


        Public Property CustomerView As ListCollectionView
            Get
                Return _uelview
            End Get
            Set(value As ListCollectionView)
                _uelview = value
                RaisePropertyChanged("CustomerView")
            End Set
        End Property


        Public Property OpenCustomerCommand As ICommand
        Public Property SearchCommand As ICommand
        Public Property PrintCommand As ICommand


#End Region

#Region "   Constructor"


        <ImportingConstructor()>
        Public Sub New(ByVal regionmanager As IRegionManager)

            _regionmanager = regionmanager
            _context = New in1Entities

            'Laden der ÜL Seite

            ModeLookup = New Dictionary(Of Integer, String)
            ModeLookup.Add(1, "gesperrt")
            ModeLookup.Add(2, "inaktiv")
            SelectedMode = 1

            OpenCustomerCommand = New RelayCommand(AddressOf OpenCustomer)
            SearchCommand = New RelayCommand(AddressOf RefreshFilter)
            PrintCommand = New RelayCommand(AddressOf Print)
        End Sub


        Private Sub LadeCustomer()
            If Not IsNothing(SelectedMode) Then
                Dim query As IEnumerable(Of Customer)
                If SelectedMode = 2 Then
                    query = _context.Customers.Where(Function(o) o.Aktiv = False)
                Else
                    query = _context.Customers.Where(Function(o) o.Blocked = True)
                End If
                OcCustomer = New ObservableCollection(Of Customer)(query.OrderBy(Function(o) o.Suchname))
                CustomerView = CType(CollectionViewSource.GetDefaultView(OcCustomer()), ListCollectionView)
                CustomerView.Filter = New Predicate(Of Object)(AddressOf CustomerFilter)
                CustomerView.SortDescriptions.Add(New SortDescription("Suchname", ListSortDirection.Ascending))
                CustomerView.Refresh()
            End If
        End Sub

        Private Sub RefreshFilter()
            If Not IsNothing(CustomerView) Then CustomerView.Refresh()
        End Sub

#End Region

#Region "   Filter"

        Private Function CustomerFilter(ByVal item As Object) As Boolean
            Dim itm = TryCast(item, Customer)
            If Not IsNothing(itm) Then
                If SearchText Is Nothing OrElse Trim(SearchText) = "" Then Return True
                If itm.Suchname.ToLower.StartsWith(SearchText.ToLower) Then Return True
            End If
            Return False
        End Function


#End Region


#Region "Commands"

        Private Sub OpenCustomer()
            Dim cs As New CustomerService

            If Not IsNothing(SelectedCustomer) Then
                cs.ShowCustomer(SelectedCustomer)
                _context.Entry(SelectedCustomer).Reload()
            End If
        End Sub


#End Region
        Private Sub Print()
            Dim cls As New Templates.RestrictedCustomerReport(ModeLookup.FirstOrDefault(Function(o) o.Key = SelectedMode).Value.ToString())
            cls.CreateDocument(OcCustomer.ToList())
            cls = Nothing
        End Sub
    End Class
End Namespace