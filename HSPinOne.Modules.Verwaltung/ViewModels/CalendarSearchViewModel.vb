﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Input
Imports System.Windows.Data
Imports System.ComponentModel.Composition
Imports Prism.Regions
Imports System.ComponentModel
Imports System.Text
Imports System.Data.Entity



Namespace ViewModel

    <Export(GetType(CalendarSearchViewModel)), PartCreationPolicy(CreationPolicy.Shared)>
    Public Class CalendarSearchViewModel
        Inherits ViewModelBase
        Implements INavigationAware


#Region "Properties"

        Private _context As in1Entities

        Private _cvsResults As ListCollectionView


        Public Property cvsResults() As ListCollectionView
            Get
                Return _cvsResults
            End Get
            Set(ByVal value As ListCollectionView)
                _cvsResults = value
            End Set
        End Property

        Private _fromDate As Date
        Public Property fromDate() As Date
            Get
                Return _fromDate
            End Get
            Set(ByVal value As Date)
                _fromDate = value
                RaisePropertyChanged("fromDate")
            End Set
        End Property

        Private _tillDate As Date
        Public Property tillDate() As Date
            Get
                Return _tillDate
            End Get
            Set(ByVal value As Date)
                _tillDate = value
                RaisePropertyChanged("tillDate")
            End Set
        End Property

        Private _SearchText As String = String.Empty
        Public Property SearchText() As String
            Get
                Return _SearchText
            End Get
            Set(ByVal value As String)
                _SearchText = value
                If Not IsNothing(cvsResults) Then cvsResults.Refresh()
                RaisePropertyChanged("SearchText")
            End Set
        End Property

        Private _SelectedCategory As Integer
        Public Property SelectedCategory() As Integer
            Get
                Return _SelectedCategory
            End Get
            Set(ByVal value As Integer)
                _SelectedCategory = value
                If Not IsNothing(cvsResults) Then cvsResults.Refresh()
                RaisePropertyChanged("SelectedCategory")
            End Set
        End Property

        Private _cvsCategories As Dictionary(Of Integer, String)
        Public Property cvsCategories() As Dictionary(Of Integer, String)
            Get
                Return _cvsCategories
            End Get
            Set(ByVal value As Dictionary(Of Integer, String))
                _cvsCategories = value
            End Set
        End Property

        Private _Suchname As String
        Public Property Suchname() As String
            Get
                Return _Suchname
            End Get
            Set(ByVal value As String)
                _Suchname = value
                RaisePropertyChanged("Suchname")
            End Set
        End Property

        Private _Selected As Object
        Public Property Selected() As Object
            Get
                Return _Selected
            End Get
            Set(ByVal value As Object)
                _Selected = value
                RaisePropertyChanged("Selected")
            End Set
        End Property

        Public Property SearchCommand As ICommand
        Public Property ExportCommand As ICommand
        Public Property GetCustomerCommand As ICommand
        Public Property ResetCustomerCommand As ICommand

        Public Property OpenCommand As ICommand


        Private _customerid As Integer = 0

        Private WithEvents _bgw As BackgroundWorker


#End Region
        Public Sub New()
            _context = New in1Entities
            SearchCommand = New RelayCommand(AddressOf SearchAction)
            ExportCommand = New RelayCommand(AddressOf ExportAction)
            GetCustomerCommand = New RelayCommand(AddressOf GetCustomerAction)
            ResetCustomerCommand = New RelayCommand(AddressOf ResetCustomerAction)
            OpenCommand = New RelayCommand(AddressOf OpenAction)


            fromDate = DateAdd(DateInterval.Day, 0, Today)
            tillDate = DateAdd(DateInterval.Day, 14, Today)

            cvsCategories = New Dictionary(Of Integer, String)

            Dim catdata = (From c In _context.Categories Select c.CategoryID, c.CategoryName).ToList

            cvsCategories.Add(0, "Alle")
            For Each itm In catdata
                cvsCategories.Add(itm.CategoryID, itm.CategoryName)
            Next

        End Sub


        Private Sub SearchAction()

            _bgw = New BackgroundWorker
            If Not _bgw.IsBusy Then
                IsBusy = True
                _bgw.RunWorkerAsync()
            End If







        End Sub

        Private Sub _bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles _bgw.DoWork
            Dim till = DateAdd(DateInterval.Day, 1, _tillDate)
            Dim query = From c In _context.Appointments.Include(Function(o) o.Location) Where c.Start >= _fromDate And c.Start <= till
                        Select Typ = If(c.BookingID Is Nothing, "VA", "B"), c.AppointmentID, c.BookingID, c.CourseID, c.Booking.Subject, c.Course.Sport.Sportname, Bezeichnung = If(c.Booking.Subject IsNot Nothing, c.Booking.Subject, c.Course.Sport.Sportname), c.Booking.Category.CategoryName, c.Start, c.Booking.CategoryID, c.Booking.Customer.Suchname, c.Booking.CustomerID, c.Location.LocationName
                        Order By Start

            e.Result = query.ToList



        End Sub

        Private Sub _bgw_Completed(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles _bgw.RunWorkerCompleted

            cvsResults = CollectionViewSource.GetDefaultView(e.Result)
            cvsResults.Filter = New Predicate(Of Object)(AddressOf FilterText)
            RaisePropertyChanged("cvsResults")
            IsBusy = False
        End Sub

        Private Function FilterText(ByVal input As Object) As Boolean


            Dim category As Integer = input.CategoryID

            If SelectedCategory = 0 And (IsNothing(SearchText) Or Trim(SearchText) = "") And (_customerid = 0) Then Return True


            If (SelectedCategory = 0 Or category = SelectedCategory) And (input.Bezeichnung.ToLower.Contains(SearchText) Or Trim(input.Bezeichnung) = "") _
                And (_customerid = 0 Or input.CustomerID = _customerid) Then Return True

            Return False
        End Function

        Private Sub ExportAction()
            Dim csv As New HSPinOne.Common.CSVExport
            Dim first As Boolean = True
            Dim line As New StringBuilder

            For Each itm In cvsResults
                If first Then
                    For Each fld In itm.GetType().GetProperties(Reflection.BindingFlags.Public Or Reflection.BindingFlags.Instance)
                        line.Append(fld.Name() & "|")
                    Next
                    csv.AddLine(line.ToString().Split("|"))
                    line.Clear()
                    first = False
                End If


                For Each fld In itm.GetType().GetProperties(Reflection.BindingFlags.Public Or Reflection.BindingFlags.Instance)
                    line.Append(fld.GetValue(itm, Nothing)).Append("|")
                Next
                csv.AddLine(line.ToString().Split("|"))
                line.Clear()
            Next
            csv.Export()
        End Sub


        Private Sub GetCustomerAction()
            Dim search As New HSPinOne.Common.SearchCustomer()
            Dim cust = search.GetCustomer()

            If Not IsNothing(cust) Then
                Suchname = cust.Suchname
                _customerid = cust.CustomerID
                cvsResults.Refresh()
            End If
        End Sub

        Private Sub ResetCustomerAction()
            _customerid = 0
            Suchname = ""
            cvsResults.Refresh()
        End Sub

        Private Sub OpenAction()
            If Not IsNothing(Selected) Then
                If Not IsNothing(Selected.BookingID) Then
                    Using con As New in1Entities
                        Dim book = con.Bookings.Find(Selected.BookingID)
                        Dim bs As New BookingService
                        bs.ShowBooking(book)
                        bs.Dispose()
                    End Using
                ElseIf Not (IsNothing(Selected.CourseID)) Then
                    Using con As New in1Entities
                        Dim course = con.Courses.Find(Selected.CourseID)
                        Dim cs As New CourseService
                        cs.ShowCourse(course)
                    End Using
                End If
                SearchAction()
            End If
        End Sub


    End Class
End Namespace