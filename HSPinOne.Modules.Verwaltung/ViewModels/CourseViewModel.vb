﻿Imports System.ComponentModel.Composition
Imports System.ComponentModel
Imports System.Windows.Data
Imports System.Data.Entity
Imports System.Windows
Imports HSPinOne.Common
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Windows.Input
Imports System.Threading
Imports HSPinOne.Modules.Verwaltung.Templates


Namespace ViewModel
    <Export(GetType(CourseViewModel)), PartCreationPolicy(CreationPolicy.Shared)>
    Public Class CourseViewModel
        Inherits ViewModelBase

        Public Event RequestClose As EventHandler
        Public Event RefreshLists As EventHandler
        Public CanClose As Boolean

        Private WithEvents BGW As New BackgroundWorker
        Private WithEvents BgwTn As New BackgroundWorker
        Private WithEvents BgwWl As New BackgroundWorker

        Private _currentVa As Course
        Private _appointmentView As ListCollectionView
        Private _selectedbooking As Booking
        Private _ocTn As ObservableCollection(Of Billing)

        Private _selectedCustomer As Billing
        Private _selectedWaitinglistTn As Waitinglist
        Private _currentWaitinglist As ListCollectionView
        Private _currentlistmembers As ListCollectionView
        Private _tnlist As ObservableCollection(Of Billing)
        Private _creator As User
        Private _editor As User
        Private _rRule As RRule
        Private _flags As ObservableCollection(Of Flag)
        Private _currentPosition As Integer
        Private _isFull As Boolean
        Private _working As Boolean

#Region "Properties"


        Private _Context As in1Entities
        Public Property Context() As in1Entities
            Get
                Return _Context
            End Get
            Set(ByVal value As in1Entities)
                _Context = value
            End Set
        End Property



        Public Property CurrentVA As Course
            Get
                Return _currentVa
            End Get
            Set(value As Course)
                _currentVa = value
                LoadCourse()
                RaisePropertyChanged("CurrentVA")
            End Set
        End Property

        Public Property AppointmentView As ListCollectionView
            Get
                Return _appointmentView
            End Get
            Set(value As ListCollectionView)
                _appointmentView = value
                RaisePropertyChanged("AppointmentView")
            End Set
        End Property

        Public Property WeekdayLookup As Dictionary(Of Integer, String)
        Public Property BuchungtemplateLookup As IEnumerable(Of Template)
        Public Property WaitinglisttemplateLookup As IEnumerable(Of Template)
        Public Property StornotemplateLookup As IEnumerable(Of Template)
        Public Property RecurrenceLookup As New Dictionary(Of String, String)
        Public Property SelectedUel As CourseStaff

        Public Property LanguagesLookup As List(Of Language)

        Public Property SelectedBooking As Booking
            Get
                Return _selectedbooking
            End Get
            Set(value As Booking)
                _selectedbooking = value
                RaisePropertyChanged("SelectedBooking")
            End Set
        End Property

        Public Property AppConflicts As ObservableCollection(Of String)
        Public Property DialogResult As Boolean
        Public Property SelectedApp As Appointment

        Public Property SelectedCustomer As Billing
            Get
                Return _selectedCustomer
            End Get
            Set(value As Billing)
                _selectedCustomer = value
                RaisePropertyChanged("SelectedCustomer")
            End Set
        End Property

        Public Property SelectedWaitinglistTn As Waitinglist
            Get
                Return _selectedWaitinglistTn
            End Get
            Set(value As Waitinglist)
                _selectedWaitinglistTn = value
                RaisePropertyChanged("SelectedWaitinglistTn")
            End Set
        End Property

        Public Property CurrentWaitinglist As ListCollectionView
            Get
                Return _currentWaitinglist
            End Get
            Set(ByVal value As ListCollectionView)
                _currentWaitinglist = value
                RaisePropertyChanged("CurrentWaitinglist")
            End Set
        End Property

        Public Property Currentlistmembers As ListCollectionView
            Get
                Return _currentlistmembers
            End Get
            Set(ByVal value As ListCollectionView)
                _currentlistmembers = value
                RaisePropertyChanged("Currentlistmembers")
            End Set
        End Property

        Public Property OCTn As ObservableCollection(Of Billing)
            Get
                Return _ocTn
            End Get
            Set(value As ObservableCollection(Of Billing))
                _ocTn = value
            End Set
        End Property



        Public ReadOnly Property Zutrittslevel As Dictionary(Of Integer, String)
            Get
                Dim ret As New Dictionary(Of Integer, String)
                ret.Add(0, "Kein Zutritt")
                ret.Add(1, "Zutritt zum Raum - Termine")
                ret.Add(2, "Global - Termine")
                ret.Add(3, "Zutritt zum Raum - Saison")
                Return ret
            End Get
        End Property

        Public Property TNList As ObservableCollection(Of Billing)
            Get
                Return _tnlist
            End Get
            Set(value As ObservableCollection(Of Billing))
                _tnlist = value
            End Set
        End Property

        Public Property Creator As User
            Get
                Return _creator
            End Get
            Set(value As User)
                _creator = value
                RaisePropertyChanged("Creator")
            End Set
        End Property

        Public Property Editor As User
            Get
                Return _editor
            End Get
            Set(value As User)
                _editor = value
                RaisePropertyChanged("Editor")
            End Set
        End Property

        Public Property RRule As RRule
            Get
                Return _rRule
            End Get
            Set(value As RRule)
                _rRule = value
                RaisePropertyChanged("RRule")
            End Set
        End Property

        Public Property Flags As ObservableCollection(Of Flag)
            Get
                Return _flags
            End Get
            Set(value As ObservableCollection(Of Flag))
                _flags = value
                RaisePropertyChanged("Flags")
            End Set
        End Property

        Public Property CurrentPosition As Integer
            Get
                Return _currentPosition
            End Get
            Set(value As Integer)
                _currentPosition = value
                RaisePropertyChanged("CurrentPosition")
            End Set
        End Property

        Private _AllTags As ObservableCollection(Of Tag) = New ObservableCollection(Of Tag)
        Public Property AllTags() As ObservableCollection(Of Tag)
            Get
                Return _AllTags
            End Get
            Set(ByVal value As ObservableCollection(Of Tag))
                _AllTags = value
            End Set
        End Property


        Private _SelectedTag As Tag
        Public Property SelectedTag() As Tag
            Get
                Return _SelectedTag
            End Get
            Set(ByVal value As Tag)
                _SelectedTag = value
                AddCourseTag()
                RaisePropertyChanged("SelectedTag")
            End Set
        End Property

        Private _SearchText As String
        Public Property SearchText() As String
            Get
                Return _SearchText
            End Get
            Set(ByVal value As String)
                _SearchText = value
                RaisePropertyChanged("SearchText")
            End Set
        End Property

        Private _SelectedMeta As CourseMeta
        Public Property SelectedMeta() As CourseMeta
            Get
                Return _SelectedMeta
            End Get
            Set(ByVal value As CourseMeta)
                _SelectedMeta = value
                RaisePropertyChanged("SelectedMeta")
            End Set
        End Property




#End Region

#Region "Command Properties"

        Public Property SaveCommand As ICommand
        Public Property SaveCloseCommand As ICommand
        Public Property DeleteCommand As ICommand
        Public Property TermineAddCommand As ICommand
        Public Property TermineClearCommand As ICommand
        Public Property BookingNewCommand As ICommand
        Public Property BookingDelCommand As ICommand
        Public Property EditTerminCommand As ICommand
        Public Property NeuTerminCommand As ICommand
        Public Property DelTerminCommand As ICommand
        Public Property UelAddCommand As ICommand
        Public Property UelDelCommand As ICommand
        Public Property UelShowCommand As ICommand
        Public Property TNShowCommand As ICommand
        Public Property StdLohnCommand As ICommand
        Public Property TNPrintCommand As ICommand
        Public Property TNMailCommand As ICommand
        Public Property TNMailAttachmentCommand As ICommand
        Public Property ConfirmationMailCommand As ICommand
        Public Property DeleteSelectedFromWaitinglistCommand As ICommand
        Public Property CsvExportCommand As ICommand
        Public Property MailCommand As ICommand
        Public Property PrintCommand As ICommand
        Public Property WordTNBCommand As ICommand
        Public Property GetAnspCommand As ICommand
        Public Property DelAnspCommand As ICommand
        Public Property ShowHistoryCommand As ICommand
        Public Property AddImageCommand As ICommand

        Public Property ImageSelectCommand As ICommand
        Public Property ImageDeleteCommand As ICommand
        Public Property SelectAllFromWaitinglistCommand() As ICommand
        Public Property MailSelectedFromWaitinglistCommand() As ICommand

        Public Property RemoveTagCommand() As ICommand

        Public Property AddMetaCommand() As ICommand
        Public Property RemoveMetaCommand() As ICommand




#End Region

#Region "Constructor"


        Public Sub New()
            DialogResult = False
        End Sub

        Private Sub LoadCourse()
            BeforeLoad()
            AfterLoad()
        End Sub

        Private Sub BeforeLoad()
            LadeCombo()

            SaveCommand = New RelayCommand(AddressOf Save, AddressOf CanSaveExecute)
            SaveCloseCommand = New RelayCommand(AddressOf SaveClose, AddressOf CanSaveExecute)
            TermineAddCommand = New RelayCommand(AddressOf TermineAdd, AddressOf CanTermineAdd)
            TermineClearCommand = New RelayCommand(AddressOf TermineClear, AddressOf CanTermineClear)
            UelAddCommand = New RelayCommand(AddressOf UelAdd, AddressOf CanUelAdd)
            UelDelCommand = New RelayCommand(AddressOf UelDel, AddressOf CanUelDel)
            UelShowCommand = New RelayCommand(AddressOf UelShow)
            TNShowCommand = New RelayCommand(AddressOf TnShow)
            StdLohnCommand = New RelayCommand(AddressOf StdLohn, AddressOf CanStdLohn)

            TNPrintCommand = New RelayCommand(AddressOf TnPrint)
            TNMailCommand = New RelayCommand(AddressOf TnMailAction)
            TNMailAttachmentCommand = New RelayCommand(AddressOf TnPrintMail)
            ConfirmationMailCommand = New RelayCommand(AddressOf ConfirmationMailCommandAction,
                                                       AddressOf CanConfirmationMail)
            DeleteSelectedFromWaitinglistCommand = New RelayCommand(AddressOf DeleteSelectedFromWaitinglistAction,
                                                                    AddressOf CanActOnWaitinglistSelection)
            MailSelectedFromWaitinglistCommand = New RelayCommand(AddressOf MailSelectedFromWaitinglistAction,
                                                                  AddressOf CanActOnWaitinglistSelection)
            SelectAllFromWaitinglistCommand = New RelayCommand(AddressOf SelectAllFromWaitinglistAction, AddressOf CanSelectAllFromWaitinglist)

            CsvExportCommand = New RelayCommand(AddressOf CsvExport)

            MailCommand = New RelayCommand(AddressOf MailCommandAction)
            PrintCommand = New RelayCommand(AddressOf PrintCommandAction)
            DeleteCommand = New RelayCommand(AddressOf DeleteAction)

            WordTNBCommand = New RelayCommand(AddressOf WordTnb, AddressOf CanWordTnb)

            NeuTerminCommand = New RelayCommand(AddressOf NeuTermin)
            EditTerminCommand = New RelayCommand(AddressOf EditTermin)
            DelTerminCommand = New RelayCommand(AddressOf DelTermin, AddressOf CanDelTermin)

            GetAnspCommand = New RelayCommand(AddressOf GetAnsp)
            DelAnspCommand = New RelayCommand(AddressOf DelAnsp)

            ShowHistoryCommand = New RelayCommand(AddressOf ShowHistory)

            AddImageCommand = New RelayCommand(AddressOf AddImage)

            ImageSelectCommand = New RelayCommand(AddressOf ImageSelectAction)
            ImageDeleteCommand = New RelayCommand(AddressOf ImageDeleteAction)

            RemoveTagCommand = New RelayCommand(AddressOf RemoveCourseTag)

            AddMetaCommand = New RelayCommand(AddressOf AddMeta)
            RemoveMetaCommand = New RelayCommand(AddressOf RemoveMeta)


        End Sub

        Private Sub SelectAllFromWaitinglistAction(ByVal obj As Object)
            Dim allSelected =
                    Not _
                    CurrentWaitinglist.Cast(Of Waitinglist)().Any(
                        Function(waitinglistItem) waitinglistItem.IsSelected = False)

            For Each item As Waitinglist In CurrentWaitinglist
                item.IsSelected = Not allSelected
            Next
        End Sub

        Private Sub MailSelectedFromWaitinglistAction(ByVal obj As Object)
            If Not IsNothing(CurrentVA) Then
                Dim cls As New clsMail
                Dim checklist As IList(Of Billing) = TryCast(Currentlistmembers.SourceCollection, IList(Of Billing))
                For Each waitinglistItem As Waitinglist In From waitinglistItem1 As Waitinglist In CurrentWaitinglist Where waitinglistItem1.IsSelected

                    If Not checklist.Any(Function(c) c.CustomerID = waitinglistItem.CustomerID) Then
                        If Not IsNothing(waitinglistItem.Customer.Mail1) Then
                            cls.AddBccRecipient(waitinglistItem.Customer.Mail1)
                            waitinglistItem.LastMailAt = DateTime.Now
                            waitinglistItem.MailsSend = waitinglistItem.MailsSend + 1
                        End If
                    End If
                Next
                cls.Subject = CurrentVA.Sport.Sportname & " " &
                              CurrentVA.Zusatzinfo & " " & CurrentVA.Zeittext

                cls.Send()

            End If
        End Sub

        Private Function CanActOnWaitinglistSelection(ByVal obj As Object) As Boolean
            If Not IsNothing(CurrentWaitinglist) Then
                Return _
                    CurrentWaitinglist.Cast(Of Waitinglist)().Any(
                        Function(waitinglistItem) waitinglistItem.IsSelected = True)
            End If

            Return False
        End Function

        Private Sub DeleteSelectedFromWaitinglistAction()

            Dim tempList = CurrentWaitinglist.Cast(Of Waitinglist)().ToList()
            If Not IsNothing(tempList) Then
                Dim dlg As New DialogService
                If dlg.AskConfirmation("löschen", "Die ausgewählten Personen von der Warteliste löschen?") = True Then

                    For Each itm In From itm1 As Object In tempList Where itm1.IsSelected
                        Context.Waitinglists.Remove(itm)
                    Next
                    tempList.RemoveAll(Function(o) o.IsSelected)
                    CurrentWaitinglist = New ListCollectionView(tempList)

                End If
            End If
        End Sub

        Private Function CanConfirmationMail(ByVal obj As Object) As Boolean
            If Not IsNothing(SelectedCustomer) Then
                If Not String.IsNullOrWhiteSpace(SelectedCustomer.Customer.Mail1) Then
                    Return True
                End If
            End If

            Return False
        End Function


        Private Sub LadeCombo()

            WeekdayLookup = New Dictionary(Of Integer, String)
            For i = 1 To 7
                WeekdayLookup.Add(i, WeekdayName(i, False, FirstDayOfWeek.Monday))
            Next
            WeekdayLookup.Add(8, "Täglich")
            WeekdayLookup.Add(10, "Einmalig")

            RecurrenceLookup = New Dictionary(Of String, String)
            RecurrenceLookup.Add("None", "Keine")
            RecurrenceLookup.Add("Daily", "Täglich")
            RecurrenceLookup.Add("Weekly", "Wöchentlich")


            Dim buchungtmpl =
                    From tp In Context.Templates Where tp.TemplatetypeID = 1 Order By tp.TemplateName Select tp
            BuchungtemplateLookup = buchungtmpl.ToList

            Dim wltmpl = From tp In Context.Templates Where tp.TemplatetypeID = 2 Order By tp.TemplateName Select tp
            WaitinglisttemplateLookup = wltmpl.ToList

            Dim stornotmpl = From tp In Context.Templates Where tp.TemplatetypeID = 4 Order By tp.TemplateName Select tp
            StornotemplateLookup = stornotmpl.ToList

            Dim query = From c In Context.Languages Order By c.English Select c
            LanguagesLookup = New List(Of Language)
            LanguagesLookup = query.ToList()

            Context.Tags.Load()
            AllTags = Context.Tags.Local()


            Creator = Context.Users.Where(Function(o) o.UserID = CurrentVA.Erstelltvon).SingleOrDefault
            Editor = Context.Users.Where(Function(o) o.UserID = CurrentVA.Bearbeitetvon).SingleOrDefault


            ' Init Flags
            Me.Flags = Flagsmanager.GetFlagList

            Dim sel = Flagsmanager.GetSelected(CurrentVA.Flags)
            For Each itm In Me.Flags
                For Each el In sel
                    If itm.Flagname = el.Flagname Then itm.IsSelected = True

                Next
                AddHandler itm.PropertyChanged, AddressOf SyncFlags
            Next
        End Sub

        Private Sub AfterLoad()



            ' Semesterliste
            Dim sterm = Listitems.GetTermList.Where(Function(o) o.TermID = CurrentVA.TermID).FirstOrDefault



            ' Defaultwerte vorbelegen
            If CurrentVA.NewRecord Then
                If IsNothing(CurrentVA.DTStart) Then

                    CurrentVA.DTStart = New DateTime(sterm.Semesterbeginn.Year, sterm.Semesterbeginn.Month,
                                                     sterm.Semesterbeginn.Day, 8, 0, 0)
                    CurrentVA.DTEnd = New DateTime(sterm.Semesterbeginn.Year, sterm.Semesterbeginn.Month,
                                                   sterm.Semesterbeginn.Day, 9, 0, 0)
                End If
                RRule = New RRule
                RRule.Freq = FrequencyType.Weekly
                RRule.Until = sterm.Semesterende
                CurrentVA.DTUntil = sterm.Semesterende

                Try
                    With CurrentVA
                        .CategoryID = Listitems.GetCategoryList.First.CategoryID
                        .CourseAudienceID = Listitems.GetCourseAudienceList.First.CourseAudienceID
                        .CourseCostID = Listitems.GetCourseCostList.First.CourseCostID
                        .CourseGenderID = Listitems.GetCourseGenderList.First.CourseGenderID
                        .CourseRegistrationID = Listitems.GetCourseRegistrationList.First.CourseRegistrationID
                        .ProductTypeID = Listitems.GetProductTypeList.First.ProductTypeID
                        .CostcenterID = Listitems.GetCostcenterList.First.CostcenterID
                        .TemplateID =
                            Context.Templates.Where(
                                Function(o) _
                                                       o.Isdefault = True And
                                                       o.Templatetype.Templatetypename = "Buchungsbestätigung").First.
                                TemplateID

                    End With
                Catch ex As Exception

                End Try

            Else
                RRule = New RRule(CurrentVA.RRule)
                If DateDiff(DateInterval.Year, New DateTime(2010, 1, 1), CurrentVA.DTUntil) < 0 Then
                    CurrentVA.DTUntil = RRule.Until
                End If

            End If

            'Preise checken auf Existenz
            Dim stati As List(Of CustomerState)
            stati = (From st In Context.CustomerStates).ToList

            Dim exists As List(Of Integer) =
                    (From preis In CurrentVA.CoursePrices Select preis.CustomerStateID).ToList()



            For Each status In stati
                If Not exists.Contains(status.CustomerStateID) Then
                    Dim neu As New CoursePrice
                    neu.CustomerState = status
                    neu.Preis = 0
                    neu.Anmeldungvon = sterm.Anmeldungab
                    neu.Anmeldungbis = sterm.Anmeldungbis
                    neu.Anmeldungerlaubt = True
                    neu.CostcenterID = CurrentVA.CostcenterID
                    CurrentVA.CoursePrices.Add(neu)
                End If
            Next

            If Not BgwTn.IsBusy Then
                BgwTn.RunWorkerAsync()
            End If

            If Not BgwWl.IsBusy Then
                BgwWl.RunWorkerAsync()
            End If

            'Appointmentview registrieren
            AppointmentView = CType(CollectionViewSource.GetDefaultView(CurrentVA.Appointments), ListCollectionView)
            AppointmentView.SortDescriptions.Add(New SortDescription("Start", ListSortDirection.Ascending))

            'Check for conflicting appointments
            ConflictCheck()
        End Sub

        Private Sub SyncFlags()
            CurrentVA.Flags = Flagsmanager.SetSelected(Me.Flags)
        End Sub


        Private Function Currentlistmembersfilter(ByVal item As Object) As Boolean
            Dim itm = CType(item, Billing)
            If Not IsNothing(itm) Then
                If IsNothing(itm.IsStorno) OrElse itm.IsStorno = False Then Return True
            End If
            Return False
        End Function

        Private Sub BgwTn_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles BgwTn.DoWork
            If Not IsNothing(CurrentVA) Then
                'Dim query = Context.Billings.Where(Function(o) o.CourseID = CurrentVA.CourseID).Include(Function(o) o.Customer)
                Dim repo As New Repositories.BillingRepository(_Context)
                'Dim query = CurrentVA.Billings
                Dim tn = repo.GetParticipants(CurrentVA.CourseID)
                e.Result = tn
            End If
        End Sub

        Private Sub BgwTn_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) _
            Handles BgwTn.RunWorkerCompleted
            If Not IsNothing(e.Result) Then
                OCTn = e.Result
                Currentlistmembers = CType(CollectionViewSource.GetDefaultView(OCTn.ToList), ListCollectionView)

                'Filtern, damit die IsStorno Leute nicht angezeigt werden
                If Not IsNothing(Currentlistmembers) Then _
                    Currentlistmembers.Filter = New Predicate(Of Object)(AddressOf Currentlistmembersfilter)
            End If

        End Sub

        Private Sub BgwWl_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles BgwWl.DoWork
            If Not IsNothing(CurrentVA) Then
                Dim list = From c In Context.Waitinglists.Include(Function(o) o.Customer) Where c.CourseID = CurrentVA.CourseID Order By c.Timestamp Select c

                Dim res = list.ToList()
                'Context.Waitinglists.Where(Function(o) o.CourseID = CurrentVA.CourseID).ToList()
                CurrentWaitinglist = CType(CollectionViewSource.GetDefaultView(res), ListCollectionView)
            End If
        End Sub

        Private Sub BgwWl_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) _
            Handles BgwWl.RunWorkerCompleted
        End Sub

#End Region


#Region "   Commands"

        Private Function Save() As Boolean

            Try
                UpdateFocusedField.Update()
                ' Flags setzen
                If RRule.GetRuleString <> CurrentVA.RRule Then CurrentVA.RRule = RRule.GetRuleString
                If Context.HasChanges Then
                    Editor = Context.Users.Where(Function(o) o.UserID = MyGlobals.CurrentUserID).SingleOrDefault
                    Context.SaveChanges(MyGlobals.CurrentUsername)
                End If
                Return True

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            Return False
        End Function

        Private Sub SaveClose()
            If Save() Then
                DialogResult = True
                RaiseEvent RequestClose(Me, Nothing)
            End If
        End Sub

        Public Sub CloseButtonClicked()
            Dim dlg As New DialogService
            If Not Thread.CurrentPrincipal.IsInRole("User") Then
                If Context.HasChanges Then _
                    MessageBox.Show(
                        "Sie sind nicht berechtigt Änderungen an Kursen vorzunehmen. Die Änderungen werden nicht gespeichert.")
                CloseWindow()

            ElseIf Context.HasChanges Then

                Dim res = dlg.AskCancelConfirmation("Speichern", "Möchten Sie die Änderungen speichern?")
                Select Case res
                    Case MessageBoxResult.Yes
                        SaveClose()
                    Case MessageBoxResult.No
                        CloseWindow()
                End Select
            Else
                CloseWindow()
            End If
        End Sub

        Private Sub CloseWindow()
            CanClose = True
            DialogResult = True
            RaiseEvent RequestClose(Me, Nothing)
        End Sub

        Private Function CanSaveExecute(ByVal param As Object) As Boolean
            If Not CurrentVA.HasErrors Then
                Return True
            End If
            Return False
        End Function

        Private Sub ShowHistory()
            Dim hv = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IHistoryViewer)
            hv.ShowHistory("Course", CurrentVA.CourseID)
        End Sub

        Private Sub DeleteAction()
            Dim dlg As New DialogService
            If CurrentVA.CourseStaffs.Any Then
                dlg.ShowInfo("Löschen",
                             "Dieser Veranstaltung sind noch Übungsleiter zugeordnet. Löschen ist erst möglich, nachdem die Zuordnung gelöscht wurde!>")
                Return
            End If
            If CurrentVA.Billings.Any Then
                dlg.ShowInfo("Löschen", "Diesem Kurs sind bereits Teilnehmer zugeordnet. Löschen nicht möglich!")
                Return
            End If

            If CurrentVA.Appointments.Any Then
                dlg.ShowInfo("Löschen",
                             "Diesem Kurs sind noch Termine zugeordnet. Löschen erst möglich, nachdem die Termine gelöscht wurden!")
                Return
            End If

            If dlg.AskConfirmation("Löschen", "Diese Veranstaltung endgültig löschen?") = True Then
                Dim books = (From c In Context.Bookings Where c.CourseID = CurrentVA.CourseID Select c).ToList

                If Not IsNothing(books) Then
                    For i = books.Count - 1 To 0 Step -1
                        Context.Bookings.Remove(books(i))
                    Next
                End If

                Do While CurrentVA.CoursePrices.Any
                    Context.CoursePrices.Remove(CurrentVA.CoursePrices.First)
                Loop
                Context.Courses.Remove(CurrentVA)
                SaveClose()
            End If
        End Sub


        Private Sub TermineAdd()
            Dim am As New AppointmentsManagement(Context, CurrentVA)
            With CurrentVA
                am.Generate(.DTStart, .DTEnd, RRule.Freq, .DTUntil, .LocationID)
            End With

            ' Den einzelnen Appointments die Übungsleiter zuweisen
            For Each itm In CurrentVA.Appointments
                For Each staff In CurrentVA.CourseStaffs
                    Dim astaff As New AppointmentStaff
                    astaff.Customer = staff.Customer
                    astaff.StdLohn = staff.StdLohn
                    itm.AppointmentStaffs.Add(astaff)
                Next
            Next


            ConflictCheck()
        End Sub

        Private Function CanTermineAdd() As Boolean
            Return True
            If Not IsNothing(SelectedBooking) Then
                If Not IsNothing(SelectedBooking.LocationID) And SelectedBooking.LocationID > 0 Then
                    Return True
                End If
            End If
            Return False
        End Function

        Private Sub TermineClear()

            Dim am As New AppointmentsManagement(Context, CurrentVA)
            am.DeleteAll()
            ConflictCheck()
        End Sub

        Private Function CanTermineClear(ByVal param As Object) As Boolean
            If Not IsNothing(CurrentVA.Appointments) AndAlso CurrentVA.Appointments.Any Then Return True
            Return False
        End Function

        Private Sub NeuTermin()
            Dim am As New AppointmentsManagement(Context, CurrentVA)
            Dim app = am.NewAppointment(CurrentVA.LocationID)
            If Not IsNothing(app) Then
                SelectedApp = app
                Dim nav As New NavigationService
                Dim ret = nav.ShowAppointment(SelectedApp, Context, True)

            End If
        End Sub


        Private Sub DelTermin()
            If Not IsNothing(SelectedApp) Then
                Context.Appointments.Remove(SelectedApp)
            End If
        End Sub

        Private Function CanDelTermin() As Boolean
            If Not IsNothing(SelectedApp) Then Return True
            Return False
        End Function

        Private Sub EditTermin()
            If Not IsNothing(SelectedApp) Then
                Dim nav As New NavigationService
                nav.ShowAppointment(SelectedApp, Context, False)
                ConflictCheck()
                AppointmentView.Refresh()
            End If
        End Sub

        Private Sub UelAdd()
            Dim nav As New SearchCustomer
            Dim win = nav.GetCustomer()
            If Not IsNothing(win) Then
                Dim uel As New CourseStaff
                uel.Customer = (From c In Context.Customers Where c.CustomerID = win.CustomerID Select c).Single
                Dim sport = Context.Sports.Find(CurrentVA.SportID)
                If Not IsNothing(sport.StdLohn) Then
                    uel.StdLohn = sport.StdLohn
                Else
                    uel.StdLohn = 0
                End If
                CurrentVA.CourseStaffs.Add(uel)

                'Übungsleiter bei AppointmentStaff setzen
                For Each itm In CurrentVA.Appointments
                    Dim astaff As New AppointmentStaff
                    astaff.Customer = uel.Customer
                    astaff.StdLohn = uel.StdLohn
                    itm.AppointmentStaffs.Add(astaff)

                Next
                AppointmentView.Refresh()
            End If
        End Sub

        Private Function CanUelAdd(ByVal param As Object) As Boolean
            Return True
        End Function

        Private Sub UelDel()
            Dim dlg As New DialogService
            If _
                dlg.AskConfirmation("Löschen",
                                    "Wirklich Übungsleiter komplett aus der Veranstaltung und allen Terminen entfernen?") =
                True Then
                ' Bei den Appointments löschen
                For Each itm In CurrentVA.Appointments
                    For i = itm.AppointmentStaffs.Count - 1 To 0 Step -1
                        If itm.AppointmentStaffs(i).CustomerID = SelectedUel.CustomerID Then
                            Context.AppointmentStaffs.Remove(itm.AppointmentStaffs(i))
                        End If
                    Next
                Next
                AppointmentView.Refresh()
                Context.CourseStaffs.Remove(SelectedUel)
            End If
        End Sub

        Private Function CanUelDel(ByVal param As Object) As Boolean
            If Not IsNothing(SelectedUel) Then Return True
            Return False
        End Function

        Private Sub StdLohn()
            Dim dlg As New NavigationService
            dlg.ShowStdLohn(SelectedUel)

            ' Bei AppointmentStaff aktuialisieren
            For Each itm In CurrentVA.Appointments
                For Each astaff In itm.AppointmentStaffs
                    If astaff.CustomerID = SelectedUel.CustomerID Then
                        astaff.StdLohn = SelectedUel.StdLohn
                    End If
                Next
            Next
        End Sub

        Private Function CanStdLohn(ByVal param As Object) As Boolean
            If Not IsNothing(SelectedUel) Then Return True
            Return False
        End Function

        Private Sub TnPrint()
            Dim cls As New CourseTNReport(CurrentVA, OCTn)
            cls.CreateDocument()
            cls.Printout()
            cls = Nothing
        End Sub

        Private Sub TnPrintMail()


            If CurrentVA.CourseStaffs.Any Then
                Dim cls As New CourseTNReport(CurrentVA, OCTn)
                For Each itm In CurrentVA.CourseStaffs
                    If Not IsNothing(itm.Customer.Mail1) Then
                        cls.AddRecipient(itm.Customer.Mail1)
                    End If
                Next
                cls.CreateDocument()
                cls.Send(CurrentVA.Sport.Sportname & " - " & IIf(Not IsNothing(CurrentVA.Zusatzinfo), CurrentVA.Zusatzinfo, "") & " " & IIf(Not IsNothing(CurrentVA.Zeittext), CurrentVA.Zeittext, ""))
            Else
                Dim dlg As New DialogService
                dlg.ShowInfo("ÜL", "Dieser Veranstaltung sind keine Übungsleiter zugeordnet")
            End If
        End Sub


        Private Sub TnMailAction()
            If Not IsNothing(OCTn) Then
                Dim dlg As New DialogService

                Dim cls As New clsMail
                Const body As String = ""
                cls.Subject = CurrentVA.Sport.Sportname & " - " & IIf(Not IsNothing(CurrentVA.Zusatzinfo), CurrentVA.Zusatzinfo, "") & " " & IIf(Not IsNothing(CurrentVA.Zeittext), CurrentVA.Zeittext, "")
                For Each itm In OCTn
                    If IsNothing(itm.IsStorno) OrElse itm.IsStorno = False Then
                        If Not IsNothing(itm.Customer) Then
                            If Not IsNothing(itm.Customer.Mail1) AndAlso Not Trim(itm.Customer.Mail1) = "" Then
                                cls.AddBccRecipient(itm.Customer.Mail1)
                            End If
                        End If
                    End If
                Next
                cls.Body = body

                If _
                    dlg.AskConfirmation("Übungsleiter in Blind Copy?",
                                        String.Format("Übungsleiter als Blindcopy (BCC) hinzufügen?")) = True Then
                    If Not IsNothing(CurrentVA.CourseStaffs) Then
                        For Each uel In CurrentVA.CourseStaffs
                            If Not IsNothing(uel.Customer.Mail1) Then
                                cls.AddBccRecipient(uel.Customer.Mail1)
                            End If
                        Next
                    End If
                End If

                cls.SendinBackground()

            End If
        End Sub


        'TNListe per Mail als Text im Body versenden
        Private Sub MailCommandAction()

            Dim mail As New clsMail
            mail.Subject = CurrentVA.Sport.Sportname & " - " & IIf(Not IsNothing(CurrentVA.Zusatzinfo), CurrentVA.Zusatzinfo, "") & " " & IIf(Not IsNothing(CurrentVA.Zeittext), CurrentVA.Zeittext, "")
            Dim body As String = ""
            body = CurrentVA.Zusatzinfo & vbNewLine &
                   CurrentVA.Zeittext & vbNewLine &
                        CurrentVA.Bemerkung & vbNewLine
            Dim i As Integer = 1
            For Each itm In OCTn
                body = body & i & ". " & itm.Customer.Suchname & " Mail: " & itm.Customer.Mail1 & vbNewLine
                i = i + 1
            Next
            mail.Body = body
            mail.SendinBackground()
        End Sub

        Private Sub ConfirmationMailCommandAction()
            If Not IsNothing(SelectedCustomer) Then

                Dim dlg As New DialogService
                If _
                    dlg.AskConfirmation("Mail Versenden",
                                        String.Format("Buchungsbestätigung für {0} versenden?",
                                                      SelectedCustomer.Customer.Suchname)) = True Then
                    Dim mailAccount = New MailAccount()
                    mailAccount.Host = GlobalSettingService.HoleEinstellung("smtphost", True)
                    mailAccount.Port = GlobalSettingService.HoleEinstellung("smtpport", True)
                    mailAccount.Username = GlobalSettingService.HoleEinstellung("smtplogin", True)
                    mailAccount.Password = GlobalSettingService.HoleEinstellung("smtppw", True)
                    mailAccount.Ssl = IIf(GlobalSettingService.HoleEinstellung("smtpssl", True) = "true", True, False)
                    mailAccount.From = GlobalSettingService.HoleEinstellung("smtpfrom", True)
                    If Not String.IsNullOrWhiteSpace(GlobalSettingService.HoleEinstellung("smtpbcc", True)) Then
                        mailAccount.Bcc = GlobalSettingService.HoleEinstellung("smtpbcc", True)
                    End If


                    Context.Templates.Load()
                    '_context.GlobalSettings.Load()
                    'Dim Settings As Object = _context.GlobalSettings


                    'Dim Templatename As String = "BuchungKurs"
                    'Dim Template As Object = _context.Templates.Where(Function(o) o.TemplateName = Templatename).First()
                    If IsNothing(CurrentVA.Template) Then
                        dlg.ShowAlert("E-Mail Vorlage", "Es ist keine E-Mail Vorlage für diesen Kurs festgelegt")
                        Return
                    End If
                    Dim Template As Template = CurrentVA.Template
                    Dim body As String = ""
                    Dim replacer As ReplaceMarker


                    'For Each itm In Currentlistmembers



                    Dim itm = SelectedCustomer
                    '  itm.Customer.Mail1 = "medien@adh.de"
                    If itm.BillingID = SelectedCustomer.BillingID And (Not IsNothing(itm.Customer.Mail1)) Then


                        Dim text = Template.TemplateText
                        Dim rpBilling = New BillingReplaceObject(itm).Output
                        Dim rpCourse = New CourseReplaceObject(itm.Course).Output
                        Dim rpCustomer = New CustomerReplaceObject(itm.Customer).Output

                        'Die Replacerlsiten zu einer zusammen fügen
                        Dim list = rpCourse.Concat(rpCustomer).Concat(rpBilling).ToDictionary(Function(kvp) kvp.Key, Function(kvp) kvp.Value)

                        replacer = New ReplaceMarker(text, list)


                        replacer.DoReplacement()

                        body = replacer.FinalString
                        replacer.Template = Template.Betreff
                        replacer.DoReplacement()
                        Dim subject = replacer.FinalString

                        Dim mapi As New clsMail()
                        mapi.AddRecipient(itm.Customer.Mail1.ToString())
                        mapi.Subject = subject
                        mapi.Body = body

                        mapi.SendinBackground()

                        'Dim inOneMail = New InOneEmail(itm.Customer.Mail1.ToString(), subject, body)
                        'inOneMail.IsHtml = Template.IsHtml

                        'Dim mailer = New EmailDeliveryHelper(mailAccount, inOneMail)
                        'mailer.SendAsync()

                        Using con As New in1Entities
                            con.Activities.Add(New Activity() _
                                                      With {.ActivityType = 1, .Direction = 2,
                                                      .Comment = subject, .Zeit = DateTime.Now(),
                                                      .UserID = MyGlobals.CurrentUserID, .CustomerID = itm.CustomerID})
                            con.SaveChanges()
                        End Using


                    End If
                End If
            End If


            '  mail.SendinBackgroundSmtp()
        End Sub

        Private Sub PrintCommandAction()

            Dim cls As New CourseReport(CurrentVA)
            cls.CreateDocument()
            cls = Nothing
        End Sub

        Private Sub GetAnsp()
            Dim nav As New SearchCustomer
            Dim win = nav.GetCustomer()
            If Not IsNothing(win) Then
                CurrentVA.Ansprechpartner = Context.Customers.Find(win.CustomerID)
            End If
        End Sub

        Private Sub DelAnsp()
            CurrentVA.Ansprechpartner = Nothing
        End Sub


        Private Sub ConflictCheck()

            If CurrentVA.Appointments.Any Then
                If Not BGW.IsBusy Then
                    _working = True
                    BGW.RunWorkerAsync()
                End If

            End If
        End Sub


        Private Sub BGW_DoWork(sender As Object, e As DoWorkEventArgs) Handles BGW.DoWork

            Try


                Dim context2 As New in1Entities
                'AppConflicts = New ObservableCollection(Of String)
                Dim appconflicts As New ObservableCollection(Of String)

                If Not IsNothing(CurrentVA.Appointments) Then


                    For Each app In CurrentVA.Appointments
                        Dim citm = app
                        app.Conflict = False
                        Dim query = From c In context2.Appointments Where c.LocationID = citm.Location.LocationID And
                                                                          ((citm.Start >= c.Start And citm.Start < c.Ende) Or
                                                                           (citm.Ende > c.Start And citm.Ende <= c.Ende) Or
                                                                           (citm.Start <= c.Start And citm.Ende >= c.Ende)) _
                                                                       And c.Canceled = False
                                    Select c


                        If Not IsNothing(query) Then
                            For Each q In query
                                Dim what As String = ""
                                If Not IsNothing(q.CourseID) Then
                                    If q.CourseID <> CurrentVA.CourseID Then
                                        app.Conflict = True

                                        If Not IsNothing(q.Course) Then
                                            what = q.Course.Sport.Sportname
                                        End If
                                        appconflicts.Add(
                                            what & " " & q.Start.ToShortDateString & " " & q.Start.ToShortTimeString & " " &
                                            q.Ende.ToShortTimeString)
                                    End If
                                Else
                                    app.Conflict = True
                                    If Not IsNothing(q.Booking.Subject) Then
                                        what = q.Booking.Subject
                                    End If
                                    appconflicts.Add(
                                        what & " " & q.Start.ToShortDateString & " " & q.Start.ToShortTimeString & " " &
                                        q.Ende.ToShortTimeString)
                                End If
                            Next
                        End If
                    Next
                    e.Result = appconflicts
                End If
            Catch ex As Exception
                'Throw New Exception("Problem beim prüfen der AppConflicts" & ex.Message, ex.InnerException)
                System.Diagnostics.Debug.WriteLine(ex.Message)
            End Try
        End Sub

        Private Sub BGW_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles BGW.ProgressChanged
        End Sub

        Private Sub BGW_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) _
            Handles BGW.RunWorkerCompleted
            _working = False
            AppConflicts = e.Result
            RaisePropertyChanged("AppConflicts")
        End Sub

        Private Sub CsvExport()

            Dim csv As New CSVExport
            If Not Trim(csv.Dateiname) = "" Then
                csv.AddLine("#", "ID", "Nachname", "Vorname", "Strasse", "PLZ", "Ort", "Status", "eMail", "Telefon1", "Telefon2", "Geburtstag", "Comment")

                'Zusammenstellen der Buchungen
                Dim liste =
                        OCTn.Where(Function(o) o.IsStorno = False).OrderBy(Function(o) o.Customer.Nachname) _
                        .ToList

                Dim i As Long = 1

                For Each itm In liste
                    csv.AddLine(i.ToString, itm.CustomerID, itm.Customer.Nachname, itm.Customer.Vorname,
                                itm.Customer.Strasse, itm.Customer.PLZ, itm.Customer.Ort,
                                itm.CustomerState.CustomerStatename, itm.Customer.Mail1, itm.Customer.Telefon1,
                                itm.Customer.Telefon2, itm.Customer.Geburtstag, itm.Comment)
                    i = i + 1
                Next
                csv.Export()
            End If
            csv = Nothing
        End Sub

        Private Sub AddImage()
            Dim res = DocManService.GetImage
            If Not IsNothing(res) And Not Trim(res) = "" Then
                If IsNothing(CurrentVA.Bemerkung) Then CurrentVA.Bemerkung = ""
                Dim newText = CurrentVA.Bemerkung.Substring(0, CurrentPosition) & " " & res.ToString & " " & CurrentVA.Bemerkung.Substring(CurrentPosition, CurrentVA.Bemerkung.Length - CurrentPosition)
                CurrentVA.Bemerkung = newText
            End If
        End Sub

        Private Sub WordTnb()
            If Not IsNothing(SelectedCustomer) Then

                For Each itm In Currentlistmembers
                    If itm.BillingID = SelectedCustomer.BillingID And (Not IsNothing(itm.Customer.Mail1)) Then

                        Dim rpBilling = New BillingReplaceObject(itm).Output
                        Dim rpCourse = New CourseReplaceObject(CurrentVA).Output
                        Dim rpCustomer = New CustomerReplaceObject(itm.Customer).Output
                        Dim rpTerm = New TermReplaceObject(CurrentVA.Term).Output


                        'Die Replacerlsiten zu einer zusammen fügen
                        Dim list = rpCourse.Concat(rpCustomer).Concat(rpBilling).Concat(rpTerm).ToDictionary(Function(kvp) kvp.Key, Function(kvp) kvp.Value)


                        Dim doc As New WordAutomation(list)
                        'doc.Customer = itm.Customer
                        'doc.Course = CurrentVA
                        'doc.AddReplacement("Gebühr", Context.Billings.Find(itm.BillingID).Brutto.ToString)
                        'Dim termine = CurrentVA.Appointments
                        'doc.AddReplacement("Kursvon",
                        '                   If _
                        '                      (Not IsNothing(termine), termine.FirstOrDefault.Start.Date.ToString("d"),
                        '                       ""))
                        'doc.AddReplacement("Kursbis",
                        '                   If(Not IsNothing(termine), termine.Last.Start.Date.ToString("d"), ""))
                        'doc.AddReplacement("AnzTermine", If(Not IsNothing(termine), termine.Count, ""))

                        doc.Show()
                        doc = Nothing

                    End If
                Next

            End If
        End Sub

        Private Function CanWordTnb(ByVal param As Object) As Boolean
            If Not SelectedCustomer Is Nothing Then
                Return True

            End If
            Return False
        End Function

        Private Sub UelShow()
            If Not IsNothing(SelectedUel) Then
                Dim cs As New CustomerService
                Dim cust = Context.Customers.Find(SelectedUel.CustomerID)
                cs.ShowCustomer(cust)
            End If
        End Sub

        Private Sub TnShow()
            If Not IsNothing(SelectedCustomer) Then
                Dim cs As New CustomerService
                Dim cust = Context.Customers.Find(SelectedCustomer.CustomerID)
                cs.ShowCustomer(cust)
            End If
        End Sub

        Private Sub ImageSelectAction()
            Dim res = DocManService.GetFullDocument
            If Not IsNothing(res) Then
                CurrentVA.CourseImage = Context.Documents.Find(res.DocumentUID)
            End If

        End Sub

        Private Sub ImageDeleteAction()
            CurrentVA.CourseImage = Nothing
        End Sub

#End Region

        Private Function CanSelectAllFromWaitinglist(ByVal obj As Object) As Boolean
            If Not IsNothing(CurrentWaitinglist) AndAlso CurrentWaitinglist.Count > 0 Then
                Return True
            End If

            Return False
        End Function

        Private Sub AddCourseTag()
            If Not IsNothing(SelectedTag) Then
                Dim Tag = CType(SelectedTag, Tag)
                If IsNothing(CurrentVA.CourseTags.Where(Function(o) o.Tag.TagID = Tag.TagID).FirstOrDefault) Then
                    Dim ct As New CourseTag
                    ct.Tag = Tag

                    CurrentVA.CourseTags.Add(ct)
                    SelectedTag = Nothing
                End If
                SearchText = Nothing
            End If
        End Sub

        Private Sub RemoveCourseTag(ByVal args As Object)
            If Not IsNothing(args) Then
                Dim ct = CurrentVA.CourseTags.Where(Function(o) o.CourseTagID = args).FirstOrDefault
                If Not IsNothing(ct) Then Context.CourseTags.Remove(ct)
            End If
        End Sub


        Private Sub AddMeta()
            Dim meta As New CourseMeta
            meta.MetaKey = "Neuer Schlüssel"
            meta.MetaValue = ""
            CurrentVA.CourseMetas.Add(meta)
        End Sub

        Private Sub RemoveMeta()
            If Not IsNothing(SelectedMeta) Then
                _Context.CourseMetas.Remove(SelectedMeta)
                SelectedMeta = Nothing
            End If
        End Sub



    End Class
End Namespace
