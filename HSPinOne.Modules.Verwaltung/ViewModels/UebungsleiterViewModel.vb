﻿Imports HSPinOne.Common
Imports HSPinOne.DAL
Imports System.Collections.ObjectModel
Imports System.ComponentModel
Imports System.Data.Entity
Imports HSPinOne.Infrastructure
Imports System.Windows.Data
Imports System.Windows.Input
Imports System.ComponentModel.Composition
Imports Prism.Regions
Imports CommonServiceLocator

Namespace ViewModel


    Public Class UebungsleiterViewModel
        Inherits ViewModelBase
        Implements INavigationAware


        Private _regionmanager As IRegionManager
        'Private _servicelocator As IServiceLocator

        Private _ocUel As ObservableCollection(Of CourseStaff)

        Private _selectedUel As CourseStaff


        Private _uelview As ListCollectionView

        Private _years As List(Of Integer)


        Private _context As in1Entities
        Private _semester As IEnumerable(Of Term)
        Private _selectedsemester As Integer
        Private _uelcheckall As Boolean



#Region "   Properties"


        Public ReadOnly Property Context() As in1Entities
            Get
                Return _context
            End Get
        End Property

        Public Property Semester As IEnumerable(Of Term)
            Get
                Return _semester
            End Get
            Set(value As IEnumerable(Of Term))
                _semester = value
            End Set
        End Property



        Public Property SelectedSemester As Integer
            Get
                Return _selectedsemester
            End Get
            Set(value As Integer)
                If value <> _selectedsemester Then
                    _selectedsemester = value
                    LadeUel()
                End If
            End Set
        End Property

        Public Property Years As List(Of Integer)
            Get
                Return _years
            End Get
            Set(value As List(Of Integer))
                _years = value
            End Set
        End Property

        Private _SearchText As String
        Public Property SearchText As String
            Get
                Return _SearchText
            End Get
            Set(value As String)
                _SearchText = value
            End Set
        End Property




        Public Property OCUel As ObservableCollection(Of CourseStaff)
            Get
                Return _ocUel
            End Get
            Set(value As ObservableCollection(Of CourseStaff))
                _ocUel = value
            End Set
        End Property


        Public Property SelectedUel As CourseStaff
            Get
                Return _selectedUel
            End Get
            Set(value As CourseStaff)
                _selectedUel = value
                RaisePropertyChanged("SelectedUel")
            End Set
        End Property



        Public Property UelView As ListCollectionView
            Get
                Return _uelview
            End Get
            Set(value As ListCollectionView)
                _uelview = value
                RaisePropertyChanged("UelView")
            End Set
        End Property

        Public Property UelCheckall As Boolean
            Get
                Return _uelcheckall
            End Get
            Set(value As Boolean)
                _uelcheckall = value
                pUelCheckall()
            End Set
        End Property




        Public Property UelErhaltenCommand As ICommand
        Public Property UelErhaltenDelCommand As ICommand
        Public Property UelMailCommand As ICommand
        Public Property OpenCustomerCommand As ICommand
        Public Property SearchCommand As ICommand
        Public Property UelPrintCommand As ICommand
        Public Property DetailCommand As ICommand





#End Region

#Region "   Constructor"



        Public Sub New()
            _regionmanager = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IRegionManager)
            '_servicelocator = ServiceLocator



            UelErhaltenCommand = New RelayCommand(AddressOf UelErhalten, AddressOf CanUelErhalten)
            UelErhaltenDelCommand = New RelayCommand(AddressOf UelErhaltenDel, AddressOf CanUelErhalten)
            UelMailCommand = New RelayCommand(AddressOf UelMail, AddressOf CanUelMail)
            OpenCustomerCommand = New RelayCommand(AddressOf OpenCustomer)
            SearchCommand = New RelayCommand(AddressOf RefreshFilter)
            UelPrintCommand = New RelayCommand(AddressOf PrintBilling, AddressOf CanUelErhalten)
            DetailCommand = New RelayCommand(AddressOf Detail, AddressOf CanUelErhalten)




        End Sub

        Private Sub OnInit()
            _context = New in1Entities

            'Laden der ÜL Seite
            Dim query = (From c In _context.Terms Where c.Aktiv = True Select c).OrderByDescending(Function(o) o.Semesterbeginn)

            Semester = query.ToList()
            For Each itm In Semester
                If itm.Semesterbeginn <= Now And itm.Semesterende >= Now Then
                    SelectedSemester = itm.TermID
                End If
            Next
            If IsNothing(SelectedSemester) Or SelectedSemester = 0 Then SelectedSemester = Semester.FirstOrDefault().TermID
        End Sub


        Private Sub LadeUel()
            If Not IsNothing(SelectedSemester) Then
                _context.Dispose()
                _context = New in1Entities
                '_context.CourseStaffs.Include("Course").Include("Customer").Where(Function(o) o.Course.TermID = SelectedSemester).Load()
                _context.CourseStaffs.Include(Function(o) o.Course).Include(Function(o) o.Customer).Include(Function(o) o.Customer.CustomerMetas).Where(Function(o) o.Course.TermID = SelectedSemester).Load()
                OCUel = _context.CourseStaffs.Local

                UelView = CType(CollectionViewSource.GetDefaultView(OCUel), ListCollectionView)
                UelView.Filter = New Predicate(Of Object)(AddressOf UelFilter)
                UelView.SortDescriptions.Add(New SortDescription("Customer.Suchname", ListSortDirection.Ascending))
                UelView.Refresh()
            End If
        End Sub

        Private Sub RefreshFilter()
            If Not IsNothing(UelView) Then UelView.Refresh()
        End Sub

#End Region

#Region "   Filter"


        Private Function UelFilter(ByVal item As Object) As Boolean
            Dim itm = TryCast(item, CourseStaff)
            If Not IsNothing(itm) And Not IsNothing(itm.Course) Then
                If itm.Course.TermID = SelectedSemester Then
                    If SearchText Is Nothing OrElse Trim(SearchText) = "" Then Return True
                    If itm.Customer.Suchname.ToLower.StartsWith(SearchText.ToLower) Then Return True
                End If

            End If
            Return False
        End Function


#End Region


        Private Sub pUelCheckall()
            For Each itm In OCUel
                ' Uncheck all first
                itm.IsSelected = False
                'If itm.Course.TermID = SelectedSemester Then
                '    itm.IsSelected = UelCheckall
                'End If
            Next
            For Each itm In UelView
                Dim cs = TryCast(itm, CourseStaff)
                cs.IsSelected = UelCheckall
            Next
        End Sub






#Region "   Commands"



        Private Sub UelErhalten()
            SelectedUel.Stdzettelzurueck = Now
            _context.SaveChanges()
        End Sub

        Private Sub UelErhaltenDel()
            SelectedUel.Stdzettelzurueck = Nothing
            _context.SaveChanges()
        End Sub

        Private Function CanUelErhalten(ByVal param As Object) As Boolean
            If Not IsNothing(SelectedUel) Then
                Return True
            End If
            Return False
        End Function

        Private Sub UelMail()
            Dim objMail As New clsMail

            For Each itm In UelView
                Dim item = TryCast(itm, CourseStaff)
                If Trim(item.Customer.Mail1) <> "" Then
                    If item.IsSelected = True Then objMail.AddRecipient(item.Customer.Mail1)
                End If
            Next
            objMail.Send()
        End Sub

        Private Function CanUelMail(ByVal param As Object) As Boolean
            If Not IsNothing(OCUel) Then
                For Each itm In OCUel
                    If itm.IsSelected Then Return True
                Next
            End If
            Return False
        End Function

        Private Sub OpenCustomer()
            Dim cs As New CustomerService

            If Not IsNothing(SelectedUel) Then
                cs.ShowCustomer(SelectedUel.Customer)
                _context.Entry(SelectedUel).Reload()
                For Each itm In SelectedUel.Customer.CustomerMetas
                    _context.Entry(itm).Reload()
                Next
            End If

        End Sub

        Private Sub PrintBilling()
            Dim itm = SelectedUel

            Dim st As New Stundenzettel(itm)
            st.Print()
            _context.Entry(SelectedUel).Reload()



        End Sub

        Private Sub Detail()
            Dim vm = ServiceLocator.Current.GetInstance(Of UelDetailViewModel)()
            Dim params = New NavigationParameters()
            params.Add("TermID", SelectedSemester)
            params.Add("Customer", SelectedUel.Customer)

            _regionmanager.RequestNavigate(RegionNames.MainRegion, New Uri("UelDetailView", UriKind.Relative), params)


        End Sub
#End Region


        Public Overrides Sub OnNavigatedTo(navigationContext As NavigationContext)
            MyBase.OnNavigatedTo(navigationContext)
            OnInit()
        End Sub





    End Class
End Namespace