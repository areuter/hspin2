﻿Imports System.ComponentModel.Composition
Imports System.Windows.Data
Imports System.ComponentModel
Imports System.Windows.Input
Imports System.Data.Entity
Imports HSPinOne.Common
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Text
Imports System.Windows
Imports CommonServiceLocator

Namespace ViewModel
    <Export(GetType(CoursesViewModel)), PartCreationPolicy(CreationPolicy.Shared)>
    Public Class CoursesViewModel
        Inherits ViewModelBase

        Private _firstload As Boolean = True

        Private _ocVeranstaltungen As ObservableCollection(Of Course)
        Private _VAView As ListCollectionView
        Private _semesterliste As IEnumerable(Of Term)
        Private _anbieter As IEnumerable(Of Party)
        Private _aktiv As Dictionary(Of Integer, String)
        Private _suchtext As String

        Private _selectedAktiv As String
        Private _selectedAnbieter As String
        Private _selectedterm As Term
        Private _context As in1Entities

        Private _lastselected As Integer

        Public Event ScrollintoView As EventHandler

        Private WithEvents bgw As BackgroundWorker


        Private Event LoadCompleted As EventHandler


#Region "Properties"


        Public Property SelectedTerm() As Term
            Get
                Return _selectedterm
            End Get
            Set(ByVal value As Term)
                _selectedterm = value
                RaisePropertyChanged("SelectedTerm")
                'FilterReset()
                If FilterBy = 1 Then LadeVA()
            End Set
        End Property

        Private _CurrentVA As Object

        Public Property CurrentVA As Object
            Get
                Return _CurrentVA
            End Get
            Set(ByVal value As Object)
                _CurrentVA = value
                RaisePropertyChanged("CurrentVA")
            End Set
        End Property


        Public Property SelectedAnbieter As String
            Get
                Return _selectedAnbieter
            End Get
            Set(ByVal value As String)
                _selectedAnbieter = value
                RaisePropertyChanged("SelectedAnbieter")
                FilterList()
                SaveSettings()
            End Set
        End Property

        Public Property SelectedAktiv As String
            Get
                Return _selectedAktiv
            End Get
            Set(ByVal value As String)
                _selectedAktiv = value
                RaisePropertyChanged("SelectedAktiv")
                FilterList()
            End Set
        End Property

        Public Property Aktiv As Dictionary(Of Integer, String)
            Get
                Return _aktiv
            End Get
            Set(ByVal value As Dictionary(Of Integer, String))
                _aktiv = value
            End Set
        End Property

        Private _SelectAll As Boolean = False

        Public Property SelectAll As Boolean
            Get
                Return _SelectAll
            End Get
            Set(value As Boolean)
                _SelectAll = value
                SelectAllAction()
                RaisePropertyChanged("SelectAll")
            End Set
        End Property

        Private _termlookup As IEnumerable(Of Term)

        Public Property TermLookup As IEnumerable(Of Term)
            Get
                Return _termlookup
            End Get
            Set(ByVal value As IEnumerable(Of Term))
                _termlookup = value
            End Set
        End Property

        Public Property Anbieter As IEnumerable(Of Party)
            Get
                Return _anbieter
            End Get
            Set(ByVal value As IEnumerable(Of Party))
                _anbieter = value
            End Set
        End Property


        Public Property VAView As ListCollectionView
            Get
                Return _VAView
            End Get
            Set(ByVal value As ListCollectionView)
                _VAView = value
            End Set
        End Property

        ' Public Property TView As IQueryableCollectionView

        Private _ocCourses As ObservableCollection(Of Course)

        Public Property OCCourses As ObservableCollection(Of Course)
            Get
                Return _ocCourses
            End Get
            Set(value As ObservableCollection(Of Course))
                _ocCourses = value
            End Set
        End Property


        Public Property Suchtext As String
            Get
                Return _suchtext
            End Get
            Set(ByVal value As String)
                _suchtext = value
                RaisePropertyChanged("Suchtext")
            End Set
        End Property

        Private _Categories As ObservableCollection(Of Category) = New ObservableCollection(Of Category)
        Public Property Categories() As ObservableCollection(Of Category)
            Get
                Return _Categories
            End Get
            Set(ByVal value As ObservableCollection(Of Category))
                _Categories = value
                RaisePropertyChanged("Categories")
            End Set
        End Property


        Private _SelectedCategory As Integer
        Public Property SelectedCategory As Integer
            Get
                Return _SelectedCategory
            End Get
            Set(ByVal value As Integer)
                _SelectedCategory = value
                FilterList()
            End Set
        End Property

        Private _CoursesFrom As Date
        Public Property CoursesFrom() As Date
            Get
                Return _CoursesFrom
            End Get
            Set(ByVal value As Date)
                _CoursesFrom = value
                If Not IsNothing(_CoursesFrom) And Not IsNothing(_CoursesTill) And FilterBy = 0 Then LadeVA()
                SaveSettings()
            End Set
        End Property

        Private _CoursesTill As Date
        Public Property CoursesTill() As Date
            Get
                Return _CoursesTill
            End Get
            Set(ByVal value As Date)
                _CoursesTill = value
                If Not IsNothing(_CoursesFrom) And Not IsNothing(_CoursesTill) And FilterBy = 0 Then LadeVA()
                SaveSettings()

            End Set
        End Property

        '0 = datum , 1 = Semester
        Private _filterBy As Integer
        Public Property FilterBy() As Integer
            Get
                Return _filterBy
            End Get
            Set(ByVal value As Integer)
                _filterBy = value
                RaisePropertyChanged("FilterBy")
                SwitchView()
                LadeVA()
                SaveSettings()
            End Set
        End Property

        Private _filtered As Boolean = False
        Public Property Filtered() As Boolean
            Get
                Return _filtered
            End Get
            Set(ByVal value As Boolean)
                _filtered = value
                RaisePropertyChanged("Filtered")
            End Set
        End Property



        Public ReadOnly Property Filteritems As Dictionary(Of Integer, String)
            Get
                Return New Dictionary(Of Integer, String) From {{0, "Datum"}, {1, "Semester"}}
            End Get
        End Property

        Private _showDatum As Visibility
        Public Property ShowDatum() As Visibility
            Get
                Return _showDatum
            End Get
            Set(ByVal value As Visibility)
                _showDatum = value
                RaisePropertyChanged("ShowDatum")
            End Set
        End Property

        Private _showTerm As Visibility
        Public Property ShowTerm() As Visibility
            Get
                Return _showTerm
            End Get
            Set(ByVal value As Visibility)
                _showTerm = value
                RaisePropertyChanged("ShowTerm")
            End Set
        End Property

        Private _IDSearch As String
        Public Property IDSearch() As String
            Get
                Return _IDSearch
            End Get
            Set(ByVal value As String)
                _IDSearch = value
                RaisePropertyChanged("IDSearch")
            End Set
        End Property


        Public Property Results As Object

        Private mysemid As Long

        Private _selecteditems As List(Of Integer)


        Public Property FilterResetCommand As ICommand
        Public Property NewVACommand As ICommand
        Public Property OpenVACommand As ICommand
        Public Property SearchCommand As ICommand
        Public Property AddCourseCommand As ICommand
        Public Property RefreshCourseCommand As ICommand
        Public Property ExportCommand As ICommand
        Public Property CloneCourseCommand As ICommand
        Public Property TNClipboardCommand As ICommand
        Public Property TNMailCommand As ICommand
        Public Property IDSearchCommand() As ICommand

#End Region


#Region "   Constructor"

        Public Sub New()

            bgw = New BackgroundWorker

            _context = New in1Entities

            _context.CourseStates.Load()


            FilterBy = My.Settings.FilterBy

            'CourseFrom und CoursesTill aus Settings lesen
            If My.Settings.CoursesFrom.Year > 2000 And My.Settings.CoursesTill.Year > 2000 Then
                CoursesFrom = My.Settings.CoursesFrom
                CoursesTill = My.Settings.CoursesTill
            Else
                CoursesFrom = DateAdd(DateInterval.Day, -60, Now)
                CoursesTill = DateAdd(DateInterval.Day, 60, Now)
            End If

            ' Semesterliste laden
            Dim query = _context.Terms.Where(Function(o) o.Aktiv).OrderByDescending(Function(o) o.Semesterbeginn)

            TermLookup = query.ToList

            Dim sterm = From c In TermLookup Where c.Semesterbeginn <= Now And c.Semesterende >= Now Select c

            If sterm.Any Then
                SelectedTerm = sterm.First
            Else
                SelectedTerm = TermLookup.First
            End If





            If _firstload Then
                LadeComboboxen()
                _firstload = False
            End If

            LadeVA()

            'Events.EVAG.GetEvent(Of RefreshCourseEvent).Subscribe(AddressOf LadeVA)


            FilterResetCommand = New RelayCommand(AddressOf FilterReset)
            NewVACommand = New RelayCommand(AddressOf NewVA, AddressOf CanNewVA)
            OpenVACommand = New RelayCommand(AddressOf OpenVA, AddressOf CanOpenVA)
            SearchCommand = New RelayCommand(AddressOf FilterList)
            AddCourseCommand = New RelayCommand(AddressOf NewVA, AddressOf CanNewVA)
            RefreshCourseCommand = New RelayCommand(AddressOf LadeVA)
            ExportCommand = New RelayCommand(AddressOf ExportToFile)
            CloneCourseCommand = New RelayCommand(AddressOf CloneCourse, AddressOf CanCloneCourse)
            TNClipboardCommand = New RelayCommand(AddressOf TNClipboard, AddressOf CanCoursesSelected)
            TNMailCommand = New RelayCommand(AddressOf TNMailAction, AddressOf CanCoursesSelected)

            IDSearchCommand = New RelayCommand(AddressOf IDSearchAction, AddressOf CanIDSearchAction)
        End Sub

#End Region

#Region "Procedures"


        Private Sub SwitchView()
            If _filterBy = 0 Then
                ShowDatum = Visibility.Visible
                ShowTerm = Visibility.Collapsed
            Else
                ShowDatum = Visibility.Collapsed
                ShowTerm = Visibility.Visible
            End If
        End Sub

        Private Sub LadeVA()

            If _firstload Then
                Return
            End If

            IsBusy = True


            ' Selected im array speichern
            If Not IsNothing(VAView) Then
                _selecteditems = New List(Of Integer)
                For Each itm In VAView
                    If itm.IsSelected Then
                        _selecteditems.Add(itm.CourseID)
                    End If
                Next
            End If

            'RaisePropertyChanged("VAView")


            'System.Diagnostics.Debug.Write("DB Sta" & Now.ToString("o") & vbNewLine)
            bgw.WorkerSupportsCancellation = False
            If Not bgw.IsBusy Then bgw.RunWorkerAsync()
        End Sub

        Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw.DoWork



            Try

                'Using con As New in1Entities
                If Not IsNothing(_context) Then _context.Dispose()
                _context = New in1Entities
                Dim con = _context
                Dim query1 As IQueryable(Of Course)

                If FilterBy = 1 Then
                    query1 =
                            From
                            c In
                            con.Courses.Include(Function(o) o.Location).Include(Function(o) o.CourseState).Include(
                                Function(o) o.Party).Include(Function(o) o.Sport).Include(Function(o) o.Term).Include(Function(o) o.Sport.SportCategory)
                            Where c.TermID = SelectedTerm.TermID
                Else
                    query1 =
                            From
                            c In
                            con.Courses.Include(Function(o) o.Location).Include(Function(o) o.CourseState).Include(
                                Function(o) o.Party).Include(Function(o) o.Sport).Include(Function(o) o.Term).Include(Function(o) o.Sport.SportCategory)
                            Where c.DTStart >= CoursesFrom And c.DTStart <= CoursesTill

                End If
                e.Result = query1.ToList
                'End Using

            Catch ex As Exception
                Dim dlg As New DialogService
                dlg.ShowAlert("Laden", "Es ist ein Problem beim Laden der Daten aufgetreten.")
                dlg = Nothing
            End Try

            '_context.Courses.Include(Function(o) o.Location).Include(Function(o) o.CourseState).Include(Function(o) o.Party).Include(Function(o) o.Sport).Include(Function(o) o.Term).Where(Function(c) c.TermID = SelectedTerm.TermID).Load()

            'OCCourses = _context.Courses.Local
        End Sub

        Private Sub bgw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) _
            Handles bgw.RunWorkerCompleted
            'System.Diagnostics.Debug.Write("DB End" & Now.ToString("o") & vbNewLine)
            RaisePropertyChanged("Anbieter")
            RaisePropertyChanged("Aktiv")

            'Dim res = New List(Of CourseShort)(e.Result)

            OCCourses = New ObservableCollection(Of Course)(CType(e.Result, List(Of Course)))

            Dim view = CType(CollectionViewSource.GetDefaultView(e.Result), ListCollectionView)


            If Not IsNothing(view) Then

                view.Filter = New Predicate(Of Object)(AddressOf Suchfilter)

                If view.SortDescriptions.Count = 0 Then
                    view.SortDescriptions.Add(New SortDescription("Sport.Sportname", ListSortDirection.Ascending))
                    view.SortDescriptions.Add(New SortDescription("Wota", ListSortDirection.Ascending))
                    view.SortDescriptions.Add(New SortDescription("Zeittext", ListSortDirection.Ascending))
                End If

            End If
            'System.Diagnostics.Debug.Write("Filter" & Now.ToString("o") & vbNewLine)

            VAView = view


            If Not IsNothing(_selecteditems) Then
                For Each itm In VAView
                    If _selecteditems.Contains(itm.CourseID) Then itm.IsSelected = True
                Next
            End If

            RaisePropertyChanged("VAView")
            'System.Diagnostics.Debug.Write("VAView" & Now.ToString("o") & vbNewLine)

            IsBusy = False
        End Sub


        Private Sub LadeComboboxen()


            Dim query1 = From a In _context.Parties Select a


            Dim anb As New ObservableCollection(Of Party)
            Dim all As New Party
            all.PartyID = 999
            all.Partyname = "Alle"
            anb.Add(all)
            For Each item In query1
                anb.Add(item)
            Next
            Anbieter = anb.ToList

            If Not IsNothing(My.Settings.LastPartyID) And My.Settings.LastPartyID > 0 Then
                SelectedAnbieter = My.Settings.LastPartyID
            Else
                SelectedAnbieter = Anbieter.FirstOrDefault.PartyID
            End If


            Dim dellist = New Dictionary(Of Integer, String)
            dellist.Add(999, "Alle anzeigen")
            dellist.Add(1, "Alle aktiven")
            dellist.Add(0, "Alle inaktiven")

            Aktiv = dellist


            'Kategorien
            Dim cats = Listitems.GetCategoryList.ToList
            Categories = New ObservableCollection(Of Category)(cats)
            Categories.Insert(0, New Category With {.CategoryID = 999, .CategoryName = "Alle Kategorien"})
            SelectedCategory = 999

            SelectedAktiv = "1"
        End Sub


        Private Function Suchfilter(ByVal item As Object) As Boolean

            Dim itm = item


            If Not IsNothing(itm) Then

                If SelectedAnbieter = "999" And SelectedAktiv = "999" And Trim(Suchtext) = "" Then Return True

                Dim booanbieter As Boolean = False
                If Not IsNothing(SelectedAnbieter) Then
                    If SelectedAnbieter = "999" Or itm.PartyID = SelectedAnbieter Then
                        booanbieter = True
                    End If
                End If


                Dim booaktiv As Boolean = False
                If Not IsNothing(SelectedAktiv) Then

                    If SelectedAktiv = "999" Or itm.Aktiv = CBool(SelectedAktiv) Then
                        booaktiv = True
                    End If
                End If

                Dim boocategory As Boolean = False
                If Not IsNothing(SelectedCategory) Then
                    If SelectedCategory = "999" Or itm.CategoryID = SelectedCategory Then
                        boocategory = True
                    End If
                End If


                Dim boosuche As Boolean = False
                If Not IsNothing(Suchtext) And Not Trim(Suchtext) = "" Then
                    If Not IsNothing(itm.Zusatzinfo) Then
                        If itm.Zusatzinfo.ToLower.Contains(Suchtext.ToLower) Then boosuche = True
                    End If

                    If Not IsNothing(itm.Sport) Then
                        If itm.Sport.Sportname.ToLower.Contains(Suchtext.ToLower) Then boosuche = True
                    End If

                Else
                    boosuche = True
                End If


                Return booanbieter And booaktiv And boosuche And boocategory

            End If


            Return False
        End Function


        Private Sub FilterList()
            If Not IsNothing(VAView) Then VAView.Refresh()
            If SelectedAnbieter = "999" And SelectedAktiv = "1" And Trim(Suchtext) = "" Then Me.Filtered = False Else Me.Filtered = True
        End Sub


#End Region

#Region "   Command Methods"

        Private Sub FilterReset()
            SelectedAnbieter = "999"
            SelectedAktiv = "1"
            SelectedCategory = "999"
            Suchtext = ""
            FilterList()
        End Sub

        Private Function CanFilterReset(ByVal param As Object) As Boolean
            Return True
        End Function


        Private Sub NewVA()

            Dim cs As New CourseService


            If cs.GetNewCourse(SelectedTerm) = True Then
                LadeVA()
                VAView.Refresh()
            End If
        End Sub

        Private Function CanNewVA(ByVal param As Object) As Boolean
            Return True
        End Function

        Private Sub OpenVA()

            If Not IsNothing(CurrentVA) Then
                Dim courseid = CType(CurrentVA.CourseID, Integer)

                Dim course = _context.Courses.Find(courseid)

                If Not IsNothing(course) Then

                    'CurrentVA = CType(VAView.CurrentItem, Course)


                    Dim cs = ServiceLocator.Current.GetInstance(Of ICourseService)()
                    If cs.ShowCourse(course) = True Then

                        'Liste neu laden

                        If Not bgw.IsBusy Then bgw.RunWorkerAsync()

                    End If
                    VAView.Refresh()
                End If

            End If



        End Sub


        Private Function CanOpenVA(ByVal param As Object) As Boolean
            Return True
        End Function

        Private Sub AddCourse()

            Events.EventInstance.EventAggregator.GetEvent(Of NewCourseEvent).Publish(Nothing)
        End Sub


        Private Sub ExportToFile()

            Dim csv As New CSVExport
            Dim wc As New WaitCursor
            Try


                Using con As New in1Entities


                    For Each itm In VAView
                        If VAView.PassesFilter(itm) Then
                            Dim courseid = CType(itm.CourseID, Integer)
                            Dim course = con.Courses.Find(courseid)
                            If Not IsNothing(course) Then

                                'Dim zeit As String = ""
                                'Dim ort As String = ""

                                Dim zeit = course.DTStart.ToShortTimeString & "-" & course.DTEnd.ToShortTimeString
                                Dim ort = course.Location.LocationName


                                Dim preistext As New StringBuilder
                                For Each preis In course.CoursePrices.OrderBy(Function(o) o.CustomerStateID)
                                    preistext.Append(preis.CustomerState.CustomerStatename)
                                    preistext.Append(" ")
                                    preistext.Append(preis.Preis.ToString("c"))
                                    preistext.Append(" ")
                                Next

                                Dim Ansp As New StringBuilder
                                If Not IsNothing(course.Sport.Ansprechpartner_Customer) Then
                                    Ansp.Append(
                                        course.Sport.Ansprechpartner_Customer.Vorname & " " &
                                        course.Sport.Ansprechpartner_Customer.Nachname & " " &
                                        course.Sport.Ansprechpartner_Customer.Mail1)
                                End If

                                Dim uel As New StringBuilder
                                For Each person In course.CourseStaffs
                                    uel.Append(person.Customer.Suchname & ", ")
                                Next

                                csv.AddLine(course.Sport.SportCategory.SportCategoryName, course.Sport.Sportname,
                                            course.Zusatzinfo, course.Zeittext, ort,
                                            course.CourseAudience.CourseAudienceName, course.CourseCost.CourseCostName,
                                            preistext.ToString, Ansp.ToString, course.Bemerkung, Weekday(course.DTStart, FirstDayOfWeek.Monday), course.DTStart.ToShortDateString, course.DTStart.ToShortTimeString, course.DTEnd.ToShortDateString, course.DTEnd.ToShortTimeString, uel.ToString)
                            End If
                        End If
                    Next
                End Using
                csv.Export()

                csv = Nothing
                wc.Dispose()

            Catch ex As Exception
                If Not IsNothing(wc) Then wc.Dispose()
                Dim dlg As New DialogService
                dlg.ShowError("Fehler", "Fehler beim Export " & vbNewLine & ex.Message)
            End Try
        End Sub

        Private Sub CloneCourse()

            If Not IsNothing(CurrentVA) Then
                Dim dlg As New DialogService
                If dlg.AskConfirmation("Duplizieren", "Wollen Sie die ausgewählte Veranstaltung duplizieren?") Then
                    Dim courseid = CType(CurrentVA.CourseID, Integer)
                    Using con As New in1Entities

                        Dim course =
                                con.Courses.AsNoTracking.Include(Function(o) o.CoursePrices).Include(Function(c) c.CourseMetas).Include(Function(c) c.CourseTags).FirstOrDefault(
                                    Function(o) o.CourseID = courseid)


                        course.Zusatzinfo = "Kopie " & course.Zusatzinfo
                        course.Erstelltam = Now
                        course.Erstelltvon = MyGlobals.CurrentUserID
                        course.Bearbeitetam = Nothing
                        course.Bearbeitetvon = Nothing
                        con.Courses.Add(course)
                        con.SaveChanges()

                    End Using

                    LadeVA()
                    dlg.ShowInfo("Duplizieren", "Die Veranstaltung wurde dupliziert")
                    dlg = Nothing
                End If
            End If
        End Sub


        Private Function CanCloneCourse() As Boolean
            If Not IsNothing(CurrentVA) Then Return True
            Return False
        End Function

        Private Sub TNClipboard()
            MyGlobals.ClipboardString = ""
            Dim cb As New StringBuilder
            Using con As New in1Entities
                For Each itm In OCCourses
                    If itm.IsSelected Then
                        Dim cid = itm.CourseID
                        Dim query =
                                From c In con.Billings Where c.CourseID = cid And c.IsStorno = False
                                Select c.CustomerID, c.IsStorno

                        For Each reci In query
                            cb.Append(reci.CustomerID & ",")
                        Next

                    End If
                Next
                MyGlobals.ClipboardString = cb.ToString
            End Using
        End Sub

        Private Sub TNMailAction()
            Dim mail As New clsMail
            Dim anz As Integer = 0

            Using con As New in1Entities
                For Each itm In OCCourses
                    If itm.IsSelected Then
                        Dim cid = itm.CourseID
                        Dim query =
                                From c In con.Billings Where c.CourseID = cid And c.IsStorno = False
                                Select c.CustomerID, c.IsStorno, c.Customer.Mail1

                        For Each reci In query
                            'cb.Append(reci.CustomerID & ",")
                            mail.AddBccRecipient(reci.Mail1)
                            anz = anz + 1
                        Next

                    End If
                Next
            End Using
            If anz > 200 Then
                Dim dlg As New DialogService
                dlg.ShowInfo("Anzahl Empfänger", "Sie haben " & anz & " Empfänger markiert. Evtl. können nicht alle gleichzeitig per Bcc angeschrieben werden")
                dlg = Nothing
            End If
            mail.SendinBackground()
        End Sub

        Private Function CanCoursesSelected() As Boolean
            If Not IsNothing(OCCourses) Then
                For Each itm In OCCourses
                    If itm.IsSelected Then Return True
                Next
            End If

            Return False
        End Function

        Private Sub SelectAllAction()
            If IsNothing(VAView) Then Return

            For Each itm In VAView
                If VAView.PassesFilter(itm) Then
                    itm.IsSelected = _SelectAll

                End If
            Next
        End Sub

        Private Sub SaveSettings()
            My.Settings.LastPartyID = SelectedAnbieter
            My.Settings.CoursesFrom = CoursesFrom
            My.Settings.CoursesTill = CoursesTill
            My.Settings.FilterBy = _filterBy
            My.Settings.Save()
        End Sub

        Private Sub IDSearchAction()
            Dim id As Integer
            If Integer.TryParse(IDSearch, id) Then
                Dim course = _context.Courses.Find(id)
                If Not IsNothing(course) Then
                    Dim cs As New CourseService()
                    cs.ShowCourse(course)
                Else
                    Dim dlg As New DialogService
                    dlg.ShowInfo("ID Suche", "Kein Kurs mit dieser ID gefunden")
                End If
            End If
        End Sub

        Private Function CanIDSearchAction() As Boolean
            If Not IsNothing(IDSearch) AndAlso IsNumeric(IDSearch) Then
                Return True
            End If
            Return False
        End Function

#End Region

        Protected Overrides Sub Finalize()


            If Not IsNothing(_context) Then _context.Dispose()

            MyBase.Finalize()
        End Sub
    End Class



End Namespace
