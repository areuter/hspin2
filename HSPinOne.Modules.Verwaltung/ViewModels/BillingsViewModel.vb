﻿Imports HSPinOne.DAL
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports HSPinOne.Infrastructure
Imports System.Windows.Data
Imports System.Windows.Input
Imports System.ComponentModel
Imports HSPinOne.Common

Namespace ViewModel



    Public Class BillingsViewModel
        Inherits HSPinOne.Infrastructure.ViewModelBase



        Public Event RequestClose As EventHandler

        Private lngPID As Long
        Private _ocBillings As ObservableCollection(Of Billing)
        Private _view As ListCollectionView
        Private _selectedstatus As String

        Private _selecteditem As Billing


        Dim bill As Billing
        Dim query As IQueryable(Of Billing)
        Dim _context As in1Entities

        Private _dialogresult As Boolean
        Public Property DialogResult As Boolean
            Get
                Return _dialogresult
            End Get
            Set(ByVal value As Boolean)
                _dialogresult = value
            End Set
        End Property

        Public Property View As ListCollectionView
            Get
                Return _view
            End Get
            Set(value As ListCollectionView)
                _view = value
                RaisePropertyChanged("View")
            End Set
        End Property

        Public Property OCBillings As ObservableCollection(Of Billing)
            Get
                Return _ocBillings
            End Get
            Set(value As ObservableCollection(Of Billing))
                _ocBillings = value
            End Set
        End Property

        Private _Status As List(Of BillingStatusItem)
        Public Property Status As List(Of BillingStatusItem)
            Get
                Return _Status
            End Get
            Set(value As List(Of BillingStatusItem))
                _Status = value
            End Set
        End Property

        Public Property SelectedStatus As String
            Get
                Return _selectedstatus
            End Get
            Set(value As String)
                _selectedstatus = value
                If Not IsNothing(View) Then _
                View.Refresh()
            End Set
        End Property

        Public Property SelectedItem As Billing
            Get
                Return _selecteditem
            End Get
            Set(value As Billing)
                _selecteditem = value
            End Set
        End Property

        Private _cust As Customer
        Public Property Cust As Customer
            Get
                Return _cust
            End Get
            Set(value As Customer)
                _cust = value
            End Set
        End Property



        Public Property NewBillCommand As ICommand
        Public Property EditBillCommand As ICommand
        Public Property PayBillCommand As ICommand
        Public Property StornoBillCommand As ICommand
        Public Property PrintCommand As ICommand
        Public Property PrintSelectedCommand As New RelayCommand(AddressOf PrintSelected, AddressOf CanPrintSammelBillAction)
        Public Property OKCommand As ICommand
        Public Property RSCommand As ICommand
        Public Property MahnstufeAddCommand As ICommand
        Public Property PrintBillCommand As ICommand
        Public Property PrintBillSammelCommand As ICommand









        Public Sub New(ByVal context As in1Entities, ByVal cust As Customer)



            _context = context
            Me.Cust = cust

            Dim billingstati = New Billingstatuslist
            Status = billingstati.Liste
            Dim itm As New BillingStatusItem
            itm.StatusID = "A"
            itm.Statusname = "Alle"
            Status.Add(itm)

            SelectedStatus = BillingStates.Offen

            EditBillCommand = New RelayCommand(AddressOf EditBill, AddressOf CanEditBill)
            PayBillCommand = New RelayCommand(AddressOf PayBill, AddressOf CanPayorStornoBill)
            StornoBillCommand = New RelayCommand(AddressOf StornoBill, AddressOf CanPayorStornoBill)
            PrintCommand = New RelayCommand(AddressOf Print)
            PrintBillCommand = New RelayCommand(AddressOf PrintBillAction, AddressOf CanPrintBill)
            PrintBillSammelCommand = New RelayCommand(AddressOf PrintBillSammelAction, AddressOf CanPrintSammelBillAction)

            NewBillCommand = New RelayCommand(AddressOf NewBill)
            'RSCommand = New RelayCommand(AddressOf RSAction, AddressOf CanRSAction)
            MahnstufeAddCommand = New RelayCommand(AddressOf MahnstufeAdd, AddressOf CanMahnstufeAdd)

            OKCommand = New RelayCommand(AddressOf OK)

            LoadBillings()



        End Sub

        Private Sub OK()
            RaiseEvent RequestClose(Me, Nothing)
        End Sub

        Private Sub LoadBillings()


            _context.Accounts.Load()
            _context.PaymentMethods.Load()


            OCBillings = Cust.Billings

            View = CType(CollectionViewSource.GetDefaultView(OCBillings), ListCollectionView)
            View.SortDescriptions.Add(New SortDescription("Buchungsdatum", ListSortDirection.Ascending))
            View.SortDescriptions.Add(New SortDescription("Faelligkeit", ListSortDirection.Ascending))


            View.Filter = New Predicate(Of Object)(AddressOf Suchfilter)
        End Sub

        Private Function Suchfilter(ByVal item As Object) As Boolean
            If SelectedStatus = "A" Then Return True
            Dim itm = TryCast(item, Billing)
            If itm IsNot Nothing Then

                If itm.BillingStatus = SelectedStatus Then
                    Return True
                    'If SelectedStatus = BillingStates.Offen Then
                    '    Return True
                    'End If
                    'If SelectedStatus = BillingStates.Storniert Then Return True
                    ''If itm.IsStorno = False Then Return True
                End If
            End If
            Return False
        End Function

        Private Sub EditBill()
            If SelectedItem IsNot Nothing Then

                Dim cs As New CustomerService
                Dim editable = False
                If SelectedItem.BillingStatus = BillingStates.Offen Then
                    editable = True
                End If
                If cs.ShowBilling(_context, SelectedItem, editable) = True Then
                    View.Refresh()

                Else
                    If _context.Entry(SelectedItem).State = EntityState.Modified Then
                        _context.Entry(SelectedItem).Reload()
                    End If
                End If

            End If

        End Sub

        Private Function BillSelected() As Boolean
            If Not IsNothing(SelectedItem) Then Return True
            Return False

        End Function

        Private Sub NewBill()

            Dim bill As New Billing
            bill.Rechnungsdatum = Now
            bill.Faelligkeit = Now
            bill.IsStorno = False
            bill.BillingStatus = BillingStates.Offen
            bill.Customer = Cust
            bill.ProductTypeID = _context.ProductTypes.FirstOrDefault.ProductTypeID
            bill.CustomerStateID = Cust.CustomerStateID

            Dim cs As New CustomerService
            If cs.ShowBilling(_context, bill, True) = True Then
                _context.Billings.Add(bill)
                'Cust.Billings.Add(bill)
                View.Refresh()

                RaisePropertyChanged("View")
            Else
                bill = Nothing
            End If



        End Sub


        Private Sub PayBill()
            'Dim dlg As New DialogService
            'Dim repo As New Repositories.BillingRepository(_context)
            'If (dlg.AskCancelConfirmation("Verbuchen", "Wirklich Zahlung verbuchen?")) = vbYes Then
            '    If repo.Verbuchen(SelectedItem) Then
            '        View.Refresh()
            '    Else
            '        dlg.ShowInfo("Verbuchen", repo.Errormessage)
            '    End If
            'End If
            SelectedItem.Buchungsdatum = Now
            SelectedItem.BillingStatus = BillingStates.Bezahlt
            Dim cs As New CustomerService
            If cs.ShowBilling(_context, SelectedItem, True) = True Then
                View.Refresh()
            Else
                If _context.Entry(SelectedItem).State = EntityState.Modified Then
                    _context.Entry(SelectedItem).Reload()
                End If
            End If

        End Sub

        Private Sub StornoBill()
            If Not IsNothing(SelectedItem) Then
                If Not IsNothing(SelectedItem.CourseID) Then
                    Dim dlg As New DialogService
                    dlg.ShowInfo("Info", "Mit dem Stornieren wird der Kunde auch aus dem Kurs entfernt")
                End If
            End If


            Dim repo As New Repositories.BillingRepository(_context)
            repo.StornoOpenBilling(SelectedItem)

            Dim cs As New CustomerService
            If cs.ShowBilling(_context, SelectedItem, True) = True Then
                View.Refresh()
            Else
                If _context.Entry(SelectedItem).State = EntityState.Modified Then
                    _context.Entry(SelectedItem).Reload()
                End If
            End If
        End Sub

        Private Function CanEditBill() As Boolean
            If Not IsNothing(SelectedItem) Then Return True
            Return False
        End Function

        Private Function CanPayorStornoBill() As Boolean
            If Not IsNothing(SelectedItem) Then
                If SelectedItem.BillingStatus = BillingStates.Offen Then Return True
            End If
            Return False
        End Function



        'Private Sub RSAction()

        '    'Originalrechnung anzeigen

        '    'Originalrechnung wieder auf offen stellen
        '    SelectedItem.Buchungsdatum = Nothing
        '    SelectedItem.BillingStatus = BillingStates.Offen
        '    SelectedItem.PaymentInformation = String.Empty



        '    Dim vm As New ViewModel.BillingViewModel(_context, SelectedItem)
        '    Dim d As New BillingDetailView()
        '    d.DataContext = vm

        '    Dim win As New WindowController
        '    AddHandler vm.RequestClose, AddressOf win.CloseWindow
        '    win.ShowWindow(d, "Rechungsdetail")

        '    If vm.DialogResult = False Then
        '        ' Bei Abbruch neu laden
        '        If _context.Entry(SelectedItem).State = EntityState.Modified Then
        '            _context.Entry(SelectedItem).Reload()
        '        End If

        '    Else
        '        ' Kopie der Rechnung erstellen
        '        Dim copy As New Billing

        '        copy.Customer = Cust
        '        copy.CustomerStateID = Cust.CustomerStateID
        '        copy.ProductTypeID = SelectedItem.ProductTypeID
        '        copy.BillingStatus = BillingStates.Rueckbelastet
        '        copy.Rueckbuchungsdatum = SelectedItem.Rueckbuchungsdatum
        '        copy.AccountID = SelectedItem.AccountID
        '        copy.PaymentMethodID = SelectedItem.PaymentMethodID
        '        copy.Brutto = SelectedItem.Brutto
        '        copy.Steuerprozent = SelectedItem.Steuerprozent
        '        copy.Rechnungsdatum = SelectedItem.Rechnungsdatum
        '        copy.Faelligkeit = SelectedItem.Faelligkeit
        '        copy.Verwendungszweck = SelectedItem.Verwendungszweck
        '        copy.Bemerkung = SelectedItem.Bemerkung
        '        copy.CostcenterID = SelectedItem.CostcenterID
        '        copy.IsStorno = False
        '        Cust.Billings.Add(copy)

        '        If vm.Stornogeb > 0 Then
        '            ' Neue Rechnung anlegen mit Stornogebühr
        '            Dim rs As New Billing
        '            rs.Customer = Cust
        '            rs.IsStorno = False
        '            rs.CustomerStateID = Cust.CustomerStateID
        '            rs.ProductTypeID = 4
        '            rs.CostcenterID = SelectedItem.CostcenterID
        '            rs.PaymentMethodID = SelectedItem.PaymentMethodID
        '            rs.Account = SelectedItem.Account
        '            'rs.AccountID = SelectedItem.AccountID
        '            rs.BillingStatus = BillingStates.Offen
        '            rs.Verwendungszweck = "Stornogebühr"
        '            rs.Rechnungsdatum = Now
        '            rs.Faelligkeit = SelectedItem.Faelligkeit
        '            rs.Brutto = vm.Stornogeb
        '            Cust.Billings.Add(rs)
        '        End If




        '        View.Refresh()


        '    End If


        'End Sub

        'Private Function CanRSAction() As Boolean

        '    If Not IsNothing(SelectedItem) Then
        '        If SelectedItem.BillingStatus = BillingStates.Bezahlt And (IsNothing(SelectedItem.IsStorno) Or SelectedItem.IsStorno = False) Then Return True
        '    End If
        '    Return False
        'End Function

        Private Sub MahnstufeAdd()
            If Not IsNothing(SelectedItem) AndAlso SelectedItem.BillingStatus = BillingStates.Offen Then
                SelectedItem.Mahnstufe = SelectedItem.Mahnstufe + 1
            End If
        End Sub

        Private Function CanMahnstufeAdd() As Boolean
            If Not IsNothing(SelectedItem) AndAlso SelectedItem.BillingStatus = BillingStates.Offen Then Return True

            Return False
        End Function

        Private Sub Print()
            Dim cls As New Templates.BillingsReport(Cust)
            cls.CreateDocument()
        End Sub

        Private Sub PrintBillAction()
            If Not IsNothing(SelectedItem) AndAlso Not IsNothing(Cust) Then

                Dim rpBilling = New BillingReplaceObject(SelectedItem).Output
                Dim rpCustomer = New CustomerReplaceObject(Cust).Output
                Dim list = rpCustomer.Concat(rpBilling)

                If Not IsNothing(SelectedItem.BookingID) Then
                    Dim book = _context.Bookings.Find(SelectedItem.BookingID)
                    Dim rpBooking = New BookingReplaceObject(book).Output
                    Dim rpAppointment = New AppointmentReplaceObject(book.Appointments.ToList())
                    rpAppointment.ListToStringWithNewLine()
                    list = list.Concat(rpAppointment.Output).Concat(rpBooking)
                End If




                Dim doc As New WordAutomation(list.ToDictionary(Function(kvp) kvp.Key, Function(kvp) kvp.Value))
                doc.Vorschau = True


                doc.Show()

            End If
        End Sub

        Private Function CanPrintBill() As Boolean
            Return True
        End Function

        Private Sub PrintBillSammelAction()
            If OCBillings.Where(Function(o) o.Checked = True).Count > 0 AndAlso Not IsNothing(Cust) Then

                Dim rpCustomer = New CustomerReplaceObject(Cust).Output

                Dim saetze As New List(Of Double)

                Dim rpBilling = New BillingReplaceObject()
                For Each itm In OCBillings.Where(Function(o) o.Checked = True)
                    rpBilling.AddTableBillingLine(itm)
                    If Not rpBilling.Output.ContainsKey("rechnung.faelligkeit") Then
                        rpBilling.Output.Add("rechnung.faelligkeit", itm.Faelligkeit.ToShortDateString())
                    End If
                    If Not saetze.Contains(itm.Steuerprozent) Then saetze.Add(itm.Steuerprozent)
                Next

                rpBilling.AddTableLine(";;;")

                For Each satz In saetze
                    Dim brutto = OCBillings.Where(Function(o) o.Checked = True And o.Steuerprozent = satz).Sum(Function(c) c.Brutto)
                    Dim netto = brutto / (100 + satz) * 100
                    Dim steuerbetrag = brutto / (100 + satz) * satz
                    rpBilling.AddTableSum("Summe " & satz & "%", brutto, steuerbetrag, satz, netto)
                Next

                rpBilling.AddTableLine("Gesamtsumme;;;" & OCBillings.Where(Function(o) o.Checked = True).Sum(Function(c) c.Brutto).ToString("c"))


                Dim billing = rpBilling.Output

                Dim list = rpCustomer.Concat(billing).ToDictionary(Function(kvp) kvp.Key, Function(kvp) kvp.Value)

                Dim doc As New WordAutomation(list)
                doc.AddTable("rechnung.positionen", rpBilling.Table, "lrrr", "200")
                doc.Vorschau = True


                doc.Show()

            End If
        End Sub

        Private Sub PrintSelected()
            If OCBillings.Where(Function(o) o.Checked = True).Count > 0 AndAlso Not IsNothing(Cust) Then
                Dim liste As New ObservableCollection(Of Billing)
                For Each itm In OCBillings
                    If itm.Checked = True Then
                        liste.Add(itm)
                    End If
                Next
                Dim rep As New Templates.BillingsTable()
                rep.CreateDocument(Cust, liste)

            End If
        End Sub

        Private Function CanPrintSammelBillAction() As Boolean
            If IsNothing(OCBillings) Then Return False
            If OCBillings.Where(Function(o) o.Checked = True).Count > 0 Then
                Return True
            End If
            Return False
        End Function

    End Class



End Namespace

