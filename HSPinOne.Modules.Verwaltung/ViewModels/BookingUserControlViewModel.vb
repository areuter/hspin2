﻿Imports HSPinOne.Common
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Input
Imports System.Collections.ObjectModel
Imports System.ComponentModel
Imports HSPinOne.Modules.Verwaltung.Templates
Imports System.Data.Entity
Imports CommonServiceLocator

Namespace ViewModel


    Public Class BookingUserControlViewModel
        Inherits ViewModelBase

        Public Event RequestClose As EventHandler

        Private WithEvents BGW As New BackgroundWorker

        Private _context As in1Entities


        Private _Book As Booking
        Public Property Book() As Booking
            Get
                Return _Book
            End Get
            Set(ByVal value As Booking)
                _Book = value
                RaisePropertyChanged("Book")
            End Set
        End Property


        Private _AppConflicts As ObservableCollection(Of String) = New ObservableCollection(Of String)
        Public Property AppConflicts() As ObservableCollection(Of String)
            Get
                Return _AppConflicts
            End Get
            Set(ByVal value As ObservableCollection(Of String))
                _AppConflicts = value
            End Set
        End Property



        Private _SelectedApp As Appointment
        Public Property SelectedApp As Appointment
            Get
                Return _SelectedApp
            End Get
            Set(value As Appointment)
                _SelectedApp = value
                RaisePropertyChanged("SelectedApp")
            End Set
        End Property

        Private _SelectedBilling As Billing
        Public Property SelectedBilling As Billing
            Get
                Return _SelectedBilling
            End Get
            Set(value As Billing)
                _SelectedBilling = value
                RaisePropertyChanged("SelectedBilling")
            End Set
        End Property

        Public Property LocationLookup As IEnumerable(Of Location)

        Public Property Recurrence As List(Of RecurrencePattern)


        Public Property DialogResult As Boolean

        Private _Verknuepfung As String
        Public Property Verknuepfung As String
            Get
                Return _Verknuepfung
            End Get
            Set(value As String)
                _Verknuepfung = value
                RaisePropertyChanged("Verknuepfung")
            End Set
        End Property

        Private _CategoryLookup As IEnumerable(Of Category)
        Public Property CategoryLookup() As IEnumerable(Of Category)
            Get
                Return _CategoryLookup
            End Get
            Set(ByVal value As IEnumerable(Of Category))
                _CategoryLookup = value
                RaisePropertyChanged("CategoryLookup")
            End Set
        End Property


        Public Property TermineAddCommand As ICommand
        Public Property TermineClearCommand As ICommand

        Public Property EditTerminCommand As ICommand
        Public Property NeuTerminCommand As ICommand
        Public Property DelTerminCommand As ICommand

        Public Property OKCommand As ICommand
        Public Property CancelCommand As ICommand
        Public Property DeleteCommand As ICommand

        Public Property SetCustomerCommand As ICommand
        Public Property DelVCommand As ICommand
        Public Property HistoryCommand As ICommand

        Public Property PrintBookingCommand As ICommand

        Public Property CreateBillingCommand As ICommand
        Public Property EditBillingCommand As ICommand
        Public Property DeleteBillingCommand As ICommand
        Public Property PrintBillingCommand As ICommand


        Public Property RRule As RRule


        Public Sub New(ByVal inpbook As Booking)
            DialogResult = False
            Recurrence = RecurrenceList.GetList()


            _context = New in1Entities

            'Load Categories
            Dim cats = _context.Categories

            CategoryLookup = cats.ToList


            If inpbook.BookingID = 0 Then
                Me.Book = New Booking
                Book.RecurrencePattern = RecurrenceList.None
                Book.RecurrencePatternEndDate = Book.Ende
                Book.Color = "Gray"
                Me.Book = inpbook
                _context.Bookings.Add(Me.Book)
            Else
                Dim mybook = _context.Bookings.Find(inpbook.BookingID)

                'Dim cat = mybook.Category.CategoryName
                If IsNothing(mybook.Color) Then
                    mybook.Color = "Gray"
                End If
                If Not IsNothing(mybook.CustomerID) Then
                    Me.Verknuepfung = _context.Customers.Find(mybook.CustomerID).Suchname
                End If
                Me.Book = mybook
            End If


            RRule = New RRule(Me.Book.RecurrencePattern)

            ' Add any initialization after the InitializeComponent() call.


            EditTerminCommand = New RelayCommand(AddressOf EditTermin)
            DelTerminCommand = New RelayCommand(AddressOf DelTermin, AddressOf CanDelTermin)
            TermineAddCommand = New RelayCommand(AddressOf TermineAdd, AddressOf CanTermineAdd)
            TermineClearCommand = New RelayCommand(AddressOf TermineClear, AddressOf CanTermineClear)
            OKCommand = New RelayCommand(AddressOf OKAction, AddressOf CanOKAction)
            CancelCommand = New RelayCommand(AddressOf CancelAction)
            DeleteCommand = New RelayCommand(AddressOf DeleteAction)
            SetCustomerCommand = New RelayCommand(AddressOf SetCustomer)
            DelVCommand = New RelayCommand(AddressOf DelCustomer)
            HistoryCommand = New RelayCommand(AddressOf History)
            PrintBookingCommand = New RelayCommand(AddressOf PrintBooking)

            CreateBillingCommand = New RelayCommand(AddressOf CreateBilling)
            EditBillingCommand = New RelayCommand(AddressOf EditBilling, AddressOf CanDoBillingAction)
            DeleteBillingCommand = New RelayCommand(AddressOf DeleteBilling, AddressOf CanDoBillingAction)
            PrintBillingCommand = New RelayCommand(AddressOf PrintBilling, AddressOf CanDoBillingAction)

            If Me.Book.Appointments.Any Then
                ConflictCheck()
            End If

        End Sub

        Private Function CanDoBillingAction(ByVal obj As Object) As Boolean
            If Not IsNothing(SelectedBilling) AndAlso SelectedBilling.BillingStatus = BillingStates.Offen Then
                Return True
            End If
            Return False
        End Function



        Private Sub TermineAdd()

            'Dim aktstart As DateTime
            'Dim aktende As DateTime

            If DateDiff(DateInterval.Minute, Book.Start, Book.Ende) = 0 Then
                Dim dlg As New DialogService
                dlg.ShowAlert("Buchung", "Es muss eine Zeitspanne zwischen Start und Ende liegen")
                Return
            End If


            If Not IsNothing(Book) Then

                Dim am As New AppointmentsManagement(_context, Book)
                am.Generate(Book.Start, Book.Ende, RRule.Freq, RRule.Until, Book.LocationID)

            End If

            ConflictCheck()
        End Sub

        Private Function CanTermineAdd() As Boolean
            If Not IsNothing(Book) Then
                If Not IsNothing(Book.LocationID) And Book.LocationID > 0 Then
                    If RRule.Freq = FrequencyType.None Then
                        Return True
                    Else
                        If Not IsNothing(RRule.Until) Then Return True
                    End If

                End If
            End If
            Return False
        End Function

        Private Sub AddEinzeltermin(ByVal book As Booking, ByVal von As DateTime, ByVal bis As DateTime)



            Dim t As New Appointment
            t.Start = von
            t.Ende = bis
            t.Canceled = False


            t.Location = _context.Locations.Find(book.LocationID)

            't.Location = LocationLookup.Where(Function(o) o.LocationID = book.LocationID).First

            'Dim ft As New FeiertagsService
            'If Not ft.IsHoliday(von) Then
            book.Appointments.Add(t)
            'End If
            'ft = Nothing

            ConflictCheck()


        End Sub

        Private Sub TermineClear()

            For i = Me.Book.Appointments.Count - 1 To 0 Step -1
                _context.Appointments.Remove(Book.Appointments(i))
            Next
            'Do While Book.Appointments.Any
            '    _context.Appointments.Remove(Book.Appointments.First)
            'Loop
            'SelectedBooking.Appointments.Clear()

            ConflictCheck()

        End Sub

        Private Function CanTermineClear(ByVal param As Object) As Boolean
            If Not IsNothing(Book) Then
                If Book.Appointments.Any Then _
                Return True
            End If
            Return False
        End Function

        Private Sub ConflictCheck()

            If Book.Appointments.Any Then
                If Not BGW.IsBusy Then
                    BGW.RunWorkerAsync()
                End If

            End If
        End Sub

        Private Sub EditTermin()
            If Not IsNothing(SelectedApp) Then

                Dim nav As New NavigationService
                Dim ret = nav.ShowAppointment(SelectedApp, _context, False)
                ConflictCheck()

            End If
        End Sub

        Private Sub DelTermin()
            If Not IsNothing(SelectedApp) Then
                _context.Appointments.Remove(SelectedApp)
                ConflictCheck()
            End If
        End Sub

        Private Function CanDelTermin() As Boolean
            Return True
        End Function

        Private Sub OKAction()
            If Not IsNothing(Book.Subject) Then
                If RRule.GetRuleString <> Book.RecurrencePattern Then Book.RecurrencePattern = RRule.GetRuleString
                _context.SaveChanges(MyGlobals.CurrentUsername)
                RaiseEvent RequestClose(Me, Nothing)
            Else
                Dim dlg As New DialogService
                dlg.ShowError("Text", "Sie müssen einen Buchungstext eingeben")
                dlg = Nothing
            End If

        End Sub

        Private Function CanOKAction() As Boolean
            If Book.Appointments.Count > 0 And (Not IsNothing(Book.Subject) OrElse Not Trim(Book.Subject) = "") And Not Book.HasErrors Then Return True
            Return False
        End Function

        Public Sub CancelAction()
            RaiseEvent RequestClose(Me, Nothing)
        End Sub

        Private Sub DeleteAction()
            Dim dlg As New DialogService
            If dlg.AskConfirmation("Löschen", "Wirklich die Buchung inklusive aller Termine und Rechnungen löschen") = True Then

                'Rechnungen checken
                Dim paid As Boolean = False
                ' Rechnungen stornieren
                For i = Book.Billings.Count - 1 To 0 Step -1

                    Dim bill = Book.Billings(i)
                    If bill.BillingStatus = BillingStates.Bezahlt Then
                        paid = True
                    End If
                Next

                If paid = True Then
                    dlg.ShowInfo("Bereits bezahlt", "Mindestens eine Rechnung dieser Buchung wurde bereits bezahlt. Löschen ist nicht möglich")
                    Return
                End If

                'Termine löschen
                For i = Book.Appointments.Count - 1 To 0 Step -1
                    _context.Appointments.Remove(Book.Appointments(i))
                Next
                ' Rechnungen stornieren
                For i = Book.Billings.Count - 1 To 0 Step -1
                    Dim billrepo As New Repositories.BillingRepository(_context)
                    Dim bill = Book.Billings(i)
                    billrepo.StornoOpenBilling(bill)

                Next
                _context.Bookings.Remove(Book)
                _context.SaveChanges()
            End If
            RaiseEvent RequestClose(Me, Nothing)
        End Sub

        Private Sub SetCustomer()
            Dim search = ServiceLocator.Current.GetInstance(Of ISearchCustomer)()
            Dim cs = search.GetCustomer
            If Not IsNothing(cs) Then

                Me.Book.Customer = _context.Customers.Find(cs.CustomerID)
                Me.Verknuepfung = Me.Book.Customer.Suchname
            End If


        End Sub

        Private Sub DelCustomer()
            If Book.Billings.Any Then
                Dim dlg As New DialogService
                dlg.ShowInfo("Kunde", "Dieser Buchung mit diesem Kunden sind Rechnungen zugeordnet. Löschen des Kunden nicht möglich")
                Return
            End If
            Me.Book.Customer = Nothing
            Verknuepfung = ""
        End Sub

        Private Sub History()
            Dim hv = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IHistoryViewer)
            hv.ShowHistory("Booking", Me.Book.BookingID)
        End Sub

        Private Sub PrintBooking()
            Dim rep As New BookingReport(Me.Book)
            rep.CreateDocument()
            rep = Nothing

        End Sub


        Private Sub CreateBilling()
            If Not IsNothing(Book.Customer) Then

                Dim svc As New AccountingTemplateService
                Dim accttpl = svc.GetTemplate()
                If IsNothing(accttpl) Then
                    Return
                End If

                Dim tpl = _context.AccountingTemplates.Find(accttpl.AccountingTemplateID)
                Dim bill As New Billing
                bill.BillingStatus = BillingStates.Offen
                bill.Customer = Book.Customer
                bill.Faelligkeit = Now
                bill.Rechnungsdatum = Now
                bill.CustomerStateID = Book.Customer.CustomerStateID
                bill.Verwendungszweck = "Raum-/Platzbuchung " & Book.Subject
                bill.AccountingTemplateID = tpl.AccountingTemplateID
                bill.KSTextern = tpl.Kostenstelle
                bill.Innenauftrag = tpl.Innenauftrag
                bill.Sachkonto = tpl.Sachkonto
                bill.Steuerkennzeichen = tpl.SteuerkennzeichenAR
                bill.Steuerprozent = tpl.Tax.TaxPercent

                Dim cs As New CustomerService
                If cs.ShowBilling(_context, bill, True) = True Then
                    bill.BruttoOriginal = bill.Brutto
                    Book.Billings.Add(bill)
                Else
                    bill = Nothing
                End If

            Else
                Dim dlg As New DialogService
                dlg.ShowInfo("Kunde", "Sie müssen zuerst einen Kunden auswählen")

            End If
        End Sub



        Private Sub EditBilling()

            If Not IsNothing(SelectedBilling) Then
                Dim bill = SelectedBilling
                If bill.BillingStatus = BillingStates.Bezahlt Then
                    Dim dlg As New DialogService
                    dlg.ShowInfo("Gesperrt", "Diese Rechnung ist bereits bezahlt. Eine Bearbeitung ist nicht mehr möglich")
                    Return
                End If

                Dim cs As New CustomerService
                If cs.ShowBilling(_context, bill, True) = True Then

                Else
                    If _context.Entry(bill).State <> EntityState.Added Then _context.Entry(bill).Reload()
                End If
            End If
        End Sub

        Private Sub DeleteBilling()
            If Not IsNothing(SelectedBilling) Then
                If SelectedBilling.BillingStatus <> BillingStates.Bezahlt Then
                    Dim billrepo As New Repositories.BillingRepository(_context)
                    billrepo.StornoOpenBilling(SelectedBilling)
                    'billrepo.StornoBilling(SelectedBilling, SelectedBilling.AccountID, SelectedBilling.PaymentMethodID, MachineSettingService.GetKasseNr(), True)
                    '_context.Billings.Remove(SelectedBilling)
                    Book.Billings.Remove(SelectedBilling)
                End If

            End If
        End Sub

        Private Sub PrintBilling()
            If Not IsNothing(SelectedBilling) AndAlso Not IsNothing(Book.Customer) Then

                Dim rpBilling = New BillingReplaceObject(SelectedBilling).Output
                Dim rpCustomer = New CustomerReplaceObject(Book.Customer).Output
                Dim rpBooking = New BookingReplaceObject(Book).Output
                Dim rpAppointment = New AppointmentReplaceObject(Book.Appointments.ToList())
                rpAppointment.ListToStringWithNewLine()

                Dim list = rpCustomer.Concat(rpBilling).Concat(rpBooking).Concat(rpAppointment.Output).ToDictionary(Function(kvp) kvp.Key, Function(kvp) kvp.Value)

                Dim doc As New WordAutomation(list)
                doc.Vorschau = True

                doc.Show()
            End If
        End Sub




        Private Sub BGW_DoWork(sender As Object, e As DoWorkEventArgs) Handles BGW.DoWork

            Dim ocappconflicts As New ObservableCollection(Of String)

            Using _context2 As New in1Entities


                'AppConflicts = New ObservableCollection(Of String)


                For i = Me.Book.Appointments.Count - 1 To 0 Step -1

                    Dim app = Me.Book.Appointments(i)
                    'Dim citm = app
                    app.Conflict = False
                    Dim query = From c In _context2.Appointments Where c.LocationID = app.Location.LocationID And
                                ((app.Start >= c.Start And app.Start < c.Ende) Or
                                 (app.Ende > c.Start And app.Ende <= c.Ende) Or
                                 (app.Start <= c.Start And app.Ende >= c.Ende)) _
                             And c.Canceled = False
                                Select c

                    Dim result = query.ToList()
                    If Not IsNothing(result) Then
                        If result.Count > 0 Then



                            For Each q In query
                                If Book.BookingID = q.BookingID Then
                                Else
                                    app.Conflict = True
                                    Dim label As String
                                    If Not IsNothing(q.CourseID) Then
                                        label = q.Course.Sport.Sportname
                                    Else
                                        label = q.Booking.Subject
                                    End If
                                    ocappconflicts.Add(label & " " & q.Start.ToShortDateString & " " & q.Start.ToShortTimeString & " " & q.Ende.ToShortTimeString)
                                End If
                            Next

                        End If
                    End If
                Next
            End Using
            e.Result = ocappconflicts

        End Sub

        Private Sub BGW_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles BGW.ProgressChanged

        End Sub

        Private Sub BGW_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles BGW.RunWorkerCompleted

            If Not IsNothing(AppConflicts) Then
                AppConflicts.Clear()
            Else
                AppConflicts = New ObservableCollection(Of String)
            End If
            Try


                If Not IsNothing(e.Result) Then
                    Dim ocappc = CType(e.Result, ObservableCollection(Of String))
                    For Each itm In ocappc
                        AppConflicts.Add(itm)
                    Next
                End If
            Catch ex As Exception

            End Try

        End Sub



    End Class

End Namespace