﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Input


Namespace ViewModel

    Public Class CourseStaffViewModel
        Inherits ViewModelBase


        Public Event RequestClose As EventHandler

        Public Property OKCommand As ICommand
        Public Property CancelCommand As ICommand

        Private _stdlohn As Decimal
        Public Property StdLohn As Decimal
            Get
                Return _stdlohn
            End Get
            Set(ByVal value As Decimal)
                _stdlohn = value
            End Set
        End Property

        Private _IsDeputy As Boolean
        Public Property IsDeputy As Boolean
            Get
                Return _IsDeputy
            End Get
            Set(value As Boolean)
                _IsDeputy = value
                RaisePropertyChanged("IsDeputy")
            End Set
        End Property

        Private staff As CourseStaff

        Public Sub New(ByVal istaff As CourseStaff)

            staff = istaff
            StdLohn = staff.StdLohn
            IsDeputy = staff.IsDeputy
            OKCommand = New RelayCommand(AddressOf OKAction)
            CancelCommand = New RelayCommand(AddressOf CancelAction)


        End Sub

        Private Sub OKAction()
            staff.StdLohn = StdLohn
            staff.IsDeputy = IsDeputy
            RaiseEvent RequestClose(Me, Nothing)

        End Sub

        Private Sub CancelAction()

            RaiseEvent RequestClose(Me, Nothing)
        End Sub
    End Class

End Namespace