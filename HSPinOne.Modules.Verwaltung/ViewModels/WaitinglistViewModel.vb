﻿Imports System.ComponentModel.Composition
Imports System.Windows.Data
Imports System.ComponentModel
Imports System.Data.Entity
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Windows.Input
Imports CommonServiceLocator
Imports Prism.Regions

Namespace ViewModel
    <Export(GetType(WaitinglistViewModel)), PartCreationPolicy(CreationPolicy.Shared)>
    Public Class WaitinglistViewModel
        Inherits ViewModelBase
        Implements INavigationAware

        Private _vaView As ListCollectionView
        Private _suchtext As String

        Private _selectedterm As Term
        Private ReadOnly _context As in1Entities


        Public Event ScrollintoView As EventHandler

        Private WithEvents bgw As BackgroundWorker


#Region "Properties"


        Public Property SelectedTerm() As Term
            Get
                Return _selectedterm
            End Get
            Set(ByVal value As Term)
                _selectedterm = value
                RaisePropertyChanged("SelectedTerm")
                LadeVa()
            End Set
        End Property

        Private _currentVa As Object

        Public Property CurrentVa As Object
            Get
                Return _currentVa
            End Get
            Set(ByVal value As Object)
                _currentVa = value
                RaisePropertyChanged("CurrentVa")
            End Set
        End Property


        Private _termlookup As IEnumerable(Of Term)

        Public Property TermLookup As IEnumerable(Of Term)
            Get
                Return _termlookup
            End Get
            Set(ByVal value As IEnumerable(Of Term))
                _termlookup = value
            End Set
        End Property

        Public Property VaView As ListCollectionView
            Get
                Return _vaView
            End Get
            Set(ByVal value As ListCollectionView)
                _vaView = value
            End Set
        End Property


        Private _ocCourses As ObservableCollection(Of Course)

        Public Property OcCourses As ObservableCollection(Of Course)
            Get
                Return _ocCourses
            End Get
            Set(value As ObservableCollection(Of Course))
                _ocCourses = value
            End Set
        End Property


        Public Property Suchtext As String
            Get
                Return _suchtext
            End Get
            Set(ByVal value As String)
                _suchtext = value
                RaisePropertyChanged("Suchtext")
            End Set
        End Property


        Public Property Results As Object

        Private _selecteditems As List(Of Integer)

        Private _firstload As Boolean = True

        Public Property FilterResetCommand As ICommand
        Public Property NewVaCommand As ICommand
        Public Property OpenVaCommand As ICommand
        Public Property SearchCommand As ICommand
        Public Property AddCourseCommand As ICommand
        Public Property RefreshCourseCommand As ICommand
        Public Property ExportCommand As ICommand
        Public Property CloneCourseCommand As ICommand
        Public Property TnClipboardCommand As ICommand

#End Region


#Region "   Constructor"

        Public Sub New()

            bgw = New BackgroundWorker

            _context = New in1Entities

            _context.CourseStates.Load()
            Dim query = _context.Terms.Where(Function(o) o.Aktiv).OrderByDescending(Function(o) o.Semesterbeginn)

            TermLookup = query.ToList

            Dim sterm = From c In TermLookup Where c.Semesterbeginn <= Now And c.Semesterende >= Now Select c

            If sterm.Any Then
                SelectedTerm = sterm.First
            Else
                SelectedTerm = TermLookup.First
            End If

            FilterResetCommand = New RelayCommand(AddressOf FilterReset)
            OpenVaCommand = New RelayCommand(AddressOf OpenVa, AddressOf CanOpenVa)
            SearchCommand = New RelayCommand(AddressOf FilterList)
            RefreshCourseCommand = New RelayCommand(AddressOf LadeVa)

            _firstload = False

        End Sub

#End Region

#Region "Procedures"

        Private Sub OnInit()
            LadeVa()
        End Sub

        Private Sub LadeVa()
            If _firstload = True Then Return
            IsBusy = True
            bgw.WorkerSupportsCancellation = False
            If Not bgw.IsBusy Then bgw.RunWorkerAsync()
        End Sub

        Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw.DoWork
            'Dim query1 =
            '        From _
            '        c In _
            '        _context.Courses.Include(Function(o) o.Location).Include(Function(o) o.CourseState).Include(
            '            Function(o) o.Party).Include(Function(o) o.Sport).Include(Function(o) o.Term)
            '        Where c.TermID = SelectedTerm.TermID

            Dim query = From c In _context.Waitinglists Where c.Course.TermID = SelectedTerm.TermID
                        Select c.CourseID, c.Course.Sport.Sportname, c.Course.Zusatzinfo, c.Course.Zeittext, c.Course.Location.LocationName
                        Order By Sportname, Zusatzinfo
                        Group By CourseID, Sportname, Zusatzinfo, Zeittext, LocationName Into Group
                        Select CourseID, Sportname, Zusatzinfo, Zeittext, LocationName, Anzahl = Group.Count()
                        Order By Sportname, Zusatzinfo

            '    Currentlist = CType(CollectionViewSource.GetDefaultView(query.ToList), ListCollectionView)



            e.Result = query.ToList
        End Sub

        Private Sub bgw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) _
            Handles bgw.RunWorkerCompleted

            Dim view = CType(CollectionViewSource.GetDefaultView(e.Result), ListCollectionView)

            If Not IsNothing(view) Then

                view.Filter = New Predicate(Of Object)(AddressOf Suchfilter)

                If view.SortDescriptions.Count = 0 Then
                    view.SortDescriptions.Add(New SortDescription("Sportname", ListSortDirection.Ascending))
                End If

            End If

            VaView = view
            If Not IsNothing(_selecteditems) Then
                For Each itm In VaView
                    If _selecteditems.Contains(itm.CourseID) Then itm.IsSelected = True
                Next
            End If

            RaisePropertyChanged("VaView")

            IsBusy = False
        End Sub


        Private Function Suchfilter(ByVal item As Object) As Boolean
            Dim itm = item

            If Not IsNothing(itm) Then

                If Not IsNothing(Suchtext) And Not Trim(Suchtext) = "" Then
                    If Not IsNothing(itm.Zusatzinfo) Then
                        If itm.Zusatzinfo.ToLower.Contains(Suchtext.ToLower) Then Return True
                    End If

                    If itm.Sportname.ToLower.Contains(Suchtext.ToLower) Then Return True


                Else
                    Return True
                End If

                Return False

            End If
            Return False
        End Function


        Private Sub FilterList()
            If Not IsNothing(VaView) Then VaView.Refresh()
        End Sub


#End Region

#Region "   Command Methods"

        Private Sub FilterReset()
            Suchtext = ""
            FilterList()
        End Sub


        Private Sub OpenVa()
            If Not IsNothing(CurrentVa) Then
                Dim courseid = CType(CurrentVa.CourseID, Integer)
                Dim course = _context.Courses.Find(courseid)
                If Not IsNothing(course) Then
                    Dim cs = ServiceLocator.Current.GetInstance(Of ICourseService)()
                    If cs.ShowCourse(course) = True Then
                        LadeVa()
                        'If _context.Entry(CurrentVa).State = EntityState.Detached Then
                        '    If Not bgw.IsBusy Then bgw.RunWorkerAsync()
                        'End If
                        VaView.Refresh()
                    End If
                End If
            End If
        End Sub

        Private Function CanOpenVa(ByVal param As Object) As Boolean
            Return True
        End Function

#End Region

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
            _context.Dispose()
        End Sub



        Public Overrides Sub OnNavigatedTo(navigationContext As NavigationContext)
            MyBase.OnNavigatedTo(navigationContext)
            OnInit()
        End Sub
    End Class

End Namespace