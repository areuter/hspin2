﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports HSPinOne.Common
Imports System.Windows.Input
Imports System.ComponentModel.Composition
Imports System.IO
Imports System.Windows
Imports System.Windows.Media.Imaging
Imports Prism.Regions

Namespace ViewModel



    <Export(GetType(DMSViewModel)), PartCreationPolicy(CreationPolicy.NonShared)>
    Public Class DMSViewModel
        Inherits ViewModelBase
        Implements INavigationAware


        Private inputpath As String
        Private outputpath As String
        Private _context As in1Entities

        Dim allowedFiletypes As New List(Of String)(New String() {".pdf", ".tif", ".tiff", ".jpg", ".gif", ".png"})

        Private _selectedfile As FileInfo
        Public Property SelectedFile As FileInfo
            Get
                Return _selectedfile
            End Get
            Set(ByVal value As FileInfo)
                _selectedfile = value
                RaisePropertyChanged("SelectedFile")
            End Set
        End Property

        Private _mStream As MemoryStream
        Public Property mStream As MemoryStream
            Get
                Return _mStream
            End Get
            Set(value As MemoryStream)
                _mStream = value
                RaisePropertyChanged("mStream")
            End Set
        End Property

        Private _mPDF As Stream
        Public Property mPDF As Stream
            Get
                Return _mPDF
            End Get
            Set(value As Stream)
                _mPDF = value
                RaisePropertyChanged("mPDF")
            End Set
        End Property



        Private _mImage As BitmapImage
        Public Property mImage As BitmapImage
            Get
                Return _mImage
            End Get
            Set(value As BitmapImage)
                _mImage = value
                RaisePropertyChanged("mImage")
            End Set
        End Property


        Private _cust As Customer
        Public Property Cust As Customer
            Get
                Return _cust
            End Get
            Set(ByVal value As Customer)
                _cust = value
                RaisePropertyChanged("Cust")
            End Set
        End Property

        Private _doc As Document
        Public Property Doc As Document
            Get
                Return _doc
            End Get
            Set(ByVal value As Document)
                _doc = value
                RaisePropertyChanged("Doc")
            End Set
        End Property

        Private _PdfVisible As Visibility = Visibility.Collapsed
        Public Property PdfVisible As Visibility
            Get
                Return _PdfVisible
            End Get
            Set(value As Visibility)
                _PdfVisible = value
                RaisePropertyChanged("PdfVisible")
            End Set
        End Property

        Private _ImageVisible As Visibility = Visibility.Collapsed
        Public Property ImageVisible As Visibility
            Get
                Return _ImageVisible
            End Get
            Set(value As Visibility)
                _ImageVisible = value
                RaisePropertyChanged("ImageVisible")
            End Set
        End Property

        Private _Presets As List(Of String)
        Public Property Presets As List(Of String)
            Get
                Return _Presets ' New List(Of String)(New String() {"Vertrag", "Kündigung", "Stundenzettel", "Lizenz", "Personalbogen"})
            End Get
            Set(value As List(Of String))
                _Presets = value
                RaisePropertyChanged("Presets")
            End Set
        End Property


        Public Property ZuordnenCommand As ICommand
        Public Property LoadNextDocumentCommand As ICommand
        Public Property SelectCustomerCommand As ICommand
        Public Property SelectFileCommand As ICommand
        Public Property ExternalPreviewCommand As ICommand



        Public Sub New()
            inputpath = GlobalSettingService.HolePfad("dmstemppath")
            If Not DirectoryExistsTask.Check(inputpath) Then
                Dim dlg As New DialogService
                dlg.ShowInfo("Pfad", "Der Pfad " & inputpath & " Pfad kann nicht geöffnet werden")
                dlg = Nothing
                inputpath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)
            End If

            outputpath = GlobalSettingService.in1DataFolder(in1Folder.documentsarchive)

            Dim presetstring = GlobalSettingService.HoleEinstellung("DMSType")
            Presets = New List(Of String)
            If Not IsNothing(presetstring) Then
                Dim presetarr = presetstring.Split(",")
                For Each itm In presetarr
                    Presets.Add(Trim(itm))
                Next
            End If

            LoadNextDocumentCommand = New RelayCommand(AddressOf LoadNextDocument)
            SelectFileCommand = New RelayCommand(AddressOf SelectFile)
            SelectCustomerCommand = New RelayCommand(AddressOf SelectCustomer)
            ZuordnenCommand = New RelayCommand(AddressOf Zuordnen, AddressOf CanZuordnen)
            ExternalPreviewCommand = New RelayCommand(AddressOf ExternalPreview)


        End Sub

        Private Sub LoadNextDocument()
            Doc = Nothing
            Dim dlg As New DialogService


            Dim di As New DirectoryInfo(inputpath)
            Dim dlist As FileInfo() = di.GetFiles()   '(*.pdf)

            If dlist.Any Then
                For i = 0 To dlist.Count - 1
                    If allowedFiletypes.Contains(dlist(i).Extension) Then
                        SelectedFile = dlist(i)
                        NewDoc()
                        Exit For
                    End If
                Next

            Else

                dlg.ShowInfo("Information", "Es sind keine Dateien mehr zu finden")

            End If
            dlg = Nothing

        End Sub

        Private Sub SelectFile()
            Doc = Nothing
            Dim dlg As New DialogService
            Dim file = dlg.GetFile("", inputpath)
            If Not IsNothing(file) Then
                Dim f As FileInfo = New FileInfo(file)
                If f.Exists Then
                    If allowedFiletypes.Contains(f.Extension) Then
                        SelectedFile = f
                        inputpath = f.Directory.FullName
                        NewDoc()
                    End If
                End If

            End If
        End Sub

        Private Sub ExternalPreview()
            If Not IsNothing(SelectedFile) Then
                ShellExecute.RunAssociatedProgram(SelectedFile.FullName)
            End If




        End Sub

        Private Sub NewDoc()

            'Stream erstellen
            Try
                IsBusy = True

                If SelectedFile.Extension = ".pdf" Then

                    PdfVisible = Visibility.Visible
                    ImageVisible = Visibility.Collapsed


                    Using fStream As FileStream = File.OpenRead(SelectedFile.FullName)
                        mStream = New MemoryStream
                        mStream.SetLength(fStream.Length)
                        fStream.Read(mStream.GetBuffer(), 0, CInt(fStream.Length))

                    End Using
                    'Dim ms As Stream = Application.GetResourceStream(New Uri(SelectedFile.FullName, UriKind.Relative)).Stream
                    'mPDF = New PdfDocumentSource(mStream)
                    mPDF = mStream
                    RaisePropertyChanged("mStream")


                Else
                    PdfVisible = Visibility.Collapsed
                    ImageVisible = Visibility.Visible



                    Dim bImage = New BitmapImage
                    bImage.BeginInit()

                    bImage.CacheOption = BitmapCacheOption.OnLoad

                    bImage.UriSource = New Uri(SelectedFile.FullName, UriKind.Absolute)

                    bImage.EndInit()
                    mImage = bImage



                End If

                IsBusy = False
                Doc = New Document
                Doc.Erstelldatum = Now

            Catch ex As Exception
                IsBusy = False
                Dim dlg As New DialogService
                dlg.ShowError("Fehler", ex.Message)
            End Try





        End Sub

        Private Sub SelectCustomer()
            Dim cs As New SearchCustomer
            Dim tcust = cs.GetCustomer()
            If Not IsNothing(tcust) Then
                Cust = tcust
            End If
        End Sub

        Private Sub SendBestMail(ByVal doc As Document)
            Dim recipient = Cust.Mail1
            If IsNothing(recipient) Then
                recipient = Cust.Mail2
            End If
            If Not IsNothing(recipient) Then
                Dim mail As New clsMail
                Dim Templatename As String = "Neuvertrag"
                Dim Template As Template
                Using con As New in1Entities
                    Template = con.Templates.First(Function(o) o.TemplateName = Templatename)
                End Using
                If IsNothing(Template) Then
                    Dim dlg As New DialogService
                    dlg.ShowError("Fehler", "Eine E-Mailvorlage >>Neuvertrag<< existiert nicht. Bitte benachrichtigen Sie den Administrator.")
                    Return
                End If
                Dim body As String = ""


                Dim rpCustomer = New CustomerReplaceObject(Cust).Output
                Dim list = rpCustomer.ToDictionary(Function(kvp) kvp.Key, Function(kvp) kvp.Value)


                Dim Replacer As ReplaceMarker

                'Template.TemplateText = Template.TemplateText.Replace(Environment.NewLine, "<br>")
                Replacer = New ReplaceMarker(Template.TemplateText, list)

                'With Replacer
                '    .AddReplacement("briefanrede", Cust.Briefanrede)
                '    .AddReplacement("mandatsreferenz", Cust.CustomerID)

                'End With

                Replacer.DoReplacement()

                mail.Body = Replacer.FinalString

                Replacer.Template = Template.Betreff
                Replacer.DoReplacement()
                mail.Subject = Replacer.FinalString




                mail.AddRecipient(Cust.Mail1)

                Dim fileName = Path.GetTempPath() & doc.Dateiname & Guid.NewGuid().ToString() & "." & doc.Extension

                File.WriteAllBytes(fileName, doc.DocumentData)
                mail.AddAttachment(fileName)


                mail.SendinBackground()
            End If
        End Sub

        Private Sub Zuordnen()

            Dim dlg As New DialogService


            Try

                'Check if File exists

                If File.Exists(SelectedFile.FullName) Then

                    IsBusy = True

                    Doc.DocumentData = File.ReadAllBytes(SelectedFile.FullName)
                    Doc.Extension = Mid(SelectedFile.Extension, 2)


                    Dim fn As String = Mid(Doc.Infotext, 1, 15)
                    For Each c In Path.GetInvalidFileNameChars()
                        fn = fn.Replace(c, "_")
                    Next

                    Doc.Dateiname = fn
                    Doc.DocumentType = 0

                    Dim act As New Activity
                    act.ActivityType = 0
                    act.Comment = Doc.Infotext
                    act.CustomerID = Cust.CustomerID
                    act.Direction = 1
                    act.Zeit = Now
                    act.UserID = MyGlobals.CurrentUserID
                    act.Document = Doc


                    Using con As New in1Entities
                        con.Activities.Add(act)
                        con.SaveChanges()
                    End Using
                    If Doc.Infotext = "Vertrag" Then
                        SendBestMail(Doc)
                    End If
                    File.Delete(SelectedFile.FullName)
                    SelectedFile = Nothing
                    mStream = Nothing
                    mPDF = Nothing
                    Doc = Nothing
                    Cust = Nothing
                    mImage = Nothing
                    IsBusy = False
                Else
                    dlg.ShowError("Fehler", "Originaldatei konnte nicht gefunden werden")
                End If


            Catch ex As Exception
                IsBusy = False
                dlg.ShowError("Fehler beim Kopieren", ex.Message)
            End Try
            dlg = Nothing
        End Sub



        Private Function CanZuordnen()
            If Not IsNothing(Cust) And Not IsNothing(SelectedFile) _
                And Not IsNothing(Doc) Then
                If Not Trim(Doc.Infotext) = "" Then Return True
            End If

            Return False
        End Function

        Protected Overrides Sub Finalize()
            MyBase.Finalize()


        End Sub

    End Class
End Namespace