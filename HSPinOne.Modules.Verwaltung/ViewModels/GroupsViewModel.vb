﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Windows.Input
Imports HSPinOne.Common
Imports System.ComponentModel.Composition
Imports Prism.Regions
Imports System.Data.Entity
Imports System.Windows.Data

Namespace ViewModel



    Public Class GroupsViewModel
        Inherits ViewModelBase
        Implements INavigationAware


        Private _hasChanges As Boolean = False


        Private _Groups As ObservableCollection(Of CustomerGroup)
        Public Property Groups As ObservableCollection(Of CustomerGroup)
            Get
                Return _Groups
            End Get
            Set(value As ObservableCollection(Of CustomerGroup))
                _Groups = value
            End Set
        End Property

        Private _GroupsView As ListCollectionView
        Public Property GroupsView() As ListCollectionView
            Get
                Return _GroupsView
            End Get
            Set(ByVal value As ListCollectionView)
                _GroupsView = value
            End Set
        End Property

        Private _SelectedGroup As CustomerGroup
        Public Property SelectedGroup As CustomerGroup
            Get
                Return _SelectedGroup
            End Get
            Set(value As CustomerGroup)
                _SelectedGroup = value
                RaisePropertyChanged("SelectedGroup")
            End Set
        End Property

        Private _SelectedCustomer As Customer
        Public Property SelectedCustomer As Customer
            Get
                Return _SelectedCustomer
            End Get
            Set(value As Customer)
                _SelectedCustomer = value
                RaisePropertyChanged("SelectedCustomer")
            End Set
        End Property

        Private _context As in1Entities

        Public Property AddCustomerCommand As ICommand
        Public Property DelCustomerCommand As ICommand
        Public Property AddGroupCommand As ICommand
        Public Property DelGroupCommand As ICommand
        Public Property SaveCommand As ICommand
        Public Property CancelCommand As ICommand
        Public Property EmailCommand As ICommand
        Public Property ExportCommand As ICommand



        Public Sub New()

            AddCustomerCommand = New RelayCommand(AddressOf AddCustomer)
            DelCustomerCommand = New RelayCommand(AddressOf DelCustomer)
            AddGroupCommand = New RelayCommand(AddressOf AddGroup)
            DelGroupCommand = New RelayCommand(AddressOf DelGroup)
            SaveCommand = New RelayCommand(AddressOf Save)
            CancelCommand = New RelayCommand(AddressOf CancelChanges)
            EmailCommand = New RelayCommand(AddressOf EmailAction)
            ExportCommand = New RelayCommand(AddressOf ExportAction)

            LoadData()

        End Sub

        Private Sub LoadData()
            If Not IsNothing(_context) Then _context.Dispose()
            _context = New in1Entities
            _context.CustomerGroups.Load()
            Groups = Nothing
            RaisePropertyChanged("Groups")
            SelectedGroup = Nothing
            Groups = _context.CustomerGroups.Local
            GroupsView = CollectionViewSource.GetDefaultView(Groups)
            GroupsView.SortDescriptions.Add(New ComponentModel.SortDescription("Groupname", ComponentModel.ListSortDirection.Descending))
            SortGroups()
            RaisePropertyChanged("Groups")
            RaisePropertyChanged("GroupsView")
            SelectedGroup = GroupsView.CurrentItem
        End Sub

        Private Sub AddGroup()
            SelectedGroup = New CustomerGroup
            SelectedGroup.Groupname = "neue Gruppe"
            _context.CustomerGroups.Add(SelectedGroup)
            GroupsView.Refresh()


        End Sub

        Private Sub SortGroups()
            GroupsView.SortDescriptions.Clear()
            GroupsView.SortDescriptions.Add(New ComponentModel.SortDescription("Groupname", ComponentModel.ListSortDirection.Ascending))

        End Sub


        Private Sub DelGroup()
            If Not IsNothing(SelectedGroup) Then
                Dim dlg As New DialogService
                If dlg.AskCancelConfirmation("Löschen", "Wirklich diese Gruppe löschen") = vbYes Then
                    _context.CustomerGroups.Remove(SelectedGroup)
                    GroupsView.Refresh()
                End If
            End If

        End Sub

        Private Sub AddCustomer()
            If IsNothing(SelectedGroup) Then Return
            Dim search As New SearchCustomer
            Dim cust = search.GetCustomer
            If Not IsNothing(cust) Then
                If Not SelectedGroup.Customers.Where(Function(o) o.CustomerID = cust.CustomerID).Count Then
                    Dim lcust = _context.Customers.Find(cust.CustomerID)
                    SelectedGroup.Customers.Add(lcust)
                    _hasChanges = True
                End If


            End If
        End Sub

        Private Sub DelCustomer()
            If SelectedCustomer Is Nothing Then Return
            SelectedGroup.Customers.Remove(SelectedCustomer)
            _hasChanges = True

        End Sub

        Private Sub EmailAction()
            Dim mail As New clsMail
            For Each itm In SelectedGroup.Customers
                mail.AddBccRecipient(itm.Mail1)
            Next
            mail.Subject = "Rundmail Gruppe " & SelectedGroup.Groupname
            mail.SendinBackground()
            mail = Nothing
        End Sub

        Private Sub ExportAction()
            Dim csv As New CSVExport
            csv.AddLine("Gender", "Nachname", "Vorname", "Strasse", "PLZ", "Ort", "Briefanrede")
            For Each itm In SelectedGroup.Customers
                csv.AddLine(itm.Gender, itm.Nachname, itm.Vorname, itm.Strasse, itm.PLZ, itm.Ort, itm.Briefanrede)
            Next
            csv.Export()
            csv = Nothing
        End Sub

        Private Sub Save()
            _hasChanges = False
            _context.SaveChanges()
        End Sub

        Private Sub CancelChanges()
            _hasChanges = False
            LoadData()
        End Sub






        Public Overrides Sub OnNavigatedFrom(navigationContext As NavigationContext)
            MyBase.OnNavigatedFrom(navigationContext)
            If Not IsNothing(_context) Then
                If _context.HasChanges Or _hasChanges Then
                    Dim dlg As New DialogService
                    If dlg.AskConfirmation("Speichern", "Möchten Sie ihre Änderungen speichern?") = True Then
                        _context.SaveChanges()
                        _hasChanges = False
                    End If
                End If
            End If
        End Sub


    End Class
End Namespace