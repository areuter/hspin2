﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Data
Imports System.ComponentModel.Composition
Imports Prism.Regions
Imports System.Windows.Input

Namespace ViewModel

    Public Class UelDetailViewModel
        Inherits ViewModelBase
        Implements INavigationAware

        Private _rm As IRegionManager

        Private _Customer As Customer
        Public Property Customer As Customer
            Get
                Return _Customer
            End Get
            Set(value As Customer)
                _Customer = value
                RaisePropertyChanged("Customer")
            End Set
        End Property

        Public Property TermID As Integer

        Private _LV As ListCollectionView
        Public Property LV As ListCollectionView
            Get
                Return _LV
            End Get
            Set(value As ListCollectionView)
                _LV = value
                RaisePropertyChanged("LV")
            End Set
        End Property

        Public Property BackCommand As ICommand



        Private Sub LoadData()
            If Not IsNothing(Customer) Then
                Dim liste As New List(Of UelDetail)
                Using con As New in1Entities
                    Dim query = From c In con.AppointmentStaffs Where c.CustomerID = Customer.CustomerID And c.Appointment.Course.TermID = TermID And c.Appointment.Canceled = False
                                Select c.Appointment.CourseID, c.Appointment.Course.Sport.Sportname, c.Appointment.Course.Zusatzinfo, c.Appointment.Course.Zeittext, c.Appointment.Start, c.StdLohn

                    Dim result = query.ToList


                    Dim groupedresult = From c In result
                                        Group c By c.CourseID, c.Sportname, c.Zusatzinfo, c.Zeittext Into Group
                                        Select CourseID, Sportname, Zusatzinfo, Zeittext, Summe = Group.Sum(Function(o) o.StdLohn), Termine = String.Join(", ", Group.Select(Function(o) o.Start.ToShortDateString))



                    For Each itm In groupedresult
                        Dim el As New UelDetail
                        el.CourseID = itm.CourseID
                        el.Sportname = itm.Sportname
                        el.Zusatzinfo = itm.Zusatzinfo
                        el.Zeittext = itm.Zeittext
                        el.Gehalt = itm.Summe
                        el.Termine = itm.Termine
                        liste.Add(el)
                    Next

                End Using
                LV = CType(CollectionViewSource.GetDefaultView(liste), ListCollectionView)
            End If
        End Sub

        Private Sub Back()
            _rm.RequestNavigate(RegionNames.MainRegion, New Uri("UebungsleiterView", UriKind.Relative))
        End Sub



        Public Sub New()
            Me._rm = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IRegionManager)

            BackCommand = New RelayCommand(AddressOf Back)
        End Sub


        Public Overrides Sub OnNavigatedTo(navigationContext As NavigationContext)
            MyBase.OnNavigatedTo(navigationContext)
            TermID = navigationContext.Parameters("TermID")
            Customer = navigationContext.Parameters("Customer")
            LoadData()
        End Sub
    End Class

    Public Class UelDetail
        Public Property CourseID As Integer
        Public Property Sportname As String
        Public Property Zusatzinfo As String
        Public Property Zeittext As String
        Public Property Gehalt As Decimal
        Public Property Termine As String
        Public Property Teilnehmer As Integer

    End Class
End Namespace