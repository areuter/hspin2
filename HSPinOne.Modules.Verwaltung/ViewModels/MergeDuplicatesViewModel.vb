﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Input
Imports HSPinOne.Common
Imports System.ComponentModel

Namespace ViewModel




    Public Class MergeDuplicatesViewModel
        Inherits ViewModelBase

        Public Event RequestClose As EventHandler


#Region "   Properties"

        Private _customer1 As Customer
        Public Property Customer1 As Customer
            Get
                Return _customer1
            End Get
            Set(value As Customer)
                _customer1 = value
                RaisePropertyChanged("Customer1")
            End Set
        End Property

        Private _customer2 As Customer
        Public Property Customer2 As Customer
            Get
                Return _customer2
            End Get
            Set(value As Customer)
                _customer2 = value
                RaisePropertyChanged("Customer2")
            End Set
        End Property

        Private _customerziel As Customer
        Public Property CustomerZiel As Customer
            Get
                Return _customerziel
            End Get
            Set(value As Customer)
                _customerziel = value
                RaisePropertyChanged("CustomerZiel")
            End Set
        End Property


        Private _BusyContent As String
        Public Property BusyContent() As String
            Get
                Return _BusyContent
            End Get
            Set(ByVal value As String)
                _BusyContent = value
                RaisePropertyChanged("BusyContent")
            End Set
        End Property

        Private _ProgressValue As Integer = 0
        Public Property ProgressValue() As Integer
            Get
                Return _ProgressValue
            End Get
            Set(ByVal value As Integer)
                _ProgressValue = value
                RaisePropertyChanged("ProgressValue")
            End Set
        End Property





        Public Property LoadCustomerCommand As ICommand
        Public Property Add1Command As ICommand
        Public Property Add2Command As ICommand
        Public Property CancelCommand As ICommand
        Public Property SaveCommand As ICommand


#End Region

        Private WithEvents _bgw As New BackgroundWorker



        Public Sub New()

            CustomerZiel = New Customer
            CustomerZiel.Gender = "m"
            CustomerZiel.CustomerStateID = 1

            LoadCustomerCommand = New RelayCommand(AddressOf LoadCustomer)
            Add1Command = New RelayCommand(AddressOf Add1)
            Add2Command = New RelayCommand(AddressOf Add2)
            SaveCommand = New RelayCommand(AddressOf Save, AddressOf CanSave)
            CancelCommand = New RelayCommand(AddressOf Cancel)

        End Sub

        Private Sub LoadCustomer(ByVal param As String)
            If Not IsNothing(param) Then

                Dim cs As New SearchCustomer
                Dim cust = cs.GetCustomer
                If Not IsNothing(cust) Then
                    If param = 1 Then
                        Customer1 = cust
                    ElseIf param = 2 Then
                        Customer2 = cust
                    End If
                End If
            End If
        End Sub

        Private Sub Add1(ByVal param As String)
            TransferColumn(Customer1, param)

        End Sub

        Private Sub Add2(ByVal param As String)
            TransferColumn(Customer2, param)
        End Sub

        Private Sub TransferColumn(ByVal fromc As Customer, ByVal param As String)
            If Not IsNothing(param) Then
                If param = "CustomerID" Then CustomerZiel.CustomerID = fromc.CustomerID
                If param = "Vorname" Then CustomerZiel.Vorname = fromc.Vorname
                If param = "Nachname" Then CustomerZiel.Nachname = fromc.Nachname
                If param = "Suchname" Then CustomerZiel.Suchname = fromc.Suchname
                If param = "Mail1" Then CustomerZiel.Mail1 = fromc.Mail1
                If param = "Mail2" Then CustomerZiel.Mail2 = fromc.Mail2
                If param = "Telefon1" Then CustomerZiel.Telefon1 = fromc.Telefon1
                If param = "Telefon2" Then CustomerZiel.Telefon2 = fromc.Telefon2
                If param = "Telefon3" Then CustomerZiel.Telefon3 = fromc.Telefon3
                If param = "Strasse" Then CustomerZiel.Strasse = fromc.Strasse
                If param = "PLZ" Then CustomerZiel.PLZ = fromc.PLZ
                If param = "Ort" Then CustomerZiel.Ort = fromc.Ort
                If param = "Kontonummer" Then CustomerZiel.Kontonummer = fromc.Kontonummer
                If param = "BLZ" Then CustomerZiel.BLZ = fromc.BLZ
                If param = "Kontoinhaber" Then CustomerZiel.Kontoinhaber = fromc.Kontoinhaber
                If param = "IBAN" Then CustomerZiel.IBAN = fromc.IBAN
                If param = "BIC" Then CustomerZiel.BIC = fromc.BIC
                If param = "Passwort" Then CustomerZiel.Passwort = fromc.Passwort
                If param = "Geburtstag" Then CustomerZiel.Geburtstag = fromc.Geburtstag
                If param = "Matrikelnummer" Then CustomerZiel.Matrikelnummer = fromc.Matrikelnummer
                If param = "Kartennummer" Then CustomerZiel.Kartennummer = fromc.Kartennummer
                If param = "CustomerStateID" Then CustomerZiel.CustomerStateID = fromc.CustomerStateID
                If param = "Blocked" Then CustomerZiel.Blocked = fromc.Blocked

            End If
        End Sub

        Private Sub Save()

            If Not _bgw.IsBusy Then
                IsBusy = True
                BusyContent = "Daten werden zusammengefügt"
                _bgw.WorkerReportsProgress = True
                _bgw.RunWorkerAsync()

            End If

        End Sub

        Private Sub bgw_ProgressChanged(ByVal sender As Object, ByVal e As ProgressChangedEventArgs) Handles _bgw.ProgressChanged
            _ProgressValue = e.ProgressPercentage
            Select Case e.ProgressPercentage
                Case 10 : BusyContent = "Tabellen werden abgefragt"
                Case 60 : BusyContent = "Neue Daten werden gespeichert"

                Case 99 : BusyContent = "Daten werden gespeichert"
            End Select

        End Sub

        Private Sub bgw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles _bgw.RunWorkerCompleted
            IsBusy = False
            Dim dlg As New DialogService
            dlg.ShowInfo("Fertig", "Die Daten wurden zusammengefügt!")
            RaiseEvent RequestClose(Me, Nothing)
        End Sub

        Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles _bgw.DoWork
            Dim _context As New in1Entities

            Try



                Dim cust = _context.Customers.Find(CustomerZiel.CustomerID)
                Dim custold As Customer

                If Customer1.CustomerID = CustomerZiel.CustomerID Then
                    custold = _context.Customers.Find(Customer2.CustomerID)
                Else
                    custold = _context.Customers.Find(Customer1.CustomerID)
                End If

                If Not IsNothing(cust) Then

                    ' Zuweisen der neuen Werte
                    cust.Vorname = CustomerZiel.Vorname
                    cust.Nachname = CustomerZiel.Nachname
                    cust.Suchname = CustomerZiel.Suchname
                    cust.Mail1 = CustomerZiel.Mail1
                    cust.Mail2 = CustomerZiel.Mail2
                    cust.Telefon1 = CustomerZiel.Telefon1
                    cust.Telefon2 = CustomerZiel.Telefon2
                    cust.Telefon3 = CustomerZiel.Telefon3
                    cust.Strasse = CustomerZiel.Strasse
                    cust.PLZ = CustomerZiel.PLZ
                    cust.Ort = CustomerZiel.Ort
                    cust.Kontonummer = CustomerZiel.Kontonummer
                    cust.BLZ = CustomerZiel.BLZ
                    cust.Kontoinhaber = CustomerZiel.Kontoinhaber
                    cust.IBAN = CustomerZiel.IBAN
                    cust.BIC = CustomerZiel.BIC
                    cust.Passwort = CustomerZiel.Passwort
                    cust.Geburtstag = CustomerZiel.Geburtstag
                    cust.Matrikelnummer = CustomerZiel.Matrikelnummer
                    cust.Kartennummer = CustomerZiel.Kartennummer
                    cust.Blocked = CustomerZiel.Blocked
                    cust.CustomerStateID = CustomerZiel.CustomerStateID


                    _bgw.ReportProgress(10)
                    ' Zuordnungen beim alten Customer ändern
                    'Aktivities
                    For i = custold.Activities.Count - 1 To 0 Step -1
                        custold.Activities(i).CustomerID = cust.CustomerID
                    Next

                    _bgw.ReportProgress(20)
                    'Billings
                    For i = custold.Billings.Count - 1 To 0 Step -1
                        custold.Billings(i).CustomerID = cust.CustomerID
                    Next

                    _bgw.ReportProgress(25)
                    'Bookings
                    For i = custold.Bookings.Count - 1 To 0 Step -1
                        custold.Bookings(i).CustomerID = cust.CustomerID
                    Next

                    'Course Ansprechpartner Achtung keine Verbindung, deshalb SQL
                    _context.Courses.SqlQuery("UPDATE Course SET AnsprechpartnerID = {0} WHERE AnsprechpartnerID = {1}", cust.CustomerID, custold.CustomerID)

                    _bgw.ReportProgress(30)
                    'Contracts
                    For i = custold.Contracts.Count - 1 To 0 Step -1
                        custold.Contracts(i).CustomerID = cust.CustomerID
                    Next

                    'CourseStaff
                    For i = custold.CourseStaffs.Count - 1 To 0 Step -1
                        custold.CourseStaffs(i).CustomerID = cust.CustomerID
                    Next

                    _bgw.ReportProgress(35)
                    'Checkin
                    For i = custold.Checkins.Count - 1 To 0 Step -1
                        custold.Checkins(i).CustomerID = cust.CustomerID
                    Next

                    _bgw.ReportProgress(40)
                    'CheckinLog
                    For i = custold.CheckinLogs.Count - 1 To 0 Step -1
                        custold.CheckinLogs(i).CustomerID = cust.CustomerID
                    Next



                    'SportsAnsprechpartner
                    For i = custold.SportsAnsprechpartner.Count - 1 To 0 Step -1
                        custold.SportsAnsprechpartner(i).Ansprechpartner_CustomerID = cust.CustomerID
                    Next

                    'SportsObmann
                    For i = custold.SportsObmann.Count - 1 To 0 Step -1
                        custold.SportsObmann(i).Obmann_CustomerID = cust.CustomerID
                    Next

                    'StaffContracts
                    For i = custold.StaffContracts.Count - 1 To 0 Step -1
                        custold.StaffContracts(i).CustomerID = cust.CustomerID
                    Next

                    'Waitinglists - kein Bezug, deshalb SQL
                    _context.Waitinglists.SqlQuery("UPDATE Waitinglist SET CustomerID = {0} WHERE CustomerID = {1}", cust.CustomerID, custold.CustomerID)

                    'Checkins
                    For i = custold.Checkins.Count - 1 To 0 Step -1
                        custold.Checkins(i).CustomerID = cust.CustomerID
                    Next

                    _bgw.ReportProgress(60)
                    'Erstmal alles aktualisieren
                    _context.SaveChanges()

                    _bgw.ReportProgress(90)

                    ''Alten Customer endgültig löschen
                    _context.Customers.Remove(custold)
                    _context.SaveChanges()


                    Customer1 = Nothing
                    Customer2 = Nothing
                    CustomerZiel = Nothing



                End If

            Catch ex As Exception

                MsgBox("Fehler beim Zusammenfügen: " & ex.Message & ex.StackTrace.ToString)
            End Try
        End Sub

        Private Function CanSave() As Boolean
            If Not IsNothing(CustomerZiel) And Not IsNothing(Customer1) And Not IsNothing(Customer2) Then
                If Not IsNothing(CustomerZiel.CustomerID) Then
                    If CustomerZiel.CustomerID = Customer1.CustomerID Or CustomerZiel.CustomerID = Customer2.CustomerID Then
                        If Customer1.CustomerID <> Customer2.CustomerID Then
                            If Not CustomerZiel.HasErrors Then _
                            Return True
                        End If
                    End If
                    End If
            End If
            Return False
        End Function

        Private Sub Cancel()
            RaiseEvent RequestClose(Me, Nothing)

        End Sub
    End Class
End Namespace
