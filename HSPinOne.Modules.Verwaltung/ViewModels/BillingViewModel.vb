﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.Windows.Input

Namespace ViewModel


    Public Class BillingViewModel
        Inherits ViewModelBase

        Private _copy As Billing

        Public Event RequestClose As EventHandler

        Private _Editable As Boolean = True
        Public Property Editable() As Boolean
            Get
                Return _Editable
            End Get
            Set(ByVal value As Boolean)
                _Editable = value
                RaisePropertyChanged("Editable")
            End Set
        End Property



        Private _bill As Billing
        Public Property Bill As Billing
            Get
                Return _bill
            End Get
            Set(value As Billing)
                _bill = value
            End Set
        End Property

        Private _context As in1Entities
        Public Property Context As in1Entities
            Get
                Return _context
            End Get
            Set(value As in1Entities)
                _context = value
            End Set
        End Property


        Private _PaymentMethods As ObservableCollection(Of PaymentMethod)
        Public Property PaymentMethods() As ObservableCollection(Of PaymentMethod)
            Get
                Return _PaymentMethods
            End Get
            Set(ByVal value As ObservableCollection(Of PaymentMethod))
                _PaymentMethods = value

            End Set
        End Property

        Private _accounts As ObservableCollection(Of Account)
        Public Property Accounts As ObservableCollection(Of Account)
            Get
                Return _accounts
            End Get
            Set(value As ObservableCollection(Of Account))
                _accounts = value
            End Set
        End Property

        Private _billingstatuslist As List(Of BillingStatusItem)
        Public Property BillingStatusList As List(Of BillingStatusItem)
            Get
                Return _billingstatuslist
            End Get
            Set(value As List(Of BillingStatusItem))
                _billingstatuslist = value
            End Set
        End Property

        Private _billingstatusitems As Dictionary(Of String, String)
        Public Property Billingstatusitems As Dictionary(Of String, String)
            Get
                Return _billingstatusitems
            End Get
            Set(value As Dictionary(Of String, String))
                _billingstatusitems = value
            End Set
        End Property

        Private _dialogresult As Boolean = False
        Public Property DialogResult As Boolean
            Get
                Return _dialogresult
            End Get
            Set(value As Boolean)
                _dialogresult = value
            End Set
        End Property

        Private _stornogeb As Decimal = 0
        Public Property Stornogeb As Decimal
            Get
                Return _stornogeb
            End Get
            Set(value As Decimal)
                _stornogeb = value
            End Set
        End Property

        Private _costcenterlookup As IEnumerable(Of Costcenter)
        Public Property CostcenterLookup As IEnumerable(Of Costcenter)
            Get
                Return _costcenterlookup
            End Get
            Set(value As IEnumerable(Of Costcenter))
                _costcenterlookup = value
            End Set
        End Property

        Private _ProductTypeLookup As IEnumerable(Of ProductType)
        Public Property ProductTypeLookup As IEnumerable(Of ProductType)
            Get
                Return _ProductTypeLookup
            End Get
            Set(value As IEnumerable(Of ProductType))
                _ProductTypeLookup = value

            End Set
        End Property

        Private _RO As Boolean = False
        Public Property RO() As Boolean
            Get
                Return _RO
            End Get
            Set(ByVal value As Boolean)
                _RO = value
                RaisePropertyChanged("RO")
            End Set
        End Property

        Private _SAPEditable As Boolean = True
        Public Property SAPEditable() As Boolean
            Get
                Return _SAPEditable
            End Get
            Set(ByVal value As Boolean)
                _SAPEditable = value
                RaisePropertyChanged("SAPEditable")
            End Set
        End Property



        Public Property OKCommand As ICommand
        Public Property CancelCommand As ICommand
        Public Property ShowHistoryCommand As ICommand
        Public Property PrintCommand As New RelayCommand(AddressOf PrintAction)




        Public Sub New(ByVal context As in1Entities, ByVal bill As Billing, Optional ByVal beditable As Boolean = False)

            Me.Context = context
            Me.Bill = bill



            If beditable = False Then
                Editable = False
                SAPEditable = False
                RO = True
            End If

            If Threading.Thread.CurrentPrincipal.IsInRole("Administrator") Then
                SAPEditable = True
            End If

            Dim cls As New Billingstatuslist
            Me.BillingStatusList = cls.Liste

            context.PaymentMethods.Load()
            context.Accounts.Load()
            PaymentMethods = context.PaymentMethods.Local
            Accounts = context.Accounts.Local

            Using con As New in1Entities


                CostcenterLookup = (From c In con.Costcenters Select c).ToList
                ProductTypeLookup = (From c In con.ProductTypes Select c).ToList
            End Using
            OKCommand = New RelayCommand(AddressOf OKAction)
            CancelCommand = New RelayCommand(AddressOf CancelAction)
            ShowHistoryCommand = New RelayCommand(AddressOf ShowHistory)


        End Sub

        Private Sub CancelAction()


            RaiseEvent RequestClose(Me, Nothing)
        End Sub

        Private Sub OKAction()

            DialogResult = True
            RaiseEvent RequestClose(Me, Nothing)

        End Sub

        Private Function CanOK() As Boolean
            ' If Bill.Validate() Then Return True
            Return False

        End Function

        Private Sub ShowHistory()
            Dim hv = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IHistoryViewer)
            hv.ShowHistory("Billing", Bill.BillingID)

        End Sub

        Private Sub PrintAction()
            Dim rep As New Templates.BillingDetailReport(Bill.BillingID)

        End Sub
    End Class
End Namespace