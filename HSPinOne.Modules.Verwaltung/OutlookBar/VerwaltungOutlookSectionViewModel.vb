﻿Imports System.Collections.ObjectModel
Imports System.Windows.Controls
Imports HSPinOne.DAL
Imports Prism.Regions
Imports System
Imports HSPinOne.Common
Imports HSPinOne.Infrastructure
Imports System.ComponentModel.Composition
Imports HSPinOne.Modules.Verwaltung.ViewModel
Imports System.ComponentModel
Imports CommonServiceLocator

Public Class VerwaltungOutlookSectionViewModel





    Private _regionmanager As IRegionManager
    'Private _servicelocator As IServiceLocator

    Private _treeViewMenu As ObservableCollection(Of OutlookSectionMenuHyperlinkViewModel)
    Public Property TreeViewMenu As ObservableCollection(Of OutlookSectionMenuHyperlinkViewModel)
        Get
            Return _treeViewMenu
        End Get
        Set(ByVal value As ObservableCollection(Of OutlookSectionMenuHyperlinkViewModel))
            _treeViewMenu = value
        End Set
    End Property

    Private _selected As ListBoxItem
    Public Property Selected As ListBoxItem
        Get
            Return _selected
        End Get
        Set(value As ListBoxItem)
            _selected = value
            Navigate()
        End Set
    End Property




    Public Sub New()


        _regionmanager = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IRegionManager)
        '_servicelocator = ServiceLocator

        Events.EventInstance.EventAggregator.GetEvent(Of NewCustomerEvent).Subscribe(AddressOf NewCustomerEventListener)
        Events.EventInstance.EventAggregator.GetEvent(Of NewCourseEvent).Subscribe(AddressOf NewCourseEventListener)
        Events.EventInstance.EventAggregator.GetEvent(Of NewBookingEvent).Subscribe(AddressOf NewBookingEventListener)

        Events.EventInstance.EventAggregator.GetEvent(Of OutlookSectionChangedEvent)().Subscribe(AddressOf DefaultNav)

        Me.TreeViewMenu = New ObservableCollection(Of OutlookSectionMenuHyperlinkViewModel)




    End Sub



    Private Sub Navigate()
        If Not IsNothing(Selected) Then

            If Not IsNothing(Selected.Tag) Then
                ViewNavigation.NavigateViews(Selected.Tag, _regionmanager)
            End If
        Else
            Dim sel As New ListBoxItem
            sel.Content = "Kunden"
            sel.Tag = "CustomersView"
            Selected = sel

            'ViewNavigation.NavigateViews("CustomersView", _regionmanager)
            'Selected.Content = "Kunden"
        End If
    End Sub

    'Private Sub Navigate(ByVal param As Object)
    '    'Dim type As Type = Me.GetType()
    '    'Dim method As MethodInfo = type.GetMethod(param.MenuItemCommand.ToString)
    '    'If method IsNot Nothing Then
    '    '    method.Invoke(Me, Nothing)
    '    'End If

    '    Dim wc As New WaitCursor
    '    If Not IsNothing(param.MenuItemView) Then
    '        _regionmanager.Regions(RegionNames.RibbonRegion).RequestNavigate(New Uri("/VerwaltungRibbonTab", UriKind.Relative))
    '    Else
    '        _regionmanager.Regions(RegionNames.RibbonRegion).RequestNavigate(New Uri(param.RibbonTab.ToString, UriKind.Relative))
    '    End If
    '    If Not IsNothing(param.MenuItemView) Then
    '        _regionmanager.RequestNavigate(RegionNames.MainRegion, New Uri(param.MenuItemView.ToString, UriKind.Relative))
    '    End If

    '    wc.Dispose()


    'End Sub

#Region "Sub Commands"
    Public Sub NavigateKunden()
        Dim wc As New WaitCursor

        _regionmanager.Regions(RegionNames.RibbonRegion).RequestNavigate(New Uri("/VerwaltungRibbonTab", UriKind.Relative))
        _regionmanager.RequestNavigate(RegionNames.MainRegion, New Uri("/CustomersView", UriKind.Relative))
        wc.Dispose()

    End Sub

    Public Sub NavigateVeranstaltung()
        Dim wc As New WaitCursor
        _regionmanager.Regions(RegionNames.RibbonRegion).RequestNavigate(New Uri("/VerwaltungRibbonTab", UriKind.Relative))
        _regionmanager.Regions(RegionNames.MainRegion).RequestNavigate(New Uri("/CoursesView", UriKind.Relative))
        wc.Dispose()
    End Sub

    Public Sub NavigateLastschriften()
        Dim wc As New WaitCursor
        _regionmanager.Regions(RegionNames.RibbonRegion).RequestNavigate(New Uri("/VerwaltungRibbonTab", UriKind.Relative))
        _regionmanager.RequestNavigate(RegionNames.MainRegion, New Uri("/DtausView", UriKind.Relative))
        wc.Dispose()
    End Sub

    Public Sub NavigateÜbungsleiter()
        Dim wc As New WaitCursor
        _regionmanager.Regions(RegionNames.RibbonRegion).RequestNavigate(New Uri("/VerwaltungRibbonTab", UriKind.Relative))
        _regionmanager.Regions(RegionNames.MainRegion).RequestNavigate(New Uri("/UebungsleiterView", UriKind.Relative))
        wc.Dispose()
    End Sub

    Public Sub NavigateDienstverträge()
        Dim wc As New WaitCursor
        _regionmanager.Regions(RegionNames.RibbonRegion).RequestNavigate(New Uri("/VerwaltungRibbonTab", UriKind.Relative))
        _regionmanager.Regions(RegionNames.MainRegion).RequestNavigate(New Uri("/DienstvertraegeView", UriKind.Relative))
        wc.Dispose()
    End Sub

    Public Sub NavigateDokumentenmanagement()
        Dim wc As New WaitCursor
        _regionmanager.Regions(RegionNames.RibbonRegion).RequestNavigate(New Uri("/VerwaltungRibbonTab", UriKind.Relative))
        _regionmanager.Regions(RegionNames.MainRegion).RequestNavigate(New Uri("/DMSView", UriKind.Relative))
        wc.Dispose()
    End Sub

    Public Sub NavigateWartelisten()
        Dim wc As New WaitCursor
        _regionmanager.Regions(RegionNames.RibbonRegion).RequestNavigate(New Uri("/VerwaltungRibbonTab", UriKind.Relative))
        _regionmanager.Regions(RegionNames.MainRegion).RequestNavigate(New Uri("/WaitinglistView", UriKind.Relative))
        wc.Dispose()
    End Sub

#End Region

    Public Sub DefaultNav(ByVal param As Object)
        If param.Name.ToString.ToLower = "verwaltung" Then

            Navigate()

        End If
    End Sub




    Public Sub NewCourseEventListener()
        Dim cs = ServiceLocator.Current.GetInstance(Of ICourseService)()
        If cs.GetNewCourse Then
            Events.EventInstance.EventAggregator.GetEvent(Of RefreshCourseEvent).Publish(Nothing)
        End If

    End Sub

    Public Sub NewCustomerEventListener()
        Dim cs = ServiceLocator.Current.GetInstance(Of ICustomerService)()
        If cs.GetNewCustomer Then
            Events.EventInstance.EventAggregator.GetEvent(Of RefreshCustomerEvent).Publish(Nothing)
        End If

    End Sub

    Public Sub NewBookingEventListener()

        Dim book As New Booking
        Dim win As New WindowController
        Dim vm As New BookingUserControlViewModel(book)

        Dim uc As New BookingUserControl
        uc.DataContext = vm
        AddHandler vm.RequestClose, AddressOf win.CloseWindow
        AddHandler win.CloseButtonClicked, AddressOf vm.CancelAction
        win.ShowWindow(uc, "Buchungen")

        Events.EventInstance.EventAggregator.GetEvent(Of RefreshCourseEvent).Publish(Nothing)
    End Sub


End Class
