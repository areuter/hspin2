﻿
Imports HSPinOne.Infrastructure
Imports Prism.Ioc
Imports Prism.Modularity
Imports Prism.Regions

Public Class VerwaltungModule
    Implements IModule

    Public Sub RegisterTypes(containerRegistry As IContainerRegistry) Implements IModule.RegisterTypes
        containerRegistry.RegisterInstance(Of ICustomerService)(New CustomerService)
        containerRegistry.RegisterInstance(Of ICourseService)(New CourseService)
        containerRegistry.RegisterInstance(Of ICurrentTermService)(New CurrentTermService)

        containerRegistry.RegisterForNavigation(GetType(CustomersView), "CustomersView")
        containerRegistry.RegisterForNavigation(GetType(CoursesView), "CoursesView")
        containerRegistry.RegisterForNavigation(GetType(DtausView), "DtausView")
        containerRegistry.RegisterForNavigation(GetType(RSView), "RSView")
        containerRegistry.RegisterForNavigation(GetType(OpenBillingsView), "OpenBillingsView")
        containerRegistry.RegisterForNavigation(GetType(UebungsleiterView), "UebungsleiterView")
        containerRegistry.RegisterForNavigation(GetType(UelDetailView), "UelDetailView")
        containerRegistry.RegisterForNavigation(GetType(DienstvertraegeView), "DienstvertraegeView")
        containerRegistry.RegisterForNavigation(GetType(DMSView), "DMSView")
        containerRegistry.RegisterForNavigation(GetType(WaitinglistView), "WaitinglistView")
        containerRegistry.RegisterForNavigation(GetType(GroupsView), "GroupsView")
        containerRegistry.RegisterForNavigation(GetType(CalendarSearchView), "CalendarSearchView")
        containerRegistry.RegisterForNavigation(GetType(VoucherView), "VoucherView")
    End Sub

    Public Sub OnInitialized(containerProvider As IContainerProvider) Implements IModule.OnInitialized

        Dim rm = containerProvider.Resolve(Of IRegionManager)()
        rm.RegisterViewWithRegion(RegionNames.TaskRegion, GetType(VerwaltungOutlookSection))
    End Sub
End Class


