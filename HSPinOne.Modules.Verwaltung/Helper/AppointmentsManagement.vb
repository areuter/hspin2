﻿Imports System.Collections.ObjectModel
Imports System.Linq
Imports System.Collections.Generic
Imports HSPinOne.DAL
Imports System.Data.Entity.SqlServer


Public Class AppointmentsManagement


    Private _apps As New ObservableCollection(Of Appointment)

    Private _currentva As Course
    Private _context As in1Entities
    Private _book As Booking

    Public Sub New(ByVal con As in1Entities, ByVal course As Course)
        _currentva = course
        _context = con
    End Sub

    Public Sub New(ByVal con As in1Entities, ByVal book As Booking)
        _book = book
        _context = con
    End Sub



    Public Sub AddSingleAppointment(ByVal start As DateTime, ByVal ende As DateTime, ByVal locid As Integer)


        'If IsNothing(_currentva.Appointments) Then
        '    _currentva.Appointments = New ObservableCollection(Of Appointment)
        'End If
        If start >= ende Then Return
        ' Feiertagsprüfung



        If IsHoliday(start) Then
            Dim dlg As New HSPinOne.Infrastructure.DialogService
            Dim ant = dlg.AskCancelConfirmation("Ferien/Feiertag", "Der Termin " & start.ToShortDateString & " fällt auf einen Ferien-/Feiertag. Soll er trotzdem eingetragen werden?")
            If ant = System.Windows.MessageBoxResult.No Or ant = System.Windows.MessageBoxResult.Cancel Then Return

        End If

        'Ignore seconds and microseconds


        Dim app As New Appointment
        app.Start = New DateTime(start.Year, start.Month, start.Day, start.Hour, start.Minute, 0, 0)
        app.Ende = New DateTime(ende.Year, ende.Month, ende.Day, ende.Hour, ende.Minute, 0, 0)
        app.Location = _context.Locations.Find(locid)

        'app.LocationID = locid
        If Not IsNothing(_currentva) Then _currentva.Appointments.Add(app)
        If Not IsNothing(_book) Then _book.Appointments.Add(app)



    End Sub

    Public Sub Generate(ByVal start As DateTime, ByVal ende As DateTime, ByVal freq As FrequencyType, ByVal until As Nullable(Of DateTime), ByVal locid As Integer)

        Dim locs As New List(Of Integer)

        If locid = 0 Then
            Return
        End If

        Dim loc = _context.Locations.Find(locid)
        Dim bookfor = loc.Bookfor

        If IsNothing(bookfor) OrElse Trim(bookfor) = "" Then
            locs.Add(locid)
        Else
            Dim arr = Split(loc.Bookfor, ",")
            For i = 0 To UBound(arr)
                If Not IsNothing(arr(i)) Then
                    locs.Add(arr(i))
                End If


            Next

        End If


        For Each itm In locs

            If freq = FrequencyType.None Then
                Dim app As New Appointment
                app.Start = start
                app.Ende = ende
                app.LocationID = itm
                '_currentva.Appointments.Add(app)
                AddSingleAppointment(start, ende, itm)


            ElseIf freq = FrequencyType.Daily Then


                Dim aktstart As DateTime = start
                Dim aktende As DateTime = ende
                Do While aktstart <= New DateTime(Year(until), Month(until), Day(until), 23, 59, 59)
                    AddSingleAppointment(aktstart, aktende, itm)
                    aktstart = DateAdd(DateInterval.Day, 1, aktstart)
                    aktende = DateAdd(DateInterval.Day, 1, aktende)
                Loop

            ElseIf freq = FrequencyType.Weekly Then
                Dim aktstart As DateTime = start
                Dim aktende As DateTime = ende
                Do While aktstart <= New DateTime(Year(until), Month(until), Day(until), 23, 59, 59)
                    AddSingleAppointment(aktstart, aktende, itm)
                    aktstart = DateAdd(DateInterval.Day, 7, aktstart)
                    aktende = DateAdd(DateInterval.Day, 7, aktende)
                Loop
            End If
        Next

    End Sub

    Public Sub DeleteAll()
        If Not IsNothing(_currentva) Then


            For i = _currentva.Appointments.Count - 1 To 0 Step -1
                _context.Appointments.Remove(_currentva.Appointments(i))
            Next

        End If

        If Not IsNothing(_book) Then
            For i = _book.Appointments.Count - 1 To 0 Step -1
                _context.Appointments.Remove(_book.Appointments(i))
            Next
        End If

    End Sub

    Public Function NewAppointment(ByVal locid As Integer) As Appointment
        Dim app As New Appointment
        app.Start = New DateTime(Year(Now), Month(Now), Day(Now), Hour(Now), 0, 0)
        app.Ende = DateAdd(DateInterval.Hour, 1, app.Start)
        app.Location = _context.Locations.Find(locid)
        '_currentva.Appointments.Add(app)
        'AddSingleAppointment(Now, DateAdd(DateInterval.Hour, 1, Now), locid)
        If Not IsNothing(_currentva) Then _currentva.Appointments.Add(app)
        If Not IsNothing(_book) Then _book.Appointments.Add(app)
        Return app


    End Function

    Private Function IsHoliday(ByVal Datum As Date) As Boolean

        Dim dtfrom = Datum.Date
        Dim dttill = dtfrom.AddHours(23).AddMinutes(59).AddSeconds(59)

        Using con As New in1Entities
            Dim res = (From c In con.Holidays Where c.HolidayDate >= dtfrom And c.HolidayDate <= dttill).Count
            If res > 0 Then Return True
        End Using



        Return False
    End Function
End Class
