﻿Imports System.Windows.Data
Imports System.Globalization
Imports System.Windows.Media

Public Class ConflictConverter
    Implements IValueConverter

    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object _
        Implements IValueConverter.Convert
        Dim param = CType(value, Boolean)
        If param = True Then
            Return Brushes.Red
        Else
            Return Brushes.White
        End If
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) _
        As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class
