﻿Imports HSPinOne.DAL
Imports System.Collections.ObjectModel
Imports System.ComponentModel

Public Class AppointmentHelper

    Private _appcollection As ObservableCollection(Of Appointment)
    Private _courseid As Integer



    Public Sub New(ByVal appointments As ObservableCollection(Of Appointment), ByVal courseid As Integer)
        _appcollection = appointments
        _courseid = courseid
    End Sub


    Private Function CheckConflicts() As ObservableCollection(Of String)

        Dim _context2 As New in1Entities
        Dim _appconflicts As New ObservableCollection(Of String)
        For Each app In _appcollection
            Dim citm = app
            app.Conflict = False
            Dim query = From c In _context2.Appointments Where c.LocationID = citm.Location.LocationID And c.Booking.CourseID <> _courseid And _
                        ((citm.Start >= c.Start And citm.Start < c.Ende) Or _
                         (citm.Ende > c.Start And citm.Ende <= c.Ende) Or _
                         (citm.Start <= c.Start And citm.Ende >= c.Ende)) Select c.Start, c.Ende, c.Booking.Course.Sport.Sportname

            If query.Any Then
                app.Conflict = True

                For Each q In query
                    _appconflicts.Add(q.Sportname & " " & q.Start.ToShortDateString & " " & q.Start.ToShortTimeString & " " & q.Ende.ToShortTimeString)
                Next

            End If
        Next
        Return _appconflicts

    End Function

End Class
