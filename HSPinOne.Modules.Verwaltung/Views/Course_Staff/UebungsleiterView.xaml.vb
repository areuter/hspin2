﻿Imports System.ComponentModel.Composition
Imports System.Diagnostics.CodeAnalysis



<Export("UebungsleiterView"), PartCreationPolicy(ComponentModel.Composition.CreationPolicy.NonShared)> _
Public Class UebungsleiterView




    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.DataContext = New ViewModel.UebungsleiterViewModel()

    End Sub



End Class