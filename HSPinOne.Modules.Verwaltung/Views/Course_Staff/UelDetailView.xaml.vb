﻿Imports System.ComponentModel.Composition
Imports System.Diagnostics.CodeAnalysis



<Export("UelDetailView"), PartCreationPolicy(ComponentModel.Composition.CreationPolicy.NonShared)> _
Public Class UelDetailView




    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.DataContext = New ViewModel.UelDetailViewModel()

    End Sub



End Class
