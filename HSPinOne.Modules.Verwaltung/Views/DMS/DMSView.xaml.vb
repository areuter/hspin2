﻿Imports System.ComponentModel.Composition
Imports System.Diagnostics.CodeAnalysis



<Export("DMSView"), PartCreationPolicy(ComponentModel.Composition.CreationPolicy.NonShared)>
Public Class DMSView




    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.DataContext = New ViewModel.DMSViewModel()

    End Sub

End Class
