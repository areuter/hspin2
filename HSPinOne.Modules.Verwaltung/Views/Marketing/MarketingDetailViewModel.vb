﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports HSPinOne.Common
Imports System.Collections.ObjectModel

Public Class MarketingDetailViewModel
    Inherits ViewModelBase

    Public Property Pricerule As Pricerule
    Public Property Couponcount As Integer

    Public Property Discounttypes As New Dictionary(Of Integer, String) From {{0, "Prozentualer Rabatt"}, {1, "Fester Betrag"}}

    Public Property GenerateCommand As New RelayCommand(AddressOf GenerateAction)
    Public Property ExportCommand As New RelayCommand(AddressOf ExportAction, AddressOf CanExportAction)

    Public Sub New()

    End Sub

    Public Sub GenerateAction()

        Dim i As Integer
        For i = 0 To Couponcount - 1
            Dim c As New Coupon
            c.Couponcode = GetCode()
            c.Created_at = Now()
            c.Times_used = 0
            c.Usage_limit = 1
            Pricerule.Coupons.Add(c)

        Next



    End Sub

    Private Sub ExportAction()
        Dim csv As New CSVExport()
        ' Add Header
        csv.AddLine("Couponcode", "Erstelltam", "Verwendet")

        For Each itm In Pricerule.Coupons
            csv.AddLine(itm.Couponcode, itm.Created_at, itm.Times_used)
        Next

        csv.Export()
        Dim dlg As New DialogService()
        dlg.ShowInfo("Export", "Es wurden " & Pricerule.Coupons.Count & " Coupons exportiert")


    End Sub

    Private Function CanExportAction() As Boolean
        If IsNothing(Pricerule) OrElse IsNothing(Pricerule.Coupons) Then Return False
        If Pricerule.Coupons.Count = 0 Then Return False
        Return True
    End Function

    Public Function GetCode()
        Dim random As Random = New Random()
        Dim length As Integer = 8

        Const chars As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        Dim result = New String(Enumerable.Repeat(chars, length).Select(Function(s) s(random.Next(s.Length))).ToArray())
        Return result

    End Function

End Class
