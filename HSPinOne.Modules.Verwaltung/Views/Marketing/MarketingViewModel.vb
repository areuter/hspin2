﻿Imports System.Collections.ObjectModel
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure

Public Class MarketingViewModel
    Inherits ViewModelBase

    Private _Pricesrules As ObservableCollection(Of Pricerule)
    Public Property Pricerules() As ObservableCollection(Of Pricerule)
        Get
            Return _Pricesrules
        End Get
        Set(ByVal value As ObservableCollection(Of Pricerule))
            _Pricesrules = value
        End Set
    End Property

    Public Property NewRuleCommand = New RelayCommand(AddressOf NewRule)


    Private Sub New()

    End Sub

    Private Sub Init()

    End Sub

    Private Sub NewRule()

    End Sub


End Class

Public Class Pricerule
    Property PriceruleID As Integer
    Property Rulename As String
    Property Ruledescription As String
    Property From_date As String
    Property To_date As String
    Property Condition_serialized As String
    Property Discounttype As Integer
    Property Amount As Decimal
    Property Rulelabel As String
    Property Uses_per_coupon As Integer

    Public Overridable Property Coupons As ObservableCollection(Of Coupon)

End Class

Public Class Coupon
    Property CouponID As Integer
    Property PriceruleID As Integer
    Property Couponcode As String
    Property Usage_limit As Integer
    Property Times_used As Integer
    Property Created_at As DateTime

End Class
