﻿Imports System.ComponentModel.Composition

Public Class GroupsView

    Public Sub New()

        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()

        ' Fügen Sie Initialisierungen nach dem InitializeComponent()-Aufruf hinzu.
        Me.DataContext = New ViewModel.GroupsViewModel()
    End Sub

End Class
