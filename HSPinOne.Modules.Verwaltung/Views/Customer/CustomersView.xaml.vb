﻿Imports HSPinOne.Infrastructure
Imports System.ComponentModel.Composition
Imports System.Diagnostics.CodeAnalysis
Imports Prism.Regions
Imports System.Windows.Input


Public Class CustomersView
    Implements INavigationAware




    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.DataContext = New ViewModel.CustomersViewModel()
    End Sub





    Public Function IsNavigationTarget(navigationContext As Prism.Regions.NavigationContext) As Boolean Implements Prism.Regions.INavigationAware.IsNavigationTarget
        Keyboard.Focus(Me.SucheTextBox)
        Return True
    End Function

    Public Sub OnNavigatedFrom(navigationContext As Prism.Regions.NavigationContext) Implements Prism.Regions.INavigationAware.OnNavigatedFrom

    End Sub

    Public Sub OnNavigatedTo(navigationContext As Prism.Regions.NavigationContext) Implements Prism.Regions.INavigationAware.OnNavigatedTo

    End Sub

    Private Sub CustomersView_Loaded(sender As Object, e As System.Windows.RoutedEventArgs) Handles Me.Loaded
        Keyboard.Focus(Me.SucheTextBox)
    End Sub

    Private Sub Hyperlink_RequestNavigate(sender As System.Object, e As System.Windows.Navigation.RequestNavigateEventArgs)

    End Sub
End Class
