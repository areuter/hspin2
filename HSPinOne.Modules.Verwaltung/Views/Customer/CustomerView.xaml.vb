﻿Imports System.ComponentModel.Composition
Imports System.Diagnostics.CodeAnalysis
Imports HSPinOne.Modules.Verwaltung.ViewModel

<Export(GetType(CustomerView)), PartCreationPolicy(CreationPolicy.Shared)> _
Public Class CustomerView


    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()
    End Sub

    ''' <summary>
    ''' Sets the ViewModel.
    ''' </summary>
    ''' <remarks>
    ''' This set-only property is annotated with the <see cref="ImportAttribute"/> so it is injected by MEF with
    ''' the appropriate view model.
    ''' </remarks>
    <Import(), _
        SuppressMessage _
            ("Microsoft.Design", "CA1044:PropertiesShouldNotBeWriteOnly", _
             Justification:="Needs to be a property to be composed by MEF")> _
    Public WriteOnly Property ViewModel() As ViewModel.CustomerViewModel
        Set(ByVal value As ViewModel.CustomerViewModel)
            Me.DataContext = value
        End Set
    End Property

End Class
