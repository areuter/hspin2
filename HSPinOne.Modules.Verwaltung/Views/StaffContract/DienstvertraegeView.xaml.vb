﻿Imports System.ComponentModel.Composition
Imports System.Diagnostics.CodeAnalysis



<Export("DienstvertraegeView"), PartCreationPolicy(ComponentModel.Composition.CreationPolicy.NonShared)> _
Public Class DienstvertraegeView




    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.DataContext = New ViewModel.DienstvertraegeViewModel()

    End Sub


End Class