﻿Imports System.ComponentModel.Composition
Imports System.Diagnostics.CodeAnalysis


Public Class WaitinglistView

    Public Sub New()

        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()

        ' Fügen Sie Initialisierungen nach dem InitializeComponent()-Aufruf hinzu.
        Me.DataContext = New ViewModel.WaitinglistViewModel()
    End Sub


End Class
