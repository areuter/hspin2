﻿Imports System.ComponentModel.Composition
Imports System.Diagnostics.CodeAnalysis



<Export("DtausView"), PartCreationPolicy(ComponentModel.Composition.CreationPolicy.Shared)> _
Public Class DtausView




    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.DataContext = New ViewModel.DtausViewModel()

    End Sub



End Class
