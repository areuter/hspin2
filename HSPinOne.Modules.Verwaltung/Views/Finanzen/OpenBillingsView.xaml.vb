﻿Imports System.ComponentModel.Composition
Imports System.Diagnostics.CodeAnalysis



<Export("OpenBillingsView"), PartCreationPolicy(ComponentModel.Composition.CreationPolicy.Shared)> _
Public Class OpenBillingsView




    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.DataContext = New ViewModel.OpenBillingsViewModel()

    End Sub



End Class
