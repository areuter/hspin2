﻿Imports System.ComponentModel.Composition
Imports System.Diagnostics.CodeAnalysis

<Export(GetType(CourseView)), PartCreationPolicy(CreationPolicy.Shared)> _
Public Class CourseView


    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

    End Sub

    ''' <summary>
    ''' Sets the ViewModel.
    ''' </summary>
    ''' <remarks>
    ''' This set-only property is annotated with the <see cref="ImportAttribute"/> so it is injected by MEF with
    ''' the appropriate view model.
    ''' </remarks>
    <Import()> _
    Public WriteOnly Property ViewModel() As ViewModel.CourseViewModel
        Set(ByVal value As ViewModel.CourseViewModel)
            Me.DataContext = value
            ' Debug.Print(value.CurrentVA.Party.Partycolor)

        End Set
    End Property

End Class
