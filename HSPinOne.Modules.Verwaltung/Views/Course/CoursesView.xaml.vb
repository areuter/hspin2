﻿Imports System.ComponentModel.Composition
Imports Prism.Regions

Imports System.Diagnostics.CodeAnalysis


Public Class CoursesView
    Implements INavigationAware


    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        Me.DataContext = New ViewModel.CoursesViewModel()



        ' Add any initialization after the InitializeComponent() call.
    End Sub



    Public Function IsNavigationTarget(ByVal navigationContext As Prism.Regions.NavigationContext) As Boolean Implements Prism.Regions.INavigationAware.IsNavigationTarget
        Return True

    End Function

    Public Sub OnNavigatedFrom(ByVal navigationContext As Prism.Regions.NavigationContext) Implements Prism.Regions.INavigationAware.OnNavigatedFrom

    End Sub

    Public Sub OnNavigatedTo(ByVal navigationContext As Prism.Regions.NavigationContext) Implements Prism.Regions.INavigationAware.OnNavigatedTo
        Me.SucheTextBox.Focus()
    End Sub



End Class
