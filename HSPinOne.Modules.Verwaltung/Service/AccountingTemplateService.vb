﻿Imports HSPinOne.Infrastructure
Imports HSPinOne.DAL
Public Class AccountingTemplateService


    Public Sub New()

    End Sub

    Public Function GetTemplate() As AccountingTemplate

        Dim vm As New ViewModel.AccountingSelectorViewModel()
        Dim view As New AccountingSelectorView()
        view.DataContext = vm

        Dim win As New WindowController()
        AddHandler vm.RequestClose, AddressOf win.CloseWindow
        AddHandler win.CloseButtonClicked, AddressOf vm.CancelAction

        win.ShowWindow(view, "Buchungsvorlage auswählen")
        If vm.Result = True Then
            Return vm.Selected
        Else
            Return Nothing
        End If

    End Function

End Class
