﻿Imports HSPinOne.DAL

Public Interface ICurrentTermService
    Function GetTerm() As Term
End Interface


Public Class CurrentTermService
    Implements ICurrentTermService


    Public Function GetTerm() As DAL.Term Implements ICurrentTermService.GetTerm
        Dim _context As New in1Entities
        Dim terms = (From c In _context.Terms).ToList

        Dim sterm = terms.Where(Function(o) o.Semesterbeginn <= Now And o.Semesterende >= Now)
        If sterm.Any Then
            Return sterm.First
        Else
            Return terms.Last
        End If

    End Function
End Class
