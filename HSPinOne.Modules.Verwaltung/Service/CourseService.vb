﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.ComponentModel.Composition
Imports System.Windows

Imports System.Data.Entity
Imports HSPinOne.Modules.Verwaltung.ViewModel


Public Interface ICourseService
    Function GetNewCourse(sem As Term) As Boolean
    Function GetNewCourse() As Boolean
    Function ShowCourse(va As Course) As Boolean

End Interface

<Export(GetType(ICourseService)), PartCreationPolicy(CreationPolicy.Shared)> _
Public Class CourseService
    Implements ICourseService

    Private _context As in1Entities

    Public Function GetNewCourse() As Boolean Implements ICourseService.GetNewCourse


        Dim ts As New CurrentTermService
        Dim sem = ts.GetTerm
        Return GetNewCourse(sem)



    End Function

    Public Function GetNewCourse(ByVal c As Course) As Boolean

        Dim va As Course
        va = c

        Dim ts As New CurrentTermService
        Dim sem = ts.GetTerm
        va.TermID = sem.TermID



        va.Anmeldungvon = sem.Showfrom ' Anzeige Online ab
        va.Anmeldungbis = sem.Showtill ' Anzeige Online bis

        If IsNothing(c.DTStart) Then
            va.DTStart = sem.Showfrom
            va.DTEnd = DateAdd(DateInterval.Hour, 1, va.DTStart)
        End If


        va.CourseStateID = 1
        va.Erstelltam = DateTime.Now
        va.Erstelltvon = MyGlobals.CurrentUserID
        va.NewRecord = True

        Return ShowCourse(va)

    End Function

    Public Function GetNewCourse(sem As Term) As Boolean Implements ICourseService.GetNewCourse

        Dim va As New Course
        va.TermID = sem.TermID


        va.Anmeldungvon = sem.Showfrom ' Anzeige Online ab
        va.Anmeldungbis = sem.Showtill ' Anzeige Online bis

        va.DTStart = sem.Showfrom
        va.DTEnd = DateAdd(DateInterval.Hour, 1, va.DTStart)

        va.CourseStateID = 1
        va.Erstelltam = DateTime.Now
        va.Erstelltvon = MyGlobals.CurrentUserID
        va.NewRecord = True

        Return ShowCourse(va)


    End Function




    Public Function ShowCourse(ByVal va As Course) As Boolean Implements ICourseService.ShowCourse


        Dim wc As New WaitCursor

        Dim ret As Boolean = False
        _context = New in1Entities
        Dim mycourse As Course

        Dim shell = Application.Current.MainWindow

        If va.NewRecord = True Then
            _context.Courses.Add(va)
            mycourse = va
        Else
            mycourse = _context.Courses.Find(va.CourseID) '' From c In _context.Courses Where c.CourseID = va.CourseID

            If IsNothing(mycourse) Then
                Return False
            End If
            _context.Billings.Where(Function(o) o.CourseID = va.CourseID).Include(Function(o) o.Customer).Load()
            _context.CourseStaffs.Where(Function(o) o.CourseID = va.CourseID).Include(Function(o) o.Customer).Load()


            ''mycourse = query.FirstOrDefault
        End If

        Try

            Dim vm = New CourseViewModel 'ServiceLocator.Current.GetInstance(Of ViewModel.CourseViewModel)()
            vm.Context = _context
            vm.CurrentVA = mycourse

            Dim view = New CourseView 'ServiceLocator.Current.GetInstance(Of CourseView)()
            view.ViewModel = vm


            Dim rad As New WindowController
            rad.SetCanClose(False)
            AddHandler vm.RequestClose, AddressOf rad.CloseWindow
            AddHandler rad.CloseButtonClicked, AddressOf vm.CloseButtonClicked
            wc.Dispose()


            rad.ShowWindow(view, "Veranstaltung") 'False, Windows.ResizeMode.CanResize, 900, 700, "Veranstaltung", shell)
            Events.EventInstance.EventAggregator.GetEvent(Of RefreshCourseEvent).Publish(Nothing)
            ret = vm.DialogResult

            vm = Nothing
            view = Nothing
            rad = Nothing
            _context.Dispose()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        If Not IsNothing(wc) Then wc.Dispose()

        Return ret
    End Function


End Class
