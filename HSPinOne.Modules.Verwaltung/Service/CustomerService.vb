﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports Prism.Regions

Imports System.Windows
Imports System.ComponentModel.Composition



Public Interface ICustomerService

    Function GetNewCustomer() As Boolean
    Function GetNewCustomerID() As Object
    Function ShowCustomer(ByVal cust As Customer) As Boolean
    Function ShowContract(ByVal context As in1Entities, ByVal dv As Contract, ByVal customerstateid As Integer) As Boolean
    Function ShowBillings(ByVal context As in1Entities, ByVal cust As Customer) As Boolean
    Function ShowStaffContract(ByVal context As in1Entities, ByVal dv As StaffContract) As Boolean
    Function ShowActivity(ByVal context As in1Entities, ByVal act As Activity) As Boolean
End Interface


Public Class CustomerService
    Implements ICustomerService

#Region "   Properties"



    Private _dialogresult As Boolean

    Private _cust As Customer
    Public Property Cust As Customer
        Get
            Return _cust
        End Get
        Set(ByVal value As Customer)
            _cust = value
        End Set
    End Property

    Private _context As in1Entities
    Public Property Context As in1Entities
        Get
            Return _context
        End Get
        Set(ByVal value As in1Entities)
            _context = value
        End Set
    End Property

    Private _vm As ViewModel.CustomerViewModel
    Public Property VM As ViewModel.CustomerViewModel
        Get
            Return _vm
        End Get
        Set(ByVal value As ViewModel.CustomerViewModel)
            _vm = value
        End Set
    End Property

    Private _Win As WindowController
    Public Property Win As WindowController
        Get
            Return _Win
        End Get
        Set(value As WindowController)
            _Win = value
        End Set
    End Property

#End Region

    Public Function GetNewCustomer() As Boolean Implements ICustomerService.GetNewCustomer


        Dim Customer = New Customer
        Customer.Aktiv = True
        Customer.Erstelltam = DateTime.Now
        Customer.Gender = "w"
        Customer.CustomerStateID = 0
        Customer.Nachname = ""
        Customer.Vorname = ""
        Customer.Suchname = ""
        Customer.LSVEnabled = True
        Customer.CountryID = 81
        Customer.Erstelltvon = MyGlobals.CurrentUserID

        Customer.NewRecord = True

        Return ShowCustomer(Customer)

    End Function


    Public Function GetNewCustomerID() As Object Implements ICustomerService.GetNewCustomerID
        Dim Customer = New Customer
        Customer.Aktiv = True
        Customer.Erstelltam = DateTime.Now
        Customer.Gender = "w"
        Customer.CustomerStateID = 0
        Customer.Nachname = ""
        Customer.Vorname = ""
        Customer.Suchname = ""
        Customer.LSVEnabled = True
        Customer.CountryID = 81
        Customer.NewRecord = True

        Customer.Erstelltvon = MyGlobals.CurrentUserID

        ShowCustomer(Customer)
        If Not IsNothing(Customer) Then
            If Not IsNothing(Customer.CustomerID) Then Return Customer.CustomerID
        End If
        Return Nothing

    End Function


    Public Function ShowCustomer(ByVal icust As DAL.Customer) As Boolean Implements ICustomerService.ShowCustomer


        Dim wc As New WaitCursor

        Try


            Using con As New in1Entities

                Dim mycustomer As Customer
                If icust.NewRecord = True Then
                    con.Customers.Add(icust)
                    mycustomer = icust
                Else
                    mycustomer = con.Customers.Find(icust.CustomerID) ' (From c In con.Customers Where c.CustomerID = icust.CustomerID).First
                End If
                Dim ret As Boolean = False

                'Dim shell = Application.Current.Windows.Cast(Of Window).SingleOrDefault(Function(o) o.IsActive = True)


                VM = New ViewModel.CustomerViewModel 'ServiceLocator.Current.GetInstance(Of ViewModel.CustomerViewModel)()
                VM.Context = con
                VM.Customer = mycustomer


                Dim view = New CustomerView ' ServiceLocator.Current.GetInstance(Of CustomerView)()
                view.ViewModel = VM
                Win = New WindowController
                Win.SetCanClose(False)
                AddHandler Win.CloseButtonClicked, AddressOf VM.CloseButtonClicked

                AddHandler VM.RequestClose, AddressOf Win.CloseWindow

                wc.Dispose()
                Win.ShowWindow(view, "Kunde") 'False, System.Windows.ResizeMode.CanResize, 900, 700, "Kunde", shell)
                Return VM.DialogResult
            End Using
            wc.Dispose()
        Catch ex As Exception

            If Not IsNothing(wc) Then wc.Dispose()
            MsgBox(ex.Message)


        End Try


            Return False

    End Function

    Private Sub CloseCustomer()
        If Not IsNothing(Win) Then
            Win.CloseWindow()
        End If
    End Sub



    Public Function ShowContract(ByVal context As in1Entities, ByVal dv As DAL.Contract, ByVal customerstateid As Integer) As Boolean Implements ICustomerService.ShowContract

        Dim uc As New ContractView
        Dim vm As New ViewModel.ContractViewModel(dv, customerstateid, context)

        uc.DataContext = vm
        Dim rad As New WindowController

        AddHandler vm.RequestClose, AddressOf rad.CloseWindow
        AddHandler rad.CloseButtonClicked, AddressOf rad.CloseWindow
        rad.ShowWindow(uc, "Vertrag")
        Return vm.DialogResult
    End Function


    Public Function ShowBillings(ByVal context As DAL.in1Entities, ByVal cust As DAL.Customer) As Boolean Implements ICustomerService.ShowBillings
        Dim uc As New BillingsView
        Dim vm As New ViewModel.BillingsViewModel(context, cust)
        uc.DataContext = vm

        Dim win As New WindowController

        AddHandler vm.RequestClose, AddressOf win.CloseWindow
        AddHandler win.CloseButtonClicked, AddressOf win.CloseWindow
        win.ShowWindow(uc, "Rechnungen")
        Return vm.DialogResult
    End Function

    Public Function ShowBilling(ByVal context As DAL.in1Entities, ByVal bill As DAL.Billing, Optional ByVal editable As Boolean = False) As Boolean

        Dim vm As New ViewModel.BillingViewModel(context, bill, editable)
        Dim d As New BillingDetailView()
        d.DataContext = vm

        Dim win As New WindowController
        AddHandler vm.RequestClose, AddressOf win.CloseWindow
        AddHandler win.CloseButtonClicked, AddressOf win.CloseWindow
        win.ShowWindow(d, "Rechungsdetail")
        Return vm.DialogResult

    End Function

    Public Function ShowStaffContract(ByVal context As DAL.in1Entities, ByVal dv As DAL.StaffContract) As Boolean Implements ICustomerService.ShowStaffContract
        Dim uc As New StaffContractView
        Dim vm As New ViewModel.StaffContractViewModel(context, dv)
        uc.DataContext = vm

        Dim win As New WindowController
        AddHandler vm.RequestClose, AddressOf win.CloseWindow
        AddHandler win.CloseButtonClicked, AddressOf win.CloseWindow
        win.ShowWindow(uc, "Dienstvertrag")
        Return vm.DialogResult

    End Function

    Public Function ShowActivity(ByVal context As DAL.in1Entities, ByVal act As DAL.Activity) As Boolean Implements ICustomerService.ShowActivity
        Dim uc As New ActivityView
        Dim vm As New ViewModel.ActivityViewModel(context, act)
        uc.DataContext = vm

        Dim win As New WindowController
        AddHandler vm.RequestClose, AddressOf win.CloseWindow
        AddHandler win.CloseButtonClicked, AddressOf win.CloseWindow
        win.ShowWindow(uc, "Aktivität")
        Return vm.DialogResult

    End Function


End Class

