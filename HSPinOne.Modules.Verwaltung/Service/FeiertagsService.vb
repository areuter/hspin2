﻿Imports HSPinOne.DAL
Imports System.Data.Entity.SqlServer

Public Class FeiertagsService




    Public Function IsHoliday(ByVal Datum As Date) As Boolean


        Dim dtfrom = Datum.Date
        Dim dttill = dtfrom.AddHours(23).AddMinutes(59).AddSeconds(59)

        Using _context As New in1Entities

            Dim res = (From c In _context.Holidays Where c.HolidayDate >= dtfrom And c.HolidayDate <= dttill).Count
            If res > 0 Then Return True
        End Using


        Return False
    End Function

    Public Sub New()

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()


    End Sub
End Class
