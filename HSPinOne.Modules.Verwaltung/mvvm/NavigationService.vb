﻿

Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Controls
Imports System.Windows.Data

Public NotInheritable Class NavigationService
    Implements INavigationService



    Public Function ShowSearchView() As Long Implements INavigationService.ShowSearchView

        'Dim container = ServiceLocator.Current.GetInstance(Of IUnityContainer)()
        'Dim search = container.Resolve(Of ISearchCustomer)()
        'Dim cust = search.GetCustomer
        'If Not IsNothing(cust) Then
        '    Return cust.CustomerID
        'End If
        Return 0

    End Function



    Public Function ShowAppointment(ByVal app As Appointment, _context As in1Entities, newrecord As Boolean) As Boolean Implements INavigationService.ShowAppointment

        Dim uc As New AppointmentView()
        Dim vm As New ViewModel.AppointmentViewModel(app, _context, newrecord)
        uc.DataContext = vm

        Dim rad As New WindowController
        AddHandler vm.RequestClose, AddressOf rad.CloseWindow
        AddHandler rad.CloseButtonClicked, AddressOf vm.CloseButtonClicked
        rad.ShowWindow(uc, "Termin")
        Return vm.DialogResult

    End Function




    Public Sub ShowStdLohn(ByVal staff As CourseStaff) Implements INavigationService.ShowStdLohn
        Dim uc As New CourseStaffStdLohnView()
        Dim vm As New ViewModel.CourseStaffViewModel(staff)

        uc.DataContext = vm
        Dim win As New WindowController
        AddHandler win.CloseButtonClicked, AddressOf win.CloseWindow
        AddHandler vm.RequestClose, AddressOf win.CloseWindow
        win.ShowWindow(uc, "Stundenlohn")




    End Sub


End Class
