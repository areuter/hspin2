﻿Imports System.Management
Imports System.Security.Cryptography
Imports System.Text

Public Class HardwareHash

    Private Shared _fingerprint As String = String.Empty

    Public Shared Function GetFingerprint() As String
        _fingerprint = GetHash("CPU >> " & cpuId() & "\nBASE >> " & baseId())
        Return _fingerprint
    End Function


    Private Shared Function GetHash(ByVal s As String) As String
        Dim sec As New MD5CryptoServiceProvider
        Dim enc As New ASCIIEncoding
        Dim bt = enc.GetBytes(s)
        Return GetHexString(sec.ComputeHash(bt))


    End Function

    Private Shared Function GetHexString(ByVal bt As Byte()) As String
        Dim s As String = String.Empty
        For i As Integer = 0 To bt.Length - 1
            Dim b As Byte = bt(i)
            Dim n As Integer, n1 As Integer, n2 As Integer
            n = CInt(b)
            n1 = n And 15
            n2 = (n >> 4) And 15
            If n2 > 9 Then
                s += (ChrW(n2 - 10 + AscW("A"c))).ToString()
            Else
                s += n2.ToString()
            End If
            If n1 > 9 Then
                s += (ChrW(n1 - 10 + AscW("A"c))).ToString()
            Else
                s += n1.ToString()
            End If
            If (i + 1) <> bt.Length AndAlso (i + 1) Mod 2 = 0 Then
                s += "-"
            End If
        Next
        Return s
    End Function


    Private Shared Function GetIdentifier(wmiClass As String, wmiProp As String) As String
        Dim res As String = ""
        Dim search = New ManagementObjectSearcher("SELECT * FROM " & wmiClass)
        Try
            Dim result = search.Get()
            For Each r In result
                If res = "" Then
                    res = r.Properties(wmiProp).ToString
                    Exit For
                End If
            Next

        Catch ex As Exception

        End Try
        Return res
    End Function

    Private Shared Function cpuId() As String
        Dim retVal = GetIdentifier("Win32_Processor", "UniqueId")
        If retVal = "" Then
            retVal = GetIdentifier("Win32_Processor", "ProcessorId")
            If retVal = "" Then
                retVal = GetIdentifier("Win32_Processor", "Name")
                If retVal = "" Then
                    retVal = GetIdentifier("Win32_Processor", "Manufacturer")
                End If
                retVal = retVal & GetIdentifier("Win32_Processor", "MaxClockSpeed")
            End If
        End If
        Return retVal
    End Function

    Private Shared Function baseId() As String
        Return GetIdentifier("Win32_BaseBoard", "Model") & _
            GetIdentifier("Win32_BaseBoard", "Manufacturer") & _
            GetIdentifier("Win32_BaseBoard", "Name") & _
            GetIdentifier("Win32_Baseboard", "SerialNumber")

    End Function

End Class
