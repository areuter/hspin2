﻿Imports System.Threading.Tasks



Public Class DirectoryExistsTask


    Public Shared Function Check(ByVal path As String, Optional ByVal timeout As Integer = 5000) As Boolean
        Dim task = New Task(Of Boolean)(Function()
                                            Dim fi = New IO.DirectoryInfo(path)
                                            Return fi.Exists
                                        End Function)
        task.Start()
        Return task.Wait(timeout) AndAlso task.Result
    End Function

End Class
