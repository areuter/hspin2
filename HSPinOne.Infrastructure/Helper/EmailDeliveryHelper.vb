﻿Imports System.Net.Mail
Imports System.Net
Imports System.Threading
Imports System.Text
Imports System.Collections.Generic
Imports System

Public Class EmailDeliveryHelper
    Private ReadOnly _mailAccount As MailAccount
    Private ReadOnly _mail As InOneEmail
    Private ReadOnly _smtpClient As SmtpClient

    Public Sub New(ByVal mailAccount As MailAccount,
                   mail As InOneEmail)

        _smtpClient = New SmtpClient(mailAccount.Host, mailAccount.Port)

        If Not String.IsNullOrEmpty(mailAccount.Username) And Not String.IsNullOrEmpty(mailAccount.Password) Then
            _smtpClient.Credentials = New NetworkCredential(mailAccount.Username, mailAccount.Password)
        End If

        _smtpClient.EnableSsl = mailAccount.Ssl
        _smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network

        _mailAccount = mailAccount
        _mail = mail

        _mail.From = New MailAddress(_mailAccount.From)
        _mail.ReplyToList.Add(_mail.From)


        If Not String.IsNullOrEmpty(_mailAccount.ReplyTo) Then
            _mail.ReplyToList.Add(New MailAddress(_mailAccount.ReplyTo))
        End If

        'If Bcc's in Account add them to message
        If Not String.IsNullOrEmpty(_mailAccount.Bcc) Then
            Dim list() As String = _mailAccount.Bcc.Split(",")
            For Each r As String In list.ToList()
                _mail.Bcc.Add(Trim(r))
            Next
        End If

        'If Cc's in Account add them to message
        If Not String.IsNullOrEmpty(_mailAccount.Cc) Then
            Dim list() As String = _mailAccount.Cc.Replace(" ", "").Split(",")
            For Each c As String In list.ToList()
                _mail.CC.Add(c)
            Next
        End If
    End Sub

    Public Sub Send()
        Me.SendMail()
    End Sub

    Public Sub SendAsync()
        Dim trd As Thread
        trd = New Thread(AddressOf SendMail)
        trd.IsBackground = True
        trd.Start()
    End Sub

    Private Function SendMail() As Object
        Try
            _smtpClient.Send(_mail)
        Catch ex As SmtpFailedRecipientException
            Dim statuscode As SmtpStatusCode = ex.StatusCode
            If (statuscode = SmtpStatusCode.MailboxBusy OrElse statuscode = SmtpStatusCode.MailboxUnavailable OrElse statuscode = SmtpStatusCode.TransactionFailed) Then
                Thread.Sleep(5000)
                _smtpClient.Send(_mail)
            Else
                Throw
            End If
            'Console.WriteLine("Exception caught in CreateMessageWithAttachment(): {0}", ex.ToString())
        End Try
        Return Nothing
    End Function
End Class

Public Class MailAccount

#Region "Constructors and Destructors"

    Public Sub New()
        Me.Id = Guid.NewGuid()
        Me.Port = 25
        Me.Ssl = False
        Me.Active = True
        Me.Host = "localhost"
        Me.Name = "Neuer Mail Account"
        Me.IsLog = False
        Me.LogToCustomer = True
        Me.IsDefault = False
    End Sub

#End Region

#Region "Public Properties"

    Public Property Active As Boolean
    Public Property From As String
    Public Property Bcc As String
    Public Property Cc As String
    Public Property ReplyTo As String
    Public Property Id As Guid
    Public Property Username As String
    Public Property Name As String
    Public Property Password As String
    Public Property Port As Integer
    Public Property Host As String
    Public Property Header As String
    Public Property Note As String
    Public Property Ssl As Boolean
    Public Property IsLog As Boolean
    Public Property LogToCustomer As Boolean
    Public Property IsDefault As Boolean

#End Region
End Class

Public Class InOneEmail
    Inherits MailMessage

    Private _isHtml As Boolean

    Sub New(ByVal recipient As List(Of String), subject As String, body As String)
        Me.Init()
        Me.Body = body
        Me.Subject = subject
        Me.AddRecipients(recipient)
    End Sub

    Sub New(ByVal recipient As String, subject As String, body As String)
        Me.Init()
        Me.Body = body
        Me.Subject = subject
        Me.AddRecipients(recipient)
    End Sub

    Public Property IsHtml() As Boolean
        Get
            Return _isHtml
        End Get
        Set(value As Boolean)
            _isHtml = value
            Me.IsBodyHtml = _isHtml
            If _isHtml Then
                Body = Body.Replace(Environment.NewLine, "<br>")
            End If
        End Set
    End Property




    Private Sub Init()
        Me.BodyEncoding = Encoding.UTF8
        Me.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure Or DeliveryNotificationOptions.OnSuccess
        Me.SubjectEncoding = Encoding.UTF8
    End Sub

    Private Sub AddRecipients(ByVal recipient As String)
        Me.To.Add(recipient)
    End Sub

    Private Sub AddRecipients(ByVal recipients As List(Of String))
        For Each recipient As String In recipients
            Me.To.Add(recipient)
        Next
    End Sub
End Class
