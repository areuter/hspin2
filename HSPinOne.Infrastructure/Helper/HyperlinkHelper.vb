﻿Imports System.Windows

Public Class HyperlinkHelper


    Public Shared ReadOnly IsExternalProperty As DependencyProperty = DependencyProperty.RegisterAttached("IsExternal", GetType(Boolean), GetType(HyperlinkHelper), _
                                                                                                          New UIPropertyMetadata(False, AddressOf OnIsExternalChanged))

    Public Shared Function GetIsExternal(ByVal obj As DependencyObject) As Boolean
        Return CType(obj.GetValue(IsExternalProperty), Boolean)
    End Function

    Public Shared Sub SetIsExternal(ByVal obj As DependencyObject, ByVal value As Boolean)
        obj.SetValue(IsExternalProperty, value)
    End Sub

    Private Shared Sub OnIsExternalChanged(ByVal sender As Object, ByVal e As DependencyPropertyChangedEventArgs)
        Dim hyperlink = CType(sender, Documents.Hyperlink)

        If e.NewValue Then
            AddHandler hyperlink.RequestNavigate, AddressOf Hyperlink_RequestNavigate
        Else
            RemoveHandler hyperlink.RequestNavigate, AddressOf Hyperlink_RequestNavigate
        End If

    End Sub

    Private Shared Sub Hyperlink_RequestNavigate(ByVal sender As Object, ByVal e As Navigation.RequestNavigateEventArgs)
        Process.Start(New ProcessStartInfo(e.Uri.AbsoluteUri))
        e.Handled = True

    End Sub

End Class
