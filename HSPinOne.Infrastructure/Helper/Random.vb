﻿Public Class Random
    Inherits System.Random

    Public Shadows Function [Next]() As Long
        Return CType(Int(MyBase.NextDouble * Long.MaxValue), Long)
    End Function

    Public Shadows Function [Next](ByVal maxValue As Long) As Long
        Return CType(Int(MyBase.NextDouble * maxValue), Long)
    End Function

    Public Shadows Function [Next](ByVal minValue As Long, _
        ByVal maxValue As Long) As Long

        Select Case True
            Case minValue < maxValue
                Return CType(Int(MyBase.NextDouble * (maxValue - minValue)), _
                    Long) + minValue
            Case minValue = maxValue
                Return minValue
            Case Else
                Throw New ArgumentOutOfRangeException
        End Select
    End Function

End Class