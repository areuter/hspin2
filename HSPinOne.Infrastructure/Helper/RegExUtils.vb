﻿Imports System.Text.RegularExpressions
Imports System.Globalization
Public Class RegExUtils


    Public Shared Function IsValidEmail(ByVal email As String) As Boolean
        If (String.IsNullOrWhiteSpace(email)) Then
            Return False
        Else
            Try


                Return Regex.IsMatch(email,
                "^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" &
                "(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250))

            Catch e As RegexMatchTimeoutException
                Return False
            End Try
        End If
        Return False
    End Function
End Class
