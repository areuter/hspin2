﻿Imports System.Reflection

Public Class EnumConstants


    Public Shared Function GetStringsFromClassConstants(ByVal type As System.Type) As String()
        Dim constants As New ArrayList()
        Dim fieldInfos As FieldInfo() =
            type.GetFields(BindingFlags.[Public] Or
                           BindingFlags.[Static] Or
                           BindingFlags.FlattenHierarchy)
        For Each fi As FieldInfo In fieldInfos
            If fi.IsLiteral AndAlso Not fi.IsInitOnly Then
                constants.Add(fi)
            End If
        Next
        Dim ConstantsStringArray As New System.Collections.Specialized.StringCollection
        For Each fi As FieldInfo In DirectCast(constants.ToArray(GetType(FieldInfo)), FieldInfo())
            ConstantsStringArray.Add(CStr(fi.GetValue(Nothing)))
        Next
        Dim retVal(ConstantsStringArray.Count - 1) As String
        ConstantsStringArray.CopyTo(retVal, 0)
        Return retVal
    End Function

End Class
