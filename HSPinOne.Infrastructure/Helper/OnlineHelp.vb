﻿Imports System.Windows
Public Class OnlineHelp

    Public Shared ReadOnly HelpIDProperty As DependencyProperty = DependencyProperty.RegisterAttached("HelpID", GetType(String), GetType(OnlineHelp), _
                                                                                                      New UIPropertyMetadata(False, AddressOf OnIsExternalChanged))

    Public Shared Function GetHelpID(ByVal obj As DependencyObject)
        Return CType(obj.GetValue(HelpIDProperty), Boolean)
    End Function

    Public Shared Sub SetHelpID(ByVal obj As DependencyObject, ByVal value As Boolean)
        obj.SetValue(HelpIDProperty, value)
    End Sub

    Private Shared Sub OnIsExternalChanged(ByVal sender As Object, ByVal e As DependencyPropertyChangedEventArgs)
        Dim hyperlink = CType(sender, Documents.Hyperlink)

        If e.NewValue Then
            AddHandler hyperlink.RequestNavigate, AddressOf Hyperlink_RequestNavigate
        Else
            RemoveHandler hyperlink.RequestNavigate, AddressOf Hyperlink_RequestNavigate
        End If

    End Sub

    Private Shared Sub Hyperlink_RequestNavigate(ByVal sender As Object, ByVal e As Navigation.RequestNavigateEventArgs)
        Process.Start(New ProcessStartInfo(e.Uri.AbsoluteUri))
        e.Handled = True

    End Sub

End Class
