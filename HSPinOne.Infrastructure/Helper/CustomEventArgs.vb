﻿Public Class CustomEventArgs
    Inherits EventArgs

    Public Result As Object
    Public Resultstring As String

    Public Sub New()
        Result = Nothing
        Resultstring = String.Empty
    End Sub
End Class
