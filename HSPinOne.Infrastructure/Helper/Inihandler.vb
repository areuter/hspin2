﻿Imports System.IO
Imports IniParser
Imports IniParser.Model

Public Class Inihandler

    Const inifolder As String = "in1"
    Const iniext As String = ".ini"

    Private _parser = New FileIniDataParser
    Private _inidata As New IniData
    Private _inifile As String

    Public Enum Inifiles
        user
        window
    End Enum

    Public Sub New(ByVal inifile As Inifiles)
        _inifile = inifile.ToString.ToLower & iniext
        Load()
    End Sub

    Private Sub Load()
        'Liegt in Appdata/Roaming eine Ini Datei?
        Dim appdata = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
        Dim folder = Path.Combine(appdata, inifolder)
        Dim fn = Path.Combine(folder, _inifile)
        If File.Exists(fn) Then
            _inidata = _parser.ReadFile(fn)
        End If
    End Sub

    Public Function GetValue(ByVal section As String, ByVal key As String, ByVal defaultvalue As String) As String
        If IsNothing(_inidata) Then Return ""

        Dim result = _inidata(section)(key)
        If Not String.IsNullOrEmpty(result) Then
            Return result
        Else
            Return defaultvalue
        End If
    End Function

    Public Sub SetValue(ByVal section As String, ByVal key As String, ByVal value As String)
        If Not _inidata.Sections.ContainsSection(section) Then
            _inidata.Sections.AddSection(section)
        End If

        If Not _inidata(section).ContainsKey(key) Then
            _inidata(section).AddKey(key, value)
        Else
            _inidata(section)(key) = value
        End If

    End Sub


    Public Sub Save()

        Dim appdata = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
        Dim folder = Path.Combine(appdata, inifolder)
        Dim fn = Path.Combine(folder, _inifile)

        If Not Directory.Exists(folder) Then
            Directory.CreateDirectory(folder)
        End If

        'If File.Exists(fn) Then
        '    File.Delete(fn)
        'End If
        _parser.WriteFile(fn, _inidata)

    End Sub

End Class
