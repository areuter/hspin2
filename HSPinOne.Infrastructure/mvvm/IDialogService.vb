﻿Imports System.Windows

Public Interface IDialogService

    Sub ShowInfo(ByVal title As String, ByVal message As String)

    Sub ShowError(ByVal title As String, ByVal message As String)

    Sub ShowAlert(ByVal title As String, ByVal message As String)

    Function AskConfirmation(ByVal title As String, ByVal message As String) As Boolean

    Function AskCancelConfirmation(ByVal title As String, ByVal message As String) As MessageBoxResult

    Function GetFile(ByVal filter As String, Optional ByVal initDir As String = "") As Object

    Function SaveFile(ByVal filter As String, ByVal filename As String, ByVal ext As String) As String


End Interface
