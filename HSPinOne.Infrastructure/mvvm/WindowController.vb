﻿Imports System.Windows.Controls
Imports System.Windows
Imports System.Configuration
Imports HSPinOne.UIControls
Imports System.Windows.Input


Public Interface IWindowController
    Sub CloseWindow()
    Sub ShowWindow(ByVal content As UserControl, ByVal headerText As String, Optional ByVal dialog As Boolean = True)

    Sub ShowWindow(ByVal content As UserControl, ByVal isRestricted As Boolean, ByVal resizeMode As ResizeMode,
                   ByVal width As Long, ByVal height As Long, ByVal headerText As String)

    Sub ShowWindow(ByVal content As UserControl, ByVal isRestricted As Boolean, ByVal resizeMode As ResizeMode,
                   ByVal width As Long, ByVal height As Long, ByVal headerText As String, ByVal owner As Object, Optional ByVal dialog As Boolean = True)


    Sub SetCanClose(ByVal value As Boolean)

    Event MainWindowClosed As EventHandler
    Event MainWindowClosing As EventHandler
    Event CloseButtonClicked As EventHandler

    ReadOnly Property DialogResult As Boolean

    Property Owner As Object
End Interface

Public Class WindowController
    Implements IWindowController

    Dim _radwindow As CustomWindow
    'Dim _radwindow As CWindow
    Private _headertext As String
    Private _content As UserControl
    Private _dialog As Boolean = True


    Public Property Owner As Object Implements IWindowController.Owner
        Get
            Return Me._radwindow.Owner
        End Get
        Set(ByVal value As Object)
            Me._radwindow.Owner = value
        End Set
    End Property

    Private _WindowTitle As String = String.Empty

    Public Property WindowTitle As String
        Get
            Return _WindowTitle
        End Get
        Set(value As String)
            _WindowTitle = value
        End Set
    End Property


    Public Event MainWindowClosed(ByVal sender As Object, ByVal e As EventArgs) _
        Implements IWindowController.MainWindowClosed

    Public Event MainWindowClosing(ByVal sender As Object, ByVal e As EventArgs) _
        Implements IWindowController.MainWindowClosing

    Public Event CloseButtonClicked(ByVal sender As Object, ByVal e As EventArgs) _
        Implements IWindowController.CloseButtonClicked

    Private _dialogresult As Boolean

    Public ReadOnly Property DialogResult As Boolean Implements IWindowController.DialogResult
        Get
            Return _dialogresult
        End Get
    End Property

    Public Sub New()
        _radwindow = New CustomWindow
        '_radwindow = New CWindow
        AddHandler _radwindow.CloseButtonClicked, AddressOf CloseButtonClickedHandler
        AddHandler _radwindow.Closed, AddressOf Window_Closed
        AddHandler _radwindow.RequestClose, AddressOf Window_Closing
    End Sub

    Public Sub CloseWindow() Implements IWindowController.CloseWindow
        _radwindow.CanClose = True
        If Not _radwindow.IsClosing Then _radwindow.Close()
    End Sub

    Public Sub HideTitle()
        _radwindow.ShowTitleBar = False
        _radwindow.ShowInTaskbar = False
        _radwindow.ShowCloseButton = False
    End Sub
    ''' <summary>
    ''' CanClose im Windows ist default auf true
    ''' Wenn das Fenster beim Klick auf X erst per Nachfrage geschlossen werden soll, dann False
    ''' </summary>
    ''' <param name="value">Schließen ohne Nachfrage?</param>
    ''' <remarks></remarks>
    Public Sub SetCanClose(ByVal value As Boolean) Implements IWindowController.SetCanClose
        _radwindow.CanClose = value
    End Sub



    Public Sub ShowWindow(ByVal content As UserControl, ByVal isRestricted As Boolean, ByVal resizeMode As ResizeMode,
                          ByVal width As Long, ByVal height As Long, ByVal headerText As String) _
        Implements IWindowController.ShowWindow


        Dim shell = Application.Current.Windows.Cast (Of Window).SingleOrDefault(Function(o) o.IsActive = True)

        ShowWindow(content, False, resizeMode, width, height, headerText, shell)
    End Sub

    Public Sub ShowWindow(ByVal content As UserControl, ByVal isRestricted As Boolean, ByVal resizeMode As ResizeMode,
                          ByVal width As Long, ByVal height As Long, ByVal headerText As String, ByVal owner As Object, Optional ByVal dialog As Boolean = True) _
        Implements IWindowController.ShowWindow

        _radwindow.SetContent(content)
        _radwindow.Title = (headerText & " - HSPinONE").ToUpper
        _radwindow.Width = width
        _radwindow.Height = height
        _radwindow.WindowStartupLocation = WindowStartupLocation.CenterOwner
        '_radwindow.CanResize = True
        _radwindow.ShowInTaskbar = False
        _radwindow.Owner = owner
        _radwindow.ShowDialog()
        '_dialogresult = _radwindow.DialogResult
    End Sub



    Public Sub ShowWindow(ByVal content As UserControl, ByVal headerText As String, Optional ByVal dialog As Boolean = True) _
        Implements IWindowController.ShowWindow

        _content = content
        _radwindow.SetContent(_content)
        _headertext = headerText
        _radwindow.Title = (_headertext & " - HSPinONE").ToUpper
        '_radwindow.CanResize = True
        Dim shell = Application.Current.Windows.Cast(Of Window).SingleOrDefault(Function(o) o.IsActive = True)

        _radwindow.Owner = shell
        _radwindow.ShowInTaskbar = False

        Dim defaultwidth = content.Width
        Dim defaultheight = content.Height + 50
        If Not defaultwidth > 150 Then defaultwidth = 200
        If Not defaultheight > 150 Then defaultheight = 200

        'LoadSettings(defaultwidth, defaultheight)
        LoadWindowSettings(defaultwidth, defaultheight)


        content.Width = Double.NaN
        content.Height = Double.NaN

        AddHandler _radwindow.Loaded, AddressOf Window_Loaded
        _radwindow.SizeToContent = SizeToContent.Manual

        If dialog = True Then
            _radwindow.ShowDialog()
        Else
            _radwindow.Show()
        End If

        '_dialogresult = _radwindow.DialogResult
    End Sub

    Private Sub LoadSettings(ByVal defaultwidth As Long, ByVal defaultheight As Long)
        'Versuche die Window Größen aus den User Settings zu laden
        If My.Settings.Properties.Item("Top_" & _headertext) Is Nothing Then
            AddProperty("Top_" & _headertext, GetType(Double), 50)
        End If
        Dim Top = GetProperty("Top_" & _headertext)

        If My.Settings.Properties.Item("Left_" & _headertext) Is Nothing Then
            AddProperty("Left_" & _headertext, GetType(Double), 50)
        End If
        Dim Left = GetProperty("Left_" & _headertext)


        If My.Settings.Properties.Item("Width_" & _headertext) Is Nothing Then
            AddProperty("Width_" & _headertext, GetType(Double), defaultwidth)
        End If
        Dim Width = GetProperty("Width_" & _headertext)


        If My.Settings.Properties.Item("Height_" & _headertext) Is Nothing Then
            AddProperty("Height_" & _headertext, GetType(Double), defaultheight)
        End If
        Dim Height = GetProperty("Height_" & _headertext)


        If My.Settings.Properties.Item("WindowState_" & _headertext) Is Nothing Then
            AddProperty("WindowState_" & _headertext, GetType(WindowState), System.Windows.WindowState.Normal)
        End If
        Dim WindowState = GetProperty("WindowState_" & _headertext)


        If IsNothing(Height) Then
            Height = _content.Height
            If Height > SystemParameters.VirtualScreenHeight Then
                Height = SystemParameters.VirtualScreenHeight
            End If
        End If
        If IsNothing(Width) Then
            Width = _content.Width
            If Width > SystemParameters.VirtualScreenWidth Then
                Width = SystemParameters.VirtualScreenWidth
            End If
        End If
        If IsNothing(Top) Then Top = 50
        If IsNothing(Left) Then Left = 50
        If IsNothing(WindowState) Then WindowState = System.Windows.WindowState.Normal

        If Top + Height/2 > SystemParameters.VirtualScreenHeight Then
            Top = SystemParameters.VirtualScreenHeight - Height
        End If

        If Left + Width/2 > SystemParameters.VirtualScreenWidth Then
            Left = SystemParameters.VirtualScreenWidth - Width
        End If

        If Top < 0 Then
            Top = 0
        End If

        If Left < 0 Then
            Left = 0
        End If

        _radwindow.Top = Top
        _radwindow.Left = Left
        _radwindow.Width = Width
        _radwindow.Height = Height
    End Sub

    Private Sub LoadWindowSettings(ByVal defaultwidth As Long, ByVal defaultheight As Long)
        Dim ih As New Inihandler(Inihandler.Inifiles.window)

        Dim windowname = _headertext

        Dim strTop = ih.GetValue(windowname, "Top", 200)
        Dim Top = Convert.ToDecimal(strTop)

        Dim strLeft = ih.GetValue(windowname, "Left", 200)
        Dim Left = Convert.ToDecimal(strLeft)

        Dim strWidth = ih.GetValue(windowname, "Width", 800)
        Dim Width = Convert.ToDecimal(strWidth)

        Dim strHeight = ih.GetValue(windowname, "Height", 600)
        Dim Height = Convert.ToDecimal(strHeight)

        Dim strState = ih.GetValue(windowname, "WindowState", 0)
        Dim WindowState = Convert.ToInt32(Convert.ToDecimal(strState))

        If IsNothing(Height) Then
            Height = _content.Height
            If Height > SystemParameters.VirtualScreenHeight Then
                Height = SystemParameters.VirtualScreenHeight
            End If
        End If
        If IsNothing(Width) Then
            Width = _content.Width
            If Width > SystemParameters.VirtualScreenWidth Then
                Width = SystemParameters.VirtualScreenWidth
            End If
        End If

        If Top + Height / 2 > SystemParameters.VirtualScreenHeight Then
            Top = SystemParameters.VirtualScreenHeight - Height
        End If

        If Left + Width / 2 > SystemParameters.VirtualScreenWidth Then
            Left = SystemParameters.VirtualScreenWidth - Width
        End If

        If Top < 0 Then
            Top = 0
        End If

        If Left < 0 Then
            Left = 0
        End If

        _radwindow.Top = Top
        _radwindow.Left = Left
        _radwindow.Width = Width
        _radwindow.Height = Height
    End Sub

    Private Sub SaveSettings()
        Try

            If Not IsNothing(_headertext) Then
                If _radwindow.WindowState <> WindowState.Minimized Then

                    Dim ih As New Inihandler(Inihandler.Inifiles.window)

                    Dim windowname = _headertext
                    ih.SetValue(windowname, "Top", _radwindow.Top)
                    ih.SetValue(windowname, "Left", _radwindow.Left)
                    ih.SetValue(windowname, "Width", _radwindow.Width)
                    ih.SetValue(windowname, "Height", _radwindow.Height)
                    ih.SetValue(windowname, "WindowState", _radwindow.WindowState)

                    ih.Save()

                    'My.Settings.Item("Top_" & _headertext) = _radwindow.Top

                    'My.Settings.Item("Left_" & _headertext) = _radwindow.Left

                    'My.Settings.Item("Width_" & _headertext) = _radwindow.Width

                    'My.Settings.Item("Height_" & _headertext) = _radwindow.Height

                    'My.Settings.Item("WindowState_" & _headertext) = _radwindow.WindowState

                    'My.Settings.Save()
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub AddProperty(ByVal propertyName As String, ByVal propertyType As Type, ByVal defaultValue As Object)
        Dim providerName As String = "LocalFileSettingsProvider"

        Dim sp As SettingsProvider = My.Settings.Providers.Item(providerName)

        Dim prop As New SettingsProperty(propertyName)
        prop.Provider = sp
        prop.PropertyType = propertyType
        prop.IsReadOnly = False
        prop.Attributes.Add(GetType(UserScopedSettingAttribute), New UserScopedSettingAttribute)
        prop.DefaultValue = defaultValue
        My.Settings.Properties.Add(prop)
    End Sub

    Private Function GetProperty(ByVal propertyName As String) As Object
        Try
            Return My.Settings.Item(propertyName)
        Catch ex As Exception

        End Try

        Return My.Settings.Properties.Item(propertyName).DefaultValue
    End Function


    Private Sub CloseButtonClickedHandler()
        RaiseEvent CloseButtonClicked(Me, Nothing)
    End Sub

    Private Sub Window_Closed(ByVal sender As Object, ByVal e As EventArgs)
        'SetDefaultWindowValues()
        _radwindow.Content = Nothing
        SaveSettings()
        RaiseEvent MainWindowClosed(Me, Nothing)
    End Sub

    Private Sub Window_Closing(ByVal sender As Object, ByVal e As EventArgs)
        SaveSettings()
        RaiseEvent MainWindowClosing(Me, Nothing)
    End Sub

    Private Sub Window_Loaded(ByVal sender As Object, ByVal e As EventArgs)
        'Focus in das neue aufgemachte Fenster verschieben
        _radwindow.MoveFocus(New TraversalRequest(FocusNavigationDirection.Next))
    End Sub

    Private Sub SetDefaultWindowValues()
        '_radwindow.IsRestricted = False
        _radwindow.ResizeMode = ResizeMode.NoResize
        _radwindow.WindowStartupLocation = WindowStartupLocation.CenterOwner
    End Sub
End Class

