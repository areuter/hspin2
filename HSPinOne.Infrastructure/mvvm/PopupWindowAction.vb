﻿Imports System.Windows
Imports System.Windows.Controls

Public Class PopupWindowAction
    Implements IPopupWindowAction

    Public Function GetWindow(ByVal windowcontent As UserControl) As Window Implements IPopupWindowAction.GetWindow
        Dim wrapperWindow As Window
        wrapperWindow = New Window

        wrapperWindow.Content = windowcontent
        wrapperWindow.ShowDialog()
        Return wrapperWindow
    End Function

End Class
