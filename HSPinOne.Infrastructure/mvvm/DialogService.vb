﻿Imports System.Windows

Public NotInheritable Class DialogService
    Implements IDialogService



    Public Function AskConfirmation(ByVal title As String, ByVal message As String) As Boolean Implements IDialogService.AskConfirmation
        Return MessageBox.Show(message, title, MessageBoxButton.YesNo, MessageBoxImage.Question) = MsgBoxResult.Yes
    End Function

    Public Function AskCancelConfirmation(ByVal title As String, ByVal message As String) As System.Windows.MessageBoxResult Implements IDialogService.AskCancelConfirmation
        Return MessageBox.Show(message, title, MessageBoxButton.YesNoCancel, MessageBoxImage.Question)
    End Function

    Public Sub ShowAlert(ByVal title As String, ByVal message As String) Implements IDialogService.ShowAlert
        MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Asterisk)
    End Sub

    Public Sub ShowError(ByVal title As String, ByVal message As String) Implements IDialogService.ShowError
        MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Error)
    End Sub

    Public Sub ShowInfo(ByVal title As String, ByVal message As String) Implements IDialogService.ShowInfo
        MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Exclamation)
    End Sub

    Public Function GetFile(ByVal filter As String, Optional ByVal initDir As String = "") As Object Implements IDialogService.GetFile

        Dim dlg As New Microsoft.Win32.OpenFileDialog()
        If Trim(initDir) <> "" Then _
            dlg.InitialDirectory = initDir
        dlg.Filter = "Alle Dokumente (.*)|*.*"
        dlg.Multiselect = False
        Dim result As Boolean = dlg.ShowDialog()
        If result = True Then
            Return dlg.FileName
        End If
        Return Nothing

    End Function

    Public Function SaveFile(ByVal filter As String, ByVal filename As String, ByVal ext As String) As String Implements IDialogService.SaveFile

        Dim dlg As New Microsoft.Win32.SaveFileDialog()
        dlg.FileName = filename
        dlg.DefaultExt = ext
        dlg.Filter = filter

        Dim result As Boolean = dlg.ShowDialog()
        If result = True Then
            Return dlg.FileName
        End If
        Return Nothing

    End Function

   

End Class
