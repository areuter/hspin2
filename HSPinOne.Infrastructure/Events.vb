﻿Imports Prism.Events


Public Class Events


    'Private Shared _Event As New EventAggregator
    'Public Shared ReadOnly Property EVAG() As EventAggregator
    '    Get
    '        Return _Event
    '    End Get
    'End Property

    Private Shared ReadOnly _eventInstance As Events = New Events()
    Private _eventAggregator As IEventAggregator

    Public Shared ReadOnly Property EventInstance As Events
        Get
            Return _eventInstance
        End Get
    End Property

    Public ReadOnly Property EventAggregator As IEventAggregator
        Get

            If _eventAggregator Is Nothing Then
                _eventAggregator = New EventAggregator()
            End If

            Return _eventAggregator
        End Get
    End Property

End Class

Public Class DateChangedEvent
    Inherits PubSubEvent(Of DateTime)
    Public Property Tag As DateTime
End Class

Public Class ViewChangedEvent
    Inherits PubSubEvent(Of String)
    Public Property location As Long
End Class

Public Class UpdateScheduleEvent
    Inherits PubSubEvent(Of Object)

End Class

Public Class PrintScheduleEvent
    Inherits PubSubEvent(Of Object)
End Class

Public Class OutlookSectionChangedEvent
    Inherits PubSubEvent(Of Object)
    Public Property OutlookSection As Object
End Class

Public Class RefreshCustomerEvent
    Inherits PubSubEvent(Of Object)

End Class

Public Class RefreshCourseEvent
    Inherits PubSubEvent(Of Object)

End Class



Public Class DashboardRefreshEvent
    Inherits PubSubEvent(Of String)
    Public Property msg As String
End Class

Public Class SaveSettingEvent
    Inherits PubSubEvent(Of String)
End Class

Public Class NewCustomerEvent
    Inherits PubSubEvent(Of Object)
End Class

Public Class NewCourseEvent
    Inherits PubSubEvent(Of Object)
End Class

Public Class NewBookingEvent
    Inherits PubSubEvent(Of Object)
End Class

Public Class LoginEvent
    Inherits PubSubEvent(Of Object)
End Class

Public Class CheckinFocusEvent
    Inherits PubSubEvent(Of Object)
End Class




