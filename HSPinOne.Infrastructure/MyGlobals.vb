﻿

Public Class MyGlobals
    Inherits ViewModelBase

    Public Shared ConnString As String


    Public Shared CurrentUser As DAL.IdentityBusinessObject

    Public Shared CurrentUsername As String
    Public Shared LoggedOn As Boolean


    Public Shared ReadOnly Property CurrentUserID As Long
        Get
            Dim user = CurrentUser
            If Not IsNothing(user) Then
                Return user.UserId
            Else
                Return 0
            End If

        End Get
    End Property

    Public Shared LongRunningProcess As Boolean = False


    Private Shared _Fingerprint As String
    Public Shared Property Fingerprint As String
        Get

            Return _Fingerprint
        End Get
        Set(value As String)
            _Fingerprint = value
        End Set

    End Property

    Public Shared LastBon As Guid

    Public Shared ClipboardString As String

    Public Shared MachineSettings As List(Of HSPinOne.DAL.MachineConfig)

    Public Shared Function QuioEnabled() As Boolean
        Dim q = MachineSettings.Where(Function(o) o.Configname = "Quio" And o.Machinename = Fingerprint).FirstOrDefault
        Dim res As Boolean = False
        If Not IsNothing(q) Then
            If q.Machinesetting = "1" Or q.Machinesetting = "True" Then res = True
        End If

        Return res
    End Function










End Class
