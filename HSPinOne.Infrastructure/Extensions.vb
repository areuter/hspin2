﻿Imports Microsoft.VisualBasic
Imports System.Runtime.CompilerServices

Public Module Extensions

    <Extension()> _
    Public Function nz(Of T)(ByVal Value As T, ByVal Replacement As T) As T

        Try
            If Value Is Nothing OrElse IsDBNull(Value) Then
                Return Replacement
            Else
                Return Value
            End If
        Catch e As Exception
            Return Replacement
        End Try

    End Function

End Module
