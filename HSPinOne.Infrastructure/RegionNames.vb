﻿Public Class RegionNames

    Public Const MenuRegion As String = "MenuRegion"
    Public Const MainRegion As String = "MainRegion"
    Public Const TaskRegion As String = "TaskRegion"
    Public Const DetailRegion As String = "DetailRegion"
    Public Const NavRegion As String = "NavRegion"
    Public Const RibbonRegion As String = "RibbonRegion"
    Public Const PopupRegion As String = "PopupRegion"
    Public Const SettingRegion As String = "SettingRegion"
    Public Const SecondaryRegion As String = "SecondaryRegion"
End Class
