﻿Imports System.Windows.Data
Imports System.Windows

Public Class BooleanToVisibilityorHiddenConverter
    Implements IValueConverter

    Private _Collapse As Boolean = False
    Public Property Collapse As Boolean
        Get
            Return _Collapse
        End Get
        Set(value As Boolean)
            _Collapse = value
        End Set
    End Property

    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.Convert
        Dim bValue = CType(value, Boolean)
        If Not IsNothing(bValue) Then
            If bValue = True Then
                Return Visibility.Visible
            Else
                If _Collapse Then
                    Return Visibility.Collapsed
                Else
                    Return Visibility.Hidden
                End If
            End If
        Else
            If _Collapse Then
                Return Visibility.Collapsed
            Else
                Return Visibility.Hidden
            End If
        End If


    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class
