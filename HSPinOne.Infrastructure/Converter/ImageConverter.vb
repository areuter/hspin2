﻿Imports System.Windows.Data
Imports System.Windows.Controls
Imports System.Windows.Media
Imports System.IO

Public Class ImageConverter
    Implements IValueConverter


    Public Function Convert(value As Object, targetType As System.Type, parameter As Object, culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.Convert



        Dim cust = TryCast(value, HSPinOne.DAL.Customer)

        '' Gender default
        Dim genderlist = New String() {"m", "w"}



        Dim img As New Imaging.BitmapImage
        img.BeginInit()
        img.CacheOption = Imaging.BitmapCacheOption.OnLoad

        If Not IsNothing(cust) Then

            ' Foto nach Geschlecht festlegen
            Dim genderimg As String = "m"
            If (genderlist.Contains(cust.Gender)) Then
                genderimg = cust.Gender
            End If

            'Foto laden falls existent
            If Not IsNothing(cust.DocumentUID) Then

                Using con As New HSPinOne.DAL.in1Entities
                    Dim inp = (From c In con.Documents Where c.DocumentUID = cust.DocumentUID Select c.DocumentUID, c.DocumentData).SingleOrDefault

                    If Not IsNothing(inp) Then
                        Dim stream As New MemoryStream(inp.DocumentData)

                        img.StreamSource = stream
                    Else

                        img.UriSource = New Uri("pack://application:,,,/HSPinOne.WPF.Shell;component/images/" & genderimg & ".png")

                    End If

                End Using


            Else
                img.UriSource = New Uri("pack://application:,,,/HSPinOne.WPF.Shell;component/images/" & genderimg & ".png")
            End If
        Else
            img.UriSource = New Uri("pack://application:,,,/HSPinOne.WPF.Shell;component/images/unknown.jpg")
        End If

        img.EndInit()
        Return img

    End Function

    Public Function ConvertBack(value As Object, targetType As System.Type, parameter As Object, culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.ConvertBack
        Throw New NotImplementedException

    End Function
End Class
