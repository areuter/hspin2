﻿Imports System.Windows.Data
Imports System.Globalization
Imports System.Windows.Controls
Imports System.Windows.Media
Imports System.Windows
Imports Enumerable = System.Linq.Enumerable

Public Class StringToBoolean
    Implements IValueConverter

#Region "IValueConverter Members"

    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object _
        Implements IValueConverter.Convert

        Dim aktiviert = IsNothing(value) Or Trim(value) = ""

        Return aktiviert
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) _
        As Object _
        Implements IValueConverter.ConvertBack

        ' Not required.
        Return Nothing
    End Function

#End Region
End Class


Public Class DateToWota
    Implements IValueConverter

    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object _
        Implements IValueConverter.Convert
        If IsDate(value) Then
            Return WeekdayName(Weekday(value, FirstDayOfWeek.Monday), True)
        Else
            Return Nothing
        End If
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) _
        As Object Implements IValueConverter.ConvertBack
        Return Nothing
    End Function
End Class

Public Class ListviewLinenumbers
    Implements IValueConverter

    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object _
        Implements IValueConverter.Convert
        Dim itm = CType(value, ListViewItem)
        Dim lv As ListView = CType(ItemsControl.ItemsControlFromItemContainer(itm), ListView)
        Dim index = lv.ItemContainerGenerator.IndexFromContainer(itm)
        Return (index + 1).ToString
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) _
        As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class

Public Class HexToColor
    Implements IValueConverter

    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object _
        Implements IValueConverter.Convert
        If Not IsNothing(value) AndAlso value.ToString() <> String.Empty Then
            Dim c = ColorConverter.ConvertFromString(value.ToString)
            Return New SolidColorBrush(Color.FromRgb(c.R, c.G, c.B))

        Else
            Return New SolidColorBrush(Color.FromRgb(50, 50, 50))
        End If
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) _
        As Object Implements IValueConverter.ConvertBack
        'Throw New NotImplementedException
        Return value.ToString
    End Function
End Class


Public Class ArgbToColor
    Implements IValueConverter

    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object _
        Implements IValueConverter.Convert
        If Not IsNothing(value) AndAlso value.ToString() <> String.Empty Then
            Dim newcolor = ColorConverter.ConvertFromString(value.ToString)
            Return newcolor
        Else
            Return Color.FromRgb(255, 255, 255)
        End If
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) _
        As Object Implements IValueConverter.ConvertBack
        ' Return New ColorConverter().ConvertFrom(value.ToString)
        Return value.ToString
    End Function
End Class

Public Class NothingToVisibility
    Implements IValueConverter

    Public Function Convert(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object,
                            ByVal culture As CultureInfo) As Object Implements IValueConverter.Convert

#If DEBUG Then
        Return Visibility.Visible
#Else
        If Not IsNothing(value) Then
            Return System.Windows.Visibility.Hidden
        Else
            Return System.Windows.Visibility.Visible
        End If
#End If
    End Function

    Public Function ConvertBack(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object,
                                ByVal culture As CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class


Public Class NothingToHidden
    Implements IValueConverter

    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object _
        Implements IValueConverter.Convert

#If DEBUG Then
        Return Visibility.Visible
#End If

        If Not IsNothing(value) Then
            If TypeOf value Is String Then
                If Not Trim(value) = "" Then
                    Return Visibility.Visible
                End If
            Else
                Return Visibility.Visible
            End If
        End If

        Return Visibility.Hidden
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) _
        As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class

Public Class BooleanToInverseVisibilityConverter
    Implements IValueConverter

    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object _
        Implements IValueConverter.Convert


        If Not IsNothing(value) Then
            If TypeOf value Is Boolean Then
                If Not value Then
                    Return Visibility.Visible
                End If
            Else
                Return Visibility.Collapsed
            End If
        End If

        Return Visibility.Collapsed
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) _
        As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class



Public Class BusyToHidden
    Implements IValueConverter

    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object _
        Implements IValueConverter.Convert


        If value Then
            Return Visibility.Hidden
        Else
            Return Visibility.Visible
        End If
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) _
        As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class

Public Class EnumMatchToBooleanConverter
    Implements IValueConverter

    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object _
        Implements IValueConverter.Convert
        If value Is Nothing OrElse parameter Is Nothing Then
            Return False
        End If

        Dim checkValue As String = value.ToString()
        Dim targetValue As String = parameter.ToString()
        Return checkValue.Equals(targetValue, StringComparison.InvariantCultureIgnoreCase)
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) _
        As Object Implements IValueConverter.ConvertBack
        If value Is Nothing OrElse parameter Is Nothing Then
            Return Nothing
        End If

        Dim useValue As Boolean = CBool(value)
        Dim targetValue As String = parameter.ToString()
        If useValue Then
            Return targetValue
        End If

        Return Nothing
    End Function
End Class

Public Class BooleanToXConverter
    Implements IValueConverter

    Public Function Convert(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object,
                            ByVal culture As CultureInfo) As Object Implements IValueConverter.Convert
        If value Is Nothing OrElse value = False Then
            Return ""
        Else
            Return "X"
        End If
    End Function

    Public Function ConvertBack(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object,
                                ByVal culture As CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class

Public Class BooleanInverseConverter
    Implements IValueConverter

    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object _
        Implements IValueConverter.Convert
        If value Is Nothing OrElse value = False Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) _
        As Object Implements IValueConverter.ConvertBack
        If value Is Nothing OrElse value = True Then
            Return False
        Else
            Return True
        End If
    End Function
End Class


Public Class GeoToDouble
    Implements IValueConverter

    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object _
        Implements IValueConverter.Convert
        If Not IsNothing(value) Then

            Return value.ToString.Replace(",", ".")
        Else
            Return Nothing
        End If
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) _
        As Object Implements IValueConverter.ConvertBack
        Dim geo As Double
        If Not IsNothing(value) Then
            If Double.TryParse(value.Replace(".", ","), geo) Then
                Return geo
            Else
                Return ""
            End If
        End If
        Return ""
    End Function
End Class


Public Class BackgroundConverter
    Implements IValueConverter

    Public Function Convert(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object,
                            ByVal culture As CultureInfo) As Object Implements IValueConverter.Convert
        Dim item As ListBoxItem = DirectCast(value, ListBoxItem)
        Dim listView As ListBox = TryCast(ItemsControl.ItemsControlFromItemContainer(item), ListBox)
        ' Get the index of a ListViewItem
        Dim index As Integer = listView.ItemContainerGenerator.IndexFromContainer(item)

        If index Mod 2 = 0 Then
            Return Brushes.LightGray
        Else
            Return Brushes.Transparent
        End If
    End Function

    Public Function ConvertBack(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object,
                                ByVal culture As CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException()
    End Function
End Class


Public Class WeekdayToString
    Implements IValueConverter

    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object _
        Implements IValueConverter.Convert
        If IsNumeric(value) Then
            Return WeekdayName(value, True)
        Else
            Return "N.N."
        End If
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) _
        As Object Implements IValueConverter.ConvertBack
        Return Nothing
    End Function
End Class