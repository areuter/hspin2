﻿Imports System.Windows.Data
Imports System.Windows.Media

Public Class ContrastConverter
    Implements IValueConverter



    Public Function Convert(value As Object, targetType As System.Type, parameter As Object, culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.Convert
        If Not IsNothing(value) Then
            Try
                If Brightness(value) < 150 Then
                    Return Brushes.White
                End If
            Catch ex As Exception
                Return Brushes.Black
            End Try

        End If
        Return Brushes.Black

    End Function

    Public Function ConvertBack(value As Object, targetType As System.Type, parameter As Object, culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function

    Private Function Brightness(ByVal b As SolidColorBrush)
        Dim c = b.Color
        Dim sum = (c.R ^ 2 * 0.241) + (c.G ^ 2 * 0.691) + (c.B ^ 2 * 0.068)
        Return CType(Math.Sqrt(sum), Integer)
    End Function
End Class
