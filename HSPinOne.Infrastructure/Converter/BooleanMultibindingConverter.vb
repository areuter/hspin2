﻿Imports System.Globalization
Imports System.Windows
Imports System.Windows.Data

Namespace Converter


    Public Class BooleanMultibindingConverter
        Implements IValueConverter

        Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object Implements IValueConverter.Convert

            If IsArray(value) Then
                Dim curr As Boolean = False
                For Each itm In value
                    curr = curr And itm
                Next
                Return curr
            End If
            Return False
        End Function

        Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object Implements IValueConverter.ConvertBack
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace