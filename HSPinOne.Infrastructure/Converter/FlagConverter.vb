﻿Imports System.Windows.Data
Imports System.Windows.Controls
Imports System.Windows.Media
Imports System.Globalization

Public Class FlagConverter
    Implements IValueConverter


    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object Implements IValueConverter.Convert

        Dim ret = New Dictionary(Of String, String)
        If Not IsNothing(value) Then
            Dim flags = Split(value, ";")
            If Not IsNothing(flags) Then
                For Each flag In flags
                    Dim itm = Split(flag, ",")
                    If UBound(itm) = 1 Then
                        'Dim color = ColorConverter.ConvertFromString(itm(1))
                        ret.Add(itm(0), itm(1))
                    End If
                Next
            End If
        End If
        Return ret
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class
