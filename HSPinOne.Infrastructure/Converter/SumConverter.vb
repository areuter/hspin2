﻿Imports System.Windows.Data

Public Class SumConverter
    Implements IValueConverter



    Public Function Convert(value As Object, targetType As System.Type, parameter As Object, culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.Convert
        If Not IsNothing(value) Then
            Dim summe As Decimal = 0
            For Each itm In value
                summe = itm(parameter)
            Next
            Return summe
        End If
        Return 0
    End Function

    Public Function ConvertBack(value As Object, targetType As System.Type, parameter As Object, culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class
