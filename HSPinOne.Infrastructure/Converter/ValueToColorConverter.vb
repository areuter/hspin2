﻿
Imports System.Globalization
Imports System.Windows.Data
Imports System.Windows.Media

Namespace Converter
    Public Class ValueToColorConverter
        Implements IValueConverter

        Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object Implements IValueConverter.Convert

            Dim brush As SolidColorBrush = New SolidColorBrush(Colors.DarkGreen)

            Dim doubleValue As Double = 0.0
            Double.TryParse(value.ToString(), doubleValue)

            If (doubleValue < 0) Then
                brush = New SolidColorBrush(Colors.Red)
            End If
            Return brush


        End Function

        Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object Implements IValueConverter.ConvertBack
            Throw New NotImplementedException()
        End Function
    End Class
End Namespace
