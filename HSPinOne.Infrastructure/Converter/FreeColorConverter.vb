﻿Imports System.Globalization
Imports System.Windows.Data
Imports System.Windows.Media

Public Class FreeColorConverter
    Implements IValueConverter


    Public Function Convert(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object Implements IValueConverter.Convert

        Dim data As Integer = -1
        Dim color = Brushes.Transparent

        Dim res = Integer.TryParse(value, data)
        If res Then
            If res Then
                Select Case data
                    Case 0 : color = Brushes.DarkRed
                    Case 1 To 5 : color = Brushes.DarkOrange
                    Case > 5 : color = Brushes.DarkGreen
                    Case Else : color = Brushes.Transparent
                End Select
            End If
        End If
        Return color
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, culture As CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function
End Class
