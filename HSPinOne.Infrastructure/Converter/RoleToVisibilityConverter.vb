﻿Imports System.Windows.Data
Imports System.Threading
Imports System.Security.Principal
Imports System.Windows
Imports System.Globalization

Public Class RoleToVisibilityConverter
    Implements IValueConverter


    Public Function Convert(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As CultureInfo) As Object Implements IValueConverter.Convert
        Dim principal = CType(value, IPrincipal)
        If Not IsNothing(principal) Then
            If principal.IsInRole(parameter) Then
                Return Visibility.Collapsed
            Else
                Return Visibility.Visible
            End If
        End If
        Return Nothing

    End Function

    Public Function ConvertBack(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException
    End Function

End Class
