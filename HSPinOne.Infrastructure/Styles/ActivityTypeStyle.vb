﻿Imports System.Windows.Controls
Imports System.Windows

Public Class AcitvityTypeStyle
    Inherits StyleSelector
    Public Overrides Function SelectStyle(item As Object, container As DependencyObject) As Style
        If Not IsNothing(item.ActivityType) And TypeOf item.ActivityType Is Integer Then

            If item.ActivityType = 1 Then
                Return Mail
            ElseIf item.ActivityType = 2 Then
                Return Phone
            ElseIf item.ActivityType = 3 Then
                Return fax
            Else
                Return Message
            End If
        End If
        Return Nothing
    End Function

    Private _phone As Style
    Public Property Phone() As Style
        Get
            Return _phone
        End Get
        Set(value As Style)
            _phone = value
        End Set
    End Property

    Private _mail As Style
    Public Property Mail() As Style
        Get
            Return _mail
        End Get
        Set(value As Style)
            _mail = value
        End Set
    End Property

    Private _fax As Style
    Public Property Fax() As Style
        Get
            Return _fax
        End Get
        Set(value As Style)
            _fax = value
        End Set
    End Property

    Private _message As Style
    Public Property Message() As Style
        Get
            Return _message
        End Get
        Set(value As Style)
            _message = value
        End Set
    End Property


End Class
