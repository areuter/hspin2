﻿Imports System.Windows.Controls
Imports System.Windows
Public Class DirectionStyle
        Inherits StyleSelector
        Public Overrides Function SelectStyle(item As Object, container As DependencyObject) As Style
        If Not IsNothing(item.Direction) And TypeOf item.Direction Is Integer Then

            If item.Direction = 1 Then
                Return Incoming
            Else
                Return Outgoing
            End If
        End If
            Return Nothing
        End Function

    Private _in As Style
    Public Property Incoming() As Style
        Get
            Return _in
        End Get
        Set(value As Style)
            _in = value
        End Set
    End Property

    Private _out As Style
    Public Property Outgoing() As Style
        Get
            Return _out
        End Get
        Set(value As Style)
            _out = value
        End Set
    End Property

End Class
