﻿Imports System.Windows

Public MustInherit Class WindowBase
    Inherits System.Windows.Window


    Public ReadOnly Property IsInDesignMode() As Boolean
        Get
            Return System.ComponentModel. _
                    DesignerProperties.GetIsInDesignMode(Me)
        End Get
    End Property

   
End Class
