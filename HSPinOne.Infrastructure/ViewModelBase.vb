﻿Imports System.ComponentModel
Imports System.Collections.Specialized
Imports System.Threading
Imports System.Diagnostics
Imports Prism.Mvvm
Imports Prism.Regions

Public MustInherit Class ViewModelBase
    Inherits BindableBase
    Implements INavigationAware

    Private _Title As String
    Public Property Title() As String
        Get
            Return _Title
        End Get
        Set(ByVal value As String)
            SetProperty(_Title, value)
        End Set
    End Property

    Private _IsBusy As Boolean = False
    Public Property IsBusy() As Boolean
        Get
            Return _IsBusy
        End Get
        Set(ByVal value As Boolean)
            SetProperty(_IsBusy, value, "IsBusy")
        End Set
    End Property


    Public Overridable Sub OnNavigatedTo(navigationContext As NavigationContext) Implements INavigationAware.OnNavigatedTo


    End Sub

    Public Overridable Sub OnNavigatedFrom(navigationContext As NavigationContext) Implements INavigationAware.OnNavigatedFrom


    End Sub

    Public Overridable Function IsNavigationTarget(navigationContext As NavigationContext) As Boolean Implements INavigationAware.IsNavigationTarget
        Return True
    End Function


End Class


