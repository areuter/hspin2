﻿Imports HSPinOne.DAL

Public Enum in1Folder
    documentsarchive = 0
    customerphotos = 1
    doctemplates = 2

End Enum





Public Class GlobalSettingService

    Public Shared Function HoleEinstellung(ByVal Einstellung As String, Optional ByVal quiet As Boolean = False) As String
        Dim _context = New in1Entities
        Dim query = From c In _context.GlobalSettings Where c.Einstellung = Einstellung.ToLower Select c

        If query.Any Then
            Return query.First.Wert
        Else
            If Not quiet Then
                Dim dlg As New HSPinOne.Infrastructure.DialogService
                dlg.ShowError("Fehler", "Die Einstellung >>" & Einstellung & "<< kann nicht gefunden werden! Bitte benachrichtigen Sie den Administrator.")

            End If
            Return Nothing
        End If
    End Function

    Public Shared Function HoleEinstellungen(ByVal list As List(Of String), Optional ByVal quiet As Boolean = False) As Dictionary(Of String, String)
        Dim _context = New in1Entities
        Dim query = From c In _context.GlobalSettings Where list.Contains(c.Einstellung.ToLower) Select c

        If query.Any Then
            Dim dic As New Dictionary(Of String, String)
            For Each itm In list
                If Not dic.ContainsKey(itm) Then
                    dic.Add(itm, query.Where(Function(o) o.Einstellung = itm).First.Wert)
                End If
            Next
            Return dic
        Else
            If Not quiet Then
                Dim dlg As New HSPinOne.Infrastructure.DialogService
                dlg.ShowError("Fehler", "Mindestens eine der Einstellungen konnte nicht gefunden werden! Bitte benachrichtigen Sie den Administrator.")

            End If
            Return Nothing
        End If
    End Function


    Public Shared Sub SchreibeEinstellung(ByVal Einstellung As String, ByVal Wert As String)
        Using con As New in1Entities
            Dim query = From c In con.GlobalSettings Where c.Einstellung = Einstellung.ToLower Select c

            If query.Any Then
                query.First.Wert = Wert
            Else
                Dim gs As New GlobalSetting
                gs.Einstellung = Einstellung
                gs.Wert = Wert
                con.GlobalSettings.Add(gs)
            End If
            con.SaveChanges()
        End Using
    End Sub

    Public Shared Sub SchreibeEinstellung(ByVal Einstellung As String, ByVal Bezeichnung As String, ByVal Wert As String)
        Using con As New in1Entities
            Dim query = From c In con.GlobalSettings Where c.Einstellung = Einstellung.ToLower Select c

            If query.Any Then
                query.First.Wert = Wert
            Else
                Dim gs As New GlobalSetting
                gs.Einstellung = Einstellung
                gs.Bezeichnung = Bezeichnung
                gs.Wert = Wert
                con.GlobalSettings.Add(gs)
            End If
            con.SaveChanges()
        End Using
    End Sub

    Public Shared Sub SchreibeEinstellung(ByVal Einstellung As String, ByVal Wert As String, ByVal Bezeichnung As String, ByVal Beschreibung As String, ByVal Typ As String)
        Using con As New in1Entities
            Dim query = From c In con.GlobalSettings Where c.Einstellung = Einstellung.ToLower Select c

            If query.Any Then
                query.First.Wert = Wert
            Else
                Dim gs As New GlobalSetting
                gs.Einstellung = Einstellung
                gs.Wert = Wert
                gs.Bezeichnung = Bezeichnung
                gs.Beschreibung = Beschreibung
                gs.Typ = Typ
                con.GlobalSettings.Add(gs)
            End If
            con.SaveChanges()
        End Using
    End Sub

    Public Shared Function HolePfad(ByVal Einstellung As String) As String
        Dim pfad As String = HoleEinstellung(Einstellung)
        If Not Right(Trim(pfad), 1) = "\" Then
            Return pfad & "\"
        Else
            Return pfad
        End If
    End Function




    Public Shared Function in1DataFolder(ByVal ftype As in1Folder) As String
        Dim pfad As String = HoleEinstellung("datafolder")
        If Not IsNothing(pfad) Then
            Dim exist = HSPinOne.Infrastructure.DirectoryExistsTask.Check(pfad)
            'Dim f As New IO.DirectoryInfo(pfad)
            If exist = True Then
                If Not Right(Trim(pfad), 1) = "\" Then
                    pfad = pfad & "\"
                End If
                'Check ob Unterordner existieren, ansonsten anlegen
                Dim subfolder As String = String.Empty
                If ftype = 0 Then
                    subfolder = "Dokumentarchiv"
                ElseIf ftype = 1 Then
                    subfolder = "Kundenfotos"
                ElseIf ftype = 2 Then
                    subfolder = "Vorlagen"
                End If
                'Dim fi As New IO.DirectoryInfo(pfad & subfolder)
                Dim exf = DirectoryExistsTask.Check(pfad & subfolder)
                If exf = False Then
                    Try
                        IO.Directory.CreateDirectory(pfad & subfolder)
                    Catch ex As Exception
                        Dim dlg As New HSPinOne.Infrastructure.DialogService
                        dlg.ShowError("Fehler", "Das Unterverzeichnis " & subfolder & " des Datenordners konnte nicht erstellt werden.")
                        dlg = Nothing
                        Return Nothing
                    End Try
                End If
                Return pfad & subfolder & "\"
            Else
                Dim dlg As New HSPinOne.Infrastructure.DialogService
                dlg.ShowError("Fehler", "Der festgelegte Datenordner existiert nicht!")
                Return Nothing
            End If
        End If
        Return Nothing


    End Function

    Private Function CheckFolder(ByVal path As String, ByVal foldername As String) As Boolean
        Dim f As New IO.DirectoryInfo(path & foldername)
        If f.Exists = False Then
            Try
                IO.Directory.CreateDirectory(path & foldername)
            Catch ex As Exception
                Dim dlg As New HSPinOne.Infrastructure.DialogService
                dlg.ShowError("Fehler", "Das Unterverzeichnis " & foldername & " des Datenordners konnte nicht erstellt werden.")
                dlg = Nothing
                Return False
            End Try
            Return True
        Else
            Return True
        End If
    End Function

    Public Shared Function GetInstitution() As String
        Dim adr As String = ""
        adr = HoleEinstellung("hspname")
        adr = adr & " " & HoleEinstellung("hspstr")
        adr = adr & " " & HoleEinstellung("hsport")
        Return adr
    End Function

    Public Function GetDefaults() As List(Of GlobalSetting)
        Dim in1defaults As New List(Of GlobalSetting)


        in1defaults.Add(NewDefault("SEPA", "1", "SEPA Aktiv", "combo", "0=Nein,1=ja"))
        in1defaults.Add(NewDefault("SepaMode", "2", "Sepa Modus", "combo", "0=CORE, 1=B2B, 2=COR1"))
        in1defaults.Add(NewDefault("link1", "", "Dashboard Link 1", "string", "Link im Markdown Format [Text](http://...)"))
        in1defaults.Add(NewDefault("link2", "", "Dashboard Link 2", "string", "Link im Markdown Format [Text](http://...)"))
        in1defaults.Add(NewDefault("link3", "", "Dashboard Link 3", "string", "Link im Markdown Format [Text](http://...)"))
        in1defaults.Add(NewDefault("link4", "", "Dashboard Link 4", "string", "Link im Markdown Format [Text](http://...)"))
        in1defaults.Add(NewDefault("web.logolink", "https://", "Web Logolink", "string", "Link bei Logoklick online"))
        in1defaults.Add(NewDefault("web.appurl", "https://", "Web URL App", "string", "URL ohne Slash"))

        in1defaults.Add(NewDefault("web.defaultaccountid", "1800", "Konto für die Onlinebuchungen", "integer"))
        in1defaults.Add(NewDefault("pos.eccashaccountid", "1460", "Transitkonto für EC Cash", "integer"))

        in1defaults.Add(NewDefault("web.address", "Postadresse HTML", "Post Adresse (HTML)", "string", "Text"))
        in1defaults.Add(NewDefault("web.phone", "0551-", "Web Telefonnummer", "string", "Telefon"))
        in1defaults.Add(NewDefault("web.mail", "info@", "Web E-Mail", "string", "Mail"))

        in1defaults.Add(NewDefault("web.imagepath", "/images", "Web Imagepfad", "string", "URL mit Slash"))
        in1defaults.Add(NewDefault("web.logoimage", "main/logo.png", "Web Logodatei", "string", "Dateiname"))
        in1defaults.Add(NewDefault("web.defaultsport", "sport.png", "Web Default Sportbild", "string", "Dateiname"))
        in1defaults.Add(NewDefault("web.defaultcontact", "contact.jpg", "Web Default Kontaktbild", "string", "Dateiname"))
        in1defaults.Add(NewDefault("web.showStaffContact", "true", "Web ÜL kontakt anzeigen", "boolean", "true=ja,false=nein"))
        in1defaults.Add(NewDefault("web.autoLoadAppointments", "true", "Web Termine sofort laden", "boolean", "true=ja,false=nein"))
        in1defaults.Add(NewDefault("web.onlinemembership", "false", "Web Online Mitgliedschaft", "boolean", "true=ja,false=nein"))
        in1defaults.Add(NewDefault("web.headerbackground", "#13306a", "Web Header Farbe", "string", "HTML Farbwert"))
        in1defaults.Add(NewDefault("web.footerbackground", "#4a4949", "Web Footer Farbe", "string", "HTML Farbwert"))
        in1defaults.Add(NewDefault("web.navigationbackground", "#7387ad", "Web Navigation Farbe", "string", "HTML Farbwert"))
        in1defaults.Add(NewDefault("web.showbooking", "true", "Web Navigation Platzbuchung anzeigen", "boolean", "true/false"))
        in1defaults.Add(NewDefault("web.showpackage", "true", "Web Navigation Onlineartikelverkauf", "boolean", "true/false"))
        in1defaults.Add(NewDefault("web.showmembership", "true", "Web Account Onlinemitgliedschaft", "boolean", "true/false"))
        in1defaults.Add(NewDefault("web.locationiframe", "false", "Web IFrame Map statt Google Maps", "boolean", "true/false"))
        in1defaults.Add(NewDefault("web.locationiframeurl", "", "Web Iframe Map Url", "string", "URL"))
        in1defaults.Add(NewDefault("web.showsearch", "true", "Web Navigation Suche anzeigen", "boolean", "true/false"))
        in1defaults.Add(NewDefault("web.showsports", "true", "Web Navigation Sportarten", "boolean", "true/false"))
        in1defaults.Add(NewDefault("web.breadcrumbs", "true", "Web Breadcrumbs anzeigen", "boolean", "true/false"))
        in1defaults.Add(NewDefault("web.languageflags", "false", "Web Flaggen für Sprachen anzeigen", "boolean", "true/false"))
        in1defaults.Add(NewDefault("web.backgroundimage", "", "Web Hintergrundbild", "string", "URL"))
        in1defaults.Add(NewDefault("DMSType", "Vertrag, Kündigung, Stundenzettel, Lizenz, Personalbogen", "DMS Auswahltypen", "string", "Kommagetrennte Liste"))
        in1defaults.Add(NewDefault("web.wpjson", "", "Web URL Newsquelle Wordpress Feed", "string", "Wordpress Posts als Newsquelle"))
        in1defaults.Add(NewDefault("packagenkgeld", "", "ArtikelID Nebenkosten des Geldverkehrs", "string"))
        in1defaults.Add(NewDefault("ldap.host", "", "LDAP Host", "string"))
        in1defaults.Add(NewDefault("ldap.port", "", "LDAP Port", "string"))
        in1defaults.Add(NewDefault("ldap.user", "", "LDAP Username", "string"))
        in1defaults.Add(NewDefault("ldap.pass", "", "LDAP Passwort", "password"))
        in1defaults.Add(NewDefault("ldap.basedn", "", "LDAP Base DN", "string"))
        in1defaults.Add(NewDefault("ldap.searchfilter", "(mail={0})", "LDAP Suchfilter", "string"))
        in1defaults.Add(NewDefault("web.ldap", "false", "LDAP Login Web", "boolean"))
        in1defaults.Add(NewDefault("client.ldap", "false", "LDAP Login Windows Client", "boolean"))
        in1defaults.Add(NewDefault("client.erefprefix", "", "Präfix vor allen EREF bei SEPA", "string"))
        in1defaults.Add(NewDefault("client.stornogrund", "Umtausch,Rückgabe,Attest", "Stornogründe (kommagetrennt)", "string"))

        Return in1defaults

    End Function

    Private Function NewDefault(ByVal Einstellung As String, ByVal Wert As String, ByVal Bezeichnung As String, ByVal Typ As String, Optional ByVal Beschreibung As String = "") As GlobalSetting
        Dim sett As New GlobalSetting
        sett.Einstellung = Einstellung
        sett.Wert = Wert
        sett.Bezeichnung = Bezeichnung
        sett.Beschreibung = Beschreibung
        sett.Typ = Typ
        Return sett
    End Function

End Class
