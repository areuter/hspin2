﻿Imports HSPinOne.DAL
Imports System.Data.Entity


Public Class MachineSettingService

    Public Enum in1MaschineSettings
        BondruckerAktiv
        BondruckerPort
        Kasse
        Bontext
        Quio
        BondruckerIP
        BondruckerIPPort
        BondruckerHead
        TerminalIP
        TerminalPort0
        TerminalPort1
        Mailer
        KasseNr
        TerminalStayLoggedin
    End Enum




    Public Shared Function in1MachineDefaults() As Dictionary(Of String, String)
        Dim _in1MachineDefaults As Dictionary(Of String, String)
        _in1MachineDefaults = New Dictionary(Of String, String)
        _in1MachineDefaults.Add("BondruckerAktiv", "0")
        _in1MachineDefaults.Add("BondruckerPort", "COM1")
        _in1MachineDefaults.Add("Kasse", "1600")
        _in1MachineDefaults.Add("Bontext", "Vielen Dank")
        _in1MachineDefaults.Add("Quio", "1")
        _in1MachineDefaults.Add("BondruckerIP", "")
        _in1MachineDefaults.Add("BondruckerIPPort", "9100")
        _in1MachineDefaults.Add("BondruckerHead", "")
        _in1MachineDefaults.Add("TerminalIP", "")
        _in1MachineDefaults.Add("TerminalPort0", "20002")
        _in1MachineDefaults.Add("TerminalPort1", "20007")
        _in1MachineDefaults.Add("Mailer", "Outlook")
        _in1MachineDefaults.Add("KasseNr", "0")
        _in1MachineDefaults.Add("TerminalStayLoggedin", "false")
        Return _in1MachineDefaults


    End Function

    Public Shared Function GetMachineSetting(ByVal Einstellung As in1MaschineSettings) As String
        Using context As New in1Entities
            Dim sett = Einstellung.ToString
            Dim query = From c In context.MachineConfigs Where c.Machinename = MyGlobals.Fingerprint And c.Configname = sett Select c

            If query.Any Then
                Dim res = query.First.Machinesetting
                Return CStr(res)
            Else
                Dim res = in1MachineDefaults.Where(Function(o) o.Key = sett)
                If Not IsNothing(res) Then
                    Return res.FirstOrDefault.Value
                End If
            End If
        End Using
        Return ""
    End Function

    Public Shared Function GetKasseNr() As Integer
        Dim intKasseNr As Integer = 0
        Int32.TryParse(GetMachineSetting(in1MaschineSettings.KasseNr), intKasseNr)
        Return intKasseNr
    End Function

    Public Shared Function GetKasseAll() As List(Of String)
        Dim strSett = in1MaschineSettings.KasseNr.ToString()

        Using con As New in1Entities
            'Dim query = From c In con.MachineConfigs Where c.Configname = strSett And Not c.Machinesetting = "" Select c.Machinesetting

            Dim query1 = con.MachineConfigs.Where(Function(o) o.Configname = strSett AndAlso Not o.Machinesetting = "").GroupBy(Function(o) o.Machinesetting).Select(Function(o) o.FirstOrDefault.Machinesetting)

            Return query1.ToList()
        End Using
    End Function
End Class
