﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Threading
Imports System.Windows
Imports System.Windows.Controls


''' <summary>
''' Static class used to attach to wpf control
''' </summary>
Public NotInheritable Class GridViewColumnResize


#Region "DependencyProperties"

    Public Shared ReadOnly WidthProperty As DependencyProperty = DependencyProperty.RegisterAttached("Width", GetType(String), GetType(GridViewColumnResize), New PropertyMetadata(AddressOf OnSetWidthCallback))

    Public Shared ReadOnly GridViewColumnResizeBehaviorProperty As DependencyProperty = DependencyProperty.RegisterAttached("GridViewColumnResizeBehavior", GetType(GridViewColumnResizeBehavior), GetType(GridViewColumnResize), Nothing)

    Public Shared ReadOnly EnabledProperty As DependencyProperty = DependencyProperty.RegisterAttached("Enabled", GetType(Boolean), GetType(GridViewColumnResize), New PropertyMetadata(AddressOf OnSetEnabledCallback))

    Public Shared ReadOnly ListViewResizeBehaviorProperty As DependencyProperty = DependencyProperty.RegisterAttached("ListViewResizeBehaviorProperty", GetType(ListViewResizeBehavior), GetType(GridViewColumnResize), Nothing)

#End Region

    Public Shared Function GetWidth(obj As DependencyObject) As String
        Return DirectCast(obj.GetValue(WidthProperty), String)
    End Function

    Public Shared Sub SetWidth(obj As DependencyObject, value As String)
        obj.SetValue(WidthProperty, value)
    End Sub

    Public Shared Function GetEnabled(obj As DependencyObject) As Boolean
        Return CBool(obj.GetValue(EnabledProperty))
    End Function

    Public Shared Sub SetEnabled(obj As DependencyObject, value As Boolean)
        obj.SetValue(EnabledProperty, value)
    End Sub

#Region "CallBack"

    Private Shared Sub OnSetWidthCallback(dependencyObject As DependencyObject, e As DependencyPropertyChangedEventArgs)
        Dim element = TryCast(dependencyObject, GridViewColumn)
        If element IsNot Nothing Then
            Dim behavior As GridViewColumnResizeBehavior = GetOrCreateBehavior(element)
            behavior.Width = TryCast(e.NewValue, String)
        Else
            Console.[Error].WriteLine("Error: Expected type GridViewColumn but found " + dependencyObject.[GetType]().Name)
        End If
    End Sub

    Private Shared Sub OnSetEnabledCallback(dependencyObject As DependencyObject, e As DependencyPropertyChangedEventArgs)
        Dim element = TryCast(dependencyObject, ListView)
        If element IsNot Nothing Then
            Dim behavior As ListViewResizeBehavior = GetOrCreateBehavior(element)
            behavior.Enabled = CBool(e.NewValue)
        Else
            Console.[Error].WriteLine("Error: Expected type ListView but found " + dependencyObject.[GetType]().Name)
        End If
    End Sub


    Private Shared Function GetOrCreateBehavior(element As ListView) As ListViewResizeBehavior
        Dim behavior = TryCast(element.GetValue(GridViewColumnResizeBehaviorProperty), ListViewResizeBehavior)
        If behavior Is Nothing Then
            behavior = New ListViewResizeBehavior(element)
            element.SetValue(ListViewResizeBehaviorProperty, behavior)
        End If

        Return behavior
    End Function

    Private Shared Function GetOrCreateBehavior(element As GridViewColumn) As GridViewColumnResizeBehavior
        Dim behavior = TryCast(element.GetValue(GridViewColumnResizeBehaviorProperty), GridViewColumnResizeBehavior)
        If behavior Is Nothing Then
            behavior = New GridViewColumnResizeBehavior(element)
            element.SetValue(GridViewColumnResizeBehaviorProperty, behavior)
        End If

        Return behavior
    End Function

#End Region

#Region "Nested type: GridViewColumnResizeBehavior"

    ''' <summary>
    ''' GridViewColumn class that gets attached to the GridViewColumn control
    ''' </summary>
    Public Class GridViewColumnResizeBehavior
        Private ReadOnly _element As GridViewColumn

        Public Sub New(element As GridViewColumn)
            _element = element
        End Sub

        Public Property Width() As String
            Get
                Return m_Width
            End Get
            Set(value As String)
                m_Width = value
            End Set
        End Property
        Private m_Width As String

        Public ReadOnly Property IsStatic() As Boolean
            Get
                Return StaticWidth >= 0
            End Get
        End Property

        Public ReadOnly Property StaticWidth() As Double
            Get
                Dim result As Double
                Return If(Double.TryParse(Width, result), result, -1)
            End Get
        End Property

        Public ReadOnly Property Percentage() As Double
            Get
                If Not IsStatic Then
                    Return Mulitplier * 100
                End If
                Return 0
            End Get
        End Property

        Public ReadOnly Property Mulitplier() As Double
            Get
                If Width = "*" OrElse Width = "1*" Then
                    Return 1
                End If
                If Width.EndsWith("*") Then
                    Dim perc As Double
                    If Double.TryParse(Width.Substring(0, Width.Length - 1), perc) Then
                        Return perc
                    End If
                End If
                Return 1
            End Get
        End Property

        Public Sub SetWidth(allowedSpace As Double, totalPercentage As Double)
            If IsStatic Then
                _element.Width = StaticWidth
            Else
                Dim width As Double = allowedSpace * (Percentage / totalPercentage)
                If width < 0 Then width = 0
                _element.Width = width
            End If
        End Sub
    End Class

#End Region

#Region "Nested type: ListViewResizeBehavior"

    ''' <summary>
    ''' ListViewResizeBehavior class that gets attached to the ListView control
    ''' </summary>
    Public Class ListViewResizeBehavior
        Private Const Margin As Integer = 25
        Private Const RefreshTime As Long = Timeout.Infinite
        Private Const Delay As Long = 500

        Private ReadOnly _element As ListView
        Private ReadOnly _timer As Timer

        Public Sub New(element As ListView)
            If element Is Nothing Then
                Throw New ArgumentNullException("element")
            End If
            _element = element
            AddHandler element.Loaded, AddressOf OnLoaded

            ' Action for resizing and re-enable the size lookup
            ' This stops the columns from constantly resizing to improve performance
            Dim resizeAndEnableSize As Action = Sub()
                                                    Resize()
                                                    AddHandler _element.SizeChanged, AddressOf OnSizeChanged

                                                End Sub
            _timer = New Timer(Function(x) Application.Current.Dispatcher.BeginInvoke(resizeAndEnableSize), Nothing, Delay, RefreshTime)
        End Sub

        Public Property Enabled() As Boolean
            Get
                Return m_Enabled
            End Get
            Set(value As Boolean)
                m_Enabled = value
            End Set
        End Property
        Private m_Enabled As Boolean


        Private Sub OnLoaded(sender As Object, e As RoutedEventArgs)
            AddHandler _element.SizeChanged, AddressOf OnSizeChanged
        End Sub

        Private Sub OnSizeChanged(sender As Object, e As SizeChangedEventArgs)
            If e.WidthChanged Then
                RemoveHandler _element.SizeChanged, AddressOf OnSizeChanged
                _timer.Change(Delay, RefreshTime)
            End If
        End Sub

        Private Sub Resize()
            If Enabled Then
                Dim totalWidth As Double = _element.ActualWidth
                Dim gv = TryCast(_element.View, GridView)
                If gv IsNot Nothing Then
                    Dim allowedSpace As Double = totalWidth - GetAllocatedSpace(gv)
                    allowedSpace = allowedSpace - Margin
                    Dim totalPercentage As Double = GridViewColumnResizeBehaviors(gv).Sum(Function(x) x.Percentage)
                    For Each behavior As GridViewColumnResizeBehavior In GridViewColumnResizeBehaviors(gv)
                        behavior.SetWidth(allowedSpace, totalPercentage)
                    Next
                End If
            End If
        End Sub

        Private Shared Function GridViewColumnResizeBehaviors(gv As GridView) As IEnumerable(Of GridViewColumnResizeBehavior)
            Dim ls As New List(Of GridViewColumnResizeBehavior)
            For Each t As GridViewColumn In gv.Columns
                Dim gridViewColumnResizeBehavior = TryCast(t.GetValue(GridViewColumnResizeBehaviorProperty), GridViewColumnResizeBehavior)
                If gridViewColumnResizeBehavior IsNot Nothing Then

                    ls.Add(gridViewColumnResizeBehavior)

                    'Return New List(Of GridViewColumnResizeBehavior) From gridViewColumnResizeBehavior
                End If
            Next
            Return ls
        End Function

        Private Shared Function GetAllocatedSpace(gv As GridView) As Double
            Dim totalWidth As Double = 0
            For Each t As GridViewColumn In gv.Columns
                Dim gridViewColumnResizeBehavior = TryCast(t.GetValue(GridViewColumnResizeBehaviorProperty), GridViewColumnResizeBehavior)
                If gridViewColumnResizeBehavior IsNot Nothing Then
                    If gridViewColumnResizeBehavior.IsStatic Then
                        totalWidth += gridViewColumnResizeBehavior.StaticWidth
                    End If
                Else
                    totalWidth += t.ActualWidth
                End If
            Next
            Return totalWidth
        End Function
    End Class

#End Region
End Class


