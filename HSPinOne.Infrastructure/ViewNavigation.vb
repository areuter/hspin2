﻿Imports Prism.Regions

Public Class ViewNavigation
    Public Shared Sub NavigateViews(ByVal strview As String, ByVal regionmanager As RegionManager)
        Try

            Dim wc As New WaitCursor

            Dim views = Split(strview, ",")
            If UBound(views) >= 0 Then _
                regionmanager.Regions(RegionNames.MainRegion).RequestNavigate(New Uri(Trim(views(0)), UriKind.Relative))
            'If UBound(views) >= 1 Then regionmanager.Regions(RegionNames.RibbonRegion).RequestNavigate(New Uri(Trim(views(1)), UriKind.Relative))
            wc.Dispose()
        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub
End Class
