﻿Imports System.Collections.ObjectModel
Imports HSPinOne.DAL


Public Interface IUserDataService

    Function GetUserByLoginID(ByVal LoginID As String) As User

    Function GetUserByID(ByVal UserID As Integer) As User

    Function Login(ByVal LoginID As String, ByVal LoginPassword As String, Optional ByVal logintype As Integer = 0) As PrincipleBusinessObject

End Interface
