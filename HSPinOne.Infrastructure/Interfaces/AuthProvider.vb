﻿Public MustInherit Class AuthProvider
    Private Shared _instance As AuthProvider

    ''' <summary>
    ''' This method determines whether the user is authorize to perform 
    ''' the requested operation
    ''' </summary>
    Public MustOverride Function CheckAccess(ByVal operation As String) As Boolean

    ''' <summary>
    ''' This method determines whether the user is authorize to perform 
    ''' the requested operation
    ''' </summary>
    Public MustOverride Function CheckAccess(ByVal commandParameter As Object) As Boolean

    Public Shared Sub Initialize(Of TProvider As {AuthProvider, New})()
        _instance = New TProvider()
    End Sub

    Public Shared Sub Initialize(Of TProvider)(ByVal parameters As Object())
        _instance = DirectCast(GetType(TProvider).GetConstructor(New Type() {GetType(Object())}).Invoke(New Object() {parameters}), AuthProvider)
    End Sub

    Public Shared ReadOnly Property Instance() As AuthProvider
        Get
            Return _instance
        End Get
    End Property
End Class

