﻿Imports System.Windows
Imports System.Windows.Input
Imports System.Windows.Data
Imports System.Windows.Controls


Public Class UpdateFocusedField

    Public Shared Sub Update()
        Dim fwe As FrameworkElement = TryCast(Keyboard.FocusedElement, FrameworkElement)
        If fwe IsNot Nothing Then
            Dim expression As BindingExpression = Nothing
            If TypeOf fwe Is TextBox Then
                expression = fwe.GetBindingExpression(TextBox.TextProperty)
            End If

            If expression IsNot Nothing Then
                expression.UpdateSource()
            End If

        End If

    End Sub





End Class
