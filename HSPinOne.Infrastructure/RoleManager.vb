﻿Imports System
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Input
Imports System.Collections.Generic

Public Class RoleManager
    Inherits DependencyObject

    Private Property Elements() As List(Of UIElement)
        Get
            Return m_Elements
        End Get
        Set(ByVal value As List(Of UIElement))
            m_Elements = value
        End Set
    End Property
    Private m_Elements As List(Of UIElement)





    ''' <summary>
    ''' Initializes a new instance of the <see cref="RoleManager"/> class.
    ''' </summary>
    Public Sub New()
        Me.Elements = New List(Of UIElement)()
    End Sub





#Region "ManagerProperty"

    Public Shared ReadOnly ManagerProperty As DependencyProperty =
        DependencyProperty.RegisterAttached("Manager", GetType(RoleManager), GetType(RoleManager), New PropertyMetadata(Nothing, AddressOf OnManagerChanged))

    Public Shared Function GetManager(ByVal obj As DependencyObject) As RoleManager

        Return CType(obj.GetValue(ManagerProperty), RoleManager)
    End Function

    Public Shared Sub SetManager(ByVal obj As DependencyObject, ByVal value As RoleManager)
        obj.SetValue(ManagerProperty, value)
    End Sub


    Private Shared Sub OnManagerChanged(ByVal obj As DependencyObject, ByVal e As DependencyPropertyChangedEventArgs)
        Dim element As UIElement = TryCast(obj, UIElement)

        If element IsNot Nothing Then
            If e.OldValue IsNot Nothing AndAlso TypeOf e.OldValue Is RoleManager Then
                DirectCast(e.OldValue, RoleManager).Clear()
            End If
            If e.NewValue IsNot Nothing AndAlso TypeOf e.NewValue Is RoleManager Then
                DirectCast(e.NewValue, RoleManager).AddElement(element)
            End If
        End If
    End Sub

#End Region

#Region "VisibilityIfLoggedProperty"

    Public Shared ReadOnly VisibilityIfLoggedProperty As DependencyProperty =
        DependencyProperty.RegisterAttached("VisibilityIfLogged", GetType(Visibility), GetType(RoleManager), New PropertyMetadata(Visibility.Collapsed, AddressOf ApplyRulesToElement))

    Public Shared Function GetVisibilityIfLogged(ByVal obj As DependencyObject) As Visibility
        Return CType(obj.GetValue(VisibilityIfLoggedProperty), Visibility)
    End Function

    Public Shared Sub SetVisibilityIfLogged(ByVal obj As DependencyObject, ByVal value As Visibility)
        obj.SetValue(VisibilityIfLoggedProperty, value)
    End Sub

#End Region

#Region "RolesProperty"

    Public Shared ReadOnly RolesProperty As DependencyProperty =
        DependencyProperty.RegisterAttached("Roles", GetType(String), GetType(RoleManager), New PropertyMetadata(String.Empty, AddressOf ApplyRulesToElement))

    Public Shared Function GetRoles(ByVal obj As DependencyObject) As String
        Return CType(obj.GetValue(RolesProperty), String)
    End Function

    Public Shared Sub SetRoles(ByVal obj As DependencyObject, ByVal value As String)
        obj.SetValue(RolesProperty, value)
    End Sub

#End Region

#Region "TaskProperty"

    Public Shared ReadOnly TaskProperty As DependencyProperty =
        DependencyProperty.RegisterAttached("Task", GetType(String), GetType(RoleManager), New PropertyMetadata(String.Empty, AddressOf ApplyRulesToElement))

    Public Shared Function GetTask(ByVal obj As DependencyObject) As String
        Return CType(obj.GetValue(TaskProperty), String)
    End Function

    Public Shared Sub SetTask(ByVal obj As DependencyObject, ByVal value As String)
        obj.SetValue(TaskProperty, value)
    End Sub

#End Region

#Region "IsReadOnlyProperty"

    Public Shared ReadOnly IsEnabledProperty As DependencyProperty =
        DependencyProperty.RegisterAttached("IsEnabled", GetType(String), GetType(RoleManager), New PropertyMetadata(String.Empty, AddressOf ApplyIsEnabledToElement))

    Public Shared Function IsEnabledRoles(ByVal obj As DependencyObject) As String
        Return CType(obj.GetValue(IsEnabledProperty), String)
    End Function

    Public Shared Sub SetIsEnabled(ByVal obj As DependencyObject, ByVal value As String)
        obj.SetValue(IsEnabledProperty, value)
    End Sub


#End Region

    ''' <summary>
    ''' Clears this instance.
    ''' </summary>
    Private Sub Clear()
        Me.Elements.Clear()
    End Sub

    ''' <summary>
    ''' Adds the element.
    ''' </summary>
    ''' <param name="element">The element.</param>
    Private Sub AddElement(ByVal element As UIElement)
        If Not Me.Elements.Contains(element) Then
            Me.Elements.Add(element)
        End If
    End Sub

    ''' <summary>
    ''' Applies the rules.
    ''' </summary>
    Public Sub ApplyRules()
        For Each element As UIElement In Me.Elements
            Dim roles As String = RoleManager.GetRoles(element)
            '#If DEBUG Then
            'element.Visibility = Visibility.Visible
            '#Else
            If Not String.IsNullOrEmpty(roles) Then
                For Each role As String In roles.Split(",")
                    If System.Threading.Thread.CurrentPrincipal.IsInRole(role) Then
                        element.Visibility = Visibility.Visible
                        Exit For
                    Else
#If DEBUG Then
                        element.Visibility = Visibility.Visible
#Else
                        element.Visibility = Visibility.Collapsed
#End If

                    End If
                Next
            Else
                Dim visibility__1 As Visibility = RoleManager.GetVisibilityIfLogged(element)

                If System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated Then
                    element.Visibility = visibility__1
                Else
                    element.Visibility = If(visibility__1 = Visibility.Collapsed, Visibility.Visible, Visibility.Collapsed)
                End If
            End If
            '#End If
        Next
    End Sub

    ''' <summary>
    ''' Applies the rules to element.
    ''' </summary>
    ''' <param name="element">The element.</param>
    Private Shared Sub ApplyRulesToElement(ByVal element As DependencyObject, ByVal e As DependencyPropertyChangedEventArgs)
        Dim manager As RoleManager = RoleManager.GetManager(element)

        If manager IsNot Nothing Then
            manager.ApplyRules()
        End If
    End Sub

    ''' <summary>
    ''' Applies the rules.
    ''' </summary>
    Public Sub ApplyIsEnabled()
        For Each element As UIElement In Me.Elements
            Dim roles As String = RoleManager.GetRoles(element)

            If Not String.IsNullOrEmpty(roles) Then
                For Each role As String In roles.Split(",")
                    If System.Threading.Thread.CurrentPrincipal.IsInRole(role) Then
                        element.IsEnabled = False
                        Exit For
                    Else
                        element.IsEnabled = True
                    End If
                Next
            Else
                Dim visibility__1 As Visibility = RoleManager.GetVisibilityIfLogged(element)

                If System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated Then
                    element.Visibility = visibility__1
                Else
                    element.Visibility = If(visibility__1 = Visibility.Collapsed, Visibility.Visible, Visibility.Collapsed)
                End If
            End If
        Next
    End Sub

    ''' <summary>
    ''' Applies the rules to element.
    ''' </summary>
    ''' <param name="element">The element.</param>
    Private Shared Sub ApplyIsEnabledToElement(ByVal element As DependencyObject, ByVal e As DependencyPropertyChangedEventArgs)
        Dim manager As RoleManager = RoleManager.GetManager(element)

        If manager IsNot Nothing Then
            manager.ApplyIsEnabled()
        End If
    End Sub

End Class

