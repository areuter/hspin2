﻿Imports System.Windows
Imports System.Windows.Controls

Public Class Permission

    Private Shared Sub RecalculateControlIsEnabled(control As Control, hasPermission As Boolean)

        control.IsEnabled = hasPermission

    End Sub

    Public Shared ReadOnly IsEnabledProperty As DependencyProperty =
        DependencyProperty.RegisterAttached("IsEnabled", GetType(String), GetType(Permission), New PropertyMetadata(String.Empty, AddressOf IsEnabled_Callback))

    Public Shared Function IsEnabledRoles(ByVal obj As DependencyObject) As String
        Return CType(obj.GetValue(IsEnabledProperty), String)
    End Function

    Public Shared Sub SetIsEnabled(ByVal obj As DependencyObject, ByVal value As String)
        obj.SetValue(IsEnabledProperty, value)
    End Sub


    Private Shared Sub IsEnabled_Callback(ByVal source As DependencyObject, ByVal e As DependencyPropertyChangedEventArgs)

        Dim hasPermisson As Boolean = False
        Dim uiElement = TryCast(source, Control)
        Dim username As String = MyGlobals.CurrentUsername
        hasPermisson = True
        RecalculateControlIsEnabled(uiElement, hasPermisson)

    End Sub
End Class
