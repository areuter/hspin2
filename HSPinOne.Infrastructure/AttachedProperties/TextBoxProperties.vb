﻿
Imports System.Windows
Imports System.Windows.Controls

Namespace Attached


    Public Class TextBoxProperties

        Public Shared IsFocusedProperty As DependencyProperty = DependencyProperty.RegisterAttached("IsFocused", GetType(Boolean), GetType(TextBoxProperties), New UIPropertyMetadata(False, AddressOf OnIsFocusedChanged))

        Public Shared Function GetIsFocused(depo As DependencyObject) As Boolean
            Return CType(depo.GetValue(IsFocusedProperty), Boolean)
        End Function

        Public Shared Sub SetIsFocused(depo As DependencyObject, value As Boolean)
            depo.SetValue(IsFocusedProperty, value)
        End Sub

        Public Shared Sub OnIsFocusedChanged(depo As DependencyObject, args As DependencyPropertyChangedEventArgs)
            Dim textBox As TextBox = CType(depo, TextBox)
            Dim newValue As Boolean = CType(args.NewValue, Boolean)
            Dim oldValue As Boolean = CType(args.OldValue, Boolean)
            If newValue AndAlso oldValue = False AndAlso textBox.IsFocused = False Then textBox.Focus()

        End Sub


    End Class


End Namespace
