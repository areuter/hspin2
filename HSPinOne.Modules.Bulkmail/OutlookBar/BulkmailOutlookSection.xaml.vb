﻿Imports System.ComponentModel.Composition
Imports HSPinOne.Infrastructure
Imports System.Diagnostics.CodeAnalysis


Public Class BulkmailOutlookSection

    Public Sub New()

        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()

        ' Fügen Sie Initialisierungen nach dem InitializeComponent()-Aufruf hinzu.
        Me.DataContext = New BulkmailOutlookSectionViewModel()
    End Sub


End Class
