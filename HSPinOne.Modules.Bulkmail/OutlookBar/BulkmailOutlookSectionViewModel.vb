﻿Imports Prism.Regions
Imports System
Imports System.Collections.ObjectModel
Imports HSPinOne.Common
Imports HSPinOne.Infrastructure
Imports System.ComponentModel.Composition
Imports System.Windows.Controls



Public Class BulkmailOutlookSectionViewModel


    Private _treeViewMenu As ObservableCollection(Of OutlookSectionMenuTreeTopItemViewModel)
    Public Property TreeViewMenu As ObservableCollection(Of OutlookSectionMenuTreeTopItemViewModel)
        Get
            Return _treeViewMenu
        End Get
        Set(ByVal value As ObservableCollection(Of OutlookSectionMenuTreeTopItemViewModel))
            _treeViewMenu = value
        End Set
    End Property

    Private _regionmanager As IRegionManager



    Private _selected As ListBoxItem
    Public Property Selected As ListBoxItem
        Get
            Return _selected
        End Get
        Set(value As ListBoxItem)
            _selected = value
            If Not IsNothing(value.Tag) Then
                ViewNavigation.NavigateViews(CType(value.Tag, String), _regionmanager)
            End If
        End Set
    End Property



    Public Sub New()

        _regionmanager = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IRegionManager)
        Events.EventInstance.EventAggregator.GetEvent(Of OutlookSectionChangedEvent)().Subscribe(AddressOf CheckRegion)

    End Sub


    Private Sub CheckRegion(ByVal param As Object)


        If CStr(param.Name).ToLower = "listenverteiler" Then

            'Dim regionManager As IRegionManager = ServiceLocator.Current.GetInstance(Of IRegionManager)()
            Dim uri As New Uri("/MailView", UriKind.Relative)
            _regionmanager.Regions(RegionNames.MainRegion).RequestNavigate(uri)

            '_regionmanager.Regions(RegionNames.RibbonRegion).RequestNavigate(New Uri("/DashboardRibbonTabView", UriKind.Relative))



        End If

    End Sub


End Class
