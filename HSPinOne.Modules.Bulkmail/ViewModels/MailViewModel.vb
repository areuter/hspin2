﻿Imports HSPinOne.Common
Imports HSPinOne.Infrastructure
Imports HSPinOne.DAL
Imports System.Data.Entity
Imports System.ComponentModel.Composition
Imports System.Collections.ObjectModel
Imports System.Windows.Input
Imports System.Net.Mail
Imports System.Threading.Thread
Imports System.ComponentModel
Imports System.Net.Mime
Imports System.Net
Imports System.IO


Namespace ViewModel


    Public Class MailViewModel
        Inherits ViewModelBase


        Private _context As in1Entities

        Private WithEvents bgw As BackgroundWorker

#Region "   Properties"



        Private _OCRecipients As ObservableCollection(Of MailRecipient)
        Public Property OCRecipients As ObservableCollection(Of MailRecipient)
            Get
                Return _OCRecipients
            End Get
            Set(value As ObservableCollection(Of MailRecipient))
                _OCRecipients = value
            End Set
        End Property

        Private _Mailtext As String = ""
        Public Property Mailtext As String
            Get
                Return _Mailtext
            End Get
            Set(value As String)
                _Mailtext = value
            End Set
        End Property

        Private _anlage As String
        Public Property Anlage As String
            Get
                Return _anlage
            End Get
            Set(value As String)
                _anlage = value
                RaisePropertyChanged("Anlage")
            End Set
        End Property

        Private _betreff As String = ""
        Public Property Betreff As String
            Get
                Return _betreff
            End Get
            Set(value As String)
                _betreff = value
            End Set
        End Property


        Private _Vorauswahl As Dictionary(Of String, String)
        Public Property Vorauswahl As Dictionary(Of String, String)
            Get
                Return _Vorauswahl
            End Get
            Set(value As Dictionary(Of String, String))
                _Vorauswahl = value
            End Set
        End Property

        Private _SelectedList As String
        Public Property SelectedList As String
            Get
                Return _SelectedList
            End Get
            Set(value As String)
                _SelectedList = value
            End Set
        End Property

        Private _Fortschritt As Integer
        Public Property Fortschritt As Integer
            Get
                Return _Fortschritt
            End Get
            Set(value As Integer)
                _Fortschritt = value
                RaisePropertyChanged("Fortschritt")
            End Set
        End Property




        Public Property TestmailRecipient As String

        Public Property GetFileCommand As ICommand
        Public Property DelFileCommand As ICommand
        Public Property TestmailCommand As ICommand
        Public Property SendCommand As ICommand
        Public Property CancelCommand As ICommand
        Public Property AddListCommand As ICommand
        Public Property ClearListCommand As ICommand
        Public Property AddCustomerCommand As ICommand
        Public Property DeleteCommand As ICommand
        Public Property ExportCommand As ICommand
        Public Property MapiMailCommand As ICommand
        Public Property PasteRecipientsCommand As ICommand

#End Region

        Public Sub New()

            GetFileCommand = New RelayCommand(AddressOf GetFile)
            DelFileCommand = New RelayCommand(AddressOf DelFile)
            TestmailCommand = New RelayCommand(AddressOf Testmail, AddressOf CanTestmail)
            SendCommand = New RelayCommand(AddressOf Bulkmail, AddressOf CanBulkmail)
            CancelCommand = New RelayCommand(AddressOf CancelSend, AddressOf CanCancelSend)
            AddListCommand = New RelayCommand(AddressOf AddList, AddressOf CanAddList)
            ClearListCommand = New RelayCommand(AddressOf ClearList)
            AddCustomerCommand = New RelayCommand(AddressOf AddCutomer)
            DeleteCommand = New RelayCommand(AddressOf Delete)
            ExportCommand = New RelayCommand(AddressOf Export2CSV)
            MapiMailCommand = New RelayCommand(AddressOf MapiMail)
            PasteRecipientsCommand = New RelayCommand(AddressOf PasteRecipients)


            Vorauswahl = New Dictionary(Of String, String)

            Vorauswahl.Add("ÜL", "Alle aktiven Übungsleiter")
            Vorauswahl.Add("M", "Alle aktiven Mitglieder")
            Vorauswahl.Add("A", "Alle aktuellen Teilnehmer")
            Vorauswahl.Add("O", "Alle Obleute")

            OCRecipients = New ObservableCollection(Of MailRecipient)

        End Sub

        Private Sub AddCutomer()
            Dim csvc As New SearchCustomer
            Dim cust = csvc.GetCustomer()
            If Not IsNothing(cust) Then
                InsertintoList(cust.Mail1, cust.CustomerID, cust.Gender)
            End If
        End Sub

        Private Sub AddList()
            If Not IsNothing(SelectedList) Then
                Dim context As New in1Entities
                Dim query As IQueryable = Nothing

                If SelectedList = "ÜL" Then
                    query = From c In context.CourseStaffs Where c.Course.Term.Semesterbeginn <= Now And c.Course.Term.Semesterende >= Now
                            Select c.Customer.Mail1, c.CustomerID, c.Customer.Gender

                ElseIf SelectedList = "M" Then
                    query = From c In context.Contracts.Include("Customer") Where c.Vertragsbeginn <= Now And c.Vertragsende >= Now
                            Select c.CustomerID, c.Customer.Mail1, c.Customer.Gender


                ElseIf SelectedList = "A" Then
                    query = From c In context.Billings.Include("Customer") Where c.Course.Term.Semesterbeginn <= Now And c.Course.Term.Semesterende >= Now
                            Select c.CustomerID, c.Customer.Mail1, c.Customer.Gender

                ElseIf SelectedList = "O" Then
                    query = From c In context.Sports Where Not c.Obmann_CustomerID Is Nothing
                            Select CustomerID = c.Obmann_CustomerID, c.Obmann_Customer.Mail1, c.Obmann_Customer.Gender

                End If

                If Not IsNothing(query) Then
                    For Each cs In query
                        If Not IsNothing(cs.Mail1) Then
                            InsertintoList(cs.Mail1, cs.CustomerID, cs.Gender)
                        End If

                    Next
                End If
            End If
        End Sub

        Private Function CanAddList() As Boolean
            If Not IsNothing(SelectedList) Then Return True
            Return False
        End Function

        Private Sub InsertintoList(ByVal email As String, Optional ByVal CustomerID As Integer = 0, Optional ByVal gender As String = "m")
            If Not IsNothing(email) Then
                Dim reci As New MailRecipient
                reci.Email = email.ToLower
                reci.Gender = gender
                If Not CustomerID = 0 Then reci.CustomerID = CustomerID
                reci.Guid = Guid.NewGuid
                If OCRecipients.Where(Function(o) o.Email.ToLower = email.ToLower).Count = 0 Then
                    OCRecipients.Add(reci)
                End If
            End If
        End Sub

        Private Sub ClearList()
            Dim dlg As New DialogService
            If dlg.AskConfirmation("Leeren", "Soll die aktuelle Verteilerliste wirklich gelöscht werden?") = True Then
                OCRecipients.Clear()
            End If
        End Sub

        Private Sub Delete(ByVal param As Guid)
            If Not IsNothing(param) Then
                Dim itm = OCRecipients.Where(Function(o) o.Guid = param).FirstOrDefault
                If Not IsNothing(itm) Then OCRecipients.Remove(itm)

            End If
        End Sub

        Private Sub GetFile()
            Dim dlg As New DialogService
            Anlage = dlg.GetFile("")

        End Sub

        Private Sub DelFile()
            Anlage = ""
        End Sub



        Private Sub Testmail()

            SendBulkMail(True)
            Dim dlg As New DialogService
            dlg.ShowInfo("Mail", "Die Test-Mail wurde an die Adresse " & TestmailRecipient & " versendet.")
            dlg = Nothing
        End Sub

        Private Function CanTestmail() As Boolean
            If Not IsNothing(TestmailRecipient) Then
                If Len(TestmailRecipient) > 0 And InStr(TestmailRecipient, "@") Then
                    If Len(Betreff) > 3 And Len(Mailtext) > 5 Then Return True
                End If
            End If
            Return False
        End Function


        Private Sub Bulkmail()

            IsBusy = True
            bgw = New BackgroundWorker
            bgw.WorkerSupportsCancellation = True
            bgw.WorkerReportsProgress = True

            If Not bgw.IsBusy Then
                MyGlobals.LongRunningProcess = True
                bgw.RunWorkerAsync()

            End If


        End Sub

        Private Function CanBulkmail() As Boolean
            If Not IsNothing(OCRecipients) Then
                If OCRecipients.Count > 0 Then
                    If Len(Betreff) > 3 And Len(Mailtext) > 5 Then Return True
                End If

            End If
            Return False
        End Function

        Private Sub CancelSend()
            If IsBusy Then
                bgw.CancelAsync()
            End If
        End Sub

        Private Function CanCancelSend() As Boolean
            If IsBusy Then Return True
            Return False
        End Function



        Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw.DoWork
            SendBulkMail(False)
        End Sub

        Private Sub bgw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles bgw.RunWorkerCompleted
            MyGlobals.LongRunningProcess = False
            IsBusy = False
        End Sub

        Private Sub bgw_ProgressChanged(ByVal sender As Object, ByVal e As ProgressChangedEventArgs) Handles bgw.ProgressChanged
            Fortschritt = e.ProgressPercentage
        End Sub


        Private Sub Export2CSV()
            Dim ex = New CSVExport
            For Each itm In OCRecipients
                ex.AddLine(itm.Email, itm.Gender, itm.CustomerID)
            Next
            ex.Export()
        End Sub

        Private Sub MapiMail()
            Dim mail As New clsMail
            For Each itm In OCRecipients
                mail.AddBccRecipient(itm.Email)
            Next
            mail.SendinBackground()
        End Sub

        Private Sub PasteRecipients()
            If Not IsNothing(MyGlobals.ClipboardString) Then
                Dim cs = Split(MyGlobals.ClipboardString, ",")
                Using con As New in1Entities
                    For Each itm In cs
                        Dim cid = CInt(Val(itm))
                        Dim query = (From c In con.Customers Where c.CustomerID = cid Select c.CustomerID, c.Mail1, c.Gender).FirstOrDefault

                        If Not IsNothing(query) Then
                            InsertintoList(query.Mail1, query.CustomerID, query.Gender)
                        End If

                    Next
                End Using
            End If
        End Sub


        Private Sub SendBulkMail(ByVal testmail As Boolean)


            Dim context As New in1Entities

            Dim mail As New MailMessage
            Dim sc As New SmtpClient()
            Dim pg As Integer = 0
            Dim i As Integer = 0

            Try


                mail.Headers.Clear()
                mail.Headers.Add("Message-ID", "<" & Guid.NewGuid.ToString & "@" & My.Settings.SMTPServer & ">")

                ' Konfiguration des Mailservers
                sc.Host = My.Settings.SMTPServer
                sc.Port = My.Settings.SMTPPort


                If My.Settings.SMTPAuth = True Then
                    sc.UseDefaultCredentials = False
                    Dim crypt As New CryptConnString

                    Dim cred As New NetworkCredential(My.Settings.SMTPUsername, crypt.encryptString(My.Settings.SMTPPassword))
                    sc.Credentials = cred
                End If

                If My.Settings.SMTPSSL Then
                    sc.EnableSsl = True
                End If

                mail.From = New MailAddress(My.Settings.SMTPSenderemail, My.Settings.SMTPSendername)
                mail.Subject = Betreff
                mail.IsBodyHtml = False
                mail.Body = Mailtext

                If Not IsNothing(Anlage) And File.Exists(Anlage) Then
                    Dim fs = New FileStream(Anlage, FileMode.Open, FileAccess.Read)
                    Dim attach As New Attachment(fs, Path.GetFileName(Anlage), MediaTypeNames.Application.Octet)
                    mail.Attachments.Add(attach)
                End If


                If testmail Then
                    mail.To.Clear()
                    mail.To.Add(New MailAddress(TestmailRecipient))
                    sc.Send(mail)

                Else


                    For Each itm In OCRecipients
                        mail.To.Clear()
                        mail.To.Add(itm.Email)

                        If itm.CustomerID > 0 Then
                            Dim log As New Activity
                            log.ActivityType = 1
                            log.Direction = 2
                            log.Zeit = Now
                            log.CustomerID = itm.CustomerID
                            log.UserID = MyGlobals.CurrentUserID
                            log.Comment = "Mail: " & Betreff
                            context.Activities.Add(log)
                            context.SaveChanges()
                        End If

                        itm.Sent = Now

                        sc.Send(mail)
                        i = i + 1
                        pg = Int(i * 100 / OCRecipients.Count)
                        bgw.ReportProgress(pg)
                    Next
                End If

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

        End Sub


    End Class
End Namespace


Public Class MailRecipient
    Inherits ViewModelBase

    Public Property Guid As Guid
    Public Property CustomerID As Integer
    Public Property Email As String
    Public Property Gender As String

    Private _Sent As DateTime
    Public Property Sent As DateTime
        Get
            Return _Sent
        End Get
        Set(value As DateTime)
            _Sent = value
            RaisePropertyChanged("Sent")
        End Set
    End Property


End Class