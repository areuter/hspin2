﻿Imports HSPinOne.Infrastructure

Imports Prism.Ioc
Imports Prism.Modularity
Imports Prism.Regions

Public Class BulkmailModule
    Implements IModule

    Public Sub RegisterTypes(containerRegistry As IContainerRegistry) Implements IModule.RegisterTypes
        containerRegistry.RegisterForNavigation(GetType(MailView), "MailView")
        containerRegistry.RegisterForNavigation(GetType(SettingsView), "SettingsView")
    End Sub

    Public Sub OnInitialized(containerProvider As IContainerProvider) Implements IModule.OnInitialized
        Dim rm = containerProvider.Resolve(Of IRegionManager)()
        rm.RegisterViewWithRegion(RegionNames.TaskRegion, GetType(BulkmailOutlookSection))

    End Sub
End Class
