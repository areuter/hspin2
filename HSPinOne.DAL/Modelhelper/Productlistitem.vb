﻿
Public Class Productlistitem

    Public Event PreisChanged As EventHandler

    Public Property Gruppe As String
    Public Property Artikelbezeichnung As String
    Public Property Zusatzinfo As String
    Public Property Preis As Decimal
    Public Property TaxPercent As Decimal = 0
    Public Property CourseID As Object
    Public Property ProductID As Object
    Public Property PackageID As Object
    Public Property Packagecredit As Nullable(Of Integer)
    Public Property CostcenterID As Nullable(Of Integer)
    Public Property Frei As Object
    Public Property Faelligkeit As DateTime
    Public Property ProductTypeID As Integer
    Public Property CategoryID As Integer
    Public Property Anmeldungvon As DateTime
    Public Property Anmeldungbis As DateTime
    Public Property Anmeldungerlaubt As Boolean
    Public Property Showanmeldezeitraum As Boolean
    Public Property Innenauftrag As String
    Public Property Sachkonto As String
    Public Property KSTextern As String
    Public Property Steuerkennzeichen As String


    Private _canstorno As Boolean = True
    Public Property CanStorno As Boolean
        Get
            Return _canstorno
        End Get
        Set(ByVal value As Boolean)
            _canstorno = value
        End Set
    End Property




End Class
