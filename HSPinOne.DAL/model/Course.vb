﻿Imports System.ComponentModel
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Linq

Public Class Course
    Implements IDataErrorInfo


    Private inTxn As Boolean = False
    Private backup As Course

    Private _newrecord As Boolean = False
    <NotMapped()> _
    Public Property NewRecord As Boolean
        Get
            Return _newrecord
        End Get
        Set(ByVal value As Boolean)
            _newrecord = value
        End Set
    End Property

    Private _IsSelected As Boolean = False
    <NotMapped()> _
    Public Property IsSelected As Boolean
        Get
            Return _IsSelected
        End Get
        Set(value As Boolean)
            _IsSelected = value
            NotifyPropertyChanged("IsSelected")
        End Set
    End Property

    <NotMapped()> _
    Public ReadOnly Property aktTN As Integer
        Get
            Return Me.Billings.Where(Function(o) o.IsStorno = False).Count
        End Get
    End Property

    <NotMapped()> _
    Public ReadOnly Property IsFull As Boolean
        Get
            If Not IsNothing(maxTN) AndAlso maxTN > 0 Then
                If aktTN >= maxTN Then Return True
            End If
            Return False
        End Get
    End Property

    <NotMapped()>
    Public ReadOnly Property Wota As Integer
        Get
            Return Weekday(Me.DTStart, FirstDayOfWeek.Monday)
        End Get
    End Property

    Private _freePlaces As Integer
    Public ReadOnly Property freePlaces As Integer
        Get
            If IsNothing(maxTN) Then Return -1
            If maxTN = 0 Then Return -1
            Dim tn As Integer
            tn = maxTN - aktTN
            If tn < 0 Then Return 0
            Return tn
        End Get
    End Property


    Private m_validationErrors As New Dictionary(Of String, String)



    Private Sub AddError(ByVal columnName As String, ByVal msg As String)
        If Not m_validationErrors.ContainsKey(columnName) Then
            m_validationErrors.Add(columnName, msg)
        End If
    End Sub

    Private Sub RemoveError(ByVal columnName As String)
        If m_validationErrors.ContainsKey(columnName) Then
            m_validationErrors.Remove(columnName)
        End If
    End Sub

    Public ReadOnly Property HasErrors() As Boolean
        Get
            Return m_validationErrors.Count > 0
        End Get
    End Property

    Public ReadOnly Property [Error] As String Implements IDataErrorInfo.Error
        Get
            If m_validationErrors.Count > 0 Then
                Return "Veranstaltungsdaten sind ungültig"
            Else
                Return Nothing
            End If
        End Get
    End Property

    Default Public ReadOnly Property Item(columnName As String) As String Implements System.ComponentModel.IDataErrorInfo.Item
        Get
            If m_validationErrors.ContainsKey(columnName) Then
                Return m_validationErrors(columnName).ToString
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public Sub New()
        'Set defaults
        Me.SportID = 0
        Me.Sport = Nothing
        Me.LocationID = 0
        Me.CourseStateID = 0
        Me.PartyID = 0
        Me.CourseRegistrationID = 0
        Me.CourseGenderID = 0
        Me.CourseCostID = 0
        Me.CourseAudienceID = 0
        Me.CostcenterID = 0
        Me.Erstelltam = Now
        Me.Aktiv = True
        Me.CostcenterID = 0


    End Sub




    Private Sub OnSportIDChanged()
        If IsNothing(_SportID) OrElse _SportID = 0 Then
            Me.AddError("SportID", "Sportart muss ausgewählt sein")
        Else
            Me.RemoveError("SportID")
        End If
    End Sub



    Private Sub OnPartyIDChanged()
        If IsNothing(_PartyID) OrElse _PartyID = 0 Then
            Me.AddError("PartyID", "Anbieter muss ausgewählt sein")
        Else
            Me.RemoveError("PartyID")
        End If
    End Sub

    Private Sub OnCourseGenderIDChanged()
        If IsNothing(_CourseGenderID) OrElse _CourseGenderID = 0 Then
            Me.AddError("CourseGenderID", "Das Geschlecht muss ausgewählt sein")
        Else
            Me.RemoveError("CourseGenderID")
        End If
    End Sub

    Private Sub OnCourseRegistrationIDChanged()
        If IsNothing(_CourseRegistrationID) OrElse _CourseRegistrationID = 0 Then
            Me.AddError("CourseRegistrationID", "Die Anmeldeinformationen muss ausgewählt sein.")
        Else
            Me.RemoveError("CourseRegistrationID")
        End If
    End Sub

    Private Sub OnCourseAudienceIDChanged()
        If IsNothing(_CourseAudienceID) OrElse _CourseAudienceID = 0 Then
            Me.AddError("CourseAudienceID", "Die Zielgruppe muss ausgewählt sein.")
        Else
            Me.RemoveError("CourseAudienceID")
        End If
    End Sub

    Private Sub OnCourseCostIDChanged()
        If IsNothing(_CourseCostID) OrElse _CourseCostID = 0 Then
            Me.AddError("CourseCostID", "Die Kosteninformation muss ausgewählt sein.")
        Else
            Me.RemoveError("CourseCostID")
        End If
    End Sub

    Private Sub OnCourseStateIDChanged()
        If IsNothing(_CourseStateID) OrElse _CourseStateID = 0 Then
            Me.AddError("CourseStateID", "Der Bearbeitungsstatus muss ausgewählt sein.")
        Else
            Me.RemoveError("CourseStateID")
        End If
    End Sub


    Private Sub OnLocationIDChanged()
        If IsNothing(_LocationID) OrElse _LocationID = 0 Then
            Me.AddError("LocationID", "Ein Ort muss ausgewählt sein")
        Else
            Me.RemoveError("LocationID")
        End If
    End Sub


    Private Sub OnCostcenterIDChanged()
        If IsNothing(_CostcenterID) OrElse _CostcenterID = 0 Then
            Me.AddError("CostcenterID", "Eine Kostenstelle muss ausgewählt sein")
        Else
            Me.RemoveError("CostcenterID")
        End If
    End Sub


    Private Sub OnFaelligkeitChanged()
        If Not IsNothing(_Faelligkeit) And IsDate(_Faelligkeit) Then
            For Each itm In Me.Billings
                itm.Faelligkeit = _Faelligkeit
            Next
        End If
    End Sub
End Class
