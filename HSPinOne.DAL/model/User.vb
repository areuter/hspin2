﻿Imports System.ComponentModel.DataAnnotations.Schema

Partial Public Class User


    Private _newrecord As Boolean = False
    <NotMapped()> _
    Public Property NewRecord As Boolean
        Get
            Return _newrecord
        End Get
        Set(ByVal value As Boolean)
            _newrecord = value
        End Set
    End Property

End Class
