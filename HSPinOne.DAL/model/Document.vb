﻿Partial Public Class Document

    Public Enum Doctype
        Kundendokument
        Kundenfoto
        Ortsbild
        Sportbild
        Dokumentvorlage
    End Enum

    Public Sub New()

    End Sub

    Public Sub New(ByVal infotext As String, erstelldatum As Date, dateiname As String, documenttype As Doctype)

        Try


            If System.IO.File.Exists(dateiname) Then
                Me.Infotext = infotext
                Me.Erstelldatum = erstelldatum
                Me.Dateiname = System.IO.Path.GetFileNameWithoutExtension(dateiname)
                Me.Extension = Mid(IO.Path.GetExtension(dateiname), 2)
                Me.DocumentType = documenttype
                Me.DocumentData = IO.File.ReadAllBytes(dateiname)
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Fehler")

        End Try
    End Sub
End Class
