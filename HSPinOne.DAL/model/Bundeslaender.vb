﻿
Imports System.Collections.Generic

Public Class Bundeslaender

    Private _bl As Dictionary(Of Long, String)
    Public Property BL As Dictionary(Of Long, String)
        Get
            Return _bl
        End Get
        Set(value As Dictionary(Of Long, String))
            _bl = value
        End Set
    End Property



    Public Function GetBundeslandbyName(ByVal bula As String) As Long

        For Each itm In BL
            If itm.Value = bula Then Return itm.Key
        Next

        Return 0
    End Function

    Public Function GetBundeslaender() As Dictionary(Of Long, String)

        Return BL
    End Function

    Public Sub New()
        BL = New Dictionary(Of Long, String)

        BL.Add(2 ^ 0, "Baden-Württemberg")
        BL.Add(2 ^ 1, "Bayern")
        BL.Add(2 ^ 2, "Berlin")
        BL.Add(2 ^ 3, "Brandenburg")
        BL.Add(2 ^ 4, "Bremen")
        BL.Add(2 ^ 5, "Hamburg")
        BL.Add(2 ^ 6, "Hessen")
        BL.Add(2 ^ 7, "Mecklenburg-Vorpommern")
        BL.Add(2 ^ 8, "Niedersachsen")
        BL.Add(2 ^ 9, "Nordrhein-Westfalen")
        BL.Add(2 ^ 10, "Rheinland-Pfalz")
        BL.Add(2 ^ 11, "Saarland")
        BL.Add(2 ^ 12, "Sachsen")
        BL.Add(2 ^ 13, "Sachsen-Anhalt")
        BL.Add(2 ^ 14, "Schleswig-Holstein")
        BL.Add(2 ^ 15, "Thüringen")



    End Sub


End Class
