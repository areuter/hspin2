﻿Public Class Category

    Public Overrides Function Equals(obj As Object) As Boolean
        If IsNothing(obj) Or Not obj.GetType() = GetType(Category) Then Return False

        Return CType(obj, Category).CategoryID = Me.CategoryID
        'Return MyBase.Equals(obj)
    End Function

End Class
