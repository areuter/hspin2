﻿Option Strict Off
Imports Microsoft.VisualBasic
Imports System.Text
Imports System.Collections.Generic
Imports System

Public Enum FrequencyType
    None
    Secondly
    Minutely
    Hourly
    Daily
    Weekly
    Monthly
    Yearly
End Enum

Public Class RRule

    Public Property Until As Nullable(Of DateTime)
    Public Property ByDay As New List(Of String)
    Public Property Freq As FrequencyType

    Public Function GetRuleString() As String
        Dim str As New StringBuilder

        If Not IsNothing(Until) Then
            Dim dat = New DateTime(Year(Until), Month(Until), Day(Until), 0, 0, 0)
            str.Append("UNTIL=" & dat.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'") & ";")
        End If

        If Not IsNothing(ByDay) Then
            Dim days As String = ""
            For Each itm In ByDay
                days = itm.ToString & ","
            Next
            str.Append("BYDAY=" & days & ";")

        End If

        If Not IsNothing(Freq) Then
            str.Append("FREQ=" & Freq.ToString & ";")
        End If
        If str.Length > 0 Then Return str.ToString

        Return String.Empty
    End Function



    Public Sub New(ByVal str As String)
        If Not String.IsNullOrEmpty(str) Then
            Dim arr As String() = Split(str, ";")
            For i = 0 To UBound(arr) - 1
                Dim typ = arr(i).Split("=")
                If typ(0) = "UNTIL" Then Until = typ(1)
                If typ(0) = "BYDAY" Then
                    Dim days = Split(typ(1), ",")
                    ByDay.Clear()
                    For j = 0 To UBound(days) - 1
                        ByDay.Add(days(j))
                    Next
                End If

                If typ(0) = "FREQ" Then Freq = DirectCast([Enum].Parse(GetType(FrequencyType), typ(1)), FrequencyType)
            Next
        End If


    End Sub

    Public Sub New()

    End Sub

End Class
