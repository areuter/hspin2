﻿Imports System.ComponentModel
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations.Schema
Partial Public Class Activity

    Private _newRecord As Boolean = False
    <NotMapped> _
    Public Property NewRecord As Boolean
        Get
            Return _newRecord
        End Get
        Set(value As Boolean)
            _newRecord = value

        End Set
    End Property

End Class
