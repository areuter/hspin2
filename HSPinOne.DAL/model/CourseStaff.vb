﻿Imports System.ComponentModel
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations.Schema

Partial Public Class CourseStaff


    Private _IsSelected As Boolean = False
    <NotMapped()> _
    Public Property IsSelected As Boolean
        Get
            Return _IsSelected
        End Get
        Set(ByVal value As Boolean)
            _IsSelected = value
            NotifyPropertyChanged("IsSelected")
        End Set
    End Property



End Class
