﻿Imports System.ComponentModel
Imports System.Collections.Generic
Partial Public Class Booking
    Implements IDataErrorInfo

    Private m_validationErrors As New Dictionary(Of String, String)

    Private _duration As Long


    Public ReadOnly Property [Error] As String Implements System.ComponentModel.IDataErrorInfo.Error
        Get
            If m_validationErrors.Count > 0 Then
                Return "Buchungsdaten sind ungültig"
            Else
                Return Nothing
            End If
        End Get
    End Property

    Default Public ReadOnly Property Item(columnName As String) As String Implements System.ComponentModel.IDataErrorInfo.Item
        Get
            If m_validationErrors.ContainsKey(columnName) Then
                Return m_validationErrors(columnName).ToString
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property HasErrors() As Boolean
        Get
            Return m_validationErrors.Count > 0
        End Get
    End Property

    Private Sub AddError(ByVal columnName As String, ByVal msg As String)
        If Not m_validationErrors.ContainsKey(columnName) Then
            m_validationErrors.Add(columnName, msg)
        End If
    End Sub

    Private Sub RemoveError(ByVal columnName As String)
        If m_validationErrors.ContainsKey(columnName) Then
            m_validationErrors.Remove(columnName)
        End If
    End Sub


    Private Sub OnLocationIDChanged()
        If IsNothing(_LocationID) Or LocationID = 0 Then
            Me.AddError("LocationID", "Sie müssen einen Ort auswählen")
        Else
            Me.RemoveError("LocationID")
        End If
    End Sub

    Private Sub OnSubjectChanged()
        If IsNothing(_Subject) Or Trim(_Subject) = "" Then
            Me.AddError("Subject", "Sie müssen einen Buchungstext eingeben!")
        ElseIf Len(_Subject) > 100 Then
            Me.AddError("Subject", "Der Buchungstext darf nur 100 Zeichen lang sein!")
        Else
            Me.RemoveError("Subject")
        End If
    End Sub

    Private Sub OnStartChanged()
        If Not IsNothing(_Start) And IsDate(_Start) Then

            'Zeitdifferenz bestimmen
            If IsNothing(_duration) Or _duration = 0 Then _duration = 60
            Ende = DateAdd(DateInterval.Minute, _duration, _Start)
            _duration = DateDiff(DateInterval.Minute, Start, Ende)

        End If

    End Sub


    Private Sub OnEndeChanged()
        If Not IsNothing(Start) And Not IsNothing(Ende) Then
            _duration = DateDiff(DateInterval.Minute, Start, Ende)
        End If
    End Sub

    Private Sub OnLocationChanged()
        If IsNothing(Location) Then
            Me.AddError("Location", "Sie müssen einen Ort auswählen")
        Else
            Me.RemoveError("Location")
        End If
    End Sub


    Private Sub OnCategoryChanged()
        If Not IsNothing(_Category) Then
            Me.Color = _Category.CategoryColor

        End If
    End Sub

    Private Sub OnCategoryIDChanged()
        If Not IsNothing(_Category) Then
            Me.Color = _Category.CategoryColor
        End If
    End Sub
End Class
