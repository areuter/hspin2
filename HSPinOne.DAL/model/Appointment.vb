﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Collections.Generic
Imports System.ComponentModel

Partial Public Class Appointment
    Implements IDataErrorInfo

    Private m_validationErrors As New Dictionary(Of String, String)

    Private Sub AddError(ByVal columnName As String, ByVal msg As String)
        If Not m_validationErrors.ContainsKey(columnName) Then
            m_validationErrors.Add(columnName, msg)
        End If
    End Sub

    Private Sub RemoveError(ByVal columnName As String)
        If m_validationErrors.ContainsKey(columnName) Then
            m_validationErrors.Remove(columnName)
        End If
    End Sub

    Public ReadOnly Property HasErrors() As Boolean
        Get
            Return m_validationErrors.Count > 0
        End Get
    End Property

    Public ReadOnly Property [Error] As String Implements System.ComponentModel.IDataErrorInfo.Error
        Get
            If m_validationErrors.Count > 0 Then
                Return "Appointment data is invalid"
            Else
                Return Nothing
            End If
        End Get
    End Property

    Default Public ReadOnly Property Item(columnName As String) As String Implements System.ComponentModel.IDataErrorInfo.Item
        Get
            If m_validationErrors.ContainsKey(columnName) Then
                Return m_validationErrors(columnName).ToString
            Else
                Return Nothing
            End If
        End Get
    End Property




    Private _conflict As Boolean
    <NotMapped()> _
    Public Property Conflict As Boolean
        Get
            Return _conflict
        End Get
        Set(ByVal value As Boolean)
            _conflict = value
            NotifyPropertyChanged("Conflict")
        End Set
    End Property

    Public ReadOnly Property AppointmentStaffsString As String
        Get


            If Not IsNothing(Me.AppointmentStaffs) Then
                Dim liste As New List(Of String)
                For Each itm In AppointmentStaffs
                    liste.Add(itm.Customer.Nachname)
                Next
                Return String.Join("/", liste.ToArray)
                NotifyPropertyChanged("AppointmentStaffsString")
            Else
                Return String.Empty
            End If

        End Get
    End Property

    Public Sub New()
        Me._Canceled = False
    End Sub


    Private Sub OnStartChanged()
        If IsNothing(_Start) Then
            Me.AddError("Start", "Der Beginn muss festgelegt werden")
        Else
            Me.RemoveError("Start")
        End If


        Spanne()
    End Sub

    Private Sub OnEndeChanged()
        If IsNothing(_Ende) Then
            Me.AddError("Ende", "Das Ende muss festgelegt werden")
        Else
            Me.RemoveError("Ende")
        End If

        Spanne()
    End Sub

    Private Sub Spanne()
        If Not IsNothing(_Start) And Not IsNothing(_Ende) Then
            If DateDiff(DateInterval.Minute, _Start, _Ende) <= 0 Then
                Me.AddError("Start", "Das Ende muss größer als der Beginn sein")
                Me.AddError("Ende", "Das Ende muss größer als der Beginn sein")
            Else
                Me.RemoveError("Start")
                Me.RemoveError("Ende")
            End If
            NotifyPropertyChanged("Start")
            NotifyPropertyChanged("Ende")
        End If
    End Sub
End Class
