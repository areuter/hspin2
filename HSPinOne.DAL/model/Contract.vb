﻿Imports System.ComponentModel
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations.Schema

Partial Public Class Contract
    Implements IDataErrorInfo

    Private m_validationErrors As New Dictionary(Of String, String)
    Private _newrecord As Boolean



    Private Sub AddError(ByVal columnName As String, ByVal msg As String)
        If Not m_validationErrors.ContainsKey(columnName) Then
            m_validationErrors.Add(columnName, msg)
        End If
    End Sub

    Private Sub RemoveError(ByVal columnName As String)
        If m_validationErrors.ContainsKey(columnName) Then
            m_validationErrors.Remove(columnName)
        End If
    End Sub

    <NotMapped()> _
    Public Property NewRecord As Boolean
        Get
            Return _newrecord
        End Get
        Set(value As Boolean)
            _newrecord = value
        End Set
    End Property

    Public ReadOnly Property HasErrors() As Boolean
        Get
            Return m_validationErrors.Count > 0
        End Get
    End Property

    Public ReadOnly Property [Error] As String Implements System.ComponentModel.IDataErrorInfo.Error
        Get
            If m_validationErrors.Count > 0 Then
                Return "Customer data is invalid"
            Else
                Return Nothing
            End If
        End Get
    End Property

    Default Public ReadOnly Property Item(columnName As String) As String Implements System.ComponentModel.IDataErrorInfo.Item
        Get
            If m_validationErrors.ContainsKey(columnName) Then
                Return m_validationErrors(columnName).ToString
            Else
                Return Nothing
            End If
        End Get
    End Property




    Public Sub New()
        'Set defaults


    End Sub



    Private Sub OnVertragsbeginnChanged()
        If IsNothing(_Vertragsbeginn) Then
            Me.AddError("Vertragsbeginn", "Der Vertragsbeginn muss festgelegt werden")
        Else
            Me.RemoveError("Vertragsbeginn")
        End If


        If Not IsNothing(_Vertragsbeginn) And Not IsNothing(_Monate) Then
            _Vertragsbeginn = _Vertragsbeginn.Date
            'Vertragsende = DateAdd(DateInterval.Month, CDbl(_Monate), CDate(_Vertragsbeginn).Date).AddMinutes(-1)
            UpdateVertragsende()
            Zahlungsbeginn = _Vertragsbeginn
        End If

    End Sub




    Private Sub OnKuendigungseingangChanged()
        If Not IsNothing(_Vertragsende) And Not IsNothing(_Kuendigungseingang) Then
            If IsDate(_Kuendigungseingang) Then Kuendigungzum = _Vertragsende
        End If
    End Sub




    Private Sub OnMonateChanged()
        If Not IsNothing(_Vertragsbeginn) And Not IsNothing(_Monate) Then
            'Vertragsende = DateAdd(DateInterval.Month, CDbl(_Monate), CDate(_Vertragsbeginn))
            UpdateVertragsende()
        End If

    End Sub


    Private Sub OnCostcenterIDChanged()
        If IsNothing(_CostcenterID) Or _CostcenterID = 0 Then
            Me.AddError("CostcenterID", "Eine Kostenstelle muss festgelegt werden")
        Else
            Me.RemoveError("CostcenterID")
        End If
    End Sub

    Private Sub OnVerlaengerungsmonateChanged()
        If IsNothing(_Verlaengerungsmonate) Or Not IsNumeric(Verlaengerungsmonate) OrElse _Verlaengerungsmonate < 0 Then
            Me.AddError("Verlaengerungsmonate", "Verlaengerungsmonate muss eine Zahl sein")
        Else
            Me.RemoveError("Verlaengerungsmonate")
        End If
    End Sub

    Private Sub UpdateVertragsende()
        Vertragsende = DateAdd(DateInterval.Month, CDbl(_Monate), CDate(_Vertragsbeginn).Date).AddMinutes(-1)
    End Sub
End Class
