﻿Imports Newtonsoft.Json
Public Class Storno

    Public Sub New()

    End Sub
    Public Sub New(ByVal billingid As Integer, ByVal strreason As String)
        StornoID = billingid
        Reason = strreason
    End Sub

    Public Sub New(ByVal jsonstorno As String)
        Dim objstorno = JsonConvert.DeserializeObject(jsonstorno)
        If Not IsNothing(objstorno) Then
            Me.StornoID = objstorno.StornoID
            Me.Reason = objstorno.Reason
        End If
    End Sub

    Public Property StornoID As Integer
    Public Property Reason As String

    Public Function GetJson() As String
        Return JsonConvert.SerializeObject(Me)
    End Function

    Public Function GetObject() As Storno
        Return Me
    End Function

    Public Shared Function IsStorno(ByVal json As String) As Boolean
        Dim st = New Storno(json)
        If Not IsNothing(st) AndAlso st.StornoID > 0 Then Return True
        Return False
    End Function

End Class
