﻿Imports System.Security.Principal

Public Class IdentityBusinessObject
    Implements IIdentity

    Private _name As String = String.Empty
    Private _authType As String = String.Empty
    Private _isAuth As Boolean = True
    Private _institutionID As Integer = 0
    Private _userid As Integer = 0
    Private _loginID As String = String.Empty

    Public Sub New(ByVal userId As Integer, ByVal institutionID As Integer, ByVal userName As String, ByVal loginID As String, ByVal authenticationType As String)
        _name = userName
        _userid = userId
        _institutionID = institutionID
        _authType = authenticationType
        _loginID = loginID
    End Sub



    Public ReadOnly Property UserId() As Integer
        Get
            Return _userid
        End Get
    End Property

    Public ReadOnly Property InstitutionID() As Integer
        Get
            Return _institutionID
        End Get
    End Property

#Region "IIdentity Members"

    Public ReadOnly Property AuthenticationType() As String Implements IIdentity.AuthenticationType
        Get
            Return _authType
        End Get
    End Property

    Public ReadOnly Property IsAuthenticated() As Boolean Implements IIdentity.IsAuthenticated
        Get
            Return _isAuth
        End Get
    End Property

    Public ReadOnly Property Name() As String Implements IIdentity.Name
        Get
            Return _name
        End Get
    End Property


    Public ReadOnly Property LoginID() As String
        Get
            Return _loginID
        End Get
    End Property


#End Region
End Class
