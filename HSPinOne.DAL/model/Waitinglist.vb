﻿Imports System.ComponentModel.DataAnnotations.Schema

Partial Public Class Waitinglist


    Private _isselected As Boolean = False
    <NotMapped()> _
    Public Property IsSelected As Boolean
        Get
            Return _isselected
        End Get
        Set(ByVal value As Boolean)
            _isselected = value
            NotifyPropertyChanged("IsSelected")
        End Set
    End Property
End Class
