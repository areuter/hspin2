﻿Imports System.ComponentModel
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.Linq

Public Class Customer
    Implements IDataErrorInfo


    Const AllowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

    Private m_validationErrors As New Dictionary(Of String, String)
    Private _newrecord As Boolean

    <NotMapped()>
    Public Property NewRecord As Boolean
        Get
            Return _newrecord
        End Get
        Set(value As Boolean)
            _newrecord = value
        End Set
    End Property



    Private Sub AddError(ByVal columnName As String, ByVal msg As String)
        If Not m_validationErrors.ContainsKey(columnName) Then
            m_validationErrors.Add(columnName, msg)
        End If
    End Sub

    Private Sub RemoveError(ByVal columnName As String)
        If m_validationErrors.ContainsKey(columnName) Then
            m_validationErrors.Remove(columnName)
        End If
    End Sub

    Public ReadOnly Property HasErrors() As Boolean
        Get
            Return m_validationErrors.Count > 0
        End Get
    End Property

    Public ReadOnly Property [Error] As String Implements System.ComponentModel.IDataErrorInfo.Error
        Get
            If m_validationErrors.Count > 0 Then
                Return "Customer data is invalid"
            Else
                Return Nothing
            End If
        End Get
    End Property

    Default Public ReadOnly Property Item(columnName As String) As String Implements System.ComponentModel.IDataErrorInfo.Item
        Get
            If m_validationErrors.ContainsKey(columnName) Then
                Return m_validationErrors(columnName).ToString
            Else
                Return Nothing
            End If
        End Get
    End Property



    Private Sub OnNachnameChanged()
        'Perform validation. 
        If Me._Nachname Is Nothing OrElse Me._Nachname.Trim() = "" OrElse Me._Nachname.Trim() = "[neu]" Then
            Me.AddError("Nachname", "Bitte einen Nachnamen eingeben.")
        Else
            Me.RemoveError("Nachname")
        End If
        Autofill()
    End Sub

    Private Sub OnVornameChanged()
        'Perform validation. 
        If _Vorname Is Nothing OrElse _Vorname.Trim() = "" Then
            Me.AddError("Vorname", "Bitte einen Vornamen eingeben.")
        Else
            Me.RemoveError("Vorname")
        End If
        Autofill()
    End Sub

    Private Sub OnGenderChanged()
        'Perform validation. 
        If _Gender Is Nothing OrElse _Gender.Trim() = "" Then
            Me.AddError("Gender", "Bitte ein Geschlecht auswählen.")
        Else
            Me.RemoveError("Gender")
        End If
        Autofill()
    End Sub


    Private Sub OnCustomerStateIDChanged()
        If IsNothing(_CustomerStateID) Or _CustomerStateID = 0 Then
            Me.AddError("CustomerStateID", "Bitte einen Status auswählen")
        Else
            Me.RemoveError("CustomerStateID")
        End If
    End Sub


    Private Sub OnSuchnameChanged()
        If _Suchname Is Nothing OrElse _Suchname.Trim() = "" Then
            Me.AddError("Suchname", "Bitte einen Suchnamen eingeben.")
        Else
            Me.RemoveError("Suchname")
        End If
    End Sub




    Private Sub Autofill()
        If NewRecord Then
            If _Gender = "m" Then
                Briefanrede = "Sehr geehrter Herr "
            ElseIf _Gender = "w" Then
                Briefanrede = "Sehr geehrte Frau "
            Else
                Briefanrede = "Sehr geehrtes Enby "
            End If
            If Not Titel Is Nothing Then
                Briefanrede = Briefanrede & Titel & " "
            End If
            Briefanrede = Briefanrede & _Nachname


            Suchname = _Nachname & ", " & _Vorname


            Kontoinhaber = Left(Suchname, 27)

        End If
    End Sub


    Private Sub OnIBANChanged()
        If Not IsNothing(_IBAN) AndAlso Not Trim(_IBAN) = "" Then
            If Not CheckForAlphaCharacters(_IBAN) Then
                Me.AddError("IBAN", "IBAN darf nur Großbuchstaben A-Z und 0-9 beinhalten")
            Else
                Me.RemoveError("IBAN")
                If _IBAN.Length < 14 Or _IBAN.Length > 34 Then
                    Me.AddError("IBAN", "IBAN ist zu kurz oder zu lang")
                Else
                    Me.RemoveError("IBAN")
                End If
            End If


        Else
            Me.RemoveError("IBAN")
        End If
    End Sub



    Private Function CheckForAlphaCharacters(ByVal StringToCheck As String)

        If Not IsNothing(StringToCheck) Then
            For i = 0 To StringToCheck.Length - 1
                If Not AllowedChars.Contains(StringToCheck.Chars(i)) Then
                    Return False
                End If
            Next
        End If
        Return True

    End Function

    Private Sub OnBICChanged()
        If Not _BIC Is Nothing AndAlso Not Trim(_BIC) = "" Then
            If Not CheckForAlphaCharacters(_IBAN) Then
                Me.AddError("BIC", "BIC darf nur Großbuchstaben A-Z und 0-9 beinhalten")
            Else
                Me.RemoveError("BIC")
                If _BIC.Length < 8 Or _BIC.Length > 11 Then
                    Me.AddError("BIC", "Die BIC ist zu kurz oder zu lang")
                Else
                    Me.RemoveError("BIC")
                End If
            End If


        Else
            Me.RemoveError("BIC")
        End If
    End Sub

    Private Sub OnKartennummerChanged()

    End Sub
End Class
