﻿Imports System.ComponentModel.DataAnnotations.Schema
Imports System.ComponentModel
Imports System.Collections.Generic


Partial Public Class Billing
    Implements IDataErrorInfo



    Private m_validationErrors As New Dictionary(Of String, String)

    Private ReadOnly dataErrorInfoSupport As DataErrorInfoSupport



    Public ReadOnly Property [Error] As String Implements System.ComponentModel.IDataErrorInfo.Error
        Get
            Return dataErrorInfoSupport.Error
        End Get
    End Property

    Default Public ReadOnly Property Item(columnName As String) As String Implements System.ComponentModel.IDataErrorInfo.Item
        Get
            Return dataErrorInfoSupport(columnName)
        End Get
    End Property


    Private _kasse As Boolean = False
    <NotMapped()> _
    Public Property Kasse As Boolean
        Get
            Return _kasse
        End Get
        Set(ByVal value As Boolean)
            _kasse = value
        End Set
    End Property

    Private _cancartdel As Boolean = False
    <NotMapped()> _
    Public Property CanCartDel As Boolean
        Get
            Return _cancartdel
        End Get
        Set(ByVal value As Boolean)
            _cancartdel = value
        End Set
    End Property

    Private _stornoid As Long
    <NotMapped()> _
    Public Property StornoID As Long
        Get
            Return _stornoid
        End Get
        Set(ByVal value As Long)
            _stornoid = value
        End Set
    End Property

    Private _checked As Boolean = False
    <NotMapped()>
    Public Property Checked As Boolean
        Get
            Return _checked
        End Get
        Set(ByVal value As Boolean)
            _checked = value
            NotifyPropertyChanged("Checked")
            OnCheckedChanged()
        End Set
    End Property

    Partial Private Sub OnCheckedChanged()

    End Sub

    Private _NewRecord As Boolean = False
    Public Property NewRecord() As Boolean
        Get
            Return _NewRecord
        End Get
        Set(ByVal value As Boolean)
            _NewRecord = value
        End Set
    End Property

    Public Sub New()
        dataErrorInfoSupport = New DataErrorInfoSupport(Me)
    End Sub
End Class
