﻿
Imports System.Linq
Public Class Customer_Bal

    Private _context As in1Entities

    Public Sub New()

    End Sub

    Public Sub New(ByRef con As in1Entities)
        _context = con

    End Sub



    Public Shared Function CustomerfromCard(ByVal card As String) As Integer
        Using con As New in1Entities
            Dim res = From c In con.Customers Where c.Kartennummer = card Select c.CustomerID

            If Not IsNothing(res) AndAlso res.Count > 0 Then
                Return res.FirstOrDefault
            End If
            Return -1
        End Using
    End Function

End Class
