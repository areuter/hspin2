﻿Imports System.Collections.Generic
Imports System.Data.Entity
Imports System.Linq
Imports System.Security.Cryptography
Imports System.Text

Public Class Billing_Bal

    Private ReadOnly _user As String

    Public Sub New(ByVal user As String)
        _user = user
    End Sub

    Public Function AddBillingFromPackage(ByVal customerid As Integer, ByVal packageid As Integer,
                                          ByVal accountid As Integer, ByVal paymentmethodid As Integer,
                                          ByVal kasse As Integer, Optional faelligkeit As DateTime? = Nothing) As Integer

        Using context As New in1Entities
            Dim customer = context.Customers.Find(customerid)
            If IsNothing(faelligkeit) Then faelligkeit = Now
            Return PAddBillingFromPackage(customer.CustomerID, Nothing, customer.CustomerStateID, packageid, accountid, paymentmethodid, kasse, faelligkeit)
        End Using
    End Function

    Public Function AddBillingFromPackage(ByVal checkinid As Integer, ByVal packageid As Integer,
                                          ByVal accountid As Integer, ByVal paymentmethodid As Integer,
                                          ByVal kasse As Integer, ByVal statedid As Integer,
                                          Optional faelligkeit As DateTime? = Nothing) As Integer

        Using context As New in1Entities
            Dim checkin = context.Checkins.Find(checkinid)
            If IsNothing(faelligkeit) Then faelligkeit = Now
            Return PAddBillingFromPackage(checkin.CustomerID, checkinid, statedid, packageid, accountid, paymentmethodid, kasse, faelligkeit)
        End Using

    End Function

    Private Function PAddBillingFromPackage(ByVal customerid As Integer?, ByVal checkinid As Integer?, ByVal stateid As Integer,
                                           ByVal packageid As Integer, ByVal accountid As Integer,
                                            ByVal paymentmethodid As Integer, ByVal kasse As Integer,
                                            ByVal faelligkeit As DateTime,
                                            Optional ByVal newprice As Decimal = Nothing
                                            ) As Integer


        Dim cashclosingid As Integer = 0
        If kasse > 0 Then
            cashclosingid = Repositories.CashClosingRepository.GetCashClosingID(kasse)
        End If

        Using context As New in1Entities

            Dim bill As New Billing
            Dim package = context.Packages.Find(packageid)

            Dim prices = context.PackagePrices.Where(Function(o) o.PackageID = packageid AndAlso o.CustomerStateID = stateid)
            Dim price = prices.FirstOrDefault
            Dim defaultcostcenter = context.Costcenters.FirstOrDefault()
            Dim acctemplate = context.AccountingTemplates.Include(Function(o) o.Tax).Where(Function(t) t.AccountingTemplateID = price.AccountingTemplateID).FirstOrDefault()


            bill.KasseNr = kasse
            bill.IsStorno = False
            bill.CanCartDel = True

            If cashclosingid > 0 Then
                bill.CashClosingID = cashclosingid
            End If

            If Not IsNothing(customerid) Then
                bill.CustomerID = customerid
            End If
            If Not IsNothing(checkinid) Then
                bill.CheckinID = checkinid
            End If

            bill.CustomerStateID = stateid
            bill.BillingStatus = BillingStates.Offen
            bill.Rechnungsdatum = Now
            bill.ProductTypeID = package.ProductTypeID
            bill.Faelligkeit = faelligkeit
            bill.Verwendungszweck = Left(package.PackageName, 250)
            bill.Checked = True

            bill.AccountID = accountid
            bill.PaymentMethodID = paymentmethodid

            bill.CostcenterID = IIf(IsNothing(price.CostcenterID), defaultcostcenter.CostcenterID, price.CostcenterID)

            If Not IsNothing(acctemplate) Then
                bill.Innenauftrag = acctemplate.Innenauftrag
                bill.Sachkonto = acctemplate.Sachkonto
                bill.KSTextern = acctemplate.Kostenstelle
                bill.Steuerkennzeichen = acctemplate.SteuerkennzeichenAR
                bill.Steuerprozent = acctemplate.Tax.TaxPercent
                bill.AccountingTemplateID = acctemplate.AccountingTemplateID
            End If


            bill.Packagecredit = package.Zutritte
            'bill.CourseID = SelectedProduct.CourseID
            'bill.ProductID = SelectedProduct.ProductID
            bill.PackageID = package.PackageID

            bill.Brutto = price.Preis
            bill.BruttoOriginal = bill.Brutto
            If newprice Then
                bill.Brutto = newprice
            End If

            bill.Hash = Helper.HashBill.CalculateHash(bill)

            context.Billings.Add(bill)
            context.SaveChanges(_user)
            Return bill.BillingID
        End Using

        Return False

    End Function

    Public Function AddBillingFromCourse(ByVal customerid As Integer, ByVal courseid As Integer,
                                         ByVal accountid As Integer, ByVal paymentmethodid As Integer, ByVal kasse As Integer) As Boolean


        Dim cashclosingid As Integer = 0
        If kasse > 0 Then
            cashclosingid = Repositories.CashClosingRepository.GetCashClosingID(kasse)
        End If

        Using context As New in1Entities

            Dim bill As New Billing

            Dim course = context.Courses.Find(courseid)
            Dim customer = context.Customers.Find(customerid)
            Dim prices = context.CoursePrices.Where(Function(o) o.CourseID = courseid AndAlso o.CustomerStateID = customer.CustomerStateID)
            Dim price = prices.FirstOrDefault
            Dim defaultcostcenter = course.CostcenterID
            Dim acctemplate = context.AccountingTemplates.Include(Function(o) o.Tax).Where(Function(t) t.AccountingTemplateID = price.AccountingTemplateID).FirstOrDefault()

            bill.KasseNr = kasse
            bill.IsStorno = False
            bill.CanCartDel = True
            If cashclosingid > 0 Then
                bill.CashClosingID = cashclosingid
            End If

            bill.CustomerID = customer.CustomerID
            bill.CustomerStateID = customer.CustomerStateID
            bill.BillingStatus = "O"
            bill.Rechnungsdatum = Now
            bill.ProductTypeID = course.ProductTypeID
            bill.Faelligkeit = IIf(IsNothing(course.Faelligkeit), Now, course.Faelligkeit)
            bill.Verwendungszweck = course.Sport.Sportname & " " & course.Zusatzinfo & " " & course.Zeittext & " (Start: " & course.DTStart.ToShortDateString & ")"
            bill.Checked = True
            bill.CostcenterID = IIf(IsNothing(price.CostcenterID), defaultcostcenter, price.CostcenterID)

            'bill.Innenauftrag = price.Innenauftrag
            'bill.Sachkonto = price.Sachkonto
            'bill.KSTextern = price.KSTextern
            'bill.Steuerkennzeichen = price.Steuerkennzeichen

            If Not IsNothing(acctemplate) Then


                bill.Innenauftrag = acctemplate.Innenauftrag
                bill.Sachkonto = acctemplate.Sachkonto
                bill.KSTextern = acctemplate.Kostenstelle
                bill.Steuerkennzeichen = acctemplate.SteuerkennzeichenAR
                bill.Steuerprozent = acctemplate.Tax.TaxPercent
                bill.AccountingTemplateID = acctemplate.AccountingTemplateID
            End If

            bill.AccountID = accountid
            bill.PaymentMethodID = paymentmethodid

            bill.Packagecredit = Nothing
            bill.CourseID = course.CourseID
            'bill.ProductID = SelectedProduct.ProductID
            'bill.PackageID = package.PackageID

            bill.Brutto = price.Preis
            'bill.Steuerprozent = price.TaxPercent

            bill.Hash = Helper.HashBill.CalculateHash(bill)

            context.Billings.Add(bill)
            context.SaveChanges(_user)
        End Using



        Return True

    End Function

    Public Function AddBillingFromBooking(ByVal customerid As Integer, ByVal bookingid As Integer) As Billing

        Throw New NotImplementedException()

        'Dim bill As New Billing

        'Using context As New in1Entities


        '    Dim booking = context.Bookings.Find(bookingid)
        '    Dim customer = context.Customers.Find(customerid)
        '    Dim prices = context.CoursePrices.Where(Function(o) o.CourseID = courseid AndAlso o.CustomerStateID = customer.CustomerStateID)
        '    Dim price = prices.FirstOrDefault
        '    Dim defaultcostcenter = course.CostcenterID


        '    bill.Kasse = True
        '    bill.IsStorno = False
        '    bill.CanCartDel = True
        '    bill.CustomerID = customer.CustomerID
        '    bill.CustomerStateID = customer.CustomerStateID
        '    bill.BillingStatus = "O"
        '    bill.Rechnungsdatum = Now
        '    bill.ProductTypeID = course.ProductTypeID
        '    bill.Faelligkeit = IIf(IsNothing(course.Faelligkeit), Now, course.Faelligkeit)
        '    bill.Verwendungszweck = course.Sport.Sportname & " " & course.Zusatzinfo & " " & course.Zeittext & " (Start: " & course.DTStart.ToShortDateString & ")"
        '    bill.Checked = True
        '    bill.CostcenterID = IIf(IsNothing(price.CostcenterID), defaultcostcenter, price.CostcenterID)
        '    bill.Innenauftrag = price.Innenauftrag
        '    bill.Sachkonto = price.Sachkonto
        '    bill.KSTextern = price.KSTextern
        '    bill.Steuerkennzeichen = price.Steuerkennzeichen

        '    bill.Packagecredit = Nothing
        '    bill.CourseID = course.CourseID
        '    'bill.ProductID = SelectedProduct.ProductID
        '    'bill.PackageID = package.PackageID

        '    bill.Brutto = price.Preis
        '    bill.Steuerprozent = price.TaxPercent

        'End Using
        ''context.Billings.Add(bill)
        'Return bill

    End Function

    Public Function AddBillingFromContract(ByVal c As Contract, ByVal faellig As DateTime, ByVal t As ContractRatePrice, ByVal vwz As String, ByVal acctemplate As AccountingTemplate, ByVal accountid As Integer) As Billing

        Dim rech As New Billing
        rech.IsStorno = False
        rech.Customer = c.Customer
        rech.CustomerStateID = c.Customer.CustomerStateID
        rech.ProductTypeID = c.ContractRate.ProductTypeID
        rech.Contract = c
        rech.Verwendungszweck = vwz
        rech.Brutto = c.Ratenbetrag
        rech.BruttoOriginal = rech.Brutto
        rech.CostcenterID = c.CostcenterID
        rech.Rechnungsdatum = Now
        rech.Faelligkeit = faellig
        rech.AccountID = accountid
        rech.PaymentMethodID = c.PaymentMethodID
        rech.BillingStatus = BillingStates.Offen

        rech.Sachkonto = acctemplate.Sachkonto ' rate.FirstOrDefault.Sachkonto
        rech.Innenauftrag = acctemplate.Innenauftrag ' rate.FirstOrDefault.Innenauftrag
        rech.KSTextern = acctemplate.Kostenstelle ' rate.FirstOrDefault.KSTextern
        rech.Steuerkennzeichen = acctemplate.SteuerkennzeichenAR ' rate.FirstOrDefault.Steuerkennzeichen
        rech.Steuerprozent = acctemplate.Tax.TaxPercent
        rech.AccountingTemplateID = acctemplate.AccountingTemplateID

        Return rech

    End Function


    Public Function Storno(ByVal billingid As Integer, ByVal kasse As Integer, Optional ByVal reason As String = "") As Integer

        Dim cashclosingid As Integer = 0
        If kasse > 0 Then
            cashclosingid = Repositories.CashClosingRepository.GetCashClosingID(kasse)
        End If

        Using con As New in1Entities
            Dim bill = con.Billings.Find(billingid)

            ' Unterscheiden zwischen Rechnung schon bezahlt oder Rechnung noch offen
            If bill.BillingStatus = BillingStates.Offen Then
                bill.BillingStatus = BillingStates.Storniert
                bill.Buchungsdatum = Now
                bill.IsStorno = True

                ' Wenn es schon eine Storno Rechnung war
                ' Orginal wieder aktivieren Isstorno = false
                If bill.Verwendungszweck.Contains("<Storno von") Then
                    Dim id = GetStornoIDfromVWZ(bill.Verwendungszweck)
                    Dim orig = con.Billings.Find(id)
                    orig.IsStorno = False
                End If

            ElseIf bill.BillingStatus = BillingStates.Bezahlt Then
                ' Minuskopie der Rechnung erstellen
                Dim copy As Billing
                copy = con.Billings.Where(Function(o) o.BillingID = billingid).AsNoTracking().FirstOrDefault()

                copy.KasseNr = kasse
                copy.BillingStatus = BillingStates.Offen
                If cashclosingid > 0 Then
                    copy.CashClosingID = cashclosingid
                End If

                copy.Rechnungsdatum = Now
                copy.Faelligkeit = Now

                If Not Trim(reason) = "" Then
                    copy.Bemerkung = "Stornogrund: " & reason
                End If


                ' Verbindungen kappen
                copy.Brutto = copy.Brutto * -1
                copy.BruttoOriginal = copy.Brutto
                copy.CourseID = Nothing
                copy.BookingID = Nothing
                copy.PackageID = Nothing
                copy.Packagecredit = 0
                copy.DtausID = Nothing
                copy.EndToEnd = Nothing
                copy.Rueckbuchungsdatum = Nothing
                copy.Verwendungszweck = "<Storno von " & bill.BillingID & "> " & copy.Verwendungszweck
                copy.IsStorno = False
                copy.PaymentInformation = ""

                copy.Hash = Helper.HashBill.CalculateHash(copy)
                con.Billings.Add(copy)

                bill.IsStorno = True

            End If

            ' Verbindungen der Originalrechnung kappen
            'bill.CourseID = Nothing
            'bill.BookingID = Nothing
            'bill.PackageID = Nothing
            'bill.Packagecredit = 0
            bill.IsStorno = True

            bill.Hash = Helper.HashBill.CalculateHash(bill)
            con.SaveChanges(_user)

            Return bill.BillingID
        End Using

        Return Nothing

    End Function

    Public Function GetStornoIDfromVWZ(ByVal vwz As String) As Integer
        Dim pattern = "^<Storno von ([\d]*)>"
        Dim match = Text.RegularExpressions.Regex.Matches(vwz, pattern, Text.RegularExpressions.RegexOptions.IgnoreCase)
        If Not IsNothing(match) Then
            Return match(0).Groups(1).Value
        End If
        Return 0
    End Function


    Public Function PayList(ByVal list As List(Of Billing), ByVal accountid As Integer, ByVal method As Integer,
                            ByVal kassenr As Integer, ByVal paymentinfo As String) As Guid

        Dim _aktbon = Guid.NewGuid

        Dim targetstate As String = BillingStates.Offen

        Dim cashclosingid As Integer = 0
        If kassenr >= 0 Then
            cashclosingid = Repositories.CashClosingRepository.GetCashClosingID(kassenr)
        End If

        If method = 1 Then 'Barzahlung
            targetstate = BillingStates.Bezahlt
        ElseIf method = 2 Or method = 3 Then 'Wenn LS, Überweisung unbezahlt lassen
        ElseIf method = 4 Then
            targetstate = BillingStates.Bezahlt
        End If


        Using con As New in1Entities


            For Each bill In list ' OCBillings
                Dim itm = con.Billings.Find(bill.BillingID)

                If itm.BillingStatus = BillingStates.Offen Then
                    itm.Vorgang = _aktbon


                    itm.Brutto = bill.Brutto
                    'itm.BruttoOriginal = itm.Brutto

                    itm.AccountID = accountid
                    itm.PaymentMethodID = method
                    itm.PaymentInformation = paymentinfo

                    itm.CashClosingID = cashclosingid

                    ' Verbuchen
                    itm.BillingStatus = targetstate
                    If method = 2 Or method = 3 Then
                        itm.KasseNr = Nothing
                    Else

                        itm.KasseNr = kassenr
                    End If

                    If targetstate = BillingStates.Bezahlt Then
                        itm.Buchungsdatum = Now
                        'itm.CashClosingID = cashclosingid
                    End If

                    itm.CheckinID = Nothing

                    itm.Hash = Helper.HashBill.CalculateHash(itm)

                End If
            Next
            con.SaveChanges(_user)
        End Using

        Return _aktbon

    End Function

    Public Function Ruecklastschrift(ByVal billindid As Integer, ByVal Rsdate As Date, ByVal Svwz As String) As Boolean

        Using _context As New in1Entities
            ' Kopie der Originalbuchung erstellen
            Dim bill = _context.Billings.Find(billindid)
            If IsNothing(bill) Then
                Return False
            End If
            Dim billid = bill.BillingID
            Dim minus = _context.Billings.Where(Function(o) o.BillingID = billid).AsNoTracking().FirstOrDefault()
            Dim copy = _context.Billings.Where(Function(o) o.BillingID = billid).AsNoTracking().FirstOrDefault()

            'Minus Rechnung erstelle auf Bezahlt und IsStorno
            minus.BillingStatus = BillingStates.Bezahlt
            minus.Brutto = bill.Brutto * -1
            minus.BruttoOriginal = minus.Brutto

            minus.Buchungsdatum = Rsdate
            minus.Rueckbuchungsdatum = Nothing

            minus.Verwendungszweck = "<Storno von " & bill.BillingID & "> " & bill.Verwendungszweck
            minus.IsStorno = True

            minus.DtausID = Nothing
            minus.EndToEnd = bill.EndToEnd

            minus.Hash = Helper.HashBill.CalculateHash(minus)

            _context.Billings.Add(minus)

            ' Kopie der Rechnung als offene Rechnung erstellen
            copy.BillingStatus = BillingStates.Offen
            copy.Mahnstufe = bill.Mahnstufe + 1
            copy.IsStorno = False
            copy.Buchungsdatum = Nothing
            copy.PaymentInformation = Nothing
            copy.DtausID = Nothing
            copy.EndToEnd = Nothing
            copy.Rueckbuchungsdatum = Nothing

            copy.Hash = Helper.HashBill.CalculateHash(copy)
            _context.Billings.Add(copy)


            'Originalrechnung auf RS stellen
            bill.Bemerkung = "RS-Grund: " & Svwz & " / " & bill.Bemerkung
            bill.Rueckbuchungsdatum = Rsdate
            bill.IsStorno = True

            bill.Hash = Helper.HashBill.CalculateHash(bill)
            _context.SaveChanges()
        End Using
        Return True

    End Function

    Public Function Ruecklastgebuehr(ByVal bill As Billing, ByVal packageid As Integer, ByVal betrag As Double, ByVal faelligkeit As DateTime)

        ' Minus rechnung RS Gebühr erstellen
        Dim newid = PAddBillingFromPackage(bill.CustomerID, Nothing, bill.CustomerStateID, packageid,
                                      bill.AccountID, bill.PaymentMethodID, 0, faelligkeit, betrag * -1)
        ' Die neue Minusrechnung gleich verbuchen
        Using con As New in1Entities
            Dim rs = con.Billings.Find(newid)
            rs.EndToEnd = bill.EndToEnd
            rs.Buchungsdatum = faelligkeit
            rs.BillingStatus = BillingStates.Bezahlt
            rs.Hash = Helper.HashBill.CalculateHash(rs)
            con.SaveChanges()
        End Using


        'Offene Rechnung erstellen
        Return PAddBillingFromPackage(bill.CustomerID, Nothing, bill.CustomerStateID, packageid,
                                      bill.AccountID, bill.PaymentMethodID, 0, faelligkeit, betrag)

    End Function

    ''' <summary>
    ''' Aboabtragen
    ''' </summary>
    ''' <param name="billingid">BillingID</param>
    ''' <returns>Restguthaben</returns>
    Public Function Aboabtragen(ByVal billingid As Integer) As Integer
        Dim result As Integer = -1

        Using con As New in1Entities

            Dim SelectedBill = con.Billings.Find(billingid)

            If Not IsNothing(SelectedBill) And Not IsNothing(SelectedBill.PackageID) Then

                If SelectedBill.Packagecredit > 0 Then

                    Dim pl As New PackageLog
                    pl.BillingID = SelectedBill.BillingID
                    pl.Zeit = Now
                    con.PackageLogs.Add(pl)

                    If SelectedBill.Packagecredit > 0 Then
                        SelectedBill.Packagecredit = SelectedBill.Packagecredit - 1
                    Else
                        SelectedBill.Packagecredit = 0
                    End If
                    con.SaveChanges()
                    result = SelectedBill.Packagecredit
                End If
            End If
        End Using
        Return result
    End Function


End Class
