﻿Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.Linq
Imports Newtonsoft.Json

Public Class Order_Bal

    Private _ErrorMessage As String
    Public ReadOnly Property ErrorMessage As String
        Get
            Return _ErrorMessage
        End Get

    End Property

    Private _con As in1Entities
    Private _user As String

    Public Sub New(ByVal user As String, ByVal context As in1Entities)
        _user = user
        _con = context
    End Sub

    Public Function TseOrder() As Tse

    End Function

    Public Function TseBeleg() As Tse

    End Function

    Public Function GetBillings(ByVal checkin As Checkin) As ObservableCollection(Of Billing)

        Dim billings As New ObservableCollection(Of Billing)

        If checkin.CheckinID = 0 Then
            Dim query = From c In _con.Billings Where c.CustomerID = checkin.CustomerID And c.BillingStatus = BillingStates.Offen And c.PaymentMethodID = 1 Select c

            billings = New ObservableCollection(Of Billing)(query.ToList())

        Else
            Dim query = From c In _con.Billings Where c.CheckinID = checkin.CheckinID And c.BillingStatus = BillingStates.Offen And c.PaymentMethodID = 1 Select c

            billings = New ObservableCollection(Of Billing)(query.ToList())
        End If
        Return billings
    End Function

    Public Function GetAbos(ByVal checkin As Checkin) As ObservableCollection(Of Billing)
        Dim query = _con.Billings.Where(Function(o) o.CustomerID = checkin.CustomerID And o.IsStorno = False And o.Packagecredit.HasValue AndAlso o.Packagecredit > 0)
        Dim billings = New ObservableCollection(Of Billing)(query.ToList)
        Return billings
    End Function
    Public Function OrderCourse(ByVal checkin As Checkin, ByVal courseid As Integer, ByVal customerstateid As Integer, ByVal cashregister As CashRegister, ByVal brutto As Decimal) As Billing

        If courseid = 0 Then
            Throw New Exception("Buchung von Kursen nur für Kunden, nicht für Tagesgäste")

        End If


        Dim bill As New Billing


            Dim course = _con.Courses.Find(courseid)
        'Dim customer = _con.Customers.Find(customerid)
        Dim price = _con.CoursePrices.Where(Function(o) o.CourseID = courseid AndAlso o.CustomerStateID = customerstateid).Single()

        Dim acctemplate = _con.AccountingTemplates.Include(Function(o) o.Tax).Where(Function(t) t.AccountingTemplateID = price.AccountingTemplateID).FirstOrDefault()

        Dim cashclosingid As Integer = 0
        If cashregister.KasseNr > 0 Then
            cashclosingid = Repositories.CashClosingRepository.GetCashClosingID(cashregister.KasseNr)
        End If



        bill.KasseNr = cashregister.KasseNr
        bill.IsStorno = False
        bill.CanCartDel = True
        If cashclosingid > 0 Then
            bill.CashClosingID = cashclosingid
        End If

        bill.CustomerID = checkin.CustomerID
        bill.CustomerStateID = customerstateid
        bill.BillingStatus = BillingStates.Offen
        bill.Rechnungsdatum = Now
        bill.ProductTypeID = course.ProductTypeID
        bill.Faelligkeit = IIf(IsNothing(course.Faelligkeit), Now, course.Faelligkeit)
        bill.Verwendungszweck = course.Sport.Sportname & " " & course.Zusatzinfo & " " & course.Zeittext & " (Start: " & course.DTStart.ToShortDateString & ")"
        bill.Checked = True
        bill.CostcenterID = IIf(IsNothing(price.CostcenterID), course.CostcenterID, price.CostcenterID)


        If Not IsNothing(acctemplate) Then
            bill.Innenauftrag = acctemplate.Innenauftrag
            bill.Sachkonto = acctemplate.Sachkonto
            bill.KSTextern = acctemplate.Kostenstelle
            bill.Steuerkennzeichen = acctemplate.SteuerkennzeichenAR
            bill.Steuerprozent = acctemplate.Tax.TaxPercent
            bill.AccountingTemplateID = acctemplate.AccountingTemplateID
        End If

        bill.AccountID = cashregister.DefaultAccountID
        bill.PaymentMethodID = cashregister.DefaultPaymentMethodID

        bill.Packagecredit = Nothing
        bill.CourseID = course.CourseID
        'bill.ProductID = SelectedProduct.ProductID
        'bill.PackageID = package.PackageID

        bill.Brutto = brutto
        'bill.Steuerprozent = price.TaxPercent

        bill.Hash = Helper.HashBill.CalculateHash(bill)

        Return bill


    End Function

    Public Function OrderPackage(ByVal checkin As Checkin, ByVal packageid As Integer, ByVal customerstateid As Integer, ByVal cashregister As CashRegister, ByVal brutto As Decimal)



        Dim bill As New Billing
        Dim package = _con.Packages.Find(packageid)


        Dim price = _con.PackagePrices.Where(Function(o) o.PackageID = packageid AndAlso o.CustomerStateID = customerstateid).Single()

        Dim defaultcostcenter = _con.Costcenters.FirstOrDefault()
        Dim acctemplate = _con.AccountingTemplates.Include(Function(o) o.Tax).Where(Function(t) t.AccountingTemplateID = price.AccountingTemplateID).FirstOrDefault()


        bill.KasseNr = cashregister.KasseNr
        bill.IsStorno = False
        bill.CanCartDel = True

        Dim cashclosingid As Integer = 0
        If bill.KasseNr > 0 Then
            cashclosingid = Repositories.CashClosingRepository.GetCashClosingID(bill.KasseNr)
        End If

        If Not IsNothing(checkin.CustomerID) AndAlso checkin.CustomerID > 0 Then
            bill.CustomerID = checkin.CustomerID
        End If
        If Not IsNothing(checkin.CheckinID) AndAlso checkin.CheckinID > 0 Then
            bill.CheckinID = checkin.CheckinID
        End If

        bill.CustomerStateID = customerstateid
        bill.BillingStatus = BillingStates.Offen
        bill.Rechnungsdatum = Now
        bill.ProductTypeID = package.ProductTypeID
        bill.Faelligkeit = Now
        bill.Verwendungszweck = Left(package.PackageName, 250)
        bill.Checked = True

        bill.AccountID = cashregister.DefaultAccountID
        bill.PaymentMethodID = cashregister.DefaultPaymentMethodID

        bill.CostcenterID = IIf(IsNothing(price.CostcenterID), defaultcostcenter.CostcenterID, price.CostcenterID)

        If Not IsNothing(acctemplate) Then
            bill.Innenauftrag = acctemplate.Innenauftrag
            bill.Sachkonto = acctemplate.Sachkonto
            bill.KSTextern = acctemplate.Kostenstelle
            bill.Steuerkennzeichen = acctemplate.SteuerkennzeichenAR
            bill.Steuerprozent = acctemplate.Tax.TaxPercent
            bill.AccountingTemplateID = acctemplate.AccountingTemplateID
        End If


        bill.Packagecredit = package.Zutritte
        'bill.CourseID = SelectedProduct.CourseID
        'bill.ProductID = SelectedProduct.ProductID
        bill.PackageID = package.PackageID

        bill.Brutto = price.Preis
        bill.BruttoOriginal = bill.Brutto
        bill.Brutto = brutto


        bill.Hash = Helper.HashBill.CalculateHash(bill)

        Return bill



    End Function

    Public Function SofortStorno(ByVal bill As Billing) As Boolean
        bill.IsStorno = True
        bill.BillingStatus = BillingStates.Storniert
        bill.Buchungsdatum = Now
        Return True
    End Function

    ''' <summary>
    ''' Unbezahlte Rechnung stornieren, beide Rechnungen werden dabei direkt verbucht. 
    ''' </summary>
    ''' <param name="bill"></param>
    ''' <param name="reason"></param>
    ''' <returns></returns>
    Public Function StornoUnbezahlt(ByRef bill As Billing, Optional ByVal reason As String = "") As Billing
        Dim id = bill.BillingID
        Dim newbill = _con.Billings.Where(Function(c) c.BillingID = id).AsNoTracking().Single()
        bill.IsStorno = True
        bill.Buchungsdatum = Now
        bill.BillingStatus = BillingStates.Bezahlt

        ' Verknüpfung zum Checkin löschen, da sonst kein Checkout
        If Not IsNothing(bill.CheckinID) Then
            bill.CheckinID = Nothing
            newbill.CheckinID = Nothing
        End If

        newbill.StornoID = bill.BillingID
        newbill.Brutto = bill.Brutto * -1

        Dim storno As New Storno(bill.BillingID, reason)
        newbill.Comment = storno.GetJson()
        newbill.Verwendungszweck = "<Storno von " & id & "> " & newbill.Verwendungszweck
        newbill.Buchungsdatum = Now
        newbill.BillingStatus = BillingStates.Bezahlt
        Return newbill

    End Function

End Class
