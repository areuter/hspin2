﻿
Imports System.Collections.ObjectModel
Imports System.Linq

Public Class Flagsmanager



    Private Shared _Liste As ObservableCollection(Of Flag)

    Public Shared Function GetFlagList() As ObservableCollection(Of Flag)


        _Liste = New ObservableCollection(Of Flag)
        Using con As New in1Entities
            Dim sett = (From c In con.GlobalSettings Where c.Einstellung = "Flags").FirstOrDefault

            If Not IsNothing(sett) Then
                Dim itms = Split(sett.Wert, ";")
                If Not IsNothing(itms) Then
                    For Each itm In itms
                        Dim el = Split(itm, ",")
                        If UBound(el) = 1 Then
                            _Liste.Add(New Flag With {.Flagname = el(0), .Flagcolor = el(1)})
                        End If
                    Next
                End If

            End If

        End Using


        Return _Liste
    End Function

    Public Shared Function GetSelected(ByVal inp As String) As ObservableCollection(Of Flag)
        Dim lst As New ObservableCollection(Of Flag)

        If Not IsNothing(inp) Then
            Dim itms = Split(inp, ";")
            If Not IsNothing(itms) Then
                For Each itm In itms
                    Dim el = Split(itm, ",")
                    If UBound(el) = 1 Then
                        lst.Add(New Flag With {.Flagname = el(0), .Flagcolor = el(1)})
                    End If
                Next
            End If
        End If

        Return lst
    End Function

    Public Shared Function SetSelected(ByVal inp As ObservableCollection(Of Flag)) As String
        Dim out As String = String.Empty
        If Not IsNothing(inp) Then
            For Each itm In inp
                If itm.IsSelected Then out = out & itm.Flagname & "," & itm.Flagcolor & ";"
            Next
        End If
        Return out
    End Function
End Class

Public Class Flag
    Inherits EntityBase

    Public Event OnSelectedChanged As EventHandler

    Private _Flagname As String

    Public Property Flagname As String
        Get
            Return _Flagname
        End Get
        Set(value As String)
            _Flagname = value
        End Set
    End Property

    Private _Flagcolor As String

    Public Property Flagcolor As String
        Get
            Return _Flagcolor
        End Get
        Set(value As String)
            _Flagcolor = value
        End Set
    End Property

    Private _IsSelected As Boolean = False

    Public Property IsSelected As Boolean
        Get
            Return _IsSelected
        End Get
        Set(value As Boolean)
            _IsSelected = value
            NotifyPropertyChanged("IsSelected")
            OnIsSelectedChanged()
        End Set
    End Property

    Public Sub OnIsSelectedChanged()
        RaiseEvent OnSelectedChanged(Me, Nothing)
    End Sub
End Class
