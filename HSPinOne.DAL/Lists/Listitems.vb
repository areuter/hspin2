﻿
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.Linq


Public Class Listitems
    Public Shared Sub ResetLists()
        _CourseGenderList = Nothing
        _CourseAudienceList = Nothing
        _CourseCostList = Nothing
        _CourseRegistrationList = Nothing
        _CourseStateList = Nothing
        _CostcenterList = Nothing
        _CategoryList = Nothing
        _ProductTypeList = Nothing
        _PartyList = Nothing
        _SportList = Nothing
        _LocationList = Nothing
        _TermList = Nothing
        _CategoryList = Nothing
    End Sub

    Private Shared _CourseGenderList As List(Of CourseGender)
    Public Shared Property CourseGenderList As List(Of CourseGender)
        Get
            If IsNothing(_CourseGenderList) Then
                Using con As New in1Entities
                    Dim query = From c In con.CourseGenders Select c
                    _CourseGenderList = query.ToList
                    Return _CourseGenderList
                End Using
            Else
                Return _CourseGenderList
            End If
        End Get
        Set(value As List(Of CourseGender))
            _CourseGenderList = value
        End Set
    End Property

    Public Shared Function GetCourseGenderList() As List(Of CourseGender)
        Return CourseGenderList

    End Function

    Private Shared _CourseAudienceList As List(Of CourseAudience)

    Public Shared Function GetCourseAudienceList() As List(Of CourseAudience)
        If IsNothing(_CourseAudienceList) Then
            Using con As New in1Entities
                Dim query = From c In con.CourseAudiences Select c

                _CourseAudienceList = query.ToList
                Return _CourseAudienceList
            End Using
        Else
            Return _CourseAudienceList
        End If

    End Function

    Private Shared _CourseCostList As List(Of CourseCost)
    Public Shared Function GetCourseCostList() As List(Of CourseCost)
        If IsNothing(_CourseCostList) Then
            Using con As New in1Entities
                Dim query = From c In con.CourseCosts Select c

                _CourseCostList = query.ToList
                Return _CourseCostList
            End Using
        Else
            Return _CourseCostList
        End If
    End Function

    Private Shared _CourseRegistrationList As List(Of CourseRegistration)
    Public Shared Function GetCourseRegistrationList() As List(Of CourseRegistration)
        If IsNothing(_CourseRegistrationList) Then
            Using con As New in1Entities
                Dim query = From c In con.CourseRegistrations Select c

                _CourseRegistrationList = query.ToList
                Return _CourseRegistrationList
            End Using
        Else
            Return _CourseRegistrationList
        End If
    End Function

    Private Shared _CourseStateList As List(Of CourseState)
    Public Shared Function GetCourseStateList() As List(Of CourseState)
        If IsNothing(_CourseStateList) Then
            Using con As New in1Entities
                Dim query = From c In con.CourseStates Order By c.CourseStateID Select c

                _CourseStateList = query.ToList
                Return _CourseStateList
            End Using
        Else
            Return _CourseStateList
        End If
    End Function

    Private Shared _CostcenterList As List(Of Costcenter)
    Public Shared Function GetCostcenterList() As List(Of Costcenter)
        If IsNothing(_CostcenterList) Then
            Using con As New in1Entities
                Dim query = From c In con.Costcenters Order By c.Costcentername Select c

                _CostcenterList = query.ToList
                Return _CostcenterList
            End Using
        Else
            Return _CostcenterList
        End If
    End Function

    Private Shared _CategoryList As List(Of Category)
    Public Shared Function GetCategoryList() As List(Of Category)
        If IsNothing(_CategoryList) Then
            Using con As New in1Entities
                Dim query = From c In con.Categories Order By c.CategoryName Select c

                _CategoryList = query.ToList
                Return _CategoryList
            End Using
        Else
            Return _CategoryList
        End If
    End Function

    Private Shared _CustomerStateList As List(Of CustomerState)
    Public Shared Function GetCustomerStateList() As List(Of CustomerState)
        If IsNothing(_CustomerStateList) Then
            Using con As New in1Entities
                Dim query = con.CustomerStates.Where(Function(o) o.Hide = False)
                _CustomerStateList = query.ToList()
                Return _CustomerStateList
            End Using
        Else
            Return _CustomerStateList
        End If
    End Function

    Private Shared _ProductTypeList As List(Of ProductType)
    Public Shared Function GetProductTypeList() As List(Of ProductType)
        If IsNothing(_ProductTypeList) Then
            Using con As New in1Entities
                Dim query = From c In con.ProductTypes Order By c.ProductTypeName Select c

                _ProductTypeList = query.ToList
                Return _ProductTypeList
            End Using
        Else
            Return _ProductTypeList
        End If
    End Function

    Private Shared _PartyList As List(Of Party)
    Public Shared Function GetPartyList() As List(Of Party)
        If IsNothing(_PartyList) Then
            Using con As New in1Entities
                Dim query = From c In con.Parties Select c

                _PartyList = query.ToList
                Return _PartyList
            End Using
        Else
            Return _PartyList
        End If
    End Function

    Private Shared _SportList As List(Of Sport)
    Public Shared Function GetSportList() As List(Of Sport)
        If IsNothing(_SportList) Then
            Using con As New in1Entities
                Dim query = From c In con.Sports Order By c.Sportname Select c

                _SportList = query.ToList
                Return _SportList
            End Using
        Else
            Return _SportList
        End If
    End Function

    Private Shared _LocationList As List(Of Location)
    Public Shared Function GetLocationList() As List(Of Location)
        If IsNothing(_LocationList) Then
            Using con As New in1Entities
                Dim query = From c In con.Locations Order By c.LocationName Select c

                _LocationList = query.ToList
                Return _LocationList
            End Using
        Else
            Return _LocationList
        End If
    End Function

    Private Shared _TermList As List(Of Term)
    Public Shared Function GetTermList() As List(Of Term)
        If IsNothing(_TermList) Then
            Using con As New in1Entities
                Dim query = From c In con.Terms Order By c.Semesterbeginn Select c

                _TermList = query.ToList
                Return _TermList
            End Using
        Else
            Return _TermList
        End If
    End Function

    Private Shared _CustomerInstitutionList As List(Of CustomerInstitution)
    Public Shared Function GetCustomerInstitutionList() As List(Of CustomerInstitution)
        If IsNothing(_CustomerInstitutionList) Then
            Using con As New in1Entities
                Dim query = From c In con.CustomerInstitutions Where c.IsActive = True Select c

                _CustomerInstitutionList = query.ToList
                Return _CustomerInstitutionList
            End Using
        Else
            Return _CustomerInstitutionList
        End If
    End Function

    Private Shared _AccountingTemplateList As List(Of AccountingTemplate)
    Public Shared Function GetAccountingTemplateList() As List(Of AccountingTemplate)
        If IsNothing(_AccountingTemplateList) Then
            Using con As New in1Entities
                Dim query = From c In con.AccountingTemplates Select c

                _AccountingTemplateList = query.ToList
                Return _AccountingTemplateList
            End Using
        Else
            Return _AccountingTemplateList
        End If
    End Function

    Private Shared _TaxList As List(Of Tax)
    Public Shared Function GetTaxList() As List(Of Tax)
        If IsNothing(_TaxList) Then
            Using con As New in1Entities
                con.Taxes.Load()
                _TaxList = con.Taxes.ToList()
            End Using
        End If
        Return _TaxList
    End Function



    Public Shared Function GetAnmeldestatusList() As Dictionary(Of Integer, String)

        Dim tmp As New Dictionary(Of Integer, String)
        tmp.Add(0, "normal")
        tmp.Add(1, "Warteliste manuell aktiviert")
        tmp.Add(2, "Ausgebucht manuell aktiviert")
        tmp.Add(3, "Keine Warteliste bei ausgebucht")
        Return tmp


    End Function


    Public Shared Sub LoadData()
        Using con As New in1Entities
            Dim query = From c In con.CourseGenders Select c

            CourseGenderList = query.ToList


        End Using
    End Sub


End Class
