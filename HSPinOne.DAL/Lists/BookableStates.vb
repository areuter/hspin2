﻿Imports System.Collections.Generic

Public Class BookableStates
    Public States As Dictionary(Of Integer, String)

    Sub New()
        States = New Dictionary(Of Integer, String)()
        States.Add(0, "Nicht buchbar")
        States.Add(1, "Beim Service buchbar")
        States.Add(2, "Online und vom Service buchbar")
    End Sub

End Class
