﻿
Imports System
Imports System.Data.Entity
Imports System.Data.Entity.Infrastructure
Imports System.Data.Entity.ModelConfiguration.Conventions
Imports System.Configuration
Imports System.Linq
Imports System.Data.Entity.Validation
Imports System.Text
Imports System.Diagnostics

Partial Public Class in1Entities
    Inherits DbContext

    Private auditHelper As AuditLogHelper
    Private auditFactory As AuditTrailFactory

    Private AuditList As New System.Collections.Generic.List(Of AuditLog)
    Private entlist As New System.Collections.Generic.List(Of DbEntityEntry)

    Private _skipMigrations As Boolean = False

    ' Nur in diesen Tabellen wird geloggt
    Dim LogTables() As String = {"Customer", "Course", "Billing", "Booking", "Contract", "CourseStaff", "StaffContract"}

    Public Sub New()

        MyBase.New(ConnectionInformation.DecryptedConnString)

    End Sub

    Public Sub New(ByVal connstring As String)
        MyBase.New(connstring)
        _skipMigrations = True
    End Sub

    Protected Overrides Sub OnModelCreating(modelBuilder As DbModelBuilder)

        modelBuilder.Conventions.Remove(Of PluralizingTableNameConvention)()

        If _skipMigrations Then
            Data.Entity.Database.SetInitializer(Of in1Entities)(Nothing)
        Else
            Data.Entity.Database.SetInitializer(New MigrateDatabaseToLatestVersion(Of in1Entities, Migrations.Configuration))
        End If

    End Sub
    Public Overrides Function SaveChanges() As Integer
        'Return MyBase.SaveChanges()

        Try
            Return MyBase.SaveChanges()


        Catch ex As DbUpdateException
            Dim innerEx = ex.InnerException

            While Not IsNothing(innerEx.InnerException)
                innerEx = innerEx.InnerException
            End While
            Database.Log = AddressOf DebugLog


            Throw New Exception(innerEx.Message)

        Catch ex As DbEntityValidationException
            Dim sb = New StringBuilder

            For Each itm In ex.EntityValidationErrors
                For Each erritm In itm.ValidationErrors
                    sb.AppendLine(String.Format("{0}-{1}-{2}", itm.Entry.Entity, erritm.PropertyName, erritm.ErrorMessage))
                Next
            Next

            Throw New Exception(sb.ToString())


        End Try
    End Function


    Public Sub DebugLog(ByVal s As String)
        Debug.WriteLine(s)
        Throw New Exception(s)
    End Sub

    Public Overloads Function SaveChanges(userid As String) As Integer

        AuditList.Clear()
        entlist.Clear()

        auditFactory = New AuditTrailFactory(Me)

        Dim retval As Integer = 0


        '' Alle Änderungen in Liste speichern
        For Each ent In Me.ChangeTracker.Entries.Where(Function(p) p.State = EntityState.Added Or p.State = EntityState.Deleted Or p.State = EntityState.Modified)
            'For each changed record, get the audit record entries and add them

            'Me.AuditLogs.Add(el)
            Dim tb = auditFactory.GetTableName(ent)
            If Not IsNothing(tb) Then
                If LogTables.Contains(tb) Then ' Nur loggen, wenn bestimmte Tabelle
                    Dim audit = auditFactory.GetAudit(ent, userid)
                    AuditList.Add(audit)
                    entlist.Add(ent)
                End If
            End If

            ' Create Createdby
            If ent.State = EntityState.Added Then
                Dim prop0 = ent.Entity.GetType().GetProperty("CreatedBy")
                If Not IsNothing(prop0) Then
                    prop0.SetValue(ent.Entity, userid, Nothing)
                End If
            End If

            ' Update Lastmodified and LastModifiedBy
            Dim prop = ent.Entity.GetType().GetProperty("LastModified")
            If Not IsNothing(prop) Then
                prop.SetValue(ent.Entity, DateTime.Now, Nothing)
            End If
            Dim prop1 = ent.Entity.GetType().GetProperty("LastModifiedBy")
            If Not IsNothing(prop1) Then
                prop1.SetValue(ent.Entity, userid, Nothing)
            End If


        Next



        '' Datensatzänderungen speichern, da sonst keine neue ID
        Try
            retval = MyBase.SaveChanges()

        Catch ex As DbUpdateException
            Dim innerEx = ex.InnerException

            While Not IsNothing(innerEx.InnerException)
                innerEx = innerEx.InnerException
            End While

            Throw New Exception(innerEx.Message)

        Catch ex As DbEntityValidationException
            Dim sb = New StringBuilder

            For Each itm In ex.EntityValidationErrors
                For Each erritm In itm.ValidationErrors
                    sb.AppendLine(String.Format("{0}-{1}-{2}", itm.Entry.Entity, erritm.PropertyName, erritm.ErrorMessage))
                Next
            Next

            Throw New Exception(sb.ToString())

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try


        '' Log speichern
        If AuditList.Count > 0 Then
            Dim i As Integer = 0
            For Each audit In AuditList
                audit.AuditLogID = Guid.NewGuid()
                If audit.EventType = "A" Then
                    audit.RecordID = auditFactory.GetKeyValue(entlist(i))
                End If
                Me.AuditLogs.Add(audit)
                i = i + 1
            Next
            Return MyBase.SaveChanges()
        End If
        Return retval

    End Function


    Public Property AdminAccesss() As DbSet(Of AdminAccess)
    Public Property Accounts() As DbSet(Of Account)
    Public Property AccountingTemplates() As DbSet(Of AccountingTemplate)
    Public Property Activities() As DbSet(Of Activity)
    Public Property Appointments() As DbSet(Of Appointment)
    Public Property AppointmentStaffs() As DbSet(Of AppointmentStaff)
    Public Property AppointmentCustomers() As DbSet(Of AppointmentCustomers)
    Public Property AuditLogs() As DbSet(Of AuditLog)
    Public Property Billings() As DbSet(Of Billing)
    Public Property BillingReceipts() As DbSet(Of BillingReceipt)
    Public Property Bookings() As DbSet(Of Booking)
    Public Property CashClosings() As DbSet(Of CashClosing)

    Public Property CashRegisters() As DbSet(Of CashRegister)
    Public Property Categories() As DbSet(Of Category)
    Public Property Checkins() As DbSet(Of Checkin)
    Public Property CheckinLogs() As DbSet(Of CheckinLog)
    Public Property Contracts() As DbSet(Of Contract)
    Public Property ContractTimestops() As DbSet(Of ContractTimestop)
    Public Property ContractRates() As DbSet(Of ContractRate)

    Public Property ContractRatePrices() As DbSet(Of ContractRatePrice)
    Public Property Costcenters() As DbSet(Of Costcenter)
    Public Property Countries() As DbSet(Of Country)
    Public Property Courses() As DbSet(Of Course)
    Public Property CourseAudiences() As DbSet(Of CourseAudience)
    Public Property CourseCosts() As DbSet(Of CourseCost)
    Public Property CourseGenders() As DbSet(Of CourseGender)
    Public Property CourseMetas() As DbSet(Of CourseMeta)
    Public Property CoursePrices() As DbSet(Of CoursePrice)
    Public Property CourseRegistrations() As DbSet(Of CourseRegistration)
    Public Property CourseStaffs() As DbSet(Of CourseStaff)
    Public Property CourseStates() As DbSet(Of CourseState)
    Public Property CourseTags() As DbSet(Of CourseTag)
    Public Property Customers() As DbSet(Of Customer)

    Public Property CustomerMetas() As DbSet(Of CustomerMeta)
    Public Property CustomerGroups() As DbSet(Of CustomerGroup)
    'Public Property CustomerMandates() As DbSet(Of CustomerMandate)

    Public Property CustomerInstitutions() As DbSet(Of CustomerInstitution)
    Public Property CustomerStates() As DbSet(Of CustomerState)
    Public Property Documents() As DbSet(Of Document)
    Public Property Dtauss() As DbSet(Of Dtaus)
    Public Property Equipments() As DbSet(Of Equipment)
    Public Property EquipmentCategories() As DbSet(Of EquipmentCategory)


    Public Property GlobalSettings() As DbSet(Of GlobalSetting)
    Public Property Holidays() As DbSet(Of Holiday)
    Public Property Institutions() As DbSet(Of Institution)
    Public Property Languages() As DbSet(Of Language)
    Public Property Locations() As DbSet(Of Location)

    Public Property LocationBookinggroups() As DbSet(Of LocationBookinggroup)

    Public Property LocationEquipments() As DbSet(Of LocationEquipment)
    Public Property LocationHours() As DbSet(Of LocationHour)

    Public Property LocationHourCosts() As DbSet(Of LocationHourCost)
    Public Property Locationgroups() As DbSet(Of Locationgroup)
    Public Property LocationLocationgroups() As DbSet(Of LocationLocationgroup)
    Public Property MachineConfigs() As DbSet(Of MachineConfig)
    Public Property Optionss() As DbSet(Of Options)
    Public Property Packages() As DbSet(Of Package)
    Public Property PackageLogs() As DbSet(Of PackageLog)
    Public Property PackagePrices() As DbSet(Of PackagePrice)
    Public Property Parties() As DbSet(Of Party)
    Public Property PaymentMethods() As DbSet(Of PaymentMethod)
    Public Property Posts() As DbSet(Of Post)
    Public Property PriceGroups() As DbSet(Of PriceGroup)

    Public Property PriceGroupPrices() As DbSet(Of PriceGroupPrice)
    Public Property Products() As DbSet(Of Product)
    Public Property ProductTypes() As DbSet(Of ProductType)
    Public Property Roles() As DbSet(Of Role)
    Public Property Serials() As DbSet(Of Serial)
    Public Property Sports() As DbSet(Of Sport)
    Public Property SportCategories() As DbSet(Of SportCategory)
    Public Property Sqlqueries() As DbSet(Of Sqlquery)
    Public Property StaffContracts() As DbSet(Of StaffContract)
    'Public Property sysdiagrams() As DbSet(Of sysdiagram)
    Public Property Tags() As DbSet(Of Tag)
    Public Property Taxes() As DbSet(Of Tax)
    Public Property Terms() As DbSet(Of Term)
    Public Property Tses() As DbSet(Of Tse)
    Public Property Users() As DbSet(Of User)

    Public Property Vouchers() As DbSet(Of Voucher)
    Public Property Vouchercodes() As DbSet(Of Vouchercode)
    Public Property WebCarts() As DbSet(Of WebCart)
    Public Property Campuss() As DbSet(Of Campus)
    Public Property Templates() As DbSet(Of Template)
    Public Property TemplateTypes() As DbSet(Of TemplateType)
    Public Property Waitinglists() As DbSet(Of Waitinglist)
    Public Property WebSessions() As DbSet(Of WebSession)

End Class
