﻿Imports System.Data.Entity
Imports System.Collections
Imports System.Data

Imports System.Data.Entity.Infrastructure
Imports System.Linq.Enumerable
Imports System.Data.Entity.Core.Objects
Imports System.Data.Entity.Core

Public Module IaExtensions

    <System.Runtime.CompilerServices.Extension> _
    Public Function GetAddedRelationships(context As DbContext) As IEnumerable()
        Return GetRelationships(context, EntityState.Added, Function(e, i) e.CurrentValues(i))
    End Function

    <System.Runtime.CompilerServices.Extension> _
    Public Function GetDeletedRelationships(context As DbContext) As IEnumerable()
        Return GetRelationships(context, EntityState.Deleted, Function(e, i) e.OriginalValues(i))
    End Function

    <System.Runtime.CompilerServices.Extension> _
    Private Function GetRelationships(context As DbContext, relationshipState As EntityState, getValue As Func(Of ObjectStateEntry, Integer, Object)) As IEnumerable()
        context.ChangeTracker.DetectChanges()
        Dim objectContext = DirectCast(context, IObjectContextAdapter).ObjectContext
        'Return objectContext.ObjectStateManager.GetObjectStateEntries(relationshipState).Where(Function(e) e.IsRelationship)
        '.[Select](Function(e) Tuple.Create(objectContext.GetObjectByKey(DirectCast(getValue(e, 0), EntityKey)), objectContext.GetObjectByKey(DirectCast(getValue(e, 1), EntityKey))))
        Return objectContext.ObjectStateManager.GetObjectStateEntries(relationshipState) _
            .Where(Function(o) o.IsRelationship) _
            .Select(Function(u) Tuple.Create(objectContext.GetObjectByKey(CType(getValue(u, 0), EntityKey)), _
                                             objectContext.GetObjectByKey(CType(getValue(u, 1), EntityKey))))
    End Function


End Module

