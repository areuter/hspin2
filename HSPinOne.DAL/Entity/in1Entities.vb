﻿Imports System.Data
Imports System.Data.Entity
Imports System.Linq.Enumerable
Imports System.Configuration
Imports System.Data.SqlClient

Partial Public Class in1Entities
    Inherits DbContext



    Public Property HasChanges As Boolean

        Get

            'Dim reladded = Me.GetAddedRelationships.Any
            'Dim reldeleted = Me.GetDeletedRelationships.Any

            Dim tracker = Me.ChangeTracker.Entries.Any(Function(e) e.State = EntityState.Added Or e.State = EntityState.Modified Or e.State = EntityState.Deleted)

            'Return reladded Or reldeleted Or tracker
            Return tracker

        End Get
        Set(value As Boolean)

        End Set
    End Property

 

End Class
