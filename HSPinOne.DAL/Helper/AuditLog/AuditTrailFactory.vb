﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Data

Imports System.Reflection
Imports System.Data.Common

Imports System.Data.Entity.Infrastructure
Imports System.ComponentModel.DataAnnotations
Imports System.Data.Entity
Imports System.ComponentModel.DataAnnotations.Schema


Public Class AuditTrailFactory
    Private context As DbContext

    Public Sub New(context As DbContext)
        Me.context = context
    End Sub
    Public Function GetAudit(entry As DbEntityEntry, userId As String) As AuditLog
        'Change this line according to your needs
        Dim audit = New AuditLog() With {
            .UserID = userId,
            .TableName = GetTableName(entry),
            .ColumnName = "-",
            .EventDateUTC = DateTime.Now.ToUniversalTime,
            .RecordID = GetKeyValue(entry)
        }

        'entry is Added 
        If entry.State = EntityState.Added Then
            Dim newValues = New StringBuilder()
            'SetAddedProperties(entry, newValues)
            'audit.NewValue = newValues.ToString()
            audit.NewValue = "Datensatz neu angelegt"
            audit.EventType = AuditActions.A.ToString()
            'entry in deleted
        ElseIf entry.State = EntityState.Deleted Then
            Dim oldValues = New StringBuilder()
            SetDeletedProperties(entry, oldValues)
            audit.OriginalValue = oldValues.ToString()
            audit.EventType = AuditActions.D.ToString()
            'entry is modified
        ElseIf entry.State = EntityState.Modified Then
            Dim oldValues = New StringBuilder()
            Dim newValues = New StringBuilder()
            SetModifiedProperties(entry, oldValues, newValues)
            audit.OriginalValue = oldValues.ToString()
            audit.NewValue = newValues.ToString()
            audit.EventType = AuditActions.M.ToString()
        End If

        Return audit
    End Function

    Private Sub SetAddedProperties(entry As DbEntityEntry, newData As StringBuilder)
        For Each propertyName In entry.CurrentValues.PropertyNames
            Dim newVal = entry.CurrentValues(propertyName)
            If newVal IsNot Nothing Then
                newData.AppendFormat("{0}={1} || ", propertyName, newVal)
            End If
        Next
        If newData.Length > 0 Then
            newData = newData.Remove(newData.Length - 3, 3)
        End If
    End Sub

    Private Sub SetDeletedProperties(entry As DbEntityEntry, oldData As StringBuilder)
        Dim dbValues As DbPropertyValues = entry.GetDatabaseValues()
        For Each propertyName In dbValues.PropertyNames
            Dim oldVal = dbValues(propertyName)
            If oldVal IsNot Nothing Then
                oldData.AppendFormat("{0}={1} || ", propertyName, oldVal)
            End If
        Next
        If oldData.Length > 0 Then
            oldData = oldData.Remove(oldData.Length - 3, 3)
        End If
    End Sub

    Private Sub SetModifiedProperties(entry As DbEntityEntry, oldData As StringBuilder, newData As StringBuilder)
        Dim dbValues As DbPropertyValues = entry.GetDatabaseValues()
        For Each propertyName In entry.OriginalValues.PropertyNames
            Dim oldVal = dbValues(propertyName)
            Dim newVal = entry.CurrentValues(propertyName)
            If oldVal IsNot Nothing AndAlso newVal IsNot Nothing AndAlso Not Equals(oldVal, newVal) Then
                newData.AppendFormat("{0}={1} || ", propertyName, newVal)
                oldData.AppendFormat("{0}={1} || ", propertyName, oldVal)
            End If
        Next
        If oldData.Length > 0 Then
            oldData = oldData.Remove(oldData.Length - 3, 3)
        End If
        If newData.Length > 0 Then
            newData = newData.Remove(newData.Length - 3, 3)
        End If
    End Sub

    Public Function GetKeyValue(entry As DbEntityEntry) As String
        Dim objectStateEntry = DirectCast(context, IObjectContextAdapter).ObjectContext.ObjectStateManager.GetObjectStateEntry(entry.Entity)
        Dim id As String = ""
        If objectStateEntry.EntityKey.EntityKeyValues IsNot Nothing Then
            id = objectStateEntry.EntityKey.EntityKeyValues(0).Value
        Else
            Dim keyName As String = ""
            'keyName = dbEntry.Entity.GetType().GetProperties().Single(Function(o) o.GetCustomAttributes(GetType(KeyAttribute), False).Count() > 0).Name
            Dim tableName = GetTableName(entry)
            If Trim(keyName) = "" Then keyName = tableName & "ID"
            id = objectStateEntry.CurrentValues(keyName)
        End If

        Return id
    End Function

    Public Function GetTableName(dbEntry As DbEntityEntry) As String
        'Dim tableAttr As TableAttribute = TryCast(dbEntry.Entity.[GetType]().GetCustomAttributes(GetType(TableAttribute), False).SingleOrDefault(), TableAttribute)
        'Dim tableName As String = If(tableAttr IsNot Nothing, tableAttr.Name, dbEntry.Entity.[GetType]().Name)
        ' Get the Table() attribute, if one exists
        Dim tableAttr As TableAttribute = TryCast(dbEntry.Entity.[GetType]().GetCustomAttributes(GetType(TableAttribute), False).SingleOrDefault(), TableAttribute)


        ' Get table name (if it has a Table attribute, use that, otherwise get the pluralized name)
        Dim tableName As String = If(tableAttr IsNot Nothing, tableAttr.Name, dbEntry.Entity.[GetType]().Name)
        Console.WriteLine(tableName.IndexOf("_"))
        Dim index As Integer = tableName.IndexOf("_")
        If (index = -1) Then
            index = tableName.Length
        End If
        tableName = tableName.Substring(0, index)

        Return tableName
    End Function
End Class

Public Enum AuditActions
    A
    M
    D
End Enum



