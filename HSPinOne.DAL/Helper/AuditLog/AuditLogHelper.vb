﻿Imports System.Data.Entity.Infrastructure
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.ComponentModel.DataAnnotations
Imports System.Linq


Public Class AuditLogHelper

    Private context As in1Entities

    Public Sub New(icontext As in1Entities)
        context = icontext
    End Sub

    Public Function GetAuditRecordsForChange(dbEntry As DbEntityEntry, userId As String) As List(Of AuditLog)

        ' Nur in diesen Tabellen wird geloggt
        Dim LogTables() As String = {"Customer", "Course", "Billing", "Booking", "Contract", "CourseStaff", "StaffContract"}

        Dim result As New List(Of AuditLog)()

        Dim changeTime As DateTime = DateTime.UtcNow

        ' Get the Table() attribute, if one exists
        Dim tableAttr As TableAttribute = TryCast(dbEntry.Entity.[GetType]().GetCustomAttributes(GetType(TableAttribute), False).SingleOrDefault(), TableAttribute)


        ' Get table name (if it has a Table attribute, use that, otherwise get the pluralized name)
        Dim tableName As String = If(tableAttr IsNot Nothing, tableAttr.Name, dbEntry.Entity.[GetType]().Name)
        Console.WriteLine(tableName.IndexOf("_"))
        Dim index As Integer = tableName.IndexOf("_")
        If (index = -1) Then
            index = tableName.Length
        End If
        tableName = tableName.Substring(0, index)


        'Nur bestimmte Tabellen loggen
        If Not LogTables.Contains(tableName) Then Return result

        ' Get primary key value (If you have more than one key column, this will need to be adjusted)
        ' als erstes versuchen das KeyAttribute zu nehmen, ansonsten Tabellenname mit ID
        Dim keyName As String = ""
        'keyName = dbEntry.Entity.GetType().GetProperties().Single(Function(o) o.GetCustomAttributes(GetType(KeyAttribute), False).Count() > 0).Name

        If Trim(keyName) = "" Then keyName = tableName & "ID"

        If dbEntry.State = Data.Entity.EntityState.Added Then
            ' For Inserts, just add the whole record
            ' If the entity implements IDescribableEntity, use the description from Describe(), otherwise use ToString()
            ' Added
            ' Again, adjust this if you have a multi-column key
            ' Or make it nullable, whatever you want
            result.Add(New AuditLog() With { _
             .AuditLogID = Guid.NewGuid(), _
             .UserID = userId, _
             .EventDateUTC = changeTime, _
             .EventType = "A", _
             .TableName = tableName, _
             .RecordID = dbEntry.CurrentValues.GetValue(Of Object)(keyName).ToString(), _
             .ColumnName = "*ALL", _
             .NewValue = "Neu angelegt" _
            })
        ElseIf dbEntry.State = Data.Entity.EntityState.Deleted Then
            ' Same with deletes, do the whole record, and use either the description from Describe() or ToString()
            ' Deleted
            result.Add(New AuditLog() With { _
             .AuditLogID = Guid.NewGuid(), _
             .UserID = userId, _
             .EventDateUTC = changeTime, _
             .EventType = "D", _
             .TableName = tableName, _
             .RecordID = dbEntry.OriginalValues.GetValue(Of Object)(keyName).ToString(), _
             .ColumnName = "*ALL", _
             .NewValue = "Datensatz gelöscht" _
            })
        ElseIf dbEntry.State = Data.Entity.EntityState.Modified Then
            For Each propertyName As String In dbEntry.OriginalValues.PropertyNames
                ' For updates, we only want to capture the columns that actually changed
                If Not Equals(dbEntry.OriginalValues.GetValue(Of Object)(propertyName), dbEntry.CurrentValues.GetValue(Of Object)(propertyName)) Then
                    ' Modified
                    result.Add(New AuditLog() With { _
                     .AuditLogID = Guid.NewGuid(), _
                     .UserID = userId, _
                     .EventDateUTC = changeTime, _
                     .EventType = "M", _
                     .TableName = tableName, _
                     .RecordID = dbEntry.OriginalValues.GetValue(Of Object)(keyName).ToString(), _
                     .ColumnName = propertyName, _
                     .OriginalValue = If(dbEntry.OriginalValues.GetValue(Of Object)(propertyName) Is Nothing, Nothing, dbEntry.OriginalValues.GetValue(Of Object)(propertyName).ToString()), _
                     .NewValue = If(dbEntry.CurrentValues.GetValue(Of Object)(propertyName) Is Nothing, Nothing, dbEntry.CurrentValues.GetValue(Of Object)(propertyName).ToString()) _
                    })
                End If
            Next
        End If
        ' Otherwise, don't do anything, we don't care about Unchanged or Detached entities

        Return result
    End Function

    Public Function GetKeyValue(ByVal entry As DbEntityEntry)
        Dim objectStateEntry = CType(context, IObjectContextAdapter).ObjectContext.ObjectStateManager.GetObjectStateEntry(entry.Entity)
        Dim id As String = ""
        If Not objectStateEntry.EntityKey.EntityKeyValues Is Nothing Then
            id = objectStateEntry.EntityKey.EntityKeyValues(0).Value
        End If
        Return id
    End Function



End Class
