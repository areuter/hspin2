﻿

Imports System.Collections.Generic

Public Class RecurrenceList

    Public Const None As String = "None"
    Public Const Daily As String = "Daily"
    Public Const Weekly As String = "Weekly"

    Public Shared Function GetList() As List(Of RecurrencePattern)

        Dim list As New List(Of RecurrencePattern)
        list.Add(New RecurrencePattern With {.Pattern = None, .Anzeige = "Keine"})
        list.Add(New RecurrencePattern With {.Pattern = Daily, .Anzeige = "Täglich"})
        list.Add(New RecurrencePattern With {.Pattern = Weekly, .Anzeige = "Wöchentlich"})
        Return list

    End Function

    '    Recurrence.Add("None", "Keine")
    'Recurrence.Add("Daily", "Täglich")
    'Recurrence.Add("Weekly", "Wöchentlich")

End Class

Public Class RecurrencePattern
    Public Property Pattern As String
    Public Property Anzeige As String
End Class
