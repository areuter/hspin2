﻿Imports System.Data.SqlClient
Imports IniParser
Imports IniParser.Parser
Imports System.IO
Imports IniParser.Model


Public Class ConnectionInformation

    Const inifolder As String = "in1"
    Const inifile As String = "config.txt"
    Const section As String = "Database"

    Private Shared _EncryptedConnString As String
    Public Shared ReadOnly Property EncrytedConnString As String
        Get
            If _EncryptedConnString Is Nothing Then
                _EncryptedConnString = LoadConnString()
            End If
            Return _EncryptedConnString
        End Get
    End Property



    Public Sub SetConnectionString(ByVal server As String, ByVal user As String, ByVal pass As String, ByVal dbname As String, ByVal integratedsecurity As Boolean)

        Dim crypt As New CryptConnString

        My.Settings.DBServer = crypt.encryptString(server)
        My.Settings.DBName = crypt.encryptString(dbname)
        My.Settings.DBUser = crypt.encryptString(user)
        My.Settings.DBPass = crypt.encryptString(pass)
        My.Settings.DBIS = integratedsecurity



        'Connstring generieren
        Dim sqlConnBuilder As New SqlConnectionStringBuilder()
        sqlConnBuilder.DataSource = server
        sqlConnBuilder.InitialCatalog = dbname
        sqlConnBuilder.IntegratedSecurity = integratedsecurity
        sqlConnBuilder.MultipleActiveResultSets = True

        If Not My.Settings.DBIS Then
            sqlConnBuilder.UserID = user
            sqlConnBuilder.Password = pass
        End If

        My.Settings.ConnString = crypt.encryptString(sqlConnBuilder.ToString)
        My.Settings.Save()

        WriteIniFile(My.Settings.ConnString)
        _EncryptedConnString = My.Settings.ConnString

    End Sub

    Private Shared Sub WriteIniFile(ByVal connstr As String)
        Dim appdata = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
        Dim folder = Path.Combine(appdata, inifolder)
        Dim fn = Path.Combine(folder, inifile)

        If Not Directory.Exists(folder) Then
            Directory.CreateDirectory(folder)
        End If

        If File.Exists(fn) Then
            File.Delete(fn)
        End If


        Dim parser As New FileIniDataParser
        Dim data = New IniData
        data.Sections.AddSection(section)
        data(section).AddKey("connstr", connstr)
        parser.WriteFile(fn, data)
        'parser.SaveFile(fn, data)
    End Sub

    Public Shared Function LoadConnString() As String

        'Liegt in Appdata/Roaming eine Ini Datei?
        Dim appdata = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
        Dim folder = Path.Combine(appdata, inifolder)
        Dim fn = Path.Combine(folder, inifile)
        If File.Exists(fn) Then
            Dim parser = New FileIniDataParser
            Dim data = parser.ReadFile(fn)
            Dim connstr = data(section)("connstr")
            If Not String.IsNullOrEmpty(connstr) Then
                Return connstr
            End If

        Else ' Keine Roaming Appdata Datei, versuche aus MySettings

            Dim connection As String = My.Settings.ConnString
            If Not String.IsNullOrEmpty(connection) Then
                ' Kopie in Roaming Data
                WriteIniFile(connection)
                Return connection
            End If
        End If

        Return Nothing


    End Function

    Public Shared Function DecryptedConnString() As String

#If DEBUG Then

        'If Net.Dns.GetHostName.StartsWith("UG-UZAH-C129") Then
        Return "Password=zhs13;Persist Security Info=True;User ID=in1goe;MultipleActiveResultSets=true;Initial Catalog=in1goetest;Data Source=134.76.156.25,1433"
        ' End If

#End If
        'Return "Password=test;Persist Security Info=True;User ID=dbeckmann;MultipleActiveResultSets=true;Initial Catalog=in1goedev;Data Source=db1.sport.uni-goettingen.de,1433"
        Dim encrypted = EncrytedConnString
        If Not IsNothing(encrypted) Then
            Dim crypt As New CryptConnString
            Dim connection As String = crypt.decryptString(encrypted)
            Return connection
        End If
        Return Nothing

    End Function

    Public Shared Function GetDatabase() As String
        Dim constr = New SqlConnectionStringBuilder(DecryptedConnString)
        Return constr.InitialCatalog

    End Function

    Public Function CheckConnection(ByVal connstr As String) As Boolean



        Try
            Using conn As SqlConnection = New SqlConnection(connstr)
                conn.Open()
                conn.Close()
            End Using
        Catch ex As Exception
            Return False
        End Try
        Return True

    End Function


    Public Shared Sub SetDemo(ByVal demo As Boolean)

        My.Settings.Demo = demo
        My.Settings.Save()

    End Sub

    Public Shared Function GetDemo()

        Return My.Settings.Demo

    End Function
End Class
