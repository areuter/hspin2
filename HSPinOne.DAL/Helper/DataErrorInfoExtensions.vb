﻿Imports System.ComponentModel

''' <summary>
''' Extends the <see cref="IDataErrorInfo"/> interface with new Validation methods.
''' </summary>
Public Module DataErrorInfoExtensions


    ''' <summary>
    ''' Validates the specified object.
    ''' </summary>
    ''' <param name="instance">The object to validate.</param>
    ''' <returns>An error message indicating what is wrong with this object. The default is an empty string ("").</returns>
    ''' <exception cref="ArgumentNullException">The argument instance must not be null.</exception>
    <System.Runtime.CompilerServices.Extension()> _
    Public Function Validate(instance As IDataErrorInfo) As String
        If instance Is Nothing Then
            Throw New ArgumentNullException("instance")
        End If

        Return If(instance.[Error], "")
    End Function

    ''' <summary>
    ''' Validates the specified member of the object.
    ''' </summary>
    ''' <param name="instance">The object to validate.</param>
    ''' <param name="memberName">The name of the member to validate.</param>
    ''' <returns>The error message for the member. The default is an empty string ("").</returns>
    ''' <exception cref="ArgumentNullException">The argument instance must not be null.</exception>
    <System.Runtime.CompilerServices.Extension()> _
    Public Function Validate(instance As IDataErrorInfo, memberName As String) As String
        If instance Is Nothing Then
            Throw New ArgumentNullException("instance")
        End If

        Return If(instance(memberName), "")
    End Function

End Module

