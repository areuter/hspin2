﻿Imports System.Collections.Generic

Public Class BillingStates

    Public Const Bezahlt As String = "B"
    Public Const Offen As String = "O"
    Public Const Storniert As String = "S"
    Public Const Rueckbelastet As String = "R"
    Public Const Pending As String = "P"

    Public Shared Function GetName(ByVal sState As String)
        Select Case sState
            Case "B"
                Return "Bezahlt"
            Case "O"
                Return "Offen"
            Case "S"
                Return "Storniert"
            Case "R"
                Return "Rückbelastet"
            Case "P"
                Return "Pending"
            Case Else
                Return "NN"
        End Select
    End Function


End Class


Public Class Billingstatuslist

    Private _liste As List(Of BillingStatusItem)
    Private _items As Dictionary(Of String, String)


    Public Property Liste As List(Of BillingStatusItem)
        Get
            Return _liste
        End Get
        Set(value As List(Of BillingStatusItem))
            _liste = value
        End Set
    End Property

    Public Property Items As Dictionary(Of String, String)
        Get
            Return _items
        End Get
        Set(value As Dictionary(Of String, String))
            _items = value
        End Set
    End Property

    Public Sub New()
        Liste = New List(Of BillingStatusItem)
        Dim s As New BillingStatusItem With {
            .StatusID = "O",
            .Statusname = "Offen"
        }
        Liste.Add(s)

        s = New BillingStatusItem With {
            .StatusID = "B",
            .Statusname = "Bezahlt"
        }
        Liste.Add(s)

        s = New BillingStatusItem With {
            .StatusID = "S",
            .Statusname = "Storniert"
        }
        Liste.Add(s)

        s = New BillingStatusItem With {
            .StatusID = "R",
            .Statusname = "Rückbelastet"
        }
        Liste.Add(s)

        s = New BillingStatusItem With {
            .StatusID = "P",
            .Statusname = "Pending"
        }
        Liste.Add(s)

    End Sub
End Class

Public Class BillingStatusItem
    Private _statusid As String

    Public Property StatusID As String
        Get
            Return _statusid
        End Get
        Set(ByVal value As String)
            _statusid = value
        End Set
    End Property

    Private _statusname As String

    Public Property Statusname As String
        Get
            Return _statusname
        End Get
        Set(ByVal value As String)
            _statusname = value
        End Set
    End Property
End Class
