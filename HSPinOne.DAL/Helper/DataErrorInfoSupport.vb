﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.Globalization
Imports System.Collections.Generic
Imports System.Linq

Public Class DataErrorInfoSupport
    Implements IDataErrorInfo

    Private ReadOnly instance As Object


    Public ReadOnly Property [Error] As String Implements System.ComponentModel.IDataErrorInfo.Error
        Get
            Return Me("")
        End Get
    End Property

    Default Public ReadOnly Property Item(columnName As String) As String Implements System.ComponentModel.IDataErrorInfo.Item
        Get
            Dim validationResults As New List(Of ValidationResult)
            If String.IsNullOrEmpty(columnName) Then
                Validator.TryValidateObject(instance, New ValidationContext(instance, Nothing, Nothing), validationResults, True)
            Else
                Dim prop As PropertyDescriptor = TypeDescriptor.GetProperties(instance)(columnName)
                If IsNothing(prop) Then
                    'Throw New ArgumentException(String.Format(CultureInfo.CurrentCulture, _
                    '                                          "The specified member {0} was not found on the instance {1}", columnName, instance.GetType()))
                Else
                    Validator.TryValidateProperty(prop.GetValue(instance), _
                                                  New ValidationContext(instance, Nothing, Nothing) With {.MemberName = columnName}, validationResults)
                End If
            End If
            Return String.Join(Environment.NewLine, validationResults.Select(Function(x) x.ErrorMessage))
        End Get
    End Property

    Public Sub New(ByVal instance As Object)

        If IsNothing(instance) Then
            Throw New ArgumentNullException("Instance")
        End If
        Me.instance = instance
    End Sub
End Class
