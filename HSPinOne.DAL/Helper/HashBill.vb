﻿
Imports System.Security.Cryptography
Imports System.Text

Namespace Helper


    Public Class HashBill
        Const HASHKEY = "EuropaGanymedIoKallisto"

        Public Shared Function CalculateHash(ByVal bill As Billing) As String
            Try

                Dim sha256 As SHA256 = SHA256.Create()
                Dim inputBytes As Byte() = Encoding.ASCII.GetBytes(HASHKEY & ":" & bill.Created & "-" & bill.Brutto.ToString() & "-" & bill.Verwendungszweck)
                Dim outputBytes As Byte() = sha256.ComputeHash(inputBytes)
                Return Convert.ToBase64String(outputBytes)
            Catch ex As Exception
                Return "error"
            End Try
        End Function

        Public Shared Function IsValid(ByVal bill As Billing, ByVal inputb64 As String) As Boolean
            Dim sha256 As SHA256 = SHA256.Create()
            Dim inputBytes As Byte() = Encoding.ASCII.GetBytes(HASHKEY & ":" & bill.Created & "-" & bill.Brutto.ToString() & "-" & bill.Verwendungszweck)
            Dim outputBytes As Byte() = sha256.ComputeHash(inputBytes)
            Dim base64Hash = Convert.ToBase64String(outputBytes)

            Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

            If comparer.Compare(base64Hash, inputb64) = 0 Then
                Return True
            Else
                Return False
            End If
        End Function

    End Class
End Namespace