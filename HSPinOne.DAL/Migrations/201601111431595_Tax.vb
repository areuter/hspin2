Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Tax
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Tax",
                Function(c) New With
                    {
                        .TaxID = c.Int(nullable := False, identity := True),
                        .TaxName = c.String(),
                        .TaxPercent = c.Decimal(nullable := False, precision := 18, scale := 2)
                    }) _
                .PrimaryKey(Function(t) t.TaxID)
            
            AddColumn("dbo.CoursePrice", "TaxPercent", Function(c) c.Decimal(nullable:=False, precision:=18, scale:=2, defaultValue:=0))
            AddColumn("dbo.PackagePrice", "TaxPercent", Function(c) c.Decimal(nullable:=False, precision:=18, scale:=2, defaultValue:=0))
            AddColumn("dbo.LocationCost", "TaxPercent", Function(c) c.Decimal(nullable:=False, precision:=18, scale:=2, defaultValue:=0))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.LocationCost", "TaxPercent")
            DropColumn("dbo.PackagePrice", "TaxPercent")
            DropColumn("dbo.CoursePrice", "TaxPercent")
            DropTable("dbo.Tax")
        End Sub
    End Class
End Namespace
