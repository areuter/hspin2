Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class CourseTermine
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Course", "DTStart", Function(c) c.DateTime(nullable := False))
            AddColumn("dbo.Course", "DTEnd", Function(c) c.DateTime(nullable := False))
            AddColumn("dbo.Course", "RRule", Function(c) c.String(maxLength := 255))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Course", "RRule")
            DropColumn("dbo.Course", "DTEnd")
            DropColumn("dbo.Course", "DTStart")
        End Sub
    End Class
End Namespace
