Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class User_customerState_WebSession
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.WebSession",
                Function(c) New With
                    {
                        .WebSessionID = c.String(nullable := False, maxLength := 128),
                        .SessionID = c.Int(nullable := False),
                        .SessionData = c.String(),
                        .Expires = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.WebSessionID)
            
            AddColumn("dbo.CustomerState", "TemplateCustomerID", Function(c) c.Int())
            AddColumn("dbo.User", "LastAliveCheck", Function(c) c.DateTime())
            AddColumn("dbo.User", "IsActive", Function(c) c.Boolean(nullable:=False, defaultValue:=True))
            AddColumn("dbo.User", "Abwesenheitsnotiz", Function(c) c.String(maxLength := 500))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.User", "Abwesenheitsnotiz")
            DropColumn("dbo.User", "IsActive")
            DropColumn("dbo.User", "LastAliveCheck")
            DropColumn("dbo.CustomerState", "TemplateCustomerID")
            DropTable("dbo.WebSession")
        End Sub
    End Class
End Namespace
