Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class BillingMahnstufe
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Billing", "Mahnstufe", Function(c) c.Int(nullable:=False, defaultValue:=0))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Billing", "Mahnstufe")
        End Sub
    End Class
End Namespace
