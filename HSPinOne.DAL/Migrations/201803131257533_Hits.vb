Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Hits
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Course", "Hits", Function(c) c.Int(nullable:=False, defaultValue:=0))
            AddColumn("dbo.Tag", "Hits", Function(c) c.Int(nullable:=False, defaultValue:=0))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Tag", "Hits")
            DropColumn("dbo.Course", "Hits")
        End Sub
    End Class
End Namespace
