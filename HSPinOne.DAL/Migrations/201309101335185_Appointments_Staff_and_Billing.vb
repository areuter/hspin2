Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class Appointments_Staff_and_Billing
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.AppointmentStaff",
                Function(c) New With
                    {
                        .AppointmentStaffID = c.Int(nullable := False, identity := True),
                        .AppointmentID = c.Int(nullable := False),
                        .CustomerID = c.Int(nullable := False),
                        .StdLohn = c.Decimal(nullable := False, precision := 18, scale := 2)
                    }) _
                .PrimaryKey(Function(t) t.AppointmentStaffID) _
                .ForeignKey("dbo.Appointment", Function(t) t.AppointmentID, cascadeDelete := True) _
                .ForeignKey("dbo.Customer", Function(t) t.CustomerID, cascadeDelete := True) _
                .Index(Function(t) t.AppointmentID) _
                .Index(Function(t) t.CustomerID)
            
            AddColumn("dbo.Billing", "AppointmentID", Function(c) c.Int())
            AddForeignKey("dbo.Billing", "AppointmentID", "dbo.Appointment", "AppointmentID")
            CreateIndex("dbo.Billing", "AppointmentID")
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.AppointmentStaff", New String() { "CustomerID" })
            DropIndex("dbo.AppointmentStaff", New String() { "AppointmentID" })
            DropIndex("dbo.Billing", New String() { "AppointmentID" })
            DropForeignKey("dbo.AppointmentStaff", "CustomerID", "dbo.Customer")
            DropForeignKey("dbo.AppointmentStaff", "AppointmentID", "dbo.Appointment")
            DropForeignKey("dbo.Billing", "AppointmentID", "dbo.Appointment")
            DropColumn("dbo.Billing", "AppointmentID")
            DropTable("dbo.AppointmentStaff")
        End Sub
    End Class
End Namespace
