Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class Billing_and_Package
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Billing", "ProductTypeID", Function(c) c.Int(nullable:=False, defaultValue:=1))
            AddColumn("dbo.Package", "CategoryID", Function(c) c.Int(nullable:=False, defaultValue:=1))
            AddForeignKey("dbo.Billing", "ProductTypeID", "dbo.ProductType", "ProductTypeID", cascadeDelete := True)
            AddForeignKey("dbo.Package", "CategoryID", "dbo.Category", "CategoryID", cascadeDelete := True)
            CreateIndex("dbo.Billing", "ProductTypeID")
            CreateIndex("dbo.Package", "CategoryID")
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.Package", New String() { "CategoryID" })
            DropIndex("dbo.Billing", New String() { "ProductTypeID" })
            DropForeignKey("dbo.Package", "CategoryID", "dbo.Category")
            DropForeignKey("dbo.Billing", "ProductTypeID", "dbo.ProductType")
            DropColumn("dbo.Package", "CategoryID")
            DropColumn("dbo.Billing", "ProductTypeID")
        End Sub
    End Class
End Namespace
