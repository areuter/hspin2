Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class LocationHourCosts
        Inherits DbMigration
    
        Public Overrides Sub Up()
            DropForeignKey("dbo.LocationCost", "CustomerState_CustomerStateID", "dbo.CustomerState")
            DropForeignKey("dbo.LocationCost", "Location_LocationID", "dbo.Location")
            DropIndex("dbo.LocationCost", New String() { "CustomerState_CustomerStateID" })
            DropIndex("dbo.LocationCost", New String() { "Location_LocationID" })
            CreateTable(
                "dbo.LocationHourCost",
                Function(c) New With
                    {
                        .LocationHourCostID = c.Int(nullable := False, identity := True),
                        .Price = c.Decimal(nullable := False, precision := 18, scale := 2),
                        .TaxPercent = c.Decimal(nullable := False, precision := 18, scale := 2),
                        .IsActive = c.Boolean(nullable := False),
                        .Vorausbuchungstage = c.Int(nullable := False),
                        .CustomerState_CustomerStateID = c.Int(),
                        .LocationHour_LocationHourID = c.Int()
                    }) _
                .PrimaryKey(Function(t) t.LocationHourCostID) _
                .ForeignKey("dbo.CustomerState", Function(t) t.CustomerState_CustomerStateID) _
                .ForeignKey("dbo.LocationHour", Function(t) t.LocationHour_LocationHourID) _
                .Index(Function(t) t.CustomerState_CustomerStateID) _
                .Index(Function(t) t.LocationHour_LocationHourID)
            
            DropTable("dbo.LocationCost")
        End Sub
        
        Public Overrides Sub Down()
            CreateTable(
                "dbo.LocationCost",
                Function(c) New With
                    {
                        .LocationCostID = c.Int(nullable := False, identity := True),
                        .Preis = c.Decimal(nullable := False, precision := 18, scale := 2),
                        .TaxPercent = c.Decimal(nullable := False, precision := 18, scale := 2),
                        .IsActive = c.Boolean(nullable := False),
                        .Vorausbuchungstage = c.Int(nullable := False),
                        .CustomerState_CustomerStateID = c.Int(),
                        .Location_LocationID = c.Int()
                    }) _
                .PrimaryKey(Function(t) t.LocationCostID)
            
            DropForeignKey("dbo.LocationHourCost", "LocationHour_LocationHourID", "dbo.LocationHour")
            DropForeignKey("dbo.LocationHourCost", "CustomerState_CustomerStateID", "dbo.CustomerState")
            DropIndex("dbo.LocationHourCost", New String() { "LocationHour_LocationHourID" })
            DropIndex("dbo.LocationHourCost", New String() { "CustomerState_CustomerStateID" })
            DropTable("dbo.LocationHourCost")
            CreateIndex("dbo.LocationCost", "Location_LocationID")
            CreateIndex("dbo.LocationCost", "CustomerState_CustomerStateID")
            AddForeignKey("dbo.LocationCost", "Location_LocationID", "dbo.Location", "LocationID")
            AddForeignKey("dbo.LocationCost", "CustomerState_CustomerStateID", "dbo.CustomerState", "CustomerStateID")
        End Sub
    End Class
End Namespace
