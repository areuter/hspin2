Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class Term_WWWStatus
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Term", "WWWStatus", Function(c) c.Int(nullable:=False, defaultValue:=0))
            Sql("UPDATE dbo.Term SET WWWStatus = 2 WHERE AktivWWW = 1")
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Term", "WWWStatus")
        End Sub
    End Class
End Namespace
