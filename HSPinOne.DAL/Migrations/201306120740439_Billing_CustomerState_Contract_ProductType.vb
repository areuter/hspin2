Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class Billing_CustomerState_Contract_ProductType
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Billing", "CustomerStateID", Function(c) c.Int(nullable:=False, defaultValue:=1))
            AddColumn("dbo.ContractRate", "ProductTypeID", Function(c) c.Int(nullable:=False, defaultValue:=3))
            AddForeignKey("dbo.Billing", "CustomerStateID", "dbo.CustomerState", "CustomerStateID", cascadeDelete := True)
            AddForeignKey("dbo.ContractRate", "ProductTypeID", "dbo.ProductType", "ProductTypeID", cascadeDelete := True)
            CreateIndex("dbo.Billing", "CustomerStateID")
            CreateIndex("dbo.ContractRate", "ProductTypeID")
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.ContractRate", New String() { "ProductTypeID" })
            DropIndex("dbo.Billing", New String() { "CustomerStateID" })
            DropForeignKey("dbo.ContractRate", "ProductTypeID", "dbo.ProductType")
            DropForeignKey("dbo.Billing", "CustomerStateID", "dbo.CustomerState")
            DropColumn("dbo.ContractRate", "ProductTypeID")
            DropColumn("dbo.Billing", "CustomerStateID")
        End Sub
    End Class
End Namespace
