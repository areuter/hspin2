Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class SportCategoryActive
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.SportCategory", "IsActive", Function(c) c.Boolean(nullable:=False, defaultValue:=True))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.SportCategory", "IsActive")
        End Sub
    End Class
End Namespace
