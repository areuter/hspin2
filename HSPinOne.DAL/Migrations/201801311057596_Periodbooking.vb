Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Periodbooking
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.LocationBookinggroup", "Periodbookingfrom", Function(c) c.DateTime(nullable := False))
            AddColumn("dbo.LocationBookinggroup", "Periodbookingtill", Function(c) c.DateTime(nullable := False))
            AddColumn("dbo.LocationBookinggroup", "Periodbookingcount", Function(c) c.Int(nullable := False))
            AddColumn("dbo.LocationBookinggroup", "Periodbookingdiscount", Function(c) c.Decimal(nullable := False, precision := 18, scale := 2))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.LocationBookinggroup", "Periodbookingdiscount")
            DropColumn("dbo.LocationBookinggroup", "Periodbookingcount")
            DropColumn("dbo.LocationBookinggroup", "Periodbookingtill")
            DropColumn("dbo.LocationBookinggroup", "Periodbookingfrom")
        End Sub
    End Class
End Namespace
