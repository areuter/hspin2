Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class CheckinLocation
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Checkin", "CheckinLocation", Function(c) c.Int(nullable:=False, defaultValue:=0))
            AddColumn("dbo.CheckinLog", "CheckinLocation", Function(c) c.Int(nullable:=False, defaultValue:=0))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.CheckinLog", "CheckinLocation")
            DropColumn("dbo.Checkin", "CheckinLocation")
        End Sub
    End Class
End Namespace
