Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class CourseMeta
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.CourseMeta",
                Function(c) New With
                    {
                        .CourseMetaID = c.Int(nullable := False, identity := True),
                        .CourseID = c.Int(nullable := False),
                        .MetaKey = c.String(maxLength := 255),
                        .MetaValue = c.String()
                    }) _
                .PrimaryKey(Function(t) t.CourseMetaID) _
                .ForeignKey("dbo.Course", Function(t) t.CourseID, cascadeDelete := True) _
                .Index(Function(t) t.CourseID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.CourseMeta", "CourseID", "dbo.Course")
            DropIndex("dbo.CourseMeta", New String() { "CourseID" })
            DropTable("dbo.CourseMeta")
        End Sub
    End Class
End Namespace
