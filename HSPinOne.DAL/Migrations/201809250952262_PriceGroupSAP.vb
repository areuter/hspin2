Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class PriceGroupSAP
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.PriceGroupPrice", "CostcenterID", Function(c) c.Int())
            AddColumn("dbo.PriceGroupPrice", "Innenauftrag", Function(c) c.String())
            AddColumn("dbo.PriceGroupPrice", "Sachkonto", Function(c) c.String())
            CreateIndex("dbo.PriceGroupPrice", "CostcenterID")
            AddForeignKey("dbo.PriceGroupPrice", "CostcenterID", "dbo.Costcenter", "CostcenterID")
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.PriceGroupPrice", "CostcenterID", "dbo.Costcenter")
            DropIndex("dbo.PriceGroupPrice", New String() { "CostcenterID" })
            DropColumn("dbo.PriceGroupPrice", "Sachkonto")
            DropColumn("dbo.PriceGroupPrice", "Innenauftrag")
            DropColumn("dbo.PriceGroupPrice", "CostcenterID")
        End Sub
    End Class
End Namespace
