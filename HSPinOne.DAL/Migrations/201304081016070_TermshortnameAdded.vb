Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class TermshortnameAdded
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Term", "Termshortname", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Term", "Termshortname")
        End Sub
    End Class
End Namespace
