Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class CourseRegistrationShop
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.CourseRegistration", "Shop", Function(c) c.Int(nullable:=False, defaultValue:=2))
            Sql("UPDATE dbo.CourseRegistration SET Shop =  0 WHERE CourseRegistrationID = 1")
            Sql("UPDATE dbo.CourseRegistration SET Shop =  0 WHERE CourseRegistrationID = 3")
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.CourseRegistration", "Shop")
        End Sub
    End Class
End Namespace
