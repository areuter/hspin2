Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class CourseAdd_WaitinglistAndStornoTemplate
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Course", "TemplateWaitinglistID", Function(c) c.Int())
            AddColumn("dbo.Course", "TemplateStornoID", Function(c) c.Int())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Course", "TemplateStornoID")
            DropColumn("dbo.Course", "TemplateWaitinglistID")
        End Sub
    End Class
End Namespace
