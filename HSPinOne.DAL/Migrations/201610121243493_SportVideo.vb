Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class SportVideo
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Sport", "VideoUrl", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Sport", "VideoUrl")
        End Sub
    End Class
End Namespace
