Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class TemplateCourse
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.TemplateType",
                Function(c) New With
                    {
                        .TemplatetypeID = c.Int(nullable := False, identity := True),
                        .Templatetypename = c.String()
                    }) _
                .PrimaryKey(Function(t) t.TemplatetypeID)
            
            AddColumn("dbo.Course", "TemplateID", Function(c) c.Int())
            AddColumn("dbo.Template", "TemplatetypeID", Function(c) c.Int())
            AddColumn("dbo.Template", "Isdefault", Function(c) c.Boolean(nullable := False))
            AddForeignKey("dbo.Course", "TemplateID", "dbo.Template", "TemplateID")
            AddForeignKey("dbo.Template", "TemplatetypeID", "dbo.TemplateType", "TemplatetypeID")
            CreateIndex("dbo.Course", "TemplateID")
            CreateIndex("dbo.Template", "TemplatetypeID")
            Sql("INSERT INTO dbo.TemplateType(Templatetypename) VALUES ('Buchungsbestätigung')")
            Sql("INSERT INTO dbo.TemplateType(Templatetypename) VALUES ('Warteliste')")
            Sql("INSERT INTO dbo.TemplateType(Templatetypename) VALUES ('Passwort vergessen')")
            Sql("INSERT INTO dbo.TemplateType(Templatetypename) VALUES ('Storno')")
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.Template", New String() { "TemplatetypeID" })
            DropIndex("dbo.Course", New String() { "TemplateID" })
            DropForeignKey("dbo.Template", "TemplatetypeID", "dbo.TemplateType")
            DropForeignKey("dbo.Course", "TemplateID", "dbo.Template")
            DropColumn("dbo.Template", "Isdefault")
            DropColumn("dbo.Template", "TemplatetypeID")
            DropColumn("dbo.Course", "TemplateID")
            DropTable("dbo.TemplateType")
        End Sub
    End Class
End Namespace
