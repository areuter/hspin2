Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class Sport_Obmann
        Inherits DbMigration
    
        Public Overrides Sub Up()
            DropForeignKey("dbo.Sport", "CustomerID", "dbo.Customer")
            DropIndex("dbo.Sport", New String() { "CustomerID" })
            AddColumn("dbo.Sport", "Ansprechpartner_CustomerID", Function(c) c.Int())
            AddColumn("dbo.Sport", "Obmann_CustomerID", Function(c) c.Int())
            AddForeignKey("dbo.Sport", "Obmann_CustomerID", "dbo.Customer", "CustomerID")
            AddForeignKey("dbo.Sport", "Ansprechpartner_CustomerID", "dbo.Customer", "CustomerID")
            CreateIndex("dbo.Sport", "Obmann_CustomerID")
            CreateIndex("dbo.Sport", "Ansprechpartner_CustomerID")
            Sql("UPDATE dbo.Sport SET Ansprechpartner_CustomerID = CustomerID")
            DropColumn("dbo.Sport", "CustomerID")
        End Sub
        
        Public Overrides Sub Down()
            AddColumn("dbo.Sport", "CustomerID", Function(c) c.Int())
            DropIndex("dbo.Sport", New String() { "Ansprechpartner_CustomerID" })
            DropIndex("dbo.Sport", New String() { "Obmann_CustomerID" })
            DropForeignKey("dbo.Sport", "Ansprechpartner_CustomerID", "dbo.Customer")
            DropForeignKey("dbo.Sport", "Obmann_CustomerID", "dbo.Customer")
            DropColumn("dbo.Sport", "Obmann_CustomerID")
            DropColumn("dbo.Sport", "Ansprechpartner_CustomerID")
            CreateIndex("dbo.Sport", "CustomerID")
            AddForeignKey("dbo.Sport", "CustomerID", "dbo.Customer", "CustomerID")
        End Sub
    End Class
End Namespace
