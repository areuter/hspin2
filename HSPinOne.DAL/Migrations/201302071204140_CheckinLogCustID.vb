Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class CheckinLogCustID
        Inherits DbMigration
    
        Public Overrides Sub Up()
            DropForeignKey("dbo.CheckinLog", "CustomerID", "dbo.Customer")
            DropIndex("dbo.CheckinLog", New String() { "CustomerID" })
            AlterColumn("dbo.CheckinLog", "CustomerID", Function(c) c.Int())
            AddForeignKey("dbo.CheckinLog", "CustomerID", "dbo.Customer", "CustomerID")
            CreateIndex("dbo.CheckinLog", "CustomerID")
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.CheckinLog", New String() { "CustomerID" })
            DropForeignKey("dbo.CheckinLog", "CustomerID", "dbo.Customer")
            AlterColumn("dbo.CheckinLog", "CustomerID", Function(c) c.Int(nullable := False))
            CreateIndex("dbo.CheckinLog", "CustomerID")
            AddForeignKey("dbo.CheckinLog", "CustomerID", "dbo.Customer", "CustomerID", cascadeDelete := True)
        End Sub
    End Class
End Namespace
