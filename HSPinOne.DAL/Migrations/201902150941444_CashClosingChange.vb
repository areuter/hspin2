Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class CashClosingChange
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.CashClosing", "KasseNr", Function(c) c.Int(nullable := False))
            AddColumn("dbo.CashClosing", "ClosedBy", Function(c) c.String())
            AddColumn("dbo.CashClosing", "SummeBar", Function(c) c.Double(nullable := False))
            AddColumn("dbo.CashClosing", "SummeKarte", Function(c) c.Double(nullable := False))
            AddColumn("dbo.CashClosing", "ECBon", Function(c) c.String())
            AddColumn("dbo.CashClosing", "Start", Function(c) c.DateTime(nullable := False))
            AddColumn("dbo.CashClosing", "Ende", Function(c) c.DateTime())
            DropColumn("dbo.CashClosing", "AccountID")
            DropColumn("dbo.CashClosing", "StartDatum")
            DropColumn("dbo.CashClosing", "EndDatum")
            DropColumn("dbo.CashClosing", "Terminal")
            DropColumn("dbo.CashClosing", "Anfangsbestand")
            DropColumn("dbo.CashClosing", "Bruttoumsatz")
            DropColumn("dbo.CashClosing", "Barzahlungen")
            DropColumn("dbo.CashClosing", "Kartenzahlungen")
            DropColumn("dbo.CashClosing", "Stornierungen")
            DropColumn("dbo.CashClosing", "Ausgaben")
            DropColumn("dbo.CashClosing", "Kassenbestand")
            DropColumn("dbo.CashClosing", "Kassenentnahme")
            DropColumn("dbo.CashClosing", "Kasseneinlage")
            DropColumn("dbo.CashClosing", "Differenz")
            DropColumn("dbo.CashClosing", "Bediener")
        End Sub
        
        Public Overrides Sub Down()
            AddColumn("dbo.CashClosing", "Bediener", Function(c) c.String())
            AddColumn("dbo.CashClosing", "Differenz", Function(c) c.Double(nullable := False))
            AddColumn("dbo.CashClosing", "Kasseneinlage", Function(c) c.Double(nullable := False))
            AddColumn("dbo.CashClosing", "Kassenentnahme", Function(c) c.Double(nullable := False))
            AddColumn("dbo.CashClosing", "Kassenbestand", Function(c) c.Double(nullable := False))
            AddColumn("dbo.CashClosing", "Ausgaben", Function(c) c.Double(nullable := False))
            AddColumn("dbo.CashClosing", "Stornierungen", Function(c) c.Double(nullable := False))
            AddColumn("dbo.CashClosing", "Kartenzahlungen", Function(c) c.Double(nullable := False))
            AddColumn("dbo.CashClosing", "Barzahlungen", Function(c) c.Double(nullable := False))
            AddColumn("dbo.CashClosing", "Bruttoumsatz", Function(c) c.Double(nullable := False))
            AddColumn("dbo.CashClosing", "Anfangsbestand", Function(c) c.Double(nullable := False))
            AddColumn("dbo.CashClosing", "Terminal", Function(c) c.String())
            AddColumn("dbo.CashClosing", "EndDatum", Function(c) c.DateTime())
            AddColumn("dbo.CashClosing", "StartDatum", Function(c) c.DateTime(nullable := False))
            AddColumn("dbo.CashClosing", "AccountID", Function(c) c.Int(nullable := False))
            DropColumn("dbo.CashClosing", "Ende")
            DropColumn("dbo.CashClosing", "Start")
            DropColumn("dbo.CashClosing", "ECBon")
            DropColumn("dbo.CashClosing", "SummeKarte")
            DropColumn("dbo.CashClosing", "SummeBar")
            DropColumn("dbo.CashClosing", "ClosedBy")
            DropColumn("dbo.CashClosing", "KasseNr")
        End Sub
    End Class
End Namespace
