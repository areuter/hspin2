Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class CourseandCustomerFlags
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Customer", "Flags", Function(c) c.String())
            AddColumn("dbo.Course", "Flags", Function(c) c.String())
            Sql("INSERT INTO GlobalSetting(Einstellung, Wert, Bezeichnung) VALUES('Flags','Wichtig,Red','Markierungen')")
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Course", "Flags")
            DropColumn("dbo.Customer", "Flags")
        End Sub
    End Class
End Namespace
