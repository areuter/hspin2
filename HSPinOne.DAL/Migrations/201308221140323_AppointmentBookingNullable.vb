Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class AppointmentBookingNullable
        Inherits DbMigration
    
        Public Overrides Sub Up()
            DropForeignKey("dbo.Appointment", "BookingID", "dbo.Booking")
            DropIndex("dbo.Appointment", New String() { "BookingID" })
            AlterColumn("dbo.Appointment", "BookingID", Function(c) c.Int())
            AddForeignKey("dbo.Appointment", "CourseID", "dbo.Course", "CourseID")
            AddForeignKey("dbo.Appointment", "BookingID", "dbo.Booking", "BookingID")
            CreateIndex("dbo.Appointment", "CourseID")
            CreateIndex("dbo.Appointment", "BookingID")

            ' Aktualisieren aller Eintr�ge bei den Appointments
            Sql("UPDATE dbo.Appointment SET CourseID = (SELECT CourseID FROM dbo.Booking WHERE Booking.BookingID = Appointment.BookingID)")

            ' Vorgaben f�r RRule bei allen Veranstaltungen setzen
            Sql("UPDATE Course SET DTStart = (SELECT MIN(Start) FROM Appointment WHERE Appointment.CourseID = Course.CourseID), " & _
                "DTEnd = (SELECT MIN(Ende) FROM Appointment WHERE Appointment.CourseID = Course.CourseID)," & _
                "RRule = 'UNTIL=' + CONVERT(VARCHAR(10), (SELECT Term.Semesterende FROM Term WHERE Term.TermID = Course.TermID), 104) + ';FREQ=Weekly;'" & _
                "WHERE (SELECT COUNT(*) FROM Appointment WHERE Appointment.CourseID = Course.CourseID) > 0")
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.Appointment", New String() { "BookingID" })
            DropIndex("dbo.Appointment", New String() { "CourseID" })
            DropForeignKey("dbo.Appointment", "BookingID", "dbo.Booking")
            DropForeignKey("dbo.Appointment", "CourseID", "dbo.Course")
            AlterColumn("dbo.Appointment", "BookingID", Function(c) c.Int(nullable := False))
            CreateIndex("dbo.Appointment", "BookingID")
            AddForeignKey("dbo.Appointment", "BookingID", "dbo.Booking", "BookingID", cascadeDelete := True)
        End Sub
    End Class
End Namespace
