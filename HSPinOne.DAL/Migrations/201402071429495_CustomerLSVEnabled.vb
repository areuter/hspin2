Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class CustomerLSVEnabled
        Inherits DbMigration
    
        Public Overrides Sub Up()
            Sql("Update Customer SET LSVEnabled = 0 WHERE LSVEnabled IS NULL")
            AlterColumn("dbo.Customer", "LSVEnabled", Function(c) c.Boolean(nullable:=False, defaultValue:=True))
        End Sub
        
        Public Overrides Sub Down()
            AlterColumn("dbo.Customer", "LSVEnabled", Function(c) c.Boolean())
        End Sub
    End Class
End Namespace
