Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class PackagePosition
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Package", "Position", Function(c) c.Int(nullable:=False, defaultValue:=0))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Package", "Position")
        End Sub
    End Class
End Namespace
