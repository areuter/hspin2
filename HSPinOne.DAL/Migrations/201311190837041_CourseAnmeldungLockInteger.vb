Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class CourseAnmeldungLockInteger
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AlterColumn("dbo.Course", "AnmeldungLock", Function(c) c.Int(nullable:=False))
        End Sub
        
        Public Overrides Sub Down()
            AlterColumn("dbo.Course", "AnmeldungLock", Function(c) c.Boolean(nullable := False))
        End Sub
    End Class
End Namespace
