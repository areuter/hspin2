Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class BookingColor
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Booking", "Color", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Booking", "Color")
        End Sub
    End Class
End Namespace
