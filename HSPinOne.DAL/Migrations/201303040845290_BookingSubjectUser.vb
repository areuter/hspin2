Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class BookingSubjectUser
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Booking", "Subject", Function(c) c.String(maxLength := 100))
            AddColumn("dbo.Booking", "UserID", Function(c) c.Int(nullable:=False, defaultValue:=1))
            AddForeignKey("dbo.Booking", "UserID", "dbo.User", "UserID", cascadeDelete := True)
            CreateIndex("dbo.Booking", "UserID")
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.Booking", New String() { "UserID" })
            DropForeignKey("dbo.Booking", "UserID", "dbo.User")
            DropColumn("dbo.Booking", "UserID")
            DropColumn("dbo.Booking", "Subject")
        End Sub
    End Class
End Namespace
