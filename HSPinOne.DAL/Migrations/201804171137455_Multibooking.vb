Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Multibooking
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Course", "AllowMultibooking", Function(c) c.Boolean(nullable:=False, defaultValue:=False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Course", "AllowMultibooking")
        End Sub
    End Class
End Namespace
