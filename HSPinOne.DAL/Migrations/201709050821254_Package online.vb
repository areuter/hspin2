Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Packageonline
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Package", "IsOnlineAvailable", Function(c) c.Boolean(nullable := False))
            AddColumn("dbo.Package", "TemplateID", Function(c) c.Int())
            CreateIndex("dbo.Package", "TemplateID")
            AddForeignKey("dbo.Package", "TemplateID", "dbo.Template", "TemplateID")
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.Package", "TemplateID", "dbo.Template")
            DropIndex("dbo.Package", New String() { "TemplateID" })
            DropColumn("dbo.Package", "TemplateID")
            DropColumn("dbo.Package", "IsOnlineAvailable")
        End Sub
    End Class
End Namespace
