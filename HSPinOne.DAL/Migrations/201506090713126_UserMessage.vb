Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class UserMessage
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.User", "Message", Function(c) c.String(maxLength := 500))
            AddColumn("dbo.User", "Dismissed", Function(c) c.Boolean(nullable:=False, defaultValue:=True))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.User", "Dismissed")
            DropColumn("dbo.User", "Message")
        End Sub
    End Class
End Namespace
