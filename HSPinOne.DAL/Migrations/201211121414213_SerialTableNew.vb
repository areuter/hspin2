Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class SerialTableNew
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Serial",
                Function(c) New With
                    {
                        .SerialID = c.Int(nullable:=False, identity:=True),
                        .SerialKey = c.String(maxLength:=200),
                        .SaltValue = c.String(maxLength:=200)
                    }) _
                .PrimaryKey(Function(t) t.SerialID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropTable("dbo.Serial")
        End Sub
    End Class
End Namespace
