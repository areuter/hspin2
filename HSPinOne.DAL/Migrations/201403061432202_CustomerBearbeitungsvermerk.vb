Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class CustomerBearbeitungsvermerk
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Customer", "Bearbeitungsvermerk", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Customer", "Bearbeitungsvermerk")
        End Sub
    End Class
End Namespace
