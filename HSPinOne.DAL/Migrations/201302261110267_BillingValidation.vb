Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class BillingValidation
        Inherits DbMigration
    
        Public Overrides Sub Up()
            DropForeignKey("dbo.Billing", "AccountID", "dbo.Account")
            DropIndex("dbo.Billing", New String() { "AccountID" })
            AlterColumn("dbo.Billing", "BillingStatus", Function(c) c.String(nullable := False))
            AlterColumn("dbo.Billing", "AccountID", Function(c) c.Int(nullable := False))
            AddForeignKey("dbo.Billing", "AccountID", "dbo.Account", "AccountID", cascadeDelete := True)
            CreateIndex("dbo.Billing", "AccountID")
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.Billing", New String() { "AccountID" })
            DropForeignKey("dbo.Billing", "AccountID", "dbo.Account")
            AlterColumn("dbo.Billing", "AccountID", Function(c) c.Int())
            AlterColumn("dbo.Billing", "BillingStatus", Function(c) c.String())
            CreateIndex("dbo.Billing", "AccountID")
            AddForeignKey("dbo.Billing", "AccountID", "dbo.Account", "AccountID")
        End Sub
    End Class
End Namespace
