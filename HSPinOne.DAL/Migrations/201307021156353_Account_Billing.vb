Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class Account_Billing
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Account", "IBAN", Function(c) c.String())
            AddColumn("dbo.Account", "BIC", Function(c) c.String())
            AddColumn("dbo.Account", "GlaeubigerID", Function(c) c.String())

            Sql("UPDATE Billing SET IsStorno = 0 WHERE IsStorno IS NULL")
            AlterColumn("dbo.Billing", "IsStorno", Function(c) c.Boolean(nullable:=False, defaultValue:=False))
        End Sub
        
        Public Overrides Sub Down()
            AlterColumn("dbo.Billing", "IsStorno", Function(c) c.Boolean())
            DropColumn("dbo.Account", "GlaeubigerID")
            DropColumn("dbo.Account", "BIC")
            DropColumn("dbo.Account", "IBAN")
        End Sub
    End Class
End Namespace
