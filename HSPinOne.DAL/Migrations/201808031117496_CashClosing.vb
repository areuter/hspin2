Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class CashClosing
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.CashClosing",
                Function(c) New With
                    {
                        .CashClosingID = c.Int(nullable := False, identity := True),
                        .AccountID = c.Int(nullable := False),
                        .StartDatum = c.DateTime(nullable := False),
                        .EndDatum = c.DateTime(),
                        .Terminal = c.String(),
                        .Anfangsbestand = c.Double(nullable := False),
                        .Bruttoumsatz = c.Double(nullable := False),
                        .Barzahlungen = c.Double(nullable := False),
                        .Kartenzahlungen = c.Double(nullable := False),
                        .Stornierungen = c.Double(nullable := False),
                        .Ausgaben = c.Double(nullable := False),
                        .Kassenbestand = c.Double(nullable := False),
                        .Kassenentnahme = c.Double(nullable := False),
                        .Kasseneinlage = c.Double(nullable := False),
                        .Differenz = c.Double(nullable := False),
                        .Bediener = c.String()
                    }) _
                .PrimaryKey(Function(t) t.CashClosingID)
            
            AddColumn("dbo.Billing", "CashClosingID", Function(c) c.Int())
            CreateIndex("dbo.Billing", "CashClosingID")
            AddForeignKey("dbo.Billing", "CashClosingID", "dbo.CashClosing", "CashClosingID")
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.Billing", "CashClosingID", "dbo.CashClosing")
            DropIndex("dbo.Billing", New String() { "CashClosingID" })
            DropColumn("dbo.Billing", "CashClosingID")
            DropTable("dbo.CashClosing")
        End Sub
    End Class
End Namespace
