Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Webfrontendstorno
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Billing", "CancellationByUser", Function(c) c.Boolean(nullable:=False, defaultValue:=False))
            AddColumn("dbo.LocationBookinggroup", "TemplateID", Function(c) c.Int())
            CreateIndex("dbo.LocationBookinggroup", "TemplateID")
            AddForeignKey("dbo.LocationBookinggroup", "TemplateID", "dbo.Template", "TemplateID")
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.LocationBookinggroup", "TemplateID", "dbo.Template")
            DropIndex("dbo.LocationBookinggroup", New String() { "TemplateID" })
            DropColumn("dbo.LocationBookinggroup", "TemplateID")
            DropColumn("dbo.Billing", "CancellationByUser")
        End Sub
    End Class
End Namespace
