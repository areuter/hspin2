Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class InitialMigration
        Inherits DbMigration
        Private _Db As String
        Public Property Db As String
            Get
                Return _Db
            End Get
            Set(value As String)
                _Db = value
            End Set
        End Property

        Public Overrides Sub Up()
            CreateTable(
                "dbo.Account",
                Function(c) New With
                    {
                        .AccountID = c.Int(nullable:=False),
                        .AccountName = c.String(),
                        .Kasse = c.Boolean(nullable:=False),
                        .Kontoinhaber = c.String(),
                        .Kontonummer = c.Long(),
                        .BLZ = c.Int()
                    }) _
                .PrimaryKey(Function(t) t.AccountID)

            Sql("INSERT INTO dbo.Account(AccountID, AccountName, Kasse) VALUES ('1600','Barkasse 1', 1)")
            Sql("INSERT INTO dbo.Account(AccountID, AccountName, Kasse) VALUES ('1800','Bankkonto 1', 1)")
            Sql("INSERT INTO dbo.Account(AccountID, AccountName, Kasse) VALUES ('1801','Bankkonto 2', 1)")

            CreateTable(
                "dbo.Billing",
                Function(c) New With
                    {
                        .BillingID = c.Int(nullable:=False, identity:=True),
                        .CustomerID = c.Int(nullable:=False),
                        .BillingStatus = c.String(),
                        .ContractID = c.Int(),
                        .CourseID = c.Int(),
                        .AccountID = c.Int(),
                        .PaymentMethodID = c.Int(nullable:=False),
                        .BookingID = c.Int(),
                        .ProductID = c.Int(),
                        .PackageID = c.Int(),
                        .Brutto = c.Decimal(nullable:=False, precision:=18, scale:=2),
                        .Steuerprozent = c.Decimal(precision:=18, scale:=2),
                        .Rechnungsdatum = c.DateTime(nullable:=False),
                        .Faelligkeit = c.DateTime(nullable:=False),
                        .Buchungsdatum = c.DateTime(),
                        .Rueckbuchungsdatum = c.DateTime(),
                        .Verwendungszweck = c.String(maxLength:=250),
                        .Bemerkung = c.String(),
                        .IsSelected = c.Boolean(),
                        .DtausID = c.Int(),
                        .Kontonummer = c.Long(),
                        .BLZ = c.Int(),
                        .IsStorno = c.Boolean(),
                        .Comment = c.String(),
                        .CostcenterID = c.Int(nullable:=False)
                    }) _
                .PrimaryKey(Function(t) t.BillingID) _
                .ForeignKey("dbo.Account", Function(t) t.AccountID) _
                .ForeignKey("dbo.Contract", Function(t) t.ContractID) _
                .ForeignKey("dbo.Customer", Function(t) t.CustomerID, cascadeDelete:=True) _
                .ForeignKey("dbo.Course", Function(t) t.CourseID) _
                .ForeignKey("dbo.Package", Function(t) t.PackageID) _
                .ForeignKey("dbo.PaymentMethod", Function(t) t.PaymentMethodID, cascadeDelete:=True) _
                .ForeignKey("dbo.Product", Function(t) t.ProductID) _
                .ForeignKey("dbo.Costcenter", Function(t) t.CostcenterID, cascadeDelete:=True) _
                .Index(Function(t) t.AccountID) _
                .Index(Function(t) t.ContractID) _
                .Index(Function(t) t.CustomerID) _
                .Index(Function(t) t.CourseID) _
                .Index(Function(t) t.PackageID) _
                .Index(Function(t) t.PaymentMethodID) _
                .Index(Function(t) t.ProductID) _
                .Index(Function(t) t.CostcenterID)

            Sql("DBCC CHECKIDENT( Billing, RESEED, 100000)")

            CreateTable(
                "dbo.Contract",
                Function(c) New With
                    {
                        .ContractID = c.Int(nullable:=False, identity:=True),
                        .CustomerID = c.Int(nullable:=False),
                        .Vertragsbeginn = c.DateTime(nullable:=False),
                        .Monate = c.Int(nullable:=False),
                        .Vertragsende = c.DateTime(nullable:=False),
                        .ContractRateID = c.Int(nullable:=False),
                        .PaymentMethodID = c.Int(nullable:=False),
                        .Ratenbetrag = c.Decimal(nullable:=False, precision:=18, scale:=2),
                        .Zahlungsbeginn = c.DateTime(nullable:=False),
                        .Startgebuehr = c.Decimal(precision:=18, scale:=2),
                        .Verlaengerungsmonate = c.Int(),
                        .Kuendigungseingang = c.DateTime(),
                        .Kuendigungzum = c.DateTime(),
                        .Erstelltam = c.DateTime(),
                        .Erstelltvon = c.Int(),
                        .Bearbeitetam = c.DateTime(),
                        .Bearbeitetvon = c.Int(),
                        .CostcenterID = c.Int(nullable:=False),
                        .Kennzeichen = c.String(maxLength:=50)
                    }) _
                .PrimaryKey(Function(t) t.ContractID) _
                .ForeignKey("dbo.ContractRate", Function(t) t.ContractRateID, cascadeDelete:=True) _
                .ForeignKey("dbo.Customer", Function(t) t.CustomerID, cascadeDelete:=True) _
                .ForeignKey("dbo.Costcenter", Function(t) t.CostcenterID, cascadeDelete:=True) _
                .Index(Function(t) t.ContractRateID) _
                .Index(Function(t) t.CustomerID) _
                .Index(Function(t) t.CostcenterID)

            CreateTable(
                "dbo.ContractRate",
                Function(c) New With
                    {
                        .ContractRateID = c.Int(nullable:=False, identity:=True),
                        .ContractRateName = c.String(),
                        .Monate = c.Int(nullable:=False),
                        .Ratenbetrag = c.Decimal(nullable:=False, precision:=18, scale:=2),
                        .Velaengerungsmonate = c.Int(nullable:=False),
                        .Kennzeichen = c.String(maxLength:=50)
                    }) _
                .PrimaryKey(Function(t) t.ContractRateID)

            CreateTable(
                "dbo.Customer",
                Function(c) New With
                    {
                        .CustomerID = c.Int(nullable:=False, identity:=True),
                        .CustomerStateID = c.Int(nullable:=False),
                        .Gender = c.String(),
                        .Vorname = c.String(),
                        .Nachname = c.String(),
                        .Titel = c.String(),
                        .Suchname = c.String(),
                        .Strasse = c.String(),
                        .PLZ = c.String(),
                        .Ort = c.String(),
                        .Geburtstag = c.DateTime(),
                        .Briefanrede = c.String(),
                        .Telefon1 = c.String(),
                        .Telefon2 = c.String(),
                        .Telefon3 = c.String(),
                        .Mail1 = c.String(),
                        .Mail2 = c.String(),
                        .Newsletter = c.Boolean(),
                        .LSVEnabled = c.Boolean(),
                        .Kontoinhaber = c.String(),
                        .Kontonummer = c.Long(),
                        .BLZ = c.Int(),
                        .IBAN = c.String(),
                        .BIC = c.String(),
                        .Kartennummer = c.Long(),
                        .Bemerkung = c.String(),
                        .Erstelltam = c.DateTime(),
                        .Erstelltvon = c.Int(),
                        .Bearbeitetam = c.DateTime(),
                        .Bearbeitetvon = c.Int(),
                        .Aktiv = c.Boolean(),
                        .Passwort = c.String(),
                        .Salt = c.String(),
                        .Matrikelnummer = c.String(),
                        .Checkinmessage = c.String(maxLength:=500),
                        .HasImage = c.Boolean(nullable:=False, defaultValue:=False)
                    }) _
                .PrimaryKey(Function(t) t.CustomerID) _
                .ForeignKey("dbo.CustomerState", Function(t) t.CustomerStateID, cascadeDelete:=True) _
                .Index(Function(t) t.CustomerStateID)
            Sql("DBCC CHECKIDENT( Customer, RESEED, 100000)")

            CreateTable(
                "dbo.Booking",
                Function(c) New With
                    {
                        .BookingID = c.Int(nullable:=False, identity:=True),
                        .CustomerID = c.Int(),
                        .CourseID = c.Int(),
                        .LocationID = c.Int(),
                        .Start = c.DateTime(nullable:=False),
                        .Ende = c.DateTime(nullable:=False),
                        .RecurrencePattern = c.String(),
                        .RecurrencePatternEndDate = c.DateTime()
                    }) _
                .PrimaryKey(Function(t) t.BookingID) _
                .ForeignKey("dbo.Location", Function(t) t.LocationID) _
                .ForeignKey("dbo.Course", Function(t) t.CourseID) _
                .ForeignKey("dbo.Customer", Function(t) t.CustomerID) _
                .Index(Function(t) t.LocationID) _
                .Index(Function(t) t.CourseID) _
                .Index(Function(t) t.CustomerID)

            Sql("DBCC CHECKIDENT( Customer, RESEED, 100000)")

            CreateTable(
                "dbo.Appointment",
                Function(c) New With
                    {
                        .AppointmentID = c.Int(nullable:=False, identity:=True),
                        .LocationID = c.Int(nullable:=False),
                        .Start = c.DateTime(nullable:=False),
                        .Ende = c.DateTime(nullable:=False),
                        .CourseID = c.Int(),
                        .BookingID = c.Int(nullable:=False),
                        .Canceled = c.Boolean(nullable:=False)
                    }) _
                .PrimaryKey(Function(t) t.AppointmentID) _
                .ForeignKey("dbo.Booking", Function(t) t.BookingID, cascadeDelete:=True) _
                .ForeignKey("dbo.Location", Function(t) t.LocationID, cascadeDelete:=True) _
                .Index(Function(t) t.BookingID) _
                .Index(Function(t) t.LocationID)

            Sql("DBCC CHECKIDENT( Appointment, RESEED, 100000)")

            CreateTable(
                "dbo.Location",
                Function(c) New With
                    {
                        .LocationID = c.Int(nullable:=False, identity:=True),
                        .LocationName = c.String(),
                        .LocationDescription = c.String(),
                        .Strasse = c.String(),
                        .PLZ = c.String(),
                        .Ort = c.String(),
                        .Latitude = c.Double(),
                        .Longitude = c.Double(),
                        .CampusID = c.Int(nullable:=False)
                    }) _
                .PrimaryKey(Function(t) t.LocationID) _
                .ForeignKey("dbo.Campus", Function(t) t.CampusID, cascadeDelete:=True) _
                .Index(Function(t) t.CampusID)

            Sql("INSERT INTO dbo.Location(LocationName, CampusID) VALUES ('Demo Raum 1', 1)")
            Sql("INSERT INTO dbo.Location(LocationName, CampusID) VALUES ('Demo Raum 2', 1)")
            Sql("INSERT INTO dbo.Location(LocationName, CampusID) VALUES ('Demo Raum 3', 1)")
            Sql("INSERT INTO dbo.Location(LocationName, CampusID) VALUES ('Demo Raum 4', 1)")

            Sql("DBCC CHECKIDENT( Location, RESEED, 1000)")

            CreateTable(
                "dbo.Course",
                Function(c) New With
                    {
                        .CourseID = c.Int(nullable:=False, identity:=True),
                        .SportID = c.Int(nullable:=False),
                        .TermID = c.Int(nullable:=False),
                        .PartyID = c.Int(nullable:=False),
                        .Zusatzinfo = c.String(),
                        .Zeittext = c.String(),
                        .AnsprechpartnerID = c.Int(),
                        .CategoryID = c.Int(),
                        .LocationID = c.Int(),
                        .CourseStateID = c.Int(nullable:=False),
                        .CourseRegistrationID = c.Int(nullable:=False),
                        .CourseCostID = c.Int(nullable:=False),
                        .CourseAudienceID = c.Int(nullable:=False),
                        .CourseGenderID = c.Int(nullable:=False),
                        .Bemerkung = c.String(),
                        .Bearbeitungsvermerk = c.String(),
                        .minTN = c.Int(),
                        .maxTN = c.Int(),
                        .Aktiv = c.Boolean(nullable:=False),
                        .Erstelltam = c.DateTime(nullable:=False),
                        .Erstelltvon = c.Int(nullable:=False),
                        .Bearbeitetam = c.DateTime(),
                        .Bearbeitetvon = c.Int(),
                        .Anmeldungvon = c.DateTime(nullable:=False),
                        .Anmeldungbis = c.DateTime(nullable:=False),
                        .AnmeldungLock = c.Boolean(nullable:=False),
                        .Faelligkeit = c.DateTime(),
                        .CostcenterID = c.Int(nullable:=False)
                    }) _
                .PrimaryKey(Function(t) t.CourseID) _
                .ForeignKey("dbo.CourseAudience", Function(t) t.CourseAudienceID, cascadeDelete:=True) _
                .ForeignKey("dbo.CourseCost", Function(t) t.CourseCostID, cascadeDelete:=True) _
                .ForeignKey("dbo.CourseGender", Function(t) t.CourseGenderID, cascadeDelete:=True) _
                .ForeignKey("dbo.CourseRegistration", Function(t) t.CourseRegistrationID, cascadeDelete:=True) _
                .ForeignKey("dbo.CourseState", Function(t) t.CourseStateID, cascadeDelete:=True) _
                .ForeignKey("dbo.Customer", Function(t) t.AnsprechpartnerID) _
                .ForeignKey("dbo.Location", Function(t) t.LocationID) _
                .ForeignKey("dbo.Party", Function(t) t.PartyID, cascadeDelete:=True) _
                .ForeignKey("dbo.Sport", Function(t) t.SportID, cascadeDelete:=True) _
                .ForeignKey("dbo.Term", Function(t) t.TermID, cascadeDelete:=True) _
                .ForeignKey("dbo.Category", Function(t) t.CategoryID) _
                .ForeignKey("dbo.Costcenter", Function(t) t.CostcenterID, cascadeDelete:=True) _
                .Index(Function(t) t.CourseAudienceID) _
                .Index(Function(t) t.CourseCostID) _
                .Index(Function(t) t.CourseGenderID) _
                .Index(Function(t) t.CourseRegistrationID) _
                .Index(Function(t) t.CourseStateID) _
                .Index(Function(t) t.AnsprechpartnerID) _
                .Index(Function(t) t.LocationID) _
                .Index(Function(t) t.PartyID) _
                .Index(Function(t) t.SportID) _
                .Index(Function(t) t.TermID) _
                .Index(Function(t) t.CategoryID) _
                .Index(Function(t) t.CostcenterID)
            Sql("DBCC CHECKIDENT( Course, RESEED, 10000)")

            CreateTable(
                "dbo.CourseAudience",
                Function(c) New With
                    {
                        .CourseAudienceID = c.Int(nullable:=False, identity:=True),
                        .CourseAudienceName = c.String()
                    }) _
                .PrimaryKey(Function(t) t.CourseAudienceID)

            Sql("INSERT INTO dbo.CourseAudience(CourseAudienceName) VALUES ('Anf�nger')")
            Sql("INSERT INTO dbo.CourseAudience(CourseAudienceName) VALUES ('Fortgeschrittene')")


            CreateTable(
                "dbo.CourseCost",
                Function(c) New With
                    {
                        .CourseCostID = c.Int(nullable:=False, identity:=True),
                        .CourseCostName = c.String()
                    }) _
                .PrimaryKey(Function(t) t.CourseCostID)

            Sql("INSERT INTO dbo.CourseCost(CourseCostName) VALUES ('Kostenlos')")
            Sql("INSERT INTO dbo.CourseCost(CourseCostName) VALUES ('Nutzerkarte')")

            CreateTable(
                "dbo.CourseGender",
                Function(c) New With
                    {
                        .CourseGenderID = c.Int(nullable:=False, identity:=True),
                        .CourseGenderName = c.String()
                    }) _
                .PrimaryKey(Function(t) t.CourseGenderID)

            Sql("INSERT INTO dbo.CourseGender(CourseGenderName) VALUES ('Frauen')")
            Sql("INSERT INTO dbo.CourseGender(CourseGenderName) VALUES ('M�nner')")
            Sql("INSERT INTO dbo.CourseGender(CourseGenderName) VALUES ('Frauen + M�nner')")


            CreateTable(
                "dbo.CourseRegistration",
                Function(c) New With
                    {
                        .CourseRegistrationID = c.Int(nullable:=False, identity:=True),
                        .CourseRegistrationName = c.String(),
                        .Chart = c.Boolean(nullable:=False),
                        .RegistrationDates = c.Boolean(nullable:=False)
                    }) _
                .PrimaryKey(Function(t) t.CourseRegistrationID)

            Sql("INSERT INTO dbo.CourseRegistration(CourseRegistrationName, Chart, RegistrationDates) VALUES ('nicht anmeldepflichtig',0,0)")
            Sql("INSERT INTO dbo.CourseRegistration(CourseRegistrationName, Chart, RegistrationDates) VALUES ('anmeldepflichtig',0,1)")
            Sql("INSERT INTO dbo.CourseRegistration(CourseRegistrationName, Chart, RegistrationDates) VALUES ('siehe Kursbeschreibung',0,0)")

            CreateTable(
                "dbo.CourseState",
                Function(c) New With
                    {
                        .CourseStateID = c.Int(nullable:=False, identity:=True),
                        .CourseStatename = c.String(),
                        .CourseStatecolor = c.String()
                    }) _
                .PrimaryKey(Function(t) t.CourseStateID)

            Sql("INSERT INTO dbo.CourseState(CourseStatename, CourseStatecolor) VALUES ('ungepr�ft','#FFFF0000')")
            Sql("INSERT INTO dbo.CourseState(CourseStatename, CourseStatecolor) VALUES ('in Bearbeitung','#FFFF8040')")
            Sql("INSERT INTO dbo.CourseState(CourseStatename, CourseStatecolor) VALUES ('gepr�ft','#FF00FF00')")


            CreateTable(
                "dbo.Party",
                Function(c) New With
                    {
                        .PartyID = c.Int(nullable:=False, identity:=True),
                        .Partyname = c.String(),
                        .Partycolor = c.String()
                    }) _
                .PrimaryKey(Function(t) t.PartyID)

            Sql("INSERT INTO dbo.Party(Partyname, Partycolor) VALUES ('Hochschulsport', '#7BBFED')")
            Sql("INSERT INTO dbo.Party(Partyname, Partycolor) VALUES ('Sportwissenschaft', '#FFCB5C')")
            Sql("INSERT INTO dbo.Party(Partyname, Partycolor) VALUES ('Externe', '#E4A1FF')")


            CreateTable(
                "dbo.Sport",
                Function(c) New With
                    {
                        .SportID = c.Int(nullable:=False, identity:=True),
                        .Sportname = c.String(),
                        .Beschreibung = c.String(),
                        .CustomerID = c.Int(),
                        .StdLohn = c.Decimal(precision:=18, scale:=2)
                    }) _
                .PrimaryKey(Function(t) t.SportID) _
                .ForeignKey("dbo.Customer", Function(t) t.CustomerID) _
                .Index(Function(t) t.CustomerID)

            Sql("INSERT INTO [dbo].[Sport](Sportname, Beschreibung) VALUES ('Default Sportart', 'Lorem Ispum')")

            Sql("DBCC CHECKIDENT( Sport, RESEED, 100)")

            CreateTable(
                "dbo.Term",
                Function(c) New With
                    {
                        .TermID = c.Int(nullable:=False, identity:=True),
                        .Termname = c.String(),
                        .Semesterbeginn = c.DateTime(nullable:=False),
                        .Semesterende = c.DateTime(nullable:=False),
                        .AktivWWW = c.Boolean(),
                        .Anmeldungab = c.DateTime(),
                        .Anmeldungbis = c.DateTime(),
                        .Aktiv = c.Boolean(nullable:=False)
                    }) _
                .PrimaryKey(Function(t) t.TermID)


            Sql("INSERT [dbo].[Term](Termname, Semesterbeginn, Semesterende, AktivWWW, Anmeldungab, Anmeldungbis, Aktiv) VALUES ('Default Semester', '2019-01-01', '2019-06-01', 'True', '2019-01-01', '2019-06-01','True' )")


            CreateTable(
                "dbo.Category",
                Function(c) New With
                    {
                        .CategoryID = c.Int(nullable:=False, identity:=True),
                        .CategoryName = c.String(),
                        .CategoryColor = c.String()
                    }) _
                .PrimaryKey(Function(t) t.CategoryID)

            Sql("INSERT INTO dbo.Category(CategoryName, CategoryColor) VALUES ('Unkategorisiert', '#00FFFFFF')")
            Sql("INSERT INTO dbo.Category(CategoryName, CategoryColor) VALUES ('Sportkurs', '#00FFEEFF')")
            Sql("INSERT INTO dbo.Category(CategoryName, CategoryColor) VALUES ('Exkursion', '#00FFFFEE')")

            CreateTable(
                "dbo.CoursePrice",
                Function(c) New With
                    {
                        .CoursePriceID = c.Int(nullable:=False, identity:=True),
                        .CourseID = c.Int(nullable:=False),
                        .CustomerStateID = c.Int(nullable:=False),
                        .Preis = c.Decimal(nullable:=False, precision:=18, scale:=2),
                        .Anmeldungvon = c.DateTime(),
                        .Anmeldungbis = c.DateTime(),
                        .Anmeldungerlaubt = c.Boolean()
                    }) _
                .PrimaryKey(Function(t) t.CoursePriceID) _
                .ForeignKey("dbo.Course", Function(t) t.CourseID, cascadeDelete:=True) _
                .ForeignKey("dbo.CustomerState", Function(t) t.CustomerStateID, cascadeDelete:=True) _
                .Index(Function(t) t.CourseID) _
                .Index(Function(t) t.CustomerStateID)

            CreateTable(
                "dbo.CustomerState",
                Function(c) New With
                    {
                        .CustomerStateID = c.Int(nullable:=False, identity:=True),
                        .CustomerStatename = c.String()
                    }) _
                .PrimaryKey(Function(t) t.CustomerStateID)

            Sql("INSERT INTO dbo.CustomerState(CustomerStatename) VALUES ('Student')")
            Sql("INSERT INTO dbo.CustomerState(CustomerStatename) VALUES ('Bedienstet')")
            Sql("INSERT INTO dbo.CustomerState(CustomerStatename) VALUES ('Extern')")

            CreateTable(
                "dbo.PackagePrice",
                Function(c) New With
                    {
                        .PackagePriceID = c.Int(nullable:=False, identity:=True),
                        .PackageID = c.Int(nullable:=False),
                        .CustomerStateID = c.Int(nullable:=False),
                        .Preis = c.Decimal(nullable:=False, precision:=18, scale:=2)
                    }) _
                .PrimaryKey(Function(t) t.PackagePriceID) _
                .ForeignKey("dbo.CustomerState", Function(t) t.CustomerStateID, cascadeDelete:=True) _
                .ForeignKey("dbo.Package", Function(t) t.PackageID, cascadeDelete:=True) _
                .Index(Function(t) t.CustomerStateID) _
                .Index(Function(t) t.PackageID)

            CreateTable(
                "dbo.Package",
                Function(c) New With
                    {
                        .PackageID = c.Int(nullable:=False, identity:=True),
                        .PackageName = c.String(),
                        .Zutritte = c.Int(nullable:=False),
                        .Warengruppe = c.Int(nullable:=False),
                        .CostcenterID = c.Int(nullable:=False)
                    }) _
                .PrimaryKey(Function(t) t.PackageID) _
                .ForeignKey("dbo.Costcenter", Function(t) t.CostcenterID, cascadeDelete:=True) _
                .Index(Function(t) t.CostcenterID)

            CreateTable(
                "dbo.Costcenter",
                Function(c) New With
                    {
                        .CostcenterID = c.Int(nullable:=False, identity:=True),
                        .Costcentername = c.String()
                    }) _
                .PrimaryKey(Function(t) t.CostcenterID)

            Sql("INSERT INTO dbo.Costcenter(Costcentername) VALUES ('Kostenstelle 1')")

            CreateTable(
                "dbo.CourseStaff",
                Function(c) New With
                    {
                        .CourseStaffID = c.Int(nullable:=False, identity:=True),
                        .CourseID = c.Int(nullable:=False),
                        .CustomerID = c.Int(nullable:=False),
                        .StdLohn = c.Decimal(nullable:=False, precision:=18, scale:=2),
                        .Stdzetteldruck = c.DateTime(),
                        .Stdzettelzurueck = c.DateTime(),
                        .StaffContractID = c.Int()
                    }) _
                .PrimaryKey(Function(t) t.CourseStaffID) _
                .ForeignKey("dbo.Course", Function(t) t.CourseID, cascadeDelete:=True) _
                .ForeignKey("dbo.Customer", Function(t) t.CustomerID, cascadeDelete:=True) _
                .ForeignKey("dbo.StaffContract", Function(t) t.StaffContractID) _
                .Index(Function(t) t.CourseID) _
                .Index(Function(t) t.CustomerID) _
                .Index(Function(t) t.StaffContractID)

            CreateTable(
                "dbo.StaffContract",
                Function(c) New With
                    {
                        .StaffContractID = c.Int(nullable:=False, identity:=True),
                        .CustomerID = c.Int(nullable:=False),
                        .Beginn = c.DateTime(nullable:=False),
                        .Ende = c.DateTime(nullable:=False),
                        .Vertragsdruck = c.DateTime(nullable:=True),
                        .Vertragzurueck = c.DateTime(nullable:=True),
                        .IsSelected = c.Boolean()
                    }) _
                .PrimaryKey(Function(t) t.StaffContractID) _
                .ForeignKey("dbo.Customer", Function(t) t.CustomerID, cascadeDelete:=True) _
                .Index(Function(t) t.CustomerID)

            CreateTable(
                "dbo.LocationLocationgroup",
                Function(c) New With
                    {
                        .ID = c.Int(nullable:=False, identity:=True),
                        .LocationID = c.Int(nullable:=False),
                        .LocationgroupID = c.Int(nullable:=False)
                    }) _
                .PrimaryKey(Function(t) t.ID) _
                .ForeignKey("dbo.Location", Function(t) t.LocationID, cascadeDelete:=True) _
                .ForeignKey("dbo.Locationgroup", Function(t) t.LocationgroupID, cascadeDelete:=True) _
                .Index(Function(t) t.LocationID) _
                .Index(Function(t) t.LocationgroupID)

            Sql("INSERT INTO dbo.LocationLocationgroup(LocationID, LocationgroupID) VALUES (1, 1)")
            Sql("INSERT INTO dbo.LocationLocationgroup(LocationID, LocationgroupID) VALUES (2, 1)")
            Sql("INSERT INTO dbo.LocationLocationgroup(LocationID, LocationgroupID) VALUES (3, 1)")
            Sql("INSERT INTO dbo.LocationLocationgroup(LocationID, LocationgroupID) VALUES (4, 1)")

            CreateTable(
                "dbo.Locationgroup",
                Function(c) New With
                    {
                        .LocationgroupId = c.Int(nullable:=False, identity:=True),
                        .Locationgroupname = c.String()
                    }) _
                .PrimaryKey(Function(t) t.LocationgroupId)

            Sql("INSERT INTO dbo.Locationgroup(Locationgroupname) VALUES ('Demo R�ume')")

            CreateTable(
                "dbo.Campus",
                Function(c) New With
                    {
                        .CampusID = c.Int(nullable:=False, identity:=True),
                        .CampusName = c.String(),
                        .CampusDescription = c.String()
                    }) _
                .PrimaryKey(Function(t) t.CampusID)
            Sql("INSERT INTO dbo.Campus(CampusName) VALUES ('Hauptcampus')")

            CreateTable(
                "dbo.LocationEquipment",
                Function(c) New With
                    {
                        .LocationEquipmentID = c.Int(nullable:=False, identity:=True),
                        .EquipmentID = c.Int(nullable:=False),
                        .LocationID = c.Int(nullable:=False),
                        .Anzahl = c.Double(nullable:=False)
                    }) _
                .PrimaryKey(Function(t) t.LocationEquipmentID)

            CreateTable(
                "dbo.Equipment",
                Function(c) New With
                    {
                        .EquipmentID = c.Int(nullable:=False, identity:=True),
                        .EquipmentCategory = c.String(),
                        .EquipmentName = c.String(),
                        .EquipmentOrder = c.Int(nullable:=False)
                    }) _
                .PrimaryKey(Function(t) t.EquipmentID)

            CreateTable(
                "dbo.Checkin",
                Function(c) New With
                    {
                        .CheckinID = c.Int(nullable:=False, identity:=True),
                        .CustomerID = c.Int(),
                        .Schluessel = c.String(),
                        .Checkintime = c.DateTime(nullable:=False),
                        .Nachname = c.String(),
                        .Vorname = c.String(),
                        .Vertragsart = c.String(),
                        .Kennzeichen = c.String(),
                        .Foto = c.String()
                    }) _
                .PrimaryKey(Function(t) t.CheckinID) _
                .ForeignKey("dbo.Customer", Function(t) t.CustomerID) _
                .Index(Function(t) t.CustomerID)
            Sql("DBCC CHECKIDENT( Checkin, RESEED, 100000)")

            CreateTable(
                "dbo.Document",
                Function(c) New With
                    {
                        .DocumentID = c.Int(nullable:=False, identity:=True),
                        .CustomerID = c.Int(nullable:=False),
                        .Infotext = c.String(),
                        .Erstelldatum = c.DateTime(nullable:=False),
                        .Dateiname = c.String()
                    }) _
                .PrimaryKey(Function(t) t.DocumentID) _
                .ForeignKey("dbo.Customer", Function(t) t.CustomerID, cascadeDelete:=True) _
                .Index(Function(t) t.CustomerID)

            CreateTable(
                "dbo.CheckinLog",
                Function(c) New With
                    {
                        .CheckinLogID = c.Int(nullable:=False, identity:=True),
                        .CustomerID = c.Int(nullable:=False),
                        .Checkin = c.DateTime(nullable:=False),
                        .Checkout = c.DateTime(nullable:=False)
                    }) _
                .PrimaryKey(Function(t) t.CheckinLogID) _
                .ForeignKey("dbo.Customer", Function(t) t.CustomerID, cascadeDelete:=True) _
                .Index(Function(t) t.CustomerID)

            CreateTable(
                "dbo.PaymentMethod",
                Function(c) New With
                    {
                        .PaymentMethodID = c.Int(nullable:=False, identity:=True),
                        .PaymentMethodname = c.String()
                    }) _
                .PrimaryKey(Function(t) t.PaymentMethodID)

            Sql("INSERT INTO dbo.PaymentMethod(PaymentMethodname) VALUES ('Bar')")
            Sql("INSERT INTO dbo.PaymentMethod(PaymentMethodname) VALUES ('Lastschrift')")
            Sql("INSERT INTO dbo.PaymentMethod(PaymentMethodname) VALUES ('�berweisung')")
            Sql("INSERT INTO dbo.PaymentMethod(PaymentMethodname) VALUES ('Kartenzahlung')")

            CreateTable(
                "dbo.Product",
                Function(c) New With
                    {
                        .ProductID = c.Int(nullable:=False, identity:=True),
                        .Productname = c.String(),
                        .Brutto = c.Decimal(nullable:=False, precision:=18, scale:=2),
                        .Steuersatz = c.Int(),
                        .CostcenterID = c.Int(nullable:=False)
                    }) _
                .PrimaryKey(Function(t) t.ProductID) _
                .ForeignKey("dbo.Costcenter", Function(t) t.CostcenterID, cascadeDelete:=True) _
                .Index(Function(t) t.CostcenterID)

            'Sql("INSERT INTO dbo.Product(Productname, Brutto, CostcenterID) VALUES ('Stornogeb�hr', '3', '1')")


            CreateTable(
                "dbo.PackageLog",
                Function(c) New With
                    {
                        .PackageLogID = c.Int(nullable:=False, identity:=True),
                        .BillingID = c.Int(nullable:=False),
                        .Zeit = c.DateTime(nullable:=False)
                    }) _
                .PrimaryKey(Function(t) t.PackageLogID) _
                .ForeignKey("dbo.Billing", Function(t) t.BillingID, cascadeDelete:=True) _
                .Index(Function(t) t.BillingID)

            CreateTable(
                "dbo.Dtaus",
                Function(c) New With
                    {
                        .DtausID = c.Int(nullable:=False, identity:=True),
                        .Agname = c.String(),
                        .Agkonto = c.String(),
                        .Agblz = c.String(),
                        .SumC = c.Int(),
                        .Sumkonto = c.Decimal(precision:=18, scale:=2),
                        .Sumblz = c.Decimal(precision:=18, scale:=2),
                        .Sumbetrag = c.Decimal(precision:=18, scale:=2),
                        .Datum = c.DateTime(),
                        .Sicherungsdatei = c.String()
                    }) _
                .PrimaryKey(Function(t) t.DtausID)

            CreateTable(
                "dbo.EquipmentCategory",
                Function(c) New With
                    {
                        .EquipmentCategoryID = c.Int(nullable:=False, identity:=True),
                        .EquipmentCategoryName = c.String(),
                        .EquipmentCategoryCategory = c.Int(nullable:=False)
                    }) _
                .PrimaryKey(Function(t) t.EquipmentCategoryID)

            CreateTable(
                "dbo.GlobalSetting",
                Function(c) New With
                    {
                        .GlobalSettingID = c.Int(nullable:=False, identity:=True),
                        .Einstellung = c.String(),
                        .Wert = c.String(),
                        .Bezeichnung = c.String(),
                        .Beschreibung = c.String()
                    }) _
                .PrimaryKey(Function(t) t.GlobalSettingID)

            Sql("INSERT INTO dbo.GlobalSetting(Einstellung, Bezeichnung) VALUES ('datafolder','Datenordner')")
            Sql("INSERT INTO dbo.GlobalSetting(Einstellung, Bezeichnung) VALUES ('dmstemppath','Temp Ordner f�r DMS')")
            Sql("INSERT INTO dbo.GlobalSetting(Einstellung, Bezeichnung, Wert) VALUES ('feedurl','URL des NewsFeed', 'http://www.adh.de/medien/rss2.xml')")
            Sql("INSERT INTO dbo.GlobalSetting(Einstellung, Bezeichnung) VALUES ('hspbundesland','Bundesland')")
            Sql("INSERT INTO dbo.GlobalSetting(Einstellung, Bezeichnung) VALUES ('hspemail','Email Adresse')")
            Sql("INSERT INTO dbo.GlobalSetting(Einstellung, Bezeichnung) VALUES ('hspname','Name der Einrichtung')")
            Sql("INSERT INTO dbo.GlobalSetting(Einstellung, Bezeichnung) VALUES ('hspstr','Stra�e')")
            Sql("INSERT INTO dbo.GlobalSetting(Einstellung, Bezeichnung) VALUES ('hsport','PLZ und Ort der Einrichtung')")
            Sql("INSERT INTO dbo.GlobalSetting(Einstellung, Bezeichnung) VALUES ('hspphone','Telefonnummer')")
            Sql("INSERT INTO dbo.GlobalSetting(Einstellung, Bezeichnung, Wert) VALUES ('kontocheckurl','URL f�r die Kontopr�froutine','http://www.sport.uni-goettingen.de/bav/scripts/check.php')")
            Sql("INSERT INTO dbo.GlobalSetting(Einstellung, Bezeichnung, Wert) VALUES ('defaultaccount','Standard Bankkonto', '1800')")

            Sql("INSERT INTO dbo.GlobalSetting(Einstellung, Bezeichnung, Wert) VALUES ('smtphost','SMTP Host', '')")
            Sql("INSERT INTO dbo.GlobalSetting(Einstellung, Bezeichnung, Wert) VALUES ('smtpport','SMTP Port', '0')")
            Sql("INSERT INTO dbo.GlobalSetting(Einstellung, Bezeichnung, Wert) VALUES ('smtplogin','SMTP Login', '')")
            Sql("INSERT INTO dbo.GlobalSetting(Einstellung, Bezeichnung, Wert) VALUES ('smtppw','SMTP Passwort', '')")
            Sql("INSERT INTO dbo.GlobalSetting(Einstellung, Bezeichnung, Wert) VALUES ('smtpssl','SMTP SSL', 'false')")
            Sql("INSERT INTO dbo.GlobalSetting(Einstellung, Bezeichnung, Wert) VALUES ('smtpfrom','SMTP Von/Antwort', '')")
            Sql("INSERT INTO dbo.GlobalSetting(Einstellung, Bezeichnung, Wert) VALUES ('smtpfrom','SMTP Von/Antwort', '')")
            Sql("INSERT INTO dbo.GlobalSetting(Einstellung, Bezeichnung, Wert) VALUES ('stornomail','Stornomail', 'false')")

            CreateTable(
                "dbo.Holiday",
                Function(c) New With
                    {
                        .HolidayID = c.Int(nullable:=False, identity:=True),
                        .HolidayDate = c.DateTime(nullable:=False),
                        .HolidayName = c.String(),
                        .Feiertag = c.Boolean()
                    }) _
                .PrimaryKey(Function(t) t.HolidayID)

            CreateTable(
                "dbo.Institution",
                Function(c) New With
                    {
                        .InstitutionID = c.Int(nullable:=False, identity:=True),
                        .Description = c.String()
                    }) _
                .PrimaryKey(Function(t) t.InstitutionID)

            Sql("INSERT INTO dbo.Institution(Description) VALUES ('Hochschulsport XY')")

            CreateTable(
                           "dbo.Role",
                           Function(c) New With
                               {
                                   .RoleID = c.Int(nullable:=False, identity:=True),
                                   .RoleName = c.String()
                               }) _
                           .PrimaryKey(Function(t) t.RoleID)

            Sql("INSERT INTO dbo.Role(RoleName) VALUES ('System Administrator')")
            Sql("INSERT INTO dbo.Role(RoleName) VALUES ('Administrator')")
            Sql("INSERT INTO dbo.Role(RoleName) VALUES ('User Extended')")
            Sql("INSERT INTO dbo.Role(RoleName) VALUES ('User')")
            Sql("INSERT INTO dbo.Role(RoleName) VALUES ('Infopoint Extended')")
            Sql("INSERT INTO dbo.Role(RoleName) VALUES ('Infopoint')")
            Sql("INSERT INTO dbo.Role(RoleName) VALUES ('�bungsleiter')")

            CreateTable(
                "dbo.User",
                Function(c) New With
                    {
                        .UserID = c.Int(nullable:=False, identity:=True),
                        .InstitutionID = c.Int(nullable:=False),
                        .RoleID = c.Int(nullable:=False),
                        .UserName = c.String(),
                        .EmailAddress = c.String(),
                        .LoginID = c.String(),
                        .Password = c.String(),
                        .SaltValue = c.String()
                    }) _
                .PrimaryKey(Function(t) t.UserID) _
                .ForeignKey("dbo.Institution", Function(t) t.InstitutionID, cascadeDelete:=True) _
                .ForeignKey("dbo.Role", Function(t) t.RoleID, cascadeDelete:=True) _
                .Index(Function(t) t.InstitutionID) _
                .Index(Function(t) t.RoleID)

            Sql("INSERT INTO [dbo].[User](InstitutionID, RoleID, UserName, EmailAddress, LoginID, Password, SaltValue) VALUES (1, 7, 'Webuser', 'support@in-1.it', 'webuser', 'EUQkHdZ09eKSfVGv6XYNpICxPGI=', 'KgTo1Q==')")
            Sql("INSERT INTO [dbo].[User](InstitutionID, RoleID, UserName, EmailAddress, LoginID, Password, SaltValue) VALUES (1, 7, 'Systemuser', 'support@in-1.it', 'systemuser', '', '')")
            Sql("INSERT INTO [dbo].[User](InstitutionID, RoleID, UserName, EmailAddress, LoginID, Password, SaltValue) VALUES (1, 2, 'Default Administrator', 'support@in-1.it', 'dadmin', 'QW5M2hSleLWricltyhSqtCnbDYw=', 'M2yoSPA=')")
            Sql("INSERT INTO [dbo].[User](InstitutionID, RoleID, UserName, EmailAddress, LoginID, Password, SaltValue) VALUES (1, 4, 'Default User', 'support@in-1.it', 'duser', 't2MF8JRHLA9J6DuERonRhlzElPs=', 'UqIKPA==')")
            Sql("INSERT INTO [dbo].[User](InstitutionID, RoleID, UserName, EmailAddress, LoginID, Password, SaltValue) VALUES (1, 6, 'Default Infopoint', 'support@in-1.it', 'dinfopoint', 'nRx/p4JmafuhqSOPMtqv9dMpFJw=', 'MerVr4c=')")

            CreateTable(
                "dbo.MachineConfig",
                Function(c) New With
                    {
                        .ID = c.Int(nullable:=False, identity:=True),
                        .Machinename = c.String(),
                        .Configname = c.String(),
                        .Machinesetting = c.String()
                    }) _
                .PrimaryKey(Function(t) t.ID)


            CreateTable(
                "dbo.Postleitzahl",
                Function(c) New With
                    {
                        .ID = c.Int(nullable:=False, identity:=True),
                        .PLZ = c.String(),
                        .Ort = c.String(),
                        .Zusatz = c.String()
                    }) _
                .PrimaryKey(Function(t) t.ID)


            CreateTable(
                "dbo.WebCart",
                Function(c) New With
                    {
                        .WebCartID = c.Int(nullable:=False, identity:=True),
                        .Session = c.String(),
                        .Artikel = c.String(),
                        .CourseID = c.Int(),
                        .SessionTimeout = c.Decimal(precision:=18, scale:=2)
                    }) _
                .PrimaryKey(Function(t) t.WebCartID)

            CreateTable(
                "dbo.Template",
                Function(c) New With
                    {
                        .TemplateID = c.Int(nullable:=False, identity:=True),
                        .TemplateName = c.String(),
                        .Betreff = c.String(),
                        .TemplateText = c.String(),
                        .IsHtml = c.Boolean(nullable:=False)
                    }) _
                .PrimaryKey(Function(t) t.TemplateID)

            Sql("INSERT INTO [dbo].[Template](TemplateName, Betreff, TemplateText, IsHtml) VALUES ('BuchungKurs', 'Anmeldung zum Hochschulsportkurs {kursname}', 'Vielen Dank f�r die Buchung', 1)")
            Sql("INSERT INTO [dbo].[Template](TemplateName, Betreff, TemplateText, IsHtml) VALUES ('Stornierung', 'Stornierung {kursname}', '{anrede} <br> Ihre Buchung f�r den {kursname}  wurde storniert.', 1)")
            Sql("INSERT INTO [dbo].[Template](TemplateName, Betreff, TemplateText, IsHtml) VALUES ('Warteliste', 'Warteliste {kursname}', '{anrede} momentan sind alle regul�ren Pl�tze f�r die Hochschulsportveranstaltung {kursname} ausgebucht. Sie befinden sich auf der Warteliste. ', 1)")
            Sql("INSERT INTO [dbo].[Template](TemplateName, Betreff, TemplateText, IsHtml) VALUES ('RequestPasswort', 'Neues Passwort', 'Ihr Passwort wurde zur�ckgesetzt. Ihr neues Passwort lautet: {password}', 1)")


            CreateTable(
                "dbo.Waitinglist",
                Function(c) New With
                    {
                        .WaitinglistID = c.Int(nullable:=False, identity:=True),
                        .CourseID = c.Int(nullable:=False),
                        .CustomerID = c.Int(nullable:=False),
                        .Timestamp = c.DateTime(nullable:=False)
                    }) _
                .PrimaryKey(Function(t) t.WaitinglistID) _
                .ForeignKey("dbo.Course", Function(t) t.CourseID, cascadeDelete:=True) _
                .ForeignKey("dbo.Customer", Function(t) t.CustomerID, cascadeDelete:=True) _
                .Index(Function(t) t.CourseID) _
                .Index(Function(t) t.CustomerID)



        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.Waitinglist", New String() { "CustomerID" })
            DropIndex("dbo.Waitinglist", New String() { "CourseID" })
            DropIndex("dbo.User", New String() { "RoleID" })
            DropIndex("dbo.User", New String() { "InstitutionID" })
            DropIndex("dbo.PackageLog", New String() { "BillingID" })
            DropIndex("dbo.Product", New String() { "CostcenterID" })
            DropIndex("dbo.CheckinLog", New String() { "CustomerID" })
            DropIndex("dbo.Document", New String() { "CustomerID" })
            DropIndex("dbo.Checkin", New String() { "CustomerID" })
            DropIndex("dbo.LocationLocationgroup", New String() { "LocationgroupID" })
            DropIndex("dbo.LocationLocationgroup", New String() { "LocationID" })
            DropIndex("dbo.StaffContract", New String() { "CustomerID" })
            DropIndex("dbo.CourseStaff", New String() { "StaffContractID" })
            DropIndex("dbo.CourseStaff", New String() { "CustomerID" })
            DropIndex("dbo.CourseStaff", New String() { "CourseID" })
            DropIndex("dbo.Package", New String() { "CostcenterID" })
            DropIndex("dbo.PackagePrice", New String() { "PackageID" })
            DropIndex("dbo.PackagePrice", New String() { "CustomerStateID" })
            DropIndex("dbo.CoursePrice", New String() { "CustomerStateID" })
            DropIndex("dbo.CoursePrice", New String() { "CourseID" })
            DropIndex("dbo.Sport", New String() { "CustomerID" })
            DropIndex("dbo.Course", New String() { "CostcenterID" })
            DropIndex("dbo.Course", New String() { "CategoryID" })
            DropIndex("dbo.Course", New String() { "TermID" })
            DropIndex("dbo.Course", New String() { "SportID" })
            DropIndex("dbo.Course", New String() { "PartyID" })
            DropIndex("dbo.Course", New String() { "LocationID" })
            DropIndex("dbo.Course", New String() { "AnsprechpartnerID" })
            DropIndex("dbo.Course", New String() { "CourseStateID" })
            DropIndex("dbo.Course", New String() { "CourseRegistrationID" })
            DropIndex("dbo.Course", New String() { "CourseGenderID" })
            DropIndex("dbo.Course", New String() { "CourseCostID" })
            DropIndex("dbo.Course", New String() { "CourseAudienceID" })
            DropIndex("dbo.Location", New String() { "CampusID" })
            DropIndex("dbo.Appointment", New String() { "LocationID" })
            DropIndex("dbo.Appointment", New String() { "BookingID" })
            DropIndex("dbo.Booking", New String() { "CustomerID" })
            DropIndex("dbo.Booking", New String() { "CourseID" })
            DropIndex("dbo.Booking", New String() { "LocationID" })
            DropIndex("dbo.Customer", New String() { "CustomerStateID" })
            DropIndex("dbo.Contract", New String() { "CostcenterID" })
            DropIndex("dbo.Contract", New String() { "CustomerID" })
            DropIndex("dbo.Contract", New String() { "ContractRateID" })
            DropIndex("dbo.Billing", New String() { "CostcenterID" })
            DropIndex("dbo.Billing", New String() { "ProductID" })
            DropIndex("dbo.Billing", New String() { "PaymentMethodID" })
            DropIndex("dbo.Billing", New String() { "PackageID" })
            DropIndex("dbo.Billing", New String() { "CourseID" })
            DropIndex("dbo.Billing", New String() { "CustomerID" })
            DropIndex("dbo.Billing", New String() { "ContractID" })
            DropIndex("dbo.Billing", New String() { "AccountID" })
            DropForeignKey("dbo.Waitinglist", "CustomerID", "dbo.Customer")
            DropForeignKey("dbo.Waitinglist", "CourseID", "dbo.Course")
            DropForeignKey("dbo.User", "RoleID", "dbo.Role")
            DropForeignKey("dbo.User", "InstitutionID", "dbo.Institution")
            DropForeignKey("dbo.PackageLog", "BillingID", "dbo.Billing")
            DropForeignKey("dbo.Product", "CostcenterID", "dbo.Costcenter")
            DropForeignKey("dbo.CheckinLog", "CustomerID", "dbo.Customer")
            DropForeignKey("dbo.Document", "CustomerID", "dbo.Customer")
            DropForeignKey("dbo.Checkin", "CustomerID", "dbo.Customer")
            DropForeignKey("dbo.LocationLocationgroup", "LocationgroupID", "dbo.Locationgroup")
            DropForeignKey("dbo.LocationLocationgroup", "LocationID", "dbo.Location")
            DropForeignKey("dbo.StaffContract", "CustomerID", "dbo.Customer")
            DropForeignKey("dbo.CourseStaff", "StaffContractID", "dbo.StaffContract")
            DropForeignKey("dbo.CourseStaff", "CustomerID", "dbo.Customer")
            DropForeignKey("dbo.CourseStaff", "CourseID", "dbo.Course")
            DropForeignKey("dbo.Package", "CostcenterID", "dbo.Costcenter")
            DropForeignKey("dbo.PackagePrice", "PackageID", "dbo.Package")
            DropForeignKey("dbo.PackagePrice", "CustomerStateID", "dbo.CustomerState")
            DropForeignKey("dbo.CoursePrice", "CustomerStateID", "dbo.CustomerState")
            DropForeignKey("dbo.CoursePrice", "CourseID", "dbo.Course")
            DropForeignKey("dbo.Sport", "CustomerID", "dbo.Customer")
            DropForeignKey("dbo.Course", "CostcenterID", "dbo.Costcenter")
            DropForeignKey("dbo.Course", "CategoryID", "dbo.Category")
            DropForeignKey("dbo.Course", "TermID", "dbo.Term")
            DropForeignKey("dbo.Course", "SportID", "dbo.Sport")
            DropForeignKey("dbo.Course", "PartyID", "dbo.Party")
            DropForeignKey("dbo.Course", "LocationID", "dbo.Location")
            DropForeignKey("dbo.Course", "AnsprechpartnerID", "dbo.Customer")
            DropForeignKey("dbo.Course", "CourseStateID", "dbo.CourseState")
            DropForeignKey("dbo.Course", "CourseRegistrationID", "dbo.CourseRegistration")
            DropForeignKey("dbo.Course", "CourseGenderID", "dbo.CourseGender")
            DropForeignKey("dbo.Course", "CourseCostID", "dbo.CourseCost")
            DropForeignKey("dbo.Course", "CourseAudienceID", "dbo.CourseAudience")
            DropForeignKey("dbo.Location", "CampusID", "dbo.Campus")
            DropForeignKey("dbo.Appointment", "LocationID", "dbo.Location")
            DropForeignKey("dbo.Appointment", "BookingID", "dbo.Booking")
            DropForeignKey("dbo.Booking", "CustomerID", "dbo.Customer")
            DropForeignKey("dbo.Booking", "CourseID", "dbo.Course")
            DropForeignKey("dbo.Booking", "LocationID", "dbo.Location")
            DropForeignKey("dbo.Customer", "CustomerStateID", "dbo.CustomerState")
            DropForeignKey("dbo.Contract", "CostcenterID", "dbo.Costcenter")
            DropForeignKey("dbo.Contract", "CustomerID", "dbo.Customer")
            DropForeignKey("dbo.Contract", "ContractRateID", "dbo.ContractRate")
            DropForeignKey("dbo.Billing", "CostcenterID", "dbo.Costcenter")
            DropForeignKey("dbo.Billing", "ProductID", "dbo.Product")
            DropForeignKey("dbo.Billing", "PaymentMethodID", "dbo.PaymentMethod")
            DropForeignKey("dbo.Billing", "PackageID", "dbo.Package")
            DropForeignKey("dbo.Billing", "CourseID", "dbo.Course")
            DropForeignKey("dbo.Billing", "CustomerID", "dbo.Customer")
            DropForeignKey("dbo.Billing", "ContractID", "dbo.Contract")
            DropForeignKey("dbo.Billing", "AccountID", "dbo.Account")
            DropTable("dbo.Waitinglist")
            DropTable("dbo.Template")
            DropTable("dbo.WebCart")
            DropTable("dbo.Postleitzahl")
            DropTable("dbo.Options")
            DropTable("dbo.MachineConfig")
            DropTable("dbo.Role")
            DropTable("dbo.User")
            DropTable("dbo.Institution")
            DropTable("dbo.Holiday")
            DropTable("dbo.GlobalSetting")
            DropTable("dbo.EquipmentCategory")
            DropTable("dbo.Dtaus")
            DropTable("dbo.PackageLog")
            DropTable("dbo.Product")
            DropTable("dbo.PaymentMethod")
            DropTable("dbo.CheckinLog")
            DropTable("dbo.Document")
            DropTable("dbo.Checkin")
            DropTable("dbo.Equipment")
            DropTable("dbo.LocationEquipment")
            DropTable("dbo.Campus")
            DropTable("dbo.Locationgroup")
            DropTable("dbo.LocationLocationgroup")
            DropTable("dbo.StaffContract")
            DropTable("dbo.CourseStaff")
            DropTable("dbo.Costcenter")
            DropTable("dbo.Package")
            DropTable("dbo.PackagePrice")
            DropTable("dbo.CustomerState")
            DropTable("dbo.CoursePrice")
            DropTable("dbo.Category")
            DropTable("dbo.Term")
            DropTable("dbo.Sport")
            DropTable("dbo.Party")
            DropTable("dbo.CourseState")
            DropTable("dbo.CourseRegistration")
            DropTable("dbo.CourseGender")
            DropTable("dbo.CourseCost")
            DropTable("dbo.CourseAudience")
            DropTable("dbo.Course")
            DropTable("dbo.Location")
            DropTable("dbo.Appointment")
            DropTable("dbo.Booking")
            DropTable("dbo.Customer")
            DropTable("dbo.ContractRate")
            DropTable("dbo.Contract")
            DropTable("dbo.Billing")
            DropTable("dbo.Account")
        End Sub
    End Class
End Namespace
