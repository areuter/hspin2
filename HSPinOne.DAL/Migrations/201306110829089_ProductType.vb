Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class ProductType
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.ProductType",
                Function(c) New With
                    {
                        .ProductTypeID = c.Int(nullable := False, identity := True),
                        .ProductTypeName = c.String()
                    }) _
                .PrimaryKey(Function(t) t.ProductTypeID)

            Sql("INSERT INTO dbo.ProductType(ProductTypeName) VALUES ('Sportkurse')")
            Sql("INSERT INTO dbo.ProductType(ProductTypeName) VALUES ('Punktekarten')")
            Sql("INSERT INTO dbo.ProductType(ProductTypeName) VALUES ('Mitgliedschaften')")
            Sql("INSERT INTO dbo.ProductType(ProductTypeName) VALUES ('Bankgeb�hren')")


            AddColumn("dbo.Course", "ProductTypeID", Function(c) c.Int(nullable:=False, defaultValue:=1))
            AddColumn("dbo.Package", "ProductTypeID", Function(c) c.Int(nullable:=False, defaultValue:=1))
            AddForeignKey("dbo.Course", "ProductTypeID", "dbo.ProductType", "ProductTypeID", cascadeDelete := True)
            AddForeignKey("dbo.Package", "ProductTypeID", "dbo.ProductType", "ProductTypeID", cascadeDelete := True)
            CreateIndex("dbo.Course", "ProductTypeID")
            CreateIndex("dbo.Package", "ProductTypeID")
            DropColumn("dbo.Package", "Warengruppe")
        End Sub
        
        Public Overrides Sub Down()
            AddColumn("dbo.Package", "Warengruppe", Function(c) c.Int(nullable := False))
            DropIndex("dbo.Package", New String() { "ProductTypeID" })
            DropIndex("dbo.Course", New String() { "ProductTypeID" })
            DropForeignKey("dbo.Package", "ProductTypeID", "dbo.ProductType")
            DropForeignKey("dbo.Course", "ProductTypeID", "dbo.ProductType")
            DropColumn("dbo.Package", "ProductTypeID")
            DropColumn("dbo.Course", "ProductTypeID")
            DropTable("dbo.ProductType")
        End Sub
    End Class
End Namespace
