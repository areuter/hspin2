Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class LocationEquipmentConstraint
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddForeignKey("dbo.LocationEquipment", "LocationID", "dbo.Location", "LocationID", cascadeDelete := True)
            AddForeignKey("dbo.LocationEquipment", "EquipmentID", "dbo.Equipment", "EquipmentID", cascadeDelete := True)
            CreateIndex("dbo.LocationEquipment", "LocationID")
            CreateIndex("dbo.LocationEquipment", "EquipmentID")
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.LocationEquipment", New String() { "EquipmentID" })
            DropIndex("dbo.LocationEquipment", New String() { "LocationID" })
            DropForeignKey("dbo.LocationEquipment", "EquipmentID", "dbo.Equipment")
            DropForeignKey("dbo.LocationEquipment", "LocationID", "dbo.Location")
        End Sub
    End Class
End Namespace
