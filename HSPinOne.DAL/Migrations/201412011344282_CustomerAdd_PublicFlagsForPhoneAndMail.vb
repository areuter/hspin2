Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class CustomerAdd_PublicFlagsForPhoneAndMail
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Customer", "Telefon1IsPublic", Function(c) c.Boolean(nullable:=False, defaultValue:=False))
            AddColumn("dbo.Customer", "Telefon2IsPublic", Function(c) c.Boolean(nullable:=False, defaultValue:=False))
            AddColumn("dbo.Customer", "Telefon3IsPublic", Function(c) c.Boolean(nullable:=False, defaultValue:=False))
            AddColumn("dbo.Customer", "Mail1IsPublic", Function(c) c.Boolean(nullable:=False, defaultValue:=True))
            AddColumn("dbo.Customer", "Mail2IsPublic", Function(c) c.Boolean(nullable:=False, defaultValue:=False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Customer", "Mail2IsPublic")
            DropColumn("dbo.Customer", "Mail1IsPublic")
            DropColumn("dbo.Customer", "Telefon3IsPublic")
            DropColumn("dbo.Customer", "Telefon2IsPublic")
            DropColumn("dbo.Customer", "Telefon1IsPublic")
        End Sub
    End Class
End Namespace
