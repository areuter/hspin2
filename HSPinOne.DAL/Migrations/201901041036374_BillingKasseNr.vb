Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class BillingKasseNr
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Billing", "KasseNr", Function(c) c.Int())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Billing", "KasseNr")
        End Sub
    End Class
End Namespace
