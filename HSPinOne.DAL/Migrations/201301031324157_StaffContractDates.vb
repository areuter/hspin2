Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class StaffContractDates
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AlterColumn("dbo.StaffContract", "Vertragsdruck", Function(c) c.DateTime())
            AlterColumn("dbo.StaffContract", "Vertragzurueck", Function(c) c.DateTime())
        End Sub
        
        Public Overrides Sub Down()
            AlterColumn("dbo.StaffContract", "Vertragzurueck", Function(c) c.DateTime(nullable := False))
            AlterColumn("dbo.StaffContract", "Vertragsdruck", Function(c) c.DateTime(nullable := False))
        End Sub
    End Class
End Namespace
