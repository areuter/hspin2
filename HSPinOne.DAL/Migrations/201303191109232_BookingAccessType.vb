Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class BookingAccessType
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Booking", "AccessType", Function(c) c.Int(nullable:=False, defaultValue:=0))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Booking", "AccessType")
        End Sub
    End Class
End Namespace
