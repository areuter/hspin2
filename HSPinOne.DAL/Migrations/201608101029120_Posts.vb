Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Posts
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Post",
                Function(c) New With
                    {
                        .PostID = c.Int(nullable := False, identity := True),
                        .Postdate = c.DateTime(nullable := False),
                        .Posttitle = c.String(),
                        .Postcontent = c.String(),
                        .Postexcerpt = c.String(),
                        .Showfrom = c.DateTime(nullable := False),
                        .Showtill = c.DateTime(nullable := False),
                        .Posttype = c.String(maxLength := 20),
                        .Postparent = c.Int(nullable := False),
                        .Postorder = c.Int(nullable := False),
                        .Poststatus = c.Int(nullable := False),
                        .Tags = c.String(),
                        .Image = c.String(),
                        .Link = c.String(),
                        .Postauthor_UserID = c.Int()
                    }) _
                .PrimaryKey(Function(t) t.PostID) _
                .ForeignKey("dbo.User", Function(t) t.Postauthor_UserID) _
                .Index(Function(t) t.Posttype) _
                .Index(Function(t) t.Poststatus) _
                .Index(Function(t) t.Postauthor_UserID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.Post", "Postauthor_UserID", "dbo.User")
            DropIndex("dbo.Post", New String() { "Postauthor_UserID" })
            DropIndex("dbo.Post", New String() { "Poststatus" })
            DropIndex("dbo.Post", New String() { "Posttype" })
            DropTable("dbo.Post")
        End Sub
    End Class
End Namespace
