Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class CourseWebLock
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Course", "WebLock", Function(c) c.Boolean(nullable:=False, defaultValue:=False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Course", "WebLock")
        End Sub
    End Class
End Namespace
