Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class CustomerMeta
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.CustomerMeta",
                Function(c) New With
                    {
                        .CustomerMetaID = c.Int(nullable := False, identity := True),
                        .CustomerID = c.Int(nullable := False),
                        .MetaKey = c.String(maxLength := 255),
                        .MetaValue = c.String()
                    }) _
                .PrimaryKey(Function(t) t.CustomerMetaID) _
                .ForeignKey("dbo.Customer", Function(t) t.CustomerID, cascadeDelete := True) _
                .Index(Function(t) t.CustomerID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.CustomerMeta", "CustomerID", "dbo.Customer")
            DropIndex("dbo.CustomerMeta", New String() { "CustomerID" })
            DropTable("dbo.CustomerMeta")
        End Sub
    End Class
End Namespace
