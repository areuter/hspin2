Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class TermShowfromtill
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Term", "Showfrom", Function(c) c.DateTime(nullable:=False, defaultValueSql:="GETDATE()"))
            AddColumn("dbo.Term", "Showtill", Function(c) c.DateTime(nullable:=False, defaultValueSql:="GETDATE()"))

            Sql("UPDATE Term SET Showfrom = DATEADD(day, -14, Semesterbeginn), Showtill = Semesterende")
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Term", "Showtill")
            DropColumn("dbo.Term", "Showfrom")
        End Sub
    End Class
End Namespace
