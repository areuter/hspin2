Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class BillingModCheckin
        Inherits DbMigration
    
        Public Overrides Sub Up()
            DropForeignKey("dbo.Billing", "CustomerID", "dbo.Customer")
            DropIndex("dbo.Billing", New String() { "CustomerID" })
            AddColumn("dbo.Billing", "CheckinID", Function(c) c.Int())
            AddColumn("dbo.Billing", "IBAN", Function(c) c.String())
            AlterColumn("dbo.Billing", "CustomerID", Function(c) c.Int())
            AddForeignKey("dbo.Billing", "CustomerID", "dbo.Customer", "CustomerID")
            AddForeignKey("dbo.Billing", "CheckinID", "dbo.Checkin", "CheckinID")
            CreateIndex("dbo.Billing", "CustomerID")
            CreateIndex("dbo.Billing", "CheckinID")
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.Billing", New String() { "CheckinID" })
            DropIndex("dbo.Billing", New String() { "CustomerID" })
            DropForeignKey("dbo.Billing", "CheckinID", "dbo.Checkin")
            DropForeignKey("dbo.Billing", "CustomerID", "dbo.Customer")
            AlterColumn("dbo.Billing", "CustomerID", Function(c) c.Int(nullable := False))
            DropColumn("dbo.Billing", "IBAN")
            DropColumn("dbo.Billing", "CheckinID")
            CreateIndex("dbo.Billing", "CustomerID")
            AddForeignKey("dbo.Billing", "CustomerID", "dbo.Customer", "CustomerID", cascadeDelete := True)
        End Sub
    End Class
End Namespace
