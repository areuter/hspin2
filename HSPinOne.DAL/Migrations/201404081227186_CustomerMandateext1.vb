Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class CustomerMandateext1
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Customer", "Mandatstyp", Function(c) c.Byte(nullable:=False, defaultValue:=0))
            AddColumn("dbo.Customer", "Mandatscounter", Function(c) c.Int(nullable:=False, defaultValue:=0))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Customer", "Mandatscounter")
            DropColumn("dbo.Customer", "Mandatstyp")
        End Sub
    End Class
End Namespace
