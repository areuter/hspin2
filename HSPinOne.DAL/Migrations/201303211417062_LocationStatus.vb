Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class LocationStatus
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Location", "Active", Function(c) c.Boolean(nullable:=False, defaultValue:=True))
            AddColumn("dbo.Location", "PublicVisible", Function(c) c.Boolean(nullable:=False, defaultValue:=True))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Location", "PublicVisible")
            DropColumn("dbo.Location", "Active")
        End Sub
    End Class
End Namespace
