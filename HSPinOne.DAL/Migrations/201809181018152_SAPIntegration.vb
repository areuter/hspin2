Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class SAPIntegration
        Inherits DbMigration
    
        Public Overrides Sub Up()
            DropForeignKey("dbo.Billing", "AppointmentID", "dbo.Appointment")
            DropIndex("dbo.Billing", New String() { "AppointmentID" })
            AddColumn("dbo.Billing", "Innenauftrag", Function(c) c.String(maxLength := 30))
            AddColumn("dbo.Billing", "Sachkonto", Function(c) c.String(maxLength := 30))
            AddColumn("dbo.PackagePrice", "CostcenterID", Function(c) c.Int())
            AddColumn("dbo.PackagePrice", "Innenauftrag", Function(c) c.String())
            AddColumn("dbo.PackagePrice", "Sachkonto", Function(c) c.String())
            AddColumn("dbo.ContractRatePrice", "CostcenterID", Function(c) c.Int())
            AddColumn("dbo.ContractRatePrice", "Innenauftrag", Function(c) c.String())
            AddColumn("dbo.ContractRatePrice", "Sachkonto", Function(c) c.String())
            CreateIndex("dbo.PackagePrice", "CostcenterID")
            CreateIndex("dbo.ContractRatePrice", "CostcenterID")
            AddForeignKey("dbo.PackagePrice", "CostcenterID", "dbo.Costcenter", "CostcenterID")
            AddForeignKey("dbo.ContractRatePrice", "CostcenterID", "dbo.Costcenter", "CostcenterID")
            DropColumn("dbo.Billing", "Kontonummer")
            DropColumn("dbo.Billing", "BLZ")
            DropColumn("dbo.Billing", "IBAN")
            DropColumn("dbo.Billing", "AppointmentID")
        End Sub
        
        Public Overrides Sub Down()
            AddColumn("dbo.Billing", "AppointmentID", Function(c) c.Int())
            AddColumn("dbo.Billing", "IBAN", Function(c) c.String())
            AddColumn("dbo.Billing", "BLZ", Function(c) c.Int())
            AddColumn("dbo.Billing", "Kontonummer", Function(c) c.Long())
            DropForeignKey("dbo.ContractRatePrice", "CostcenterID", "dbo.Costcenter")
            DropForeignKey("dbo.PackagePrice", "CostcenterID", "dbo.Costcenter")
            DropIndex("dbo.ContractRatePrice", New String() { "CostcenterID" })
            DropIndex("dbo.PackagePrice", New String() { "CostcenterID" })
            DropColumn("dbo.ContractRatePrice", "Sachkonto")
            DropColumn("dbo.ContractRatePrice", "Innenauftrag")
            DropColumn("dbo.ContractRatePrice", "CostcenterID")
            DropColumn("dbo.PackagePrice", "Sachkonto")
            DropColumn("dbo.PackagePrice", "Innenauftrag")
            DropColumn("dbo.PackagePrice", "CostcenterID")
            DropColumn("dbo.Billing", "Sachkonto")
            DropColumn("dbo.Billing", "Innenauftrag")
            CreateIndex("dbo.Billing", "AppointmentID")
            AddForeignKey("dbo.Billing", "AppointmentID", "dbo.Appointment", "AppointmentID")
        End Sub
    End Class
End Namespace
