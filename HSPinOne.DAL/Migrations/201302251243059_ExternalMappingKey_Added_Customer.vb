Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class ExternalMappingKey_Added_Customer
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Customer", "ExternalMappingKey", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Customer", "ExternalMappingKey")
        End Sub
    End Class
End Namespace
