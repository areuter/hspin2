Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class CourseStaffOutpayment
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.CourseStaff", "OutpaymentDate", Function(c) c.DateTime())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.CourseStaff", "OutpaymentDate")
        End Sub
    End Class
End Namespace
