Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class PackageIsActive
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Package", "IsActive", Function(c) c.Boolean(nullable:=False, defaultValue:=True))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Package", "IsActive")
        End Sub
    End Class
End Namespace
