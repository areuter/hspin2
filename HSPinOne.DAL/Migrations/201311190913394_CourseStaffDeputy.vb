Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class CourseStaffDeputy
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.CourseStaff", "IsDeputy", Function(c) c.Boolean(nullable:=False, defaultValue:=False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.CourseStaff", "IsDeputy")
        End Sub
    End Class
End Namespace
