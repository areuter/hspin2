Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class SerialNameColumnAndDefaults
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Serial", "SerialName", Function(c) c.String(nullable:=False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Serial", "SerialName")
        End Sub
    End Class
End Namespace
