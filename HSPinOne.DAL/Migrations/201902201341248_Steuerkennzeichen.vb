Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Steuerkennzeichen
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Billing", "Steuerkennzeichen", Function(c) c.String(maxLength := 20))
            AddColumn("dbo.CoursePrice", "Steuerkennzeichen", Function(c) c.String(maxLength := 20))
            AddColumn("dbo.PackagePrice", "Steuerkennzeichen", Function(c) c.String(maxLength := 20))
            AddColumn("dbo.PriceGroupPrice", "Steuerkennzeichen", Function(c) c.String(maxLength := 20))
            AddColumn("dbo.ContractRatePrice", "Steuerkennzeichen", Function(c) c.String(maxLength := 20))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.ContractRatePrice", "Steuerkennzeichen")
            DropColumn("dbo.PriceGroupPrice", "Steuerkennzeichen")
            DropColumn("dbo.PackagePrice", "Steuerkennzeichen")
            DropColumn("dbo.CoursePrice", "Steuerkennzeichen")
            DropColumn("dbo.Billing", "Steuerkennzeichen")
        End Sub
    End Class
End Namespace
