Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class LocationExtras
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Location", "ImageList", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Location", "ImageList")
        End Sub
    End Class
End Namespace
