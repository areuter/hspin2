Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class BookingCategory
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Booking", "CategoryID", Function(c) c.Int())
            CreateIndex("dbo.Booking", "CategoryID")
            AddForeignKey("dbo.Booking", "CategoryID", "dbo.Category", "CategoryID")
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.Booking", "CategoryID", "dbo.Category")
            DropIndex("dbo.Booking", New String() { "CategoryID" })
            DropColumn("dbo.Booking", "CategoryID")
        End Sub
    End Class
End Namespace
