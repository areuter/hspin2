Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class LocationBookinggroup
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.LocationBookinggroup",
                Function(c) New With
                    {
                        .LocationBookinggroupID = c.Int(nullable:=False, identity:=True),
                        .Groupname = c.String()
                    }) _
                .PrimaryKey(Function(t) t.LocationBookinggroupID)

            Sql("INSERT INTO dbo.LocationBookinggroup (Groupname) VALUES ('Standard')")
            
        End Sub
        
        Public Overrides Sub Down()
            DropTable("dbo.LocationBookinggroup")
        End Sub
    End Class
End Namespace
