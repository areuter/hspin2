Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class CustomerImagepublic
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Customer", "ImageIsPublic", Function(c) c.Boolean(nullable:=False, defaultValue:=False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Customer", "ImageIsPublic")
        End Sub
    End Class
End Namespace
