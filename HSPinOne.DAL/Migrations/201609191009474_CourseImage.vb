Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class CourseImage
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Course", "CourseImageUID", Function(c) c.Guid())
            CreateIndex("dbo.Course", "CourseImageUID")
            AddForeignKey("dbo.Course", "CourseImageUID", "dbo.Document", "DocumentUID")
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.Course", "CourseImageUID", "dbo.Document")
            DropIndex("dbo.Course", New String() { "CourseImageUID" })
            DropColumn("dbo.Course", "CourseImageUID")
        End Sub
    End Class
End Namespace
