Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class LocationBooking
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.LocationCost",
                Function(c) New With
                    {
                        .LocationCostID = c.Int(nullable := False, identity := True),
                        .Preis = c.Decimal(nullable := False, precision := 18, scale := 2),
                        .IsActive = c.Boolean(nullable := False),
                        .Vorausbuchungstage = c.Int(nullable := False),
                        .CustomerState_CustomerStateID = c.Int(),
                        .Location_LocationID = c.Int()
                    }) _
                .PrimaryKey(Function(t) t.LocationCostID) _
                .ForeignKey("dbo.CustomerState", Function(t) t.CustomerState_CustomerStateID) _
                .ForeignKey("dbo.Location", Function(t) t.Location_LocationID) _
                .Index(Function(t) t.CustomerState_CustomerStateID) _
                .Index(Function(t) t.Location_LocationID)
            
            CreateTable(
                "dbo.LocationHour",
                Function(c) New With
                    {
                        .LocationHourID = c.Int(nullable := False, identity := True),
                        .ValidFrom = c.DateTime(nullable := False),
                        .ValidTill = c.DateTime(nullable := False),
                        .Weekday = c.Byte(nullable := False),
                        .Starttime = c.DateTime(nullable := False),
                        .Endtime = c.DateTime(nullable := False),
                        .Location_LocationID = c.Int()
                    }) _
                .PrimaryKey(Function(t) t.LocationHourID) _
                .ForeignKey("dbo.Location", Function(t) t.Location_LocationID) _
                .Index(Function(t) t.Location_LocationID)
            
            AddColumn("dbo.Location", "SlotMin", Function(c) c.String())
            AddColumn("dbo.Location", "Template_TemplateID", Function(c) c.Int())
            CreateIndex("dbo.Location", "Template_TemplateID")
            AddForeignKey("dbo.Location", "Template_TemplateID", "dbo.Template", "TemplateID")
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.Location", "Template_TemplateID", "dbo.Template")
            DropForeignKey("dbo.LocationHour", "Location_LocationID", "dbo.Location")
            DropForeignKey("dbo.LocationCost", "Location_LocationID", "dbo.Location")
            DropForeignKey("dbo.LocationCost", "CustomerState_CustomerStateID", "dbo.CustomerState")
            DropIndex("dbo.LocationHour", New String() { "Location_LocationID" })
            DropIndex("dbo.LocationCost", New String() { "Location_LocationID" })
            DropIndex("dbo.LocationCost", New String() { "CustomerState_CustomerStateID" })
            DropIndex("dbo.Location", New String() { "Template_TemplateID" })
            DropColumn("dbo.Location", "Template_TemplateID")
            DropColumn("dbo.Location", "SlotMin")
            DropTable("dbo.LocationHour")
            DropTable("dbo.LocationCost")
        End Sub
    End Class
End Namespace
