Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class CustomerInstitution
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.CustomerInstitution",
                Function(c) New With
                    {
                        .CustomerInstitutionID = c.Int(nullable:=False, identity:=True),
                        .CustomerInstitutionName = c.String(),
                        .IsActive = c.Boolean(nullable:=False, defaultValue:=True)
                    }) _
                .PrimaryKey(Function(t) t.CustomerInstitutionID)
            
            AddColumn("dbo.Customer", "CustomerInstitutionID", Function(c) c.Int())
            CreateIndex("dbo.Customer", "CustomerInstitutionID")
            AddForeignKey("dbo.Customer", "CustomerInstitutionID", "dbo.CustomerInstitution", "CustomerInstitutionID")
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.Customer", "CustomerInstitutionID", "dbo.CustomerInstitution")
            DropIndex("dbo.Customer", New String() { "CustomerInstitutionID" })
            DropColumn("dbo.Customer", "CustomerInstitutionID")
            DropTable("dbo.CustomerInstitution")
        End Sub
    End Class
End Namespace
