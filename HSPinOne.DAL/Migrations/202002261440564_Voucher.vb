Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Voucher
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Vouchercode",
                Function(c) New With
                    {
                        .VouchercodeID = c.Int(nullable := False, identity := True),
                        .VoucherID = c.Int(nullable := False),
                        .Code = c.String(maxLength := 50),
                        .Usedat = c.DateTime(),
                        .Created = c.DateTime(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.VouchercodeID) _
                .ForeignKey("dbo.Voucher", Function(t) t.VoucherID, cascadeDelete := True) _
                .Index(Function(t) t.VoucherID)
            
            CreateTable(
                "dbo.Voucher",
                Function(c) New With
                    {
                        .VoucherID = c.Int(nullable := False, identity := True),
                        .Title = c.String(maxLength := 100),
                        .Description = c.String(),
                        .Validfrom = c.DateTime(nullable := False),
                        .Validtill = c.DateTime(nullable := False),
                        .Actiontype = c.Int(nullable := False),
                        .Discount = c.Decimal(nullable := False, precision := 18, scale := 2),
                        .Username = c.String(maxLength := 50)
                    }) _
                .PrimaryKey(Function(t) t.VoucherID)
            
            AddColumn("dbo.Billing", "BruttoOriginal", Function(c) c.Decimal(nullable := False, precision := 18, scale := 2))
            AddColumn("dbo.Billing", "VouchercodeID", Function(c) c.Int())
            Sql("DBCC CHECKIDENT ('Voucher', RESEED, 10000)")
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.Vouchercode", "VoucherID", "dbo.Voucher")
            DropIndex("dbo.Vouchercode", New String() { "VoucherID" })
            DropColumn("dbo.Billing", "VouchercodeID")
            DropColumn("dbo.Billing", "BruttoOriginal")
            DropTable("dbo.Voucher")
            DropTable("dbo.Vouchercode")
        End Sub
    End Class
End Namespace
