Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class Checkin_CustomerState
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Checkin", "CustomerStateID", Function(c) c.Int(nullable:=False, defaultValue:=1))
            AddForeignKey("dbo.Checkin", "CustomerStateID", "dbo.CustomerState", "CustomerStateID", cascadeDelete := True)
            CreateIndex("dbo.Checkin", "CustomerStateID")
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.Checkin", New String() { "CustomerStateID" })
            DropForeignKey("dbo.Checkin", "CustomerStateID", "dbo.CustomerState")
            DropColumn("dbo.Checkin", "CustomerStateID")
        End Sub
    End Class
End Namespace
