Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Hash
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Billing", "Hash", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Billing", "Hash")
        End Sub
    End Class
End Namespace
