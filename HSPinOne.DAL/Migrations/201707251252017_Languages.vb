Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Languages
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Language",
                Function(c) New With
                    {
                        .ISOCode = c.String(nullable := False, maxLength := 2),
                        .English = c.String(),
                        .Flag = c.String()
                    }) _
                .PrimaryKey(Function(t) t.ISOCode)

            AddColumn("dbo.Customer", "Languages", Function(c) c.String())
            AddColumn("dbo.Course", "Language", Function(c) c.String(defaultValue:="de"))

            Sql("INSERT INTO Language(ISOCode, English) VALUES ('de','German')")
            Sql("INSERT INTO Language(ISOCode, English) VALUES ('en','English')")
            Sql("INSERT INTO Language(ISOCode, English) VALUES ('fr','French')")
            Sql("INSERT INTO Language(ISOCode, English) VALUES ('es','Spanish')")
            Sql("UPDATE Course SET Language ='de' WHERE Anmeldungbis > GETDATE()")
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Course", "Language")
            DropColumn("dbo.Customer", "Languages")
            DropTable("dbo.Language")
        End Sub
    End Class
End Namespace
