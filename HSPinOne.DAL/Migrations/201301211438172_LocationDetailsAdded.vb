Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class LocationDetailsAdded
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Location", "Capacity", Function(c) c.Int())
            AddColumn("dbo.Location", "Area", Function(c) c.Int())
            AddColumn("dbo.Location", "Contact", Function(c) c.Int())
            AddColumn("dbo.Location", "Key", Function(c) c.Boolean(nullable:=False, defaultValue:=True))
            AddColumn("dbo.Role", "ParentID", Function(c) c.Int())

            Sql("UPDATE dbo.Role SET ParentID =  2 WHERE RoleName = 'System Administrator'")
            Sql("UPDATE dbo.Role SET ParentID =  3 WHERE RoleName = 'Administrator'")
            Sql("UPDATE dbo.Role SET ParentID =  4 WHERE RoleName = 'User Extended'")
            Sql("UPDATE dbo.Role SET ParentID =  5 WHERE RoleName = 'User'")
            Sql("UPDATE dbo.Role SET ParentID =  6 WHERE RoleName = 'Infopoint Extended'")
            Sql("UPDATE dbo.Role SET ParentID =  7 WHERE RoleName = 'Infopoint'")
            
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Role", "ParentID")
            DropColumn("dbo.Location", "Key")
            DropColumn("dbo.Location", "Contact")
            DropColumn("dbo.Location", "Area")
            DropColumn("dbo.Location", "Capacity")
        End Sub
    End Class
End Namespace
