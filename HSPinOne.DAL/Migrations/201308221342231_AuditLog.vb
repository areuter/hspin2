Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class AuditLog
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.AuditLog",
                Function(c) New With
                    {
                        .AuditLogID = c.Guid(nullable := False),
                        .UserID = c.String(nullable := False, maxLength := 50),
                        .EventDateUTC = c.DateTime(nullable := False),
                        .EventType = c.String(nullable := False, maxLength := 1),
                        .TableName = c.String(nullable := False, maxLength := 100),
                        .RecordID = c.String(nullable := False, maxLength := 100),
                        .ColumnName = c.String(nullable := False, maxLength := 100),
                        .OriginalValue = c.String(),
                        .NewValue = c.String()
                    }) _
                .PrimaryKey(Function(t) t.AuditLogID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropTable("dbo.AuditLog")
        End Sub
    End Class
End Namespace
