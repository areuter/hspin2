Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class BillingSteuerprozent
        Inherits DbMigration
    
        Public Overrides Sub Up()
            Sql("UPDATE Billing SET Steuerprozent = 0")
            AlterColumn("dbo.Billing", "Steuerprozent", Function(c) c.Decimal(nullable:=False, precision:=18, scale:=2, defaultValue:=0))
        End Sub
        
        Public Overrides Sub Down()
            AlterColumn("dbo.Billing", "Steuerprozent", Function(c) c.Decimal(precision := 18, scale := 2))
        End Sub
    End Class
End Namespace
