Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class WaitinglistAdd_MailSendTracking
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Waitinglist", "LastMailAt", Function(c) c.DateTime())
            AddColumn("dbo.Waitinglist", "MailsSend", Function(c) c.Int(nullable:=False, defaultValue:=0))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Waitinglist", "MailsSend")
            DropColumn("dbo.Waitinglist", "LastMailAt")
        End Sub
    End Class
End Namespace
