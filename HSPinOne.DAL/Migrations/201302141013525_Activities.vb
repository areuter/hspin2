Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class Activities
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Activity",
                Function(c) New With
                    {
                        .ActivityID = c.Int(nullable := False, identity := True),
                        .ActivityType = c.Int(nullable := False),
                        .Direction = c.Int(nullable := False),
                        .Comment = c.String(),
                        .Zeit = c.DateTime(nullable := False),
                        .UserID = c.Int(),
                        .CustomerID = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ActivityID) _
                .ForeignKey("dbo.User", Function(t) t.UserID) _
                .ForeignKey("dbo.Customer", Function(t) t.CustomerID, cascadeDelete := True) _
                .Index(Function(t) t.UserID) _
                .Index(Function(t) t.CustomerID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.Activity", New String() { "CustomerID" })
            DropIndex("dbo.Activity", New String() { "UserID" })
            DropForeignKey("dbo.Activity", "CustomerID", "dbo.Customer")
            DropForeignKey("dbo.Activity", "UserID", "dbo.User")
            DropTable("dbo.Activity")
        End Sub
    End Class
End Namespace
