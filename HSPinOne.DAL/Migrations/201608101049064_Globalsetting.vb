Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Globalsetting
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.GlobalSetting", "Preferencename", Function(c) c.String(maxLength := 100))
            AddColumn("dbo.GlobalSetting", "Typ", Function(c) c.String(maxLength := 250))
            CreateIndex("dbo.GlobalSetting", "Preferencename")
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.GlobalSetting", New String() { "Preferencename" })
            DropColumn("dbo.GlobalSetting", "Typ")
            DropColumn("dbo.GlobalSetting", "Preferencename")
        End Sub
    End Class
End Namespace
