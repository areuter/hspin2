Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class LocationHourBemerkung
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.LocationHour", "Bemerkung", Function(c) c.String(maxLength := 200))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.LocationHour", "Bemerkung")
        End Sub
    End Class
End Namespace
