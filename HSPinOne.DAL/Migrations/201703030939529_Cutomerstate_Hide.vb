Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Cutomerstate_Hide
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.CustomerState", "Hide", Function(c) c.Boolean(nullable:=False, defaultValue:=False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.CustomerState", "Hide")
        End Sub
    End Class
End Namespace
