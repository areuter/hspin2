Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class Checkin_Infotext
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Checkin", "Infotext", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Checkin", "Infotext")
        End Sub
    End Class
End Namespace
