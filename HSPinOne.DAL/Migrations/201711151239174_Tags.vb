Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Tags
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.CourseTag",
                Function(c) New With
                    {
                        .CourseTagID = c.Int(nullable := False, identity := True),
                        .TagID = c.Int(nullable := False),
                        .CourseID = c.Int(nullable := False),
                        .Tagorder = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.CourseTagID) _
                .ForeignKey("dbo.Course", Function(t) t.CourseID, cascadeDelete := True) _
                .ForeignKey("dbo.Tag", Function(t) t.TagID, cascadeDelete := True) _
                .Index(Function(t) t.TagID) _
                .Index(Function(t) t.CourseID)

            CreateTable(
                "dbo.Tag",
                Function(c) New With
                    {
                        .TagID = c.Int(nullable:=False, identity:=True),
                        .Tagname = c.String(),
                        .Slug = c.String(),
                        .Taggroup = c.Int(nullable:=False, defaultValue:=0),
                        .Tagparent = c.Int(nullable:=False, defaultValue:=0)
                    }) _
                .PrimaryKey(Function(t) t.TagID)


            Sql("INSERT INTO Tag(Tagname, Slug) VALUES('Tag1','tag1')")

        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.CourseTag", "TagID", "dbo.Tag")
            DropForeignKey("dbo.CourseTag", "CourseID", "dbo.Course")
            DropIndex("dbo.CourseTag", New String() { "CourseID" })
            DropIndex("dbo.CourseTag", New String() { "TagID" })
            DropTable("dbo.Tag")
            DropTable("dbo.CourseTag")
        End Sub
    End Class
End Namespace
