Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class SportCategory
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.SportCategory",
                Function(c) New With
                    {
                        .SportCategoryID = c.Int(nullable:=False, identity:=True),
                        .SportCategoryName = c.String(nullable:=False, maxLength:=250)
                    }) _
                .PrimaryKey(Function(t) t.SportCategoryID)

            Sql("INSERT INTO dbo.SportCategory(SportCategoryName) VALUES ('Standard')")

            AddColumn("dbo.Sport", "SportCategoryID", Function(c) c.Int(nullable:=False, defaultValue:=1))
            AddForeignKey("dbo.Sport", "SportCategoryID", "dbo.SportCategory", "SportCategoryID", cascadeDelete := True)
            CreateIndex("dbo.Sport", "SportCategoryID")
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.Sport", New String() { "SportCategoryID" })
            DropForeignKey("dbo.Sport", "SportCategoryID", "dbo.SportCategory")
            DropColumn("dbo.Sport", "SportCategoryID")
            DropTable("dbo.SportCategory")
        End Sub
    End Class
End Namespace
