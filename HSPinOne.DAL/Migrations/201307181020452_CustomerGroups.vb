Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class CustomerGroups
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.CustomerGroup",
                Function(c) New With
                    {
                        .CustomerGroupID = c.Int(nullable := False, identity := True),
                        .Groupname = c.String(nullable := False, maxLength := 250)
                    }) _
                .PrimaryKey(Function(t) t.CustomerGroupID)
            
            CreateTable(
                "dbo.CustomerGroupCustomer",
                Function(c) New With
                    {
                        .CustomerGroup_CustomerGroupID = c.Int(nullable := False),
                        .Customer_CustomerID = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) New With { t.CustomerGroup_CustomerGroupID, t.Customer_CustomerID }) _
                .ForeignKey("dbo.CustomerGroup", Function(t) t.CustomerGroup_CustomerGroupID, cascadeDelete := True) _
                .ForeignKey("dbo.Customer", Function(t) t.Customer_CustomerID, cascadeDelete := True) _
                .Index(Function(t) t.CustomerGroup_CustomerGroupID) _
                .Index(Function(t) t.Customer_CustomerID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.CustomerGroupCustomer", New String() { "Customer_CustomerID" })
            DropIndex("dbo.CustomerGroupCustomer", New String() { "CustomerGroup_CustomerGroupID" })
            DropForeignKey("dbo.CustomerGroupCustomer", "Customer_CustomerID", "dbo.Customer")
            DropForeignKey("dbo.CustomerGroupCustomer", "CustomerGroup_CustomerGroupID", "dbo.CustomerGroup")
            DropTable("dbo.CustomerGroupCustomer")
            DropTable("dbo.CustomerGroup")
        End Sub
    End Class
End Namespace
