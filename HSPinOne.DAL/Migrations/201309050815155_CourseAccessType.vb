Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class CourseAccessType
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Course", "AccessType", Function(c) c.Int(nullable := False))
            DropTable("dbo.Postleitzahl")
        End Sub
        
        Public Overrides Sub Down()
            CreateTable(
                "dbo.Postleitzahl",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .PLZ = c.String(),
                        .Ort = c.String(),
                        .Zusatz = c.String()
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
            DropColumn("dbo.Course", "AccessType")
        End Sub
    End Class
End Namespace
