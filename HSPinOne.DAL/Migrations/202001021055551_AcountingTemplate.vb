Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class AcountingTemplate
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.AccountingTemplate",
                Function(c) New With
                    {
                        .AccountingTemplateID = c.Int(nullable:=False, identity:=True),
                        .Shortcode = c.String(),
                        .Bezeichnung = c.String(),
                        .Bereich = c.String(),
                        .Kostenstelle = c.String(),
                        .Innenauftrag = c.String(),
                        .Sachkonto = c.String(),
                        .SteuerkennzeichenAR = c.String(),
                        .TaxID = c.Int(nullable:=False)
                    }) _
                .PrimaryKey(Function(t) t.AccountingTemplateID) _
                .ForeignKey("dbo.Tax", Function(t) t.TaxID, cascadeDelete:=True) _
                .Index(Function(t) t.TaxID)

            Sql("INSERT INTO Tax(TaxName,Taxpercent) VALUES('Regelsteuersatz',19)")
            Sql("INSERT INTO Tax(TaxName,Taxpercent) VALUES('Erm. Steuersatz',7)")
            Sql("INSERT INTO Tax(TaxName,Taxpercent) VALUES('Durchschnittsatz �24 Nr.3',10.7)")
            Sql("INSERT INTO Tax(TaxName,Taxpercent) VALUES('Durchschnittsatz �24 Nr.1',5.5)")
            Sql("INSERT INTO Tax(TaxName,Taxpercent) VALUES('Steuerfrei',0)")

            AddColumn("dbo.Billing", "AccountingTemplateID", Function(c) c.Int())
            AddColumn("dbo.CoursePrice", "AccountingTemplateID", Function(c) c.Int())
            AddColumn("dbo.PackagePrice", "AccountingTemplateID", Function(c) c.Int())
            AddColumn("dbo.PriceGroupPrice", "AccountingTemplateID", Function(c) c.Int())
            AddColumn("dbo.ContractRatePrice", "AccountingTemplateID", Function(c) c.Int())
            CreateIndex("dbo.Billing", "AccountingTemplateID")
            CreateIndex("dbo.CoursePrice", "AccountingTemplateID")
            CreateIndex("dbo.PackagePrice", "AccountingTemplateID")
            CreateIndex("dbo.PriceGroupPrice", "AccountingTemplateID")
            CreateIndex("dbo.ContractRatePrice", "AccountingTemplateID")
            AddForeignKey("dbo.Billing", "AccountingTemplateID", "dbo.AccountingTemplate", "AccountingTemplateID")
            AddForeignKey("dbo.CoursePrice", "AccountingTemplateID", "dbo.AccountingTemplate", "AccountingTemplateID")
            AddForeignKey("dbo.PackagePrice", "AccountingTemplateID", "dbo.AccountingTemplate", "AccountingTemplateID")
            AddForeignKey("dbo.PriceGroupPrice", "AccountingTemplateID", "dbo.AccountingTemplate", "AccountingTemplateID")
            AddForeignKey("dbo.ContractRatePrice", "AccountingTemplateID", "dbo.AccountingTemplate", "AccountingTemplateID")
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.ContractRatePrice", "AccountingTemplateID", "dbo.AccountingTemplate")
            DropForeignKey("dbo.PriceGroupPrice", "AccountingTemplateID", "dbo.AccountingTemplate")
            DropForeignKey("dbo.PackagePrice", "AccountingTemplateID", "dbo.AccountingTemplate")
            DropForeignKey("dbo.CoursePrice", "AccountingTemplateID", "dbo.AccountingTemplate")
            DropForeignKey("dbo.Billing", "AccountingTemplateID", "dbo.AccountingTemplate")
            DropForeignKey("dbo.AccountingTemplate", "TaxID", "dbo.Tax")
            DropIndex("dbo.ContractRatePrice", New String() { "AccountingTemplateID" })
            DropIndex("dbo.PriceGroupPrice", New String() { "AccountingTemplateID" })
            DropIndex("dbo.PackagePrice", New String() { "AccountingTemplateID" })
            DropIndex("dbo.CoursePrice", New String() { "AccountingTemplateID" })
            DropIndex("dbo.Billing", New String() { "AccountingTemplateID" })
            DropIndex("dbo.AccountingTemplate", New String() { "TaxID" })
            DropColumn("dbo.ContractRatePrice", "AccountingTemplateID")
            DropColumn("dbo.PriceGroupPrice", "AccountingTemplateID")
            DropColumn("dbo.PackagePrice", "AccountingTemplateID")
            DropColumn("dbo.CoursePrice", "AccountingTemplateID")
            DropColumn("dbo.Billing", "AccountingTemplateID")
            DropTable("dbo.AccountingTemplate")
        End Sub
    End Class
End Namespace
