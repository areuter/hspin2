Imports System
Imports System.Data.Entity
Imports System.Data.Entity.Migrations
Imports System.Linq

Namespace Migrations


    '' Neu Migration mit ... in der Package Manager Console
    '  Add-Migration NamederMigration -ProjectName HSPinOne.DAL -StartUpProjectName HSPinOne.WPF.Shell

    '' Manuelles Update der Datenbank mit Debug Infos aus der Package Manager Console
    '' Update-Database -Verbose -ProjectName HSPinOne.DAL -StartUpProjectName HSPinOne.WPF.Shell

    Friend NotInheritable Class Configuration
        Inherits DbMigrationsConfiguration(Of in1Entities)

        Public Sub New()
            AutomaticMigrationsEnabled = False
            AutomaticMigrationDataLossAllowed = False

        End Sub

        Protected Overrides Sub Seed(context As in1Entities)
            '  This method will be called after migrating to the latest version.

            '  You can use the DbSet(Of T).AddOrUpdate() helper extension method 
            '  to avoid creating duplicate seed data. E.g.
            '
            '    context.People.AddOrUpdate(
            '       Function(c) c.FullName,
            '       New Customer() With {.FullName = "Andrew Peters"},
            '       New Customer() With {.FullName = "Brice Lambson"},
            '       New Customer() With {.FullName = "Rowan Miller"})

            'context.Accounts.AddOrUpdate(New Account() With {.AccountID = 1600, .AccountName = "Barkasse 1"},
            '                             New Account() With {.AccountID = 1800, .AccountName = "Bankkonto 1"}
            '                             )



            'context.CourseAudiences.AddOrUpdate(New CourseAudience() With {.CourseAudienceName = "Anf�nger"},
            '                                    New CourseAudience() With {.CourseAudienceName = "Fortgeschrittene"}
            '                                    )


            'context.CourseCosts.AddOrUpdate(New CourseCost() With {.CourseCostName = "Kostenlos"},
            '                                New CourseCost() With {.CourseCostName = "Kursgeb�hr"}
            '                                )


            'context.CourseGenders.AddOrUpdate(New CourseGender() With {.CourseGenderName = "Frauen"},
            '                                  New CourseGender() With {.CourseGenderName = "M�nner"},
            '                                  New CourseGender() With {.CourseGenderName = "Frauen + M�nner"}
            '                                  )
            'context.CourseRegistrations.AddOrUpdate(New CourseRegistration With {.CourseRegistrationName = "nicht anmeldepflichtig", .Chart = False, .RegistrationDates = False},
            '                                        New CourseRegistration With {.CourseRegistrationName = "anmeldepflichtig", .Chart = False, .RegistrationDates = False},
            '                                        New CourseRegistration With {.CourseRegistrationName = "siehe Kursbeschreibung", .Chart = False, .RegistrationDates = False}
            '                                        )


            'context.CourseStates.AddOrUpdate(New CourseState With {.CourseStatename = "ungepr�ft", .CourseStatecolor = "#FFFF0000"},
            '                                 New CourseState With {.CourseStatename = "in Bearbeitung", .CourseStatecolor = "#FFFF8040"},
            '                                 New CourseState With {.CourseStatename = "gepr�ft", .CourseStatecolor = "FF00FF00"}
            '                                 )

            'context.Parties.AddOrUpdate(New Party With {.Partyname = "Hochschulsport", .Partycolor = "#3D86B8"},
            '                            New Party With {.Partyname = "Sportwissenschaft", .Partycolor = "#FF8C17"},
            '                            New Party With {.Partyname = "Externe", .Partycolor = "#AF418E"}
            '                            )


            'context.Categories.AddOrUpdate(New Category With {.CategoryName = "unkategorisiert", .CategoryColor = "#00FFFFFF"})


            'context.CustomerStates.AddOrUpdate(New CustomerState With {.CustomerStatename = "Student"},
            '                                   New CustomerState With {.CustomerStatename = "Bedienstet"},
            '                                   New CustomerState With {.CustomerStatename = "Extern"}
            '                                   )
            'context.Costcenters.AddOrUpdate(New Costcenter With {.Costcentername = "Kostenstelle 1"})

            'context.PaymentMethods.AddOrUpdate(New PaymentMethod With {.PaymentMethodname = "Bar"},
            '                                   New PaymentMethod With {.PaymentMethodname = "Lastschrift"},
            '                                   New PaymentMethod With {.PaymentMethodname = "�berweisung"}
            '                                   )

            'context.GlobalSettings.AddOrUpdate(New GlobalSetting With {.Einstellung = "datafolder", .Beschreibung = "Datenordner"},
            '                                   New GlobalSetting With {.Einstellung = "feedurl", .Beschreibung = "URL des RSS Feeds"},
            '                                   New GlobalSetting With {.Einstellung = "hspname", .Beschreibung = "Name der Einrichtung"},
            '                                   New GlobalSetting With {.Einstellung = "hspstr", .Beschreibung = "Stra�e"},
            '                                   New GlobalSetting With {.Einstellung = "hsport", .Beschreibung = "PLZ und Ort"},
            '                                   New GlobalSetting With {.Einstellung = "hspbundesland", .Beschreibung = "Bundesland"},
            '                                   New GlobalSetting With {.Einstellung = "hspphone", .Beschreibung = "Telefonnummer"},
            '                                   New GlobalSetting With {.Einstellung = "hspemail", .Beschreibung = "E-Mail Adresse"},
            '                                   New GlobalSetting With {.Einstellung = "kontocheckurl", .Beschreibung = "URL f�r die Kontopr�froutine", .Wert = "http://www.sport.uni-goettingen.de/bav/scripts/check.php"},
            '                                   New GlobalSetting With {.Einstellung = "defaultaccount", .Beschreibung = "Standardkonto", .Wert = 1800}
            '                                   )






        End Sub

    End Class

End Namespace
