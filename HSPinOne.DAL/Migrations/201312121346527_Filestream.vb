Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class Filestream
        Inherits DbMigration
    
        Public Overrides Sub Up()
            DropForeignKey("dbo.Document", "CustomerID", "dbo.Customer")
            DropIndex("dbo.Document", New String() { "CustomerID" })
            AddColumn("dbo.Document", "DocumentUID", Function(c) c.Guid(nullable := False, identity := True))
            AddColumn("dbo.Document", "Extension", Function(c) c.String(maxLength := 8))
            AddColumn("dbo.Document", "DocumentData", Function(c) c.Binary())
            AddColumn("dbo.Document", "DocumentType", Function(c) c.Int(nullable := False))
            AddColumn("dbo.Activity", "DocumentUID", Function(c) c.Guid())
            AlterColumn("dbo.Document", "CustomerID", Function(c) c.Int())
            DropPrimaryKey("dbo.Document", New String() { "DocumentID" })
            AddPrimaryKey("dbo.Document", "DocumentUID")
            AddForeignKey("dbo.Document", "CustomerID", "dbo.Customer", "CustomerID")
            AddForeignKey("dbo.Activity", "DocumentUID", "dbo.Document", "DocumentUID")
            CreateIndex("dbo.Document", "CustomerID")
            CreateIndex("dbo.Activity", "DocumentUID")
            DropColumn("dbo.Document", "DocumentID")
        End Sub
        
        Public Overrides Sub Down()
            AddColumn("dbo.Document", "DocumentID", Function(c) c.Int(nullable := False, identity := True))
            DropIndex("dbo.Activity", New String() { "DocumentUID" })
            DropIndex("dbo.Document", New String() { "CustomerID" })
            DropForeignKey("dbo.Activity", "DocumentUID", "dbo.Document")
            DropForeignKey("dbo.Document", "CustomerID", "dbo.Customer")
            DropPrimaryKey("dbo.Document", New String() { "DocumentUID" })
            AddPrimaryKey("dbo.Document", "DocumentID")
            AlterColumn("dbo.Document", "CustomerID", Function(c) c.Int(nullable := False))
            DropColumn("dbo.Activity", "DocumentUID")
            DropColumn("dbo.Document", "DocumentType")
            DropColumn("dbo.Document", "DocumentData")
            DropColumn("dbo.Document", "Extension")
            DropColumn("dbo.Document", "DocumentUID")
            CreateIndex("dbo.Document", "CustomerID")
            AddForeignKey("dbo.Document", "CustomerID", "dbo.Customer", "CustomerID", cascadeDelete := True)
        End Sub
    End Class
End Namespace
