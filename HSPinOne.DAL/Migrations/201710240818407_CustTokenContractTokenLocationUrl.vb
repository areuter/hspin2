Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class CustTokenContractTokenLocationUrl
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Customer", "PasswordToken", Function(c) c.String())
            AddColumn("dbo.Location", "IframeUrl", Function(c) c.String())
            AddColumn("dbo.Contract", "AgbAccepted", Function(c) c.Boolean(nullable:=False, defaultValue:=False))
            AddColumn("dbo.Contract", "Token", Function(c) c.String(maxLength := 250))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Contract", "Token")
            DropColumn("dbo.Contract", "AgbAccepted")
            DropColumn("dbo.Location", "IframeUrl")
            DropColumn("dbo.Customer", "PasswordToken")
        End Sub
    End Class
End Namespace
