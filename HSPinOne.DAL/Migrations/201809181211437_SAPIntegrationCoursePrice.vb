Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class SAPIntegrationCoursePrice
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.CoursePrice", "CostcenterID", Function(c) c.Int())
            AddColumn("dbo.CoursePrice", "Innenauftrag", Function(c) c.String())
            AddColumn("dbo.CoursePrice", "Sachkonto", Function(c) c.String())
            CreateIndex("dbo.CoursePrice", "CostcenterID")
            AddForeignKey("dbo.CoursePrice", "CostcenterID", "dbo.Costcenter", "CostcenterID")
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.CoursePrice", "CostcenterID", "dbo.Costcenter")
            DropIndex("dbo.CoursePrice", New String() { "CostcenterID" })
            DropColumn("dbo.CoursePrice", "Sachkonto")
            DropColumn("dbo.CoursePrice", "Innenauftrag")
            DropColumn("dbo.CoursePrice", "CostcenterID")
        End Sub
    End Class
End Namespace
