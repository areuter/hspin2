Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class CustomerRole
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Customer", "RoleID", Function(c) c.Int())
            CreateIndex("dbo.Customer", "RoleID")
            AddForeignKey("dbo.Customer", "RoleID", "dbo.Role", "RoleID")
            Sql("INSERT INTO Role(RoleName) VALUES ('Kunde')")
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.Customer", "RoleID", "dbo.Role")
            DropIndex("dbo.Customer", New String() { "RoleID" })
            DropColumn("dbo.Customer", "RoleID")
        End Sub
    End Class
End Namespace
