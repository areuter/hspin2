Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class BillingTracker
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Billing", "Created", Function(c) c.DateTime(nullable:=False, defaultValueSql:="GETDATE()"))
            AddColumn("dbo.Billing", "CreatedBy", Function(c) c.String())
            AddColumn("dbo.Billing", "LastModified", Function(c) c.DateTime())
            AddColumn("dbo.Billing", "LastModifiedBy", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Billing", "LastModifiedBy")
            DropColumn("dbo.Billing", "LastModified")
            DropColumn("dbo.Billing", "CreatedBy")
            DropColumn("dbo.Billing", "Created")
        End Sub
    End Class
End Namespace
