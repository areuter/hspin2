Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class CustomerCompany
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Customer", "Company", Function(c) c.String(maxLength := 100))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Customer", "Company")
        End Sub
    End Class
End Namespace
