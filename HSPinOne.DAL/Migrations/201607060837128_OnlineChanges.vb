Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class OnlineChanges
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Appointment", "TNCount", Function(c) c.Int(nullable:=False, defaultValue:=0))
            AddColumn("dbo.Booking", "BookingType", Function(c) c.Int(nullable:=False, defaultValue:=0))
            AddColumn("dbo.Location", "StornoHours", Function(c) c.Int(nullable:=False, defaultValue:=-1))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Location", "StornoHours")
            DropColumn("dbo.Booking", "BookingType")
            DropColumn("dbo.Appointment", "TNCount")
        End Sub
    End Class
End Namespace
