Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class CourseDTUntil
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Course", "DTUntil", Function(c) c.DateTime(nullable:=False))

        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Course", "DTUntil")
        End Sub
    End Class
End Namespace
