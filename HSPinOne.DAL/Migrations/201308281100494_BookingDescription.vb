Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class BookingDescription
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Booking", "Description", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Booking", "Description")
        End Sub
    End Class
End Namespace
