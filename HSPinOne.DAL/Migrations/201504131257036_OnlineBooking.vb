Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class OnlineBooking
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Location", "BookingGroup", Function(c) c.Int(nullable:=False, defaultValue:=0))
            AddColumn("dbo.LocationHour", "OnlineBooking", Function(c) c.Boolean(nullable:=False, defaultValue:=False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.LocationHour", "OnlineBooking")
            DropColumn("dbo.Location", "BookingGroup")
        End Sub
    End Class
End Namespace
