Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class BillingPaymentInformation
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Billing", "PaymentInformation", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Billing", "PaymentInformation")
        End Sub
    End Class
End Namespace
