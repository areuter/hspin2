Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class KSTextern
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Billing", "KSTextern", Function(c) c.String(maxLength := 30))
            AddColumn("dbo.CoursePrice", "KSTextern", Function(c) c.String(maxLength := 30))
            AddColumn("dbo.PackagePrice", "KSTextern", Function(c) c.String(maxLength := 30))
            AddColumn("dbo.PriceGroupPrice", "KSTextern", Function(c) c.String(maxLength := 30))
            AddColumn("dbo.ContractRatePrice", "KSTextern", Function(c) c.String(maxLength := 30))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.ContractRatePrice", "KSTextern")
            DropColumn("dbo.PriceGroupPrice", "KSTextern")
            DropColumn("dbo.PackagePrice", "KSTextern")
            DropColumn("dbo.CoursePrice", "KSTextern")
            DropColumn("dbo.Billing", "KSTextern")
        End Sub
    End Class
End Namespace
