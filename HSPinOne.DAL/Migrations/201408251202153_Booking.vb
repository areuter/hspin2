Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class Booking
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddForeignKey("dbo.Billing", "BookingID", "dbo.Booking", "BookingID")
            CreateIndex("dbo.Billing", "BookingID")
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.Billing", New String() { "BookingID" })
            DropForeignKey("dbo.Billing", "BookingID", "dbo.Booking")
        End Sub
    End Class
End Namespace
