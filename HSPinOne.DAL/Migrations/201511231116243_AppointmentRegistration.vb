Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class AppointmentRegistration
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.AppointmentCustomers",
                Function(c) New With
                    {
                        .AppointmentCustomersID = c.Int(nullable := False, identity := True),
                        .AppointmentID = c.Int(nullable := False),
                        .CustomerID = c.Int(nullable := False),
                        .BookingDate = c.DateTime(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.AppointmentCustomersID) _
                .ForeignKey("dbo.Appointment", Function(t) t.AppointmentID, cascadeDelete := True) _
                .ForeignKey("dbo.Customer", Function(t) t.CustomerID, cascadeDelete := True) _
                .Index(Function(t) t.AppointmentID) _
                .Index(Function(t) t.CustomerID)
            
            AddColumn("dbo.Course", "RegisterAppointments", Function(c) c.Boolean(nullable:=False, defaultValue:=False))
            AddColumn("dbo.Course", "RegisterAppointmentDays", Function(c) c.Int(nullable:=False, defaultValue:=0))
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.AppointmentCustomers", "CustomerID", "dbo.Customer")
            DropForeignKey("dbo.AppointmentCustomers", "AppointmentID", "dbo.Appointment")
            DropIndex("dbo.AppointmentCustomers", New String() { "CustomerID" })
            DropIndex("dbo.AppointmentCustomers", New String() { "AppointmentID" })
            DropColumn("dbo.Course", "RegisterAppointmentDays")
            DropColumn("dbo.Course", "RegisterAppointments")
            DropTable("dbo.AppointmentCustomers")
        End Sub
    End Class
End Namespace
