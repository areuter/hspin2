Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class LocationHourType
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.LocationHour", "HourType", Function(c) c.Int(nullable:=False, defaultValue:=0))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.LocationHour", "HourType")
        End Sub
    End Class
End Namespace
