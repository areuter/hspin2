Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class AccountFix
        Inherits DbMigration
    
        Public Overrides Sub Up()
            DropForeignKey("dbo.Billing", "AccountID", "dbo.Account")
            DropPrimaryKey("dbo.Account")
            AlterColumn("dbo.Account", "AccountID", Function(c) c.Int(nullable := False))
            AddPrimaryKey("dbo.Account", "AccountID")
            AddForeignKey("dbo.Billing", "AccountID", "dbo.Account", "AccountID", cascadeDelete:=False)
            Sql("INSERT INTO Account(AccountID, AccountName, Kasse) VALUES (1460,'Geldtransit', 0)")
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.Billing", "AccountID", "dbo.Account")
            DropPrimaryKey("dbo.Account")
            AlterColumn("dbo.Account", "AccountID", Function(c) c.Int(nullable := False, identity := True))
            AddPrimaryKey("dbo.Account", "AccountID")
            AddForeignKey("dbo.Billing", "AccountID", "dbo.Account", "AccountID", cascadeDelete := True)
        End Sub
    End Class
End Namespace
