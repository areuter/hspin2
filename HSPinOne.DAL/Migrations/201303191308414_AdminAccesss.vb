Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class AdminAccesss
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.AdminAccess",
                Function(c) New With
                    {
                        .AdminAccessID = c.Int(nullable := False, identity := True),
                        .CustomerID = c.Int(nullable := False),
                        .Bemerkung = c.String()
                    }) _
                .PrimaryKey(Function(t) t.AdminAccessID) _
                .ForeignKey("dbo.Customer", Function(t) t.CustomerID, cascadeDelete := True) _
                .Index(Function(t) t.CustomerID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.AdminAccess", New String() { "CustomerID" })
            DropForeignKey("dbo.AdminAccess", "CustomerID", "dbo.Customer")
            DropTable("dbo.AdminAccess")
        End Sub
    End Class
End Namespace
