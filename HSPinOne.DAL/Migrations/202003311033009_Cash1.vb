Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Cash1
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.BillingReceipt",
                Function(c) New With
                    {
                        .BillingReceiptID = c.Int(nullable := False, identity := True),
                        .KasseNr = c.String(),
                        .KasseBelegNr = c.Int(nullable := False),
                        .Created = c.DateTime(nullable := False),
                        .ReceiptType = c.Int(nullable := False),
                        .Brutto = c.Decimal(nullable := False, precision := 18, scale := 2)
                    }) _
                .PrimaryKey(Function(t) t.BillingReceiptID)
            
            CreateTable(
                "dbo.CashRegister",
                Function(c) New With
                    {
                        .CashRegisterID = c.Int(nullable := False, identity := True),
                        .KasseNr = c.Int(nullable := False),
                        .LastReceipt = c.Int(nullable := False),
                        .UseTse = c.Boolean(nullable := False),
                        .DefaultAccountID = c.Int(nullable := False),
                        .DefaultPaymentMethodID = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.CashRegisterID)
            
            CreateTable(
                "dbo.Tse",
                Function(c) New With
                    {
                        .TseID = c.Int(nullable := False, identity := True),
                        .BillingID = c.Int(),
                        .BillingReceiptID = c.Int(),
                        .Transactionnr = c.Int(nullable := False),
                        .SignaturCounter = c.Int(nullable := False),
                        .Signature = c.String(),
                        .Start = c.DateTime(nullable := False),
                        .Finish = c.DateTime()
                    }) _
                .PrimaryKey(Function(t) t.TseID)
            
            AddColumn("dbo.Billing", "NewRecord", Function(c) c.Boolean(nullable := False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Billing", "NewRecord")
            DropTable("dbo.Tse")
            DropTable("dbo.CashRegister")
            DropTable("dbo.BillingReceipt")
        End Sub
    End Class
End Namespace
