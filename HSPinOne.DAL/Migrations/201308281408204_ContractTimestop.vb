Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class ContractTimestop
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.ContractTimestop",
                Function(c) New With
                    {
                        .ContractTimestopID = c.Int(nullable := False, identity := True),
                        .ContractID = c.Int(nullable := False),
                        .TStart = c.DateTime(nullable := False),
                        .TEnd = c.DateTime(nullable := False),
                        .Reason = c.String(maxLength := 150)
                    }) _
                .PrimaryKey(Function(t) t.ContractTimestopID) _
                .ForeignKey("dbo.Contract", Function(t) t.ContractID, cascadeDelete := True) _
                .Index(Function(t) t.ContractID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropIndex("dbo.ContractTimestop", New String() { "ContractID" })
            DropForeignKey("dbo.ContractTimestop", "ContractID", "dbo.Contract")
            DropTable("dbo.ContractTimestop")
        End Sub
    End Class
End Namespace
