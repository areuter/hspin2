Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class CustomerPhoto
        Inherits DbMigration
    
        Public Overrides Sub Up()
            DropForeignKey("dbo.Document", "CustomerID", "dbo.Customer")
            DropIndex("dbo.Document", New String() { "CustomerID" })
            AddColumn("dbo.Customer", "DocumentUID", Function(c) c.Guid())
            AddForeignKey("dbo.Customer", "DocumentUID", "dbo.Document", "DocumentUID")
            CreateIndex("dbo.Customer", "DocumentUID")
            DropColumn("dbo.Customer", "HasImage")
        End Sub
        
        Public Overrides Sub Down()
            AddColumn("dbo.Customer", "HasImage", Function(c) c.Boolean(nullable := False))
            DropIndex("dbo.Customer", New String() { "DocumentUID" })
            DropForeignKey("dbo.Customer", "DocumentUID", "dbo.Document")
            DropColumn("dbo.Customer", "DocumentUID")
            CreateIndex("dbo.Document", "CustomerID")
            AddForeignKey("dbo.Document", "CustomerID", "dbo.Customer", "CustomerID")
        End Sub
    End Class
End Namespace
