﻿Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Tse3
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.BillingReceipt", "PaymentMethodID", Function(c) c.Int(nullable := False))
            AddColumn("dbo.BillingReceipt", "PaymentInformation", Function(c) c.String())
            CreateIndex("dbo.BillingReceipt", "PaymentMethodID")
            AddForeignKey("dbo.BillingReceipt", "PaymentMethodID", "dbo.PaymentMethod", "PaymentMethodID", cascadeDelete := True)
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.BillingReceipt", "PaymentMethodID", "dbo.PaymentMethod")
            DropIndex("dbo.BillingReceipt", New String() { "PaymentMethodID" })
            DropColumn("dbo.BillingReceipt", "PaymentInformation")
            DropColumn("dbo.BillingReceipt", "PaymentMethodID")
        End Sub
    End Class
End Namespace
