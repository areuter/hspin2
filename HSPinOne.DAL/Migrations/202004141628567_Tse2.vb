﻿Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Tse2
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.BillingReceipt", "BonTyp", Function(c) c.String(maxLength := 50))
            AddColumn("dbo.BillingReceipt", "GVTyp", Function(c) c.String(maxLength := 50))
            AddColumn("dbo.BillingReceipt", "CashClosingID", Function(c) c.Int(nullable := False))
            CreateIndex("dbo.BillingReceipt", "CashClosingID")
            AddForeignKey("dbo.BillingReceipt", "CashClosingID", "dbo.CashClosing", "CashClosingID", cascadeDelete := True)
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.BillingReceipt", "CashClosingID", "dbo.CashClosing")
            DropIndex("dbo.BillingReceipt", New String() { "CashClosingID" })
            DropColumn("dbo.BillingReceipt", "CashClosingID")
            DropColumn("dbo.BillingReceipt", "GVTyp")
            DropColumn("dbo.BillingReceipt", "BonTyp")
        End Sub
    End Class
End Namespace
