Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class BillingPackagecredit
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Billing", "Packagecredit", Function(c) c.Int())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Billing", "Packagecredit")
        End Sub

    End Class
End Namespace
