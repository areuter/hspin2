Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class LocationBookfor
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Location", "Bookfor", Function(c) c.String(maxLength := 100))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Location", "Bookfor")
        End Sub
    End Class
End Namespace
