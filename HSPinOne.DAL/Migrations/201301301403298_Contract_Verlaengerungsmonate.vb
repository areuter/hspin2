Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class Contract_Verlaengerungsmonate
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AlterColumn("dbo.Contract", "Verlaengerungsmonate", Function(c) c.Int(nullable:=False, defaultValue:=0))
        End Sub
        
        Public Overrides Sub Down()
            AlterColumn("dbo.Contract", "Verlaengerungsmonate", Function(c) c.Int())
        End Sub
    End Class
End Namespace
