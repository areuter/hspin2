Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class CustomerMandat
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Customer", "Mandatsdatum", Function(c) c.DateTime())
            AddColumn("dbo.Customer", "LetzterEinzug", Function(c) c.DateTime())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Customer", "LetzterEinzug")
            DropColumn("dbo.Customer", "Mandatsdatum")
        End Sub
    End Class
End Namespace
