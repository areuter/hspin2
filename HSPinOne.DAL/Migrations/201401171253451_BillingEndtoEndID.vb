Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class BillingEndtoEndID
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Billing", "EndToEnd", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Billing", "EndToEnd")
        End Sub
    End Class
End Namespace
