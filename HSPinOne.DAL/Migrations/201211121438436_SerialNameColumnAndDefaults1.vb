Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class SerialNameColumnAndDefaults1
        Inherits DbMigration
    
        Public Overrides Sub Up()
            Sql("INSERT INTO dbo.Serial(SerialName) VALUES ('Basic')")
            Sql("INSERT INTO dbo.Serial(SerialName) VALUES ('Fitness')")
            Sql("INSERT INTO dbo.Serial(SerialName) VALUES ('Turnstile')")
            Sql("INSERT INTO dbo.Serial(SerialName) VALUES ('Document')")
            Sql("INSERT INTO dbo.Serial(SerialName) VALUES ('Outlook')")
            Sql("INSERT INTO dbo.Serial(SerialName) VALUES ('Online-News')")
            Sql("INSERT INTO dbo.Serial(SerialName) VALUES ('Online-StaffCommitment')")
            Sql("INSERT INTO dbo.Serial(SerialName) VALUES ('Online-StaffInfo')")
            Sql("INSERT INTO dbo.Serial(SerialName) VALUES ('Rent')")
            Sql("INSERT INTO dbo.Serial(SerialName) VALUES ('DigitalSignage')")
            Sql("INSERT INTO dbo.Serial(SerialName) VALUES ('QualityManagement')")
        End Sub
        
        Public Overrides Sub Down()
        End Sub
    End Class
End Namespace
