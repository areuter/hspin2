Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class BlockedCustomer
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Customer", "Blocked", Function(c) c.Boolean(nullable:=False, defaultValue:=False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Customer", "Blocked")
        End Sub
    End Class
End Namespace
