Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class ContractRateTemplates
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.ContractRate", "VertragsbeginnTemplateID", Function(c) c.Int())
            AddColumn("dbo.ContractRate", "KuendigungTemplateID", Function(c) c.Int())
            CreateIndex("dbo.ContractRate", "VertragsbeginnTemplateID")
            CreateIndex("dbo.ContractRate", "KuendigungTemplateID")
            AddForeignKey("dbo.ContractRate", "KuendigungTemplateID", "dbo.Template", "TemplateID")
            AddForeignKey("dbo.ContractRate", "VertragsbeginnTemplateID", "dbo.Template", "TemplateID")
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.ContractRate", "VertragsbeginnTemplateID", "dbo.Template")
            DropForeignKey("dbo.ContractRate", "KuendigungTemplateID", "dbo.Template")
            DropIndex("dbo.ContractRate", New String() { "KuendigungTemplateID" })
            DropIndex("dbo.ContractRate", New String() { "VertragsbeginnTemplateID" })
            DropColumn("dbo.ContractRate", "KuendigungTemplateID")
            DropColumn("dbo.ContractRate", "VertragsbeginnTemplateID")
        End Sub
    End Class
End Namespace
