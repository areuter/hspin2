Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class ContractPrices
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.ContractRatePrice",
                Function(c) New With
                    {
                        .ContractRatePriceID = c.Int(nullable:=False, identity:=True),
                        .CustomerStateID = c.Int(nullable:=False),
                        .ContractRateID = c.Int(nullable:=False),
                        .Preis = c.Decimal(nullable:=False, precision:=18, scale:=2),
                        .TaxPercent = c.Decimal(nullable:=False, precision:=18, scale:=2)
                    }) _
                .PrimaryKey(Function(t) t.ContractRatePriceID) _
                .ForeignKey("dbo.ContractRate", Function(t) t.ContractRateID, cascadeDelete:=True) _
                .ForeignKey("dbo.CustomerState", Function(t) t.CustomerStateID, cascadeDelete:=True) _
                .Index(Function(t) t.CustomerStateID) _
                .Index(Function(t) t.ContractRateID)
            
            AddColumn("dbo.CourseCost", "Description", Function(c) c.String())
            AddColumn("dbo.ContractRate", "IsOnlineAvailable", Function(c) c.Boolean(nullable:=False, defaultValue:=True))
            AddColumn("dbo.ContractRate", "CostcenterID", Function(c) c.Int(nullable:=False, defaultValue:=1))
            AddColumn("dbo.ContractRate", "Startgebuehr", Function(c) c.Decimal(nullable := False, precision := 18, scale := 2))
            AddColumn("dbo.ContractRate", "Description", Function(c) c.String())
            AddColumn("dbo.ContractRate", "IsActive", Function(c) c.Boolean(nullable:=False, defaultValue:=True))
            CreateIndex("dbo.ContractRate", "CostcenterID")
            AddForeignKey("dbo.ContractRate", "CostcenterID", "dbo.Costcenter", "CostcenterID", cascadeDelete:=False)
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.ContractRate", "CostcenterID", "dbo.Costcenter")
            DropForeignKey("dbo.ContractRatePrice", "CustomerStateID", "dbo.CustomerState")
            DropForeignKey("dbo.ContractRatePrice", "ContractRateID", "dbo.ContractRate")
            DropIndex("dbo.ContractRatePrice", New String() { "ContractRateID" })
            DropIndex("dbo.ContractRatePrice", New String() { "CustomerStateID" })
            DropIndex("dbo.ContractRate", New String() { "CostcenterID" })
            DropColumn("dbo.ContractRate", "IsActive")
            DropColumn("dbo.ContractRate", "Description")
            DropColumn("dbo.ContractRate", "Startgebuehr")
            DropColumn("dbo.ContractRate", "CostcenterID")
            DropColumn("dbo.ContractRate", "IsOnlineAvailable")
            DropColumn("dbo.CourseCost", "Description")
            DropTable("dbo.ContractRatePrice")
        End Sub
    End Class
End Namespace
