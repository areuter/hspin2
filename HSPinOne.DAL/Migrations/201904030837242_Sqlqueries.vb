Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Sqlqueries
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Sqlquery",
                Function(c) New With
                    {
                        .SqlqueryID = c.Int(nullable := False, identity := True),
                        .Bezeichnung = c.String(maxLength := 50),
                        .Beschreibung = c.String(),
                        .Sql = c.String(),
                        .Input = c.String()
                    }) _
                .PrimaryKey(Function(t) t.SqlqueryID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropTable("dbo.Sqlquery")
        End Sub
    End Class
End Namespace
