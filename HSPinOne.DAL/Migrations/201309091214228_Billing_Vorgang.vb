Imports System
Imports System.Data.Entity.Migrations

Namespace Migrations
    Public Partial Class Billing_Vorgang
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Billing", "Vorgang", Function(c) c.Guid(nullable:=False, defaultValue:=New Guid))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Billing", "Vorgang")
        End Sub
    End Class
End Namespace
