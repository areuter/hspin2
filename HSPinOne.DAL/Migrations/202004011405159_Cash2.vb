Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Cash2
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Billing", "BillingReceiptID", Function(c) c.Int())
            AddColumn("dbo.PaymentMethod", "AccountID", Function(c) c.Int())
            AddColumn("dbo.PaymentMethod", "IsActive", Function(c) c.Boolean(nullable:=False, defaultValue:=True))
            AddColumn("dbo.BillingReceipt", "Creator", Function(c) c.String(maxLength := 50))
            CreateIndex("dbo.Billing", "BillingReceiptID")
            AddForeignKey("dbo.Billing", "BillingReceiptID", "dbo.BillingReceipt", "BillingReceiptID")
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.Billing", "BillingReceiptID", "dbo.BillingReceipt")
            DropIndex("dbo.Billing", New String() { "BillingReceiptID" })
            DropColumn("dbo.BillingReceipt", "Creator")
            DropColumn("dbo.PaymentMethod", "IsActive")
            DropColumn("dbo.PaymentMethod", "AccountID")
            DropColumn("dbo.Billing", "BillingReceiptID")
        End Sub
    End Class
End Namespace
