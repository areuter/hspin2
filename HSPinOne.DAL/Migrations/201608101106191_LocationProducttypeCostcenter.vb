Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class LocationProducttypeCostcenter
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Location", "Costcenter_CostcenterID", Function(c) c.Int())
            AddColumn("dbo.Location", "ProductType_ProductTypeID", Function(c) c.Int())
            CreateIndex("dbo.Location", "Costcenter_CostcenterID")
            CreateIndex("dbo.Location", "ProductType_ProductTypeID")
            AddForeignKey("dbo.Location", "Costcenter_CostcenterID", "dbo.Costcenter", "CostcenterID")
            AddForeignKey("dbo.Location", "ProductType_ProductTypeID", "dbo.ProductType", "ProductTypeID")
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.Location", "ProductType_ProductTypeID", "dbo.ProductType")
            DropForeignKey("dbo.Location", "Costcenter_CostcenterID", "dbo.Costcenter")
            DropIndex("dbo.Location", New String() { "ProductType_ProductTypeID" })
            DropIndex("dbo.Location", New String() { "Costcenter_CostcenterID" })
            DropColumn("dbo.Location", "ProductType_ProductTypeID")
            DropColumn("dbo.Location", "Costcenter_CostcenterID")
        End Sub
    End Class
End Namespace
