Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class PriceGroups
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.PriceGroup",
                Function(c) New With
                    {
                        .PriceGroupID = c.Int(nullable := False, identity := True),
                        .PriceGroupname = c.String(),
                        .IsActive = c.Boolean(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.PriceGroupID)
            
            CreateTable(
                "dbo.PriceGroupPrice",
                Function(c) New With
                    {
                        .PriceGroupPriceID = c.Int(nullable := False, identity := True),
                        .PriceGroupID = c.Int(nullable := False),
                        .CustomerStateID = c.Int(nullable := False),
                        .Price = c.Decimal(nullable := False, precision := 18, scale := 2),
                        .TaxPercent = c.Decimal(nullable := False, precision := 18, scale := 2),
                        .IsActive = c.Boolean(nullable := False),
                        .Vorausbuchungstage = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.PriceGroupPriceID) _
                .ForeignKey("dbo.CustomerState", Function(t) t.CustomerStateID, cascadeDelete := True) _
                .ForeignKey("dbo.PriceGroup", Function(t) t.PriceGroupID, cascadeDelete := True) _
                .Index(Function(t) t.PriceGroupID) _
                .Index(Function(t) t.CustomerStateID)
            
            AddColumn("dbo.LocationHour", "PriceGroupID", Function(c) c.Int())
            CreateIndex("dbo.LocationHour", "PriceGroupID")
            AddForeignKey("dbo.LocationHour", "PriceGroupID", "dbo.PriceGroup", "PriceGroupID")
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.LocationHour", "PriceGroupID", "dbo.PriceGroup")
            DropForeignKey("dbo.PriceGroupPrice", "PriceGroupID", "dbo.PriceGroup")
            DropForeignKey("dbo.PriceGroupPrice", "CustomerStateID", "dbo.CustomerState")
            DropIndex("dbo.PriceGroupPrice", New String() { "CustomerStateID" })
            DropIndex("dbo.PriceGroupPrice", New String() { "PriceGroupID" })
            DropIndex("dbo.LocationHour", New String() { "PriceGroupID" })
            DropColumn("dbo.LocationHour", "PriceGroupID")
            DropTable("dbo.PriceGroupPrice")
            DropTable("dbo.PriceGroup")
        End Sub
    End Class
End Namespace
