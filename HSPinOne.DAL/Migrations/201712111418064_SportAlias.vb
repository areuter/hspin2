Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class SportAlias
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Sport", "Aliasfor", Function(c) c.Int(nullable:=True))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Sport", "Aliasfor")
        End Sub
    End Class
End Namespace
