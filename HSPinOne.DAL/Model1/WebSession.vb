﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel

Partial Public Class WebSession
    Inherits EntityBase

    Private _SessionID As Integer
    Public Property SessionID As Integer
        Get
            Return _SessionID
        End Get
        Set(ByVal value As Integer)
            _SessionID = value
        End Set
    End Property


    Private _WebSessionID As String
    Public Property WebSessionID As String
        Get
            Return _WebSessionID
        End Get
        Set(ByVal value As String)
            _WebSessionID = value
        End Set
    End Property

    Partial Private Sub OnWebSessionIDChanged()
    End Sub

    Private _SessionData As String
    Public Property SessionData As String
        Get
            Return _SessionData
        End Get
        Set(ByVal value As String)
            _SessionData = value
            NotifyPropertyChanged("SessionData")
            OnSessionDataChanged()
        End Set
    End Property

    Partial Private Sub OnSessionDataChanged()
    End Sub

    Private _Expires As Integer
    Public Property Expires As Integer
        Get
            Return _Expires
        End Get
        Set(ByVal value As Integer)
            _Expires = value
            NotifyPropertyChanged("Expires")
            OnExpiresChanged()
        End Set
    End Property

    Partial Private Sub OnExpiresChanged()
    End Sub

End Class
