﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Public Class Post
    Inherits EntityBase


    Private _PostID As Integer
    Public Property PostID() As Integer
        Get
            Return _PostID
        End Get
        Set(ByVal value As Integer)
            _PostID = value
        End Set
    End Property

    Private _Postdate As DateTime
    Public Property Postdate() As DateTime
        Get
            Return _Postdate
        End Get
        Set(ByVal value As DateTime)
            _Postdate = value
        End Set
    End Property

    Private _Posttitle As String
    Public Property Posttitle() As String
        Get
            Return _Posttitle
        End Get
        Set(ByVal value As String)
            _Posttitle = value
        End Set
    End Property


    Private _Postcontent As String
    Public Property Postcontent() As String
        Get
            Return _Postcontent
        End Get
        Set(ByVal value As String)
            _Postcontent = value
        End Set
    End Property

    Private _Postexcerpt As String
    Public Property Postexcerpt() As String
        Get
            Return _Postexcerpt
        End Get
        Set(ByVal value As String)
            _Postexcerpt = value
        End Set
    End Property



    Private _Showfrom As DateTime
    Public Property Showfrom() As DateTime
        Get
            Return _Showfrom
        End Get
        Set(ByVal value As DateTime)
            _Showfrom = value
        End Set
    End Property

    Private _Showtill As DateTime
    Public Property Showtill() As DateTime
        Get
            Return _Showtill
        End Get
        Set(ByVal value As DateTime)
            _Showtill = value
        End Set
    End Property

    Private _Posttype As String
    <Index> _
    <MaxLength(20)> _
    Public Property Posttype() As String
        Get
            Return _Posttype
        End Get
        Set(ByVal value As String)
            _Posttype = value
        End Set
    End Property

    Private _Postparent As Integer
    Public Property Postparent() As Integer
        Get
            Return _Postparent
        End Get
        Set(ByVal value As Integer)
            _Postparent = value
        End Set
    End Property

    Private _Postorder As Integer
    Public Property Postorder() As Integer
        Get
            Return _Postorder
        End Get
        Set(ByVal value As Integer)
            _Postorder = value
        End Set
    End Property

    Private _Poststatus As Integer
    <Index> _
    Public Property Poststatus() As Integer
        Get
            Return _Poststatus
        End Get
        Set(ByVal value As Integer)
            _Poststatus = value
        End Set
    End Property

    Private _Tags As String
    Public Property Tags() As String
        Get
            Return _Tags
        End Get
        Set(ByVal value As String)
            _Tags = value
        End Set
    End Property

    Private _Image As String
    Public Property Image() As String
        Get
            Return _Image
        End Get
        Set(ByVal value As String)
            _Image = value
        End Set
    End Property

    Private _Link As String
    Public Property Link() As String
        Get
            Return _Link
        End Get
        Set(ByVal value As String)
            _Link = value
        End Set
    End Property



    Private _Postauthor As User
    Public Overridable Property Postauthor() As User
        Get
            Return _Postauthor
        End Get
        Set(ByVal value As User)
            _Postauthor = value
        End Set
    End Property
















End Class
