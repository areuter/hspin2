﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations


Public Class CustomerMeta
    Inherits EntityBase


    Private _CustomerMetaID As Integer
    <Key>
    Public Property CustomerMetaID() As Integer
        Get
            Return _CustomerMetaID
        End Get
        Set(ByVal value As Integer)
            _CustomerMetaID = value
            NotifyPropertyChanged("CustomerMetaID")
        End Set
    End Property

    Private _CustomerID As Integer
    Public Property CustomerID() As Integer
        Get
            Return _CustomerID
        End Get
        Set(ByVal value As Integer)
            _CustomerID = value
        End Set
    End Property

    Private _Customer As Customer
    <Schema.ForeignKey("CustomerID")>
    Public Overridable Property Customer() As Customer
        Get
            Return _Customer
        End Get
        Set(ByVal value As Customer)
            _Customer = value
        End Set
    End Property

    Private _MetaKey As String
    <MaxLength(255)>
    Public Property MetaKey() As String
        Get
            Return _MetaKey
        End Get
        Set(ByVal value As String)
            _MetaKey = value
            NotifyPropertyChanged("MetaKey")
        End Set
    End Property

    Private _MetaValue As String
    Public Property MetaValue() As String
        Get
            Return _MetaValue
        End Get
        Set(ByVal value As String)
            _MetaValue = value
            NotifyPropertyChanged("MetaValue")
        End Set
    End Property






End Class
