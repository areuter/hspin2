﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations

Public Class Sqlquery
    Inherits EntityBase

    Private _SqlqueryID As Integer
    Public Property SqlqueryID() As Integer
        Get
            Return _SqlqueryID
        End Get
        Set(ByVal value As Integer)
            _SqlqueryID = value
            NotifyPropertyChanged("SqlqueryID")
        End Set
    End Property

    Private _Bezeichnung As String
    <MaxLength(50)>
    Public Property Bezeichnung() As String
        Get
            Return _Bezeichnung
        End Get
        Set(ByVal value As String)
            _Bezeichnung = value
            NotifyPropertyChanged("Bezeichnung")
        End Set
    End Property

    Private _Beschreibung As String
    Public Property Beschreibung() As String
        Get
            Return _Beschreibung
        End Get
        Set(ByVal value As String)
            _Beschreibung = value
            NotifyPropertyChanged("Beschreibung")
        End Set
    End Property

    Private _Sql As String
    Public Property Sql() As String
        Get
            Return _Sql
        End Get
        Set(ByVal value As String)
            _Sql = value
            NotifyPropertyChanged("Sql")
        End Set
    End Property

    Private _Input As String
    Public Property Input() As String
        Get
            Return _Input
        End Get
        Set(ByVal value As String)
            _Input = value
            NotifyPropertyChanged("Input")
        End Set
    End Property






End Class
