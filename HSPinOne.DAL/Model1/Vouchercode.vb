﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Public Class Vouchercode
    Inherits EntityBase

    Private _VouchercodeID As Integer
    Public Property VouchercodeID() As Integer
        Get
            Return _VouchercodeID
        End Get
        Set(ByVal value As Integer)
            _VouchercodeID = value
            NotifyPropertyChanged("VouchercodeID")
        End Set
    End Property

    Private _VoucherID As Integer
    Public Property VoucherID() As Integer
        Get
            Return _VoucherID
        End Get
        Set(ByVal value As Integer)
            _VoucherID = value
            NotifyPropertyChanged("VoucherID")
        End Set
    End Property

    Private _Voucher As Voucher
    <ForeignKey("VoucherID")>
    Public Overridable Property Voucher() As Voucher
        Get
            Return _Voucher
        End Get
        Set(ByVal value As Voucher)
            _Voucher = value
            NotifyPropertyChanged("Voucher")
        End Set
    End Property

    Private _Code As String
    <StringLength(50)>
    Public Property Code() As String
        Get
            Return _Code
        End Get
        Set(ByVal value As String)
            _Code = value
            NotifyPropertyChanged("Code")
        End Set
    End Property

    Private _Usedat As DateTime?
    Public Property Usedat() As DateTime?
        Get
            Return _Usedat
        End Get
        Set(ByVal value As DateTime?)
            _Usedat = value
            NotifyPropertyChanged("Usedat")
        End Set
    End Property

    Private _Created As DateTime
    Public Property Created() As DateTime
        Get
            Return _Created
        End Get
        Set(ByVal value As DateTime)
            _Created = value
            NotifyPropertyChanged("Created")
        End Set
    End Property



End Class
