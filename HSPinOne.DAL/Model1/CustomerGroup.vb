﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Partial Public Class CustomerGroup
    Inherits EntityBase

    Private _CustomerGroupID As Integer
    Public Property CustomerGroupID As Integer
        Get
            Return _CustomerGroupID
        End Get
        Set(value As Integer)
            _CustomerGroupID = value
            NotifyPropertyChanged("CustomerGroupID")
        End Set
    End Property

    Private _Groupname As String
    <Required()> _
    <MaxLength(250)> _
    Public Property Groupname As String
        Get
            Return _Groupname
        End Get
        Set(value As String)
            _Groupname = value
            NotifyPropertyChanged("Groupname")
        End Set
    End Property

    Private _Customers As ObservableCollection(Of Customer) = New ObservableCollection(Of Customer)
    Public Overridable Property Customers As ObservableCollection(Of Customer)
        Get
            Return _Customers
        End Get
        Set(value As ObservableCollection(Of Customer))
            _Customers = value
        End Set
    End Property

End Class
