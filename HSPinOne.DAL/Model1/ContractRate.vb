
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations

Partial Public Class ContractRate
         Inherits EntityBase

    Private _ContractRateID As Integer
    Public Property ContractRateID As Integer
        Get
            Return _ContractRateID
        End Get
        Set(ByVal value As Integer)
            _ContractRateID = value
			NotifyPropertyChanged("ContractRateID")
			OnContractRateIDChanged()
        End Set
    End Property

	Private Partial Sub OnContractRateIDChanged()
	End Sub

    Private _ContractRateName As String
    Public Property ContractRateName As String
        Get
            Return _ContractRateName
        End Get
        Set(ByVal value As String)
            _ContractRateName = value
			NotifyPropertyChanged("ContractRateName")
			OnContractRateNameChanged()
        End Set
    End Property

	Private Partial Sub OnContractRateNameChanged()
	End Sub

    Private _Monate As Integer
    Public Property Monate As Integer
        Get
            Return _Monate
        End Get
        Set(ByVal value As Integer)
            _Monate = value
			NotifyPropertyChanged("Monate")
			OnMonateChanged()
        End Set
    End Property

	Private Partial Sub OnMonateChanged()
	End Sub

    Private _Ratenbetrag As Decimal
    Public Property Ratenbetrag As Decimal
        Get
            Return _Ratenbetrag
        End Get
        Set(ByVal value As Decimal)
            _Ratenbetrag = value
			NotifyPropertyChanged("Ratenbetrag")
			OnRatenbetragChanged()
        End Set
    End Property

	Private Partial Sub OnRatenbetragChanged()
	End Sub

    Private _Velaengerungsmonate As Integer
    Public Property Velaengerungsmonate As Integer
        Get
            Return _Velaengerungsmonate
        End Get
        Set(ByVal value As Integer)
            _Velaengerungsmonate = value
			NotifyPropertyChanged("Velaengerungsmonate")
			OnVelaengerungsmonateChanged()
        End Set
    End Property

	Private Partial Sub OnVelaengerungsmonateChanged()
    End Sub

    Private _Kennzeichen As String
    <MaxLength(50)> _
    Public Property Kennzeichen As String
        Get
            Return _Kennzeichen
        End Get
        Set(value As String)
            _Kennzeichen = value
            NotifyPropertyChanged("Kennzeichen")
        End Set
    End Property


    Private _Contracts As ObservableCollection(Of Contract) = New ObservableCollection(Of Contract)
    Public Overridable Property Contracts As ObservableCollection(Of Contract)
        Get
            Return _Contracts
        End Get
        Set(ByVal value As ObservableCollection(Of Contract))
            _Contracts = value
			NotifyPropertyChanged("Contracts")
			OnContractsChanged()
        End Set
    End Property

	Private Partial Sub OnContractsChanged()
    End Sub

    Private _ProductTypeID As Integer
    Public Property ProductTypeID As Integer
        Get
            Return _ProductTypeID
        End Get
        Set(value As Integer)
            _ProductTypeID = value
            NotifyPropertyChanged("ProductTypeID")
        End Set
    End Property

    Private _ProductType As ProductType
    Public Property ProductType As ProductType
        Get
            Return _ProductType
        End Get
        Set(value As ProductType)
            _ProductType = value
            NotifyPropertyChanged("ProductType")
        End Set
    End Property


    Private _IsOnlineAvailable As Boolean = False
    Public Property IsOnlineAvailable() As Boolean
        Get
            Return _IsOnlineAvailable
        End Get
        Set(ByVal value As Boolean)
            _IsOnlineAvailable = value
            NotifyPropertyChanged("IsOnlineAvailable")
        End Set
    End Property


    Private _CostcenterID As Integer
    Public Property CostcenterID() As Integer
        Get
            Return _CostcenterID
        End Get
        Set(ByVal value As Integer)
            _CostcenterID = value
        End Set
    End Property

    <Schema.ForeignKey("CostcenterID")> _
    Private _Costcenter
    Public Property Costcenter() As Costcenter
        Get
            Return _Costcenter
        End Get
        Set(ByVal value As Costcenter)
            _Costcenter = value
        End Set
    End Property

    Private _Startgebuehr As Decimal
    Public Property Startgebuehr() As Decimal
        Get
            Return _Startgebuehr
        End Get
        Set(ByVal value As Decimal)
            _Startgebuehr = value
            NotifyPropertyChanged("Startgebuehr")
        End Set
    End Property

    Private _Description As String
    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal value As String)
            _Description = value
            NotifyPropertyChanged("Description")
        End Set
    End Property




    Private _IsActive As Boolean = True
    Public Property IsActive() As Boolean
        Get
            Return _IsActive
        End Get
        Set(ByVal value As Boolean)
            _IsActive = value
            NotifyPropertyChanged("IsActive")
        End Set
    End Property



    Private _ContractRatePrices As ObservableCollection(Of ContractRatePrice) = New ObservableCollection(Of ContractRatePrice)
    Public Property ContractRatePrices() As ObservableCollection(Of ContractRatePrice)
        Get
            Return _ContractRatePrices
        End Get
        Set(ByVal value As ObservableCollection(Of ContractRatePrice))
            _ContractRatePrices = value
            NotifyPropertyChanged("ContractRatePrices")
        End Set
    End Property

    Private _VertragsbeginnTemplateID As Nullable(Of Integer)
    Public Property VertragsbeginnTemplateID() As Nullable(Of Integer)
        Get
            Return _VertragsbeginnTemplateID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _VertragsbeginnTemplateID = value
        End Set
    End Property


    Private _VertragsbeginnTemplate As Template
    <Schema.ForeignKey("VertragsbeginnTemplateID")>
    Public Property VertragsbeginnTemplate() As Template
        Get
            Return _VertragsbeginnTemplate
        End Get
        Set(ByVal value As Template)
            _VertragsbeginnTemplate = value
            NotifyPropertyChanged("VertragsbeginnTemplate")
        End Set
    End Property


    Private _KuendigungTemplateID As Nullable(Of Integer)
    Public Property KuendigungTemplateID() As Nullable(Of Integer)
        Get
            Return _KuendigungTemplateID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _KuendigungTemplateID = value
        End Set
    End Property


    Private _KuendigungTemplate As Template
    <Schema.ForeignKey("KuendigungTemplateID")>
    Public Property KuendigungTemplate() As Template
        Get
            Return _KuendigungTemplate
        End Get
        Set(ByVal value As Template)
            _KuendigungTemplate = value
            NotifyPropertyChanged("KuendigungTemplate")
        End Set
    End Property



End Class
