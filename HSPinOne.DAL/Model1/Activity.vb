﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations

Public Class Activity
    Inherits EntityBase


    Private _ActivityID As Integer
    Public Property ActivityID As Integer
        Get
            Return _ActivityID
        End Get
        Set(value As Integer)
            _ActivityID = value
        End Set
    End Property



    '''0=Dokument, 1=Email, 2=Telefon, 3=Fax, 4=Persönlich, 
    Private _ActivityType As Integer
    Public Property ActivityType As Integer
        Get
            Return _ActivityType
        End Get
        Set(value As Integer)
            _ActivityType = value
            NotifyPropertyChanged("ActivityType")
        End Set
    End Property


    ''' Reinkommend, Rausgehend 1=Eingehend, 2=Ausgehend
    Private _Direction As Integer
    Public Property Direction As Integer
        Get
            Return _Direction
        End Get
        Set(value As Integer)
            _Direction = value
            NotifyPropertyChanged("Direction")
        End Set
    End Property

    Private _comment As String
    Public Property Comment As String
        Get
            Return _comment
        End Get
        Set(value As String)
            _comment = value
            NotifyPropertyChanged("Comment")
        End Set
    End Property

    Private _Zeit As DateTime
    Public Property Zeit As DateTime
        Get
            Return _Zeit
        End Get
        Set(value As DateTime)
            _Zeit = value
        End Set
    End Property

    Private _UserID As Nullable(Of Integer)
    Public Property UserID As Nullable(Of Integer)
        Get
            Return _UserID
        End Get
        Set(value As Nullable(Of Integer))
            _UserID = value
        End Set
    End Property

    Private _User As User
    Public Overridable Property User As User
        Get
            Return _User
        End Get
        Set(value As User)
            _User = value
        End Set
    End Property

    Private _CustomerID As Integer
    Public Property CustomerID As Integer
        Get
            Return _CustomerID
        End Get
        Set(value As Integer)
            _CustomerID = value
        End Set
    End Property

    Private _Customer As Customer
    Public Overridable Property Customer As Customer
        Get
            Return _Customer
        End Get
        Set(value As Customer)
            _Customer = value
        End Set
    End Property


    Private _DocumentUID As Nullable(Of Guid)
    Public Property DocumentUID As Nullable(Of Guid)
        Get
            Return _DocumentUID
        End Get
        Set(value As Nullable(Of Guid))
            _DocumentUID = value
            NotifyPropertyChanged("DocumentUID")
        End Set
    End Property

    Private _Document As Document
    Public Overridable Property Document As Document
        Get
            Return _Document
        End Get
        Set(value As Document)
            _Document = value
            NotifyPropertyChanged("Document")
        End Set
    End Property



End Class
