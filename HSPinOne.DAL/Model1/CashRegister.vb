﻿
Public Class CashRegister

    Private _CashRegisterID As Integer
    Public Property CashRegisterID() As Integer
        Get
            Return _CashRegisterID
        End Get
        Set(ByVal value As Integer)
            _CashRegisterID = value
        End Set
    End Property

    Private _KasseNr As Integer
    Public Property KasseNr() As Integer
        Get
            Return _KasseNr
        End Get
        Set(ByVal value As Integer)
            _KasseNr = value
        End Set
    End Property

    Private _LastReceipt As Integer
    Public Property LastReceipt() As Integer
        Get
            Return _LastReceipt
        End Get
        Set(ByVal value As Integer)
            _LastReceipt = value
        End Set
    End Property

    Private _UseTse As Boolean = False
    Public Property UseTse() As Boolean
        Get
            Return _UseTse
        End Get
        Set(ByVal value As Boolean)
            _UseTse = value
        End Set
    End Property

    Private _DefaultAccountID As Integer
    Public Property DefaultAccountID() As Integer
        Get
            Return _DefaultAccountID
        End Get
        Set(ByVal value As Integer)
            _DefaultAccountID = value
        End Set
    End Property

    Private _DefaultPaymentMethodID As Integer
    Public Property DefaultPaymentMethodID() As Integer
        Get
            Return _DefaultPaymentMethodID
        End Get
        Set(ByVal value As Integer)
            _DefaultPaymentMethodID = value
        End Set
    End Property


End Class
