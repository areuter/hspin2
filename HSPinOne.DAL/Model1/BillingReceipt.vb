﻿
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Public Class BillingReceipt
    Inherits EntityBase


    Private _BillingReceiptID As Integer
    <Key>
    Public Property BillingReceiptID() As Integer
        Get
            Return _BillingReceiptID
        End Get
        Set(ByVal value As Integer)
            _BillingReceiptID = value
        End Set
    End Property

    Private _KasseNr As String
    Public Property KasseNr() As String
        Get
            Return _KasseNr
        End Get
        Set(ByVal value As String)
            _KasseNr = value
        End Set
    End Property

    Private _KasseBelegNr As Integer
    Public Property KasseBelegNr() As Integer
        Get
            Return _KasseBelegNr
        End Get
        Set(ByVal value As Integer)
            _KasseBelegNr = value
        End Set
    End Property


    Private _Created As DateTime
    Public Property Created() As DateTime
        Get
            Return _Created
        End Get
        Set(ByVal value As DateTime)
            _Created = value
        End Set
    End Property

    Private _ReceiptType As Integer
    Public Property ReceiptType() As Integer
        Get
            Return _ReceiptType
        End Get
        Set(ByVal value As Integer)
            _ReceiptType = value
        End Set
    End Property

    Private _BonTyp As String
    <MaxLength(50)>
    Public Property BonTyp() As String
        Get
            Return _BonTyp
        End Get
        Set(ByVal value As String)
            _BonTyp = value
        End Set
    End Property



    Private _GVTyp As String
    <MaxLength(50)>
    Public Property GVTyp() As String
        Get
            Return _GVTyp
        End Get
        Set(ByVal value As String)
            _GVTyp = value
        End Set
    End Property


    Private _Brutto As Decimal
    Public Property Brutto() As Decimal
        Get
            Return _Brutto
        End Get
        Set(ByVal value As Decimal)
            _Brutto = value
        End Set
    End Property

    Private _Creator As String
    <MaxLength(50)>
    Public Property Creator() As String
        Get
            Return _Creator
        End Get
        Set(ByVal value As String)
            _Creator = value
            NotifyPropertyChanged("Creator")
        End Set
    End Property

    Private _CashClosingID As Integer
    Public Property CashClosingID() As Integer
        Get
            Return _CashClosingID
        End Get
        Set(ByVal value As Integer)
            _CashClosingID = value
        End Set
    End Property

    Private _CashClosing As CashClosing
    <ForeignKey("CashClosingID")>
    Public Property CashClosing() As CashClosing
        Get
            Return _CashClosing
        End Get
        Set(ByVal value As CashClosing)
            _CashClosing = value
        End Set
    End Property

    Private _PaymentMethod As PaymentMethod
    <ForeignKey("PaymentMethodID")>
    Public Property PaymentMethod() As PaymentMethod
        Get
            Return _PaymentMethod
        End Get
        Set(ByVal value As PaymentMethod)
            _PaymentMethod = value
        End Set
    End Property

    Private _PaymentMethodID As Integer
    Public Property PaymentMethodID() As Integer
        Get
            Return _PaymentMethodID
        End Get
        Set(ByVal value As Integer)
            _PaymentMethodID = value
        End Set
    End Property

    Private _PaymentInformation As String
    Public Property PaymentInformation() As String
        Get
            Return _PaymentInformation
        End Get
        Set(ByVal value As String)
            _PaymentInformation = value
        End Set
    End Property


    Public Overridable Property Billings As ObservableCollection(Of Billing)

End Class
