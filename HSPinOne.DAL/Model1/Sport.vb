

Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations.Schema

Partial Public Class Sport
         Inherits EntityBase

    Private _SportID As Integer
    Public Property SportID As Integer
        Get
            Return _SportID
        End Get
        Set(ByVal value As Integer)
            _SportID = value
			NotifyPropertyChanged("SportID")
			OnSportIDChanged()
        End Set
    End Property

	Private Partial Sub OnSportIDChanged()
	End Sub

    Private _Sportname As String
    Public Property Sportname As String
        Get
            Return _Sportname
        End Get
        Set(ByVal value As String)
            _Sportname = Trim(value)
			NotifyPropertyChanged("Sportname")
			OnSportnameChanged()
        End Set
    End Property

	Private Partial Sub OnSportnameChanged()
	End Sub

    Private _Beschreibung As String
    Public Property Beschreibung As String
        Get
            Return _Beschreibung
        End Get
        Set(ByVal value As String)
            _Beschreibung = value
			NotifyPropertyChanged("Beschreibung")
			OnBeschreibungChanged()
        End Set
    End Property

	Private Partial Sub OnBeschreibungChanged()
	End Sub

    Private _StdLohn As Nullable(Of Decimal)
    Public Property StdLohn As Nullable(Of Decimal)
        Get
            Return _StdLohn
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            _StdLohn = value
			NotifyPropertyChanged("StdLohn")
			OnStdLohnChanged()
        End Set
    End Property

	Private Partial Sub OnStdLohnChanged()
    End Sub

    Private _Ansprechpartner_CustomerID As Nullable(Of Integer)
    <ForeignKey("Ansprechpartner_Customer")> _
    Public Property Ansprechpartner_CustomerID As Nullable(Of Integer)
        Get
            Return _Ansprechpartner_CustomerID
        End Get
        Set(value As Nullable(Of Integer))
            _Ansprechpartner_CustomerID = value
            NotifyPropertyChanged("Ansprechpartner_CustomerID")
        End Set
    End Property

    Private _Ansprechpartner_Customer As Customer
    Public Overridable Property Ansprechpartner_Customer As Customer
        Get
            Return _Ansprechpartner_Customer
        End Get
        Set(ByVal value As Customer)
            _Ansprechpartner_Customer = value
            NotifyPropertyChanged("Ansprechpartner_Customer")
            OnCustomerChanged()
        End Set
    End Property


    Partial Private Sub OnCustomerChanged()
    End Sub



    Private _Obmann_CustomerID As Nullable(Of Integer)
    <ForeignKey("Obmann_Customer")> _
    Public Property Obmann_CustomerID As Nullable(Of Integer)
        Get
            Return _Obmann_CustomerID
        End Get
        Set(value As Nullable(Of Integer))
            _Obmann_CustomerID = value
            NotifyPropertyChanged("Obmann_CustomerID")
        End Set
    End Property

    Private _Obmann_Customer As Customer
    Public Overridable Property Obmann_Customer As Customer
        Get
            Return _Obmann_Customer
        End Get
        Set(value As Customer)
            _Obmann_Customer = value
            NotifyPropertyChanged("Obmann_Customer")
        End Set
    End Property



    Private _Courses As ObservableCollection(Of Course) = New ObservableCollection(Of Course)
    Public Overridable Property Courses As ObservableCollection(Of Course)
        Get
            Return _Courses
        End Get
        Set(ByVal value As ObservableCollection(Of Course))
            _Courses = value
            NotifyPropertyChanged("Courses")
            OnCoursesChanged()
        End Set
    End Property

    Partial Private Sub OnCoursesChanged()
    End Sub

    Private _SportCategoryID As Integer
    Public Property SportCategoryID As Integer
        Get
            Return _SportCategoryID
        End Get
        Set(value As Integer)
            _SportCategoryID = value
            NotifyPropertyChanged("SportCategoryID")
        End Set
    End Property

    Private _SportCategory As SportCategory
    Public Overridable Property SportCategory As SportCategory
        Get
            Return _SportCategory
        End Get
        Set(value As SportCategory)
            _SportCategory = value
            NotifyPropertyChanged("SportCategory")
        End Set
    End Property


    Private _VideoUrl As String
    Public Property VideoUrl() As String
        Get
            Return _VideoUrl
        End Get
        Set(ByVal value As String)
            _VideoUrl = value
            NotifyPropertyChanged("VideoUrl")
        End Set
    End Property


    Private _Aliasfor As Nullable(Of Integer)
    Public Property Aliasfor() As Nullable(Of Integer)
        Get
            Return _Aliasfor
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _Aliasfor = value
            NotifyPropertyChanged("Aliasfor")
        End Set
    End Property




End Class
