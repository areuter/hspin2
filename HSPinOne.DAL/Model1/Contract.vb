
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations


Partial Public Class Contract
         Inherits EntityBase

    Private _ContractID As Integer
    <Key()> _
    Public Property ContractID As Integer
        Get
            Return _ContractID
        End Get
        Set(ByVal value As Integer)
            _ContractID = value
            NotifyPropertyChanged("ContractID")
            OnContractIDChanged()
        End Set
    End Property

	Private Partial Sub OnContractIDChanged()
	End Sub

    Private _CustomerID As Integer
    Public Property CustomerID As Integer
        Get
            Return _CustomerID
        End Get
        Set(ByVal value As Integer)
            _CustomerID = value
			NotifyPropertyChanged("CustomerID")
			OnCustomerIDChanged()
        End Set
    End Property

	Private Partial Sub OnCustomerIDChanged()
	End Sub

    Private _Vertragsbeginn As Date
    Public Property Vertragsbeginn As Date
        Get
            Return _Vertragsbeginn
        End Get
        Set(ByVal value As Date)
            _Vertragsbeginn = value
			NotifyPropertyChanged("Vertragsbeginn")
			OnVertragsbeginnChanged()
        End Set
    End Property

	Private Partial Sub OnVertragsbeginnChanged()
	End Sub

    Private _Monate As Integer
    Public Property Monate As Integer
        Get
            Return _Monate
        End Get
        Set(ByVal value As Integer)
            _Monate = value
			NotifyPropertyChanged("Monate")
			OnMonateChanged()
        End Set
    End Property

	Private Partial Sub OnMonateChanged()
	End Sub

    Private _Vertragsende As Date
    Public Property Vertragsende As Date
        Get
            Return _Vertragsende
        End Get
        Set(ByVal value As Date)
            _Vertragsende = value
			NotifyPropertyChanged("Vertragsende")
			OnVertragsendeChanged()
        End Set
    End Property

	Private Partial Sub OnVertragsendeChanged()
	End Sub

    Private _ContractRateID As Integer
    Public Property ContractRateID As Integer
        Get
            Return _ContractRateID
        End Get
        Set(ByVal value As Integer)
            _ContractRateID = value
			NotifyPropertyChanged("ContractRateID")
			OnContractRateIDChanged()
        End Set
    End Property

	Private Partial Sub OnContractRateIDChanged()
	End Sub

    Private _PaymentMethodID As Integer
    Public Property PaymentMethodID As Integer
        Get
            Return _PaymentMethodID
        End Get
        Set(ByVal value As Integer)
            _PaymentMethodID = value
			NotifyPropertyChanged("PaymentMethodID")
			OnPaymentMethodIDChanged()
        End Set
    End Property

	Private Partial Sub OnPaymentMethodIDChanged()
	End Sub

    Private _Ratenbetrag As Decimal
    Public Property Ratenbetrag As Decimal
        Get
            Return _Ratenbetrag
        End Get
        Set(ByVal value As Decimal)
            _Ratenbetrag = value
			NotifyPropertyChanged("Ratenbetrag")
			OnRatenbetragChanged()
        End Set
    End Property

	Private Partial Sub OnRatenbetragChanged()
	End Sub

    Private _Zahlungsbeginn As Date
    Public Property Zahlungsbeginn As Date
        Get
            Return _Zahlungsbeginn
        End Get
        Set(ByVal value As Date)
            _Zahlungsbeginn = value
			NotifyPropertyChanged("Zahlungsbeginn")
			OnZahlungsbeginnChanged()
        End Set
    End Property

	Private Partial Sub OnZahlungsbeginnChanged()
	End Sub

    Private _Startgebuehr As Nullable(Of Decimal)
    Public Property Startgebuehr As Nullable(Of Decimal)
        Get
            Return _Startgebuehr
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            _Startgebuehr = value
			NotifyPropertyChanged("Startgebuehr")
			OnStartgebuehrChanged()
        End Set
    End Property

	Private Partial Sub OnStartgebuehrChanged()
	End Sub

    Private _Verlaengerungsmonate As Integer
    Public Property Verlaengerungsmonate As Integer
        Get
            Return _Verlaengerungsmonate
        End Get
        Set(ByVal value As Integer)
            _Verlaengerungsmonate = value
            NotifyPropertyChanged("Verlaengerungsmonate")
            OnVerlaengerungsmonateChanged()
        End Set
    End Property

	Private Partial Sub OnVerlaengerungsmonateChanged()
	End Sub

    Private _Kuendigungseingang As Nullable(Of Date)
    Public Property Kuendigungseingang As Nullable(Of Date)
        Get
            Return _Kuendigungseingang
        End Get
        Set(ByVal value As Nullable(Of Date))
            _Kuendigungseingang = value
			NotifyPropertyChanged("Kuendigungseingang")
			OnKuendigungseingangChanged()
        End Set
    End Property

	Private Partial Sub OnKuendigungseingangChanged()
	End Sub

    Private _Kuendigungzum As Nullable(Of Date)
    Public Property Kuendigungzum As Nullable(Of Date)
        Get
            Return _Kuendigungzum
        End Get
        Set(ByVal value As Nullable(Of Date))
            _Kuendigungzum = value
			NotifyPropertyChanged("Kuendigungzum")
			OnKuendigungzumChanged()
        End Set
    End Property

	Private Partial Sub OnKuendigungzumChanged()
	End Sub

    Private _Erstelltam As Nullable(Of Date)
    Public Property Erstelltam As Nullable(Of Date)
        Get
            Return _Erstelltam
        End Get
        Set(ByVal value As Nullable(Of Date))
            _Erstelltam = value
			NotifyPropertyChanged("Erstelltam")
			OnErstelltamChanged()
        End Set
    End Property

	Private Partial Sub OnErstelltamChanged()
	End Sub

    Private _Erstelltvon As Nullable(Of Integer)
    Public Property Erstelltvon As Nullable(Of Integer)
        Get
            Return _Erstelltvon
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _Erstelltvon = value
			NotifyPropertyChanged("Erstelltvon")
			OnErstelltvonChanged()
        End Set
    End Property

	Private Partial Sub OnErstelltvonChanged()
	End Sub

    Private _Bearbeitetam As Nullable(Of Date)
    Public Property Bearbeitetam As Nullable(Of Date)
        Get
            Return _Bearbeitetam
        End Get
        Set(ByVal value As Nullable(Of Date))
            _Bearbeitetam = value
			NotifyPropertyChanged("Bearbeitetam")
			OnBearbeitetamChanged()
        End Set
    End Property

	Private Partial Sub OnBearbeitetamChanged()
	End Sub

    Private _Bearbeitetvon As Nullable(Of Integer)
    Public Property Bearbeitetvon As Nullable(Of Integer)
        Get
            Return _Bearbeitetvon
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _Bearbeitetvon = value
			NotifyPropertyChanged("Bearbeitetvon")
			OnBearbeitetvonChanged()
        End Set
    End Property

	Private Partial Sub OnBearbeitetvonChanged()
	End Sub


    Private _Billings As ObservableCollection(Of Billing) = New ObservableCollection(Of Billing)
    Public Overridable Property Billings As ObservableCollection(Of Billing)
        Get
            Return _Billings
        End Get
        Set(ByVal value As ObservableCollection(Of Billing))
            _Billings = value
			NotifyPropertyChanged("Billings")
			OnBillingsChanged()
        End Set
    End Property

	Private Partial Sub OnBillingsChanged()
	End Sub

    Private _ContractRate As ContractRate
    Public Overridable Property ContractRate As ContractRate
        Get
            Return _ContractRate
        End Get
        Set(ByVal value As ContractRate)
            _ContractRate = value
			NotifyPropertyChanged("ContractRate")
			OnContractRateChanged()
        End Set
    End Property

	Private Partial Sub OnContractRateChanged()
	End Sub

    Private _Customer As Customer
    Public Overridable Property Customer As Customer
        Get
            Return _Customer
        End Get
        Set(ByVal value As Customer)
            _Customer = value
			NotifyPropertyChanged("Customer")
			OnCustomerChanged()
        End Set
    End Property

    Partial Private Sub OnCustomerChanged()
    End Sub

    Private _CostcenterID As Integer
    Public Property CostcenterID As Integer
        Get
            Return _CostcenterID
        End Get
        Set(value As Integer)
            _CostcenterID = value
            NotifyPropertyChanged("CostcenterID")
            OnCostcenterIDChanged()
        End Set
    End Property

    Partial Private Sub OnCostcenterIDChanged()
    End Sub

    Private _Costcenter As Costcenter
    Public Property Costcenter As Costcenter
        Get
            Return _Costcenter
        End Get
        Set(value As Costcenter)
            _Costcenter = value
            NotifyPropertyChanged("Costcenter")
            OnCostcenterChanged()
        End Set
    End Property

    Partial Private Sub OnCostcenterChanged()
    End Sub


    Private _Kennzeichen As String
    <MaxLength(50)> _
    Public Property Kennzeichen As String
        Get
            Return _Kennzeichen
        End Get
        Set(value As String)
            _Kennzeichen = value
            NotifyPropertyChanged("Kennzeichen")
        End Set
    End Property

    Private _ContractTimestops As ObservableCollection(Of ContractTimestop) = New ObservableCollection(Of ContractTimestop)
    Public Overridable Property ContractTimestops As ObservableCollection(Of ContractTimestop)
        Get
            Return _ContractTimestops
        End Get
        Set(value As ObservableCollection(Of ContractTimestop))
            _ContractTimestops = value
            NotifyPropertyChanged("ContractTimestops")
        End Set
    End Property

    Private _AgbAccepted As Boolean = False
    Public Property AgbAccepted() As Boolean
        Get
            Return _AgbAccepted
        End Get
        Set(ByVal value As Boolean)
            _AgbAccepted = value
        End Set
    End Property


    Private _Token As String
    <MaxLength(250)>
    Public Property Token() As String
        Get
            Return _Token
        End Get
        Set(ByVal value As String)
            _Token = value
        End Set
    End Property



End Class
