
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel

Partial Public Class Appointment
         Inherits EntityBase

    Private _AppointmentID As Integer
    Public Property AppointmentID As Integer
        Get
            Return _AppointmentID
        End Get
        Set(ByVal value As Integer)
            _AppointmentID = value
			NotifyPropertyChanged("AppointmentID")
			OnAppointmentIDChanged()
        End Set
    End Property

	Private Partial Sub OnAppointmentIDChanged()
	End Sub

    Private _LocationID As Integer
    Public Property LocationID As Integer
        Get
            Return _LocationID
        End Get
        Set(ByVal value As Integer)
            _LocationID = value
			NotifyPropertyChanged("LocationID")
			OnLocationIDChanged()
        End Set
    End Property

	Private Partial Sub OnLocationIDChanged()
	End Sub

    Private _Start As Date
    Public Property Start As Date
        Get
            Return _Start
        End Get
        Set(ByVal value As Date)
            _Start = value
			NotifyPropertyChanged("Start")
			OnStartChanged()
        End Set
    End Property

	Private Partial Sub OnStartChanged()
	End Sub

    Private _Ende As Date
    Public Property Ende As Date
        Get
            Return _Ende
        End Get
        Set(ByVal value As Date)
            _Ende = value
			NotifyPropertyChanged("Ende")
			OnEndeChanged()
        End Set
    End Property

	Private Partial Sub OnEndeChanged()
	End Sub

    Private _CourseID As Nullable(Of Integer)
    Public Property CourseID As Nullable(Of Integer)
        Get
            Return _CourseID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _CourseID = value
			NotifyPropertyChanged("CourseID")
			OnCourseIDChanged()
        End Set
    End Property

	Private Partial Sub OnCourseIDChanged()
	End Sub

    Private _BookingID As Nullable(Of Integer)
    Public Property BookingID As Nullable(Of Integer)
        Get
            Return _BookingID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _BookingID = value
            NotifyPropertyChanged("BookingID")
            OnBookingIDChanged()
        End Set
    End Property

	Private Partial Sub OnBookingIDChanged()
	End Sub

    Private _Canceled As Boolean
    Public Property Canceled As Boolean
        Get
            Return _Canceled
        End Get
        Set(ByVal value As Boolean)
            _Canceled = value
            NotifyPropertyChanged("Canceled")
            OnCanceledChanged()
        End Set
    End Property

	Private Partial Sub OnCanceledChanged()
    End Sub

    Private _TNCount As Integer = 0
    Public Property TNCount() As Integer
        Get
            Return _TNCount
        End Get
        Set(ByVal value As Integer)
            _TNCount = value
            NotifyPropertyChanged("TNCount")
        End Set
    End Property



    Private _Course As Course
    Public Overridable Property Course As Course
        Get
            Return _Course
        End Get
        Set(value As Course)
            _Course = value
            NotifyPropertyChanged("Course")
        End Set
    End Property


    Private _Booking As Booking
    Public Overridable Property Booking As Booking
        Get
            Return _Booking
        End Get
        Set(ByVal value As Booking)
            _Booking = value
			NotifyPropertyChanged("Booking")
			OnBookingChanged()
        End Set
    End Property

	Private Partial Sub OnBookingChanged()
	End Sub

    Private _Location As Location
    Public Overridable Property Location As Location
        Get
            Return _Location
        End Get
        Set(ByVal value As Location)
            _Location = value
			NotifyPropertyChanged("Location")
			OnLocationChanged()
        End Set
    End Property

	Private Partial Sub OnLocationChanged()
    End Sub

    Private _AppointmentStaffs As New ObservableCollection(Of AppointmentStaff)
    Public Overridable Property AppointmentStaffs As ObservableCollection(Of AppointmentStaff)
        Get
            Return _AppointmentStaffs
        End Get
        Set(value As ObservableCollection(Of AppointmentStaff))
            _AppointmentStaffs = value
            NotifyPropertyChanged("AppointmentStaffs")
        End Set
    End Property


End Class
