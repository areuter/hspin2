

Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations


Partial Public Class User
         Inherits EntityBase

    Private _UserID As Integer
    Public Property UserID As Integer
        Get
            Return _UserID
        End Get
        Set(ByVal value As Integer)
            _UserID = value
			NotifyPropertyChanged("UserID")
			OnUserIDChanged()
        End Set
    End Property

	Private Partial Sub OnUserIDChanged()
	End Sub

    Private _InstitutionID As Integer
    Public Property InstitutionID As Integer
        Get
            Return _InstitutionID
        End Get
        Set(ByVal value As Integer)
            _InstitutionID = value
			NotifyPropertyChanged("InstitutionID")
			OnInstitutionIDChanged()
        End Set
    End Property

	Private Partial Sub OnInstitutionIDChanged()
	End Sub

    Private _RoleID As Integer
    Public Property RoleID As Integer
        Get
            Return _RoleID
        End Get
        Set(ByVal value As Integer)
            _RoleID = value
			NotifyPropertyChanged("RoleID")
			OnRoleIDChanged()
        End Set
    End Property

	Private Partial Sub OnRoleIDChanged()
	End Sub

    Private _UserName As String
    Public Property UserName As String
        Get
            Return _UserName
        End Get
        Set(ByVal value As String)
            _UserName = Trim(value)
			NotifyPropertyChanged("UserName")
			OnUserNameChanged()
        End Set
    End Property

	Private Partial Sub OnUserNameChanged()
	End Sub

    Private _EmailAddress As String
    Public Property EmailAddress As String
        Get
            Return _EmailAddress
        End Get
        Set(ByVal value As String)
            _EmailAddress = value
			NotifyPropertyChanged("EmailAddress")
			OnEmailAddressChanged()
        End Set
    End Property

	Private Partial Sub OnEmailAddressChanged()
	End Sub

    Private _LoginID As String
    Public Property LoginID As String
        Get
            Return _LoginID
        End Get
        Set(ByVal value As String)
            _LoginID = value
			NotifyPropertyChanged("LoginID")
			OnLoginIDChanged()
        End Set
    End Property

	Private Partial Sub OnLoginIDChanged()
	End Sub

    Private _Password As String
    Public Property Password As String
        Get
            Return _Password
        End Get
        Set(ByVal value As String)
            _Password = value
			NotifyPropertyChanged("Password")
			OnPasswordChanged()
        End Set
    End Property

	Private Partial Sub OnPasswordChanged()
	End Sub

    Private _SaltValue As String
    Public Property SaltValue As String
        Get
            Return _SaltValue
        End Get
        Set(ByVal value As String)
            _SaltValue = value
			NotifyPropertyChanged("SaltValue")
			OnSaltValueChanged()
        End Set
    End Property

	Private Partial Sub OnSaltValueChanged()
	End Sub


    Private _Institution As Institution
    Public Overridable Property Institution As Institution
        Get
            Return _Institution
        End Get
        Set(ByVal value As Institution)
            _Institution = value
			NotifyPropertyChanged("Institution")
			OnInstitutionChanged()
        End Set
    End Property

	Private Partial Sub OnInstitutionChanged()
	End Sub

    Private _Role As Role
    Public Overridable Property Role As Role
        Get
            Return _Role
        End Get
        Set(ByVal value As Role)
            _Role = value
			NotifyPropertyChanged("Role")
			OnRoleChanged()
        End Set
    End Property

	Private Partial Sub OnRoleChanged()
    End Sub

    Private _LastAliveCheck As Nullable(Of DateTime)
    Public Property LastAliveCheck As Nullable(Of DateTime)
        Get
            Return _LastAliveCheck
        End Get
        Set(value As Nullable(Of DateTime))
            _LastAliveCheck = value
            NotifyPropertyChanged("LastAliveCheck")
        End Set
    End Property


    Private _IsActive As Boolean
    Public Property IsActive As Boolean
        Get
            Return _IsActive
        End Get
        Set(value As Boolean)
            _IsActive = value
        End Set
    End Property



    Private _Abwesenheitsnotiz As String
    <MaxLength(500)> _
    Public Property Abwesenheitsnotiz As String
        Get
            Return _Abwesenheitsnotiz
        End Get
        Set(value As String)
            _Abwesenheitsnotiz = value
        End Set
    End Property

    Private _Message As String
    <MaxLength(500)> _
Public Property Message() As String
        Get
            Return _Message
        End Get
        Set(ByVal value As String)
            _Message = value
        End Set
    End Property

    Private _Dismissed As Boolean = False
    Public Property Dismissed() As Boolean
        Get
            Return _Dismissed
        End Get
        Set(ByVal value As Boolean)
            _Dismissed = value
        End Set
    End Property





End Class
