﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel

Public Class LocationEquipment
    Inherits EntityBase


    Private _LocationEquipmentID As Integer
    Public Property LocationEquipmentID As Integer
        Get
            Return _LocationEquipmentID
        End Get
        Set(ByVal value As Integer)
            _LocationEquipmentID = value
            NotifyPropertyChanged("LocationEquipmentID")
            OnLocationEquipmentIDChanged()
        End Set
    End Property

    Partial Private Sub OnLocationEquipmentIDChanged()
    End Sub


    Private _EquipmentID As Integer
    Public Property EquipmentID As Integer
        Get
            Return _EquipmentID
        End Get
        Set(ByVal value As Integer)
            _EquipmentID = value
            NotifyPropertyChanged("EquipmentID")
            OnEquipmentIDChanged()
        End Set
    End Property

    Partial Private Sub OnEquipmentIDChanged()
    End Sub


    Private _LocationID As Integer
    Public Property LocationID As Integer
        Get
            Return _LocationID
        End Get
        Set(ByVal value As Integer)
            _LocationID = value
            NotifyPropertyChanged("LocationID")
            OnLocationIDChanged()
        End Set
    End Property

    Partial Private Sub OnLocationIDChanged()
    End Sub


    Private _Anzahl As Double
    Public Property Anzahl As Double
        Get
            Return _Anzahl
        End Get
        Set(ByVal value As Double)
            _Anzahl = value
            NotifyPropertyChanged("Anzahl")
            OnAnzahlChanged()
        End Set
    End Property

    Partial Private Sub OnAnzahlChanged()
    End Sub

    Private _Equipment As Equipment
    Public Property Equipment As Equipment
        Get
            Return _Equipment
        End Get
        Set(ByVal value As Equipment)
            _Equipment = value
            NotifyPropertyChanged("Equipment")
            OnEquipmentChanged()
        End Set
    End Property

    Partial Private Sub OnEquipmentChanged()
    End Sub
End Class
