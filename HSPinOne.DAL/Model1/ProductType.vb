﻿
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations.Schema

''' Warengruppe

Public Class ProductType
    Inherits EntityBase


    Private _ProductTypeID As Integer
    Public Property ProductTypeID As Integer
        Get
            Return _ProductTypeID
        End Get
        Set(value As Integer)
            _ProductTypeID = value
            NotifyPropertyChanged("ProductTypeID")
        End Set
    End Property

    Private _ProductTypeName As String
    Public Property ProductTypeName As String
        Get
            Return _ProductTypeName
        End Get
        Set(value As String)
            _ProductTypeName = value
            NotifyPropertyChanged("ProductTypeName")
        End Set
    End Property

End Class
