﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations.Schema

Partial Public Class Waitinglist
    Inherits EntityBase

    Private _WaitinglistID As Integer
    Public Property WaitinglistID As Integer
        Get
            Return _WaitinglistID
        End Get
        Set(ByVal value As Integer)
            _WaitinglistID = value
        End Set
    End Property

    Private _CourseID As Integer
    Public Property CourseID As Integer
        Get
            Return _CourseID
        End Get
        Set(ByVal value As Integer)
            _CourseID = value
        End Set
    End Property

    Private _CustomerID As Integer
    Public Property CustomerID As Integer
        Get
            Return _CustomerID
        End Get
        Set(ByVal value As Integer)
            _CustomerID = value
        End Set
    End Property

    Private _Timestamp As DateTime
    Public Property Timestamp As DateTime
        Get
            Return _Timestamp
        End Get
        Set(ByVal value As DateTime)
            _Timestamp = value
        End Set
    End Property

    Private _LastMailAt As Nullable(Of DateTime)
    Public Property LastMailAt As Nullable(Of DateTime)
        Get
            Return _LastMailAt
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _LastMailAt = value
        End Set
    End Property


    Private _MailsSend As Integer
    Public Property MailsSend As Integer
        Get
            Return _MailsSend
        End Get
        Set(ByVal value As Integer)
            _MailsSend = value
        End Set
    End Property

    Private _Course As Course
    <ForeignKey("CourseID")> _
    Public Overridable Property Course As Course
        Get
            Return _Course
        End Get
        Set(ByVal value As Course)
            _Course = value
        End Set
    End Property

    Private _Customer As Customer
    <ForeignKey("CustomerID")> _
    Public Overridable Property Customer As Customer
        Get
            Return _Customer
        End Get
        Set(ByVal value As Customer)
            _Customer = value
        End Set
    End Property
End Class
