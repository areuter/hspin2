﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Public Class Language
    Inherits EntityBase


    Private _ISOCode As String
    <Key>
    <DatabaseGenerated(DatabaseGeneratedOption.None)>
    <MaxLength(2)>
    Public Property ISOCode() As String
        Get
            Return _ISOCode
        End Get
        Set(ByVal value As String)
            _ISOCode = value
            NotifyPropertyChanged("ISOCode")
        End Set
    End Property

    Private _English As String
    Public Property English() As String
        Get
            Return _English
        End Get
        Set(ByVal value As String)
            _English = value
            NotifyPropertyChanged("English")
        End Set
    End Property

    Private _Flag As String
    Public Property Flag() As String
        Get
            Return _Flag
        End Get
        Set(ByVal value As String)
            _Flag = value
            NotifyPropertyChanged("Flag")
        End Set
    End Property





End Class
