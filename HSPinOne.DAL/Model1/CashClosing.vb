﻿
Public Class CashClosing
    Inherits EntityBase


    Private _CashClosingID As Integer
    Public Property CashClosingID() As Integer
        Get
            Return _CashClosingID
        End Get
        Set(ByVal value As Integer)
            _CashClosingID = value
            NotifyPropertyChanged("CashClosingID")
        End Set
    End Property

    Private _KasseNr As Integer
    Public Property KasseNr() As Integer
        Get
            Return _KasseNr
        End Get
        Set(ByVal value As Integer)
            _KasseNr = value
            NotifyPropertyChanged("KasseNr")
        End Set
    End Property

    Private _ClosedBy As String
    Public Property ClosedBy() As String
        Get
            Return _ClosedBy
        End Get
        Set(ByVal value As String)
            _ClosedBy = value
            NotifyPropertyChanged("ClosedBy")
        End Set
    End Property

    Private _SummeBar As Double
    Public Property SummeBar() As Double
        Get
            Return _SummeBar
        End Get
        Set(ByVal value As Double)
            _SummeBar = value
            NotifyPropertyChanged("SummeBar")
        End Set
    End Property

    Private _SummeKarte As Double
    Public Property SummeKarte() As Double
        Get
            Return _SummeKarte
        End Get
        Set(ByVal value As Double)
            _SummeKarte = value
            NotifyPropertyChanged("SummeKarte")
        End Set
    End Property

    Private _ECBon As String
    Public Property ECBon() As String
        Get
            Return _ECBon
        End Get
        Set(ByVal value As String)
            _ECBon = value
            NotifyPropertyChanged("ECBon")
        End Set
    End Property

    Private _Start As DateTime
    Public Property Start() As DateTime
        Get
            Return _Start
        End Get
        Set(ByVal value As DateTime)
            _Start = value
            NotifyPropertyChanged("Start")
        End Set
    End Property

    Private _Ende As DateTime?
    Public Property Ende() As DateTime?
        Get
            Return _Ende
        End Get
        Set(ByVal value As DateTime?)
            _Ende = value
            NotifyPropertyChanged("Ende")
        End Set
    End Property




End Class
