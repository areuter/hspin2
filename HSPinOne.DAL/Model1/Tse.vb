﻿Public Class Tse

    Private _TseID As Integer
    Public Property TseID() As Integer
        Get
            Return _TseID
        End Get
        Set(ByVal value As Integer)
            _TseID = value
        End Set
    End Property

    Private _BillingID As Integer?
    Public Property BillingID() As Integer?
        Get
            Return _BillingID
        End Get
        Set(ByVal value As Integer?)
            _BillingID = value
        End Set
    End Property

    Private _BillingReceiptID As Integer?
    Public Property BillingReceiptID() As Integer?
        Get
            Return _BillingReceiptID
        End Get
        Set(ByVal value As Integer?)
            _BillingReceiptID = value
        End Set
    End Property


    Private _Transactionnr As Integer
    Public Property Transactionnr() As Integer
        Get
            Return _Transactionnr
        End Get
        Set(ByVal value As Integer)
            _Transactionnr = value
        End Set
    End Property

    Private _SignatureCounter As Integer
    Public Property SignaturCounter() As Integer
        Get
            Return _SignatureCounter
        End Get
        Set(ByVal value As Integer)
            _SignatureCounter = value
        End Set
    End Property

    Private _Signature As String
    Public Property Signature() As String
        Get
            Return _Signature
        End Get
        Set(ByVal value As String)
            _Signature = value
        End Set
    End Property

    Private _Start As DateTime
    Public Property Start() As DateTime
        Get
            Return _Start
        End Get
        Set(ByVal value As DateTime)
            _Start = value
        End Set
    End Property

    Private _Finish As DateTime?
    Public Property Finish() As DateTime?
        Get
            Return _Finish
        End Get
        Set(ByVal value As DateTime?)
            _Finish = value
        End Set
    End Property


End Class
