﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Public Class ContractRatePrice
    Inherits EntityBase


    Private _ContractRatePriceID As Integer
    Public Property ContractRatePriceID() As Integer
        Get
            Return _ContractRatePriceID
        End Get
        Set(ByVal value As Integer)
            _ContractRatePriceID = value
        End Set
    End Property

    Private _CustomerStateID As Integer
    Public Property CustomerStateID As Integer
        Get
            Return _CustomerStateID
        End Get
        Set(ByVal value As Integer)
            _CustomerStateID = value
            NotifyPropertyChanged("CustomerStateID")
            OnCustomerStateIDChanged()
        End Set
    End Property

    Private _CustomerState As CustomerState
    <Schema.ForeignKey("CustomerStateID")>
    Public Overridable Property CustomerState As CustomerState
        Get
            Return _CustomerState
        End Get
        Set(ByVal value As CustomerState)
            _CustomerState = value
            NotifyPropertyChanged("CustomerState")
        End Set
    End Property

    Private _ContractRateID As Integer
    Public Property ContractRateID() As Integer
        Get
            Return _ContractRateID
        End Get
        Set(ByVal value As Integer)
            _ContractRateID = value
            NotifyPropertyChanged("ContractRateID")
        End Set
    End Property

    Private _ContractRate As ContractRate
    <Schema.ForeignKey("ContractRateID")>
    Public Property ContractRate() As ContractRate
        Get
            Return _ContractRate
        End Get
        Set(ByVal value As ContractRate)
            _ContractRate = value
            NotifyPropertyChanged("ContractRate")
        End Set
    End Property



    Partial Private Sub OnCustomerStateIDChanged()
    End Sub

    Private _Preis As Decimal = 0
    Public Property Preis As Decimal
        Get
            Return _Preis
        End Get
        Set(ByVal value As Decimal)
            _Preis = value
            NotifyPropertyChanged("Preis")
        End Set
    End Property

    Private _TaxPercent As Decimal
    Public Property TaxPercent() As Decimal
        Get
            Return _TaxPercent
        End Get
        Set(ByVal value As Decimal)
            _TaxPercent = value
            NotifyPropertyChanged("TaxPercent")
        End Set
    End Property


    Private _CostcenterID As Nullable(Of Integer)
    Public Property CostcenterID() As Nullable(Of Integer)
        Get
            Return _CostcenterID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _CostcenterID = value
        End Set
    End Property

    Private _Costcenter As Costcenter
    <ForeignKey("CostcenterID")>
    Public Overridable Property Costcenter() As Costcenter
        Get
            Return _Costcenter
        End Get
        Set(ByVal value As Costcenter)
            _Costcenter = value
            NotifyPropertyChanged("Costcenter")
        End Set
    End Property

    Private _Innenauftrag As String
    Public Property Innenauftrag() As String
        Get
            Return _Innenauftrag
        End Get
        Set(ByVal value As String)
            _Innenauftrag = value
            NotifyPropertyChanged("Innenauftrag")
        End Set
    End Property

    Private _Sachkonto As String
    Public Property Sachkonto() As String
        Get
            Return _Sachkonto
        End Get
        Set(ByVal value As String)
            _Sachkonto = value
            NotifyPropertyChanged("Sachkonto")
        End Set
    End Property


    Private _KSTextern As String
    <MaxLength(30)>
    Public Property KSTextern() As String
        Get
            Return _KSTextern
        End Get
        Set(ByVal value As String)
            _KSTextern = value
            NotifyPropertyChanged("KSTextern")
        End Set
    End Property

    Private _Steuerkennzeichen As String
    <MaxLength(20)>
    Public Property Steuerkennzeichen() As String
        Get
            Return _Steuerkennzeichen
        End Get
        Set(ByVal value As String)
            _Steuerkennzeichen = value
            NotifyPropertyChanged("Steuerkennzeichen")
        End Set
    End Property

    Private _AccountingTemplateID As Nullable(Of Integer)
    Public Property AccountingTemplateID() As Nullable(Of Integer)
        Get
            Return _AccountingTemplateID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _AccountingTemplateID = value
            NotifyPropertyChanged("AccountingTemplateID")
        End Set
    End Property

    Private _AccountingTemplate As AccountingTemplate
    <ForeignKey("AccountingTemplateID")>
    Public Property AccountingTemplate() As AccountingTemplate
        Get
            Return _AccountingTemplate
        End Get
        Set(ByVal value As AccountingTemplate)
            _AccountingTemplate = value
            NotifyPropertyChanged("AccountingTemplate")
        End Set
    End Property

End Class
