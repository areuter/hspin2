﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations


Public Class CourseMeta
    Inherits EntityBase

    Private _CourseMetaID As Integer
    <Key>
    Public Property CourseMetaID() As Integer
        Get
            Return _CourseMetaID
        End Get
        Set(ByVal value As Integer)
            _CourseMetaID = value
        End Set
    End Property

    Private _CourseID As Integer
    Public Property CourseID() As Integer
        Get
            Return _CourseID
        End Get
        Set(ByVal value As Integer)
            _CourseID = value
        End Set
    End Property

    Private _Course As Course
    <Schema.ForeignKey("CourseID")>
    Public Overridable Property Course() As Course
        Get
            Return _Course
        End Get
        Set(ByVal value As Course)
            _Course = value
        End Set
    End Property

    Private _MetaKey As String
    <MaxLength(255)>
    Public Property MetaKey() As String
        Get
            Return _MetaKey
        End Get
        Set(ByVal value As String)
            _MetaKey = value
        End Set
    End Property

    Private _MetaValue As String
    Public Property MetaValue() As String
        Get
            Return _MetaValue
        End Get
        Set(ByVal value As String)
            _MetaValue = value
        End Set
    End Property

End Class
