
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel

Partial Public Class Checkin
         Inherits EntityBase

    Private _CheckinID As Integer
    Public Property CheckinID As Integer
        Get
            Return _CheckinID
        End Get
        Set(ByVal value As Integer)
            _CheckinID = value
			NotifyPropertyChanged("CheckinID")
			OnCheckinIDChanged()
        End Set
    End Property

	Private Partial Sub OnCheckinIDChanged()
    End Sub

    Private _CheckinLocation As Integer
    Public Property CheckinLocation As Integer
        Get
            Return _CheckinLocation
        End Get
        Set(value As Integer)
            _CheckinLocation = value

        End Set
    End Property

    Private _CustomerID As Nullable(Of Integer)
    Public Property CustomerID As Nullable(Of Integer)
        Get
            Return _CustomerID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _CustomerID = value
			NotifyPropertyChanged("CustomerID")
			OnCustomerIDChanged()
        End Set
    End Property

	Private Partial Sub OnCustomerIDChanged()
	End Sub

    Private _Schluessel As String
    Public Property Schluessel As String
        Get
            Return _Schluessel
        End Get
        Set(ByVal value As String)
            _Schluessel = value
			NotifyPropertyChanged("Schluessel")
			OnSchluesselChanged()
        End Set
    End Property

	Private Partial Sub OnSchluesselChanged()
    End Sub

    Private _Checkintime As Date
    Public Property Checkintime As Date
        Get
            Return _Checkintime
        End Get
        Set(ByVal value As Date)
            _Checkintime = value
        End Set
    End Property

    Private _Nachname As String
    Public Property Nachname As String
        Get
            Return _Nachname
        End Get
        Set(ByVal value As String)
            _Nachname = value
			NotifyPropertyChanged("Nachname")
			OnNachnameChanged()
        End Set
    End Property

	Private Partial Sub OnNachnameChanged()
	End Sub

    Private _Vorname As String
    Public Property Vorname As String
        Get
            Return _Vorname
        End Get
        Set(ByVal value As String)
            _Vorname = value
			NotifyPropertyChanged("Vorname")
			OnVornameChanged()
        End Set
    End Property

	Private Partial Sub OnVornameChanged()
	End Sub

    Private _Vertragsart As String
    Public Property Vertragsart As String
        Get
            Return _Vertragsart
        End Get
        Set(ByVal value As String)
            _Vertragsart = value
			NotifyPropertyChanged("Vertragsart")
			OnVertragsartChanged()
        End Set
    End Property

	Private Partial Sub OnVertragsartChanged()
	End Sub

    Private _Kennzeichen As String
    Public Property Kennzeichen As String
        Get
            Return _Kennzeichen
        End Get
        Set(ByVal value As String)
            _Kennzeichen = value
			NotifyPropertyChanged("Kennzeichen")
			OnKennzeichenChanged()
        End Set
    End Property

	Private Partial Sub OnKennzeichenChanged()
	End Sub

    Private _Foto As String
    Public Property Foto As String
        Get
            Return _Foto
        End Get
        Set(ByVal value As String)
            _Foto = value
			NotifyPropertyChanged("Foto")
			OnFotoChanged()
        End Set
    End Property

	Private Partial Sub OnFotoChanged()
	End Sub


    Private _Customer As Customer
    Public Overridable Property Customer As Customer
        Get
            Return _Customer
        End Get
        Set(ByVal value As Customer)
            _Customer = value
			NotifyPropertyChanged("Customer")
			OnCustomerChanged()
        End Set
    End Property

	Private Partial Sub OnCustomerChanged()
    End Sub


    Private _Infotext As String
    Public Property Infotext As String
        Get
            Return _Infotext
        End Get
        Set(value As String)
            _Infotext = value
            NotifyPropertyChanged("Infotext")
        End Set
    End Property

    Private _CustomerStateID As Integer
    Public Property CustomerStateID As Integer
        Get
            Return _CustomerStateID
        End Get
        Set(value As Integer)
            _CustomerStateID = value
            NotifyPropertyChanged("CustomerStateID")
        End Set
    End Property

    Private _CustomerState As CustomerState
    Public Overridable Property CustomerState As CustomerState
        Get
            Return _CustomerState
        End Get
        Set(value As CustomerState)
            _CustomerState = value
            NotifyPropertyChanged("CustomerState")
        End Set
    End Property


    Private _Billings As ObservableCollection(Of Billing)
    Public Overridable Property Billings As ObservableCollection(Of Billing)
        Get
            Return _Billings
        End Get
        Set(value As ObservableCollection(Of Billing))
            _Billings = value
        End Set
    End Property
End Class
