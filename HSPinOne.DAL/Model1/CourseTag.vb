﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Public Class CourseTag
    Inherits EntityBase

    Private _CourseTagID As Integer
    <Key>
    Public Property CourseTagID() As Integer
        Get
            Return _CourseTagID
        End Get
        Set(ByVal value As Integer)
            _CourseTagID = value
        End Set
    End Property

    Private _TagID As Integer
    Public Property TagID() As Integer
        Get
            Return _TagID
        End Get
        Set(ByVal value As Integer)
            _TagID = value
        End Set
    End Property

    Private _Tag As Tag
    <ForeignKey("TagID")>
    Public Overridable Property Tag() As Tag
        Get
            Return _Tag
        End Get
        Set(ByVal value As Tag)
            _Tag = value
        End Set
    End Property

    Private _CourseID As Integer
    Public Property CourseID() As Integer
        Get
            Return _CourseID
        End Get
        Set(ByVal value As Integer)
            _CourseID = value
        End Set
    End Property

    Private _Course As Course
    <ForeignKey("CourseID")>
    Public Overridable Property Course() As Course
        Get
            Return _Course
        End Get
        Set(ByVal value As Course)
            _Course = value
        End Set
    End Property


    Private _Tagorder As Integer = 0
    Public Property Tagorder() As Integer
        Get
            Return _Tagorder
        End Get
        Set(ByVal value As Integer)
            _Tagorder = value
        End Set
    End Property




End Class
