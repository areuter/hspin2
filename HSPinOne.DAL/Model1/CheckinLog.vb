﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel

Partial Public Class CheckinLog
    Inherits EntityBase

    Private _CheckinLogID As Integer
    Public Property CheckinLogID As Integer
        Get
            Return _CheckinLogID
        End Get
        Set(value As Integer)
            _CheckinLogID = value
        End Set
    End Property

    Private _CheckinLocation As Integer
    Public Property CheckinLocation As Integer
        Get
            Return _CheckinLocation
        End Get
        Set(value As Integer)
            _CheckinLocation = value

        End Set
    End Property

    Private _CustomerID As Nullable(Of Integer)
    Public Property CustomerID As Nullable(Of Integer)
        Get
            Return _CustomerID
        End Get
        Set(value As Nullable(Of Integer))
            _CustomerID = value
        End Set
    End Property

    Private _Customer As Customer
    Public Overridable Property Customer As Customer
        Get
            Return _Customer
        End Get
        Set(value As Customer)
            _Customer = value
        End Set
    End Property

    Private _Checkin As Date
    Public Property Checkin As Date
        Get
            Return _Checkin
        End Get
        Set(value As Date)
            _Checkin = value
        End Set
    End Property

    Private _Checkout As Date
    Public Property Checkout As Date
        Get
            Return _Checkout
        End Get
        Set(value As Date)
            _Checkout = value
        End Set
    End Property

End Class
