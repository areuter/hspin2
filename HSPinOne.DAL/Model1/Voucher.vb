﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Public Class Voucher
    Inherits EntityBase

    <Key>
    Private _VoucherID As Integer
    Public Property VoucherID() As Integer
        Get
            Return _VoucherID
        End Get
        Set(ByVal value As Integer)
            _VoucherID = value
            NotifyPropertyChanged("VoucherID")
        End Set
    End Property

    Private _Title As String
    <StringLength(100)>
    Public Property Title() As String
        Get
            Return _Title
        End Get
        Set(ByVal value As String)
            _Title = value
            NotifyPropertyChanged("Title")
        End Set
    End Property

    Private _Description As String
    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal value As String)
            _Description = value
            NotifyPropertyChanged("Description")
        End Set
    End Property

    Private _Validfrom As DateTime
    Public Property Validfrom() As DateTime
        Get
            Return _Validfrom
        End Get
        Set(ByVal value As DateTime)
            _Validfrom = value
            NotifyPropertyChanged("Validfrom")
        End Set
    End Property

    Private _Validtill As DateTime
    Public Property Validtill() As DateTime
        Get
            Return _Validtill
        End Get
        Set(ByVal value As DateTime)
            _Validtill = value
            NotifyPropertyChanged("Validtill")
        End Set
    End Property

    Private _Actiontype As Vouchertype
    Public Property Actiontype() As Vouchertype
        Get
            Return _Actiontype
        End Get
        Set(ByVal value As Vouchertype)
            _Actiontype = value
            NotifyPropertyChanged("Actiontype")
        End Set
    End Property

    Private _Discount As Decimal = 0
    Public Property Discount() As Decimal
        Get
            Return _Discount
        End Get
        Set(ByVal value As Decimal)
            _Discount = value
            NotifyPropertyChanged("Discount")
        End Set
    End Property

    Private _Vouchercodes As ObservableCollection(Of Vouchercode) = New ObservableCollection(Of Vouchercode)
    Public Overridable Property Vouchercodes() As ObservableCollection(Of Vouchercode)
        Get
            Return _Vouchercodes
        End Get
        Set(ByVal value As ObservableCollection(Of Vouchercode))
            _Vouchercodes = value
        End Set
    End Property

    Private _Username As String
    <StringLength(50)>
    Public Property Username() As String
        Get
            Return _Username
        End Get
        Set(ByVal value As String)
            _Username = value
        End Set
    End Property




End Class
