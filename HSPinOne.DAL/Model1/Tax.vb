﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations
Public Class Tax
    Inherits EntityBase

    Private _TaxID As Integer
    Public Property TaxID() As Integer
        Get
            Return _TaxID
        End Get
        Set(ByVal value As Integer)
            _TaxID = value
        End Set
    End Property

    Private _TaxName As String
    Public Property TaxName() As String
        Get
            Return _TaxName
        End Get
        Set(ByVal value As String)
            _TaxName = value
        End Set
    End Property

    Private _TaxPercent As Decimal
    Public Property TaxPercent() As Decimal
        Get
            Return _TaxPercent
        End Get
        Set(ByVal value As Decimal)
            _TaxPercent = value
        End Set
    End Property




End Class
