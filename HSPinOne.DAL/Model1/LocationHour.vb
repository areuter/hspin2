﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations
Public Class LocationHour
    Inherits EntityBase

    Private _LocationHourID As Integer
    Public Property LocationHourID As Integer
        Get
            Return _LocationHourID
        End Get
        Set(value As Integer)
            _LocationHourID = value
            NotifyPropertyChanged("LocationHourID")
        End Set
    End Property

    Private _Location As Location
    Public Overridable Property Location As Location
        Get
            Return _Location
        End Get
        Set(value As Location)
            _Location = value
            NotifyPropertyChanged("Location")
        End Set
    End Property


    Private _ValidFrom As DateTime = Now.Date
    Public Property ValidFrom As DateTime
        Get
            Return _ValidFrom
        End Get
        Set(value As DateTime)
            _ValidFrom = value
            NotifyPropertyChanged("ValidFrom")
        End Set
    End Property

    Private _ValidTill As DateTime = Now.Date
    Public Property ValidTill As DateTime
        Get
            Return _ValidTill
        End Get
        Set(value As DateTime)
            _ValidTill = value
            NotifyPropertyChanged("ValidTill")
        End Set
    End Property

    Private _Weekday As Byte = 1
    Public Property Weekday As Byte
        Get
            Return _Weekday
        End Get
        Set(value As Byte)
            _Weekday = value
            NotifyPropertyChanged("Weekday")
        End Set
    End Property

    Private _Starttime As DateTime = Now
    Public Property Starttime As DateTime
        Get
            Return _Starttime
        End Get
        Set(value As DateTime)
            _Starttime = value
            NotifyPropertyChanged("Starttime")
        End Set
    End Property


    Private _Endtime As DateTime = Now
    Public Property Endtime As DateTime
        Get
            Return _Endtime
        End Get
        Set(value As DateTime)
            _Endtime = value
            NotifyPropertyChanged("Endtime")
        End Set
    End Property

    Private _hourType
    Public Property HourType() As LocationHourType
        Get
            Return _hourType
        End Get
        Set(ByVal value As LocationHourType)
            _hourType = value
            NotifyPropertyChanged("HourType")
        End Set
    End Property


    Private _OnlineBooking As Boolean
    Public Property OnlineBooking() As Boolean
        Get
            Return _OnlineBooking
        End Get
        Set(ByVal value As Boolean)
            _OnlineBooking = value
            NotifyPropertyChanged("OnlineBooking")
        End Set
    End Property

    Private _Bemerkung As String
    <MaxLength(200)> _
Public Property Bemerkung() As String
        Get
            Return _Bemerkung
        End Get
        Set(ByVal value As String)
            _Bemerkung = value
        End Set
    End Property

    Private _LocationHourCosts As New ObservableCollection(Of LocationHourCost)
    Public Overridable Property LocationHourCosts() As ObservableCollection(Of LocationHourCost)
        Get
            Return _LocationHourCosts
        End Get
        Set(ByVal value As ObservableCollection(Of LocationHourCost))
            _LocationHourCosts = value
        End Set
    End Property

    Private _PriceGroupID As Nullable(Of Integer)
    Public Property PriceGroupID() As Nullable(Of Integer)
        Get
            Return _PriceGroupID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _PriceGroupID = value
            NotifyPropertyChanged("PriceGroupID")
        End Set
    End Property

    Private _PriceGroup As PriceGroup
    Public Property PriceGroup() As PriceGroup
        Get
            Return _PriceGroup
        End Get
        Set(ByVal value As PriceGroup)
            _PriceGroup = value
            NotifyPropertyChanged("PriceGroup")
        End Set
    End Property


End Class
