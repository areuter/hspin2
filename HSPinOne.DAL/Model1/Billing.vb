Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Partial Public Class Billing
    Inherits EntityBase

    Private _BillingID As Integer
    <Key()>
    Public Property BillingID As Integer
        Get
            Return _BillingID
        End Get
        Set(ByVal value As Integer)
            _BillingID = value
            NotifyPropertyChanged("BillingID")
            OnBillingIDChanged()
        End Set
    End Property

    Partial Private Sub OnBillingIDChanged()
    End Sub

    Private _CustomerID As Nullable(Of Integer)
    Public Property CustomerID As Nullable(Of Integer)
        Get
            Return _CustomerID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _CustomerID = value
            NotifyPropertyChanged("CustomerID")
            OnCustomerIDChanged()
        End Set
    End Property

    Partial Private Sub OnCustomerIDChanged()
    End Sub

    Private _BillingStatus As String
    <Required(AllowEmptyStrings:=False, ErrorMessage:="Status darf nicht leer sein")>
    Public Property BillingStatus As String
        Get
            Return _BillingStatus
        End Get
        Set(ByVal value As String)
            _BillingStatus = value
            NotifyPropertyChanged("BillingStatus")
            OnBillingStatusChanged()
        End Set
    End Property

    Partial Private Sub OnBillingStatusChanged()
    End Sub

    Private _ContractID As Nullable(Of Integer)
    Public Property ContractID As Nullable(Of Integer)
        Get
            Return _ContractID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _ContractID = value
            NotifyPropertyChanged("ContractID")
            OnContractIDChanged()
        End Set
    End Property

    Partial Private Sub OnContractIDChanged()
    End Sub

    Private _CourseID As Nullable(Of Integer)
    Public Property CourseID As Nullable(Of Integer)
        Get
            Return _CourseID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _CourseID = value
            NotifyPropertyChanged("CourseID")
            OnCourseIDChanged()
        End Set
    End Property

    Partial Private Sub OnCourseIDChanged()
    End Sub

    Private _AccountID As Nullable(Of Integer)
    <Required(ErrorMessage:="Konto darf nicht leer sein")>
    Public Property AccountID As Nullable(Of Integer)
        Get
            Return _AccountID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _AccountID = value
            NotifyPropertyChanged("AccountID")
            OnAccountIDChanged()
        End Set
    End Property

    Partial Private Sub OnAccountIDChanged()
    End Sub

    Private _PaymentMethodID As Integer
    <Required(ErrorMessage:="Zahlungsmethode darf nicht leer sein")>
    <Range(1, 10000, ErrorMessage:="Zahlungsmethode muss gr��er als Null sein")>
    Public Property PaymentMethodID As Integer
        Get
            Return _PaymentMethodID
        End Get
        Set(ByVal value As Integer)
            _PaymentMethodID = value
            NotifyPropertyChanged("PaymentMethodID")
            OnPaymentMethodIDChanged()
        End Set
    End Property

    Partial Private Sub OnPaymentMethodIDChanged()
    End Sub

    Private _PaymentInformation As String
    Public Property PaymentInformation As String
        Get
            Return _PaymentInformation
        End Get
        Set(value As String)
            _PaymentInformation = value
            NotifyPropertyChanged("PaymentInformation")
        End Set
    End Property

    Private _BookingID As Nullable(Of Integer)
    Public Property BookingID As Nullable(Of Integer)
        Get
            Return _BookingID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _BookingID = value
            NotifyPropertyChanged("BookingID")
            OnBookingIDChanged()
        End Set
    End Property

    Partial Private Sub OnBookingIDChanged()
    End Sub

    Private _ProductID As Nullable(Of Integer)
    Public Property ProductID As Nullable(Of Integer)
        Get
            Return _ProductID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _ProductID = value
            NotifyPropertyChanged("ProductID")
            OnProductIDChanged()
        End Set
    End Property

    Partial Private Sub OnProductIDChanged()
    End Sub

    Private _PackageID As Nullable(Of Integer)
    Public Property PackageID As Nullable(Of Integer)
        Get
            Return _PackageID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _PackageID = value
            NotifyPropertyChanged("PackageID")
            OnPackageIDChanged()
        End Set
    End Property

    Partial Private Sub OnPackageIDChanged()
    End Sub

    Private _Brutto As Decimal

    Public Property Brutto As Decimal
        Get
            Return _Brutto
        End Get
        Set(ByVal value As Decimal)
            _Brutto = value
            NotifyPropertyChanged("Brutto")
            OnBruttoChanged()
        End Set
    End Property

    Partial Private Sub OnBruttoChanged()
    End Sub

    Private _Steuerprozent As Decimal = 0
    Public Property Steuerprozent As Decimal
        Get
            Return _Steuerprozent
        End Get
        Set(ByVal value As Decimal)
            _Steuerprozent = value
            NotifyPropertyChanged("Steuerprozent")
            OnSteuerprozentChanged()
        End Set
    End Property

    Partial Private Sub OnSteuerprozentChanged()
    End Sub

    Private _Rechnungsdatum As Date
    Public Property Rechnungsdatum As Date
        Get
            Return _Rechnungsdatum
        End Get
        Set(ByVal value As Date)
            _Rechnungsdatum = value
            NotifyPropertyChanged("Rechnungsdatum")
            OnRechnungsdatumChanged()
        End Set
    End Property

    Partial Private Sub OnRechnungsdatumChanged()
    End Sub

    Private _Faelligkeit As Date
    Public Property Faelligkeit As Date
        Get
            Return _Faelligkeit
        End Get
        Set(value As Date)
            _Faelligkeit = value
            NotifyPropertyChanged("Faelligkeit")
            OnFaelligkeitChanged()
        End Set
    End Property

    Partial Private Sub OnFaelligkeitChanged()
    End Sub

    Private _Buchungsdatum As Nullable(Of Date)
    Public Property Buchungsdatum As Nullable(Of Date)
        Get
            Return _Buchungsdatum
        End Get
        Set(ByVal value As Nullable(Of Date))
            _Buchungsdatum = value
            NotifyPropertyChanged("Buchungsdatum")
            OnBuchungsdatumChanged()
        End Set
    End Property

    Partial Private Sub OnBuchungsdatumChanged()
    End Sub

    Private _Rueckbuchungsdatum As Nullable(Of Date)
    Public Property Rueckbuchungsdatum As Nullable(Of Date)
        Get
            Return _Rueckbuchungsdatum
        End Get
        Set(ByVal value As Nullable(Of Date))
            _Rueckbuchungsdatum = value
            NotifyPropertyChanged("Rueckbuchungsdatum")
            OnRueckbuchungsdatumChanged()
        End Set
    End Property

    Partial Private Sub OnRueckbuchungsdatumChanged()
    End Sub

    Private _Verwendungszweck As String
    <MaxLength(250)>
    Public Property Verwendungszweck As String
        Get
            Return _Verwendungszweck
        End Get
        Set(ByVal value As String)
            _Verwendungszweck = Left(value, 250)
            NotifyPropertyChanged("Verwendungszweck")
            OnVerwendungszweckChanged()
        End Set
    End Property

    Partial Private Sub OnVerwendungszweckChanged()
    End Sub

    Private _Bemerkung As String
    Public Property Bemerkung As String
        Get
            Return _Bemerkung
        End Get
        Set(ByVal value As String)
            _Bemerkung = value
            NotifyPropertyChanged("Bemerkung")
            OnBemerkungChanged()
        End Set
    End Property

    Partial Private Sub OnBemerkungChanged()
    End Sub

    Private _Mahnstufe As Integer
    <DefaultValue(0)>
    Public Property Mahnstufe As Integer
        Get
            Return _Mahnstufe
        End Get
        Set(value As Integer)
            _Mahnstufe = value
            NotifyPropertyChanged("Mahnstufe")
        End Set
    End Property

    Private _IsSelected As Nullable(Of Boolean)
    Public Property IsSelected As Nullable(Of Boolean)
        Get
            Return _IsSelected
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            _IsSelected = value
            NotifyPropertyChanged("IsSelected")
            OnIsSelectedChanged()
        End Set
    End Property

    Partial Private Sub OnIsSelectedChanged()
    End Sub

    Private _DtausID As Nullable(Of Integer)
    Public Property DtausID As Nullable(Of Integer)
        Get
            Return _DtausID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _DtausID = value
            NotifyPropertyChanged("DtausID")
            OnDtausIDChanged()
        End Set
    End Property

    Partial Private Sub OnDtausIDChanged()
    End Sub


    Private _IsStorno As Boolean
    Public Property IsStorno As Boolean
        Get
            Return _IsStorno
        End Get
        Set(ByVal value As Boolean)
            _IsStorno = value
            NotifyPropertyChanged("IsStorno")
            OnIsStornoChanged()
        End Set
    End Property

    Partial Private Sub OnIsStornoChanged()
    End Sub


    Private _Account As Account
    Public Overridable Property Account As Account
        Get
            Return _Account
        End Get
        Set(ByVal value As Account)
            _Account = value
            NotifyPropertyChanged("Account")
            OnAccountChanged()
        End Set
    End Property

    Partial Private Sub OnAccountChanged()
    End Sub

    Private _Contract As Contract
    Public Overridable Property Contract As Contract
        Get
            Return _Contract
        End Get
        Set(ByVal value As Contract)
            _Contract = value
            NotifyPropertyChanged("Contract")
            OnContractChanged()
        End Set
    End Property

    Partial Private Sub OnContractChanged()
    End Sub

    Private _Course As Course
    Public Overridable Property Course As Course
        Get
            Return _Course
        End Get
        Set(ByVal value As Course)
            _Course = value
            NotifyPropertyChanged("Course")
            OnCourseChanged()
        End Set
    End Property

    Partial Private Sub OnCourseChanged()
    End Sub

    Private _Package As Package
    Public Overridable Property Package As Package
        Get
            Return _Package
        End Get
        Set(ByVal value As Package)
            _Package = value
            NotifyPropertyChanged("Package")
            OnPackageChanged()
        End Set
    End Property

    Partial Private Sub OnPackageChanged()
    End Sub

    Private _Packagecredit As Nullable(Of Integer)
    Public Property Packagecredit As Nullable(Of Integer)
        Get
            Return _Packagecredit
        End Get
        Set(value As Nullable(Of Integer))
            _Packagecredit = value
            NotifyPropertyChanged("Packagecredit")
            OnPackagecreditChanged()
        End Set
    End Property

    Partial Private Sub OnPackagecreditChanged()
    End Sub

    Private _PaymentMethod As PaymentMethod
    Public Overridable Property PaymentMethod As PaymentMethod
        Get
            Return _PaymentMethod
        End Get
        Set(ByVal value As PaymentMethod)
            _PaymentMethod = value
            NotifyPropertyChanged("PaymentMethod")
            OnPaymentMethodChanged()
        End Set
    End Property

    Partial Private Sub OnPaymentMethodChanged()
    End Sub

    Private _Product As Product
    Public Overridable Property Product As Product
        Get
            Return _Product
        End Get
        Set(ByVal value As Product)
            _Product = value
            NotifyPropertyChanged("Product")
            OnProductChanged()
        End Set
    End Property

    Partial Private Sub OnProductChanged()
    End Sub

    Private _Customer As Customer
    Public Overridable Property Customer As Customer
        Get
            Return _Customer
        End Get
        Set(ByVal value As Customer)
            _Customer = value
            NotifyPropertyChanged("Customer")
            OnCustomerChanged()
        End Set
    End Property

    Partial Private Sub OnCustomerChanged()
    End Sub

    Private _Comment As String
    Public Property Comment As String
        Get
            Return _Comment
        End Get
        Set(ByVal value As String)
            _Comment = value
            NotifyPropertyChanged("Comment")
            OnCommentChanged()
        End Set
    End Property

    Partial Private Sub OnCommentChanged()
    End Sub

    Private _PackageLogs As ObservableCollection(Of PackageLog) = New ObservableCollection(Of PackageLog)
    Public Overridable Property PackageLogs As ObservableCollection(Of PackageLog)
        Get
            Return _PackageLogs
        End Get
        Set(ByVal value As ObservableCollection(Of PackageLog))
            _PackageLogs = value
            NotifyPropertyChanged("PackageLogs")
            OnPackageLogsChanged()
        End Set
    End Property

    Partial Private Sub OnPackageLogsChanged()
    End Sub

    Private _CostcenterID As Integer
    <Required(ErrorMessage:="Kostenstelle darf nicht leer sein")>
    <Range(1, 1000000, ErrorMessage:="Kostenstelle muss gr��er als Null sein")>
    Public Property CostcenterID As Integer
        Get
            Return _CostcenterID
        End Get
        Set(value As Integer)
            _CostcenterID = value
        End Set
    End Property

    Private _Costcenter As Costcenter
    Public Overridable Property Costcenter As Costcenter
        Get
            Return _Costcenter
        End Get
        Set(ByVal value As Costcenter)
            _Costcenter = value
        End Set
    End Property

    Private _CheckinID As Nullable(Of Integer)
    Public Property CheckinID As Nullable(Of Integer)
        Get
            Return _CheckinID
        End Get
        Set(value As Nullable(Of Integer))
            _CheckinID = value
            NotifyPropertyChanged("CheckinID")
        End Set
    End Property

    Private _Checkin As Checkin
    Public Overridable Property Checkin As Checkin
        Get
            Return _Checkin
        End Get
        Set(value As Checkin)
            _Checkin = value
        End Set
    End Property


    Private _EndToEnd As String
    Public Property EndToEnd As String
        Get
            Return _EndToEnd
        End Get
        Set(value As String)
            _EndToEnd = value
            NotifyPropertyChanged("EndToEnd")
        End Set
    End Property


    Private _ProductTypeID As Integer
    <Required(ErrorMessage:="Warengruppe darf nicht leer sein")>
    <Range(1, 1000000, ErrorMessage:="Warengruppe muss gr��er als Null sein")>
    Public Property ProductTypeID As Integer
        Get
            Return _ProductTypeID
        End Get
        Set(value As Integer)
            _ProductTypeID = value
            NotifyPropertyChanged("ProductTypeID")
        End Set
    End Property

    Private _ProductType As ProductType
    Public Overridable Property ProductType As ProductType
        Get
            Return _ProductType
        End Get
        Set(value As ProductType)
            _ProductType = value
            NotifyPropertyChanged("ProductType")
        End Set
    End Property

    Private _CustomerStateID As Integer
    Public Property CustomerStateID As Integer
        Get
            Return _CustomerStateID
        End Get
        Set(value As Integer)
            _CustomerStateID = value
            NotifyPropertyChanged("CustomerStateID")
        End Set
    End Property

    Private _CustomerState As CustomerState
    Public Property CustomerState As CustomerState
        Get
            Return _CustomerState
        End Get
        Set(value As CustomerState)
            _CustomerState = value
            NotifyPropertyChanged("CustomerState")
        End Set
    End Property

    Private _Vorgang As Guid
    Public Property Vorgang As Guid
        Get
            Return _Vorgang
        End Get
        Set(value As Guid)
            _Vorgang = value
            NotifyPropertyChanged("Vorgang")
        End Set
    End Property

    Private _CancellationByUser As Boolean = False
    Public Property CancellationByUser() As Boolean
        Get
            Return _CancellationByUser
        End Get
        Set(ByVal value As Boolean)
            _CancellationByUser = value
            NotifyPropertyChanged("CancellationByUser")
        End Set
    End Property

    Private _CashClosingID As Nullable(Of Integer)
    Public Property CashClosingID() As Nullable(Of Integer)
        Get
            Return _CashClosingID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _CashClosingID = value
        End Set
    End Property

    <DataAnnotations.Schema.ForeignKey("CashClosingID")>
    Private _CashClosing As CashClosing
    Public Overridable Property CashClosing() As CashClosing
        Get
            Return _CashClosing
        End Get
        Set(ByVal value As CashClosing)
            _CashClosing = value
        End Set
    End Property


    Private _Innenauftrag As String
    <MaxLength(30)>
    Public Property Innenauftrag() As String
        Get
            Return _Innenauftrag
        End Get
        Set(ByVal value As String)
            _Innenauftrag = value
            NotifyPropertyChanged("Innenauftrag")
        End Set
    End Property

    Private _Sachkonto As String
    <MaxLength(30)>
    Public Property Sachkonto() As String
        Get
            Return _Sachkonto
        End Get
        Set(ByVal value As String)
            _Sachkonto = value
            NotifyPropertyChanged("Sachkonto")
        End Set
    End Property

    Private _KSTextern As String
    <MaxLength(30)>
    Public Property KSTextern() As String
        Get
            Return _KSTextern
        End Get
        Set(ByVal value As String)
            _KSTextern = value
            NotifyPropertyChanged("KSTextern")
        End Set
    End Property

    Private _Created As DateTime = Now
    Public Property Created() As DateTime
        Get
            Return _Created
        End Get
        Set(ByVal value As DateTime)
            _Created = value
            NotifyPropertyChanged("Created")
        End Set
    End Property

    Private _CreatedBy As String
    Public Property CreatedBy() As String
        Get
            Return _CreatedBy
        End Get
        Set(ByVal value As String)
            _CreatedBy = value
            NotifyPropertyChanged("CreatedBy")
        End Set
    End Property

    Private _LastModified As Nullable(Of DateTime)
    Public Property LastModified() As Nullable(Of DateTime)
        Get
            Return _LastModified
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            _LastModified = value
            NotifyPropertyChanged("LastModified")
        End Set
    End Property

    Private _LastModifiedBy As String
    Public Property LastModifiedBy() As String
        Get
            Return _LastModifiedBy
        End Get
        Set(ByVal value As String)
            _LastModifiedBy = value
            NotifyPropertyChanged("LastModifiedBy")
        End Set
    End Property

    Private _KasseNr As Integer?
    Public Property KasseNr() As Integer?
        Get
            Return _KasseNr
        End Get
        Set(ByVal value As Integer?)
            _KasseNr = value
            NotifyPropertyChanged("KasseNr")
        End Set
    End Property

    Private _Steuerkennzeichen As String
    <MaxLength(20)>
    Public Property Steuerkennzeichen() As String
        Get
            Return _Steuerkennzeichen
        End Get
        Set(ByVal value As String)
            _Steuerkennzeichen = value
            NotifyPropertyChanged("Steuerkennzeichen")
        End Set
    End Property

    Private _AccountingTemplateID As Nullable(Of Integer)
    Public Property AccountingTemplateID() As Nullable(Of Integer)
        Get
            Return _AccountingTemplateID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _AccountingTemplateID = value
            NotifyPropertyChanged("AccountingTemplateID")
        End Set
    End Property

    Private _AccountingTemplate As AccountingTemplate
    <ForeignKey("AccountingTemplateID")>
    Public Property AccountingTemplate() As AccountingTemplate
        Get
            Return _AccountingTemplate
        End Get
        Set(ByVal value As AccountingTemplate)
            _AccountingTemplate = value
            NotifyPropertyChanged("AccountingTemplate")
        End Set
    End Property

    Private _Hash As String
    Public Property Hash() As String
        Get
            Return _Hash
        End Get
        Set(ByVal value As String)
            _Hash = value
        End Set
    End Property

    Private _BruttoOriginal As Decimal
    Public Property BruttoOriginal() As Decimal
        Get
            Return _BruttoOriginal
        End Get
        Set(ByVal value As Decimal)
            _BruttoOriginal = value
            NotifyPropertyChanged("BruttoOriginal")
        End Set
    End Property

    Private _VouchercodeID As Nullable(Of Integer)
    Public Property VouchercodeID() As Nullable(Of Integer)
        Get
            Return _VouchercodeID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _VouchercodeID = value
            NotifyPropertyChanged("Vouchercode")
        End Set
    End Property

    Private _BillingReceiptID As Integer?
    Public Property BillingReceiptID() As Integer?
        Get
            Return _BillingReceiptID
        End Get
        Set(ByVal value As Integer?)
            _BillingReceiptID = value
        End Set
    End Property

    <ForeignKey("BillingReceiptID")>
    Public Overridable Property BillingReceipt As BillingReceipt




End Class
