
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations

Partial Public Class GlobalSetting
    Inherits EntityBase

    Private _GlobalSettingID As Integer
    Public Property GlobalSettingID As Integer
        Get
            Return _GlobalSettingID
        End Get
        Set(ByVal value As Integer)
            _GlobalSettingID = value
        End Set
    End Property

    Private _Einstellung As String
    Public Property Einstellung As String
        Get
            Return _Einstellung
        End Get
        Set(ByVal value As String)
            _Einstellung = value
			NotifyPropertyChanged("Einstellung")
			OnEinstellungChanged()
        End Set
    End Property

	Private Partial Sub OnEinstellungChanged()
    End Sub

    Private _Preferencename As String
    <MaxLength(100)> _
    <Schema.Index> _
    Public Property Preferencename() As String
        Get
            Return _Preferencename
        End Get
        Set(ByVal value As String)
            _Preferencename = value
        End Set
    End Property


    Private _Wert As String
    Public Property Wert As String
        Get
            Return _Wert
        End Get
        Set(ByVal value As String)
            _Wert = value
			NotifyPropertyChanged("Wert")
			OnWertChanged()
        End Set
    End Property

	Private Partial Sub OnWertChanged()
	End Sub

    Private _Bezeichnung As String
    Public Property Bezeichnung As String
        Get
            Return _Bezeichnung
        End Get
        Set(ByVal value As String)
            _Bezeichnung = value
			NotifyPropertyChanged("Bezeichnung")
			OnBezeichnungChanged()
        End Set
    End Property

	Private Partial Sub OnBezeichnungChanged()
	End Sub

    Private _Beschreibung As String
    Public Property Beschreibung As String
        Get
            Return _Beschreibung
        End Get
        Set(ByVal value As String)
            _Beschreibung = value
			NotifyPropertyChanged("Beschreibung")
			OnBeschreibungChanged()
        End Set
    End Property

	Private Partial Sub OnBeschreibungChanged()
    End Sub

    Private _Typ As String
    <MaxLength(250)> _
    Public Property Typ() As String
        Get
            Return _Typ
        End Get
        Set(ByVal value As String)
            _Typ = value
        End Set
    End Property


End Class
