﻿Imports System.ComponentModel.DataAnnotations

Public Class ContractTimestop
    Inherits EntityBase

    Private _ContractTimestopID As Integer
    Public Property ContractTimestopID As Integer
        Get
            Return _ContractTimestopID
        End Get
        Set(value As Integer)
            _ContractTimestopID = value
            NotifyPropertyChanged("ContractTimestop")
        End Set
    End Property

    Private _ContractID As Integer
    Public Property ContractID As Integer
        Get
            Return _ContractID
        End Get
        Set(value As Integer)
            _ContractID = value
            NotifyPropertyChanged("ContractID")
        End Set
    End Property

    Private _Contract As Contract
    Public Property Contract As Contract
        Get
            Return _Contract
        End Get
        Set(value As Contract)
            _Contract = value
            NotifyPropertyChanged("Contract")
        End Set
    End Property

    Private _TStart As DateTime
    Public Property TStart As DateTime
        Get
            Return _TStart
        End Get
        Set(value As DateTime)
            _TStart = value
            NotifyPropertyChanged("TStart")
        End Set
    End Property

    Private _TEnd As DateTime
    Public Property TEnd As DateTime
        Get
            Return _TEnd
        End Get
        Set(value As DateTime)
            _TEnd = value
            NotifyPropertyChanged("TEnd")
        End Set
    End Property

    Private _Reason As String
    <MaxLength(150)> _
    Public Property Reason As String
        Get
            Return _Reason
        End Get
        Set(value As String)
            _Reason = value
            NotifyPropertyChanged("Reason")
        End Set
    End Property


End Class
