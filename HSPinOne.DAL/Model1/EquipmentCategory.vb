﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel

Public Class EquipmentCategory
    Inherits EntityBase

    Private _EquipmentCategoryID As Integer
    Public Property EquipmentCategoryID As Integer
        Get
            Return _EquipmentCategoryID
        End Get
        Set(ByVal value As Integer)
            _EquipmentCategoryID = value
            NotifyPropertyChanged("EquipmentCategoryID")
            OnEquipmentCategoryIDChanged()
        End Set
    End Property

    Partial Private Sub OnEquipmentCategoryIDChanged()
    End Sub

    Private _EquipmentCategoryName As String
    Public Property EquipmentCategoryName As String
        Get
            Return _EquipmentCategoryName
        End Get
        Set(ByVal value As String)
            _EquipmentCategoryName = value
            NotifyPropertyChanged("EquipmentCategoryName")
            OnEquipmentCategoryNameChanged()
        End Set
    End Property

    Partial Private Sub OnEquipmentCategoryNameChanged()
    End Sub

    Private _EquipmentCategoryOrder As Integer
    Public Property EquipmentCategoryCategory As Integer
        Get
            Return _EquipmentCategoryOrder
        End Get
        Set(ByVal value As Integer)
            _EquipmentCategoryOrder = value
            NotifyPropertyChanged("EquipmentCategoryOrder")
            OnEquipmentCategoryOrderChanged()
        End Set
    End Property

    Partial Private Sub OnEquipmentCategoryOrderChanged()
    End Sub

End Class


