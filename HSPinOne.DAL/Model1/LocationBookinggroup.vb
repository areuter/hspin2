﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Public Class LocationBookinggroup
    Inherits EntityBase

    Private _LocationBookinggroupID As Integer
    Public Property LocationBookinggroupID() As Integer
        Get
            Return _LocationBookinggroupID
        End Get
        Set(ByVal value As Integer)
            _LocationBookinggroupID = value
        End Set
    End Property

    Private _Groupname As String
    Public Property Groupname() As String
        Get
            Return _Groupname
        End Get
        Set(ByVal value As String)
            _Groupname = value
        End Set
    End Property

    Private _Periodbookingfrom As DateTime
    Public Property Periodbookingfrom() As DateTime
        Get
            Return _Periodbookingfrom
        End Get
        Set(ByVal value As DateTime)
            _Periodbookingfrom = value
        End Set
    End Property

    Private _Periodbookingtill As DateTime
    Public Property Periodbookingtill() As DateTime
        Get
            Return _Periodbookingtill
        End Get
        Set(ByVal value As DateTime)
            _Periodbookingtill = value
        End Set
    End Property

    Private _Periodbookingcount As Integer = 0
    Public Property Periodbookingcount() As Integer
        Get
            Return _Periodbookingcount
        End Get
        Set(ByVal value As Integer)
            _Periodbookingcount = value
        End Set
    End Property

    Private _Periodbookingdiscount As Decimal = 0
    Public Property Periodbookingdiscount() As Decimal
        Get
            Return _Periodbookingdiscount
        End Get
        Set(ByVal value As Decimal)
            _Periodbookingdiscount = value
        End Set
    End Property

    Private _TemplateID As Integer?
    Public Property TemplateID() As Integer?
        Get
            Return _TemplateID
        End Get
        Set(ByVal value As Integer?)
            _TemplateID = value
        End Set
    End Property

    Private _Template As Template
    <ForeignKey("TemplateID")>
    Public Overridable Property Template() As Template
        Get
            Return _Template
        End Get
        Set(ByVal value As Template)
            _Template = value
        End Set
    End Property


End Class
