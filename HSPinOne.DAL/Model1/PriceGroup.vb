﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Public Class PriceGroup
    Inherits EntityBase


    Private _PriceGroupID As Integer
    Public Property PriceGroupID() As Integer
        Get
            Return _PriceGroupID
        End Get
        Set(ByVal value As Integer)
            _PriceGroupID = value
        End Set
    End Property

    Private _PriceGroupname As String
    Public Property PriceGroupname() As String
        Get
            Return _PriceGroupname
        End Get
        Set(ByVal value As String)
            _PriceGroupname = value
        End Set
    End Property

    Private _IsActive As Boolean
    Public Property IsActive() As Boolean
        Get
            Return _IsActive
        End Get
        Set(ByVal value As Boolean)
            _IsActive = value
        End Set
    End Property

    Private _PriceGroupPrices As ObservableCollection(Of PriceGroupPrice) = New ObservableCollection(Of PriceGroupPrice)
    Public Overridable Property PriceGroupPrices() As ObservableCollection(Of PriceGroupPrice)
        Get
            Return _PriceGroupPrices
        End Get
        Set(ByVal value As ObservableCollection(Of PriceGroupPrice))
            _PriceGroupPrices = value
        End Set
    End Property


End Class
