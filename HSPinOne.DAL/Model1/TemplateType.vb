﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel

Public Class TemplateType
    Inherits EntityBase

    Private _TemplatetypeID As Integer
    Public Property TemplatetypeID As Integer
        Get
            Return _TemplatetypeID
        End Get
        Set(ByVal value As Integer)
            _TemplatetypeID = value
            NotifyPropertyChanged("TemplatetypeID")
            OnTemplatetypeIDChanged()
        End Set
    End Property

    Partial Private Sub OnTemplatetypeIDChanged()
    End Sub

    Private _Templatetypename As String
    Public Property Templatetypename As String
        Get
            Return _Templatetypename
        End Get
        Set(ByVal value As String)
            _Templatetypename = value
            NotifyPropertyChanged("Templatetypename")
            OnTemplatetypeNameChanged()
        End Set
    End Property

    Partial Private Sub OnTemplatetypenameChanged()
    End Sub

End Class
