﻿

Public Class AdminAccess
    Inherits EntityBase


    Private _AdminAccessID As Integer
    Public Property AdminAccessID As Integer
        Get
            Return _AdminAccessID
        End Get
        Set(value As Integer)
            _AdminAccessID = value
        End Set
    End Property

    Private _CustomerID As Integer
    Public Property CustomerID As Integer
        Get
            Return _CustomerID
        End Get
        Set(value As Integer)
            _CustomerID = value
        End Set
    End Property

    Private _Customer As Customer
    Public Overridable Property Customer As Customer
        Get
            Return _Customer
        End Get
        Set(value As Customer)
            _Customer = value
        End Set
    End Property

    Private _Bemerkung As String
    Public Property Bemerkung As String
        Get
            Return _Bemerkung
        End Get
        Set(value As String)
            _Bemerkung = value
        End Set
    End Property

End Class
