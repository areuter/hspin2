

Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations.Schema

Partial Public Class Package
    Inherits EntityBase

    Private _PackageID As Integer
    Public Property PackageID As Integer
        Get
            Return _PackageID
        End Get
        Set(ByVal value As Integer)
            _PackageID = value
            NotifyPropertyChanged("PackageID")
            OnPackageIDChanged()
        End Set
    End Property

    Partial Private Sub OnPackageIDChanged()
    End Sub

    Private _PackageName As String
    Public Property PackageName As String
        Get
            Return _PackageName
        End Get
        Set(ByVal value As String)
            _PackageName = Trim(value)
            NotifyPropertyChanged("PackageName")
            OnPackageNameChanged()
        End Set
    End Property

    Partial Private Sub OnPackageNameChanged()
    End Sub

    Private _Zutritte As Integer
    Public Property Zutritte As Integer
        Get
            Return _Zutritte
        End Get
        Set(ByVal value As Integer)
            _Zutritte = value
            NotifyPropertyChanged("Zutritte")
            OnZutritteChanged()
        End Set
    End Property

    Partial Private Sub OnZutritteChanged()
    End Sub


    Private _Billings As ObservableCollection(Of Billing) = New ObservableCollection(Of Billing)
    Public Overridable Property Billings As ObservableCollection(Of Billing)
        Get
            Return _Billings
        End Get
        Set(ByVal value As ObservableCollection(Of Billing))
            _Billings = value
            NotifyPropertyChanged("Billings")
            OnBillingsChanged()
        End Set
    End Property

    Partial Private Sub OnBillingsChanged()
    End Sub

    Private _PackagePrices As ObservableCollection(Of PackagePrice) = New ObservableCollection(Of PackagePrice)
    Public Overridable Property PackagePrices As ObservableCollection(Of PackagePrice)
        Get
            Return _PackagePrices
        End Get
        Set(ByVal value As ObservableCollection(Of PackagePrice))
            _PackagePrices = value
            NotifyPropertyChanged("PackagePrices")
            OnPackagePricesChanged()
        End Set
    End Property

    Partial Private Sub OnPackagePricesChanged()
    End Sub

    Private _CostcenterID As Integer
    Public Property CostcenterID As Integer
        Get
            Return _CostcenterID
        End Get
        Set(value As Integer)
            _CostcenterID = value
        End Set
    End Property

    Private _Costcenter As Costcenter
    Public Overloads Property Costcenter As Costcenter
        Get
            Return _Costcenter
        End Get
        Set(value As Costcenter)
            _Costcenter = value
        End Set
    End Property

    Private _ProductTypeID As Integer
    Public Property ProductTypeID As Integer
        Get
            Return _ProductTypeID
        End Get
        Set(value As Integer)
            _ProductTypeID = value
            NotifyPropertyChanged("ProductTypeID")
        End Set
    End Property

    Private _ProductType As ProductType
    Public Overridable Property ProductType As ProductType
        Get
            Return _ProductType
        End Get
        Set(value As ProductType)
            _ProductType = value
            NotifyPropertyChanged("ProductType")
        End Set
    End Property

    Private _CategoryID As Integer
    Public Property CategoryID As Integer
        Get
            Return _CategoryID
        End Get
        Set(value As Integer)
            _CategoryID = value
            NotifyPropertyChanged("CategoryID")
        End Set
    End Property

    Private _Category As Category
    Public Overridable Property Category As Category
        Get
            Return _Category
        End Get
        Set(value As Category)
            _Category = value
            NotifyPropertyChanged("Category")
        End Set
    End Property

    Private _Position As Integer = 0
    Public Property Position As Integer
        Get
            Return _Position
        End Get
        Set(value As Integer)
            _Position = value
            NotifyPropertyChanged("Position")
        End Set
    End Property

    Private _IsActive As Boolean = False
    Public Property IsActive() As Boolean
        Get
            Return _IsActive
        End Get
        Set(ByVal value As Boolean)
            _IsActive = value
            NotifyPropertyChanged("IsActive")
        End Set
    End Property

    Private _IsOnlineAvailable As Boolean = False
    Public Property IsOnlineAvailable() As Boolean
        Get
            Return _IsOnlineAvailable
        End Get
        Set(ByVal value As Boolean)
            _IsOnlineAvailable = value
            NotifyPropertyChanged("IsOnlineAvailable")
        End Set
    End Property


    Private _TemplateID As Nullable(Of Integer)
    Public Property TemplateID() As Nullable(Of Integer)
        Get
            Return _TemplateID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _TemplateID = value
        End Set
    End Property

    Private _Template As Template
    <ForeignKey("TemplateID")>
    Public Overridable Property NewProperty() As Template
        Get
            Return _Template
        End Get
        Set(ByVal value As Template)
            _Template = value
        End Set
    End Property




End Class
