﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations

Public Class Tag
    Inherits EntityBase


    Private _TagID As Integer
    <Key>
    Public Property TagID() As Integer
        Get
            Return _TagID
        End Get
        Set(ByVal value As Integer)
            _TagID = value
        End Set
    End Property

    Private _Tagname As String
    Public Property Tagname() As String
        Get
            Return _Tagname
        End Get
        Set(ByVal value As String)
            _Tagname = value
        End Set
    End Property

    Private _Slug As String
    Public Property Slug() As String
        Get
            Return _Slug
        End Get
        Set(ByVal value As String)
            _Slug = value
        End Set
    End Property

    Private _Taggroup As Integer = 0
    Public Property Taggroup() As Integer
        Get
            Return _Taggroup
        End Get
        Set(ByVal value As Integer)
            _Taggroup = value
        End Set
    End Property

    Private _Tagparent As Integer = 0
    Public Property Tagparent() As Integer
        Get
            Return _Tagparent
        End Get
        Set(ByVal value As Integer)
            _Tagparent = value
        End Set
    End Property

    Private _Hits As Integer = 0
    Public Property Hits() As Integer
        Get
            Return _Hits
        End Get
        Set(ByVal value As Integer)
            _Hits = value
        End Set
    End Property

End Class
