Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Partial Public Class Customer
         Inherits EntityBase

    Private _CustomerID As Integer
    <Key()> _
    Public Property CustomerID As Integer
        Get
            Return _CustomerID
        End Get
        Set(ByVal value As Integer)
            _CustomerID = value
            NotifyPropertyChanged("CustomerID")
            OnCustomerIDChanged()
        End Set
    End Property

	Private Partial Sub OnCustomerIDChanged()
	End Sub

    Private _CustomerStateID As Integer
    Public Property CustomerStateID As Integer
        Get
            Return _CustomerStateID
        End Get
        Set(ByVal value As Integer)
            _CustomerStateID = value
			NotifyPropertyChanged("CustomerStateID")
			OnCustomerStateIDChanged()
        End Set
    End Property

	Private Partial Sub OnCustomerStateIDChanged()
	End Sub

    Private _Gender As String
    Public Property Gender As String
        Get
            Return _Gender
        End Get
        Set(ByVal value As String)
            _Gender = value
			NotifyPropertyChanged("Gender")
			OnGenderChanged()
        End Set
    End Property

	Private Partial Sub OnGenderChanged()
	End Sub

    Private _Vorname As String
    Public Property Vorname As String
        Get
            Return _Vorname
        End Get
        Set(ByVal value As String)
            _Vorname = value
			NotifyPropertyChanged("Vorname")
			OnVornameChanged()
        End Set
    End Property

	Private Partial Sub OnVornameChanged()
	End Sub

    Private _Nachname As String
    Public Property Nachname As String
        Get
            Return _Nachname
        End Get
        Set(ByVal value As String)
            _Nachname = value
			NotifyPropertyChanged("Nachname")
			OnNachnameChanged()
        End Set
    End Property

	Private Partial Sub OnNachnameChanged()
	End Sub

    Private _Titel As String
    Public Property Titel As String
        Get
            Return _Titel
        End Get
        Set(ByVal value As String)
            _Titel = value
			NotifyPropertyChanged("Titel")
			OnTitelChanged()
        End Set
    End Property

	Private Partial Sub OnTitelChanged()
	End Sub

    Private _Suchname As String
    Public Property Suchname As String
        Get
            Return _Suchname
        End Get
        Set(ByVal value As String)
            _Suchname = value
			NotifyPropertyChanged("Suchname")
			OnSuchnameChanged()
        End Set
    End Property

	Private Partial Sub OnSuchnameChanged()
	End Sub

    Private _Strasse As String
    Public Property Strasse As String
        Get
            Return _Strasse
        End Get
        Set(ByVal value As String)
            _Strasse = value
			NotifyPropertyChanged("Strasse")
			OnStrasseChanged()
        End Set
    End Property

	Private Partial Sub OnStrasseChanged()
	End Sub

    Private _PLZ As String
    Public Property PLZ As String
        Get
            Return _PLZ
        End Get
        Set(ByVal value As String)
            _PLZ = value
			NotifyPropertyChanged("PLZ")
			OnPLZChanged()
        End Set
    End Property

	Private Partial Sub OnPLZChanged()
	End Sub

    Private _Ort As String
    Public Property Ort As String
        Get
            Return _Ort
        End Get
        Set(ByVal value As String)
            _Ort = value
			NotifyPropertyChanged("Ort")
			OnOrtChanged()
        End Set
    End Property

	Private Partial Sub OnOrtChanged()
    End Sub

    Private _CountryID As Integer
    Public Property CountryID As Integer
        Get
            Return _CountryID
        End Get
        Set(ByVal value As Integer)
            _CountryID = value
            NotifyPropertyChanged("CountryID")
            OnCountryIDChanged()
        End Set
    End Property

    Partial Private Sub OnCountryIDChanged()
    End Sub

    Private _Geburtstag As Nullable(Of Date)
    Public Property Geburtstag As Nullable(Of Date)
        Get
            Return _Geburtstag
        End Get
        Set(ByVal value As Nullable(Of Date))
            _Geburtstag = value
			NotifyPropertyChanged("Geburtstag")
			OnGeburtstagChanged()
        End Set
    End Property

	Private Partial Sub OnGeburtstagChanged()
	End Sub

    Private _Briefanrede As String
    Public Property Briefanrede As String
        Get
            Return _Briefanrede
        End Get
        Set(ByVal value As String)
            _Briefanrede = value
			NotifyPropertyChanged("Briefanrede")
			OnBriefanredeChanged()
        End Set
    End Property

	Private Partial Sub OnBriefanredeChanged()
	End Sub

    Private _Telefon1IsPublic As Boolean
    Public Property Telefon1IsPublic As Boolean
        Get
            Return _Telefon1IsPublic
        End Get
        Set(ByVal value As Boolean)
            _Telefon1IsPublic = value
            NotifyPropertyChanged("Telefon1IsPublic")
        End Set
    End Property

    Private _Telefon1 As String
    Public Property Telefon1 As String
        Get
            Return _Telefon1
        End Get
        Set(ByVal value As String)
            _Telefon1 = value
			NotifyPropertyChanged("Telefon1")
			OnTelefon1Changed()
        End Set
    End Property

	Private Partial Sub OnTelefon1Changed()
	End Sub


    Private _Telefon2IsPublic As Boolean
    Public Property Telefon2IsPublic As Boolean
        Get
            Return _Telefon2IsPublic
        End Get
        Set(ByVal value As Boolean)
            _Telefon2IsPublic = value
            NotifyPropertyChanged("Telefon2IsPublic")
        End Set
    End Property

    Private _Telefon2 As String
    Public Property Telefon2 As String
        Get
            Return _Telefon2
        End Get
        Set(ByVal value As String)
            _Telefon2 = value
			NotifyPropertyChanged("Telefon2")
			OnTelefon2Changed()
        End Set
    End Property

	Private Partial Sub OnTelefon2Changed()
    End Sub


    Private _Telefon3IsPublic As Boolean
    Public Property Telefon3IsPublic As Boolean
        Get
            Return _Telefon3IsPublic
        End Get
        Set(ByVal value As Boolean)
            _Telefon3IsPublic = value
            NotifyPropertyChanged("Telefon3IsPublic")
        End Set
    End Property

    Private _Telefon3 As String
    Public Property Telefon3 As String
        Get
            Return _Telefon3
        End Get
        Set(ByVal value As String)
            _Telefon3 = value
			NotifyPropertyChanged("Telefon3")
			OnTelefon3Changed()
        End Set
    End Property

	Private Partial Sub OnTelefon3Changed()
	End Sub


    Private _Mail1IsPublic As Boolean
    Public Property Mail1IsPublic As Boolean
        Get
            Return _Mail1IsPublic
        End Get
        Set(ByVal value As Boolean)
            _Mail1IsPublic = value
            NotifyPropertyChanged("Mail1IsPublic")
        End Set
    End Property

    Private _Mail1 As String
    Public Property Mail1 As String
        Get
            Return _Mail1
        End Get
        Set(ByVal value As String)
            _Mail1 = value
			NotifyPropertyChanged("Mail1")
			OnMail1Changed()
        End Set
    End Property

	Private Partial Sub OnMail1Changed()
	End Sub


    Private _Mail2IsPublic As Boolean
    Public Property Mail2IsPublic As Boolean
        Get
            Return _Mail2IsPublic
        End Get
        Set(ByVal value As Boolean)
            _Mail2IsPublic = value
            NotifyPropertyChanged("Mail2IsPublic")
        End Set
    End Property

    Private _Mail2 As String
    Public Property Mail2 As String
        Get
            Return _Mail2
        End Get
        Set(ByVal value As String)
            _Mail2 = value
			NotifyPropertyChanged("Mail2")
			OnMail2Changed()
        End Set
    End Property

	Private Partial Sub OnMail2Changed()
	End Sub

    Private _Newsletter As Nullable(Of Boolean)
    Public Property Newsletter As Nullable(Of Boolean)
        Get
            Return _Newsletter
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            _Newsletter = value
			NotifyPropertyChanged("Newsletter")
			OnNewsletterChanged()
        End Set
    End Property

	Private Partial Sub OnNewsletterChanged()
	End Sub

    Private _LSVEnabled As Boolean = True
    Public Property LSVEnabled As Boolean
        Get
            Return _LSVEnabled
        End Get
        Set(ByVal value As Boolean)
            _LSVEnabled = value
            NotifyPropertyChanged("LSVEnabled")
            OnLSVEnabledChanged()
        End Set
    End Property

	Private Partial Sub OnLSVEnabledChanged()
	End Sub

    Private _Kontoinhaber As String
    Public Property Kontoinhaber As String
        Get
            Return _Kontoinhaber
        End Get
        Set(ByVal value As String)
            _Kontoinhaber = value
			NotifyPropertyChanged("Kontoinhaber")
			OnKontoinhaberChanged()
        End Set
    End Property

	Private Partial Sub OnKontoinhaberChanged()
	End Sub

    Private _Kontonummer As Nullable(Of Long)
    Public Property Kontonummer As Nullable(Of Long)
        Get
            Return _Kontonummer
        End Get
        Set(ByVal value As Nullable(Of Long))
            _Kontonummer = value
			NotifyPropertyChanged("Kontonummer")
			OnKontonummerChanged()
        End Set
    End Property

	Private Partial Sub OnKontonummerChanged()
	End Sub

    Private _BLZ As Nullable(Of Integer)
    Public Property BLZ As Nullable(Of Integer)
        Get
            Return _BLZ
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _BLZ = value
			NotifyPropertyChanged("BLZ")
			OnBLZChanged()
        End Set
    End Property

	Private Partial Sub OnBLZChanged()
	End Sub

    Private _IBAN As String
    Public Property IBAN As String
        Get
            Return _IBAN
        End Get
        Set(ByVal value As String)
            _IBAN = value
			NotifyPropertyChanged("IBAN")
        End Set
    End Property

	Private Partial Sub OnIBANChanged()
	End Sub

    Private _BIC As String
    Public Property BIC As String
        Get
            Return _BIC
        End Get
        Set(ByVal value As String)
            _BIC = value
			NotifyPropertyChanged("BIC")
        End Set
    End Property

	Private Partial Sub OnBICChanged()
	End Sub

    Private _Kartennummer As Nullable(Of Long)
    Public Property Kartennummer As Nullable(Of Long)
        Get
            Return _Kartennummer
        End Get
        Set(ByVal value As Nullable(Of Long))
            _Kartennummer = value
			NotifyPropertyChanged("Kartennummer")
			OnKartennummerChanged()
        End Set
    End Property

	Private Partial Sub OnKartennummerChanged()
	End Sub

    Private _Bemerkung As String
    Public Property Bemerkung As String
        Get
            Return _Bemerkung
        End Get
        Set(ByVal value As String)
            _Bemerkung = value
			NotifyPropertyChanged("Bemerkung")
			OnBemerkungChanged()
        End Set
    End Property

	Private Partial Sub OnBemerkungChanged()
    End Sub

    Private _Bearbeitungsvermerk As String
    Public Property Bearbeitungsvermerk As String
        Get
            Return _Bearbeitungsvermerk
        End Get
        Set(value As String)
            _Bearbeitungsvermerk = value
            NotifyPropertyChanged("Bearbeitungsvermerk")
            OnBearbeitungsvermerkChanged()
        End Set
    End Property

    Partial Private Sub OnBearbeitungsvermerkChanged()
    End Sub


    Private _Erstelltam As Nullable(Of Date)
    Public Property Erstelltam As Nullable(Of Date)
        Get
            Return _Erstelltam
        End Get
        Set(ByVal value As Nullable(Of Date))
            _Erstelltam = value
			NotifyPropertyChanged("Erstelltam")
			OnErstelltamChanged()
        End Set
    End Property

	Private Partial Sub OnErstelltamChanged()
	End Sub

    Private _Erstelltvon As Nullable(Of Integer)
    Public Property Erstelltvon As Nullable(Of Integer)
        Get
            Return _Erstelltvon
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _Erstelltvon = value
			NotifyPropertyChanged("Erstelltvon")
			OnErstelltvonChanged()
        End Set
    End Property

	Private Partial Sub OnErstelltvonChanged()
	End Sub

    Private _Bearbeitetam As Nullable(Of Date)
    Public Property Bearbeitetam As Nullable(Of Date)
        Get
            Return _Bearbeitetam
        End Get
        Set(ByVal value As Nullable(Of Date))
            _Bearbeitetam = value
			NotifyPropertyChanged("Bearbeitetam")
			OnBearbeitetamChanged()
        End Set
    End Property

	Private Partial Sub OnBearbeitetamChanged()
	End Sub

    Private _Bearbeitetvon As Nullable(Of Integer)
    Public Property Bearbeitetvon As Nullable(Of Integer)
        Get
            Return _Bearbeitetvon
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _Bearbeitetvon = value
			NotifyPropertyChanged("Bearbeitetvon")
			OnBearbeitetvonChanged()
        End Set
    End Property

	Private Partial Sub OnBearbeitetvonChanged()
	End Sub

    Private _Aktiv As Nullable(Of Boolean)
    Public Property Aktiv As Nullable(Of Boolean)
        Get
            Return _Aktiv
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            _Aktiv = value
			NotifyPropertyChanged("Aktiv")
			OnAktivChanged()
        End Set
    End Property

	Private Partial Sub OnAktivChanged()
	End Sub

    Private _Passwort As String
    Public Property Passwort As String
        Get
            Return _Passwort
        End Get
        Set(ByVal value As String)
            _Passwort = value
			NotifyPropertyChanged("Passwort")
			OnPasswortChanged()
        End Set
    End Property

	Private Partial Sub OnPasswortChanged()
	End Sub

    Private _Salt As String
    Public Property Salt As String
        Get
            Return _Salt
        End Get
        Set(ByVal value As String)
            _Salt = value
			NotifyPropertyChanged("Salt")
			OnSaltChanged()
        End Set
    End Property

	Private Partial Sub OnSaltChanged()
	End Sub

    Private _Matrikelnummer As String
    Public Property Matrikelnummer As String
        Get
            Return _Matrikelnummer
        End Get
        Set(ByVal value As String)
            _Matrikelnummer = value
			NotifyPropertyChanged("Matrikelnummer")
			OnMatrikelnummerChanged()
        End Set
    End Property

	Private Partial Sub OnMatrikelnummerChanged()
    End Sub

    Private _Checkinmessage As String
    <MaxLength(500)>
    Public Property Checkinmessage As String
        Get
            Return _Checkinmessage
        End Get
        Set(ByVal value As String)
            _Checkinmessage = value
        End Set
    End Property


    Private _DocumentUID As Nullable(Of Guid)
    Public Property DocumentUID As Nullable(Of Guid)
        Get
            Return _DocumentUID
        End Get
        Set(value As Nullable(Of Guid))
            _DocumentUID = value
            NotifyPropertyChanged("DocumentUID")
        End Set
    End Property

    Private _Document As Document
    Public Overridable Property Document As Document
        Get
            Return _Document
        End Get
        Set(value As Document)
            _Document = value
            NotifyPropertyChanged("Document")
        End Set
    End Property

    Private _Billings As ObservableCollection(Of Billing) = New ObservableCollection(Of Billing)
    Public Overridable Property Billings As ObservableCollection(Of Billing)
        Get
            Return _Billings
        End Get
        Set(ByVal value As ObservableCollection(Of Billing))
            _Billings = value
			NotifyPropertyChanged("Billings")
			OnBillingsChanged()
        End Set
    End Property

	Private Partial Sub OnBillingsChanged()
	End Sub

    Private _Bookings As ObservableCollection(Of Booking) = New ObservableCollection(Of Booking)
    Public Overridable Property Bookings As ObservableCollection(Of Booking)
        Get
            Return _Bookings
        End Get
        Set(ByVal value As ObservableCollection(Of Booking))
            _Bookings = value
			NotifyPropertyChanged("Bookings")
			OnBookingsChanged()
        End Set
    End Property

	Private Partial Sub OnBookingsChanged()
	End Sub

    Private _Checkins As ObservableCollection(Of Checkin) = New ObservableCollection(Of Checkin)
    Public Overridable Property Checkins As ObservableCollection(Of Checkin)
        Get
            Return _Checkins
        End Get
        Set(ByVal value As ObservableCollection(Of Checkin))
            _Checkins = value
			NotifyPropertyChanged("Checkins")
			OnCheckinsChanged()
        End Set
    End Property

	Private Partial Sub OnCheckinsChanged()
	End Sub

    Private _Contracts As ObservableCollection(Of Contract) = New ObservableCollection(Of Contract)
    Public Overridable Property Contracts As ObservableCollection(Of Contract)
        Get
            Return _Contracts
        End Get
        Set(ByVal value As ObservableCollection(Of Contract))
            _Contracts = value
			NotifyPropertyChanged("Contracts")
			OnContractsChanged()
        End Set
    End Property

	Private Partial Sub OnContractsChanged()
	End Sub

    Private _CourseStaffs As ObservableCollection(Of CourseStaff) = New ObservableCollection(Of CourseStaff)
    Public Overridable Property CourseStaffs As ObservableCollection(Of CourseStaff)
        Get
            Return _CourseStaffs
        End Get
        Set(ByVal value As ObservableCollection(Of CourseStaff))
            _CourseStaffs = value
			NotifyPropertyChanged("CourseStaffs")
			OnCourseStaffsChanged()
        End Set
    End Property

	Private Partial Sub OnCourseStaffsChanged()
	End Sub

    Private _CustomerState As CustomerState
    Public Overridable Property CustomerState As CustomerState
        Get
            Return _CustomerState
        End Get
        Set(ByVal value As CustomerState)
            _CustomerState = value
			NotifyPropertyChanged("CustomerState")
			OnCustomerStateChanged()
        End Set
    End Property

	Private Partial Sub OnCustomerStateChanged()
	End Sub



    Private _SportsObmann As ObservableCollection(Of Sport) = New ObservableCollection(Of Sport)
    <InverseProperty("Obmann_Customer")> _
    Public Overridable Property SportsObmann As ObservableCollection(Of Sport)
        Get
            Return _SportsObmann
        End Get
        Set(ByVal value As ObservableCollection(Of Sport))
            _SportsObmann = value
            NotifyPropertyChanged("SportsObmannCustomers")
        End Set
    End Property

    Private _SportsAnsprechpartner As ObservableCollection(Of Sport) = New ObservableCollection(Of Sport)
    <InverseProperty("Ansprechpartner_Customer")> _
    Public Overridable Property SportsAnsprechpartner As ObservableCollection(Of Sport)
        Get
            Return _SportsAnsprechpartner
        End Get
        Set(value As ObservableCollection(Of Sport))
            _SportsAnsprechpartner = value
        End Set
    End Property



    Private _StaffContracts As ObservableCollection(Of StaffContract) = New ObservableCollection(Of StaffContract)
    Public Overridable Property StaffContracts As ObservableCollection(Of StaffContract)
        Get
            Return _StaffContracts
        End Get
        Set(ByVal value As ObservableCollection(Of StaffContract))
            _StaffContracts = value
			NotifyPropertyChanged("StaffContracts")
			OnStaffContractsChanged()
        End Set
    End Property

	Private Partial Sub OnStaffContractsChanged()
    End Sub

    Private _CheckinLogs As ObservableCollection(Of CheckinLog) = New ObservableCollection(Of CheckinLog)
    Public Overridable Property CheckinLogs As ObservableCollection(Of CheckinLog)
        Get
            Return _CheckinLogs
        End Get
        Set(value As ObservableCollection(Of CheckinLog))
            _CheckinLogs = value
        End Set
    End Property

    Private _Activities As ObservableCollection(Of Activity) = New ObservableCollection(Of Activity)
    Public Overridable Property Activities As ObservableCollection(Of Activity)
        Get
            Return _Activities
        End Get
        Set(value As ObservableCollection(Of Activity))
            _Activities = value
        End Set
    End Property

    Private _ExternalMappingKey As String
    Public Property ExternalMappingKey As String
        Get
            Return _ExternalMappingKey
        End Get
        Set(ByVal value As String)
            _ExternalMappingKey = value
            NotifyPropertyChanged("ExternalMappingKey")
        End Set
    End Property

    Private _Blocked As Boolean = False
    Public Property Blocked As Boolean
        Get
            Return _Blocked
        End Get
        Set(value As Boolean)
            _Blocked = value
            NotifyPropertyChanged("Blocked")
        End Set
    End Property

    Private _Groups As ObservableCollection(Of CustomerGroup) = New ObservableCollection(Of CustomerGroup)
    Public Overridable Property Groups As ObservableCollection(Of CustomerGroup)
        Get
            Return _Groups
        End Get
        Set(value As ObservableCollection(Of CustomerGroup))
            _Groups = value
        End Set
    End Property

    Private _Mandatsdatum As Nullable(Of DateTime)
    Public Property Mandatsdatum As Nullable(Of DateTime)
        Get
            Return _Mandatsdatum
        End Get
        Set(value As Nullable(Of DateTime))
            _Mandatsdatum = value
            NotifyPropertyChanged("Mandatsdatum")
        End Set
    End Property

    Private _LetzterEinzug As Nullable(Of DateTime)
    Public Property LetzterEinzug As Nullable(Of DateTime)
        Get
            Return _LetzterEinzug
        End Get
        Set(value As Nullable(Of DateTime))
            _LetzterEinzug = value
            NotifyPropertyChanged("LetzterEinzug")
        End Set
    End Property


    ''' 0 = Erstlastschrift, 1 = Folgelastschrift
    Private _Mandatstyp As Byte = 0
    Public Property Mandatstyp As Byte
        Get
            Return _Mandatstyp
        End Get
        Set(value As Byte)
            _Mandatstyp = value
            NotifyPropertyChanged("Mandatstyp")
        End Set
    End Property

    Private _Mandatscounter As Integer = 0
    Public Property Mandatscounter As Integer
        Get
            Return _Mandatscounter
        End Get
        Set(value As Integer)
            _Mandatscounter = value
            NotifyPropertyChanged("Mandatscounter")
        End Set
    End Property


    
    Private _Country As Country
    <ForeignKey("CountryID")> _
    <DefaultValue(81)> _
    Public Overridable Property Country As Country
        Get
            Return _Country
        End Get
        Set(ByVal value As Country)
            _Country = value
            NotifyPropertyChanged("Country")
            OnCountryChanged()
        End Set
    End Property

    Partial Private Sub OnCountryChanged()
    End Sub

    Private _Flags As String
    Public Property Flags As String
        Get
            Return _Flags
        End Get
        Set(value As String)
            _Flags = value
            NotifyPropertyChanged("Flags")
        End Set
    End Property

    Private _RoleID As Nullable(Of Integer)
    Public Property RoleID() As Nullable(Of Integer)
        Get
            Return _RoleID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _RoleID = value
        End Set
    End Property

    Private _Role As Role
    <ForeignKey("RoleID")> _
    Public Overridable Property Role() As Role
        Get
            Return _Role
        End Get
        Set(ByVal value As Role)
            _Role = value
        End Set
    End Property


    Private _ImageIsPublic As Boolean = False
    Public Property ImageIsPublic() As Boolean
        Get
            Return _ImageIsPublic
        End Get
        Set(ByVal value As Boolean)
            _ImageIsPublic = value
            NotifyPropertyChanged("ImageIsPublic")
        End Set
    End Property

    Private _CustomerInstitutionID As Nullable(Of Integer)
    Public Property CustomerInstitutionID() As Nullable(Of Integer)
        Get
            Return _CustomerInstitutionID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _CustomerInstitutionID = value
            NotifyPropertyChanged("CustomerInstitutionID")
        End Set
    End Property

    Private _CustomerInstitution As CustomerInstitution
    <ForeignKey("CustomerInstitutionID")>
    Public Overridable Property CustomerInstitution() As CustomerInstitution
        Get
            Return _CustomerInstitution
        End Get
        Set(ByVal value As CustomerInstitution)
            _CustomerInstitution = value
        End Set
    End Property

    Private _Languages As String
    Public Property Languages() As String
        Get
            Return _Languages
        End Get
        Set(ByVal value As String)
            _Languages = value
            NotifyPropertyChanged("Languages")
        End Set
    End Property

    Private _PasswordToken As String
    Public Property PasswordToken() As String
        Get
            Return _PasswordToken
        End Get
        Set(ByVal value As String)
            _PasswordToken = value
        End Set
    End Property


    Private _CustomerMetas As ObservableCollection(Of CustomerMeta) = New ObservableCollection(Of CustomerMeta)
    Public Overridable Property CustomerMetas() As ObservableCollection(Of CustomerMeta)
        Get
            Return _CustomerMetas
        End Get
        Set(ByVal value As ObservableCollection(Of CustomerMeta))
            _CustomerMetas = value
        End Set
    End Property


    Private _Company As String
    <MaxLength(100)>
    Public Property Company() As String
        Get
            Return _Company
        End Get
        Set(ByVal value As String)
            _Company = value
            NotifyPropertyChanged("Company")
        End Set
    End Property




End Class
