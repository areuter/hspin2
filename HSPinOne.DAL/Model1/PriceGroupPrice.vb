﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations.Schema
Imports System.ComponentModel.DataAnnotations

Public Class PriceGroupPrice
    Inherits EntityBase


    Private _PriceGroupPriceID As Integer
    Public Property PriceGroupPriceID() As Integer
        Get
            Return _PriceGroupPriceID
        End Get
        Set(ByVal value As Integer)
            _PriceGroupPriceID = value
            NotifyPropertyChanged("PriceGroupPriceID")
        End Set
    End Property

    Private _PriceGroupID As Integer
    Public Property PriceGroupID() As Integer
        Get
            Return _PriceGroupID
        End Get
        Set(ByVal value As Integer)
            _PriceGroupID = value
            NotifyPropertyChanged("PriceGroupID")
        End Set
    End Property

    Private _PriceGroup As PriceGroup
    Public Property PriceGroup() As PriceGroup
        Get
            Return _PriceGroup
        End Get
        Set(ByVal value As PriceGroup)
            _PriceGroup = value
            NotifyPropertyChanged("PriceGroup")
        End Set
    End Property

    Private _CustomerStateID As Integer
    Public Property CustomerStateID() As Integer
        Get
            Return _CustomerStateID
        End Get
        Set(ByVal value As Integer)
            _CustomerStateID = value
            NotifyPropertyChanged("CustomerStateID")
        End Set
    End Property

    Private _CustomerState As CustomerState
    Public Property CustomerState() As CustomerState
        Get
            Return _CustomerState
        End Get
        Set(ByVal value As CustomerState)
            _CustomerState = value
            NotifyPropertyChanged("CustomerState")
        End Set
    End Property

    Private _Price As Decimal = 0
    Public Property Price() As Decimal
        Get
            Return _Price
        End Get
        Set(ByVal value As Decimal)
            _Price = value
            NotifyPropertyChanged("Price")
        End Set
    End Property

    Private _TaxPercent As Decimal = 0
    Public Property TaxPercent() As Decimal
        Get
            Return _TaxPercent
        End Get
        Set(ByVal value As Decimal)
            _TaxPercent = value
            NotifyPropertyChanged("TaxPercent")
        End Set
    End Property

    Private _IsActive As Boolean = True
    Public Property IsActive As Boolean
        Get
            Return _IsActive
        End Get
        Set(value As Boolean)
            _IsActive = value
            NotifyPropertyChanged("IsActive")
        End Set
    End Property

    Private _Vorausbuchungstage As Integer = 14
    Public Property Vorausbuchungstage As Integer
        Get
            Return _Vorausbuchungstage

        End Get
        Set(value As Integer)
            _Vorausbuchungstage = value
            NotifyPropertyChanged("Vorausbuchungstage")
        End Set
    End Property

    Private _CostcenterID As Nullable(Of Integer)
    Public Property CostcenterID() As Nullable(Of Integer)
        Get
            Return _CostcenterID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _CostcenterID = value
        End Set
    End Property

    Private _Costcenter As Costcenter
    <ForeignKey("CostcenterID")>
    Public Overridable Property Costcenter() As Costcenter
        Get
            Return _Costcenter
        End Get
        Set(ByVal value As Costcenter)
            _Costcenter = value
            NotifyPropertyChanged("Costcenter")
        End Set
    End Property

    Private _Innenauftrag As String
    Public Property Innenauftrag() As String
        Get
            Return _Innenauftrag
        End Get
        Set(ByVal value As String)
            _Innenauftrag = value
            NotifyPropertyChanged("Innenauftrag")
        End Set
    End Property

    Private _Sachkonto As String
    Public Property Sachkonto() As String
        Get
            Return _Sachkonto
        End Get
        Set(ByVal value As String)
            _Sachkonto = value
            NotifyPropertyChanged("Sachkonto")
        End Set
    End Property


    Private _KSTextern As String
    <MaxLength(30)>
    Public Property KSTextern() As String
        Get
            Return _KSTextern
        End Get
        Set(ByVal value As String)
            _KSTextern = value
            NotifyPropertyChanged("KSTextern")
        End Set
    End Property


    Private _Steuerkennzeichen As String
    <MaxLength(20)>
    Public Property Steuerkennzeichen() As String
        Get
            Return _Steuerkennzeichen
        End Get
        Set(ByVal value As String)
            _Steuerkennzeichen = value
            NotifyPropertyChanged("Steuerkennzeichen")
        End Set
    End Property

    Private _AccountingTemplateID As Nullable(Of Integer)
    Public Property AccountingTemplateID() As Nullable(Of Integer)
        Get
            Return _AccountingTemplateID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _AccountingTemplateID = value
            NotifyPropertyChanged("AccountingTemplateID")
        End Set
    End Property

    Private _AccountingTemplate As AccountingTemplate
    <ForeignKey("AccountingTemplateID")>
    Public Property AccountingTemplate() As AccountingTemplate
        Get
            Return _AccountingTemplate
        End Get
        Set(ByVal value As AccountingTemplate)
            _AccountingTemplate = value
            NotifyPropertyChanged("AccountingTemplate")
        End Set
    End Property

End Class
