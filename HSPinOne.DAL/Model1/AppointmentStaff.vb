﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel

Public Class AppointmentStaff
    Inherits EntityBase

    Private _AppointmentStaffID As Integer
    Public Property AppointmentStaffID As Integer
        Get
            Return _AppointmentStaffID
        End Get
        Set(value As Integer)
            _AppointmentStaffID = value
            NotifyPropertyChanged("AppointmentStaffID")
        End Set
    End Property

    Private _AppointmentID As Integer
    Public Property AppointmentID As Integer
        Get
            Return _AppointmentID
        End Get
        Set(value As Integer)
            _AppointmentID = value
            NotifyPropertyChanged("AppointmentID")
        End Set
    End Property

    Private _Appointment As Appointment
    Public Overridable Property Appointment As Appointment
        Get
            Return _Appointment

        End Get
        Set(value As Appointment)
            _Appointment = value
            NotifyPropertyChanged("Appointment")
        End Set
    End Property

    Private _CustomerID As Integer
    Public Property CustomerID As Integer
        Get
            Return _CustomerID
        End Get
        Set(value As Integer)
            _CustomerID = value
            NotifyPropertyChanged("CustomerID")
        End Set
    End Property

    Private _Customer As Customer
    Public Overridable Property Customer As Customer
        Get
            Return _Customer
        End Get
        Set(value As Customer)
            _Customer = value
            NotifyPropertyChanged("Customer")
        End Set
    End Property

    Private _StdLohn As Decimal
    Public Property StdLohn As Decimal
        Get
            Return _StdLohn
        End Get
        Set(value As Decimal)
            _StdLohn = value
            NotifyPropertyChanged("StdLohn")
        End Set
    End Property


End Class
