﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel

Public Class Equipment
    Inherits EntityBase

    Private _EquipmentID As Integer
    Public Property EquipmentID As Integer
        Get
            Return _EquipmentID
        End Get
        Set(ByVal value As Integer)
            _EquipmentID = value
            NotifyPropertyChanged("EquipmentID")
            OnEquipmentIDChanged()
        End Set
    End Property

    Partial Private Sub OnEquipmentIDChanged()
    End Sub

    Private _EquipmentCategory As String
    Public Property EquipmentCategory As String
        Get
            Return _EquipmentCategory
        End Get
        Set(ByVal value As String)
            _EquipmentCategory = value
            NotifyPropertyChanged("EquipmentCategory")
            OnEquipmentCategoryChanged()
        End Set
    End Property

    Partial Private Sub OnEquipmentCategoryChanged()
    End Sub

    Private _EquipmentName As String
    Public Property EquipmentName As String
        Get
            Return _EquipmentName
        End Get
        Set(ByVal value As String)
            _EquipmentName = value
            NotifyPropertyChanged("EquipmentName")
            OnEquipmentNameChanged()
        End Set
    End Property

    Partial Private Sub OnEquipmentNameChanged()
    End Sub

    Private _EquipmentOrder As Integer
    Public Property EquipmentOrder As Integer
        Get
            Return _EquipmentOrder
        End Get
        Set(ByVal value As Integer)
            _EquipmentOrder = value
            NotifyPropertyChanged("EquipmentOrder")
            OnEquipmentOrderChanged()
        End Set
    End Property

    Partial Private Sub OnEquipmentOrderChanged()
    End Sub

    Private _LocationEquipments As ObservableCollection(Of LocationEquipment) = New ObservableCollection(Of LocationEquipment)
    Public Overridable Property LocationEquipments As ObservableCollection(Of LocationEquipment)
        Get
            Return _LocationEquipments
        End Get
        Set(ByVal value As ObservableCollection(Of LocationEquipment))
            _LocationEquipments = value
            NotifyPropertyChanged("LocationEquipments")
            OnLocationEquipmentsChanged()
        End Set
    End Property

    Partial Private Sub OnLocationEquipmentsChanged()
    End Sub
End Class

