﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Public Class AppointmentCustomers
    Inherits EntityBase


    Private _AppointmentCustomersID As Integer
    Public Property AppointmentCustomersID() As Integer
        Get
            Return _AppointmentCustomersID
        End Get
        Set(ByVal value As Integer)
            _AppointmentCustomersID = value
        End Set
    End Property


    Private _AppointmentID As Integer
    Public Property AppointmentID() As Integer
        Get
            Return _AppointmentID
        End Get
        Set(ByVal value As Integer)
            _AppointmentID = value
        End Set
    End Property

    Private _Appointment As Appointment
    Public Overridable Property Appointment() As Appointment
        Get
            Return _Appointment
        End Get
        Set(ByVal value As Appointment)
            _Appointment = value
        End Set
    End Property

    Private _CustomerID As Integer
    Public Property CustomerID() As Integer
        Get
            Return _CustomerID
        End Get
        Set(ByVal value As Integer)
            _CustomerID = value
        End Set
    End Property

    Private _Customer As Customer
    Public Overridable Property Customer() As Customer
        Get
            Return _Customer
        End Get
        Set(ByVal value As Customer)
            _Customer = value
        End Set
    End Property

    Private _BookingDate As DateTime
    Public Property BookingDate() As DateTime
        Get
            Return _BookingDate
        End Get
        Set(ByVal value As DateTime)
            _BookingDate = value
        End Set
    End Property


End Class
