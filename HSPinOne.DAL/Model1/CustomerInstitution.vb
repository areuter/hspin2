﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema



Partial Public Class CustomerInstitution
    Inherits EntityBase

    Private _CustomerInstitutionID As Integer
    Public Property CustomerInstitutionID() As Integer
        Get
            Return _CustomerInstitutionID
        End Get
        Set(ByVal value As Integer)
            _CustomerInstitutionID = value
            NotifyPropertyChanged("CustomerInstitutionID")
        End Set
    End Property

    Private _CustomerInstitutionName As String
    Public Property CustomerInstitutionName() As String
        Get
            Return _CustomerInstitutionName
        End Get
        Set(ByVal value As String)
            _CustomerInstitutionName = value
            NotifyPropertyChanged("CustomerInstitutionName")
        End Set
    End Property

    Private _IsActive As Boolean = True
    Public Property IsActive() As Boolean
        Get
            Return _IsActive
        End Get
        Set(ByVal value As Boolean)
            _IsActive = value
        End Set
    End Property






End Class
