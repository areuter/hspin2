﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Partial Public Class Country
    Inherits EntityBase

    Private _CountryID As Integer
    Public Property CountryID As Integer
        Get
            Return _CountryID
        End Get
        Set(ByVal value As Integer)
            _CountryID = value
            NotifyPropertyChanged("CountryID")
            OnCountryIDChanged()
        End Set
    End Property

    Partial Private Sub OnCountryIDChanged()
    End Sub

    Private _CountryName As String
    Public Property CountryName As String
        Get
            Return _CountryName
        End Get
        Set(ByVal value As String)
            _CountryName = value
            NotifyPropertyChanged("CountryName")
            OnCountryNameChanged()
        End Set
    End Property

    Partial Private Sub OnCountryNameChanged()
    End Sub

    Private _Iso2 As String
    Public Property Iso2 As String
        Get
            Return _Iso2
        End Get
        Set(ByVal value As String)
            _Iso2 = value
            NotifyPropertyChanged("Iso2")
            OnIso2Changed()
        End Set
    End Property

    Partial Private Sub OnIso2Changed()
    End Sub

    Private _Iso3 As String
    Public Property Iso3 As String
        Get
            Return _Iso3
        End Get
        Set(ByVal value As String)
            _Iso3 = value
            NotifyPropertyChanged("Iso3")
            OnIso3Changed()
        End Set
    End Property

    Partial Private Sub OnIso3Changed()
    End Sub

    Private _AddressFormat As Integer
    Public Property AddressFormat As Integer
        Get
            Return _AddressFormat
        End Get
        Set(ByVal value As Integer)
            _AddressFormat = value
            NotifyPropertyChanged("AddressFormat")
            OnAddressFormatChanged()
        End Set
    End Property

    Partial Private Sub OnAddressFormatChanged()
    End Sub

    Private _Active As Byte
    Public Property Active As Byte
        Get
            Return _Active
        End Get
        Set(ByVal value As Byte)
            _Active = value
            NotifyPropertyChanged("Active")
            OnActiveChanged()
        End Set
    End Property

    Partial Private Sub OnActiveChanged()
    End Sub

End Class
