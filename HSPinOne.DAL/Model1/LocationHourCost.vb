﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations
Partial Public Class LocationHourCost
    Inherits EntityBase


    Private _LocationHourCostID As Integer
    Public Property LocationHourCostID As Integer
        Get
            Return _LocationHourCostID
        End Get
        Set(value As Integer)
            _LocationHourCostID = value
            NotifyPropertyChanged("LocationHourCostID")
        End Set
    End Property

    Private _CustomerState As CustomerState
    Public Overridable Property CustomerState As CustomerState
        Get
            Return _CustomerState
        End Get
        Set(value As CustomerState)
            _CustomerState = value
            NotifyPropertyChanged("CustomerState")
        End Set
    End Property

    Private _Price As Decimal = 0
    Public Property Price As Decimal
        Get
            Return _Price
        End Get
        Set(value As Decimal)
            _Price = value
            NotifyPropertyChanged("Price")
        End Set
    End Property

    Private _TaxPercent As Decimal
    Public Property TaxPercent() As Decimal
        Get
            Return _TaxPercent
        End Get
        Set(ByVal value As Decimal)
            _TaxPercent = value
            NotifyPropertyChanged("TaxPercent")
        End Set
    End Property



    Private _IsActive As Boolean = True
    Public Property IsActive As Boolean
        Get
            Return _IsActive
        End Get
        Set(value As Boolean)
            _IsActive = value
            NotifyPropertyChanged("IsActive")
        End Set
    End Property

    Private _Vorausbuchungstage As Integer = 14
    Public Property Vorausbuchungstage As Integer
        Get
            Return _Vorausbuchungstage

        End Get
        Set(value As Integer)
            _Vorausbuchungstage = value
            NotifyPropertyChanged("Vorausbuchungstage")
        End Set
    End Property

End Class
