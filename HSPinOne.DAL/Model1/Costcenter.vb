﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel

Partial Public Class Costcenter
    Inherits EntityBase

    Private _CostcenterID As Integer
    Public Property CostcenterID As Integer
        Get
            Return _CostcenterID
        End Get
        Set(value As Integer)
            _CostcenterID = value
        End Set
    End Property

    Private _Costcentername As String
    Public Property Costcentername As String
        Get
            Return _Costcentername
        End Get
        Set(value As String)
            _Costcentername = value
        End Set
    End Property

End Class
