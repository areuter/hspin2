﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Public Class AccountingTemplate
    Inherits EntityBase


    Private _AccountingTemplateID As Integer
    <Key>
    Public Property AccountingTemplateID() As Integer
        Get
            Return _AccountingTemplateID
        End Get
        Set(ByVal value As Integer)
            _AccountingTemplateID = value
            NotifyPropertyChanged("AccountingTemplateID")
        End Set
    End Property

    Private _Shortcode As String
    Public Property Shortcode() As String
        Get
            Return _Shortcode
        End Get
        Set(ByVal value As String)
            _Shortcode = value
            NotifyPropertyChanged("Shortcode")
        End Set
    End Property

    Private _Bezeichnung As String
    Public Property Bezeichnung() As String
        Get
            Return _Bezeichnung
        End Get
        Set(ByVal value As String)
            _Bezeichnung = value
            NotifyPropertyChanged("Bezeichnung")
        End Set
    End Property

    Private _Bereich As String
    Public Property Bereich() As String
        Get
            Return _Bereich
        End Get
        Set(ByVal value As String)
            _Bereich = value
            NotifyPropertyChanged("Bereich")
        End Set
    End Property

    Private _Kostenstelle As String
    Public Property Kostenstelle() As String
        Get
            Return _Kostenstelle
        End Get
        Set(ByVal value As String)
            _Kostenstelle = value
            NotifyPropertyChanged("Kostenstelle")
        End Set
    End Property

    Private _Innenauftrag As String
    Public Property Innenauftrag() As String
        Get
            Return _Innenauftrag
        End Get
        Set(ByVal value As String)
            _Innenauftrag = value
            NotifyPropertyChanged("Innenauftrag")
        End Set
    End Property

    Private _Sachkonto As String
    Public Property Sachkonto() As String
        Get
            Return _Sachkonto
        End Get
        Set(ByVal value As String)
            _Sachkonto = value
            NotifyPropertyChanged("Sachkonto")
        End Set
    End Property

    Private _SteuerkennzeichenAR As String
    Public Property SteuerkennzeichenAR() As String
        Get
            Return _SteuerkennzeichenAR
        End Get
        Set(ByVal value As String)
            _SteuerkennzeichenAR = value
            NotifyPropertyChanged("SteuerkennzeichenAR")
        End Set
    End Property

    Private _TaxID As Integer
    Public Property TaxID() As Integer
        Get
            Return _TaxID
        End Get
        Set(ByVal value As Integer)
            _TaxID = value
            NotifyPropertyChanged("TaxID")
        End Set
    End Property

    Private _Tax As Tax
    <ForeignKey("TaxID")>
    Public Overridable Property Tax() As Tax
        Get
            Return _Tax
        End Get
        Set(ByVal value As Tax)
            _Tax = value
            NotifyPropertyChanged("Tax")
        End Set
    End Property
End Class

