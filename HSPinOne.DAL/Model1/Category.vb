﻿
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel



Partial Public Class Category
    Inherits EntityBase

    Private _CategoryID As Integer
    Public Property CategoryID As Integer
        Get
            Return _CategoryID
        End Get
        Set(ByVal value As Integer)
            _CategoryID = value
            NotifyPropertyChanged("CategoryID")
            OnCategoryIDChanged()
        End Set
    End Property

    Partial Private Sub OnCategoryIDChanged()
    End Sub

    Private _CategoryName As String
    Public Property CategoryName As String
        Get
            Return _CategoryName
        End Get
        Set(ByVal value As String)
            _CategoryName = value
            NotifyPropertyChanged("CategoryName")
            OnCategoryNameChanged()
        End Set
    End Property

    Partial Private Sub OnCategoryNameChanged()
    End Sub

    Private _CategoryColor As String
    Public Property CategoryColor As String
        Get
            Return _CategoryColor
        End Get
        Set(ByVal value As String)
            _CategoryColor = value
            NotifyPropertyChanged("CategoryColor")
            OnCategoryColorChanged()
        End Set
    End Property

    Partial Private Sub OnCategoryColorChanged()
    End Sub

End Class
