﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations

Public Class AuditLog
    Inherits EntityBase


    Private _AuditLogID As Guid
    Public Property AuditLogID As Guid
        Get
            Return _AuditLogID
        End Get
        Set(value As Guid)
            _AuditLogID = value
        End Set
    End Property

    Private _UserID As String
    <Required()> _
    <MaxLength(50)> _
    Public Property UserID As String
        Get
            Return _UserID
        End Get
        Set(value As String)
            _UserID = value
        End Set
    End Property

    Private _EventDateUTC As DateTime
    <Required()> _
    Public Property EventDateUTC As DateTime
        Get
            Return _EventDateUTC
        End Get
        Set(value As DateTime)
            _EventDateUTC = value
        End Set
    End Property

    Private _EventType As String
    <MaxLength(1)> _
    <Required()> _
    Public Property EventType As String
        Get
            Return _EventType
        End Get
        Set(value As String)
            _EventType = value
        End Set
    End Property

    Private _TableName As String
    <MaxLength(100)> _
    <Required()> _
    Public Property TableName As String
        Get
            Return _TableName
        End Get
        Set(value As String)
            _TableName = value
        End Set
    End Property

    Private _RecordID As String
    <Required()> _
    <MaxLength(100)> _
    Public Property RecordID As String
        Get
            Return _RecordID
        End Get
        Set(value As String)
            _RecordID = value
        End Set
    End Property

    Private _ColumnName As String
    <Required()> _
    <MaxLength(100)> _
    Public Property ColumnName As String
        Get
            Return _ColumnName
        End Get
        Set(value As String)
            _ColumnName = value
        End Set
    End Property

    Private _OriginalValue As String
    Public Property OriginalValue As String
        Get
            Return _OriginalValue
        End Get
        Set(value As String)
            _OriginalValue = value
        End Set
    End Property

    Private _NewValue As String
    Public Property NewValue As String
        Get
            Return _NewValue
        End Get
        Set(value As String)
            _NewValue = value
        End Set
    End Property



End Class
