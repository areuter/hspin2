﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Public Class SportCategory
    Inherits EntityBase


    Private _SportCategoryID As Integer
    Public Property SportCategoryID As Integer
        Get
            Return _SportCategoryID
        End Get
        Set(value As Integer)
            _SportCategoryID = value
            NotifyPropertyChanged("SportCategoryID")
        End Set
    End Property

    Private _SportCategoryName As String
    <Required()>
    <MaxLength(250)>
    Public Property SportCategoryName As String
        Get
            Return _SportCategoryName
        End Get
        Set(value As String)
            _SportCategoryName = Trim(value)
            NotifyPropertyChanged("SportCategoryName")
        End Set
    End Property

    Private _IsActive As Boolean = True
    Public Property IsActive() As Boolean
        Get
            Return _IsActive
        End Get
        Set(ByVal value As Boolean)
            _IsActive = value
            NotifyPropertyChanged("IsActive")
        End Set
    End Property

End Class
