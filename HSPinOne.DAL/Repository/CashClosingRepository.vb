﻿Imports System.Linq


Namespace Repositories


    Public Class CashClosingRepository


        Public Shared Function GetCashClosingID(ByVal kasse As Integer) As Integer

            Using con As New in1Entities
                Dim cash = From c In con.CashClosings Where c.KasseNr = kasse And Not c.Ende.HasValue Select c

                If cash.Any Then
                    Return cash.FirstOrDefault.CashClosingID
                Else
                    ' Kasse öffnen
                    Dim newcc As New CashClosing
                    newcc.KasseNr = kasse
                    newcc.Start = Now
                    con.CashClosings.Add(newcc)
                    con.SaveChanges()
                    Return newcc.CashClosingID
                End If
            End Using
            Return 0
        End Function




    End Class
End Namespace