﻿Imports HSPinOne.DAL
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.Linq


Namespace Repositories


    Public Class BillingRepository

        Private _Errormessage As String
        Public ReadOnly Property Errormessage() As String
            Get
                Return _Errormessage
            End Get
        End Property


        Private _context As in1Entities

        Public Sub New(ByRef con As in1Entities)
            _context = con
        End Sub

        Public Function AddBilling(ByVal bill As Billing) As Billing
            Throw New NotImplementedException
        End Function

        Public Function GetActiveBillings(ByVal customerid As Integer, Optional ByVal take As Integer = 100) As ObservableCollection(Of Billing)

            Dim result = New ObservableCollection(Of Billing)
            Dim query = _context.Billings.Where(Function(o) o.CustomerID = customerid AndAlso
                                                    o.IsStorno = False AndAlso
                                                    (o.CourseID.HasValue Or o.Packagecredit > 0 Or o.BookingID.HasValue)) _
                                                    .OrderByDescending(Function(o) o.Rechnungsdatum) _
                                                    .Take(take)

            query.Load

            result = _context.Billings.Local

            Return result


            '_context.Billings.Where(Function(o) o.CustomerID = Customer.CustomerID).OrderByDescending(Function(o) o.Rechnungsdatum).Take(200).Load()

        End Function


        Public Function GetParticipants(ByVal courseid) As ObservableCollection(Of Billing)
            Dim result = New ObservableCollection(Of Billing)
            Dim query = _context.Billings.Where(Function(o) o.CourseID = courseid AndAlso
                                                    o.IsStorno = False) _
                                                    .Include(Function(o) o.Customer) _
                                                    .OrderByDescending(Function(o) o.Rechnungsdatum)
            result = _context.Billings.Local
            Return result
        End Function

        Public Function GetAllBillings(ByVal customerid As Integer, Optional ByVal take As Integer = 100) As ObservableCollection(Of Billing)

            Dim result = New ObservableCollection(Of Billing)
            Dim query = _context.Billings.Where(Function(o) o.CustomerID = customerid AndAlso
                                                    o.IsStorno = False).OrderByDescending(Function(o) o.Rechnungsdatum).Take(take)
            If query.Count > 0 Then
                result = New ObservableCollection(Of Billing)(query.ToList)
            End If
            Return result


            '_context.Billings.Where(Function(o) o.CustomerID = Customer.CustomerID).OrderByDescending(Function(o) o.Rechnungsdatum).Take(200).Load()

        End Function

        Public Sub RemoveAssociations(ByRef bill As Billing)
            ' Verbindungen der Originalrechnung kappen
            'bill.CourseID = Nothing
            'bill.BookingID = Nothing
            'bill.PackageID = Nothing
            'bill.Packagecredit = 0
        End Sub

        Public Sub Invert(ByRef bill As Billing)
            bill.Verwendungszweck = "Storno: " & bill.Verwendungszweck
            bill.Brutto = bill.Brutto * -1
        End Sub

        Public Sub StornoOpenBilling(ByRef bill As Billing)
            RemoveAssociations(bill)
            bill.IsStorno = True
            bill.BillingStatus = BillingStates.Storniert
            bill.Buchungsdatum = Now

        End Sub


        ''' <summary>
        ''' Kopiert eine Original Entity aus der Datenbank, kappt Verbindungen im Original 
        ''' und in der Kopie, Storno Verwendungzweck und Betrag invertiert
        ''' </summary>
        ''' <param name="bill"></param>
        ''' <returns></returns>
        Public Function StornoAndCopy(ByRef bill As Billing) As Billing

            Dim billid = bill.BillingID
            Dim copy As Billing

            Using con As New in1Entities
                copy = con.Billings.Where(Function(o) o.BillingID = billid).AsNoTracking().FirstOrDefault()

                ' Verbindungen kappen
                copy.Brutto = copy.Brutto * -1
                copy.CourseID = Nothing
                copy.BookingID = Nothing
                copy.PackageID = Nothing
                copy.Packagecredit = 0
                copy.Verwendungszweck = "Storno: " & copy.Verwendungszweck

                ' Verbindungen der Originalrechnung kappen
                bill.CourseID = Nothing
                bill.BookingID = Nothing
                bill.PackageID = Nothing
                bill.Packagecredit = 0


            End Using
            Return copy
        End Function


        Public Function StornoBilling(ByRef bill As Billing, ByVal accountid As Integer, ByVal paymethodid As Integer, ByVal KasseNr As Integer, Optional ByVal directPay As Boolean = False, Optional ByVal checkinid As Integer = 0) As Boolean

            If Not IsNothing(bill) AndAlso bill.IsStorno = False Then
                ' Storno buchung erzeugen
                Dim storno As New Billing
                storno.AccountID = accountid
                storno.BillingStatus = BillingStates.Offen
                storno.Brutto = bill.Brutto * -1
                storno.Steuerprozent = bill.Steuerprozent
                storno.CostcenterID = bill.CostcenterID
                storno.CustomerID = bill.CustomerID
                storno.CustomerStateID = bill.CustomerStateID
                storno.PaymentMethodID = paymethodid
                storno.ProductTypeID = bill.ProductTypeID
                storno.Faelligkeit = Now
                storno.IsStorno = True
                If (checkinid > 0) Then
                    storno.CheckinID = checkinid
                End If
                storno.Rechnungsdatum = Now
                storno.StornoID = bill.BillingID
                storno.Verwendungszweck = "Storno: " & bill.Verwendungszweck
                storno.Innenauftrag = bill.Innenauftrag
                storno.Sachkonto = bill.Sachkonto
                storno.KSTextern = bill.KSTextern
                storno.KasseNr = KasseNr


                _context.Billings.Add(storno)

                bill.IsStorno = True
                bill.CourseID = Nothing
                bill.BookingID = Nothing
                bill.Packagecredit = 0

                If directPay = True Then
                    storno.BillingStatus = BillingStates.Bezahlt
                    storno.Buchungsdatum = Now
                    bill.BillingStatus = BillingStates.Bezahlt
                    bill.Buchungsdatum = Now
                Else
                    storno.Kasse = True
                    storno.Checked = True
                End If

                Return True
            End If
            Return False
        End Function


        Public Function GetClone(ByVal id As Integer) As Billing
            Try
                Using con As New in1Entities
                    Return con.Billings.Where(Function(o) o.BillingID = id).AsNoTracking().FirstOrDefault()
                End Using
            Catch ex As Exception
                Throw New Exception("Rechnung mit dieser ID nicht gefunden")
            End Try



        End Function

        Public Function Ruecklastschrift(ByRef bill As Billing, ByVal Rsdate As Date, ByVal Svwz As String) As Boolean

            ' Kopie der Originalbuchung erstellen
            Dim billid = bill.BillingID
            Dim minus = _context.Billings.Where(Function(o) o.BillingID = billid).AsNoTracking().FirstOrDefault()
            Dim copy = _context.Billings.Where(Function(o) o.BillingID = billid).AsNoTracking().FirstOrDefault()

            'Minus Rechnung erstelle auf Bezahlt und IsStorno
            minus.BillingStatus = BillingStates.Bezahlt
            minus.Brutto = bill.Brutto * -1

            minus.Buchungsdatum = Rsdate

            minus.Verwendungszweck = "RS:" & bill.Verwendungszweck
            minus.IsStorno = True

            minus.DtausID = Nothing
            minus.EndToEnd = Nothing

            _context.Billings.Add(minus)

            ' Kopie der Rechnung als offene Rechnung erstellen
            copy.BillingStatus = BillingStates.Offen
            copy.Mahnstufe = bill.Mahnstufe + 1
            copy.IsStorno = False
            copy.Buchungsdatum = Nothing
            copy.PaymentInformation = Nothing
            copy.DtausID = Nothing
            copy.EndToEnd = Nothing
            _context.Billings.Add(copy)


            'Originalrechnung auf RS stellen
            bill.Bemerkung = "RS-Grund: " & Svwz & " / " & bill.Bemerkung
            bill.Rueckbuchungsdatum = Rsdate
            bill.IsStorno = True

            Return True

        End Function

        Public Function Verbuchen(ByRef bill As Billing) As Boolean
            If bill.AccountID < 1800 Then
                Me._Errormessage = "Eine Buchung auf ein Barkassenkonto ist nur am Infopoint möglich"
                Return False
            End If
            If Not bill.PaymentMethodID = 3 Then
                Me._Errormessage = "Verbuchen ist hier nur gegen Überweisung möglich"
                Return False
            End If
            bill.BillingStatus = BillingStates.Bezahlt
            bill.Buchungsdatum = Now
            Return True
        End Function

        ''' <summary>
        ''' Lade noch nicht abgerechnete Kassenposten in den Context
        ''' </summary>
        ''' <param name="kassenr"></param>
        Public Sub LoadCashReport(ByVal kassenr As Integer)

            _context.Billings.Where(Function(o) _
                    Not o.CashClosingID.HasValue And
                    o.BillingStatus = BillingStates.Bezahlt And
                    (o.PaymentMethodID = 1 Or o.PaymentMethodID = 4) And
                    o.KasseNr = kassenr) _
                    .Include(Function(o) o.Customer) _
                    .Include(Function(o) o.ProductType) _
                    .Include(Function(o) o.CustomerState) _
                    .Include(Function(o) o.PaymentMethod).Load()
        End Sub

        Public Sub LoadJournal(ByVal kassenr As Integer, ByVal dtfrom As DateTime, ByVal dttill As DateTime)
            _context.Billings.Where(Function(o) _
                o.Buchungsdatum >= dtfrom And
                o.Buchungsdatum <= dttill And
                (o.PaymentMethodID = 1 Or o.PaymentMethodID = 4) And
                o.KasseNr = kassenr) _
                .Include(Function(o) o.Customer) _
                .Include(Function(o) o.ProductType) _
                .Include(Function(o) o.CustomerState) _
                .Include(Function(o) o.PaymentMethod).Load()
        End Sub

        Public Sub LoadJournal(ByVal cashclosingid As Integer)
            _context.Billings.Where(Function(o) _
                o.CashClosingID = cashclosingid _
                And (o.PaymentMethodID = 1 Or o.PaymentMethodID = 4)) _
            .Include(Function(o) o.Customer) _
                .Include(Function(o) o.ProductType) _
                .Include(Function(o) o.CustomerState) _
                .Include(Function(o) o.PaymentMethod).Load()
        End Sub

        Public Sub LoadJournal(ByVal dtfrom As DateTime, ByVal dttill As DateTime)
            _context.Billings.Where(Function(o) _
                o.Buchungsdatum >= dtfrom And
                o.Buchungsdatum <= dttill) _
                .Include(Function(o) o.Customer) _
                .Include(Function(o) o.ProductType) _
                .Include(Function(o) o.CustomerState) _
                .Include(Function(o) o.PaymentMethod).Load()
        End Sub
    End Class
End Namespace