﻿Imports System.ComponentModel.Composition
Imports System.Windows.Controls
Imports HSPinOne.Common
Imports HSPinOne.Infrastructure
Imports Prism.Regions
Imports System.Collections.ObjectModel
Imports CommonServiceLocator

Public Class InfopointOutlookSectionViewModel
    Private _regionmanager As IRegionManager

    Private _treeViewMenu As ObservableCollection(Of OutlookSectionMenuTreeTopItemViewModel)

    Public Property TreeViewMenu As ObservableCollection(Of OutlookSectionMenuTreeTopItemViewModel)
        Get
            Return _treeViewMenu
        End Get
        Set(ByVal value As ObservableCollection(Of OutlookSectionMenuTreeTopItemViewModel))
            _treeViewMenu = value
        End Set
    End Property

    Private _selected As ListBoxItem

    Public Property Selected As ListBoxItem
        Get
            Return _selected
        End Get
        Set(value As ListBoxItem)
            _selected = value
            Navigate()
        End Set
    End Property


    Private Sub Navigate()
        If Not IsNothing(Selected) Then


            If Not IsNothing(Selected.Tag) Then
                ViewNavigation.NavigateViews(CType(Selected.Tag, String), CType(_regionmanager, RegionManager))
            End If
        Else

            ViewNavigation.NavigateViews("Infopoint1View", CType(_regionmanager, RegionManager))
        End If
    End Sub


    Public Sub New()

        Me._regionmanager = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IRegionManager)


        Events.EventInstance.EventAggregator.GetEvent(Of OutlookSectionChangedEvent)().Subscribe(AddressOf DefaultNav)
    End Sub

    Private Sub DefaultNav(ByVal param As Object)
        If param.Name.ToString.ToLower = "infopoint" Then Navigate()
    End Sub
End Class
