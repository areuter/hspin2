﻿Imports System.ComponentModel.Composition
Imports HSPinOne.Infrastructure


Public Class InfopointOutlookSection
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.DataContext = New InfopointOutlookSectionViewModel()
    End Sub

End Class
