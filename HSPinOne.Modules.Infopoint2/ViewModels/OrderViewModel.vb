﻿Imports System.Collections.ObjectModel
Imports System.ComponentModel
Imports System.Data.Entity
Imports System.Threading
Imports System.Threading.Tasks
Imports System.Windows.Data
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports Prism.Regions

Namespace ViewModel


    Public Class OrderViewModel
        Inherits ViewModelBase

        Private _context As in1Entities

        Private _Person As String = "Person"
        Public Property Person() As String
            Get
                Return _Person
            End Get
            Set(ByVal value As String)
                _Person = value
            End Set
        End Property

        Private _Checkin As Checkin
        Public Property Checkin() As Checkin
            Get
                Return _Checkin
            End Get
            Set(ByVal value As Checkin)
                _Checkin = value
                RaisePropertyChanged("Checkin")
            End Set
        End Property

        Private _SelectedState As Integer
        Public Property SelectedState() As Integer
            Get
                Return _SelectedState
            End Get
            Set(ByVal value As Integer)
                _SelectedState = value
                RaisePropertyChanged("SelectedState")
                LoadProducts()
            End Set
        End Property



        Private _Summe As Decimal = 12.5
        Public Property Summe() As Decimal
            Get
                Return _Summe
            End Get
            Set(ByVal value As Decimal)
                _Summe = value
                RaisePropertyChanged("Summe")
            End Set
        End Property

        Private _LVBillings As ListCollectionView
        Public Property LVBillings() As ListCollectionView
            Get
                Return _LVBillings
            End Get
            Set(ByVal value As ListCollectionView)
                _LVBillings = value
                RaisePropertyChanged("LVBillings")
            End Set
        End Property

        Private _CashRegister As CashRegister
        Public Property CashRegister() As CashRegister
            Get
                Return _CashRegister
            End Get
            Set(ByVal value As CashRegister)
                _CashRegister = value
                RaisePropertyChanged("CashRegister")
            End Set
        End Property

        Private _LVProducts As ListCollectionView
        Public Property LVProducts() As ListCollectionView
            Get
                Return _LVProducts
            End Get
            Set(ByVal value As ListCollectionView)
                _LVProducts = value
                RaisePropertyChanged("LVProducts")
            End Set
        End Property

        Private _Searchterm As String
        Public Property Searchterm() As String
            Get
                Return _Searchterm
            End Get
            Set(ByVal value As String)
                _Searchterm = value
                RaisePropertyChanged("Searchterm")
            End Set
        End Property

        Private _SelectedCategory As Category
        Public Property SelectedCategory() As Category
            Get
                Return _SelectedCategory
            End Get
            Set(ByVal value As Category)
                _SelectedCategory = value
                RaisePropertyChanged("SelectedCategory")
                UpdateProductsfilter()
            End Set
        End Property

        Private _Categories As ObservableCollection(Of Category)
        Public Property Categories() As ObservableCollection(Of Category)
            Get
                Return _Categories
            End Get
            Set(ByVal value As ObservableCollection(Of Category))
                _Categories = value
            End Set
        End Property

        Private _OCBillings As ObservableCollection(Of Billing)
        Public Property OCBillings() As ObservableCollection(Of Billing)
            Get
                Return _OCBillings
            End Get
            Set(ByVal value As ObservableCollection(Of Billing))
                _OCBillings = value
            End Set
        End Property

        Private _SelectedBill As Billing
        Public Property SelectedBill() As Billing
            Get
                Return _SelectedBill
            End Get
            Set(ByVal value As Billing)
                _SelectedBill = value
                RaisePropertyChanged("SelectedBill")
            End Set
        End Property



        Public Property SearchCommand As New RelayCommand(AddressOf UpdateProductsfilter)
        Public Property AddCommand As New RelayCommand(AddressOf AddAction)

        Public Property OrderCommand As New RelayCommand(AddressOf OrderAction)

        Public Property BackCommand As New RelayCommand(AddressOf NavigateBack)
        Public Property CashCommand As New RelayCommand(AddressOf NavigateCashView)

        Public Property StornoCommand As New RelayCommand(AddressOf StornoAction)
        Public Property RabattCommand As New RelayCommand(AddressOf RabattAction, AddressOf CanRabattAction)


        Private _productlist As ObservableCollection(Of Productlistitem)

        Private _kassenr As Integer = 0

        Private _bal As Order_Bal

        Private _regionmanager As IRegionManager

        Private _listLock As Object = New Object()
        Public Sub New()

            Me._regionmanager = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IRegionManager)



            Using con As New in1Entities
                con.Categories.Load()
                Categories = con.Categories.Local
                Categories.Insert(0, New Category With {.CategoryID = 9999, .CategoryName = "Alle"})
                SelectedCategory = Categories.First
            End Using
        End Sub

        Private Sub Load()

            ' Neuer Kontext um alte Änderungen zu verwerfen
            _context = New in1Entities

            _kassenr = MachineSettingService.GetKasseNr()

            _bal = New Order_Bal(MyGlobals.CurrentUsername, _context)

            CashRegister = _context.CashRegisters.Where(Function(o) o.KasseNr = _kassenr).Single()

            LoadProducts()

            LoadBillings()

        End Sub

        Private Sub LoadBillings()

            OCBillings = _bal.GetBillings(Checkin)

            LVBillings = CType(CollectionViewSource.GetDefaultView(OCBillings), ListCollectionView)
            LVBillings.Filter = New Predicate(Of Object)(AddressOf BillingFilter)

            Summe_berechnen()
        End Sub

        Private Function BillingFilter(ByVal itm As Object)
            Dim bill = TryCast(itm, Billing)
            If IsNothing(bill) Then Return False

            'If bill.Verwendungszweck.StartsWith("<Storno") Then Return False
            Return True

        End Function

        Private Sub Summe_berechnen()
            Summe = OCBillings.Sum(Function(o) o.Brutto)
        End Sub
        Private Sub LoadProducts()

            If IsNothing(_context) Then
                Return
            End If

            _productlist = New ObservableCollection(Of Productlistitem)

            'Dim courses = From c In _context.CoursePrices Where c.CustomerStateID = Customer.CustomerStateID
            Dim _now = DateTime.Now


            Dim courses = From c In _context.Courses Join p In _context.CoursePrices On c.CourseID Equals p.CourseID
                          Where p.CustomerStateID = SelectedState And c.Anmeldungvon < _now AndAlso c.Anmeldungbis > _now _
                              And c.CourseRegistration.Shop > 0 And c.Aktiv = True
                          Select New With {.CourseID = c.CourseID, .Sportname = c.Sport.Sportname, .Zusatzinfo = c.Zusatzinfo, c.Faelligkeit,
                                          .Zeittext = c.Zeittext, .DtStart = c.DTStart, .maxTN = c.maxTN, .Preis = p.Preis, .CostcenterID = c.CostcenterID,
                                            .ProductTypeID = c.ProductTypeID, .CategoryID = c.CategoryID,
                                            .Anmeldungvon = p.Anmeldungvon,
                                            .Anmeldungbis = p.Anmeldungbis,
                                            .Anmeldungerlaubt = p.Anmeldungerlaubt,
                                            p.TaxPercent,
                                            p.Innenauftrag,
                                            p.Sachkonto,
                                            p.KSTextern,
                                            p.Steuerkennzeichen,
                                            .pCostcenterID = p.CostcenterID,
                                           .Belegt = (From a In _context.Billings Where a.CourseID = c.CourseID And (a.IsStorno = False) Select a.CourseID).Count}

            If courses.Any Then
                For Each itm In courses
                    Dim prod As New Productlistitem
                    prod.Gruppe = "K"
                    prod.Artikelbezeichnung = itm.Sportname & " " & itm.Zusatzinfo
                    prod.Zusatzinfo = itm.Zeittext & " (Start: " & itm.DtStart.ToShortDateString & ")"
                    prod.Preis = itm.Preis
                    prod.TaxPercent = itm.TaxPercent
                    prod.CategoryID = itm.CategoryID
                    prod.CourseID = itm.CourseID
                    If Not IsNothing(itm.pCostcenterID) Then
                        prod.CostcenterID = itm.pCostcenterID
                    Else
                        prod.CostcenterID = itm.CostcenterID
                    End If

                    prod.ProductTypeID = itm.ProductTypeID
                    prod.Innenauftrag = itm.Innenauftrag
                    prod.Sachkonto = itm.Sachkonto
                    prod.KSTextern = itm.KSTextern
                    prod.Steuerkennzeichen = itm.Steuerkennzeichen

                    'Anmeldungerlaubt
                    prod.Showanmeldezeitraum = True
                    prod.Anmeldungvon = itm.Anmeldungvon
                    prod.Anmeldungbis = itm.Anmeldungbis
                    prod.Anmeldungerlaubt = False
                    If itm.Anmeldungvon < DateTime.Now And itm.Anmeldungbis > DateTime.Now Then
                        prod.Anmeldungerlaubt = True
                    End If
                    If itm.Anmeldungerlaubt = False Then prod.Anmeldungerlaubt = False

                    If itm.maxTN > 0 Then
                        prod.Frei = itm.maxTN - itm.Belegt
                    End If

                    If Not IsNothing(itm.Faelligkeit) Then
                        prod.Faelligkeit = itm.Faelligkeit
                    Else
                        prod.Faelligkeit = Now
                    End If
                    _productlist.Add(prod)
                Next
            End If


            Dim packages = From c In _context.Packages Join p In _context.PackagePrices On c.PackageID Equals p.PackageID
                           Where p.CustomerStateID = SelectedState And c.IsActive = True
                           Select New With {c.PackageID, c.PackageName, p.Preis, p.TaxPercent, c.Zutritte,
                               c.CostcenterID, c.ProductTypeID, c.CategoryID, p.Innenauftrag, p.Sachkonto,
                               .pCostcenterID = p.CostcenterID, p.KSTextern, p.Steuerkennzeichen
                               }


            For Each itm In packages
                Dim prod As New Productlistitem
                prod.Gruppe = "P"
                prod.Artikelbezeichnung = itm.PackageName
                prod.Preis = itm.Preis
                prod.TaxPercent = itm.TaxPercent
                prod.Innenauftrag = itm.Innenauftrag
                prod.Sachkonto = itm.Sachkonto
                prod.KSTextern = itm.KSTextern
                prod.Steuerkennzeichen = itm.Steuerkennzeichen

                If Not IsNothing(itm.pCostcenterID) Then
                    prod.CostcenterID = itm.pCostcenterID
                Else
                    prod.CostcenterID = itm.CostcenterID
                End If

                prod.PackageID = itm.PackageID
                prod.Packagecredit = itm.Zutritte
                prod.ProductTypeID = itm.ProductTypeID
                prod.CategoryID = itm.CategoryID
                prod.Faelligkeit = Now
                _productlist.Add(prod)
            Next

            LVProducts = CollectionViewSource.GetDefaultView(_productlist)
            LVProducts.SortDescriptions.Add(New SortDescription("Artikelbezeichnung", ListSortDirection.Ascending))
            LVProducts.Filter = New Predicate(Of Object)(AddressOf Productsfilter)
            RaisePropertyChanged("LVProducts")
            Searchterm = ""
        End Sub

        Private Function Productsfilter(ByVal item As Object) As Boolean
            Dim itm = TryCast(item, Productlistitem)
            If itm IsNot Nothing Then

                ' Bei Tagesgästen Kurse ausblenden
                If Not IsNothing(Checkin) AndAlso (IsNothing(Checkin.CustomerID) OrElse Checkin.CustomerID = 0) Then
                    If Not IsNothing(itm.CourseID) Then Return False
                End If

                ' Filter nach Suchbegriff und Category
                If IsNothing(_Searchterm) OrElse Trim(_Searchterm) = "" Then
                    If itm.CategoryID = SelectedCategory.CategoryID Or SelectedCategory.CategoryID = 9999 Then
                        Return True
                    End If
                Else
                    If itm.Artikelbezeichnung.ToLower.Contains(_Searchterm.ToLower) Then Return True
                End If
            End If
            Return False
        End Function

        Private Sub UpdateProductsfilter()
            If Not IsNothing(LVProducts) Then LVProducts.Refresh()
        End Sub

        Private Async Sub AddAction(ByVal selected As Object)
            If IsNothing(selected) Then
                Return
            End If

            BindingOperations.EnableCollectionSynchronization(OCBillings, _listLock)

            IsBusy = True

            Dim selectedProduct = CType(selected, Productlistitem)
            Await Task.Run(Sub() AddActionTask(selectedProduct))

            LoadBillings()

            IsBusy = False
        End Sub

        Private Sub AddActionTask(ByVal selectedProduct As Productlistitem)


            Dim res As Billing = Nothing
            If Not IsNothing(selectedProduct.CourseID) Then
                If IsNothing(Checkin.CustomerID) OrElse Checkin.CustomerID = 0 Then

                    Return
                End If
                res = _bal.OrderCourse(Checkin, selectedProduct.CourseID, SelectedState, CashRegister, selectedProduct.Preis)
            ElseIf Not IsNothing(selectedProduct.PackageID) Then
                res = _bal.OrderPackage(Checkin, selectedProduct.PackageID, SelectedState, _CashRegister, selectedProduct.Preis)
            End If
            If Not IsNothing(res) Then
                'e.Result = res

                _context.Billings.Add(res)
                'OCBillings.Add(res)
                ' Erstmal speichern um BillingID zu bekommen
                _context.SaveChanges(MyGlobals.CurrentUsername)

                ' Auf TSE absichern
                Dim tse As New TSE.TseHandler()
                Dim tseresult = tse.SendeBestellung(res)

                ' Speichern in der Tabelle Tse
                _context.Tses.Add(tseresult)
                _context.SaveChanges(MyGlobals.CurrentUsername)


            End If

        End Sub



        Private Sub OrderAction()
            _context.SaveChanges(MyGlobals.CurrentUsername)
        End Sub

        Private Sub StornoAction(ByVal params As Object)
            Dim bill = TryCast(params, Billing)
            If IsNothing(bill) Then Return

            If bill.IsStorno = True OrElse bill.Verwendungszweck.StartsWith("<Storno") Then Return

            ' Ist bereits Stornobuchung
            If Not IsNothing(bill.Comment) Then
                If Storno.IsStorno(bill.Comment) Then Return
            End If

            Dim newbill = _bal.StornoUnbezahlt(bill)
            _context.Billings.Add(newbill)

            ' Erstmal speichern für Billing ID
            _context.SaveChanges(MyGlobals.CurrentUsername)

            ' Auf TSE absichern
            Dim tse As New TSE.TseHandler()
            Dim tseresult = tse.SendeBestellung(newbill)

            ' Speichern in der Tabelle Tse
            _context.Tses.Add(tseresult)
            _context.SaveChanges(MyGlobals.CurrentUsername)

            LoadBillings()
        End Sub

        Private Sub RabattAction()
            Dim dlg As New WindowController
            Dim view As New RabattView()
            Dim vm As New RabattViewModel()
            vm.Preis = SelectedBill.Brutto
            view.DataContext = vm

            AddHandler vm.RequestClose, AddressOf dlg.CloseWindow
            dlg.ShowWindow(view, "Rabatt")
            If Not IsNothing(vm) AndAlso vm.Result = True Then
                If vm.Preis <> SelectedBill.BruttoOriginal Then

                    'TSE Bestellung minus
                    Dim tse As New TSE.TseHandler()
                    SelectedBill.Brutto = SelectedBill.Brutto * -1
                    Dim tseresult = tse.SendeBestellung(SelectedBill)
                    ' Speichern in der Tabelle Tse
                    _context.Tses.Add(tseresult)

                    'Brutto neu setzen
                    SelectedBill.Brutto = vm.Preis


                    'TSE Bestellung wieder senden
                    tseresult = tse.SendeBestellung(SelectedBill)
                    ' Speichern in der Tabelle Tse
                    _context.Tses.Add(tseresult)

                    ' Alles speichern
                    _context.SaveChanges(MyGlobals.CurrentUserID)

                    LoadBillings()
                End If
            End If

        End Sub

        Private Function CanRabattAction() As Boolean
            If Not IsNothing(SelectedBill) Then
                Return True
            End If
            Return False
        End Function

        Private Sub NavigateCashView()
            OrderAction()
            If Not IsNothing(Checkin) Then
                Dim params As New NavigationParameters()
                params.Add("Checkin", Checkin)
                _regionmanager.RequestNavigate(RegionNames.MainRegion, New Uri("CashView", UriKind.Relative), params)
            End If
        End Sub

        Private Sub NavigateBack()
            Dim mainregion = _regionmanager.Regions(RegionNames.MainRegion)
            mainregion.NavigationService.Journal.GoBack()
        End Sub

        Public Overrides Sub OnNavigatedTo(navigationContext As NavigationContext)
            MyBase.OnNavigatedTo(navigationContext)

            Dim params = navigationContext.Parameters
            If Not IsNothing(params("Checkin")) Then
                Me.Checkin = params("Checkin")
            Else
                If Not IsNothing(params("Customer")) Then
                    Dim cust = TryCast(params("Customer"), Customer)
                    Dim checkin As New Checkin
                    checkin.CheckinID = 0
                    checkin.Vorname = cust.Vorname
                    checkin.Nachname = cust.Nachname
                    checkin.CustomerStateID = cust.CustomerStateID
                    checkin.CustomerID = cust.CustomerID
                    SelectedState = cust.CustomerStateID
                    Me.Checkin = checkin
                End If

            End If

            Load()


        End Sub

    End Class
End Namespace