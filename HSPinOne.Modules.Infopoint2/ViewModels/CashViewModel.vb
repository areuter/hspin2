﻿Imports System.Collections.ObjectModel
Imports System.ComponentModel
Imports System.Data.Entity
Imports System.Windows.Data
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports Prism.Regions
Imports HSPinOne.TSE
Imports HSPinOne.TSE.TseEnums
Imports HSPinOne.Common

Namespace ViewModel


    Public Class CashViewModel
        Inherits ViewModelBase

        Private _context As in1Entities

        Private _Person As String = "Person"
        Public Property Person() As String
            Get
                Return _Person
            End Get
            Set(ByVal value As String)
                _Person = value
            End Set
        End Property

        Private _Checkin As Checkin
        Public Property Checkin() As Checkin
            Get
                Return _Checkin
            End Get
            Set(ByVal value As Checkin)
                _Checkin = value
            End Set
        End Property


        Private _Summe As Decimal = 12.5
        Public Property Summe() As Decimal
            Get
                Return _Summe
            End Get
            Set(ByVal value As Decimal)
                _Summe = value
                RaisePropertyChanged("Summe")
            End Set
        End Property


        Private _LVBillings As ListCollectionView
        Public Property LVBillings() As ListCollectionView
            Get
                Return _LVBillings
            End Get
            Set(ByVal value As ListCollectionView)
                _LVBillings = value
                RaisePropertyChanged("LVBillings")
            End Set
        End Property

        Private _CashRegister As CashRegister
        Public Property CashRegister() As CashRegister
            Get
                Return _CashRegister
            End Get
            Set(ByVal value As CashRegister)
                _CashRegister = value
                RaisePropertyChanged("CashRegister")
            End Set
        End Property

        Private _PaymentMethods As ObservableCollection(Of PaymentMethod)
        Public Property PaymentMethods() As ObservableCollection(Of PaymentMethod)
            Get
                Return _PaymentMethods
            End Get
            Set(ByVal value As ObservableCollection(Of PaymentMethod))
                _PaymentMethods = value
                RaisePropertyChanged("PaymentMethods")
            End Set
        End Property

        Private _Message As String
        Public Property Message() As String
            Get
                Return _Message
            End Get
            Set(ByVal value As String)
                _Message = value
                RaisePropertyChanged("Message")
            End Set
        End Property



        Public Property PayCommand As New RelayCommand(AddressOf PayAction)
        Public Property BackCommand As New RelayCommand(AddressOf NavigateBack)

        Private _Billings As ObservableCollection(Of Billing)
        Private _bal As Order_Bal
        Private _regionmanager As IRegionManager

        Private _bgw As New BackgroundWorker

        Private _kassenr As Integer

        Private _paymentmethod As PaymentMethod

        Private _opiresult As String = ""

        Private _cashclosingid As Integer = 0


        Public Sub New()
            Me._regionmanager = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IRegionManager)

            _context = New in1Entities
        End Sub





        Private Sub LoadBillings()

            _bal = New Order_Bal(MyGlobals.CurrentUsername, _context)

            _kassenr = MachineSettingService.GetKasseNr()
            _cashclosingid = Repositories.CashClosingRepository.GetCashClosingID(_kassenr)

            CashRegister = _context.CashRegisters.Where(Function(o) o.KasseNr = _kassenr).Single()

            Dim pm = _context.PaymentMethods.Where(Function(o) o.IsActive = True).ToList()
            PaymentMethods = New ObservableCollection(Of PaymentMethod)(pm)

            _Billings = _bal.GetBillings(Checkin)

            For Each itm In _Billings
                itm.Checked = True
                AddHandler itm.PropertyChanged, AddressOf Summe_berechnen
            Next

            LVBillings = CollectionViewSource.GetDefaultView(_Billings)

            Summe_berechnen()


        End Sub

        Private Sub Summe_berechnen()
            Summe = _Billings.Where(Function(o) o.Checked = True).Sum(Function(o) o.Brutto)
        End Sub


        Private Sub PayAction(ByVal param As Object)

            Dim belegtyp As TSE.TseEnums.BON_TYP
            Dim presult As Boolean = False
            Dim targetstate As String = BillingStates.Offen

            If PaymentMethods.Any(Function(o) o.PaymentMethodID = param) Then
                _paymentmethod = PaymentMethods.Where(Function(o) o.PaymentMethodID = param).Single

                Select Case param
                    Case 1
                        belegtyp = BON_TYP.Beleg
                        presult = PayCash()
                        targetstate = BillingStates.Bezahlt

                    Case 2
                        belegtyp = BON_TYP.AVBestellung
                        presult = PayLS()
                        targetstate = BillingStates.Offen
                    Case 3
                        belegtyp = BON_TYP.AVBestellung
                        presult = PayInvoice()
                        targetstate = BillingStates.Offen
                    Case 4
                        belegtyp = BON_TYP.Beleg
                        presult = PayCard()

                End Select

                ' Vorgang abbrechen, wenn Fehler beim Zahlungsvorgang
                If presult = False Then
                    Return
                End If


                ' Bon erzeugen für Tse und erstmal speichern

                Dim receipt As New BillingReceipt
                receipt.KasseNr = CashRegister.KasseNr
                receipt.KasseBelegNr = CashRegister.LastReceipt + 1
                receipt.Created = Now
                receipt.Creator = MyGlobals.CurrentUsername
                receipt.BonTyp = belegtyp.ToString()
                receipt.CashClosingID = _cashclosingid
                receipt.PaymentMethodID = _paymentmethod.PaymentMethodID

                If _paymentmethod.PaymentMethodID = 4 Then
                    receipt.PaymentInformation = _opiresult
                End If

                CashRegister.LastReceipt = receipt.KasseBelegNr

                '_context.BillingReceipts.Add(receipt)
                '_context.SaveChanges(MyGlobals.CurrentUsername)

                ' Billings diesem Bon zuordnen
                Dim summe As Decimal = 0
                For Each itm In _Billings
                    If itm.Checked = True Then
                        itm.BillingReceipt = receipt
                        itm.BillingStatus = targetstate
                        If targetstate = BillingStates.Bezahlt Then
                            itm.Buchungsdatum = Now
                        End If

                        itm.PaymentMethodID = _paymentmethod.PaymentMethodID
                        itm.AccountID = _paymentmethod.AccountID

                        If _paymentmethod.PaymentMethodID = 4 Then
                            itm.PaymentInformation = _opiresult
                        End If

                        ' Verknüpfung zum Checkin löschen, da sonst kein Checkout
                        itm.CheckinID = Nothing

                        'receipt.Billings.Add(itm)
                        summe += itm.Brutto
                    End If
                Next
                receipt.Brutto = summe

                ' Versuch zu speichern
                _context.SaveChanges(MyGlobals.CurrentUsername)



                ' Tse Eintrag
                Dim tse As New TseHandler()

                Dim result = tse.SendeKassenbeleg(belegtyp, receipt)
                _context.Tses.Add(result)
                _context.SaveChanges(MyGlobals.CurrentUsername)



                LoadBillings()

                Dim bonsvc As New BonService(receipt.BillingReceiptID, False, Checkin.CustomerID)

                NavigateInfopoint()

                'Dim bondruck As New HSPinOne.Common.Bondruck()
                'bondruck.PrintReceipt(receipt, False, True)
            End If
        End Sub



        Private Function PayCash() As Boolean




            Return True

        End Function

        Private Function PayCard() As Boolean
            Dim opi As New OPIService()
            If opi.CheckTerminal = False Then
                ' Terminal nicht erreichbar
                Message = opi.Message
                Return False
            End If




            For Each itm In _Billings
                If itm.Checked = True Then
                    itm.BillingStatus = BillingStates.Pending  ' Alles auf Pending bevor Übergabe an EC Gerät
                End If
            Next

            ' Speichern vor Übergabe an EC Cash
            _context.SaveChanges(MyGlobals.CurrentUsername)

            If opi.Pay(Summe) Then
                _opiresult = opi.Paymentinfo
                Return True
            Else
                If opi.ReturnCode = 1 Then
                    Dim dlg As New DialogService
                    Dim ans = dlg.AskConfirmation("EC Cash", "Die EC Buchung wurde mit Fehlern beendet. _
                                Soll die Zahlung trotzdem als bezahlt verbucht werden?")

                    If ans = True Then
                        _opiresult = "EC Fehler, manuell verbucht"
                        Return True
                    End If

                End If
            End If
            Return False
        End Function

        Private Function PayLS() As Boolean

            Return True
        End Function

        Private Function PayInvoice() As Boolean

            Return True
        End Function


        Private Sub NavigateBack()
            Dim mainregion = _regionmanager.Regions(RegionNames.MainRegion)
            mainregion.NavigationService.Journal.GoBack()
        End Sub

        Private Sub NavigateInfopoint()
            Dim mainregion = _regionmanager.Regions(RegionNames.MainRegion)
            mainregion.NavigationService.RequestNavigate("Infopoint1View")
        End Sub

        Public Overrides Sub OnNavigatedTo(navigationContext As NavigationContext)
            MyBase.OnNavigatedTo(navigationContext)

            Dim params = navigationContext.Parameters
            If Not IsNothing(params("Checkin")) Then
                Me.Checkin = params("Checkin")
            Else
                If Not IsNothing(params("Customer")) Then
                    Dim cust = TryCast(params("Customer"), Customer)
                    Dim checkin As New Checkin
                    checkin.CheckinID = 0
                    checkin.Vorname = cust.Vorname
                    checkin.Nachname = cust.Nachname
                    checkin.CustomerStateID = cust.CustomerStateID
                    checkin.CustomerID = cust.CustomerID
                    Me.Checkin = checkin
                End If

            End If

            LoadBillings()

        End Sub

    End Class
End Namespace