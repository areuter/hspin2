﻿Imports HSPinOne.Infrastructure
Imports HSPinOne.DAL

Public Class RabattViewModel
    Inherits ViewModelBase


    Private _Preis As Decimal
    Public Property Preis() As Decimal
        Get
            Return _Preis
        End Get
        Set(ByVal value As Decimal)
            _Preis = value
            RaisePropertyChanged("Preis")
        End Set
    End Property

    Public Event RequestClose As EventHandler

    Private _Result As Boolean
    Public ReadOnly Property Result() As Boolean
        Get
            Return _Result
        End Get
    End Property

    Public Property OKCommand As New RelayCommand(AddressOf OKAction, AddressOf CanOKAction)
    Public Property CancelCommand As New RelayCommand(AddressOf CancelAction)



    Public Sub New()

    End Sub

    Private Sub OKAction()
        _Result = True
        RaiseEvent RequestClose(Me, Nothing)
    End Sub

    Private Function CanOKAction() As Boolean
        If Not IsNothing(Preis) AndAlso IsNumeric(Preis) Then
            Return True

        End If
        Return False

    End Function

    Private Sub CancelAction()
        _Result = False
        RaiseEvent RequestClose(Me, Nothing)

    End Sub


End Class
