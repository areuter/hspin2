﻿Imports HSPinOne.Infrastructure
Imports HSPinOne.DAL
Imports Prism.Regions
Imports System.Collections.ObjectModel

Namespace ViewModel


    Public Class CheckinQuickViewModel
        Inherits ViewModelBase

        Private _Checkin As Checkin

#Region "Properties"


        Public Property Checkin() As Checkin
            Get
                Return _Checkin
            End Get
            Set(ByVal value As Checkin)
                _Checkin = value
                RaisePropertyChanged("Checkin")
            End Set
        End Property

        Private _OP As Decimal = 0
        Public Property OP() As Decimal
            Get
                Return _OP
            End Get
            Set(ByVal value As Decimal)
                _OP = value
                RaisePropertyChanged("OP")
            End Set
        End Property

        Private _LVAbos As ObservableCollection(Of Billing)
        Public Property LVAbos() As ObservableCollection(Of Billing)
            Get
                Return _LVAbos
            End Get
            Set(ByVal value As ObservableCollection(Of Billing))
                _LVAbos = value
                RaisePropertyChanged("LVAbos")
            End Set
        End Property

        Public Property CheckoutCommand As New RelayCommand(AddressOf CheckoutAction)
        Public Property OrderCommand As New RelayCommand(AddressOf OrderAction)
        Public Property CashCommand As New RelayCommand(AddressOf CashAction, AddressOf CanCashAction)

        Public Property CancelCommand As New RelayCommand(AddressOf CancelAction)


#End Region

        Private _context As in1Entities
        Private _navcontext As NavigationContext

        Public Sub New()

        End Sub

        Private Sub LoadCheckin(ByVal checkinid As Integer)
            Using con As New in1Entities
                Checkin = con.Checkins.Find(checkinid)
                Dim bal As New Order_Bal(MyGlobals.CurrentUserID, con)
                Dim billings = bal.GetBillings(Checkin)
                OP = billings.Sum(Function(o) o.Brutto)
                LVAbos = bal.GetAbos(Checkin)
            End Using
        End Sub

        Private Sub CancelAction()
            _navcontext.NavigationService.Journal.GoBack()
        End Sub
        Private Sub CheckoutAction()
            If Not IsNothing(Checkin) Then
                Dim dlg As New DialogService

                'Check ob Kunde wirklich noch eingecheckt

                '_context.Entry(Checkin).Reload()
                Dim exists = Nothing
                Using con As New in1Entities
                    exists = con.Checkins.Find(Checkin.CheckinID)


                    If Not IsNothing(exists) Then

                        ' Check ob Tagesgast
                        If IsNothing(Checkin.CustomerID) Then
                            If OP > 0 Then
                                dlg.ShowInfo("Tagesgast", "Checkout von Tagesgästen erst nach Zahlung möglich")
                                Return
                            End If
                        End If

                        'Noch Posten offen?
                        If OP > 0 Then
                            Dim ret = dlg.AskCancelConfirmation("Offene Posten", "Es sind noch Rechnungen offen. Kunde trotzdem auschecken?")
                            If ret <> System.Windows.MessageBoxResult.Yes Then Return
                        End If

                        Dim log As New CheckinLog
                        If Not IsNothing(Checkin.CustomerID) Then
                            log.CustomerID = Checkin.CustomerID
                        End If
                        log.Checkin = Checkin.Checkintime
                        log.CheckinLocation = Checkin.CheckinLocation
                        log.Checkout = Now

                        con.CheckinLogs.Add(log)

                        con.Checkins.Remove(exists)
                        con.SaveChanges()
                    Else
                        dlg.ShowInfo("Checkout", "Der Kunde wurde bereits anderweitig ausgecheckt!")
                    End If
                End Using

                _navcontext.NavigationService.Journal.GoBack()

            End If
        End Sub

        Private Sub OrderAction()
            Dim params As New NavigationParameters
            params.Add("Checkin", Checkin)

            _navcontext.NavigationService.RequestNavigate("OrderView", params)
        End Sub

        Private Sub CashAction()
            Dim params As New NavigationParameters
            params.Add("Checkin", Checkin)

            _navcontext.NavigationService.RequestNavigate("CashView", params)
        End Sub

        Private Function CanCashAction() As Boolean
            If OP > 0 Then Return True
            Return False
        End Function

        Public Overrides Sub OnNavigatedTo(navigationContext As NavigationContext)
            MyBase.OnNavigatedTo(navigationContext)
            _navcontext = navigationContext
            Dim params = navigationContext.Parameters
            If params.ContainsKey("CheckinID") Then
                LoadCheckin(params("CheckinID"))
            Else

            End If

        End Sub

    End Class

End Namespace