﻿

Imports Prism.Modularity
Imports Prism.Ioc
Imports Prism.Regions
Imports HSPinOne.Infrastructure

Public Class Infopoint2Module
    Implements IModule

    Public Sub RegisterTypes(containerRegistry As IContainerRegistry) Implements IModule.RegisterTypes
        'Throw New NotImplementedException()

        containerRegistry.RegisterForNavigation(GetType(OrderView), "OrderView")
        containerRegistry.RegisterForNavigation(GetType(CashView), "CashView")
        containerRegistry.RegisterForNavigation(GetType(CheckinQuickView), "CheckinQuickView")


    End Sub

    Public Sub OnInitialized(containerProvider As IContainerProvider) Implements IModule.OnInitialized
        'Dim rm = containerProvider.Resolve(Of IRegionManager)()
        'rm.RegisterViewWithRegion(RegionNames.TaskRegion, GetType(InfopointOutlookSection))
    End Sub
End Class
