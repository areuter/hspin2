﻿



Public Class TseEnums
        Public Enum ReceiptType
            Beleg
            Bestellung
            Rechnung
        End Enum

        Public Enum BillingEnums
            Umsatz
            Pfand
            PfandRueckzahlung
            Rabatt
            Aufschlag
            ZuschussEcht
            ZuschussUnecht
            TrinkgeldAG
            TrinkgeldAN
            EinzweckgutscheinKauf
            EinzweckgutscheinEinloesung
            MehrzweckgutscheinKauf
            MehrzweckgutscheinEinloesung
            Forderungsentstehung
            Forderungsaufloesung
            Anzahlungseinstellung
            Anzahlungsaufloesung
            Anfangsbestand
            Privatentnahme
            Privateinlage
            Geldtransit
            Lohnzahlung
            Einzahlung
            Auszahlung
            DifferenzSollIst
        End Enum


        Public Enum ZAHLART_TYP
            Bar
            Unbar
            Keine
            ECKarte
            Kreditkarte
            ElZahlungsdienstleister
            Guthabenkarte
        End Enum

        Public Enum BON_TYP
            Beleg
            AVRechnung
            AVTransfer
            AVBestellung
            AVTraining
            AVBelegstorno
            AVBelegabbruch
            AVSachbezug
            AVSonstige
        End Enum

        Public Enum ProcessType

            Kassenbeleg_V1
            Bestellung

        End Enum
    End Class
