﻿Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.Globalization
Imports System.Text
Imports HSPinOne.DAL
Imports System.Linq




Public Enum ProcessType
        Beleg
        Bestellung
    End Enum

    Public Class TseHandler
        ReadOnly cul = CultureInfo.InvariantCulture

        Private _data As String

        Private _taxes As ObservableCollection(Of Tax)

        Private _taxbrutto() As Decimal

        Public Sub New()
            Using con As New in1Entities
                con.Taxes.Load()
                _taxes = con.Taxes.Local()
                ReDim _taxbrutto(_taxes.Count)
                For Each tax In _taxes
                    _taxbrutto(tax.TaxID - 1) = 0
                Next
            End Using
        End Sub

        Private Function AddOrder(ByVal bill As Billing) As String

            '  2;”Eisbecher ““Himbeere“““;3.99\n
            Dim anzahl = 1
            If bill.Brutto < 0 Then anzahl = -1

            Dim sb As New StringBuilder
            sb.Append(anzahl)
            sb.Append(";")
            sb.Append(bill.Verwendungszweck.Replace("""", """"""))
            sb.Append(";")
            sb.Append(bill.Brutto.ToString("F2", cul))
            sb.AppendLine()

            _data = sb.ToString()
            Return _data


        End Function

        Private Function AddBill(ByVal bill As Billing)

            ' Existiert der Steuersatz
            Dim tax = _taxes.Where(Function(o) o.TaxPercent = bill.Steuerprozent).Single
            _taxbrutto(tax.TaxID) = _taxbrutto(tax.TaxID) + bill.Brutto


        End Function
        Public Function Pay(ByVal typ As TseEnums.BON_TYP, ByVal bar As Decimal, ByVal unbar As Decimal) As String
            'Beleg^75.33_7.99_0.00_0.00_0.00^10.00:Bar_5.00:Bar:CHF_5.00:Bar:USD_64.30:Unbar



            Dim sb As New StringBuilder
            sb.Append(typ.ToString())
            sb.Append("^")

            For i = 0 To _taxbrutto.Length - 1
                sb.Append(_taxbrutto(i).ToString("F2", cul))
                sb.Append("_")
            Next
            sb.Append("^")
            If bar > 0 Then
                sb.Append(bar.ToString("F2", cul))
                sb.Append(":")
                sb.Append("Bar")
            End If
            If bar > 0 And unbar > 0 Then
                sb.Append("_")
            End If
            If unbar > 0 Then
                sb.Append(unbar.ToString("F2", cul))
                sb.Append(":")
                sb.Append("Unbar")
            End If
            _data = sb.ToString()
            Return _data

        End Function

    Public Function SendeKassenbeleg(ByVal typ As TseEnums.BON_TYP, ByVal receipt As BillingReceipt) As HSPinOne.DAL.Tse

        For Each bill In receipt.Billings
            AddBill(bill)
        Next

        Dim bar = receipt.Billings.Where(Function(o) o.PaymentMethodID = 1).Sum(Function(o) o.Brutto) ' Barzahlungen
        Dim unbar = receipt.Billings.Where(Function(o) o.PaymentMethodID = 4).Sum(Function(o) o.Brutto) ' Kartenzahlungen

        _data = Pay(typ, bar, unbar)

        Dim result = Send(ProcessType.Beleg, _data)
        result.BillingReceiptID = receipt.BillingReceiptID
        Return result

    End Function

    Public Function SendeBestellung(ByVal bill As Billing) As HSPinOne.DAL.Tse
        Dim data = AddOrder(bill)
        Dim result = Send(ProcessType.Bestellung, _data)
        result.BillingID = bill.BillingID ' Verknüpfung mit der Billing
        Return result
    End Function

    Private Function Send(ByVal typ As ProcessType, ByVal data As String) As HSPinOne.DAL.Tse

        Dim processtype = "Bestellung-V1"

        If typ = TseEnums.ProcessType.Kassenbeleg_V1 Then
            processtype = "Kassenbeleg-V1"
        End If

        Dim tsedata As New TseData
        tsedata.ProcessType = processtype
        tsedata.ProcessData = data

        ' Senden der Daten an die TSE und Antwort abwarten
        ' Antwort dann zurückgeben als Tse und einfügen in Tabelle

        Dim result As New HSPinOne.DAL.Tse
        'result.BillingID = 0
        'result.BillingReceiptID = 0
        'result.Finish = Nothing ' Muss von tse kommen
        result.SignaturCounter = 0 ' von Tse
        result.Signature = "" 'von Tse
        result.Start = Now ' Muss von Tse kommen
        result.Transactionnr = 0 'von Tse
        Return result

    End Function

End Class

    Public Class TseData
        Public Property ProcessType As String
        Public Property ProcessData As String
    End Class




