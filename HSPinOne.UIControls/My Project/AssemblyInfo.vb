﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Resources
Imports System.Windows

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("HSPinOne.UIControls")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Uni Gö")> 
<Assembly: AssemblyProduct("HSPinOne.UIControls")> 
<Assembly: AssemblyCopyright("Copyright © Uni Gö 2013")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("1347dbe6-260a-488f-915c-7de31cf5cf3e")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 

<Assembly: ThemeInfo(ResourceDictionaryLocation.None, ResourceDictionaryLocation.SourceAssembly)> 