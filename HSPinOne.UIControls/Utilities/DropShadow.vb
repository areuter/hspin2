﻿Imports System.Runtime.InteropServices
Imports System.Windows
Imports System.Windows.Interop

Public Class DropShadow

<StructLayout(LayoutKind.Sequential)> _
    Public Structure Margins
        Public Left As Integer
        Public Right As Integer
        Public Top As Integer
        Public Bottom As Integer
    End Structure

    <DllImport("dwmapi.dll")> _
    Private Shared Function DwmSetWindowAttribute(hwnd As IntPtr, attr As Integer, ByRef attrValue As Integer, attrSize As Integer) As Integer
    End Function

    <DllImport("dwmapi.dll")> _
    Private Shared Function DwmExtendFrameIntoClientArea(hWnd As IntPtr, ByRef pMarInset As Margins) As Integer
    End Function

    ''' <summary> 
    ''' Drops a standard shadow to a WPF Window, even if the window isborderless. Only works with DWM (Vista and Seven). 
    ''' This method is much more efficient than setting AllowsTransparency to true and using the DropShadow effect, 
    ''' as AllowsTransparency involves a huge permormance issue (hardware acceleration is turned off for all the window). 
    ''' </summary> 
    ''' <param name="window">Window to which the shadow will be applied</param> 
    Public Shared Sub DropShadowToWindow(window As Window)

        If (Not DropShadow(Window)) Then

            AddHandler Window.SourceInitialized, New EventHandler(AddressOf window_SourceInitialized)
        End If
    End Sub

    Private Shared Sub window_SourceInitialized(ByVal sender As Object, ByVal e As EventArgs)

        Dim window = CType(sender, Window)

        DropShadow(window)
        RemoveHandler window.SourceInitialized, New EventHandler(AddressOf window_SourceInitialized)



    End Sub

    ''' <summary> 
    ''' The actual method that makes API calls to drop the shadow to the window 
    ''' </summary> 
    ''' <param name="window">Window to which the shadow will be applied</param> 
    ''' <returns>True if the method succeeded, false if not</returns> 
    Private Shared Function DropShadow(window As Window) As Boolean

        Try

            Dim helper As WindowInteropHelper = New WindowInteropHelper(window)
            Dim val As Integer = 2
            Dim ret1 As Integer = DwmSetWindowAttribute(helper.Handle, 2, val, 4)

            If (ret1 = 0) Then

                Dim m As Margins = New Margins() With {.Bottom = 1, .Left = 1, .Right = 1, .Top = 1}
                Dim ret2 As Integer = DwmExtendFrameIntoClientArea(helper.Handle, m)
                Return ret2 = 0

            Else

                Return False
            End If

        Catch ex As Exception

            '' Probably dwmapi.dll not found (incompatible OS) 
            Return False
        End Try
    End Function

End Class
