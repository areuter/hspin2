﻿Imports System.ComponentModel
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Input
Imports System.Windows.Media
Imports System.Windows.Documents
Imports System.Windows.Data

Public Class GridViewSort
#Region "Public attached properties"

    Public Shared Function GetCommand(obj As DependencyObject) As ICommand
        Return DirectCast(obj.GetValue(CommandProperty), ICommand)
    End Function

    Public Shared Sub SetCommand(obj As DependencyObject, value As ICommand)
        obj.SetValue(CommandProperty, value)
    End Sub

    ' Using a DependencyProperty as the backing store for Command.  This enables animation, styling, binding, etc...
    Public Shared ReadOnly CommandProperty As DependencyProperty = DependencyProperty.RegisterAttached("Command", GetType(ICommand), GetType(GridViewSort), New UIPropertyMetadata(Nothing, Sub(o, e)
                                                                                                                                                                                                Dim lv = TryCast(o, ItemsControl)
                                                                                                                                                                                                If lv IsNot Nothing Then
                                                                                                                                                                                                    If Not GetAutoSort(lv) Then
                                                                                                                                                                                                        ' Don't change click handler if AutoSort enabled
                                                                                                                                                                                                        If e.OldValue IsNot Nothing AndAlso e.NewValue Is Nothing Then
                                                                                                                                                                                                            lv.[RemoveHandler](GridViewColumnHeader.ClickEvent, New RoutedEventHandler(AddressOf ColumnHeader_Click))
                                                                                                                                                                                                        End If
                                                                                                                                                                                                        If e.OldValue Is Nothing AndAlso e.NewValue IsNot Nothing Then
                                                                                                                                                                                                            lv.[AddHandler](GridViewColumnHeader.ClickEvent, New RoutedEventHandler(AddressOf ColumnHeader_Click))
                                                                                                                                                                                                        End If
                                                                                                                                                                                                    End If
                                                                                                                                                                                                End If

                                                                                                                                                                                            End Sub))

    Public Shared Function GetAutoSort(obj As DependencyObject) As Boolean
        Return CBool(obj.GetValue(AutoSortProperty))
    End Function

    Public Shared Sub SetAutoSort(obj As DependencyObject, value As Boolean)
        obj.SetValue(AutoSortProperty, value)
    End Sub

    ' Using a DependencyProperty as the backing store for AutoSort.  This enables animation, styling, binding, etc...
    Public Shared ReadOnly AutoSortProperty As DependencyProperty = DependencyProperty.RegisterAttached("AutoSort", GetType(Boolean), GetType(GridViewSort), New UIPropertyMetadata(False, Sub(o, e)
                                                                                                                                                                                               Dim lv = CType(o, ListView)
                                                                                                                                                                                               If lv IsNot Nothing Then
                                                                                                                                                                                                   If GetCommand(lv) Is Nothing Then
                                                                                                                                                                                                       ' Don't change click handler if a command is set
                                                                                                                                                                                                       Dim oldValue As Boolean = CBool(e.OldValue)
                                                                                                                                                                                                       Dim newValue As Boolean = CBool(e.NewValue)
                                                                                                                                                                                                       If oldValue AndAlso Not newValue Then
                                                                                                                                                                                                           lv.RemoveHandler(GridViewColumnHeader.ClickEvent, New RoutedEventHandler(AddressOf ColumnHeader_Click))

                                                                                                                                                                                                           'listView.[RemoveHandler](GridViewColumnHeader.ClickEvent, New RoutedEventHandler(AddressOf ColumnHeader_Click))
                                                                                                                                                                                                       End If
                                                                                                                                                                                                       If Not oldValue AndAlso newValue Then
                                                                                                                                                                                                           lv.AddHandler(GridViewColumnHeader.ClickEvent, New RoutedEventHandler(AddressOf ColumnHeader_Click))
                                                                                                                                                                                                       End If
                                                                                                                                                                                                   End If
                                                                                                                                                                                               End If

                                                                                                                                                                                           End Sub))

    Public Shared Function GetPropertyName(obj As DependencyObject) As String
        Return DirectCast(obj.GetValue(PropertyNameProperty), String)
    End Function

    Public Shared Sub SetPropertyName(obj As DependencyObject, value As String)
        obj.SetValue(PropertyNameProperty, value)
    End Sub

    ' Using a DependencyProperty as the backing store for PropertyName.  This enables animation, styling, binding, etc...
    Public Shared ReadOnly PropertyNameProperty As DependencyProperty = DependencyProperty.RegisterAttached("PropertyName", GetType(String), GetType(GridViewSort), New UIPropertyMetadata(Nothing))

    Public Shared Function GetShowSortGlyph(obj As DependencyObject) As Boolean
        Return CBool(obj.GetValue(ShowSortGlyphProperty))
    End Function

    Public Shared Sub SetShowSortGlyph(obj As DependencyObject, value As Boolean)
        obj.SetValue(ShowSortGlyphProperty, value)
    End Sub

    ' Using a DependencyProperty as the backing store for ShowSortGlyph.  This enables animation, styling, binding, etc...
    Public Shared ReadOnly ShowSortGlyphProperty As DependencyProperty = DependencyProperty.RegisterAttached("ShowSortGlyph", GetType(Boolean), GetType(GridViewSort), New UIPropertyMetadata(True))

    Public Shared Function GetSortGlyphAscending(obj As DependencyObject) As ImageSource
        Return DirectCast(obj.GetValue(SortGlyphAscendingProperty), ImageSource)
    End Function

    Public Shared Sub SetSortGlyphAscending(obj As DependencyObject, value As ImageSource)
        obj.SetValue(SortGlyphAscendingProperty, value)
    End Sub

    ' Using a DependencyProperty as the backing store for SortGlyphAscending.  This enables animation, styling, binding, etc...
    Public Shared ReadOnly SortGlyphAscendingProperty As DependencyProperty = DependencyProperty.RegisterAttached("SortGlyphAscending", GetType(ImageSource), GetType(GridViewSort), New UIPropertyMetadata(Nothing))

    Public Shared Function GetSortGlyphDescending(obj As DependencyObject) As ImageSource
        Return DirectCast(obj.GetValue(SortGlyphDescendingProperty), ImageSource)
    End Function

    Public Shared Sub SetSortGlyphDescending(obj As DependencyObject, value As ImageSource)
        obj.SetValue(SortGlyphDescendingProperty, value)
    End Sub

    ' Using a DependencyProperty as the backing store for SortGlyphDescending.  This enables animation, styling, binding, etc...
    Public Shared ReadOnly SortGlyphDescendingProperty As DependencyProperty = DependencyProperty.RegisterAttached("SortGlyphDescending", GetType(ImageSource), GetType(GridViewSort), New UIPropertyMetadata(Nothing))

#End Region

#Region "Private attached properties"

    Private Shared Function GetSortedColumnHeader(obj As DependencyObject) As GridViewColumnHeader
        Return DirectCast(obj.GetValue(SortedColumnHeaderProperty), GridViewColumnHeader)
    End Function

    Private Shared Sub SetSortedColumnHeader(obj As DependencyObject, value As GridViewColumnHeader)
        obj.SetValue(SortedColumnHeaderProperty, value)
    End Sub

    ' Using a DependencyProperty as the backing store for SortedColumn.  This enables animation, styling, binding, etc...
    Private Shared ReadOnly SortedColumnHeaderProperty As DependencyProperty = DependencyProperty.RegisterAttached("SortedColumnHeader", GetType(GridViewColumnHeader), GetType(GridViewSort), New UIPropertyMetadata(Nothing))


#End Region

#Region "Column header click event handler"

    Private Shared Sub ColumnHeader_Click(sender As Object, e As RoutedEventArgs)
        Dim headerClicked As GridViewColumnHeader = TryCast(e.OriginalSource, GridViewColumnHeader)
        If headerClicked IsNot Nothing AndAlso headerClicked.Column IsNot Nothing Then
            'Dim propertyName As String = GetPropertyName(headerClicked.Column)
            Dim memberBinding = CType(e.OriginalSource, GridViewColumnHeader).Column.DisplayMemberBinding
            If Not IsNothing(memberBinding) Then
                Dim propertyName = CType(memberBinding, Binding).Path.Path
                If Not String.IsNullOrEmpty(propertyName) Then
                    Dim listView As ListView = GetAncestor(Of ListView)(headerClicked)
                    If listView IsNot Nothing Then
                        Dim command As ICommand = GetCommand(listView)
                        If command IsNot Nothing Then
                            If command.CanExecute(propertyName) Then
                                command.Execute(propertyName)
                            End If
                        ElseIf GetAutoSort(listView) Then
                            ApplySort(listView.Items, propertyName, listView, headerClicked)
                        End If
                    End If
                End If
            End If
        End If

    End Sub

#End Region

#Region "Helper methods"

    Public Shared Function GetAncestor(Of T As DependencyObject)(reference As DependencyObject) As T
        Dim parent As DependencyObject = VisualTreeHelper.GetParent(reference)
        While Not (TypeOf parent Is T)
            parent = VisualTreeHelper.GetParent(parent)
        End While
        If parent IsNot Nothing Then
            Return DirectCast(parent, T)
        Else
            Return Nothing
        End If
    End Function

    Public Shared Sub ApplySort(view As ICollectionView, propertyName As String, listView As ListView, sortedColumnHeader As GridViewColumnHeader)
        Dim direction As ListSortDirection = ListSortDirection.Ascending

        If view.SortDescriptions.Count > 0 And Not (Keyboard.Modifiers And ModifierKeys.Shift) > 0 Then
            Dim currentSort As SortDescription = view.SortDescriptions(0)
            If currentSort.PropertyName = propertyName Then
                If currentSort.Direction = ListSortDirection.Ascending Then
                    direction = ListSortDirection.Descending
                Else
                    direction = ListSortDirection.Ascending
                End If
            End If
            view.SortDescriptions.Clear()

            Dim currentSortedColumnHeader As GridViewColumnHeader = GetSortedColumnHeader(listView)
            If currentSortedColumnHeader IsNot Nothing Then
                RemoveSortGlyph(currentSortedColumnHeader)
            End If
        End If
        If Not String.IsNullOrEmpty(propertyName) Then
            view.SortDescriptions.Add(New SortDescription(propertyName, direction))
            'If Not GetShowSortGlyph(listView) Then
            RemoveSortGlyph(sortedColumnHeader)
            AddSortGlyph(sortedColumnHeader, direction, If(direction = ListSortDirection.Ascending, GetSortGlyphAscending(listView), GetSortGlyphDescending(listView)))
            'End If
            SetSortedColumnHeader(listView, sortedColumnHeader)
        End If
    End Sub

    Private Shared Sub AddSortGlyph(columnHeader As GridViewColumnHeader, direction As ListSortDirection, sortGlyph As ImageSource)
        Dim adornerLayer__1 As AdornerLayer = AdornerLayer.GetAdornerLayer(columnHeader)
        adornerLayer__1.Add(New SortGlyphAdorner(columnHeader, direction, sortGlyph))
    End Sub

    Private Shared Sub RemoveSortGlyph(columnHeader As GridViewColumnHeader)
        Dim adornerLayer__1 As AdornerLayer = AdornerLayer.GetAdornerLayer(columnHeader)
        Dim adorners As Adorner() = adornerLayer__1.GetAdorners(columnHeader)
        If adorners IsNot Nothing Then
            For Each adorner As Adorner In adorners
                If TypeOf adorner Is SortGlyphAdorner Then
                    adornerLayer__1.Remove(adorner)
                End If
            Next
        End If
    End Sub

#End Region

#Region "SortGlyphAdorner nested class"

    Private Class SortGlyphAdorner
        Inherits Adorner
        Private _columnHeader As GridViewColumnHeader
        Private _direction As ListSortDirection
        Private _sortGlyph As ImageSource

        Public Sub New(columnHeader As GridViewColumnHeader, direction As ListSortDirection, sortGlyph As ImageSource)
            MyBase.New(columnHeader)
            _columnHeader = columnHeader
            _direction = direction
            _sortGlyph = sortGlyph
        End Sub

        Private Function GetDefaultGlyph() As Geometry
            Dim x1 As Double = _columnHeader.ActualWidth - 13
            Dim x2 As Double = x1 + 10
            Dim x3 As Double = x1 + 5
            Dim y1 As Double = _columnHeader.ActualHeight / 2 - 3
            Dim y2 As Double = y1 + 5

            If _direction = ListSortDirection.Ascending Then
                Dim tmp As Double = y1
                y1 = y2
                y2 = tmp
            End If

            Dim pathSegmentCollection As New PathSegmentCollection()
            pathSegmentCollection.Add(New LineSegment(New Point(x2, y1), True))
            pathSegmentCollection.Add(New LineSegment(New Point(x3, y2), True))

            Dim pathFigure As New PathFigure(New Point(x1, y1), pathSegmentCollection, True)

            Dim pathFigureCollection As New PathFigureCollection()
            pathFigureCollection.Add(pathFigure)

            Dim pathGeometry As New PathGeometry(pathFigureCollection)
            Return pathGeometry
        End Function

        Protected Overrides Sub OnRender(drawingContext As DrawingContext)
            MyBase.OnRender(drawingContext)

            If _sortGlyph IsNot Nothing Then
                Dim x As Double = _columnHeader.ActualWidth - 13
                Dim y As Double = _columnHeader.ActualHeight / 2 - 5
                Dim rect As New Rect(x, y, 10, 10)
                drawingContext.DrawImage(_sortGlyph, rect)
            Else
                drawingContext.DrawGeometry(Brushes.LightGray, New Pen(Brushes.Gray, 1.0), GetDefaultGlyph())
            End If
        End Sub
    End Class

#End Region
End Class





