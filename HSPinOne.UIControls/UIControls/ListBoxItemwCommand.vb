﻿Imports System.Windows.Controls
Imports System.Windows.Input
Imports System.Windows


Public Class ListBoxItemwCommand
    Inherits ListBoxItem
    Implements ICommandSource

    ' Make Command a dependency property so it can use databinding.
    Public Shared ReadOnly CommandProperty As DependencyProperty =
        DependencyProperty.Register("Command", GetType(ICommand),
            GetType(ListBoxItemwCommand),
            New PropertyMetadata(CType(Nothing, ICommand),
                New PropertyChangedCallback(AddressOf CommandChanged)))


    Public ReadOnly Property Command1() As ICommand Implements ICommandSource.Command
        Get
            Return CType(GetValue(CommandProperty), ICommand)
        End Get
    End Property

    Public Property Command() As ICommand
        Get
            Return CType(GetValue(CommandProperty), ICommand)
        End Get
        Set(ByVal value As ICommand)
            SetValue(CommandProperty, value)
        End Set
    End Property

    ' Command dependency property change callback.
    Private Shared Sub CommandChanged(ByVal d As DependencyObject, ByVal e As DependencyPropertyChangedEventArgs)
        Dim cs As ListBoxItemwCommand = CType(d, ListBoxItemwCommand)
        cs.HookUpCommand(CType(e.OldValue, ICommand), CType(e.NewValue, ICommand))
    End Sub

    ' Add a new command to the Command Property.
    Private Sub HookUpCommand(ByVal oldCommand As ICommand, ByVal newCommand As ICommand)
        ' If oldCommand is not null, then we need to remove the handlers.
        If oldCommand IsNot Nothing Then
            RemoveCommand(oldCommand, newCommand)
        End If
        AddCommand(oldCommand, newCommand)
    End Sub

    ' Remove an old command from the Command Property.
    Private Sub RemoveCommand(ByVal oldCommand As ICommand, ByVal newCommand As ICommand)
        Dim handler As EventHandler = AddressOf CanExecuteChanged
        RemoveHandler oldCommand.CanExecuteChanged, handler
    End Sub

    ' Add the command.
    Private Sub AddCommand(ByVal oldCommand As ICommand, ByVal newCommand As ICommand)
        Dim handler As New EventHandler(AddressOf CanExecuteChanged)
        Dim canExecuteChangedHandler = handler
        If newCommand IsNot Nothing Then
            AddHandler newCommand.CanExecuteChanged, canExecuteChangedHandler
        End If
    End Sub

    Private Sub CanExecuteChanged(ByVal sender As Object, ByVal e As EventArgs)

        If Me.Command IsNot Nothing Then
            Dim command As RoutedCommand = TryCast(Me.Command, RoutedCommand)

            ' If a RoutedCommand.
            If command IsNot Nothing Then
                If command.CanExecute(CommandParameter, CommandTarget) Then
                    Me.IsEnabled = True
                Else
                    Me.IsEnabled = False
                End If
                ' If a not RoutedCommand.
            Else
                If Me.Command.CanExecute(CommandParameter) Then
                    Me.IsEnabled = True
                Else
                    Me.IsEnabled = False
                End If
            End If
        End If
    End Sub

    Protected Overrides Sub OnMouseLeftButtonDown(e As System.Windows.Input.MouseButtonEventArgs)
        MyBase.OnMouseLeftButtonDown(e)
        If Me.Command IsNot Nothing Then
            Dim command As RoutedCommand = TryCast(Me.Command, RoutedCommand)

            If command IsNot Nothing Then
                command.Execute(CommandParameter, CommandTarget)
            Else
                CType(Me.Command, ICommand).Execute(CommandParameter)
            End If
        End If
    End Sub

    Public ReadOnly Property CommandParameter As Object Implements System.Windows.Input.ICommandSource.CommandParameter
        Get
            Return Nothing
        End Get
    End Property

    Public ReadOnly Property CommandTarget As System.Windows.IInputElement Implements System.Windows.Input.ICommandSource.CommandTarget
        Get
            Return Nothing
        End Get
    End Property

    Public Sub New()
        MyBase.New()

    End Sub
End Class
