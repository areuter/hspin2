﻿Imports System.Windows.Controls
Imports System.Windows
Imports System.Windows.Media
Imports System.Windows.Threading
Imports System.Windows.Input



Public Enum SearchMode
    Instant
    Delayed
End Enum

Public Class SearchTextBox
    Inherits TextBox


    Public Shared LabelTextProperty As DependencyProperty = DependencyProperty.Register("LabelText", GetType(String), GetType(SearchTextBox))

    Public Shared LabelTextColorProperty As DependencyProperty = DependencyProperty.Register("LabelTextColor", GetType(Brush), GetType(SearchTextBox))

    Public Shared SearchModeProperty As DependencyProperty = DependencyProperty.Register("SearchMode", GetType(SearchMode), GetType(SearchTextBox), New PropertyMetadata(SearchMode.Instant))

    Private Shared HasTextPropertyKey As DependencyPropertyKey = DependencyProperty.RegisterReadOnly("HasText", GetType(Boolean), GetType(SearchTextBox), New PropertyMetadata())
    Public Shared HasTextProperty As DependencyProperty = HasTextPropertyKey.DependencyProperty

    Private Shared IsMouseLeftButtonDownPropertyKey As DependencyPropertyKey = DependencyProperty.RegisterReadOnly("IsMouseLeftButtonDown", GetType(Boolean), GetType(SearchTextBox), New PropertyMetadata())
    Public Shared IsMouseLeftButtonDownProperty = IsMouseLeftButtonDownPropertyKey.DependencyProperty

    Public Shared SearchEventTimeDelayProperty As DependencyProperty = DependencyProperty.Register("SearchEventTimeDelay", GetType(Duration), GetType(SearchTextBox), New PropertyMetadata(New Duration(New TimeSpan(0, 0, 0, 0, 500)), New PropertyChangedCallback(AddressOf OnSearchEventTimeDelayChanged)))

    'Public Shared ReadOnly SearchEvent As RoutedEvent = EventManager.RegisterRoutedEvent("Search", RoutingStrategy.Bubble, GetType(RoutedEventHandler), GetType(SearchTextBox))

    Public Shared ReadOnly SearchCommandProperty As DependencyProperty = DependencyProperty.RegisterAttached("SearchCommand", GetType(ICommand), GetType(SearchTextBox))

    Public Shared Function GetSearchCommand(ByVal obj As SearchTextBox) As ICommand
        Return CType(obj.GetValue(SearchCommandProperty), ICommand)
    End Function

    Public Shared Sub SetSearchCommand(ByVal obj As SearchTextBox, ByVal value As ICommand)
        obj.SetValue(SearchCommandProperty, value)
    End Sub


    Public Property LabelText As String
        Get
            Return CType(GetValue(LabelTextProperty), String)
        End Get
        Set(value As String)
            SetValue(LabelTextProperty, value)
        End Set
    End Property

    Public Property LabelTextColor As Brush
        Get
            Return CType(GetValue(LabelTextColorProperty), Brush)
        End Get
        Set(value As Brush)
            SetValue(LabelTextColorProperty, value)
        End Set
    End Property

    Public Property SearchMode As SearchMode
        Get
            Return CType(GetValue(SearchModeProperty), SearchMode)
        End Get
        Set(value As SearchMode)
            SetValue(SearchModeProperty, value)
        End Set
    End Property

    Public Property HasText As Boolean
        Get
            Return CType(GetValue(HasTextProperty), Boolean)
        End Get
        Set(value As Boolean)
            SetValue(HasTextPropertyKey, value)
        End Set
    End Property

    Public Property SearchEventTimeDelay As Duration
        Get
            Return CType(GetValue(SearchEventTimeDelayProperty), Duration)
        End Get
        Set(value As Duration)
            SetValue(SearchEventTimeDelayProperty, value)
        End Set
    End Property

    Public Property IsMouseLeftButtonDown As Boolean
        Get
            Return CType(GetValue(IsMouseLeftButtonDownProperty), Boolean)
        End Get
        Set(value As Boolean)
            SetValue(IsMouseLeftButtonDownPropertyKey, value)
        End Set
    End Property



    Shared Sub New()
        DefaultStyleKeyProperty.OverrideMetadata(GetType(SearchTextBox), New FrameworkPropertyMetadata(GetType(SearchTextBox)))


    End Sub

    Private searchEventDelayTimer As DispatcherTimer

    Public Sub New()
        MyBase.New()

        searchEventDelayTimer = New DispatcherTimer
        searchEventDelayTimer.Interval = SearchEventTimeDelay.TimeSpan
        AddHandler searchEventDelayTimer.Tick, AddressOf OnSearchEventDelayTimerTick

    End Sub

    Sub OnSearchEventDelayTimerTick(ByVal o As Object, ByVal e As EventArgs)
        searchEventDelayTimer.Stop()
        RaiseSearchEvent()
    End Sub


    Shared Sub OnSearchEventTimeDelayChanged(ByVal o As DependencyObject, ByVal e As DependencyPropertyChangedEventArgs)
        Dim stb As SearchTextBox = CType(o, SearchTextBox)
        If Not IsNothing(stb) Then
            stb.searchEventDelayTimer.Interval = CType(e.NewValue, Duration).TimeSpan
            stb.searchEventDelayTimer.Stop()
        End If
    End Sub

    Public Overrides Sub OnApplyTemplate()
        MyBase.OnApplyTemplate()
        Dim iconBorder As Border = CType(GetTemplateChild("PART_SearchIconBorder"), Border)
        If Not IsNothing(iconBorder) Then
            AddHandler iconBorder.MouseLeftButtonDown, AddressOf IconBorder_MouseLeftButtonDown
            AddHandler iconBorder.MouseLeftButtonUp, AddressOf IconBorder_MouseLeftButtonUp
            AddHandler iconBorder.MouseLeave, AddressOf IconBorder_MouseLeave
        End If
    End Sub

    Private Sub IconBorder_MouseLeftButtonDown(ByVal obj As Object, ByVal e As MouseButtonEventArgs)
        IsMouseLeftButtonDown = True
    End Sub

    Private Sub IconBorder_MouseLeftButtonUp(ByVal obj As Object, ByVal e As MouseButtonEventArgs)
        If Not IsMouseLeftButtonDown Then Return

        If HasText And SearchMode = SearchMode.Instant Then Me.Text = ""

        If HasText And SearchMode = SearchMode.Delayed Then RaiseSearchEvent()

        IsMouseLeftButtonDown = False
    End Sub

    Private Sub IconBorder_MouseLeave(ByVal obj As Object, ByVal e As MouseEventArgs)
        IsMouseLeftButtonDown = False
    End Sub

    Protected Overrides Sub OnKeyDown(e As System.Windows.Input.KeyEventArgs)
        If (e.Key = Key.Escape And SearchMode = SearchMode.Instant) Then
            Me.Text = ""
        ElseIf (e.Key = Key.Return Or e.Key = Key.Enter) And SearchMode = SearchMode.Delayed Then
            RaiseSearchEvent()
        Else
            MyBase.OnKeyDown(e)
        End If


    End Sub

    Protected Overrides Sub OnTextChanged(e As System.Windows.Controls.TextChangedEventArgs)
        MyBase.OnTextChanged(e)
        HasText = Text.Length <> 0

        If SearchMode = SearchMode.Instant Then
            searchEventDelayTimer.Stop()
            searchEventDelayTimer.Start()
        End If

    End Sub

    Private Sub RaiseSearchEvent()


        Dim cmd = GetSearchCommand(Me)
        If Not IsNothing(cmd) Then cmd.Execute(Nothing)




    End Sub

End Class
