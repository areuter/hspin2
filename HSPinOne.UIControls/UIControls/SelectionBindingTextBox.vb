﻿Imports System.Windows.Controls
Imports System.Windows

Public Class SelectionBindingTextBox
    Inherits TextBox



    Public Shared ReadOnly BindableSelectionStartProperty As DependencyProperty = DependencyProperty.Register("BindableSelectionStart", GetType(Integer), GetType(SelectionBindingTextBox), New PropertyMetadata(0, New PropertyChangedCallback(AddressOf OnBindableSelectionStartChanged)))

    Public Shared ReadOnly BindableSelectionLengthProperty As DependencyProperty = DependencyProperty.Register("BindableSelectionLength", GetType(Integer), GetType(SelectionBindingTextBox), New PropertyMetadata(0, New PropertyChangedCallback(AddressOf OnBindableSelectionLengthChanged)))

    Private changeFromUI As Boolean

    Public Sub New()
        AddHandler Me.SelectionChanged, AddressOf Me.OnSelectionChanged1
    End Sub

    Public Property BindableSelectionStart() As Integer
        Get
            Return CInt(GetValue(BindableSelectionStartProperty))
        End Get

        Set(value As Integer)
            SetValue(BindableSelectionStartProperty, value)
        End Set
    End Property

    Public Property BindableSelectionLength() As Integer
        Get
            Return CInt(GetValue(BindableSelectionLengthProperty))
        End Get

        Set(value As Integer)
            SetValue(BindableSelectionLengthProperty, value)
        End Set
    End Property

    Private Shared Sub OnBindableSelectionStartChanged(dependencyObject As DependencyObject, args As DependencyPropertyChangedEventArgs)
        Dim textBox = TryCast(dependencyObject, SelectionBindingTextBox)

        If Not textBox.changeFromUI Then
            Dim newValue As Integer = CInt(args.NewValue)
            textBox.SelectionStart = newValue
        Else
            textBox.changeFromUI = False
        End If
    End Sub

    Private Shared Sub OnBindableSelectionLengthChanged(dependencyObject As DependencyObject, args As DependencyPropertyChangedEventArgs)
        Dim textBox = TryCast(dependencyObject, SelectionBindingTextBox)

        If Not textBox.changeFromUI Then
            Dim newValue As Integer = CInt(args.NewValue)
            textBox.SelectionLength = newValue
        Else
            textBox.changeFromUI = False
        End If
    End Sub

    Private Sub OnSelectionChanged1(sender As Object, e As RoutedEventArgs)
        If Me.BindableSelectionStart <> Me.SelectionStart Then
            Me.changeFromUI = True
            Me.BindableSelectionStart = Me.SelectionStart
        End If

        If Me.BindableSelectionLength <> Me.SelectionLength Then
            Me.changeFromUI = True
            Me.BindableSelectionLength = Me.SelectionLength
        End If
    End Sub


End Class
