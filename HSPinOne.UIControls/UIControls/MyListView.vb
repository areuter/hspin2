﻿Imports System.Windows.Controls
Imports System.Windows
Imports System.Windows.Media

Public Class MyListView
    Inherits ListView


    Public Property AlternatingRowBackground As Brush
        Get
            Return CType(GetValue(AlternatingRowBackgroundProperty), Brush)
        End Get
        Set(value As Brush)
            SetValue(AlternatingRowBackgroundProperty, value)
        End Set
    End Property

    Public Shared ReadOnly AlternatingRowBackgroundProperty As DependencyProperty = DependencyProperty.Register("AlternatingRowBackground", GetType(Brush), GetType(MyListView))



    Shared Sub New()
        DefaultStyleKeyProperty.OverrideMetadata(GetType(MyListView), New FrameworkPropertyMetadata(GetType(MyListView)))
    End Sub



    Protected Overrides Function GetContainerForItemOverride() As DependencyObject
        'Return MyBase.GetContainerForItemOverride()
        Return New MyListViewItem()
    End Function


End Class


