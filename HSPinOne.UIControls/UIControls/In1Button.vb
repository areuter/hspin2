﻿Imports System.Windows.Controls
Imports System.Windows
Imports System.Windows.Media
Imports MahApps.Metro.IconPacks

Public Class In1Button
    Inherits Button





    Public Property IconSource As PackIconMaterialKind
        Get
            Return CType(GetValue(IconSourceProperty), PackIconMaterialKind)
        End Get
        Set(value As PackIconMaterialKind)
            SetValue(IconSourceProperty, value)
        End Set
    End Property

    Public Shared ReadOnly IconSourceProperty As DependencyProperty = DependencyProperty.Register("IconSource", GetType(PackIconMaterialKind), GetType(In1Button), New PropertyMetadata(PackIconMaterialKind.None))


    Public Property IconSize As Integer
        Get
            Return CType(GetValue(IconSizeProperty), Integer)
        End Get
        Set(value As Integer)
            SetValue(IconSizeProperty, value)
        End Set
    End Property

    Public Shared ReadOnly IconSizeProperty As DependencyProperty = DependencyProperty.Register("IconSize", GetType(Integer), GetType(In1Button), New PropertyMetadata(36))



    Public Property IconPosition() As Dock
        Get
            Return GetValue(IconPositionProperty)
        End Get
        Set(ByVal value As Dock)
            SetValue(IconPositionProperty, value)
        End Set
    End Property

    Public Shared ReadOnly IconPositionProperty As DependencyProperty = DependencyProperty.Register("IconPosition", GetType(Dock), GetType(In1Button), New PropertyMetadata(Dock.Top))

    Shared Sub New()
        DefaultStyleKeyProperty.OverrideMetadata(GetType(In1Button), New FrameworkPropertyMetadata(GetType(In1Button)))
    End Sub

End Class
