﻿Imports System.Windows.Controls
Imports System.Windows
Imports System.Windows.Media

Public Class OutlookTabItem
    Inherits TabItem

    Public Property TagName As String

    Public Property AlternatingRowBackground As Brush
        Get
            Return CType(GetValue(AlternatingRowBackgroundProperty), Brush)
        End Get
        Set(value As Brush)
            SetValue(AlternatingRowBackgroundProperty, value)
        End Set
    End Property

    Public Shared ReadOnly AlternatingRowBackgroundProperty As DependencyProperty = DependencyProperty.Register("AlternatingRowBackground", GetType(Brush), GetType(OutlookTabItem))


    Shared Sub New()
        DefaultStyleKeyProperty.OverrideMetadata(GetType(OutlookTabItem), New FrameworkPropertyMetadata(GetType(OutlookTabItem)))
    End Sub
End Class


