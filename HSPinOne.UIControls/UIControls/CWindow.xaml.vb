﻿Imports System.Windows.Controls
Imports System.Windows.Input
Imports System.Windows
Imports System.Windows.Shapes
Imports System.Runtime.InteropServices
Imports System.Windows.Interop
Imports System.Windows.Media


Public Class CWindow
    Inherits Window


    '<DllImport("user32.dll")> _
    'Private Shared Function SetWindowRgn(ByVal hWnd As IntPtr, hRgn As IntPtr, bRedraw As Boolean) As Int32
    'End Function

    '<DllImport("gdi32.dll")> _
    'Private Shared Function CreateRoundRectRgn(x1 As Integer, y1 As Integer, x2 As Integer, y2 As Integer, cx As Integer, cy As Integer) As IntPtr

    'End Function


    Private _isResizing As Boolean
    Private _initY As Double
    Private _initX As Double
    Private direction As ResizeDirection

    Private Const CURSOR_OFFSET_SMALL As Double = 3
    Private Const CURSOR_OFFSET_LARGE As Double = 5


    Private _x As Double = 0
    Private _Y As Double = 0

    Public Event CloseButtonClicked As EventHandler

    Public Event RequestClose As EventHandler

    Private Enum ResizeDirection
        Top
        Bottom
        Left
        Right
        TopLeft
        BottomRight
        TopRight
        BottomLeft
    End Enum

    Private _WindowTitle As String = String.Empty
    Public Property WindowTitle As String
        Get
            Return _WindowTitle
        End Get
        Set(value As String)
            _WindowTitle = value

        End Set
    End Property

    Private _CanClose As Boolean = False
    Public Property CanClose As Boolean
        Get
            Return _CanClose
        End Get
        Set(value As Boolean)
            _CanClose = value
        End Set
    End Property

    Private _IsClosing As Boolean = False
    Public Property IsClosing As Boolean
        Get
            Return _IsClosing
        End Get
        Set(value As Boolean)
            _IsClosing = value
        End Set
    End Property

    Private _CanResize As Boolean = True
    Public Property CanResize As Boolean
        Get
            Return _CanResize
        End Get
        Set(value As Boolean)
            _CanResize = value
        End Set
    End Property

    Protected Overrides Sub OnInitialized(e As EventArgs)
        DropShadow.DropShadowToWindow(Me)
        MyBase.OnInitialized(e)
    End Sub

    Protected Overrides Sub OnClosing(e As ComponentModel.CancelEventArgs)

        If Not CanClose Then
            e.Cancel = True
        End If
        MyBase.OnClosing(e)
    End Sub

    Private Sub MainWindowClosed(sender As Object, e As EventArgs)
        Me.Close()
    End Sub

    Private Sub Window_SizeChanged(sender As Object, e As SizeChangedEventArgs)

    End Sub

    Private Sub Window_KeyUp(sender As Object, e As KeyEventArgs)

    End Sub

    Private Sub Window_MouseLeftButtonDown(sender As Object, e As MouseButtonEventArgs)

    End Sub

    Private Sub Window_MouseLeftButtonUp(sender As Object, e As MouseButtonEventArgs)
        _isResizing = False
    End Sub

    Private Sub Window_MouseDoubleClicked(sender As Object, e As MouseButtonEventArgs)
        'Dim pos = Mouse.GetPosition(Me)
        'If pos.Y < 20 Then
        '    ToggleWindowSize()
        'End If
    End Sub

    Private Sub ToggleWindowSize()

        If Me.WindowState = WindowState.Maximized Then
            Me.WindowState = WindowState.Normal
        Else
            Me.WindowState = WindowState.Maximized
        End If

    End Sub



    Private Sub Initialize_Sizes()

        Dim _MinWidth As Double = DragHandleLeft.Width + _
                                  DragHandleRight.Width + _
                                  Border_Chrome.BorderThickness.Left + _
                                  Border_Chrome.BorderThickness.Right + 100

        If MinWidth < _MinWidth Then _
           MinWidth = _MinWidth

        Dim _MinHeight As Double = DragHandleTop.Height + _
                                   DragHandleBottom.Height + _
                                   Border_Chrome.BorderThickness.Top + _
                                   Border_Chrome.BorderThickness.Bottom + 100

        If MinHeight < _MinHeight Then _
           MinHeight = _MinHeight

    End Sub

    Private Sub Resize_Begin(ByVal sender As Object, ByVal e As MouseEventArgs)
        _isResizing = True
        DirectCast(sender, Rectangle).CaptureMouse()
    End Sub

    Private Sub Resize_End(ByVal sender As Object, ByVal e As MouseEventArgs)
        _isResizing = False
        DirectCast(sender, Rectangle).ReleaseMouseCapture()
    End Sub

    Private Sub Resize(ByVal sender As Object, ByVal e As MouseEventArgs)
        If _isResizing = False Then Exit Sub

        _x = e.GetPosition(Me).X
        _Y = e.GetPosition(Me).Y

        Select Case DirectCast(sender, Rectangle).Name

            Case "DragHandleTopLeft" : Resize_Width_Left()
                Resize_Height_Top()
            Case "DragHandleTop" : Resize_Height_Top()
            Case "DragHandleTopRight" : Resize_Width_Right()
                Resize_Height_Top()

            Case "DragHandleLeft" : Resize_Width_Left()
            Case "DragHandleRight" : Resize_Width_Right()


            Case "DragHandleBottomLeft" : Resize_Width_Left()
                Resize_Height_Bottom()
            Case "DragHandleBottom" : Resize_Height_Bottom()
            Case "DragHandleBottomRight" : Resize_Width_Right()
                Resize_Height_Bottom()

            Case Else : MessageBox.Show("Error in Resize")

        End Select

    End Sub


    Private Sub Resize_Width_Left()

        _x -= CURSOR_OFFSET_SMALL

        If Width - _x >= MinWidth Then
            If Width - _x <= MaxWidth Then

                Width -= _x
                Left += _x

            End If
        End If

    End Sub

    Private Sub Resize_Width_Right()

        _x += CURSOR_OFFSET_LARGE

        Select Case _x

            Case Is < MinWidth : Width = MinWidth
            Case Is > MaxWidth : Width = MaxWidth

            Case Else : Width = _x

        End Select

    End Sub

    Private Sub Resize_Height_Top()

        _Y -= CURSOR_OFFSET_SMALL

        If Height - _Y >= MinHeight Then
            If Height - _Y <= MaxHeight Then

                Height -= _Y
                Top += _Y

            End If
        End If

    End Sub

    Private Sub Resize_Height_Bottom()

        _Y += CURSOR_OFFSET_SMALL

        Select Case _Y

            Case Is < MinHeight : Height = MinHeight
            Case Is > MaxHeight : Height = MaxHeight

            Case Else : Height = _Y

        End Select

    End Sub



    ' Drag Handles


    Private Sub DragHandle_MouseEnter(sender As Object, e As MouseEventArgs)

    End Sub



    Private Sub btnMinimize_Clicked(sender As Object, e As RoutedEventArgs)
        Me.WindowState = WindowState.Minimized
    End Sub

    Private Sub btnMaxRestore_Clicked(sender As Object, e As RoutedEventArgs)
        If Me.WindowState = WindowState.Maximized Then
            Me.WindowState = WindowState.Normal
        Else
            Me.WindowState = WindowState.Maximized
        End If

    End Sub

    Protected Overrides Sub OnStateChanged(e As EventArgs)
        If Me.WindowState = WindowState.Maximized Then
            Me.btnMaxRestore.Content = 2
        Else
            Me.btnMaxRestore.Content = 1
        End If



        MyBase.OnStateChanged(e)

    End Sub

    Private Sub btnClose_Clicked(sender As Object, e As RoutedEventArgs)
        'Me.Close()
        RaiseEvent CloseButtonClicked(Me, Nothing)
    End Sub

    Public Sub SetContent(ByVal uc As UserControl)
        Me.UCContent.Content = uc

    End Sub



    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Initialize_Sizes()
    End Sub

    Private Sub DockPanel1_MouseLeftButtonDown(sender As Object, e As MouseButtonEventArgs) Handles DockPanel1.MouseLeftButtonDown
        If Not _isResizing Then
            Me.DragMove()
        End If
    End Sub

    'Private Sub CWindow_Loaded(sender As Object, e As System.Windows.RoutedEventArgs) Handles Me.Loaded
    '    Dim hwnd As IntPtr = New WindowInteropHelper(Me).Handle
    '    SetWindowRgn(hwnd, CreateRoundRectRgn(0, 0, 300, 300, 75, 75), True)
    'End Sub
End Class
