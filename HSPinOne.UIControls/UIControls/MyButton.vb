﻿Imports System.Windows.Controls
Imports System.Windows
Imports System.Windows.Media

Public Class MyButton
    Inherits Button


    Shared Sub New()
        DefaultStyleKeyProperty.OverrideMetadata(GetType(MyButton), New FrameworkPropertyMetadata(GetType(MyButton)))
    End Sub

    Public Property ImageSource As ImageSource
        Get
            Return CType(GetValue(ImageSourceProperty), ImageSource)
        End Get
        Set(value As ImageSource)
            SetValue(ImageSourceProperty, value)
        End Set
    End Property

    Public Shared ReadOnly ImageSourceProperty As DependencyProperty = DependencyProperty.Register("ImageSource", GetType(ImageSource), GetType(MyButton))

    Public Property ImageBrush As DrawingBrush
        Get
            Return CType(GetValue(ImageBrushProperty), DrawingBrush)
        End Get
        Set(value As DrawingBrush)
            SetValue(ImageBrushProperty, value)

        End Set
    End Property

    Public Shared ReadOnly ImageBrushProperty As DependencyProperty = DependencyProperty.Register("ImageBrush", GetType(DrawingBrush), GetType(MyButton))

    Public Property IconBrush As Canvas
        Get
            Return CType(GetValue(IconBrushProperty), Canvas)
        End Get
        Set(value As Canvas)
            SetValue(IconBrushProperty, value)

        End Set
    End Property

    Public Shared ReadOnly IconBrushProperty As DependencyProperty = DependencyProperty.Register("IconBrush", GetType(Canvas), GetType(MyButton))



End Class
