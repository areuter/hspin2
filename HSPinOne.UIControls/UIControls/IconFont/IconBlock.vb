﻿
Imports System.ComponentModel
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Media

Public Class IconBlock
    Inherits TextBlock

    Public Shared ReadOnly Font As New FontFamily(New Uri("pack://application:,,,/HSPinOne.WPF.Shell;component/fonts/#inone"), "./#inone")



    Public Property Icon() As IconChar
        Get
            Return DirectCast(GetValue(IconProperty), IconChar)
        End Get
        Set(value As IconChar)
            SetValue(IconProperty, value)
        End Set
    End Property

    ' Using a DependencyProperty as the backing store for Icon.  This enables animation, styling, binding, etc...
    Public Shared ReadOnly IconProperty As DependencyProperty = DependencyProperty.Register("Icon", GetType(IconChar), GetType(IconBlock), New PropertyMetadata(IconChar.None, AddressOf OnIconPropertyChanged))

    Private Shared Sub OnIconPropertyChanged(d As DependencyObject, e As DependencyPropertyChangedEventArgs)
        d.SetValue(TextBlock.TextProperty, Char.ConvertFromUtf32(CInt(e.NewValue)))
    End Sub

    Public Sub New()

        Dim descriptor = DependencyPropertyDescriptor.FromProperty(TextBlock.TextProperty, GetType(IconBlock))
        descriptor.AddValueChanged(Me, AddressOf OnTextValueChanged)


        Me.FontFamily = Font
        Me.VerticalAlignment = System.Windows.VerticalAlignment.Center
        Me.HorizontalAlignment = System.Windows.HorizontalAlignment.Center

        Me.SnapsToDevicePixels = True
    End Sub

    Private Sub OnTextValueChanged(sender As Object, e As EventArgs)

        Dim str = Me.Text
        If str.Length = 1 AndAlso [Enum].IsDefined(GetType(IconChar), Char.ConvertToUtf32(str, 0)) Then
            Return
        Else
            Throw New FormatException()
        End If

    End Sub
End Class


