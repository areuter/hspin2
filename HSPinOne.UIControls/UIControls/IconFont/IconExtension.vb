﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Windows.Markup


Public Class IconExtension
    Inherits MarkupExtension

    Public Property ToolTip() As String
        Get
            Return TryCast(_control.ToolTip, String)
        End Get
        Set(value As String)
            _control.ToolTip = value
        End Set
    End Property

    Private _control As IconBlock

    Public Sub New(symbol As IconChar)
        _control = New IconBlock() With {.Icon = symbol}
    End Sub

    Public Overrides Function ProvideValue(serviceProvider As IServiceProvider) As Object
        Return _control
    End Function
End Class


