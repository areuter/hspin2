﻿Imports System.Windows
Imports System.Windows.Media
Imports System.Windows.Markup

Public Class FontIcon
    Inherits MarkupExtension


    Public Property Text As String

    Public Property FontFamily As FontFamily

    Public Property Style As FontStyle


    Public Property Weight As FontWeight

    Public Property Stretch As FontStretch

    Public Property Brush As Brush

    Public Property Size As Double

    Public Sub New()
        Text = "G"
        FontFamily = New FontFamily(New Uri("pack://application:,,,/HSPinOne.WPF.Shell;component/fonts/#inone"), "./#inone")
        Style = FontStyles.Normal
        Weight = FontWeights.Normal
        Stretch = FontStretches.Normal
        Size = 1
        Brush = New SolidColorBrush(Colors.Black)
    End Sub




    Private Shared Function CreateGlyph(text As String, fontFamily As FontFamily, fontStyle As FontStyle, fontWeight As FontWeight, fontStretch As FontStretch, foreBrush As Brush, size As Double) As ImageSource
        If fontFamily IsNot Nothing AndAlso Not [String].IsNullOrEmpty(text) Then
            Dim typeface As New Typeface(fontFamily, fontStyle, fontWeight, fontStretch)
            Dim glyphTypeface As GlyphTypeface = New GlyphTypeface()
            If Not typeface.TryGetGlyphTypeface(glyphTypeface) Then
                Throw New InvalidOperationException("No glyphtypeface found")
            End If

            Dim glyphIndexes As UShort() = New UShort(text.Length - 1) {}
            Dim advanceWidths As Double() = New Double(text.Length - 1) {}
            For n As Integer = 1 To text.Length
                Dim glyphIndex As UShort = glyphTypeface.CharacterToGlyphMap(AscW(Mid(text, n, 1)))
                glyphIndexes(n - 1) = glyphIndex
                Dim width As Double = glyphTypeface.AdvanceWidths(glyphIndex) * 1.0
                advanceWidths(n - 1) = width
            Next

            Dim gr As New GlyphRun(glyphTypeface, 0, False, size, glyphIndexes, New Point(0, 0), _
             advanceWidths, Nothing, Nothing, Nothing, Nothing, Nothing, _
             Nothing)
            Dim glyphRunDrawing As New GlyphRunDrawing(foreBrush, gr)

            Return New DrawingImage(glyphRunDrawing)
        End If
        Return Nothing
    End Function

    Public Overrides Function ProvideValue(serviceProvider As System.IServiceProvider) As Object

        Return CreateGlyph(Text, FontFamily, Style, Weight, Stretch, Brush, Size)
    End Function
End Class
