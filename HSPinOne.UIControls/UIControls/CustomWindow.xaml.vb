﻿Imports System.Windows
Imports System.Windows.Controls
Imports System.ComponentModel
Imports MahApps.Metro.Controls
Imports System.Windows.Input
Imports System

Public Class CustomWindow
    Inherits MetroWindow



    Public Event CloseButtonClicked As EventHandler
    Public Event Window_Closed As EventHandler
    Public Event RequestClose As EventHandler

    Private _CanClose As Boolean = True
    Public Property CanClose As Boolean
        Get
            Return _CanClose
        End Get
        Set(value As Boolean)
            _CanClose = value
        End Set
    End Property

    Private _IsClosing As Boolean = False
    Public Property IsClosing As Boolean
        Get
            Return _IsClosing
        End Get
        Set(value As Boolean)
            _IsClosing = value
        End Set
    End Property

    Private _ShowTitle As Boolean = True
    Public Property ShowTitle() As Boolean
        Get
            Return _ShowTitle
        End Get
        Set(ByVal value As Boolean)
            _ShowTitle = value
            Me.ShowTitleBar = value
            Me.ShowInTaskbar = value
        End Set
    End Property


    Public Sub SetContent(ByVal uc As UserControl)
        Me.UCContent.Content = uc

    End Sub

    Private Sub CustomWindow_Closing(ByVal sender As Object, ByVal e As EventArgs)

        'Me.Close()
    End Sub


    Protected Overrides Sub OnClosing(e As CancelEventArgs)
        UpdateFocusedField()
        IsClosing = True
        RaiseEvent CloseButtonClicked(Me, e)

        If Not CanClose Then
            e.Cancel = True
        End If
        MyBase.OnClosing(e)
    End Sub


    Private Sub UpdateFocusedField()
        Dim fwe As FrameworkElement = TryCast(Keyboard.FocusedElement, FrameworkElement)
        If fwe IsNot Nothing Then
            Dim expression As Data.BindingExpression = Nothing
            If TypeOf fwe Is TextBox Then
                expression = fwe.GetBindingExpression(TextBox.TextProperty)
            End If

            If expression IsNot Nothing Then
                expression.UpdateSource()
            End If

        End If

    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        AddHandler Me.PreviewKeyDown, AddressOf EscapeKeyPressed
    End Sub

    Private Sub EscapeKeyPressed(ByVal sender As Object, ByVal e As KeyEventArgs)
        If e.Key = Key.Escape Then
            Me.Close()
        End If
    End Sub
End Class
