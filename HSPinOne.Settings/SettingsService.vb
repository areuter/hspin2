﻿
Imports CommonServiceLocator
Imports HSPinOne.Infrastructure
Imports Prism.Regions
Imports System.ComponentModel.Composition





Public Class SettingsService
    Implements ISettingsService


    Private _regionmanager As IRegionManager

    Private win As MainSettingsView


    Public Sub GetSettings() Implements ISettingsService.GetSettings

        Dim vm = ServiceLocator.Current.GetInstance(Of ViewModel.MainSettingsViewModel)()
        win = New MainSettingsView
        win.ViewModel = vm
        _regionmanager = vm.RegionManager
        If Not _regionmanager.Regions.Where(Function(o) o.Name = RegionNames.SettingRegion).Any Then
            win.SetValue(RegionManager.RegionManagerProperty, _regionmanager)
        End If

        win.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner
        win.Title = "Einstellungen"
        win.Width = 960
        win.Height = 720

        AddHandler vm.RequestClose, AddressOf CloseWin
        AddHandler win.Closed, AddressOf RemoveReg

        win.ShowDialog()


    End Sub

    Private Sub RemoveReg()
        _regionmanager.Regions.Remove(RegionNames.SettingRegion)
    End Sub


    Private Sub CloseWin()

        win.Close()

        win = Nothing
    End Sub

    Public Sub New()
        Me._regionmanager = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IRegionManager)


    End Sub
End Class
