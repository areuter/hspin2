﻿
Imports System.Windows.Input
Imports HSPinOne.Infrastructure


Namespace ViewModel




    Public Class SettingItemViewModel
        Inherits ViewModelBase


        Private _menuitemname As String

        Public Property MenuItemName As String
            Get
                Return _menuitemname
            End Get
            Set(value As String)
                _menuitemname = value
                RaisePropertyChanged("MenuItemName")
            End Set
        End Property



        Private _menuItemCommand As ICommand

        Public Property MenuItemCommand
            Get
                Return _menuItemCommand
            End Get
            Set(value)
                _menuItemCommand = value
                RaisePropertyChanged("MenuItemcommand")
            End Set
        End Property


    End Class

End Namespace

