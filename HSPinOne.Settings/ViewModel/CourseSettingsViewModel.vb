﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.Windows.Input
Imports System.Windows.Data
Imports System.Linq

Namespace ViewModel


    Public Class CourseSettingsViewModel
        Inherits ViewModelBase

        Private _context As in1Entities



        Private _audience As ObservableCollection(Of CourseAudience)
        Public Property Audience As ObservableCollection(Of CourseAudience)
            Get
                Return _audience
            End Get
            Set(value As ObservableCollection(Of CourseAudience))
                _audience = value
            End Set
        End Property



        Private _gender As ObservableCollection(Of CourseGender)
        Public Property Gender As ObservableCollection(Of CourseGender)
            Get
                Return _gender
            End Get
            Set(value As ObservableCollection(Of CourseGender))
                _gender = value
            End Set
        End Property

        Private _cost As ObservableCollection(Of CourseCost)
        Public Property Cost As ObservableCollection(Of CourseCost)
            Get
                Return _cost
            End Get
            Set(value As ObservableCollection(Of CourseCost))
                _cost = value
            End Set
        End Property

        Private _registration As ObservableCollection(Of CourseRegistration)
        Public Property Registration As ObservableCollection(Of CourseRegistration)
            Get
                Return _registration
            End Get
            Set(value As ObservableCollection(Of CourseRegistration))
                _registration = value
            End Set
        End Property

        Private _languages As ObservableCollection(Of Language)
        Public Property Languages() As ObservableCollection(Of Language)
            Get
                Return _languages
            End Get
            Set(ByVal value As ObservableCollection(Of Language))
                _languages = value
            End Set
        End Property

        Private _Tags As ObservableCollection(Of Tag)
        Public Property Tags() As ObservableCollection(Of Tag)
            Get
                Return _Tags
            End Get
            Set(ByVal value As ObservableCollection(Of Tag))
                _Tags = value
            End Set
        End Property


        Private _OCTables As ObservableCollection(Of String)
        Public Property OCTables() As ObservableCollection(Of String)
            Get
                Return _OCTables
            End Get
            Set(ByVal value As ObservableCollection(Of String))
                _OCTables = value
            End Set
        End Property

        Private _Selected As String
        Public Property Selected() As String
            Get
                Return _Selected
            End Get
            Set(ByVal value As String)
                _Selected = value
                UpdateView()
            End Set
        End Property

        Private _SelectedTable As ListCollectionView
        Public Property SelectedTable() As ListCollectionView
            Get
                Return _SelectedTable
            End Get
            Set(ByVal value As ListCollectionView)
                _SelectedTable = value
                RaisePropertyChanged("SelectedTable")
            End Set
        End Property

        Private _LocationBookinggroups As ObservableCollection(Of LocationBookinggroup)
        Public Property LocationBookinggroups() As ObservableCollection(Of LocationBookinggroup)
            Get
                Return _LocationBookinggroups
            End Get
            Set(ByVal value As ObservableCollection(Of LocationBookinggroup))
                _LocationBookinggroups = value
            End Set
        End Property

        Private _AccountingTemplates As ObservableCollection(Of AccountingTemplate)
        Public Property AccountingTemplates() As ObservableCollection(Of AccountingTemplate)
            Get
                Return _AccountingTemplates
            End Get
            Set(ByVal value As ObservableCollection(Of AccountingTemplate))
                _AccountingTemplates = value
            End Set
        End Property



        Property SaveCommand As ICommand


        Public Sub New(ByVal context As in1Entities)

            OCTables = New ObservableCollection(Of String)
            OCTables.Add("CourseAudiences")
            OCTables.Add("CourseCosts")
            OCTables.Add("CourseGenders")
            OCTables.Add("CourseRegistrations")
            OCTables.Add("SportCategories")
            OCTables.Add("CustomerInstitutions")
            OCTables.Add("Languages")
            OCTables.Add("Tags")
            OCTables.Add("LocationBookinggroups")
            OCTables.Add("AccountingTemplates")

            _context = context

            SaveCommand = New RelayCommand(AddressOf SaveAction)

        End Sub


        Public Sub UpdateView()

            Select Case Selected
                Case "CourseAudiences"
                    _context.CourseAudiences.Load()
                    SelectedTable = CollectionViewSource.GetDefaultView(_context.CourseAudiences.Local)

                Case "CourseCosts"
                    _context.CourseCosts.Load()
                    SelectedTable = CollectionViewSource.GetDefaultView(_context.CourseCosts.Local)

                Case "CourseGenders"
                    _context.CourseGenders.Load()
                    SelectedTable = CollectionViewSource.GetDefaultView(_context.CourseGenders.Local)

                Case "CourseRegistrations"
                    _context.CourseRegistrations.Load()
                    SelectedTable = CollectionViewSource.GetDefaultView(_context.CourseRegistrations.Local)

                Case "SportCategories"
                    _context.SportCategories.Load()
                    SelectedTable = CollectionViewSource.GetDefaultView(_context.CustomerInstitutions.Local)

                Case "CustomerInstitutions"
                    _context.CustomerInstitutions.Load()
                    SelectedTable = CollectionViewSource.GetDefaultView(_context.CustomerInstitutions.Local)

                Case "Languages"
                    _context.Languages.Load()
                    SelectedTable = CollectionViewSource.GetDefaultView(_context.Languages.Local)

                Case "Tags"
                    _context.Tags.Load()
                    SelectedTable = CollectionViewSource.GetDefaultView(_context.Tags.Local)

                Case "LocationBookinggroups"
                    _context.LocationBookinggroups.Load()
                    SelectedTable = CollectionViewSource.GetDefaultView(_context.LocationBookinggroups.Local)

                Case "AccountingTemplates"
                    _context.AccountingTemplates.Load()
                    SelectedTable = CollectionViewSource.GetDefaultView(_context.AccountingTemplates.Local)

            End Select



        End Sub

        Public Sub SaveAction()

            _context.SaveChanges()
        End Sub

    End Class
End Namespace