﻿Imports System.Windows.Media

Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.Windows.Input
Imports Enumerable = System.Linq.Enumerable


Namespace ViewModel

    Public Class TemplateViewModel
        Inherits Infrastructure.ViewModelBase

        Private _context As in1Entities
        Private _templates As ObservableCollection(Of Template)
        Public Property Templates As ObservableCollection(Of Template)
            Get
                Return _templates
            End Get
            Set(ByVal value As ObservableCollection(Of Template))
                _templates = value
            End Set
        End Property

        Private _selectedtemplate As Template
        Public Property SelectedTemplate As Template
            Get
                Return _selectedtemplate
            End Get
            Set(ByVal value As Template)
                _selectedtemplate = value
                RaisePropertyChanged("SelectedTemplate")
            End Set
        End Property

        Private _templatetypelookup As ObservableCollection(Of TemplateType)
        Public Property TemplatetypeLookup As ObservableCollection(Of TemplateType)
            Get
                Return _templatetypelookup
            End Get
            Set(ByVal value As ObservableCollection(Of TemplateType))
                _templatetypelookup = value
            End Set
        End Property

        Public Property NewCommand As ICommand
        Public Property DelCommand As ICommand
        Public Property CancelCommand As ICommand



        Public Sub New(ByVal context As in1Entities)

            _context = context
            Init()

        End Sub


        Private Sub Init()
            _context.Templates.Load()
            _context.TemplateTypes.Load()
            Templates = _context.Templates.Local
            TemplatetypeLookup = _context.TemplateTypes.Local
            NewCommand = New RelayCommand(AddressOf NewTemplate)
            DelCommand = New RelayCommand(AddressOf DelTemplate, AddressOf CanDelTemplate)
        End Sub


        Private Sub NewTemplate()
            Dim templ As New Template
            templ.TemplateName = "Neuer Ort"

            _context.Templates.Add(templ)

            SelectedTemplate = templ

        End Sub

        Private Sub DelTemplate()
            If Not IsNothing(SelectedTemplate) Then

                Dim dlg As New DialogService
                Dim any = From c In _context.Courses Where c.TemplateID = SelectedTemplate.TemplateID Select c
                If any.Any() Then

                    dlg.ShowAlert("Löschen", "Sie können das Template nicht löschen. Es ist Kursen zugeordnet")
                Else

                    If dlg.AskConfirmation("Löschen", "Wirklich dieses Template löschen?") = True Then

                        _context.Templates.Remove(SelectedTemplate)
                        SelectedTemplate = Nothing
                    End If
                End If

            End If
        End Sub

        Private Function CanDelTemplate() As Boolean
            If Not IsNothing(SelectedTemplate) Then
                Return True
            End If
            Return False
        End Function


    End Class
End Namespace
