﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.Windows.Input



Imports System.ComponentModel.Composition
Imports System.Windows
Imports System.Windows.Data
Imports System.Linq
Imports CommonServiceLocator

Namespace ViewModel
    <Export(GetType(SportsViewModel)), PartCreationPolicy(CreationPolicy.Shared)>
    Public Class SportsViewModel
        Inherits HSPinOne.Infrastructure.ViewModelBase

        Private _context As in1Entities


#Region "   Properties"

        Private _sports As ObservableCollection(Of Sport)
        Public Property Sports As ObservableCollection(Of Sport)
            Get
                Return _sports
            End Get
            Set(ByVal value As ObservableCollection(Of Sport))
                _sports = value
            End Set
        End Property

        Private _selectedsport As Sport
        Public Property SelectedSport As Sport
            Get
                Return _selectedsport
            End Get
            Set(ByVal value As Sport)
                _selectedsport = value
                RaisePropertyChanged("SelectedSport")
            End Set
        End Property

        Private _CurrentPosition As Integer
        Public Property CurrentPosition As Integer
            Get
                Return _CurrentPosition
            End Get
            Set(value As Integer)
                _CurrentPosition = value
                RaisePropertyChanged("CurrentPosition")
            End Set
        End Property


        Private _SportView As ListCollectionView
        Public Property SportsView() As ListCollectionView
            Get
                Return _SportView
            End Get
            Set(ByVal value As ListCollectionView)
                _SportView = value
            End Set
        End Property

        Public Property SportCategories As ObservableCollection(Of SportCategory)


        Public Property NewCommand As ICommand
        Public Property DelCommand As ICommand
        Public Property SearchCommand As ICommand
        Public Property GetObmannCommand As ICommand
        Public Property DelAnsprechpartnerCommand As ICommand
        Public Property DelObmannCommand As ICommand
        Public Property InsertImageCommand As ICommand
        Public Property DeleteAliasCommand As ICommand

        Public Property SaveCommand As ICommand




#End Region

        Public Sub New(ByVal context As in1Entities)
            _context = context




            Init()



        End Sub

        Private Sub Init()

            _context.SportCategories.Load()
            SportCategories = _context.SportCategories.Local

            _context.Sports.OrderBy(Function(o) o.Sportname).Load()
            Sports = _context.Sports.Local

            SportsView = CType(CollectionViewSource.GetDefaultView(Sports.ToList()), ListCollectionView)

            NewCommand = New RelayCommand(AddressOf NewSport)
            DelCommand = New RelayCommand(AddressOf DelSport, AddressOf CanDelSport)
            SearchCommand = New RelayCommand(AddressOf GetAnsprechpartner, AddressOf CanGetAnsprechpartner)
            GetObmannCommand = New RelayCommand(AddressOf GetObmann, AddressOf CanGetAnsprechpartner)
            DelObmannCommand = New RelayCommand(AddressOf DelObmann)
            DelAnsprechpartnerCommand = New RelayCommand(AddressOf DelAnsprechpartner)
            SaveCommand = New RelayCommand(AddressOf SaveSports)
            InsertImageCommand = New RelayCommand(AddressOf AddImageintoText)
            DeleteAliasCommand = New RelayCommand(AddressOf DeleteAliasAction)


            'Events.EVAG.GetEvent(Of SaveSettingEvent).Subscribe(AddressOf SaveSports)
        End Sub


        Private Sub NewSport()

            Dim neu As New Sport
            neu.Sportname = "Neue Sportart"

            Sports.Add(neu)
            neu.SportCategoryID = SportCategories.FirstOrDefault().SportCategoryID
            SelectedSport = neu


        End Sub

        Private Sub DelSport()
            If Not IsNothing(SelectedSport) Then
                Dim dlg As New DialogService
                If SelectedSport.Courses.Count > 0 Then
                    MessageBox.Show("Die Sportart kann nicht gelöscht werden. Es bestehen noch Verknüpfungen zu einzelnen Veranstaltungen.")
                    'If dlg.AskCancelConfirmation("Sportart löschen", "Die Sportart kann nicht gelöscht werden. Es bestehen noch Verknüpfungen zu einzelnen Veranstaltungen.") = True Then

                    'End If
                Else
                    ShowConfirm()

                    'If dlg.AskConfirmation("Löschen", "Wollen Sie die Sportart löschen?") = True Then

                    'End If
                End If
            End If
        End Sub
        Public Sub ShowConfirm()

            Dim dlg As New DialogService
            If dlg.AskConfirmation("Löschen", "Wollen Sie die Sportart löschen?") = True Then
                Sports.Remove(SelectedSport)
                SelectedSport = Nothing
            End If
        End Sub


        Private Function CanDelSport() As Boolean
            If Not IsNothing(SelectedSport) Then Return True
            Return False
        End Function

        Private Sub SaveSports()
            Try
                _context.SaveChanges()
                HSPinOne.DAL.Listitems.ResetLists()
            Catch ex As Exception
                Dim dlg As New DialogService
                dlg.ShowAlert("Fehler", ex.Message)
            End Try

        End Sub

        Private Function Sportsfilter(ByVal itm As Object) As Boolean
            Dim sport As Sport = CType(itm, Sport)
            If Not IsNothing(sport) AndAlso Not IsNothing(SelectedSport) Then
                If sport.SportID <> SelectedSport.SportID Then Return True
            End If
            Return False
        End Function

        Private Sub DeleteAliasAction()
            SelectedSport.Aliasfor = Nothing
        End Sub

        Private Sub GetAnsprechpartner()
            Dim search = ServiceLocator.Current.GetInstance(Of ISearchCustomer)()

            Dim cust = search.GetCustomer()
            If Not IsNothing(cust) Then
                SelectedSport.Ansprechpartner_CustomerID = cust.CustomerID
                _context.Customers.Find(cust.CustomerID)


            End If
        End Sub

        Private Sub GetObmann()
            Dim search = ServiceLocator.Current.GetInstance(Of ISearchCustomer)()

            Dim cust = search.GetCustomer()
            If Not IsNothing(cust) Then
                SelectedSport.Obmann_CustomerID = cust.CustomerID
                _context.Customers.Find(cust.CustomerID)


            End If
        End Sub

        Private Sub DelAnsprechpartner()
            'SelectedSport.ObmannID = Nothing
            SelectedSport.Ansprechpartner_Customer = Nothing
        End Sub

        Private Sub DelObmann()
            SelectedSport.Obmann_Customer = Nothing
        End Sub

        Private Function CanGetAnsprechpartner() As Boolean
            If Not IsNothing(SelectedSport) Then Return True
            Return False
        End Function

        Private Sub AddImageintoText()


            Dim res = HSPinOne.Common.DocManService.GetImage
            If Not IsNothing(res) And Not Trim(res) = "" And Not IsNothing(SelectedSport) Then
                If IsNothing(SelectedSport.Beschreibung) Then SelectedSport.Beschreibung = ""
                Dim newText = SelectedSport.Beschreibung.Substring(0, CurrentPosition) & " " & res.ToString & " " & SelectedSport.Beschreibung.Substring(CurrentPosition, SelectedSport.Beschreibung.Length - CurrentPosition)
                SelectedSport.Beschreibung = newText
            End If



        End Sub

    End Class

End Namespace
