﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.Windows.Input


Namespace ViewModel

    Public Class EquipmentViewModel
        Inherits ViewModelBase

        Private _context As in1Entities
        Private _equipment As ObservableCollection(Of Equipment)
        Public Property Equipments As ObservableCollection(Of Equipment)
            Get
                Return _equipment
            End Get
            Set(ByVal value As ObservableCollection(Of Equipment))
                _equipment = value
            End Set
        End Property

        Private _selectedequipment As Equipment
        Public Property SelectedEquipment As Equipment
            Get
                Return _selectedequipment
            End Get
            Set(ByVal value As Equipment)
                _selectedequipment = value
                RaisePropertyChanged("SelectedEquipment")
            End Set
        End Property

        Private _categorylookup As ObservableCollection(Of EquipmentCategory)
        Public Property CategoryLookup As ObservableCollection(Of EquipmentCategory)
            Get
                Return _categorylookup
            End Get
            Set(ByVal value As ObservableCollection(Of EquipmentCategory))
                _categorylookup = value
            End Set
        End Property

        Public Property NewCommand As ICommand
        Public Property DelCommand As ICommand
        Public Property CancelCommand As ICommand



        Public Sub New(ByVal context As in1Entities)

            _context = context
            Init()

        End Sub


        Private Sub Init()
            _context.Equipments.Load()
            Equipments = _context.Equipments.Local

            _context.EquipmentCategories.Load()
            CategoryLookup = _context.EquipmentCategories.Local

            NewCommand = New RelayCommand(AddressOf NewEquipment)
            DelCommand = New RelayCommand(AddressOf DelEquipment)
        End Sub


        Private Sub NewEquipment()
            Dim loc As New Equipment
            loc.EquipmentName = "Neue Equipment"

            _context.Equipments.Add(loc)

            SelectedEquipment = loc

        End Sub

        Private Sub DelEquipment()
            If Not IsNothing(SelectedEquipment) Then
                Dim dlg As New DialogService
                If dlg.AskConfirmation("Löschen", "Wirklich diese Equipment löschen? Löschen ist nur möglich, wenn dieser Kategorie keine Veranstaltungen oder Termine mehr zugeordnet sind") = True Then
                    _context.Equipments.Remove(SelectedEquipment)
                End If
            End If
        End Sub

    End Class
End Namespace