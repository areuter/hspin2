﻿Imports HSPinOne.Infrastructure
Imports HSPinOne.Common
Imports System.Windows.Input


Namespace ViewModel


    Public Class LocalSettingsViewModel
        Inherits ViewModelBase


        Public Property BondruckerAktiv As Boolean
        Public Property Bondruckeranschluss As String

        Public Property SaveLocalSettingsCommand As ICommand
        Private _cls As Bondruck



        Public Sub New()
            _cls = New Bondruck
   
            SaveLocalSettingsCommand = New RelayCommand(AddressOf SaveLocalSettings)
        End Sub

        Private Sub SaveLocalSettings()

        End Sub

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
            _cls = Nothing
        End Sub
    End Class
End Namespace