﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.Windows.Input

Namespace ViewModel


    Public Class PricegroupViewModel
        Inherits ViewModelBase


        Private _context As New in1Entities


        Private _cvsPriceGroups As ObservableCollection(Of PriceGroup)
        Public Property cvsPriceGroups() As ObservableCollection(Of PriceGroup)
            Get
                Return _cvsPriceGroups
            End Get
            Set(ByVal value As ObservableCollection(Of PriceGroup))
                _cvsPriceGroups = value
            End Set
        End Property

        Private _SelectedPriceGroup As PriceGroup
        Public Property SelectedPriceGroup() As PriceGroup
            Get
                Return _SelectedPriceGroup
            End Get
            Set(ByVal value As PriceGroup)
                _SelectedPriceGroup = value
                RaisePropertyChanged("SelectedPriceGroup")
            End Set
        End Property

        Private _SelectedRow As PriceGroupPrice
        Public Property SelectedRow() As PriceGroupPrice
            Get
                Return _SelectedRow
            End Get
            Set(ByVal value As PriceGroupPrice)
                _SelectedRow = value
                RaisePropertyChanged("SelectedRow")
            End Set
        End Property



        Private _cvsCustomerStates As ObservableCollection(Of CustomerState)
        Public Property cvsCustomerStates() As ObservableCollection(Of CustomerState)
            Get
                Return _cvsCustomerStates
            End Get
            Set(ByVal value As ObservableCollection(Of CustomerState))
                _cvsCustomerStates = value
            End Set
        End Property




        Public Property NewPriceGroupCommand As ICommand
        Public Property SaveCommand As ICommand
        Public Property RemoveCommand As ICommand

        Public Property AddRowCommand As ICommand


        Public Sub New()

            NewPriceGroupCommand = New RelayCommand(AddressOf NewPriceGroupAction)
            SaveCommand = New RelayCommand(AddressOf SaveAction)
            RemoveCommand = New RelayCommand(AddressOf RemoveAction)
            AddRowCommand = New RelayCommand(AddressOf AddRowAction)

            _context.PriceGroups.Load()
            cvsPriceGroups = _context.PriceGroups.Local

            _context.CustomerStates.Load()
            cvsCustomerStates = _context.CustomerStates.Local

            SelectedPriceGroup = cvsPriceGroups.FirstOrDefault()

        End Sub

        Private Sub NewPriceGroupAction()
            SelectedPriceGroup = New PriceGroup
            SelectedPriceGroup.PriceGroupname = "Neue Preisgruppe"
            _context.PriceGroups.Add(SelectedPriceGroup)
        End Sub

        Private Sub RemoveAction()
            _context.PriceGroupPrices.Remove(SelectedRow)
        End Sub

        Private Sub AddRowAction()
            Dim pgp = New PriceGroupPrice
            SelectedPriceGroup.PriceGroupPrices.Add(pgp)
        End Sub

        Private Sub SaveAction()
            _context.SaveChanges()
        End Sub

    End Class

End Namespace