﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.Windows.Input

Namespace ViewModel




    Public Class PackageViewModel
        Inherits ViewModelBase



        Private _context As in1Entities


        Private _Packages As ObservableCollection(Of Package)
        Public Property Packages As ObservableCollection(Of Package)
            Get
                Return _Packages
            End Get
            Set(value As ObservableCollection(Of Package))
                _Packages = value
            End Set
        End Property

        Private _SelectedPackage As Package
        Public Property SelectedPackage As Package
            Get
                Return _SelectedPackage
            End Get
            Set(value As Package)
                _SelectedPackage = value
                RaisePropertyChanged("SelectedPackage")
            End Set
        End Property

        Private _CostcenterLookup As IEnumerable(Of Costcenter)
        Public Property CostcenterLookup As IEnumerable(Of Costcenter)
            Get
                Return _CostcenterLookup
            End Get
            Set(value As IEnumerable(Of Costcenter))
                _CostcenterLookup = value
            End Set
        End Property

        Private _ProductTypeLookup As IEnumerable(Of ProductType)
        Public Property ProductTypeLookup As IEnumerable(Of ProductType)
            Get
                Return _ProductTypeLookup
            End Get
            Set(value As IEnumerable(Of ProductType))
                _ProductTypeLookup = value
            End Set
        End Property

        Private _CategoryLookup As IEnumerable(Of Category)
        Public Property CategoryLookup As IEnumerable(Of Category)
            Get
                Return _CategoryLookup
            End Get
            Set(value As IEnumerable(Of Category))
                _CategoryLookup = value
            End Set
        End Property

        Private _TemplateLookup As IEnumerable(Of Template)
        Public Property TemplateLookup() As IEnumerable(Of Template)
            Get
                Return _TemplateLookup
            End Get
            Set(ByVal value As IEnumerable(Of Template))
                _TemplateLookup = value
            End Set
        End Property



        Private cs As ObservableCollection(Of CustomerState)

        Public Property AddNewCommand As ICommand




        Public Sub New(ByVal context As in1Entities)

            _context = context
            _context.CustomerStates.Load()
            cs = _context.CustomerStates.Local

            _context.Packages.Load()
            Packages = _context.Packages.Local

            CostcenterLookup = (From c In _context.Costcenters Select c).ToList

            ProductTypeLookup = (From c In _context.ProductTypes Select c).ToList

            CategoryLookup = (From c In _context.Categories Select c).ToList

            TemplateLookup = (From c In _context.Templates Select c).ToList


            AddNewCommand = New RelayCommand(AddressOf AddNew)


        End Sub

        Private Sub AddNew()
            Dim pk As New Package
            pk.PackageName = "Neuer Name"
            pk.Category = CategoryLookup.First
            pk.Costcenter = CostcenterLookup.First
            pk.ProductType = ProductTypeLookup.First

            For Each itm In cs
                Dim price As New PackagePrice
                price.CustomerStateID = itm.CustomerStateID
                price.Preis = 0
                pk.PackagePrices.Add(price)
            Next
            Packages.Add(pk)
            SelectedPackage = pk
        End Sub

    End Class

End Namespace
