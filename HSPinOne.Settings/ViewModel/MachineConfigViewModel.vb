﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.Windows.Input


Namespace ViewModel

    Public Class MachineConfigViewModel
        Inherits ViewModelBase

        Private _OCMachineConfig As ObservableCollection(Of MachineConfig)
        Public Property OCMachineConfig As ObservableCollection(Of MachineConfig)
            Get
                Return _OCMachineConfig
            End Get
            Set(value As ObservableCollection(Of MachineConfig))
                _OCMachineConfig = value
            End Set
        End Property

        Private _Selected As MachineConfig
        Public Property Selected As MachineConfig
            Get
                Return _Selected
            End Get
            Set(value As MachineConfig)
                _Selected = value
                RaisePropertyChanged("Selected")
            End Set
        End Property

        Private _context As in1Entities
        Private _fp As String

        Public Property AddConfigCommand As ICommand
        Public Property DelConfigCommand As ICommand
        Public Property SaveConfigCommand As ICommand




        Public Sub New()

            _fp = MyGlobals.Fingerprint
            _context = New in1Entities

            _context.MachineConfigs.Where(Function(o) o.Machinename = _fp).Load()
            OCMachineConfig = _context.MachineConfigs.Local


            AddConfigCommand = New RelayCommand(AddressOf Add)
            DelConfigCommand = New RelayCommand(AddressOf Del, AddressOf CanDel)
            SaveConfigCommand = New RelayCommand(AddressOf Save)


        End Sub

        Private Sub Save()
            _context.SaveChanges()
        End Sub

        Private Sub Add()
            Dim item As New MachineConfig
            item.Machinename = _fp
            _context.MachineConfigs.Add(item)
        End Sub

        Private Sub Del()
            If Not IsNothing(Selected) Then
                _context.MachineConfigs.Remove(Selected)
            End If
        End Sub

        Private Function CanDel() As Boolean
            If Not IsNothing(Selected) Then Return True
            Return False
        End Function
    End Class

End Namespace