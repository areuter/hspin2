﻿Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Windows.Input
Imports HSPinOne.DAL
Imports System.ComponentModel

Imports Prism.Regions
Imports System.ComponentModel.Composition
Imports System.Reflection
Imports HSPinOne.UIControls
Imports CommonServiceLocator

Namespace ViewModel


    <Export(GetType(MainSettingsViewModel)), PartCreationPolicy(CreationPolicy.NonShared)>
    Public Class MainSettingsViewModel
        Inherits ViewModelBase



        Public Property CancelCommand As ICommand
        Public Property SaveCommand As ICommand
        Public Event RequestClose As EventHandler
        Private _regionmanager As IRegionManager
        Private _servicelocator As IServiceLocator
        Private wc As WaitCursor

        Private _settingitems As ObservableCollection(Of SettingItemViewModel)
        Public Property SettingItems As ObservableCollection(Of SettingItemViewModel)
            Get
                Return _settingitems
            End Get
            Set(ByVal value As ObservableCollection(Of SettingItemViewModel))
                _settingitems = value
            End Set
        End Property

        Public Property RegionManager As IRegionManager
            Get
                Return _regionmanager
            End Get
            Set(value As IRegionManager)
                _regionmanager = value
            End Set
        End Property



        Private _context As in1Entities
        Public Property NavigateCommand As ICommand

        Public Sub New(ByVal regionmanager As IRegionManager, ByVal servicelocator As IServiceLocator)

            CancelCommand = New RelayCommand(AddressOf CancelAction)
            SaveCommand = New RelayCommand(AddressOf SaveAction)
            NavigateCommand = New RelayCommand(AddressOf Navigate)

            '_regionmanager = rm 'ServiceLocator.Current.GetInstance(Of IRegionManager)()

            _regionmanager = regionmanager
            _servicelocator = servicelocator

            _context = New in1Entities

            ' RegisterItems()

        End Sub

        Private Sub Navigate(ByVal param As Object)
            wc = New WaitCursor
            Dim type As Type = Me.GetType()
            Dim method As MethodInfo = type.GetMethod(param.MenuItemCommand.ToString)
            If method IsNot Nothing Then
                method.Invoke(Me, Nothing)
            End If




            'Dim view As New GlobalSettingView
            'Dim vm As New ViewModel.GlobalSettingsViewModel(_context)
            'view.DataContext = vm
            '_regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            '_regionmanager.Regions(RegionNames.SettingRegion).Activate(view)

            'Dim view As Object = Activator.CreateInstance(Type.GetType("GlobalSettingView"))


            'Dim view As New GlobalSettingsView
            'Dim vm As New ViewModel.GlobalSettingsViewModel(_context)
            'view.DataContext = vm
            '_regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            '_regionmanager.Regions(RegionNames.SettingRegion).Activate(view)


            'If Not IsNothing(param.MenuItemView) Then
            '    '_regionmanager.Regions(RegionNames.RibbonRegion).RequestNavigate(New Uri("/VerwaltungRibbonTab", UriKind.Relative))
            'Else
            '    '_regionmanager.Regions(RegionNames.RibbonRegion).RequestNavigate(New Uri(param.RibbonTab.ToString, UriKind.Relative))
            'End If
            'If Not IsNothing(param.MenuItemView) Then
            '    _regionmanager.RequestNavigate(RegionNames.SettingRegion, New Uri(param.MenuItemView.ToString, UriKind.Relative))
            'End If
            If Not IsNothing(wc) Then wc.Dispose()


        End Sub


        Public Sub NavigateGlobalSettings()
            Dim view As New GlobalSettingsView
            Dim vm As New ViewModel.GlobalSettingsViewModel(_context)
            view.DataContext = vm
            _regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            _regionmanager.Regions(RegionNames.SettingRegion).Activate(view)
        End Sub

        Public Sub NavigateCourseSettings()
            Dim view As New CourseSettingsView
            Dim vm As New ViewModel.CourseSettingsViewModel(_context)
            view.DataContext = vm
            _regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            _regionmanager.Regions(RegionNames.SettingRegion).Activate(view)
        End Sub

        Public Sub NavigateSports()
            Dim view As New SportsView
            Dim vm As New ViewModel.SportsViewModel(_context)
            view.DataContext = vm
            _regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            _regionmanager.Regions(RegionNames.SettingRegion).Activate(view)
        End Sub

        Public Sub NavigateLocation()
            Dim view As New LocationView
            Dim vm As New ViewModel.LocationViewModel(_context)
            view.DataContext = vm
            _regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            _regionmanager.Regions(RegionNames.SettingRegion).Activate(view)
        End Sub

        Public Sub NavigateAccount()
            Dim view As New AccountView
            Dim vm As New ViewModel.AccountViewModel(_context)
            view.DataContext = vm
            _regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            _regionmanager.Regions(RegionNames.SettingRegion).Activate(view)
        End Sub

        Public Sub NavigateSemesterSetting()
            Dim view As New SemesterSettingView
            Dim vm As New ViewModel.SemesterSettingViewModel(_context)
            view.DataContext = vm
            _regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            _regionmanager.Regions(RegionNames.SettingRegion).Activate(view)
        End Sub
        Public Sub NavigateContractSetting()
            Dim view As New ContractView
            _regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            _regionmanager.Regions(RegionNames.SettingRegion).Activate(view)
        End Sub
        Public Sub NavigateHoliday()
            Dim view As New HolidayView
            Dim vm As New ViewModel.HolidayViewModel(_context)
            view.DataContext = vm
            _regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            _regionmanager.Regions(RegionNames.SettingRegion).Activate(view)
        End Sub

        Public Sub NavigateTemplate()
            Dim view As New TemplateView
            Dim vm As New ViewModel.TemplateViewModel(_context)
            view.DataContext = vm
            _regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            _regionmanager.Regions(RegionNames.SettingRegion).Activate(view)
        End Sub


        Public Sub NavigateCategory()
            Dim view As New CategoryView
            Dim vm As New ViewModel.CategoryViewModel(_context)
            view.DataContext = vm
            _regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            _regionmanager.Regions(RegionNames.SettingRegion).Activate(view)
        End Sub


        Public Sub NavigateSerial()
            Dim view As New SerialView
            Dim vm As New ViewModel.SerialViewModel(_context)
            view.DataContext = vm
            _regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            _regionmanager.Regions(RegionNames.SettingRegion).Activate(view)
        End Sub


        Public Sub NavigateUser()
            Dim view As New UserView
            Dim vm As New ViewModel.UserViewModel(_context)
            view.DataContext = vm
            _regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            _regionmanager.Regions(RegionNames.SettingRegion).Activate(view)
        End Sub

        Public Sub NavigateEquipment()
            Dim view As New EquipmentView
            Dim vm As New ViewModel.EquipmentViewModel(_context)
            view.DataContext = vm
            _regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            _regionmanager.Regions(RegionNames.SettingRegion).Activate(view)
        End Sub

        Public Sub NavigatePriceGroupConfig()
            Dim view As New PricegroupView
            _regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            _regionmanager.Regions(RegionNames.SettingRegion).Activate(view)
        End Sub

        Public Sub NavigateLocationGroup()
            Dim view As New LocationGroupView
            Dim vm As New ViewModel.LocationgroupViewModel()
            view.DataContext = vm
            _regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            _regionmanager.Regions(RegionNames.SettingRegion).Activate(view)
        End Sub

        Public Sub NavigateLocalSettings()
            Dim view As New LocalSettingsView
            Dim vm As New ViewModel.LocalSettingsViewModel()
            view.DataContext = vm
            _regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            _regionmanager.Regions(RegionNames.SettingRegion).Activate(view)
        End Sub

        Public Sub NavigateMachineConfig()
            Dim view As New MachineConfigView
            Dim vm As New ViewModel.MachineConfigViewModel()
            view.DataContext = vm
            _regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            _regionmanager.Regions(RegionNames.SettingRegion).Activate(view)
        End Sub

        Public Sub NavigateAdminAccess()
            Dim view As New AdminAccessView
            Dim vm As New ViewModel.AdminAccessViewModel()
            view.DataContext = vm
            _regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            _regionmanager.Regions(RegionNames.SettingRegion).Activate(view)
        End Sub

        Public Sub NavigatePackageConfig()
            Dim view As New PackageView
            Dim vm As New ViewModel.PackageViewModel(_context)
            view.DataContext = vm
            _regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            _regionmanager.Regions(RegionNames.SettingRegion).Activate(view)
        End Sub

        Public Sub NavigateSEPA()
            Dim view As New SEPAPrepareView
            Dim vm As New ViewModel.SEPAPrepareViewModel()
            view.DataContext = vm
            _regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            _regionmanager.Regions(RegionNames.SettingRegion).Activate(view)
        End Sub

        Public Sub NavigateAccounting()
            Dim view As New AccountingTemplateView
            Dim vm As New ViewModel.AccountingTemplateViewModel
            view.DataContext = vm
            _regionmanager.Regions(RegionNames.SettingRegion).Add(view)
            _regionmanager.Regions(RegionNames.SettingRegion).Activate(view)
        End Sub

        Public Sub NavigateDokmanConfig()
            wc.Dispose()
            Dim dokman = HSPinOne.Common.DocManService.GetImage

        End Sub

        Private Sub RegisterItems()



        End Sub



        Private Sub CancelAction()
            _context.Dispose()
            RaiseEvent RequestClose(Me, Nothing)
        End Sub

        Private Sub SaveAction()

            _context.SaveChanges(MyGlobals.CurrentUsername)
            ''_context.Dispose()
            HSPinOne.DAL.Listitems.ResetLists()
            RaiseEvent RequestClose(Me, Nothing)
        End Sub
    End Class

End Namespace











