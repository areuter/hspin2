﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Data.Entity

Imports System.Windows.Input
Imports CommonServiceLocator

Namespace ViewModel


    Public Class AdminAccessViewModel
        Inherits ViewModelBase

        Private _context As in1Entities

        Private _OCAA As ObservableCollection(Of AdminAccess)
        Public Property OCAA As ObservableCollection(Of AdminAccess)
            Get
                Return _OCAA
            End Get
            Set(value As ObservableCollection(Of AdminAccess))
                _OCAA = value
            End Set
        End Property

        Private _Selected As AdminAccess
        Public Property Selected As AdminAccess
            Get
                Return _Selected
            End Get
            Set(value As AdminAccess)
                _Selected = value
                RaisePropertyChanged("Selected")
            End Set
        End Property

        Public Property NewCommand As ICommand
        Public Property DelCommand As ICommand




        Public Sub New()

            _context = New in1Entities

            _context.AdminAccesss().Load()
            OCAA = _context.AdminAccesss.Local

            NewCommand = New RelayCommand(AddressOf Add)
            DelCommand = New RelayCommand(AddressOf Del)



        End Sub

        Private Sub Add()
            Dim cs = ServiceLocator.Current.GetInstance(Of ISearchCustomer)()

            Dim cust = cs.GetCustomer
            If Not IsNothing(cust) Then

                'Check ob noch nicht in Liste
                Dim count = (From c In OCAA Where c.CustomerID = cust.CustomerID).Count
                If count = 0 Then
                    Dim item As New AdminAccess
                    item.Customer = _context.Customers.Find(cust.CustomerID)
                    _context.AdminAccesss.Add(item)
                    _context.SaveChanges()
                End If
            End If

        End Sub

        Private Sub Del()
            If Not IsNothing(Selected) Then
                _context.AdminAccesss.Remove(Selected)
                _context.SaveChanges()
            End If
        End Sub


    End Class
End Namespace