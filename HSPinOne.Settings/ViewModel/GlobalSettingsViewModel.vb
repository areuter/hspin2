﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.Windows.Input
Imports System.ComponentModel.Composition
Imports Prism.Regions
Imports System.Windows.Controls
Imports System.Windows.Data

Namespace ViewModel
    <Export(GetType(GlobalSettingsViewModel)), PartCreationPolicy(CreationPolicy.Shared)> _
    Public Class GlobalSettingsViewModel
        Inherits HSPinOne.Infrastructure.ViewModelBase

        Private _context As in1Entities

        Private _globalsetting As GlobalSetting
        Public Property GlobalSetting As GlobalSetting
            Get
                Return _globalsetting
            End Get
            Set(value As GlobalSetting)
                _globalsetting = value
                RaisePropertyChanged("GlobalSetting")
            End Set
        End Property

        Private _globalsettings As ObservableCollection(Of GlobalSetting)
        Public Property GlobalSettings As ObservableCollection(Of GlobalSetting)
            Get
                Return _globalsettings
            End Get
            Set(ByVal value As ObservableCollection(Of GlobalSetting))
                _globalsettings = value
            End Set
        End Property

        Private _Controls As ObservableCollection(Of Control)
        Public Property Controls() As ObservableCollection(Of Control)
            Get
                Return _Controls
            End Get
            Set(ByVal value As ObservableCollection(Of Control))
                _Controls = value
            End Set
        End Property



        Public Sub New(ByVal context As in1Entities)

            _context = context


            _context.GlobalSettings.Load()
            GlobalSettings = _context.GlobalSettings.Local



        End Sub


        Private Sub AddItem(ByVal setting As GlobalSetting)

            Dim st As New StackPanel
            st.Orientation = Orientation.Horizontal
            Dim lb As New Label
            lb.Content = setting.Bezeichnung
            st.Children.Add(lb)

            Select Case setting.Typ
                Case "boolean"
                    Dim cb As New ComboBox
                    cb.ItemsSource = New Dictionary(Of String, String) From {{"0", "nein"}, {"1", "ja"}}
                    cb.SelectedValuePath = "Key"
                    cb.DisplayMemberPath = "Value"
                    st.Children.Add(cb)

                Case Else
                    Dim tb As New TextBox
                    tb.Text = "{Binding Wert}"
                    st.Children.Add(tb)


            End Select
        End Sub
    End Class



End Namespace