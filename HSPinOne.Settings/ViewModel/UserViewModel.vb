﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Windows.Input
Imports System.Data.Entity

Imports HSPinOne.Modules.UserLogin
Imports System.Text
Imports CommonServiceLocator

Namespace ViewModel


    Public Class UserViewModel
        Inherits ViewModelBase
        Private _context As in1Entities
        Private _institutionlookup As IEnumerable(Of Institution)
        Private _rolelookup As ObservableCollection(Of Role)


#Region "   Properties"

        Private _user As ObservableCollection(Of User)
        Public Property Users As ObservableCollection(Of User)
            Get
                Return _user
            End Get
            Set(ByVal value As ObservableCollection(Of User))
                _user = value
            End Set
        End Property

        Private _selecteduser As User
        Public Property SelectedUser As User
            Get
                Return _selecteduser
            End Get
            Set(ByVal value As User)
                _selecteduser = value
                RaisePropertyChanged("SelectedUser")
            End Set
        End Property

        Private _roles As ObservableCollection(Of Role)
        Public Property Roles As ObservableCollection(Of Role)
            Get
                Return _roles
            End Get
            Set(ByVal value As ObservableCollection(Of Role))
                _roles = value
            End Set
        End Property

        Private _pass As String
        Public Property Pass As String
            Get
                Return _pass
            End Get
            Set(ByVal value As String)
                _pass = value
                RaisePropertyChanged("Pass")
            End Set
        End Property

        Public Property InstitutionLookup As IEnumerable(Of Institution)
            Get
                Return _institutionlookup
            End Get
            Set(ByVal value As IEnumerable(Of Institution))
                _institutionlookup = value
            End Set
        End Property

        Public Property RoleLookup As ObservableCollection(Of Role)
            Get
                Return _rolelookup
            End Get
            Set(ByVal value As ObservableCollection(Of Role))
                _rolelookup = value
            End Set
        End Property

        Public Property NewCommand As ICommand
        Public Property DelCommand As ICommand
        Public Property SearchCommand As ICommand
        Public Property SaveCommand As ICommand

        Public Property AddRoleCommand As ICommand



#End Region

        Public Sub New(ByVal context As in1Entities)
            _context = context


            Init()



        End Sub

        Private Sub Init()

            _context.Users.Load()
            Users = _context.Users.Local

            _context.Institutions.Load()
            InstitutionLookup = _context.Institutions.Local

            _context.Roles.Load()
            RoleLookup = _context.Roles.Local

            '      InstitutionLookup = Users

            NewCommand = New RelayCommand(AddressOf NewUser)
            DelCommand = New RelayCommand(AddressOf DelUser, AddressOf CanDelUser)
            SearchCommand = New RelayCommand(AddressOf GetAnsprechpartner, AddressOf CanGetAnsprechpartner)
            SaveCommand = New RelayCommand(AddressOf SaveUsers)
            AddRoleCommand = New RelayCommand(AddressOf AddRole)

            'Events.EVAG.GetEvent(Of SaveSettingEvent).Subscribe(AddressOf SaveSports)
        End Sub


        Private Sub AddRole()
            'If Users > 0 Then
            '    'Debug.Print(lbRoles.SelectedValue.RoleName)
            '    'lbAssignedRoles.Items.Add(lbRoles.SelectedItem.RoleName)
            '    'lbRoles.Items.Remove(lbAssignedRoles.SelectedItem)
            'End If
        End Sub

        Private Sub NewUser()

            Dim neu As New User
            neu.UserName = "Neuer User"
            Pass = ""
            neu.NewRecord = True
            'Users.Add(neu)
            SelectedUser = neu


        End Sub

        Private Sub DelUser()
            If Not IsNothing(SelectedUser) Then
                Dim dlg As New DialogService
                If dlg.AskConfirmation("Löschen", "Wollen Sie den User löschen?") = True Then
                    Users.Remove(SelectedUser)
                    SelectedUser = Nothing
                End If
            End If
        End Sub

        Private Function CanDelUser() As Boolean
            If Not IsNothing(SelectedUser) Then Return True
            Return False
        End Function

        Private Sub SaveUsers()
            Try
                If Not IsNothing(Pass) Then

                    Dim salt As Byte()
                    salt = GenSalt()
                    SelectedUser.SaltValue = Convert.ToBase64String(salt)
                    SelectedUser.Password = GenHash(Pass, salt)
                    ' SelectedUser.UserToRoles.Add(

                    If SelectedUser.NewRecord Then
                        Users.Add(SelectedUser)
                        SelectedUser.NewRecord = False
                    End If
                End If

                _context.SaveChanges()
            Catch ex As Exception
                Dim dlg As New DialogService
                dlg.ShowAlert("Fehler", ex.Message)
            End Try

        End Sub

        Private Sub GetAnsprechpartner()
            Dim search = ServiceLocator.Current.GetInstance(Of ISearchCustomer)()

            Dim cust = search.GetCustomer
            If Not IsNothing(cust) Then
                SelectedUser.UserID = cust.CustomerID
                _context.Customers.Find(cust.CustomerID)
                _context.Customers.Load()

            End If
        End Sub

        Private Function CanGetAnsprechpartner() As Boolean
            If Not IsNothing(SelectedUser) Then Return True
            Return False
        End Function

        Public Function GenHash(ByVal LoginPassword As String, ByVal salt As Byte())
            Try

                Return Crypto.ComputeHash(LoginPassword, "SHA1", salt)

            Catch ex As Exception
                Debug.Print("Exception")
                Throw New Exception(ex.Message, ex.InnerException)
            End Try
        End Function

        Public Function GenSalt()
            Return Crypto.generateRadomSaltvalue()
        End Function


    End Class


End Namespace
