﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.Windows.Input

Namespace ViewModel


    Public Class ContractViewModel
        Inherits ViewModelBase

        Private _context As New in1Entities


        Private _ContractRates As ObservableCollection(Of ContractRate)
        Public Property ContractRates() As ObservableCollection(Of ContractRate)
            Get
                Return _ContractRates
            End Get
            Set(ByVal value As ObservableCollection(Of ContractRate))
                _ContractRates = value
            End Set
        End Property

        Private _Selected As ContractRate
        Public Property Selected() As ContractRate
            Get
                Return _Selected
            End Get
            Set(ByVal value As ContractRate)
                _Selected = value
                SelectedChanged()
                RaisePropertyChanged("Selected")
            End Set
        End Property

        Private _Templates As ObservableCollection(Of Template) = New ObservableCollection(Of Template)
        Public Property Templates() As ObservableCollection(Of Template)
            Get
                Return _Templates
            End Get
            Set(ByVal value As ObservableCollection(Of Template))
                _Templates = value
            End Set
        End Property


        Public Property SaveCommand As ICommand
        Public Property AddCommand As ICommand


        Private _cs As New ObservableCollection(Of CustomerState)
        Public Sub New()

            SaveCommand = New RelayCommand(AddressOf Save)
            AddCommand = New RelayCommand(AddressOf AddNew)

            _context.ContractRates.Load()
            _context.ContractRatePrices.Load()
            ContractRates = _context.ContractRates.Local()

            _context.CustomerStates.Load()
            _cs = _context.CustomerStates.Local()

            _context.Templates.Load()
            _Templates = _context.Templates.Local()

            Selected = ContractRates.FirstOrDefault
        End Sub

        Private Sub SelectedChanged()
            If IsNothing(Selected.ContractRatePrices) Then
                Selected.ContractRatePrices = New ObservableCollection(Of ContractRatePrice)
            End If
            If Not IsNothing(Selected.ContractRatePrices) Then
                Dim p = (From c In Selected.ContractRatePrices Select c.CustomerStateID).ToList
                For Each itm In _cs
                    If Not p.Contains(itm.CustomerStateID) Then
                        Dim price As New ContractRatePrice
                        price.CustomerStateID = itm.CustomerStateID
                        price.Preis = 0
                        price.TaxPercent = 0
                        Selected.ContractRatePrices.Add(price)
                    End If
                Next
            End If

        End Sub

        Private Sub AddNew()
            Dim c As New ContractRate
            c.ContractRateName = "Neuer Vertragstarif"
            c.Monate = 1
            c.Velaengerungsmonate = 0
            c.Ratenbetrag = 0
            c.Startgebuehr = 0
            _context.ContractRates.Add(c)
            Selected = c
        End Sub

        Private Sub Save()
            _context.SaveChanges()
        End Sub

    End Class
End Namespace