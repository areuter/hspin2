﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Windows.Data
Imports System.Data.Entity
Imports System.Windows.Input


Namespace ViewModel

    Public Class LocationgroupViewModel
        Inherits ViewModelBase

        Private _context As in1Entities

        Private _AllLocations As ListCollectionView
        Public Property AllLocations As ListCollectionView
            Get
                Return _AllLocations
            End Get
            Set(ByVal value As ListCollectionView)
                _AllLocations = value
            End Set
        End Property

        Private _AllLocationgroups As ObservableCollection(Of Locationgroup)
        Public Property AllLocationgroups As ObservableCollection(Of Locationgroup)
            Get
                Return _AllLocationgroups
            End Get
            Set(ByVal value As ObservableCollection(Of Locationgroup))
                _AllLocationgroups = value
            End Set
        End Property



        Private _Selected As ObservableCollection(Of LocationLocationgroup)
        Public Property Selected As ObservableCollection(Of LocationLocationgroup)
            Get
                Return _Selected
            End Get
            Set(ByVal value As ObservableCollection(Of LocationLocationgroup))
                _Selected = value
            End Set
        End Property

        Private _SelectedLocationgroup As Locationgroup
        Public Property SelectedLocationgroup As Locationgroup
            Get
                Return _SelectedLocationgroup
            End Get
            Set(ByVal value As Locationgroup)
                _SelectedLocationgroup = value
                RaisePropertyChanged("SelectedLocationgroup")
                FilterRefresh()
            End Set
        End Property

        Private _SelectedLocation As Location
        Public Property SelectedLocation As Location
            Get
                Return _SelectedLocation
            End Get
            Set(ByVal value As Location)
                _SelectedLocation = value
            End Set
        End Property

        Private _SelectedLocation1 As LocationLocationgroup
        Public Property SelectedLocation1 As LocationLocationgroup
            Get
                Return _SelectedLocation1
            End Get
            Set(ByVal value As LocationLocationgroup)
                _SelectedLocation1 = value
            End Set
        End Property

        Public Property AddCommand As ICommand
        Public Property DelCommand As ICommand
        Public Property SaveCommand As ICommand
        Public Property AddResCommand As ICommand
        Public Property DelResCommand As ICommand



        Public Sub New()

            AddCommand = New RelayCommand(AddressOf Add, AddressOf CanAdd)
            DelCommand = New RelayCommand(AddressOf Del, AddressOf CanDel)
            AddResCommand = New RelayCommand(AddressOf AddRes, AddressOf CanAddRes)
            DelResCommand = New RelayCommand(AddressOf DelRes, AddressOf CanDelRes)
            SaveCommand = New RelayCommand(AddressOf Save, AddressOf CanSave)

            _context = New in1Entities

            'Laden aller Locations
            Dim locs = From c In _context.Locations Select c
            Me.AllLocations = CType(CollectionViewSource.GetDefaultView(locs.ToList), ListCollectionView)


            'Laden der Locationgroups
            _context.Locationgroups.Load()
            Me.AllLocationgroups = _context.Locationgroups.Local
            If Me.AllLocationgroups.Any Then
                Me.SelectedLocationgroup = Me.AllLocationgroups.FirstOrDefault
            End If

            Me.AllLocations.Filter = New Predicate(Of Object)(AddressOf Locationfilter)


        End Sub

        Private Function Locationfilter(ByVal item As Object) As Boolean
            Dim itm = TryCast(item, Location)
            If itm IsNot Nothing Then
                If Not IsNothing(SelectedLocationgroup) Then
                    Dim res = (From c In SelectedLocationgroup.LocationLocationgroups Where c.LocationID = itm.LocationID).Count
                    If res = 0 Then Return True
                End If
            End If
            Return False

        End Function


        Private Sub FilterRefresh()
            Me.AllLocations.Refresh()
        End Sub

        Private Sub Add()
            If Not IsNothing(SelectedLocation) Then
                Dim itm As New LocationLocationgroup
                itm.Location = SelectedLocation
                SelectedLocationgroup.LocationLocationgroups.Add(itm)
                FilterRefresh()

            End If
        End Sub

        Private Function CanAdd() As Boolean
            If Not IsNothing(SelectedLocationgroup) And Not IsNothing(SelectedLocation) Then
                Return True
            End If
            Return False
        End Function

        Private Sub Del()
            If Not IsNothing(SelectedLocation1) Then
                _context.LocationLocationgroups.Remove(_SelectedLocation1)
                FilterRefresh()
            End If
        End Sub

        Private Function CanDel() As Boolean
            If Not IsNothing(_SelectedLocation1) Then
                Return True
            End If
            Return False
        End Function

        Private Sub AddRes()
            Dim res As New Locationgroup
            res.Locationgroupname = "(neu)"
            _context.Locationgroups.Add(res)
            SelectedLocationgroup = res
        End Sub

        Private Function CanAddRes() As Boolean

            Return True
        End Function

        Private Sub DelRes()
            _context.Locationgroups.Remove(SelectedLocationgroup)
        End Sub

        Private Function CanDelRes() As Boolean
            If Not IsNothing(SelectedLocationgroup) Then
                Return True
            End If
            Return False
        End Function


        Private Sub Save()
            _context.SaveChanges()
        End Sub

        Private Function CanSave() As Boolean
            If _context.HasChanges Then
                Return True
            End If
            Return False
        End Function


    End Class

End Namespace