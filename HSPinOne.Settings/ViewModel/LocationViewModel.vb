﻿
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.Windows.Input
Imports System.Windows.Data


Namespace ViewModel


    Public Class LocationViewModel
        Inherits Infrastructure.ViewModelBase


        Private _context As in1Entities

        Private _locations As ObservableCollection(Of Location)
        Public Property Locations As ObservableCollection(Of Location)
            Get
                Return _locations
            End Get
            Set(ByVal value As ObservableCollection(Of Location))
                _locations = value
            End Set
        End Property

        Private _selectedlocation As Location
        Public Property SelectedLocation As Location
            Get
                Return _selectedlocation
            End Get
            Set(ByVal value As Location)
                _selectedlocation = value
                RaisePropertyChanged("SelectedLocation")
                FilterRefresh()
            End Set
        End Property

        Private _campulookup As ObservableCollection(Of Campus)
        Public Property CampusLookup As ObservableCollection(Of Campus)
            Get
                Return _campulookup
            End Get
            Set(ByVal value As ObservableCollection(Of Campus))
                _campulookup = value
            End Set
        End Property

        Private _allequipment As ListCollectionView
        Public Property AllEquipment As ListCollectionView
            Get
                Return _allequipment
            End Get
            Set(ByVal value As ListCollectionView)
                _allequipment = value
            End Set
        End Property

        Private _SelectedEquipment As Equipment
        Public Property SelectedEquipment As Equipment
            Get
                Return _SelectedEquipment
            End Get
            Set(ByVal value As Equipment)
                _SelectedEquipment = value
            End Set
        End Property


        Private _SelectedEquipment1 As LocationEquipment
        Public Property SelectedEquipment1 As LocationEquipment
            Get
                Return _SelectedEquipment1
            End Get
            Set(ByVal value As LocationEquipment)
                _SelectedEquipment1 = value
            End Set
        End Property

        Public ReadOnly Property Weekdays As Dictionary(Of Byte, String)
            Get
                Dim wd As New Dictionary(Of Byte, String)
                wd.Add(7, "Sonntag")
                wd.Add(1, "Montag")
                wd.Add(2, "Dienstag")
                wd.Add(3, "Mittwoch")
                wd.Add(4, "Donnerstag")
                wd.Add(5, "Freitag")
                wd.Add(6, "Samstag")
                Return wd
            End Get
        End Property

        Private _SelectedHour As LocationHour
        Public Property SelectedHour() As LocationHour
            Get
                Return _SelectedHour
            End Get
            Set(ByVal value As LocationHour)
                _SelectedHour = value
                RaisePropertyChanged("SelectedHour")
                If Not IsNothing(SelectedHour) Then SelectedHourChanged()
            End Set
        End Property

        Private _BookinggroupLookup As List(Of LocationBookinggroup)
        Public Property BookinggroupLookup() As List(Of LocationBookinggroup)
            Get
                Return _BookinggroupLookup
            End Get
            Set(ByVal value As List(Of LocationBookinggroup))
                _BookinggroupLookup = value
            End Set
        End Property

        Private _TemplatesLookup As List(Of Template)
        Public Property TemplatesLookup() As List(Of Template)
            Get
                Return _TemplatesLookup
            End Get
            Set(ByVal value As List(Of Template))
                _TemplatesLookup = value
            End Set
        End Property


        Public ReadOnly Property LocationHourTypes() As Dictionary(Of LocationHourType, String)
            Get

                Return New Dictionary(Of LocationHourType, String) From {{0, "Privat"}, {10, "Öffentlich"}}
            End Get

        End Property

        Private _PriceGroupsLookup As List(Of PriceGroup)
        Public Property PriceGroupsLookup() As List(Of PriceGroup)
            Get
                Return _PriceGroupsLookup
            End Get
            Set(ByVal value As List(Of PriceGroup))
                _PriceGroupsLookup = value
                RaisePropertyChanged("PriceGroupsLookup")
            End Set
        End Property





        Private _CustomerStates As ObservableCollection(Of CustomerState)
        Public ReadOnly Property CustomerStates As ObservableCollection(Of CustomerState)
            Get
                Return _CustomerStates
            End Get

        End Property

        Private _ProductTypeLookup As ObservableCollection(Of ProductType)
        Public ReadOnly Property ProductTypeLookup() As ObservableCollection(Of ProductType)
            Get
                Return _ProductTypeLookup
            End Get
        End Property

        Private _CostcenterLookup As ObservableCollection(Of Costcenter)
        Public ReadOnly Property CostcenterLookup() As ObservableCollection(Of Costcenter)
            Get
                Return _CostcenterLookup
            End Get
        End Property





        Public Property AddEquipCommand As ICommand
        Public Property DelEquipCommand As ICommand
        Public Property NewCommand As ICommand
        Public Property DelCommand As ICommand
        Public Property CancelCommand As ICommand

        Public Property GetAnspCommand As ICommand
        Public Property DelAnspCommand As ICommand
        Public Property InsertImageCommand As ICommand

        Public Property DelLocationHourCommand As ICommand




        Public Sub New(ByVal context As in1Entities)

            _context = context
            Init()

        End Sub


        Private Sub Init()
            AddEquipCommand = New RelayCommand(AddressOf AddEquip, AddressOf CanAddEquip)
            DelEquipCommand = New RelayCommand(AddressOf DelEquip, AddressOf CanDelEquip)

            NewCommand = New RelayCommand(AddressOf NewLocation)
            DelCommand = New RelayCommand(AddressOf DelLocation)

            GetAnspCommand = New RelayCommand(AddressOf GetAnsp)
            InsertImageCommand = New RelayCommand(AddressOf InsertImage)
            DelLocationHourCommand = New RelayCommand(AddressOf DelLocationHourAction)

            _context.Campuss.Load()
            CampusLookup = _context.Campuss.Local

            _context.Locations.Load()
            Locations = _context.Locations.Local


            'Laden aller Equipments
            Dim equip = From c In _context.Equipments Select c
            Me.AllEquipment = CType(CollectionViewSource.GetDefaultView(equip.ToList), ListCollectionView)

            Me.AllEquipment.Filter = New Predicate(Of Object)(AddressOf Equipmentfilter)

            _context.CustomerStates.Load()
            _CustomerStates = _context.CustomerStates.Local

            _context.LocationBookinggroups.Load()
            _BookinggroupLookup = _context.LocationBookinggroups.Local().ToList()

            _context.PriceGroups.Load()
            PriceGroupsLookup = _context.PriceGroups.Local().ToList()



            Dim templates = From c In _context.Templates Select c

            _TemplatesLookup = templates.ToList

            _context.Costcenters.Load()
            _CostcenterLookup = _context.Costcenters.Local
            _context.ProductTypes.Load()
            _ProductTypeLookup = _context.ProductTypes.Local
        End Sub


        Private Sub DelLocationHourAction(params)
            If Not IsNothing(params) Then
                Dim itm = CType(params, LocationHour)
                If Not IsNothing(itm) Then
                    _context.LocationHours.Remove(itm)
                End If
            End If
        End Sub
        Private Function Equipmentfilter(ByVal item As Object) As Boolean
            Dim itm = TryCast(item, Equipment)
            If itm IsNot Nothing Then
                If Not IsNothing(SelectedLocation) Then
                    Dim res = (From c In SelectedLocation.LocationEquipments Where c.EquipmentID = itm.EquipmentID).Count
                    If res = 0 Then Return True
                End If
            End If
            Return False

        End Function

        Private Sub SelectedHourChanged()
            'Preise checken
            Dim stati As List(Of CustomerState)
            stati = (From st In _context.CustomerStates).ToList

            Dim exists As List(Of Integer) =
                    (From preis In SelectedHour.LocationHourCosts Select preis.CustomerState.CustomerStateID).ToList()

            For Each status In stati
                If Not exists.Contains(status.CustomerStateID) Then
                    Dim neu As New LocationHourCost
                    neu.CustomerState = status
                    neu.Price = 0
                    neu.IsActive = True
                    neu.TaxPercent = 0
                    neu.Vorausbuchungstage = 14
                    SelectedHour.LocationHourCosts.Add(neu)
                End If
            Next
        End Sub

        Private Sub FilterRefresh()
            Me.AllEquipment.Refresh()
        End Sub

        Private Sub NewLocation()
            Dim loc As New Location
            loc.LocationName = "Neuer Ort"
            loc.Campus = CampusLookup.First

            _context.Locations.Add(loc)

            SelectedLocation = loc


        End Sub


        Private Sub AddEquip()
            If Not IsNothing(SelectedEquipment) Then
                Dim itm As New LocationEquipment
                itm.Equipment = SelectedEquipment
                SelectedLocation.LocationEquipments.Add(itm)
                FilterRefresh()

            End If
        End Sub

        Private Function CanAddEquip() As Boolean
            If Not IsNothing(SelectedLocation) And Not IsNothing(SelectedEquipment) Then
                Return True
            End If
            Return False
        End Function

        Private Sub DelEquip()
            If Not IsNothing(SelectedEquipment1) Then
                _context.LocationEquipments.Remove(_SelectedEquipment1)
                FilterRefresh()
            End If
        End Sub

        Private Function CanDelEquip() As Boolean
            If Not IsNothing(_SelectedEquipment1) Then
                Return True
            End If
            Return False
        End Function

        Private Sub DelLocation()
            If Not IsNothing(SelectedLocation) Then
                
                Dim dlg As New DialogService
                If dlg.AskConfirmation("Löschen", "Wirklich diesen Ort löschen? Löschen ist nur möglich, wenn diesem Ort keine Veranstaltungen oder Termin zugeordnet sind") = True Then
                    _context.Locations.Remove(SelectedLocation)
                End If
            End If
        End Sub


        Private Sub GetAnsp()
            Dim search As New HSPinOne.Common.SearchCustomer
            Dim res = search.GetCustomer
            If Not IsNothing(res) Then
                SelectedLocation.Contact = res.CustomerID
            End If

        End Sub

 

        Private Sub InsertImage()
            Dim res = HSPinOne.Common.DocManService.GetImage
            If Not IsNothing(res) And Not Trim(res) = "" And Not IsNothing(SelectedLocation) Then
                'Dim newText = SelectedSport.Beschreibung.Substring(0, CurrentPosition) & " " & res.ToString & " " & SelectedSport.Beschreibung.Substring(CurrentPosition, SelectedSport.Beschreibung.Length - CurrentPosition)
                'SelectedSport.Beschreibung = newText

                Dim arr = Split(SelectedLocation.ImageList, ",").ToList
                arr.Add(res)
                SelectedLocation.ImageList = Join(arr.ToArray, ",")

            End If
        End Sub
    End Class
End Namespace