﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.Windows.Data
Imports System.Windows.Input

Namespace ViewModel




    Public Class AccountViewModel
        Inherits ViewModelBase



        Private _context As in1Entities

        Private _accounts As ObservableCollection(Of Account)
        Public Property Accounts As ObservableCollection(Of Account)
            Get
                Return _accounts
            End Get
            Set(value As ObservableCollection(Of Account))
                _accounts = value
            End Set
        End Property

        Private _SelectedTable As ListCollectionView
        Public Property SelectedTable() As ListCollectionView
            Get
                Return _SelectedTable
            End Get
            Set(ByVal value As ListCollectionView)
                _SelectedTable = value
                RaisePropertyChanged("SelectedTable")
            End Set
        End Property

        Public Sub New(ByVal context As in1Entities)

            _context = context

            _context.Accounts.Load()
            'Accounts = _context.Accounts.Local
            SelectedTable = CollectionViewSource.GetDefaultView(_context.Accounts.Local)

        End Sub
    End Class

End Namespace
