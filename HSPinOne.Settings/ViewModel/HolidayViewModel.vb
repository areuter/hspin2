﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Windows.Input
Imports System.Data.Entity




Namespace ViewModel


    Public Class HolidayViewModel
        Inherits ViewModelBase

        Private _context As in1Entities

        Private _holidays As ObservableCollection(Of Holiday)
        Public Property Holidays As ObservableCollection(Of Holiday)
            Get
                Return _holidays
            End Get
            Set(value As ObservableCollection(Of Holiday))
                _holidays = value
            End Set
        End Property

        Private _selectedholiday As Holiday
        Public Property SelectedHoliday As Holiday
            Get
                Return _selectedholiday
            End Get
            Set(value As Holiday)
                _selectedholiday = value
                RaisePropertyChanged("SelectedHoliday")
            End Set
        End Property

        Private _von As DateTime
        Public Property Von As DateTime
            Get
                Return _von
            End Get
            Set(value As DateTime)
                _von = value
                RaisePropertyChanged("Von")
            End Set
        End Property

        Private _bis As DateTime
        Public Property Bis As DateTime
            Get
                Return _bis
            End Get
            Set(value As DateTime)
                _bis = value
                RaisePropertyChanged("Bis")
            End Set
        End Property

        Private _bezeichnung As String
        Public Property Bezeichnung As String
            Get
                Return _bezeichnung
            End Get
            Set(value As String)
                _bezeichnung = value
                RaisePropertyChanged("Bezeichnung")
            End Set
        End Property

        Private _jahr As Long
        Public Property Jahr As Long
            Get
                Return _jahr
            End Get
            Set(value As Long)
                _jahr = value
                RaisePropertyChanged("Jahr")
            End Set
        End Property

        Public Property NewCommand As ICommand
        Public Property DelCommand As ICommand
        Public Property GenerateCommand As ICommand





        Public Sub New(context As in1Entities)
            _context = context

            NewCommand = New RelayCommand(AddressOf Add)
            DelCommand = New RelayCommand(AddressOf Del, AddressOf CanDel)
            GenerateCommand = New RelayCommand(AddressOf Generate, AddressOf CanGenerate)

            _context.Holidays.Load()
            Holidays = _context.Holidays.Local




        End Sub

        Private Sub Add()
            Dim hol = New Holiday
            hol.HolidayName = "Neuer Feiertag"
            hol.HolidayDate = Now
            hol.Feiertag = False
            Holidays.Add(hol)
            SelectedHoliday = hol
        End Sub

        Private Sub Del()
            If Not IsNothing(SelectedHoliday) Then
                _context.Holidays.Remove(SelectedHoliday)
                SelectedHoliday = Nothing
            End If
        End Sub

        Private Function CanDel() As Boolean
            If Not IsNothing(SelectedHoliday) Then
                If Not SelectedHoliday.Feiertag.Value = True Then
                    Return True
                End If

            End If
            Return False
        End Function


        Private Sub Generate()

            Dim dlg As New DialogService

            'Bundesland holen
            Dim bl = GlobalSettingService.HoleEinstellung("hspbundesland")
            If IsNothing(bl) Then
                dlg.ShowError("Fehler", "Es ist kein Bundesland hinterlegt")
                Return
            End If

            Dim ft As New HSPinOne.Common.Feiertagsermittlung

            Dim fts = ft.GetAll(bl, Jahr)
            For Each itm In fts
                Dim hol As New Holiday
                hol.HolidayDate = itm.Key
                hol.HolidayName = itm.Value
                hol.Feiertag = True

                'Check, ob bereits existiert

                Dim res = From c In Holidays Where DateDiff(DateInterval.Day, c.HolidayDate, hol.HolidayDate) = 0
                If res.Count = 0 Then Holidays.Add(hol)
            Next





        End Sub

        Private Function CanGenerate() As Boolean
            If Not IsNothing(Jahr) Then
                If IsNumeric(Jahr) Then
                    Return True
                End If
            End If
            Return False
        End Function


    End Class

End Namespace
