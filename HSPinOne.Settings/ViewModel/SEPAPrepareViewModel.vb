﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.ComponentModel
Imports System.Windows.Input

Namespace ViewModel


    Public Class SEPAPrepareViewModel
        Inherits ViewModelBase

        Private WithEvents bgw As New BackgroundWorker

        Private _Progress As Integer = 0
        Public Property Progress As Integer
            Get
                Return _Progress
            End Get
            Set(value As Integer)
                _Progress = value
                RaisePropertyChanged("Progress")
            End Set
        End Property

        Public Property StartCommand As ICommand
        Public Property CancelCommand As ICommand


        Private Sub CreateMandates()

            'Dim i As Integer = 0
            'Using con As New in1Entities
            '    Dim query = From c In con.Contracts Where c.CustomerMandateID Is Nothing Select c

            '    For Each itm In query
            '        Dim cm As New CustomerMandate
            '        cm.ContractID = itm.ContractID
            '        cm.CustomerID = itm.CustomerID
            '        cm.DateOfSignature = itm.Vertragsbeginn
            '        cm.Mandatsreferenz = itm.CustomerID.ToString & "-" & itm.ContractID.ToString
            '        cm.Recurring = True
            '        cm.Status = 0
            '        cm.ValidTill = "31.12.2049"
            '        itm.CustomerMandate = cm
            '        i = i + 1
            '        If 10 Mod i = 0 Then con.SaveChanges()
            '    Next


            'End Using

        End Sub


        Private Sub Convert()
            Dim dlg As New DialogService
            If dlg.AskConfirmation("Starten", "Alle Kundenmandate für die Verträge erzeugen?") = True Then

                CreateMandates()
                'If Not bgw.IsBusy Then
                '    bgw.WorkerReportsProgress = True
                '    bgw.WorkerSupportsCancellation = True
                '    bgw.RunWorkerAsync()
                'End If
            End If

        End Sub

        Private Function CanConvert() As Boolean
            If Not IsBusy Then Return True
            Return False
        End Function

        Private Sub bgw_Cancel()
            bgw.CancelAsync()
        End Sub

        Private Function bgw_CanCancel() As Boolean
            If IsBusy Then Return True
            Return False
        End Function

        Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw.DoWork

            IsBusy = True
            Using con As New in1Entities

                Dim pfad = GlobalSettingService.in1DataFolder(in1Folder.documentsarchive)

                Dim query = (From c In con.Documents Where c.Dateiname.Contains(".tif") Or c.Dateiname.Contains(".pdf")).ToList

                Dim total = query.Count

                Dim i As Integer = 0
                For Each itm In query
                    If bgw.CancellationPending Then Exit For

                    'Check if File exists
                    Dim cpfad = pfad & itm.Dateiname
                    If System.IO.File.Exists(cpfad) Then


                        Dim pos = InStrRev(itm.Dateiname, ".")


                        itm.DocumentData = System.IO.File.ReadAllBytes(pfad & itm.Dateiname)

                        itm.Extension = Mid(itm.Dateiname, pos + 1)
                        Dim fn As String = itm.Infotext
                        For Each c In System.IO.Path.GetInvalidFileNameChars()
                            fn = fn.Replace(c, "_")
                        Next

                        itm.Dateiname = fn

                        Dim cust = con.Customers.Find(itm.CustomerID)
                        If Not IsNothing(cust) Then

                            Dim act As New Activity
                            act.ActivityType = 0
                            act.Comment = itm.Infotext
                            act.CustomerID = itm.CustomerID
                            act.Direction = 1
                            act.Zeit = itm.Erstelldatum
                            act.UserID = 2
                            act.DocumentUID = itm.DocumentUID
                            con.Activities.Add(act)

                            act = Nothing
                        End If
                        con.SaveChanges()
                    End If

                    i = i + 1
                    bgw.ReportProgress(CInt(i * 100 / total))

                Next
                e.Result = total

            End Using


        End Sub

        Private Sub bgw_ProgressChanged(ByVal sender As Object, ByVal e As ProgressChangedEventArgs) Handles bgw.ProgressChanged
            Progress = e.ProgressPercentage
        End Sub

        Private Sub bgw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles bgw.RunWorkerCompleted
            IsBusy = False
            Dim dlg As New DialogService
            dlg.ShowInfo("Konvertierung", "Es wurden " & e.Result & " Datensätze konvertiert")
            dlg = Nothing
        End Sub

        Public Sub New()
            StartCommand = New RelayCommand(AddressOf Convert, AddressOf CanConvert)
            CancelCommand = New RelayCommand(AddressOf bgw_Cancel, AddressOf bgw_CanCancel)
        End Sub
    End Class

End Namespace
