﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports HSPinOne.Common
Imports System.Collections.ObjectModel
Imports System.Windows.Input
Imports System.Data.Entity

Imports HSPinOne.Modules.UserLogin
Imports System.Text
Imports System.Collections.Specialized
Imports System.Windows
Imports System.Threading


Namespace ViewModel


    Public Class SerialViewModel
        Inherits ViewModelBase
        Private _context As in1Entities
        Private _institutionlookup As IEnumerable(Of Institution)
        Private _rolelookup As ObservableCollection(Of Role)


#Region "   Properties"

        Private _serial As ObservableCollection(Of Serial)
        Public Property Serials As ObservableCollection(Of Serial)
            Get
                Return _serial
            End Get
            Set(ByVal value As ObservableCollection(Of Serial))
                Console.WriteLine(value)
                _serial = value
            End Set
        End Property

        Private _selecteduser As User
        Public Property SelectedUser As User
            Get
                Return _selecteduser
            End Get
            Set(ByVal value As User)
                _selecteduser = value
                RaisePropertyChanged("SelectedUser")
            End Set
        End Property

        Private _pass As String = "Test"
        Public Property Pass As String
            Get
                Return _pass
            End Get
            Set(ByVal value As String)
                _pass = value
            End Set
        End Property

        Public Property NewCommand As ICommand
        Public Property DelCommand As ICommand
        Public Property SearchCommand As ICommand
        Public Property SaveCommand As ICommand
        Public Property ShowMessageCommand As ICommand

        Public Property AddRoleCommand As ICommand



#End Region

        Public Sub New(ByVal context As in1Entities)
            _context = context
            Init()
 

        End Sub

        Private Property salt As Object

        Private Property SaltValue As String

        Private Property Password As Object
        Private Property Result As Boolean
        Private Property InstitutCode As Object
        Private Property tm As System.DateTime


        Private Sub Init()
            SaveCommand = New RelayCommand(AddressOf SaveSettings)
            ShowMessageCommand = New RelayCommand(AddressOf ShowMessage)


            _context.Serials.Load()
            Serials = _context.Serials.Local

            For Each s In Serials

                If s.SerialKey.Length > 32 Then
                    Dim words As String() = Crypto.DecryptString(s.SerialKey).Split(New Char() {"|"c})
                    If DateTime.Parse(words.First) > Now Then
                        'Do stuff if access
                    Else
                        'do stuff if no access
                    End If
                End If


            Next

        End Sub

        Public Sub ShowMessage()
            MessageBox.Show("Hello , Welcome to EventTrigger for MVVM.")

        End Sub



        Private Function CanGetAnsprechpartner() As Boolean
            If Not IsNothing(SelectedUser) Then Return True
            Return False
        End Function

        Public Function GenHash(ByVal LoginPassword As String, ByVal salt As Byte())
            Try

                Return Crypto.ComputeHash(LoginPassword, "SHA1", salt)

            Catch ex As Exception
                Debug.Print("Exception")
                Throw New Exception(ex.Message, ex.InnerException)
            End Try
        End Function

        Public Function GenSalt()
            Return Crypto.generateRadomSaltvalue()
        End Function

        Private Sub SaveSettings()
            Try
                _context.SaveChanges()
            Catch ex As Exception
                Dim dlg As New DialogService
                dlg.ShowAlert("Fehler", ex.Message)
            End Try

        End Sub

    End Class

End Namespace
