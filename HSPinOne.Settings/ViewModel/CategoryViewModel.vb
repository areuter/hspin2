﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.Windows.Input


Namespace ViewModel

    Public Class CategoryViewModel
        Inherits ViewModelBase

        Private _context As in1Entities


        Private _categorys As ObservableCollection(Of Category)
        Public Property Categorys As ObservableCollection(Of Category)
            Get
                Return _categorys
            End Get
            Set(ByVal value As ObservableCollection(Of Category))
                _categorys = value

            End Set

        End Property

        Private _selectedcategory As Category
        Public Property SelectedCategory As Category
            Get
                Return _selectedcategory
            End Get
            Set(ByVal value As Category)
                _selectedcategory = value
                RaisePropertyChanged("SelectedCategory")
            End Set
        End Property

        Private _campulookup As ObservableCollection(Of Campus)
        Public Property CampusLookup As ObservableCollection(Of Campus)
            Get
                Return _campulookup
            End Get
            Set(ByVal value As ObservableCollection(Of Campus))
                _campulookup = value
            End Set
        End Property

        Private _selectedcolor As String
        Public Property SelectedColor As String
            Get
                Return _selectedcolor
            End Get
            Set(ByVal value As String)
                _selectedcolor = value
                RaisePropertyChanged("SelectedColor")
            End Set
        End Property

        Public Property NewCommand As ICommand
        Public Property DelCommand As ICommand
        Public Property CancelCommand As ICommand



        Public Sub New(ByVal context As in1Entities)

            _context = context
            Init()

        End Sub


        Private Sub Init()
            _context.Categories.Load()
            Categorys = _context.Categories.Local

            NewCommand = New RelayCommand(AddressOf NewCategory)
            DelCommand = New RelayCommand(AddressOf DelCategory)
        End Sub


        Private Sub NewCategory()
            Dim loc As New Category
            loc.CategoryName = "Neue Kategorie"

            _context.Categories.Add(loc)

            SelectedCategory = loc

        End Sub

        Private Sub DelCategory()
            If Not IsNothing(SelectedCategory) Then
                Dim dlg As New DialogService
                If dlg.AskConfirmation("Löschen", "Wirklich diese Kategorie löschen? Löschen ist nur möglich, wenn dieser Kategorie keine Veranstaltungen oder Termine mehr zugeordnet sind") = True Then
                    _context.Categories.Remove(SelectedCategory)
                End If
            End If
        End Sub

    End Class
End Namespace