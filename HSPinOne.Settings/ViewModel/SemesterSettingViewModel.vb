﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.Windows.Input
Imports System.ComponentModel

Namespace ViewModel



    Public Class SemesterSettingViewModel
        Inherits ViewModelBase

        Private _context As in1Entities

        Private WithEvents bgw As New BackgroundWorker

        Private _semester As ObservableCollection(Of Term)
        Public Property Semester As ObservableCollection(Of Term)
            Get
                Return _semester
            End Get
            Set(ByVal value As ObservableCollection(Of Term))
                _semester = value
            End Set
        End Property


        Private _selected As Term
        Public Property Selected As Term
            Get
                Return _selected
            End Get
            Set(ByVal value As Term)
                _selected = value
                If value.TermID > 0 Then
                    CopyVisible = System.Windows.Visibility.Visible
                Else
                    CopyVisible = System.Windows.Visibility.Collapsed
                End If
                SetOld()
                RaisePropertyChanged("Selected")
            End Set
        End Property

        Private _selectedsource As Term
        Public Property SelectedSource As Term
            Get
                Return _selectedsource
            End Get
            Set(ByVal value As Term)
                _selectedsource = value
                Anzahl = SelectedSource.Courses.Where(Function(o) o.Aktiv = True).Count
                RaisePropertyChanged("SelectedSource")
            End Set
        End Property

        Private _MitUelCopy As Boolean = True
        Public Property MitUelCopy() As Boolean
            Get
                Return _MitUelCopy
            End Get
            Set(ByVal value As Boolean)
                _MitUelCopy = value
            End Set
        End Property



        Private _anzahl As Integer
        Public Property Anzahl As Integer
            Get
                Return _anzahl
            End Get
            Set(value As Integer)
                _anzahl = value
                RaisePropertyChanged("Anzahl")
            End Set
        End Property

        Private _fortschritt As Integer
        Public Property Fortschritt As Integer
            Get
                Return _fortschritt
            End Get
            Set(value As Integer)
                _fortschritt = value
                RaisePropertyChanged("Fortschritt")
            End Set
        End Property

        Private _CopyVisible As System.Windows.Visibility
        Public Property CopyVisible As System.Windows.Visibility
            Get
                Return _CopyVisible
            End Get
            Set(value As System.Windows.Visibility)
                _CopyVisible = value
                RaisePropertyChanged("CopyVisible")
            End Set
        End Property


        Public Property NewCommand As ICommand
        Public Property CopyCommand As ICommand
        Public Property CancelCopyCommand As ICommand
        Public Property SetOnlineCommand As ICommand

        Private _oldfrom As DateTime
        Private _oldtill As DateTime

        Public Sub New(ByVal context As in1Entities)
            _context = context

            _context.Terms.OrderBy(Function(o) o.Semesterbeginn).Load()
            Semester = _context.Terms.Local

            NewCommand = New RelayCommand(AddressOf NeuSemester)

            CopyCommand = New RelayCommand(AddressOf Kopieren, AddressOf CanKopieren)
            CancelCopyCommand = New RelayCommand(AddressOf Kopierenabbrechen, AddressOf CanKopierenabbrechen)
            SetOnlineCommand = New RelayCommand(AddressOf SetOnline)

        End Sub

        Private Sub SetOld()
            If Not IsNothing(Selected) Then
                _oldfrom = Selected.Showfrom
                _oldtill = Selected.Showtill
            End If
        End Sub

        Private Sub NeuSemester()
            Dim sem As New Term
            sem.Termname = "Neues Semester"
            sem.Semesterbeginn = Now
            sem.Semesterende = Now
            sem.AktivWWW = False
            sem.Aktiv = False
            sem.Anmeldungab = Now
            sem.Anmeldungbis = Now
            sem.Showfrom = Now
            sem.Showtill = Now
            _context.Terms.Add(sem)
            Selected = sem
        End Sub

        Private Sub Kopieren()
            Dim dlg As New DialogService
            If dlg.AskConfirmation("Datenübernahme", "Wollen Sie wirklich die Daten des alten Semesters übernehmen? Dieser Vorgang kan länger dauern.") = True Then

                bgw.WorkerReportsProgress = True
                bgw.WorkerSupportsCancellation = True
                bgw.RunWorkerAsync()


            End If
        End Sub

        Private Function CanKopieren() As Boolean
            If Not IsNothing(Selected) Then
                If Selected.TermID > 0 Then
                    If Not IsNothing(SelectedSource) Then
                        If SelectedSource.TermID <> Selected.TermID Then
                            Return True
                        End If
                    End If
                End If
            End If
            Return False
        End Function

        Private Sub Kopierenabbrechen()
            Dim dlg As New DialogService
            If dlg.AskConfirmation("Abbrechen", "Sind sie sicher, dass sie die Semesterübernahme abbrechen möchten? Die bereits kopierten Daten bleiben erhalten") = True Then
                bgw.CancelAsync()
            End If

        End Sub

        Private Function CanKopierenabbrechen() As Boolean
            If bgw.IsBusy Then
                Return True
            End If
            Return False
        End Function

        Private Sub SetOnline()
            Dim dlg As New DialogService
            Dim cnt As Integer = 0
            Dim cntall As Integer = 0
            If dlg.AskConfirmation("Setzen", "Sollen wirklich alle Veranstaltungen des Semester auf die neuen Sichtbarkeittermine gesetzt werden") = True Then
                Using con As New in1Entities
                    Dim query = From c In _context.Courses Where c.TermID = Selected.TermID And c.Aktiv = True Select c

                    For Each itm In query
                        ' Nur aktualisieren, wenn Anmeldetermine unverändert.
                        If _oldfrom = itm.Anmeldungvon And _oldtill = itm.Anmeldungbis Then
                            itm.Anmeldungvon = Selected.Showfrom
                            itm.Anmeldungbis = Selected.Showtill
                            cnt = cnt + 1
                        End If

                    Next
                    con.SaveChanges()
                    cntall = query.Count()
                End Using
                dlg.ShowInfo("Fertig", "Es wurden " & cnt & " von " & cntall & " Veranstaltungen aktualisiert")
            End If



        End Sub


        Private Sub bgw_DoWork(ByVal sender As Object, e As DoWorkEventArgs) Handles bgw.DoWork

            Dim _con As New in1Entities
            Dim counter As Integer = 1
            For Each itm In SelectedSource.Courses.Where(Function(o) o.Aktiv = True)
                Dim kopie As New Course
                kopie.Aktiv = False
                kopie.Anmeldungbis = Selected.Showtill
                kopie.AnmeldungLock = False
                kopie.Anmeldungvon = Selected.Showfrom
                kopie.AnsprechpartnerID = itm.AnsprechpartnerID
                kopie.Bearbeitungsvermerk = itm.Bearbeitungsvermerk
                kopie.Bemerkung = itm.Bemerkung
                kopie.CategoryID = itm.CategoryID
                kopie.CostcenterID = itm.CostcenterID
                kopie.TemplateID = itm.TemplateID
                kopie.ProductTypeID = itm.ProductTypeID
                kopie.CourseAudienceID = itm.CourseAudienceID
                kopie.AccessType = itm.AccessType
                kopie.CourseCostID = itm.CourseCostID
                kopie.CourseGenderID = itm.CourseGenderID
                kopie.CourseRegistrationID = itm.CourseRegistrationID
                kopie.CourseStateID = 1
                kopie.LocationID = itm.LocationID
                kopie.maxTN = itm.maxTN
                kopie.minTN = itm.minTN
                kopie.PartyID = itm.PartyID
                kopie.SportID = itm.SportID
                kopie.TermID = Selected.TermID
                kopie.Zeittext = itm.Zeittext
                kopie.Zusatzinfo = itm.Zusatzinfo
                kopie.Erstelltvon = 2
                kopie.Erstelltam = Now
                kopie.DTStart = itm.DTStart
                kopie.DTEnd = itm.DTEnd
                kopie.RRule = itm.RRule
                kopie.DTUntil = itm.DTUntil
                kopie.Language = itm.Language




                ' Die Courseprices mit den Anmeldeterminen werden kopiert und an die Anmeldetermine des Semesters angepasst
                If itm.CoursePrices.Any Then
                    For Each oprice In itm.CoursePrices
                        Dim cp As New CoursePrice
                        cp.Anmeldungbis = Selected.Anmeldungbis
                        cp.Anmeldungerlaubt = oprice.Anmeldungerlaubt
                        cp.Anmeldungvon = Selected.Anmeldungab
                        cp.CustomerStateID = oprice.CustomerStateID
                        cp.Preis = oprice.Preis
                        cp.TaxPercent = oprice.TaxPercent
                        cp.CostcenterID = oprice.CostcenterID
                        cp.KSTextern = oprice.KSTextern
                        cp.Innenauftrag = oprice.Innenauftrag
                        cp.Sachkonto = oprice.Sachkonto
                        cp.AccountingTemplateID = oprice.AccountingTemplateID
                        kopie.CoursePrices.Add(cp)
                    Next
                End If

                ' Die ÜL mit Stundenlohn werden kopiert
                If itm.CourseStaffs.Any And MitUelCopy Then
                    For Each ostaff In itm.CourseStaffs
                        Dim st As New CourseStaff
                        st.CustomerID = ostaff.CustomerID
                        st.StdLohn = ostaff.StdLohn
                        kopie.CourseStaffs.Add(st)
                    Next
                End If

                'Tags koopieren
                If itm.CourseTags.Any Then
                    For Each tag In itm.CourseTags
                        Dim t As New CourseTag
                        t.TagID = tag.TagID
                        t.Tagorder = tag.Tagorder
                        kopie.CourseTags.Add(t)
                    Next
                End If

                'CourseMetas kopieren
                If itm.CourseMetas.Any Then
                    For Each meta In itm.CourseMetas
                        Dim m As New CourseMeta
                        m.MetaKey = meta.MetaKey
                        m.MetaValue = meta.MetaValue
                        kopie.CourseMetas.Add(m)
                    Next
                End If

                'Zu den Courses hinzufügen
                _con.Courses.Add(kopie)

                ' Speichern
                _con.SaveChanges("System")
                bgw.ReportProgress(counter)
                counter = counter + 1
            Next
            _con.Dispose()
        End Sub

        Private Sub bgw_ProgressChanged(ByVal sender As Object, ByVal e As ProgressChangedEventArgs) Handles bgw.ProgressChanged
            Fortschritt = e.ProgressPercentage
        End Sub

        Private Sub bgw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles bgw.RunWorkerCompleted
            Dim dlg As New DialogService
            dlg.ShowInfo("Fertig", "Die Daten des Semesters wurden kopiert")
            dlg = Nothing
        End Sub


    End Class



End Namespace
