﻿Imports HSPinOne.Infrastructure
Imports HSPinOne.DAL
Imports System.Collections.ObjectModel
Imports System.Data.Entity

Namespace ViewModel


    Public Class AccountingTemplateViewModel
        Inherits ViewModelBase


        Private _context As New in1Entities


        Private _AccountingTemplates As ObservableCollection(Of AccountingTemplate)
        Public Property AccountingTemplates() As ObservableCollection(Of AccountingTemplate)
            Get
                Return _AccountingTemplates
            End Get
            Set(ByVal value As ObservableCollection(Of AccountingTemplate))
                _AccountingTemplates = value
            End Set
        End Property

        Private _Selected As AccountingTemplate
        Public Property Selected() As AccountingTemplate
            Get
                Return _Selected
            End Get
            Set(ByVal value As AccountingTemplate)
                _Selected = value
                RaisePropertyChanged("Selected")
            End Set
        End Property

        Public Property SaveCommand As New RelayCommand(AddressOf SaveAction)
        Public Property NewCommand As New RelayCommand(AddressOf NewAction)
        Public Sub New()
            _context.AccountingTemplates.Load()
            _AccountingTemplates = _context.AccountingTemplates.Local

        End Sub

        Private Sub SaveAction()
            _context.SaveChanges()
        End Sub

        Private Sub NewAction()
            Dim tpl As New AccountingTemplate With {
            .Bezeichnung = "Neue Buchungsvorlage"
        }
            AccountingTemplates.Add(tpl)
            Selected = tpl
        End Sub

    End Class
End Namespace