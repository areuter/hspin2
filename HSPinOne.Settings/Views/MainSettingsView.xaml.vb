﻿
Imports System.ComponentModel.Composition

<Export(GetType(MainSettingsView)), PartCreationPolicy(CreationPolicy.Shared)> _
Public Class MainSettingsView



    Public Sub New()

        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()

        ' Fügen Sie Initialisierungen nach dem InitializeComponent()-Aufruf hinzu.


    End Sub
    <Import()> _
    Public WriteOnly Property ViewModel As ViewModel.MainSettingsViewModel
        Set(ByVal value As ViewModel.MainSettingsViewModel)
            Me.DataContext = value
        End Set
    End Property
End Class
