﻿Imports System.Windows.Media
Imports HSPinOne.Modules.Settings

Public Class CategoryView

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub CategoryColorPicker_SelectedColorChanged(sender As System.Object, e As System.EventArgs)
        Dim test As Color

        test = sender.SelectedColor
        Me.SelectedCategory.DataContext.SelectedCategory.CategoryColor = test.ToString
        Me.SelectedCategory.DataContext.SelectedColor = test.ToString

    End Sub
End Class
