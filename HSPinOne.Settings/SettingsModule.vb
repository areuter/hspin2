﻿

Imports Prism.Regions
Imports Prism.Modularity
Imports HSPinOne.Infrastructure
Imports Prism.Ioc

Public Class SettingsModule
    Implements IModule


    Public Sub RegisterTypes(containerRegistry As IContainerRegistry) Implements IModule.RegisterTypes
        'Throw New NotImplementedException()
        containerRegistry.RegisterInstance(Of ISettingsService)(New SettingsService())
    End Sub

    Public Sub OnInitialized(containerProvider As IContainerProvider) Implements IModule.OnInitialized


    End Sub


    'Protected Overridable Sub RegisterViewsAndServices()


    '    _container.RegisterType(Of ISettingsService, SettingsService)()

    '    _container.RegisterType(Of Object, MainSettingsView)()
    '    _container.RegisterType(Of Object, SportsView)()


    'End Sub

End Class
