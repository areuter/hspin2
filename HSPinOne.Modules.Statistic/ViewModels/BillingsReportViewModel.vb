﻿Imports System.Collections.ObjectModel
Imports System.ComponentModel
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Public Class BillingsReportViewModel
    Inherits ViewModelBase

    Private _From As DateTime = Now
    Public Property From() As DateTime
        Get
            Return _From
        End Get
        Set(ByVal value As DateTime)
            _From = value
        End Set
    End Property

    Private _Till As DateTime = Now
    Public Property Till() As DateTime
        Get
            Return _Till
        End Get
        Set(ByVal value As DateTime)
            _Till = value
        End Set
    End Property

    Private _Billings As ObservableCollection(Of Billing)

    Public Property Billings() As ObservableCollection(Of Billing)
        Get
            Return _Billings
        End Get
        Set(ByVal value As ObservableCollection(Of Billing))
            _Billings = value
            RaisePropertyChanged("Billings")
        End Set
    End Property

    Public Property RefreshCommand = New RelayCommand(AddressOf Refresh)

    Private WithEvents Bgw As New BackgroundWorker
    Private _context As New in1Entities

    Public Sub New()
        From = DateAdd(DateInterval.Day, -1, From)
    End Sub

    Private Sub Refresh()
        _context = New in1Entities
        If Not Bgw.IsBusy Then
            IsBusy = True
            Bgw.RunWorkerAsync()
        End If

    End Sub

    Private Sub Bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles Bgw.DoWork

        Dim repo As New Repositories.BillingRepository(_context)
        repo.LoadJournal(From, Till)
        e.Result = _context.Billings.Local


    End Sub

    Private Sub Bgw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles Bgw.RunWorkerCompleted
        If Not IsNothing(e.Error) Then

        Else
            Billings = e.Result
        End If
        IsBusy = False
    End Sub


End Class
