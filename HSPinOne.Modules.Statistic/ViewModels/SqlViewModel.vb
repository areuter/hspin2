﻿Imports System.Collections.ObjectModel
Imports System.ComponentModel
Imports System.Data
Imports System.Data.Entity
Imports System.Data.SqlClient
Imports System.Threading.Tasks
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure

Public Class SqlViewModel
    Inherits ViewModelBase

    Private _Sqlqueries As ObservableCollection(Of Sqlquery)
    Public Property Sqlqueries() As ObservableCollection(Of Sqlquery)
        Get
            Return _Sqlqueries
        End Get
        Set(ByVal value As ObservableCollection(Of Sqlquery))
            _Sqlqueries = value
            RaisePropertyChanged("Sqlqueries")
        End Set
    End Property

    Private _SelectedSqlquery As Sqlquery
    Public Property SelectedSqlquery() As Sqlquery
        Get
            Return _SelectedSqlquery
        End Get
        Set(ByVal value As Sqlquery)
            _SelectedSqlquery = value
            CreateControls()
            RaisePropertyChanged("SelectedSqlquery")
        End Set
    End Property

    Private _LV As ListCollectionView
    Public Property LV() As ListCollectionView
        Get
            Return _LV
        End Get
        Set(ByVal value As ListCollectionView)
            _LV = value
            RaisePropertyChanged("LV")
        End Set
    End Property

    Public Property Data As New ObservableCollection(Of DynamicControl)


    Public Property ExecuteCommand As ICommand = New RelayCommand(AddressOf QueryAction)

    Private WithEvents _bgw As New BackgroundWorker

    Private _dgView As DataView
    Public Property dgView() As DataView
        Get
            Return _dgView
        End Get
        Set(ByVal value As DataView)
            _dgView = value
            RaisePropertyChanged("dgView")
        End Set
    End Property


    Private view As DataView

    Private _sql As String

    Public Sub New()
        IsBusy = False
        Using con As New in1Entities
            con.Sqlqueries.Load()
            Sqlqueries = con.Sqlqueries.Local
            If Sqlqueries.Count > 0 Then
                SelectedSqlquery = Sqlqueries.FirstOrDefault()
            End If
        End Using
    End Sub

    Private Sub CreateControls()
        Data.Clear()

        If Not IsNothing(SelectedSqlquery) Then
            Dim input As String = SelectedSqlquery.Input
            Dim arr = input.Split(",")
            If arr.Length > 0 Then
                For Each itm In arr
                    Dim desc = itm.Split(":")
                    If desc.Length = 2 Then
                        Data.Add(New DynamicControl With {.Key = Trim(desc(0)), .Value = "", .Typ = Trim(desc(1))})
                    End If

                Next
            End If
        End If

    End Sub

    Private Sub QueryAction()
        If Not _bgw.IsBusy Then
            IsBusy = True
            Dim args() As String
            ReDim args(Data.Count)
            Dim i As Integer = 0
            For Each itm In Data
                args(i) = itm.Value
                If itm.Typ = "date" Then
                    Dim dt As DateTime
                    DateTime.TryParse(itm.Value, dt)
                    args(i) = dt.ToString()

                End If
                i = i + 1
            Next
            _sql = String.Format(SelectedSqlquery.Sql, args)
            _bgw.RunWorkerAsync()
        End If

    End Sub

    Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles _bgw.DoWork
        Dim result = GetAnonymousResults(_sql)
        e.Result = result
    End Sub

    Private Sub bgw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles _bgw.RunWorkerCompleted
        IsBusy = False
        If e.Result = 0 Then
            dgView = view
        End If
    End Sub

    Private Function GetAnonymousResults(strsql As String) As Integer
        Try


            Using con As New in1Entities
                Dim connstr = con.Database.Connection.ConnectionString

                Dim conn As New SqlConnection(connstr)
                Dim command As New SqlCommand(strsql, conn)
                conn.Open()
                Dim reader As SqlDataReader = command.ExecuteReader()

                Dim table As New DataTable
                table.Load(reader)
                reader.Close()
                conn.Close()

                view = table.DefaultView
                Return 0
            End Using

        Catch ex As Exception
            Return 1
        End Try
    End Function

End Class

Public Class DynamicControl
    Property Key As String
    Property Value As String
    Property Typ As String


End Class