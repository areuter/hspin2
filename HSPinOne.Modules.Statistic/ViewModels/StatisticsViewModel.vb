﻿
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Reflection
Imports System.Data.Entity
Imports System.ComponentModel

Public Class StatisticsViewModel
    Inherits HSPinOne.Infrastructure.ViewModelBase


#Region "Properties"


    Private _Queries As List(Of Queryitem) = New List(Of Queryitem)
    Public Property Queries() As List(Of Queryitem)
        Get
            Return _Queries
        End Get
        Set(ByVal value As List(Of Queryitem))
            _Queries = value
        End Set
    End Property

    Private _SelectedQuery As Queryitem
    Public Property SelectedQuery() As Queryitem
        Get
            Return _SelectedQuery
        End Get
        Set(ByVal value As Queryitem)
            _SelectedQuery = value
            If Not IsNothing(SelectedQuery.Action) Then
                'ExecuteQuery()
            End If
            RaisePropertyChanged("SelectedQuery")
        End Set
    End Property


    Private _Filter1Value As DateTime = Now
    Public Property Filter1Value() As DateTime
        Get
            Return _Filter1Value
        End Get
        Set(ByVal value As DateTime)
            _Filter1Value = value
        End Set
    End Property

    Private _Filter2Value As DateTime = Now
    Public Property Filter2Value() As DateTime
        Get
            Return _Filter2Value
        End Get
        Set(ByVal value As DateTime)
            _Filter2Value = value
        End Set
    End Property

    Private _Results As ListCollectionView
    Public Property Results() As ListCollectionView
        Get
            Return _Results
        End Get
        Set(ByVal value As ListCollectionView)
            _Results = value
        End Set
    End Property

    Private _Terms As List(Of Term)
    Public Property Terms() As List(Of Term)
        Get
            Return _Terms
        End Get
        Set(ByVal value As List(Of Term))
            _Terms = value
        End Set
    End Property

    Private _TermID As Integer
    Public Property TermID() As Integer
        Get
            Return _TermID
        End Get
        Set(ByVal value As Integer)
            _TermID = value
        End Set
    End Property


    Public Property ExecuteCommand As ICommand

#End Region


    Private _context As in1Entities

    Private WithEvents _bgw As BackgroundWorker



    Public Sub New()

        Using con As New in1Entities
            Terms = (From c In con.Terms Order By c.Semesterbeginn Descending).ToList()
        End Using

        Queries.Add(New Queryitem With {.Name = "Checkins/Monat", .Filter1 = "Datum", .Action = "GetCheckins"})
        Queries.Add(New Queryitem With {.Name = "Vertragsabschlüsse/Tag", .Filter1 = "Datum", .Action = "GetContractsperDay"})
        Queries.Add(New Queryitem With {.Name = "Auslaufende Verträge", .Action = "GetEndingContracts"})
        Queries.Add(New Queryitem With {.Name = "Verteilung Verträge", .Filter1 = "Datum", .Action = "GetGenderContracts"})

        Queries.Add(New Queryitem With {.Name = "Kursstatistik", .ShowTerm = True, .Action = "GetCourseStatistic"})
        Queries.Add(New Queryitem With {.Name = "Kursbilanz", .ShowTerm = True, .Action = "GetCourseBilanz"})
        Queries.Add(New Queryitem With {.Name = "Kursstatistik Gender", .ShowTerm = True, .Action = "GetCourseGender"})
        Queries.Add(New Queryitem With {.Name = "Kurse nach Kategorie", .ShowTerm = True, .Action = "GetCourseperCategory"})
        Queries.Add(New Queryitem With {.Name = "Übungsleiter Gender", .ShowTerm = True, .Action = "GetTeamerGender"})

        Queries.Add(New Queryitem With {.Name = "Fluktuation", .Filter1 = "Datum", .Filter2 = "Datum", .Action = "GetFluktuation"})

        Queries.Add(New Queryitem With {.Name = "Warengruppenreport/Zeitraum", .Filter1 = "Datum", .Filter2 = "Datum", .Action = "GetProductTypes"})
        Queries.Add(New Queryitem With {.Name = "Locationreport", .Filter1 = "Datum", .Filter2 = "Datum", .Action = "GetLocationStats"})

        Queries.Add(New Queryitem With {.Name = "Artikelreport", .Filter1 = "Datum", .Filter2 = "Datum", .Action = "GetArticles"})

        ExecuteCommand = New RelayCommand(AddressOf ExecuteQuery)

        SelectedQuery = Queries.FirstOrDefault()
    End Sub

    Private Sub ExecuteQuery()

        IsBusy = True
        _context = New in1Entities
        _bgw = New BackgroundWorker
        If Not _bgw.IsBusy Then
            _bgw.RunWorkerAsync()
        End If



    End Sub

    Private Sub bgwDoWork(sender As Object, e As DoWorkEventArgs) Handles _bgw.DoWork


        CallByName(Me, SelectedQuery.Action, CallType.Method)


    End Sub

    Private Sub bgwRunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles _bgw.RunWorkerCompleted
        RaisePropertyChanged("Results")
        IsBusy = False
        _context.Dispose()
    End Sub


    Public Sub GetCheckins()
        Dim oldto = _context.Database.CommandTimeout
        _context.Database.CommandTimeout = 60
        If Not IsNothing(Filter1Value) Then
            Dim start = DateSerial(Year(Filter1Value), Month(Filter1Value), 1)
            Dim ende = DateAdd(DateInterval.Month, 1, start)
            'Dim query = (From c In _context.Customers Where c.Aktiv = True Group By c.Gender Into Anzahl = Count())
            Dim query = From c In _context.CheckinLogs
                        Where c.Checkin >= start And c.Checkin <= ende
                        Group By
                            cYear = c.Checkin.Year,
                            cMonth = c.Checkin.Month,
                            cDay = c.Checkin.Day,
                            cLoc = c.CheckinLocation
                            Into Group
                        Let xYear = cYear, xMonth = cMonth, xDay = cDay
                        Order By xYear, xMonth, xDay
                        Select New With {
                            .Year = cYear,
                            .Month = cMonth,
                            .Day = cDay,
                            .CheckinLocation = cLoc,
                    .Anzahl = Group.Count()
                        }

            _Results = CType(CollectionViewSource.GetDefaultView(query.ToList), ListCollectionView)
        End If
        _context.Database.CommandTimeout = oldto
    End Sub

    Public Sub GetContractsperDay()
        If Not IsNothing(Filter1Value) Then
            Dim query = From c In _context.Contracts _
            Where c.Vertragsbeginn >= Filter1Value
            Group By
                cYear = c.Vertragsbeginn.Year,
                cMonth = c.Vertragsbeginn.Month,
                cDay = c.Vertragsbeginn.Day
            Into Group _
            Let xYear = cYear, xMonth = cMonth, xDay = cDay _
            Order By xYear, xMonth, xDay _
            Select New With {
                .Year = cYear,
                .Month = cMonth,
                .Day = cDay,
                .Vertraege = Group.Count
            }


            _Results = CType(CollectionViewSource.GetDefaultView(query.ToList), ListCollectionView)
        End If
    End Sub

    Public Sub GetEndingContracts()
        Dim datend = DateAdd(DateInterval.Day, 14, Now)
        Dim query = From c In _context.Contracts Where c.Vertragsende < datend And c.Vertragsende > Now And c.Verlaengerungsmonate = 0 _
                    Select c.Customer.Suchname, c.Vertragsende

        _Results = CType(CollectionViewSource.GetDefaultView(query.ToList), ListCollectionView)
    End Sub

    Public Sub GetGenderContracts()
        If Not IsNothing(Filter1Value) Then
            Dim SelectedDate = CDate(Filter1Value)
            Dim query = From c In _context.Contracts
                        Where c.Vertragsbeginn <= SelectedDate And c.Vertragsende >= SelectedDate
                        Group By c.Customer.Gender, c.Customer.CustomerState.CustomerStatename, c.ContractRate.ContractRateName, c.Ratenbetrag Into Group
                        Select New With {
                .Contract = ContractRateName,
                .Statusgruppe = CustomerStatename,
                .Gender = Gender,
                .Ratenbetrag = Ratenbetrag,
                .Summe = Group.Sum(Function(o) o.Ratenbetrag),
                .Gesamt = Group.Count()
            }
            _Results = CType(CollectionViewSource.GetDefaultView(query.ToList), ListCollectionView)
        End If
    End Sub

    Public Sub GetCourseBilanz()
        If Not IsNothing(TermID) Then
            Dim query = From c In _context.Billings _
                Where c.CourseID IsNot Nothing And c.IsStorno = False And c.Course.TermID = TermID _
                Group By
                    c.CourseID,
                    c.Course.Location.LocationName,
                    c.Course.Sport.Sportname,
                    c.Course.Zusatzinfo,
                    c.Course.Location.Campus.CampusName
                    Into Group _
                Let CourseName = Sportname + " " + If(Zusatzinfo Is Nothing, "", Zusatzinfo) _
                Order By CampusName, CourseName _
                Select New With { _
                    .CourseID = CourseID, _
                    .CourseName = CourseName, _
                    .Location = LocationName, _
                    .Campus = CampusName, _
                    .Anzahl = Group.Count, _
                    .Einnahmen = Group.Sum(Function(s) s.Brutto) _
                }

            _Results = CType(CollectionViewSource.GetDefaultView(query.ToList), ListCollectionView)
        End If
    End Sub
    Public Sub GetCourseStatistic()
        If Not IsNothing(TermID) Then
            Dim query = From c In _context.Billings _
                Where c.CourseID IsNot Nothing And c.IsStorno = False And c.Course.TermID = TermID And c.Course.maxTN > 0 And c.Course.maxTN IsNot Nothing
                Group By
                    c.CourseID,
                    c.Course.Location.LocationName,
                    c.Course.Sport.Sportname,
                    c.Course.Zeittext,
                    c.Course.Zusatzinfo,
                    c.Course.Location.Campus.CampusName,
                    c.Course.maxTN,
                    c.Course.minTN
                    Into Group _
                Let
                    CourseName = Sportname + " " + If(Zusatzinfo Is Nothing, "", Zusatzinfo),
                    lFrei = maxTN - Group.Count,
                    lAuslastung = (Group.Count * 100) / maxTN _
                Order By CampusName, CourseName _
                Select New With {
                   .CourseID = CourseID,
                   .CourseName = CourseName,
                   .Zeittext = Zeittext,
                   .Location = LocationName,
                   .Campus = CampusName,
                   .Anzahl = Group.Count,
                   .Frei = lFrei,
                   .Auslastung = lAuslastung,
                   .MaxTN = maxTN
               }

            _Results = CType(CollectionViewSource.GetDefaultView(query.ToList), ListCollectionView)
        End If
    End Sub

    Public Sub GetCourseperCategory()
        If Not IsNothing(TermID) Then

            Dim query =
                From c In _context.Courses
                Join s In _context.Sports On c.SportID Equals s.SportID
                Join cat In _context.Categories On c.CategoryID Equals cat.CategoryID
                Where (c.TermID = TermID And c.Aktiv = True)
            Group cat By cat.CategoryName, cat.CategoryID Into Group
            Select New With {
                .Name = CategoryName,
                .Anzahl = Group.Count
            }
            _Results = CType(CollectionViewSource.GetDefaultView(query.ToList), ListCollectionView)
        End If
    End Sub

    Public Sub GetFluktuation()
        Dim start As DateTime = DateSerial(Year(Filter1Value), Month(Filter1Value), 1)
        Dim ende As DateTime = DateSerial(Year(Filter2Value), Month(Filter2Value), 1)
        Dim itms As New List(Of Fluktuation)

        Do While start < ende
            Dim contracts1 = (From c In _context.Contracts Where c.Vertragsbeginn <= start And c.Vertragsende >= start).Count()
            Dim monatsende = start.AddMonths(1).AddHours(-1)
            Dim contracts2 = (From c In _context.Contracts Where c.Vertragsbeginn <= monatsende And c.Vertragsende >= monatsende).Count()
            Dim av As Decimal = (contracts1 + contracts2) / 2

            Dim kuend = (From c In _context.Contracts Where c.Kuendigungzum >= start And c.Kuendigungzum <= monatsende).Count()

            Dim strdatum = Year(start).ToString() & "." & Month(start).ToString()

            itms.Add(New Fluktuation With {.Monat = strdatum, .Vertraege = av, .Kuendigungen = kuend})

            start = start.AddMonths(1)

        Loop
        _Results = CType(CollectionViewSource.GetDefaultView(itms), ListCollectionView)

    End Sub

    Public Sub GetProductTypes()
        Dim start As DateTime = DateSerial(Year(Filter1Value), Month(Filter1Value), Day(Filter1Value))
        Dim ende As DateTime = DateSerial(Year(Filter2Value), Month(Filter2Value), Day(Filter2Value))
        ende = DateAdd(DateInterval.Day, 1, ende)

        Dim query = From c In _context.Billings.Include(Function(o) o.ProductType)
        Where c.Faelligkeit > start And c.Faelligkeit < ende And c.IsStorno = False
        Group By c.ProductTypeID, c.ProductType.ProductTypeName
        Into Group
        Select New With {
            .Warengruppe = ProductTypeName,
            .Summe = Group.Sum(Function(o) o.Brutto)
        }
        Results = CType(CollectionViewSource.GetDefaultView(query.ToList), ListCollectionView)
    End Sub

    Public Sub GetLocationStats()
        Dim start As DateTime = DateSerial(Year(Filter1Value), Month(Filter1Value), Day(Filter1Value))
        Dim ende As DateTime = DateSerial(Year(Filter2Value), Month(Filter2Value), Day(Filter2Value))
        ende = DateAdd(DateInterval.Day, 1, ende)

        Dim query = From c In _context.Billings
                    Where c.Faelligkeit > start And c.Faelligkeit < ende And c.IsStorno = False
                    Join b In _context.Bookings On c.BookingID Equals b.BookingID
                    Group By b.Location.LocationName
                    Into Group
                    Select New With {
                        .Ort = LocationName,
                        .Summe = Group.Sum(Function(o) o.c.Brutto)
                    }
        Results = CType(CollectionViewSource.GetDefaultView(query.ToList), ListCollectionView)

    End Sub

    Public Sub GetCourseGender()
        If Not IsNothing(TermID) Then
            Dim query = From c In _context.Billings _
                Where c.CourseID IsNot Nothing And c.IsStorno = False And c.Course.TermID = TermID _
                Group By
                    c.CourseID,
                    c.Course.Location.LocationName,
                    c.Course.Sport.Sportname,
                    c.Course.Zusatzinfo,
                    c.Course.Location.Campus.CampusName
                Into Group _
                Let CourseName = Sportname + " " + If(Zusatzinfo Is Nothing, "", Zusatzinfo) _
                Order By CampusName, CourseName _
                Select New With { _
                    .CourseID = CourseID, _
                    .CourseName = CourseName, _
                    .Location = LocationName, _
                    .Campus = CampusName, _
                    .Männer = Group.Where(Function(d) d.Customer.Gender = "m").Count(), _
                    .ProzentMänner = (Group.Where(Function(d) d.Customer.Gender = "m").Count()) / Group.Count(), _
                    .Frauen = Group.Where(Function(d) d.Customer.Gender = "w").Count(), _
                    .ProzentFrauen = (Group.Where(Function(d) d.Customer.Gender = "w").Count()) / Group.Count(), _
                    .Gesamt = Group.Count() _
                }

            Results = CType(CollectionViewSource.GetDefaultView(query.ToList), ListCollectionView)
        End If
    End Sub

    Public Sub GetTeamerGender()
        If Not IsNothing(TermID) Then
            Dim query = From c In _context.CourseStaffs
                        Where c.Course.TermID = TermID
                        Group By c.Customer.Gender
                        Into Group
                        Select New With {
                            .KursemitUeL = Gender,
                            .Anzahl = Group.Count()
                        }

            Results = CType(CollectionViewSource.GetDefaultView(query.ToList), ListCollectionView)
        End If
    End Sub

    Public Sub GetArticles()
        Dim start As DateTime = DateSerial(Year(Filter1Value), Month(Filter1Value), Day(Filter1Value))
        Dim ende As DateTime = DateSerial(Year(Filter2Value), Month(Filter2Value), Day(Filter2Value))
        ende = DateAdd(DateInterval.Day, 1, ende)

        Dim query = From c In _context.Billings.Include(Function(o) o.Package)
                    Where c.Rechnungsdatum > start And c.Rechnungsdatum < ende And c.IsStorno = False
                    Group By c.Package.PackageName
                    Into Group
                    Select New With {
                        .Packagename = PackageName,
                        .Summe = Group.Sum(Function(o) o.Brutto)
                    }
        Results = CType(CollectionViewSource.GetDefaultView(query.ToList), ListCollectionView)
    End Sub

End Class


Public Class Queryitem
    Inherits ViewModelBase

    Private _Name As String
    Public Property Name() As String
        Get
            Return _Name
        End Get
        Set(ByVal value As String)
            _Name = value
        End Set
    End Property

    Private _Filter1 As String
    Public Property Filter1() As String
        Get
            Return _Filter1
        End Get
        Set(ByVal value As String)
            _Filter1 = value
            If value.Length > 0 Then
                ShowFilter1 = True
            Else
                ShowFilter1 = False
            End If
            RaisePropertyChanged("Filter1")
        End Set
    End Property

    Private _ShowFilter1 As Boolean = False
    Public Property ShowFilter1() As Boolean
        Get
            Return _ShowFilter1
        End Get
        Set(ByVal value As Boolean)
            _ShowFilter1 = value
            RaisePropertyChanged("ShowFilter1")
        End Set
    End Property


    Private _Filter2 As String
    Public Property Filter2() As String
        Get
            Return _Filter2
        End Get
        Set(ByVal value As String)
            _Filter2 = value
            If value.Length > 0 Then
                ShowFilter2 = True
            Else
                ShowFilter2 = False
            End If
            RaisePropertyChanged("Filter2")
        End Set
    End Property

    Private _ShowFilter2 As Boolean = False
    Public Property ShowFilter2() As Boolean
        Get
            Return _ShowFilter2
        End Get
        Set(ByVal value As Boolean)
            _ShowFilter2 = value
            RaisePropertyChanged("ShowFilter2")
        End Set
    End Property


    Private _ShowTerm As Boolean = False
    Public Property ShowTerm() As Boolean
        Get
            Return _ShowTerm
        End Get
        Set(ByVal value As Boolean)
            _ShowTerm = value
            RaisePropertyChanged("ShowTerm")
        End Set
    End Property


    Private _Action As String
    Public Property Action() As String
        Get
            Return _Action
        End Get
        Set(ByVal value As String)
            _Action = value
        End Set
    End Property





End Class

