﻿Imports System.ComponentModel.Composition
Imports HSPinOne.Infrastructure
Imports Telerik.Windows.Controls


Public Class StatisticsView


    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub Button_Click(sender As Object, e As RoutedEventArgs)


        Dim fn As String
        fn = "export" & Year(Now) & Month(Now) & Day(Now) & Hour(Now) & Minute(Now) & Second(Now) & ".xlsx"
        Dim dlg As New DialogService
        Dim sfn = dlg.SaveFile("XLSX-Dateien (*.xlsx)|*.xlsx", fn, ".xlsx")
        If Not Trim(sfn) = "" Then
            Using stream As IO.Stream = IO.File.Open(sfn, IO.FileMode.OpenOrCreate)
                tableView.ExportToXlsx(stream, New GridViewDocumentExportOptions() With {
                    .ShowColumnHeaders = True,
                    .ShowColumnFooters = True,
                    .ShowGroupFooters = True
                })
            End Using

            'tableView.ExportToXls(sfn)
        End If
    End Sub
End Class
