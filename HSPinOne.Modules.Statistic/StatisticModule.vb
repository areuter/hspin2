﻿Imports HSPinOne.Infrastructure
Imports Prism.Ioc
Imports Prism.Modularity
Imports Prism.Regions

Public Class StatisticModule
    Implements IModule

    Public Sub RegisterTypes(containerRegistry As IContainerRegistry) Implements IModule.RegisterTypes

        containerRegistry.RegisterForNavigation(GetType(StatisticsView), "StatisticsView")
        containerRegistry.RegisterForNavigation(GetType(SqlView), "SqlView")
        containerRegistry.RegisterForNavigation(GetType(BillingsReportView), "BillingsReportView")

    End Sub

    Public Sub OnInitialized(containerProvider As IContainerProvider) Implements IModule.OnInitialized
        Dim rm = containerProvider.Resolve(Of IRegionManager)()
        rm.RegisterViewWithRegion(RegionNames.TaskRegion, GetType(StatisticsOutlookSectionneu))

    End Sub
End Class
