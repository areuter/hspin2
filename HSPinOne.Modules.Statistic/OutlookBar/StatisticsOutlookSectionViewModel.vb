﻿Imports System.ComponentModel.Composition
Imports HSPinOne.Common
Imports HSPinOne.Infrastructure
Imports Prism.Regions


<Export(GetType(StatisticsOutlookSectionViewModel)), PartCreationPolicy(CreationPolicy.Shared)>
Public Class StatisticsOutlookSectionViewModel


    Private _regionmanager As IRegionManager

    Private _Selected As ListBoxItem
    Public Property Selected() As ListBoxItem
        Get
            Return _Selected
        End Get
        Set(ByVal value As ListBoxItem)
            _Selected = value
            Navigate()
        End Set
    End Property



    Public Sub New()

        Me._regionmanager = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IRegionManager)
        'Me.TreeViewMenu = New StatisticsNavigation()

        Events.EventInstance.EventAggregator.GetEvent(Of OutlookSectionChangedEvent)().Subscribe(AddressOf DefaultNav)
    End Sub

    Private Sub Navigate()
        If Not IsNothing(Selected) Then


            If Not IsNothing(Selected.Tag) Then
                ViewNavigation.NavigateViews(CType(Selected.Tag, String), CType(_regionmanager, RegionManager))

            End If
        Else

        End If
    End Sub


    Public Sub DefaultNav(ByVal param As Object)
        If param.Name.ToLower = "statistik" Then

            Navigate()
            ViewNavigation.NavigateViews("StatisticsView", _regionmanager)

        End If
    End Sub
End Class
