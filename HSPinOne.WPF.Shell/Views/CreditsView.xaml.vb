﻿Public Class CreditsView


    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        'Version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString

        Dim ver As String = ""
        'Dim ver As String = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString
        Try
            Dim aver = Reflection.Assembly.GetExecutingAssembly().GetName().Version
            ver = aver.ToString()
#If DEBUG Then
            ver = "Debug Mode"
#End If
        Catch ex As Exception
            ver = "Unable to determine Version"
        End Try



        Me.VersionTextblock.Text = ver


    End Sub
End Class
