﻿Imports System.Deployment.Application

Public Class UpdateCheck

    Dim deployment As ApplicationDeployment



    Private Sub Update()


        If ApplicationDeployment.IsNetworkDeployed Then
            deployment = CType(ApplicationDeployment.CurrentDeployment, ApplicationDeployment)
            AddHandler deployment.UpdateProgressChanged, AddressOf UpdateProgressChanged
            AddHandler deployment.UpdateCompleted, AddressOf UpdateCompleted
            deployment.UpdateAsync()
        End If


    End Sub

    Private Sub UpdateProgressChanged(ByVal sender As Object, ByVal e As DeploymentProgressChangedEventArgs)
        Me.pgBar1.Value = e.ProgressPercentage
    End Sub

    Private Sub UpdateCompleted(ByVal sender As Object, ByVal e As ComponentModel.AsyncCompletedEventArgs)
        If IsNothing(e.Error) Then
            MsgBox("Das Update wurde erfolgreich durchgeführt. Bitte starten Sie die Anwendung jetzt neu!", , "Update erfolgreich")
            'Dim app = deployment.UpdateLocation.AbsoluteUri
            'Process.Start(app)

            App.Current.Shutdown()
        Else
            Me.txtMessage.Text = "Leider ist ein Fehler beim Update aufgetreten!"
        End If
    End Sub

    Private Sub btnExit_Click(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles btnExit.Click
        If Not IsNothing(deployment) Then deployment.UpdateAsyncCancel()
        App.Current.Shutdown()
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Update()
    End Sub
End Class
