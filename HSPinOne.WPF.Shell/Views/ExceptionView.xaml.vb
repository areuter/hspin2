﻿Imports System.Net
Imports System.IO
Imports System.Text
Imports System.Windows.Threading

Public Class ExceptionView


    Private _message As String
    Private _innerexception As String
    Private _stacktrace As String


    Public Sub New(ByVal sender As Object, ByVal e As DispatcherUnhandledExceptionEventArgs)

        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()

        ' Fügen Sie Initialisierungen nach dem InitializeComponent()-Aufruf hinzu.

        _message = e.Exception.Message.ToString

        Dim sb As New System.Text.StringBuilder()
        Dim ex = e.Exception.InnerException
        While ex IsNot Nothing
            sb.AppendLine("_______________InnerExeption:__________________")
            sb.AppendLine(ex.Message)
            sb.AppendLine(vbCrLf)
            ex = ex.InnerException

        End While
        _innerexception = sb.ToString
        _stacktrace = e.Exception.StackTrace.ToString

        Me.ErrorText.Text = _message
        Me.ErrorDetail.Text = _stacktrace

    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles Button2.Click
        'Senden



        'Wegen Proxy
        System.Net.ServicePointManager.Expect100Continue = False

        Dim request As System.Net.HttpWebRequest = WebRequest.Create("http://www.sport.uni-goettingen.de/in1/error.php")

        Dim myHost As String = System.Net.Dns.GetHostName
        Dim myusername As String = Environment.UserName

        Dim ver As String = ""
        'Dim ver As String = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString
        Try
            ver = System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString
        Catch ex As Exception
            ver = "Debug Mode"
        End Try


        Dim postData As String = "Message="
        postData = postData & "Absender: " & myHost & vbNewLine & "Windows-Benutzername: " & myusername & vbNewLine & "Version: " & ver & vbNewLine
        If Not IsNothing(HSPinOne.Infrastructure.MyGlobals.CurrentUsername) Then
            postData = postData & "Angemeldeter Benutzer: " & HSPinOne.Infrastructure.MyGlobals.CurrentUsername & vbNewLine
        End If
        postData = postData & _message
        postData = postData & vbNewLine & _innerexception & vbCrLf & "StackTrace===============" & vbCrLf & _stacktrace
        'postData = urlencode(postData)


        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)

        request.Method = "POST"

        request.ProtocolVersion = HttpVersion.Version10
        request.ContentType = "application/x-www-form-urlencoded"
        request.ContentLength = byteArray.Length

        Dim datastream As Stream = request.GetRequestStream

        datastream.Write(byteArray, 0, byteArray.Length)
        datastream.Close()

        Dim response As WebResponse = request.GetResponse()

        response.Close()


        Me.Close()
    End Sub
End Class
