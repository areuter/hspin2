﻿Imports System.ComponentModel.Composition
Imports System.ComponentModel
Imports HSPinOne.Infrastructure
Imports System.IO
Imports MahApps.Metro.Controls
'Imports System.Diagnostics.CodeAnalysis



Partial Public Class Shell1


    Public Sub New()

        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()

        ' Fügen Sie Initialisierungen nach dem InitializeComponent()-Aufruf hinzu.
        Me.DataContext = New ShellViewModel
        LoadSettings()
    End Sub


    Private Sub LoadSettings()
        'Versuche die Window Größen aus den User Settings zu laden
        Dim Top = My.Settings.WindowTop
        Dim Left = My.Settings.WindowLeft
        Dim Width = My.Settings.WindowWidth
        Dim Height = My.Settings.WindowHeight
        Dim WindowState = My.Settings.WindowState

        If Top + Height / 2 > SystemParameters.VirtualScreenHeight Then
            Top = SystemParameters.VirtualScreenHeight - Height
        End If

        If Left + Width / 2 > SystemParameters.VirtualScreenWidth Then
            Left = SystemParameters.VirtualScreenWidth - Width
        End If

        If Top < 0 Then
            Top = 0
        End If

        If Left < 0 Then
            Left = 0
        End If

        Me.Top = Top
        Me.Left = Left
        Me.Width = Width
        Me.Height = Height
        Me.WindowState = WindowState
    End Sub

    Private Sub SaveSettings()
        If Me.WindowState <> WindowState.Minimized Then
            My.Settings.WindowTop = Me.Top
            My.Settings.WindowLeft = Me.Left
            My.Settings.WindowWidth = Me.Width
            My.Settings.WindowHeight = Me.Height
            My.Settings.WindowState = Me.WindowState
            My.Settings.Save()
        End If

    End Sub


    Private Sub Shell_Closing(ByVal sender As Object, ByVal e As CancelEventArgs) Handles Me.Closing
        SaveSettings()
        If MyGlobals.LongRunningProcess = True Then
            If Not MsgBox("Im Hintergrund werden noch Mails versendet. Sind Sie sicher, dass Sie das Programm beenden möchten?", vbYesNoCancel + vbExclamation, "Beenden") = vbYes Then
                e.Cancel = True
            End If
        End If

        'Aufräumen der verweisten Dateien
        Try
            Dim filesstart = Path.GetTempPath() & "in1*"
            Dim dir As DirectoryInfo = New DirectoryInfo(Path.GetTempPath())
            Dim files As FileInfo() = dir.GetFiles
            For Each f As FileInfo In files
                If f.Name.StartsWith("in1") Then
                    Try
                        f.Delete()
                    Catch ex As Exception

                    End Try

                End If
            Next
        Catch ex As Exception

        End Try

    End Sub



    Private Sub CloseWindow_Click(sender As Object, e As RoutedEventArgs)
        Me.Close()
    End Sub
End Class
