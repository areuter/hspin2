﻿Imports HSPinOne.DAL
Imports System.Data.SqlClient

Public Class DbConnectionView




    Private Sub DbConnectionView_Loaded(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles Me.Loaded



    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles Button1.Click

        Dim conninfo As New HSPinOne.DAL.ConnectionInformation

        'Connstring generieren
        Dim sqlConnBuilder As New SqlConnectionStringBuilder()
        sqlConnBuilder.DataSource = Me.txtServer.Text
        sqlConnBuilder.InitialCatalog = Me.txtDatenbank.Text
        sqlConnBuilder.IntegratedSecurity = Me.chkIS.IsChecked
        sqlConnBuilder.MultipleActiveResultSets = True

        If Not sqlConnBuilder.IntegratedSecurity Then
            sqlConnBuilder.UserID = Me.txtUser.Text
            sqlConnBuilder.Password = Me.txtPass.Password
        End If


        If conninfo.CheckConnection(sqlConnBuilder.ToString) Then
            conninfo.SetConnectionString(Me.txtServer.Text, Me.txtUser.Text, Me.txtPass.Password, Me.txtDatenbank.Text, Me.chkIS.IsChecked)

            Me.DialogResult = True
            Me.Close()
        Else
            MsgBox("Datenbankverbindung kann nicht hergestellt werden", MsgBoxStyle.Critical, "Fehler")
        End If



    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub chkIS_Checked(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles chkIS.Checked
        Me.txtUser.IsEnabled = False
        Me.txtPass.IsEnabled = False
    End Sub

    Private Sub chkIS_Unchecked(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles chkIS.Unchecked
        Me.txtUser.IsEnabled = True
        Me.txtPass.IsEnabled = True
    End Sub
End Class
