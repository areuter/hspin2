﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Windows.Threading
Imports System.Globalization
Imports HSPinOne.DAL
Imports System.Windows.Markup
Imports System.IO
Imports System.Windows.Media.Animation
Imports System.Data.Entity
Imports System.Linq
Imports Prism.Unity
Imports Prism.Ioc
Imports Prism.Modularity
Imports System.Windows
Imports HSPinOne.Modules.Dashboard
Imports HSPinOne.Modules.Infopoint
Imports HSPinOne.Modules.Infopoint2
Imports HSPinOne.Modules.Verwaltung
Imports HSPinOne.Modules.Kalender
Imports HSPinOne.Modules.Settings
Imports HSPinOne.Modules.Bulkmail
Imports HSPinOne.Common
Imports HSPinOne.Modules.UserLogin
Imports HSPinOne.Infrastructure
Imports HSPinOne.Modules.Statistic
Imports Prism.Mvvm
Imports Prism.Regions

Partial Class App
    Inherits PrismApplication

    Const UPDATEURL = "http://db1.sport.uni-goettingen.de/hspinone/"

    ' Application-level events, such as Startup, Exit, and DispatcherUnhandledException
    ' can be handled in this file.

    Protected Overrides Sub RegisterTypes(containerRegistry As IContainerRegistry)
        'Throw New NotImplementedException()

    End Sub

    Protected Overrides Function CreateShell() As Window
        'Throw New NotImplementedException()
        'Return CommonServiceLocator.ServiceLocator.Current.GetInstance(Of Shell1)

        'Return Container.Resolve(Of Shell1)()
        Return Nothing

    End Function

    Private Sub StartShell()
        Dim appShell = Container.Resolve(Of Shell1)()
        appShell.Show()

        MainWindow = appShell

        'there lines was Not executed because of null Shell - so must duplicate here. Originally called from PrismApplicationBase.Initialize
        RegionManager.SetRegionManager(MainWindow, Container.Resolve(Of IRegionManager)())
        RegionManager.UpdateRegions()
        InitializeModules()
        MyBase.OnInitialized()
    End Sub

    Protected Overrides Sub InitializeShell(shell As Window)
        MyBase.InitializeShell(shell)
    End Sub



    Protected Overrides Sub ConfigureModuleCatalog(moduleCatalog As IModuleCatalog)

        moduleCatalog.AddModule(Of CommonModule)()
        moduleCatalog.AddModule(Of UserLoginModule)()
        moduleCatalog.AddModule(Of DashboardModule)()
        moduleCatalog.AddModule(Of VerwaltungModule)()
        moduleCatalog.AddModule(Of InfopointModule)()
        moduleCatalog.AddModule(Of Infopoint2Module)()
        moduleCatalog.AddModule(Of KalenderModule)()
        moduleCatalog.AddModule(Of SettingsModule)()
        moduleCatalog.AddModule(Of BulkmailModule)()
        moduleCatalog.AddModule(Of StatisticModule)()

        MyBase.ConfigureModuleCatalog(moduleCatalog)
    End Sub

    Protected Overrides Sub InitializeModules()

        Try
            MyBase.InitializeModules()

            'CheckforUpdate.RestoreSettings()

            'Check if User.Config is corrupt and restore it 
            CheckUserConfig()

            My.Settings.Reload()

            CheckMigrations()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub CheckMigrations()

        'Migrationen ausführen, wenn vorhanden
        Dim conninfo As New ConnectionInformation
        Try
            If conninfo.CheckConnection(ConnectionInformation.DecryptedConnString) Then
                Dim context As New in1Entities
                Dim query = (From c In context.GlobalSettings Select c.Einstellung)
                context.Dispose()
            End If
        Catch ex As Exception
            Throw ex
        End Try


    End Sub





    Private Sub Application_DispatcherUnhandledException(ByVal sender As Object, ByVal e As DispatcherUnhandledExceptionEventArgs) Handles Me.DispatcherUnhandledException
        e.Handled = True
        Dim view As New ExceptionView(sender, e)
        view.Topmost = True
        view.ShowDialog()
        view = Nothing

    End Sub


    Protected Overrides Sub OnStartup(ByVal e As StartupEventArgs)



        Telerik.Windows.Controls.StyleManager.ApplicationTheme = New Telerik.Windows.Controls.Office2016Theme()

        'Set Current OS culture (WPF normally uses en-US as Culture?
        FrameworkElement.LanguageProperty.OverrideMetadata(GetType(FrameworkElement), New FrameworkPropertyMetadata(XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)))

        ShowSplashWindow()
        MyBase.OnStartup(e)




        ViewModelLocationProvider.SetDefaultViewTypeToViewModelTypeResolver(Function(viewType)
                                                                                Dim viewName = viewType.FullName
                                                                                Dim viewAssemblyName = viewType.GetType().Assembly.FullName
                                                                                Dim viewModelName = String.Format(CultureInfo.InvariantCulture, "{0}ViewModel, {1}", viewName, viewAssemblyName)
                                                                                Return Type.GetType(viewModelName)
                                                                            End Function)

    End Sub


    Public Sub ShowSplashWindow()


        Dim splashwindow As New SplashWindow
        Dim tryagain As Boolean = True

        splashwindow.Show()
        Dim initializer As New Action(Sub()
                                          ' Simulation einer Initialisierung
                                          Try
                                              '  Initialisierung 
                                              Dim conninfo As New ConnectionInformation


                                              Do While tryagain
                                                  ' Auf Update prüfen
                                                  splashwindow.Dispatcher.Invoke(DispatcherPriority.Normal, New Action(Sub()
                                                                                                                           splashwindow.SetInfo("Suche nach Updates", 5)
                                                                                                                       End Sub))


                                                  'If CheckforUpdate.ClickonceUpdate Then
                                                  '    splashwindow.Dispatcher.Invoke(DispatcherPriority.Normal, New Action(Sub()
                                                  '                                                                             Dim win As New UpdateCheck
                                                  '                                                                             win.ShowDialog()
                                                  '                                                                         End Sub))

                                                  'End If

                                                  CheckforUpdate.SquirrelUpdate(UPDATEURL)

                                                  'System.Threading.Thread.Sleep(500)
                                                  splashwindow.Dispatcher.Invoke(DispatcherPriority.Normal, New Action(Sub()
                                                                                                                           splashwindow.SetInfo("Aufbau der Datenbankverbindung", 10)

                                                                                                                       End Sub))
                                                  If conninfo.CheckConnection(ConnectionInformation.DecryptedConnString) = True Then


                                                      splashwindow.Dispatcher.Invoke(DispatcherPriority.Normal, New Action(Sub()
                                                                                                                               splashwindow.SetInfo("Verbindung zur Datenbank erfolgreich", 80)

                                                                                                                           End Sub))
                                                      tryagain = False
                                                  Else

                                                      splashwindow.Dispatcher.Invoke(DispatcherPriority.Normal, New Action(Sub()
                                                                                                                               splashwindow.SetInfo("Verbindung zur Datenbank fehlgeschlagen", 10)
                                                                                                                               Dim frm As New DbConnectionView
                                                                                                                               If Not frm.ShowDialog Then
                                                                                                                                   'My.Application.Shutdown()
                                                                                                                                   Me.Shutdown()
                                                                                                                               End If

                                                                                                                           End Sub))



                                                  End If

                                              Loop

                                          Catch ex As Exception
                                              MessageBox.Show("Fehler beim Initialisieren: " + ex.Message, "Splash-Fenster", MessageBoxButton.OK, MessageBoxImage.[Error])
                                              'My.Application.Shutdown()
                                              Me.Shutdown()
                                          End Try

                                      End Sub)

        ' Delegat für den Callback der asynchronen Ausführung
        Dim splashCloser As New AsyncCallback(Function(result)



                                                  ' Das Hauptfenster erzeugen, der Anwendung zuweisen
                                                  ' und öffnen
                                                  Me.Dispatcher.Invoke(DispatcherPriority.Normal, New Action(Sub()
                                                                                                                 StartShell()

                                                                                                             End Sub))
                                                  ' Schließen des Splash-Fensters
                                                  splashwindow.Dispatcher.Invoke(DispatcherPriority.Normal, New Action(Sub()
                                                                                                                           splashwindow.Close()

                                                                                                                       End Sub))


                                                  Return True
                                              End Function)

        ' Asynchrones Starten der Initialisierung
        initializer.BeginInvoke(splashCloser, Nothing)

        'If Not IsNothing(ent) Then ent.Dispose()


    End Sub


    Private Sub CheckUserConfig()
        Dim isValid As Boolean = False
        Do While Not isValid
            Try
                Dim appSettings = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).AppSettings
                isValid = True
            Catch ex As ConfigurationErrorsException

                If (ex.Filename.EndsWith("user.config")) Then
                    File.Delete(ex.Filename)
                End If
            End Try
        Loop

    End Sub


End Class
