﻿'Imports System.ComponentModel.Composition.Hosting
Imports Prism.Regions
Imports Prism.Unity.PrismApplication
Imports System.Windows


Imports HSPinOne.Modules.Dashboard
Imports HSPinOne.Modules.Infopoint
Imports HSPinOne.Modules.Verwaltung
Imports HSPinOne.Modules.Kalender
Imports HSPinOne.Modules.Settings
Imports HSPinOne.Modules.Bulkmail
Imports HSPinOne.Common
Imports HSPinOne.Modules.UserLogin
Imports HSPinOne.Infrastructure
Imports HSPinOne.Modules.Statistic
Imports Prism.Unity
Imports CommonServiceLocator

Partial Public Class Bootstrapper







    Protected Overrides Sub InitializeShell()

        MyBase.InitializeShell()

        App.Current.MainWindow = CType(Me.Shell, Window)
        App.Current.MainWindow.Show()

    End Sub

    Protected Overrides Sub ConfigureModuleCatalog()

        MyBase.ConfigureModuleCatalog()
    End Sub

    '    Protected Overrides Sub ConfigureContainer()

    '        MyBase.ConfigureContainer()
    '    End Sub

    '    Protected Overrides Sub ConfigureModuleCatalog()

    '    End Sub


    '    Protected Overrides Sub ConfigureAggregateCatalog()
    '        Me.AggregateCatalog.Catalogs.Add(New AssemblyCatalog(GetType(Bootstrapper).Assembly))
    '        Me.AggregateCatalog.Catalogs.Add(New AssemblyCatalog(GetType(HSPinOneCommands).Assembly))
    '        Me.AggregateCatalog.Catalogs.Add(New AssemblyCatalog(GetType(CommonModule).Assembly))
    '        Me.AggregateCatalog.Catalogs.Add(New AssemblyCatalog(GetType(UserLoginModule).Assembly))
    '        Me.AggregateCatalog.Catalogs.Add(New AssemblyCatalog(GetType(DashboardModule).Assembly))
    '        Me.AggregateCatalog.Catalogs.Add(New AssemblyCatalog(GetType(VerwaltungModule).Assembly))
    '        Me.AggregateCatalog.Catalogs.Add(New AssemblyCatalog(GetType(InfopointModule).Assembly))
    '        Me.AggregateCatalog.Catalogs.Add(New AssemblyCatalog(GetType(KalenderModule).Assembly))
    '        Me.AggregateCatalog.Catalogs.Add(New AssemblyCatalog(GetType(SettingsModule).Assembly))
    '        Me.AggregateCatalog.Catalogs.Add(New AssemblyCatalog(GetType(BulkmailModule).Assembly))
    '        'Me.AggregateCatalog.Catalogs.Add(New AssemblyCatalog(GetType(StatisticsModule).Assembly))
    '        Me.AggregateCatalog.Catalogs.Add(New AssemblyCatalog(GetType(StatisticModule).Assembly))

    '#If DEBUG Then
    '        Console.WriteLine()
    '#End If

    '    End Sub


    'Protected Overrides Function ConfigureDefaultRegionBehaviors() As IRegionBehaviorFactory
    '    Dim factory = MyBase.ConfigureDefaultRegionBehaviors()

    '    factory.AddIfMissing("AutoPopulateExportedViewsBehavior", GetType(AutoPopulateExportedViewsBehavior))

    '    Return factory
    'End Function

    'Protected Overrides Function ConfigureRegionAdapterMappings() As Prism.Regions.RegionAdapterMappings

    '    Dim mappings = MyBase.ConfigureRegionAdapterMappings()
    '    If IsNothing(mappings) Then Return Nothing

    '    Return mappings

    'End Function

End Class
