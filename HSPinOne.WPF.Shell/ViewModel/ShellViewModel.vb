﻿Imports System.ComponentModel.Composition
Imports Prism.Regions
Imports HSPinOne.Infrastructure
Imports System.Collections.ObjectModel
Imports System.ComponentModel
Imports HSPinOne.DAL
Imports System.Threading
Imports HSPinOne.UIControls
Imports System.Linq
Imports CommonServiceLocator

<Export()>
Public Class ShellViewModel
    Inherits Infrastructure.ViewModelBase
    Implements INavigationAware






    Private _selectedsection As TabItem = New TabItem
    Public Property SelectedSection As TabItem
        Get
            Return _selectedsection
        End Get
        Set(ByVal value As TabItem)
            If Not IsNothing(value) Then
                If value.Name <> _selectedsection.Name Then
                    _selectedsection = value
                    Events.EventInstance.EventAggregator.GetEvent(Of OutlookSectionChangedEvent)().Publish(SelectedSection)
                End If
            End If

        End Set
    End Property



    'Private _selectedsection As RadOutlookBarItem = New RadOutlookBarItem
    'Public Property SelectedSection As RadOutlookBarItem
    '    Get
    '        Return _selectedsection
    '    End Get
    '    Set(ByVal value As RadOutlookBarItem)
    '        If Not IsNothing(value) Then
    '            If value.Header <> _selectedsection.Header Then
    '                _selectedsection = value
    '                Events.EVAG.GetEvent(Of OutlookSectionChangedEvent)().Publish(SelectedSection)
    '            End If
    '        End If

    '    End Set
    'End Property

    Private _username As String
    Public Property Username As String
        Get
            Return _username
        End Get
        Set(value As String)
            _username = value
            RaisePropertyChanged("Username")
        End Set
    End Property

    Private _locked As Boolean
    Public Property Locked As Boolean
        Get
            Return _locked
        End Get
        Set(value As Boolean)
            _locked = value
            RaisePropertyChanged("Locked")
        End Set
    End Property

    Private _Db As String
    Public Property Db As String
        Get
            Return _Db
        End Get
        Set(value As String)
            _Db = value
            RaisePropertyChanged("Db")
        End Set
    End Property

    Private _Institution As String
    Public Property Institution As String
        Get
            Return _Institution
        End Get
        Set(value As String)
            _Institution = value
            RaisePropertyChanged("Institution")
        End Set
    End Property

    Private _PopupSettingsOpen As Boolean = False
    Public Property PopupSettingsOpen As Boolean
        Get
            Return _PopupSettingsOpen
        End Get
        Set(value As Boolean)
            _PopupSettingsOpen = value
            RaisePropertyChanged("PopupSettingsOpen")
        End Set
    End Property

    Private _PopupUserOpen As Boolean = False
    Public Property PopupUserOpen As Boolean
        Get
            Return _PopupUserOpen
        End Get
        Set(value As Boolean)
            _PopupUserOpen = value
            RaisePropertyChanged("PopupUserOpen")
        End Set
    End Property

    Public ReadOnly Property CurrentRoleManager As RoleManager
        Get
            Return App.Current.FindResource("roleManager")
        End Get
    End Property

    Private _Zeit As DateTime = Now
    Public Property Zeit As DateTime
        Get
            Return _Zeit
        End Get
        Set(value As DateTime)
            _Zeit = value
            RaisePropertyChanged("Zeit")
        End Set
    End Property

    Private m_items As New ObservableCollection(Of OutlookTabItem)()

    Public Property Items() As ObservableCollection(Of OutlookTabItem)
        Get
            Return m_items
        End Get
        Set(ByVal value As ObservableCollection(Of OutlookTabItem))
            m_items = value
        End Set
    End Property

    Private _UserMessage As String
    Public Property UserMessage() As String
        Get
            Return _UserMessage
        End Get
        Set(ByVal value As String)
            _UserMessage = value
            RaisePropertyChanged("UserMessage")
        End Set
    End Property

    Private _ShowMessagebox As System.Windows.Visibility = Visibility.Collapsed
    Public Property ShowMessagebox() As System.Windows.Visibility
        Get
            Return _ShowMessagebox
        End Get
        Set(ByVal value As System.Windows.Visibility)
            _ShowMessagebox = value
            RaisePropertyChanged("ShowMessagebox")
        End Set
    End Property

    Private _dismissed As Boolean = False


    Private _Version As String
    Public Property Version() As String
        Get
            Return _Version
        End Get
        Set(ByVal value As String)
            _Version = value
            RaisePropertyChanged("Version")
        End Set
    End Property



    Public Property SettingsCommand As ICommand
    Public Property LogoutCommand As ICommand
    Public Property ShutdownCommand As ICommand
    Public Property ConnectionCommand As ICommand
    Public Property EmailCommand As ICommand
    Public Property InternetCommand As ICommand
    Public Property WordCommand As ICommand
    Public Property ExplorerCommand As ICommand
    Public Property CalculatorCommand As ICommand
    Public Property HelpCommand As ICommand
    Public Property TicketCommand As ICommand
    Public Property CreditsCommand As ICommand

    Public Property DismissCommand As ICommand

    Public Property PopupSettingsCommand As ICommand
    Public Property PopupUserCommand As ICommand

    Public Property PasswordCommand As ICommand





    Private WithEvents bgw As BackgroundWorker

    Public Sub New()


        SettingsCommand = New RelayCommand(AddressOf Settings)
        LogoutCommand = New RelayCommand(AddressOf LogoutAction)
        ShutdownCommand = New RelayCommand(AddressOf ShutdownAction)
        ConnectionCommand = New RelayCommand(AddressOf ConnectionAction)

        HelpCommand = New RelayCommand(AddressOf Help)
        'TicketCommand = New RelayCommand(AddressOf Ticket)
        CreditsCommand = New RelayCommand(AddressOf Credits)
        PopupSettingsCommand = New RelayCommand(AddressOf PopupSettings)
        PopupUserCommand = New RelayCommand(AddressOf PopupUser)
        DismissCommand = New RelayCommand(AddressOf DismissAction)
        PasswordCommand = New RelayCommand(AddressOf PasswordAction)

        'Items = New ObservableCollection(Of OutlookTabItem)()
        'Dim item = New OutlookTabItem
        'item.Header = "Automatic"
        'item.Tag = "Tag"
        'item.TagName = "TagName"
        'Me.Items.Add(item)

        Events.EventInstance.EventAggregator.GetEvent(Of LoginEvent).Subscribe(AddressOf LoginAction)

        Db = ConnectionInformation.GetDatabase

        Dim check As New StartBackgroundTasks
        check.Execute()
        check = Nothing

        Dim gs = GlobalSettingService.HoleEinstellung("hspname", True)
        If Trim(gs) = "" Then
            Institution = "HSPinOne"
        Else
            Institution = gs
        End If


        ' Regelmäßige Aufgaben (Uhrzeit, NotificationCheck)
        bgw = New BackgroundWorker
        If Not bgw.IsBusy Then
            bgw.WorkerReportsProgress = True
            bgw.RunWorkerAsync()
        End If

        Version = In1Version.GetVersion()
    End Sub

    Private Sub PopupUser()
        PopupUserOpen = True
    End Sub
    Private Sub PopupSettings()
        PopupSettingsOpen = True
    End Sub

    Private Sub CheckNotifications()
        Using con As New in1Entities
            Dim query = From c In con.Users Where c.UserName = Me.Username Select c


            If Not IsNothing(query) Then

                If query.Count = 0 Then
                    Return
                End If

                If _dismissed = False Then
                    If query.First.Dismissed = False Then
                        Me.ShowMessagebox = System.Windows.Visibility.Visible
                        Me.UserMessage = query.FirstOrDefault.Message
                    End If
                Else
                    query.First.Dismissed = True
                    con.SaveChanges()
                    _dismissed = False
                End If
            End If


        End Using
    End Sub

    Private Sub DismissAction()

        Me.ShowMessagebox = System.Windows.Visibility.Collapsed
        _dismissed = True

    End Sub


    Private Sub Credits()

        Dim uc As New CreditsView

        Dim win As New WindowController
        win.ShowWindow(uc, True, ResizeMode.CanResize, 800, 600, "Credits")
    End Sub


    Private Sub Settings()

        Dim settingsservice = ServiceLocator.Current.GetInstance(Of ISettingsService)()
        settingsservice.GetSettings()
        settingsservice = Nothing

    End Sub

    Private Sub LoginAction()
        Me.Username = MyGlobals.CurrentUsername
        Me.Locked = MyGlobals.LoggedOn
    End Sub

    Private Sub LogoutAction()

        PopupUserOpen = False
        MyGlobals.CurrentUsername = ""
        MyGlobals.CurrentUser = Nothing
        MyGlobals.LoggedOn = False
        Thread.CurrentPrincipal = Nothing
        CurrentRoleManager.ApplyRules()

        Events.EventInstance.EventAggregator.GetEvent(Of LoginEvent).Publish(Nothing)
        Dim rg = ServiceLocator.Current.GetInstance(Of IRegionManager)()
        Dim uri = New Uri("UserLoginView", UriKind.Relative)
        rg.Regions(RegionNames.MainRegion).RequestNavigate(uri)


    End Sub


    Private Sub PasswordAction()

        Dim uc As New PasswordView

        Dim win As New WindowController
        win.ShowWindow(uc, True, ResizeMode.CanResize, 800, 600, "Passwort ändern")
    End Sub



    Private Sub ShutdownAction()
        'My.Application.Shutdown()
        App.Current.Shutdown()
    End Sub

    Private Sub ConnectionAction()
        Dim vw As New DbConnectionView
        If vw.ShowDialog() = True Then
            MessageBox.Show("Die Änderung erfolgt erst nach einem Neustart der Anwendung. Bitte starten Sie die Anwendung neu!", "Neustart")
            System.Windows.Application.Current.Shutdown()
        End If
    End Sub



    Private Sub Help()
        Process.Start("http://hspinone.sport.uni-goettingen.de/")
    End Sub




    Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw.DoWork
        Dim _cnt As Integer = 0
        While True

            If _cnt > 10 Then _cnt = 0
            If _cnt = 0 Then
                CheckNotifications()
            End If
            bgw.ReportProgress(1)
            _cnt = _cnt + 1
            Thread.Sleep(5000)
        End While
    End Sub

    Private Sub bgw_ProgressChanged(ByVal sender As Object, ByVal e As ProgressChangedEventArgs) Handles bgw.ProgressChanged
        Me.Zeit = DateTime.Now
    End Sub

End Class

