﻿Imports HSPinOne.Infrastructure
Imports HSPinOne.DAL

Public Class SplashWindowViewModel
    Inherits ViewModelBase

    Private _version As String
    Public Property Version As String
        Get
            Return _version
        End Get
        Set(ByVal value As String)
            _version = value
            RaisePropertyChanged("Version")
        End Set
    End Property

    Private _message As String = "Initializing"
    Public Property Message As String
        Get
            Return _message
        End Get
        Set(ByVal value As String)
            _message = value
            RaisePropertyChanged("Message")
        End Set
    End Property

    Private _updateUrl As String

    Public Sub New(ByVal updateUrl As String)
        _updateUrl = updateUrl
        Dim ver As String
        Try
            ver = In1Version.GetVersion()
        Catch ex As Exception
            ver = "Unable to determine version"
            Version = ver
        End Try


        IsBusy = True


    End Sub

    Public Sub Init()
        Dim tryagain As Boolean = True
        Try
            '  Initialisierung 
            Dim conninfo As New HSPinOne.DAL.ConnectionInformation


            Do While tryagain
                ' Auf Update prüfen

                Message = "Suche nach Updates"

                CheckforUpdate.SquirrelUpdate(_updateUrl)

                Message = "Aufbau der Datenbankverbindung"

                If conninfo.CheckConnection(ConnectionInformation.DecryptedConnString) = True Then
                    Message = "Verbindung zur Datenbank erfolgreich"
                    tryagain = False
                Else

                    Message = "Verbindung zur Datenbank fehlgeschlagen"
                    Dim frm As New DbConnectionView
                    If Not frm.ShowDialog Then
                        'My.Application.Shutdown()
                        Application.Current.Shutdown()
                        'Me.Shutdown()
                    End If

                End If

            Loop

        Catch ex As Exception
            MessageBox.Show("Fehler beim Initialisieren: " + ex.Message, "Splash-Fenster", MessageBoxButton.OK, MessageBoxImage.[Error])
            Application.Current.Shutdown()
            'Me.Shutdown()
        End Try
    End Sub
End Class
