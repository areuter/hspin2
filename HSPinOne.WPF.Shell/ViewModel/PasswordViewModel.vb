﻿Imports System.Linq
Imports CommonServiceLocator
Imports HSPinOne.Infrastructure


Public Class PasswordViewModel
    Inherits ViewModelBase

    Private _OldPassword As String = String.Empty
    Public Property OldPassword() As String
        Get
            Return _OldPassword
        End Get
        Set(ByVal value As String)
            _OldPassword = value
            RaisePropertyChanged("OldPassword")
        End Set
    End Property

    Private _NewPassword As String = String.Empty
    Public Property NewPassword() As String
        Get
            Return _NewPassword
        End Get
        Set(ByVal value As String)
            _NewPassword = value
            RaisePropertyChanged("NewPassword")
        End Set
    End Property

    Private _NewPassword1 As String = String.Empty
    Public Property NewPassword1() As String
        Get
            Return _NewPassword1
        End Get
        Set(ByVal value As String)
            _NewPassword1 = value
            RaisePropertyChanged("NewPassword1")
        End Set
    End Property

    Private _Message As String = "Mindestens 7 Zeichen"
    Public Property Message() As String
        Get
            Return _Message
        End Get
        Set(ByVal value As String)
            _Message = value
            RaisePropertyChanged("Message")
        End Set
    End Property

    Private _Show As System.Windows.Visibility = Visibility.Visible
    Public Property Show() As System.Windows.Visibility
        Get
            Return _Show
        End Get
        Set(ByVal value As System.Windows.Visibility)
            _Show = value
            RaisePropertyChanged("Show")
        End Set
    End Property


    Public Property ChangePasswordCommand As ICommand

    Private _userAccess As IUserDataService

    Public Sub New()
        ChangePasswordCommand = New RelayCommand(AddressOf ChangePassword, AddressOf CanChangePassword)
        _userAccess = ServiceLocator.Current.GetInstance(Of IUserDataService)()
    End Sub

    Private Sub ChangePassword()
        Dim login = MyGlobals.CurrentUser.LoginID

        Using _con As New HSPinOne.DAL.in1Entities
            Dim user = (From c In _con.Users Where c.LoginID = login Select c).FirstOrDefault


            If Not IsNothing(user) AndAlso Crypto.Verify(OldPassword, user.Password, "SHA1", user.SaltValue) Then

                Dim salt As Byte()
                salt = GenSalt()
                user.SaltValue = Convert.ToBase64String(salt)
                user.Password = GenHash(NewPassword, salt)

                _con.SaveChanges()
                Message = "Das Passwort wurde geändert"
                Show = Visibility.Visible

            Else
                Message = "Das alte Passwort ist nicht korrekt"
                Show = Visibility.Visible
            End If
        End Using
    End Sub

    Private Function CanChangePassword(ByVal param As Object) As Boolean


        Return Validate()


        Return False
    End Function

    Private Function Validate() As Boolean

        If Not IsNothing(NewPassword) AndAlso String.Compare(NewPassword, NewPassword1, False) = 0 AndAlso NewPassword.Length > 7 Then

            Return True
        End If
        Return False
    End Function

    Public Function GenHash(ByVal LoginPassword As String, ByVal salt As Byte())
        Try

            Return Crypto.ComputeHash(LoginPassword, "SHA1", salt)

        Catch ex As Exception
            Debug.Print("Exception")
            Throw New Exception(ex.Message, ex.InnerException)
        End Try
    End Function

    Public Function GenSalt()
        Return Crypto.generateRadomSaltvalue()
    End Function

End Class
