﻿Imports System.Configuration
Imports System.Deployment.Application
Imports System.Reflection
Imports System.Threading.Tasks
Imports Squirrel
Imports System.IO

Public Class CheckforUpdate

    Public Shared Function ClickonceUpdate() As Boolean
        If ApplicationDeployment.IsNetworkDeployed Then
            Dim deployment = CType(ApplicationDeployment.CurrentDeployment, ApplicationDeployment)
            'AddHandler deployment.CheckForUpdateCompleted, AddressOf CheckforUpdate_Completed

            Try
                Return deployment.CheckForUpdate()

            Catch ex As InvalidOperationException

            End Try
        End If
        Return False
    End Function



    Public Shared Async Sub SquirrelUpdate(ByVal url As String)
        Try


            Using mgr As New UpdateManager(url)
                Await mgr.UpdateApp
                'BackupSettings()
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Public Shared Sub BackupSettings()
        Dim settingsFile As String = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).FilePath
        Dim destination As String = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\..\last.config"
        If File.Exists(settingsFile) Then
            IO.File.Copy(settingsFile, destination, True)
        End If
    End Sub

    Public Shared Sub RestoreSettings()
        Dim destFile As String = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).FilePath
        Dim sourceFile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) & "\..\last.config"

        If Not File.Exists(sourceFile) Then
            Return
        End If

        Try
            Directory.CreateDirectory(Path.GetDirectoryName(destFile))
        Catch __unusedException1__ As Exception
        End Try

        Try
            File.Copy(sourceFile, destFile, True)
        Catch __unusedException1__ As Exception
        End Try

        Try
            File.Delete(sourceFile)
        Catch __unusedException1__ As Exception
        End Try
    End Sub


End Class
