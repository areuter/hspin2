﻿
Public Class SplashWindow


    Public Sub New()

        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()

        Dim ver As String = ""
        'Dim ver As String = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString
        Try
            Dim aver = Reflection.Assembly.GetExecutingAssembly().GetName().Version
            ver = aver.ToString()
#If DEBUG Then
            ver = "Debug Mode"
#End If
        Catch ex As Exception
            ver = "Unable to determine Version"
        End Try
        Me.VersionTextBlock.Text = ver

    End Sub

    Public Sub SetInfo(ByVal message As String, ByVal prozent As Integer)
        Me.txtInfo.Text = message
    End Sub
End Class
