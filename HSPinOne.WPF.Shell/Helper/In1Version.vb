﻿Public Class In1Version


    Public Shared Function GetVersion()

        Dim ver = ""
#If DEBUG Then
        ver = "Debug Mode: "
#End If
        Try
            Dim aver = Reflection.Assembly.GetExecutingAssembly().GetName().Version
            ver = ver & aver.ToString()
        Catch ex As Exception
            ver = "Error getting Version"
        End Try

        Return ver
    End Function

End Class
