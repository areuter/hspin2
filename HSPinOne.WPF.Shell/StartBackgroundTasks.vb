﻿Imports System.ComponentModel
Imports HSPinOne.Infrastructure
Imports HSPinOne.DAL
Imports System.Linq

Public Class StartBackgroundTasks


    Private WithEvents bgw As BackgroundWorker

    Private message As String = ""

    Private _mc As List(Of MachineConfig)

    Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw.DoWork

        ' 1. Hardware Hash generieren
        Dim fp As String = ""
        fp = HardwareHash.GetFingerprint

        ' Version abrufen
        Dim ver As String = "NN"
        Dim aver = Reflection.Assembly.GetExecutingAssembly().GetName().Version
        ver = aver.ToString()


        ' Einstellungsmöglichkeiten checken und bei Bedarf einfügen
        Dim defs = MachineSettingService.in1MachineDefaults
        ' Version einfügen
        defs.Add("Version", ver)

        Using con As New in1Entities
            Dim machinconfigs = con.MachineConfigs.Where(Function(o) o.Machinename = fp)
            For Each itm In defs
                Dim el = itm
                Dim ex = machinconfigs.Where(Function(o) o.Configname = el.Key And o.Machinename = fp)
                If Not ex.Any Then
                    'Defaults setzen
                    Dim mc As New MachineConfig
                    mc.Machinename = fp
                    mc.Configname = itm.Key
                    mc.Machinesetting = el.Value
                    con.MachineConfigs.Add(mc)
                Else
                    ' Set version
                    If el.Key = "Version" Then
                        Dim conf = ex.First
                        conf.Machinesetting = el.Value
                    End If
                End If
            Next
            con.SaveChanges()
            _mc = con.MachineConfigs.Where(Function(o) o.Machinename = fp).ToList
        End Using


        e.Result = fp


        ' Globale Einstellungen überprüfen und ggf. setzen
        Dim svc As New GlobalSettingService()
        Dim setts = svc.GetDefaults()
        For Each itm In setts
            Dim check = GlobalSettingService.HoleEinstellung(itm.Einstellung, True)
            If IsNothing(check) Then
                GlobalSettingService.SchreibeEinstellung(itm.Einstellung, itm.Wert, itm.Bezeichnung, itm.Beschreibung, itm.Typ)
            End If
        Next

        ' Durchlaufende Posten Konto prüfen
        Using con As New in1Entities
            Dim query = con.Accounts.Where(Function(o) o.AccountID = 1370).FirstOrDefault()
            If IsNothing(query) Then
                Dim acc As New Account With {.AccountID = 1370, .AccountName = "Durchlaufende Posten", .Kasse = False}
                con.Accounts.Add(acc)
                con.SaveChanges()
            End If
        End Using

    End Sub
    Private Sub bgw_Completed(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles bgw.RunWorkerCompleted
        MyGlobals.Fingerprint = e.Result  ' HardwareHash.GetFingerprint
        MyGlobals.MachineSettings = _mc
        If Not Trim(message) = "" Then
            Dim dlg As New DialogService
            dlg.ShowError("Problem", "Folgende Einstellungen sind nicht gesetzt: " & message & " Bitte legen Sie für die ordnungsgemäße Funktion der Software diese Einstellungen fest")
            dlg = Nothing
        End If
    End Sub





    Public Sub Execute()
        'Background Init
        bgw = New BackgroundWorker
        If Not bgw.IsBusy Then bgw.RunWorkerAsync()
    End Sub

End Class
