﻿

Imports Prism.Modularity
Imports Prism.Ioc
Imports Prism.Regions
Imports HSPinOne.Infrastructure

Public Class InfopointModule
    Implements IModule

    Public Sub RegisterTypes(containerRegistry As IContainerRegistry) Implements IModule.RegisterTypes
        'Throw New NotImplementedException()
        containerRegistry.RegisterForNavigation(GetType(Infopoint1View), "Infopoint1View")
        containerRegistry.RegisterForNavigation(GetType(CheckinView), "CheckinView")
        containerRegistry.RegisterForNavigation(GetType(CashReportView), "CashReportView")
        containerRegistry.RegisterForNavigation(GetType(CheckinVerkaufView), "CheckinVerkaufView")
        containerRegistry.RegisterForNavigation(GetType(CheckinCustomerView), "CheckinCustomerView")
        containerRegistry.RegisterForNavigation(GetType(CashZBonView), "CashZBonView")
        containerRegistry.RegisterForNavigation(GetType(CashClosingsView), "CashClosingsView")


    End Sub

    Public Sub OnInitialized(containerProvider As IContainerProvider) Implements IModule.OnInitialized
        Dim rm = containerProvider.Resolve(Of IRegionManager)()
        rm.RegisterViewWithRegion(RegionNames.TaskRegion, GetType(InfopointOutlookSection))
    End Sub
End Class
