﻿Imports System.ComponentModel.Composition
Imports Prism.Regions
Imports System.Windows.Input
Imports Prism.Events
Imports HSPinOne.Infrastructure

Public Class CheckinView
    Implements INavigationAware

    Private _ea As IEventAggregator

    Public Sub New()

        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()

        ' Fügen Sie Initialisierungen nach dem InitializeComponent()-Aufruf hinzu.
        Me.DataContext = New ViewModel.CheckinViewModel()

        _ea = Events.EventInstance.EventAggregator

        ' This call is required by the designer.
        InitializeComponent()



        _ea.GetEvent(Of CheckinFocusEvent).Subscribe(AddressOf SetFocus)

    End Sub

    Public Function IsNavigationTarget(ByVal navigationContext As Prism.Regions.NavigationContext) As Boolean Implements Prism.Regions.INavigationAware.IsNavigationTarget

        Return True
    End Function

    Public Sub OnNavigatedFrom(ByVal navigationContext As Prism.Regions.NavigationContext) Implements Prism.Regions.INavigationAware.OnNavigatedFrom

    End Sub

    Public Sub OnNavigatedTo(ByVal navigationContext As Prism.Regions.NavigationContext) Implements Prism.Regions.INavigationAware.OnNavigatedTo

        Keyboard.Focus(Me.KeyTextbox)


    End Sub

    Private Sub SetFocus()
        Keyboard.Focus(Me.KeyTextbox)
    End Sub




    Private Sub CheckinView_Loaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles MyBase.Loaded
        Keyboard.Focus(Me.KeyTextbox)
    End Sub
End Class
