﻿Imports Prism.Regions
Imports System.ComponentModel.Composition



Public Class Infopoint1View
    Implements IRegionMemberLifetime




    Public ReadOnly Property KeepAlive As Boolean Implements IRegionMemberLifetime.KeepAlive
        Get
            Return False
        End Get
    End Property



    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.DataContext = New ViewModel.Infopoint1ViewModel()




    End Sub







End Class
