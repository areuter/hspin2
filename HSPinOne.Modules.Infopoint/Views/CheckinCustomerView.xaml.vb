﻿Imports System.ComponentModel.Composition
Imports Prism.Regions
Imports System.Windows.Input



Public Class CheckinCustomerView
    Implements INavigationAware




    Public Function IsNavigationTarget(navigationContext As Prism.Regions.NavigationContext) As Boolean Implements Prism.Regions.INavigationAware.IsNavigationTarget
        Keyboard.Focus(Me.KeyTextbox)
        Return True
    End Function

    Public Sub OnNavigatedFrom(navigationContext As Prism.Regions.NavigationContext) Implements Prism.Regions.INavigationAware.OnNavigatedFrom

    End Sub

    Public Sub OnNavigatedTo(navigationContext As Prism.Regions.NavigationContext) Implements Prism.Regions.INavigationAware.OnNavigatedTo

    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.DataContext = New ViewModel.CheckinCustomerViewModel()
    End Sub

    Private Sub UserControl_Loaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles MyBase.Loaded
        Keyboard.Focus(Me.KeyTextbox)
    End Sub
End Class
