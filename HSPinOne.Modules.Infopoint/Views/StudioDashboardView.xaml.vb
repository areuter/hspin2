﻿Imports System.ComponentModel.Composition
Imports Prism.Regions


<Export("StudioDashboardView"), PartCreationPolicy(CreationPolicy.NonShared)> _
Public Class StudioDashboardView
    Implements INavigationAware

    <Import()> _
    Public WriteOnly Property ViewModel As ViewModel.StudioDashboardViewModel
        Set(ByVal value As ViewModel.StudioDashboardViewModel)
            Me.DataContext = value
            'RibbonActivator()
        End Set
    End Property

    Public Function IsNavigationTarget(ByVal navigationContext As Prism.Regions.NavigationContext) As Boolean Implements Prism.Regions.INavigationAware.IsNavigationTarget
        Return True
    End Function

    Public Sub OnNavigatedFrom(ByVal navigationContext As Prism.Regions.NavigationContext) Implements Prism.Regions.INavigationAware.OnNavigatedFrom

    End Sub

    Public Sub OnNavigatedTo(ByVal navigationContext As Prism.Regions.NavigationContext) Implements Prism.Regions.INavigationAware.OnNavigatedTo

    End Sub
End Class
