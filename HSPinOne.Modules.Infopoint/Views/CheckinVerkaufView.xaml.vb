﻿Imports System.ComponentModel.Composition
Imports Prism.Regions


Public Class CheckinVerkaufView
    Implements IRegionMemberLifetime

    Public Sub New()

        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()

        ' Fügen Sie Initialisierungen nach dem InitializeComponent()-Aufruf hinzu.
        Me.DataContext = New ViewModel.CheckinVerkaufViewModel()
    End Sub

    Public ReadOnly Property KeepAlive As Boolean Implements IRegionMemberLifetime.KeepAlive
        Get
            Return True
        End Get
    End Property
End Class
