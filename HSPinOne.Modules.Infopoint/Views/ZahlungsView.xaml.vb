﻿Imports HSPinOne.DAL
Imports System.Data.Entity
Imports HSPinOne.Infrastructure
Imports System.Windows.Input


Public Class Zahlungsview

    Private _betrag As Decimal
    Private _gegeben As Decimal
    Private _zahlungsart As Integer
    Private _bondruck As Boolean
    Private _lsvenabled As Boolean









    Public Property Betrag As Decimal
        Get
            Return _betrag
        End Get
        Set(value As Decimal)
            _betrag = value
        End Set
    End Property

    Public Property Gegeben As Decimal
        Get
            Return _gegeben
        End Get
        Set(value As Decimal)
            _gegeben = value
        End Set
    End Property

    Public Property Zahlungsart As Integer
        Get
            Return _zahlungsart
        End Get
        Set(value As Integer)
            _zahlungsart = value
        End Set
    End Property

    Public Property Bondruck As Boolean
        Get
            Return _bondruck
        End Get
        Set(value As Boolean)
            _bondruck = value
        End Set
    End Property

    Public Property LSVEnabled As Boolean
        Get
            Return _lsvenabled

        End Get
        Set(value As Boolean)
            _lsvenabled = value
        End Set
    End Property


    Private _account As Integer
    Public Property Account As Integer
        Get
            Return _account
        End Get
        Set(value As Integer)
            _account = value
        End Set
    End Property


    Private _accounts As IEnumerable(Of Account)
    Public Property Accounts As IEnumerable(Of Account)
        Get
            Return _accounts
        End Get
        Set(value As IEnumerable(Of Account))
            _accounts = value
        End Set
    End Property


    Public Property PayCommand As ICommand


    Public Sub New(ByVal decbetrag As Decimal, ByVal LSV As Boolean)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Betrag = decbetrag
        Gegeben = Betrag

        LSVEnabled = LSV

        PayCommand = New RelayCommand(AddressOf Pay)


        Dim _context As New in1Entities
        _context.Accounts.Where(Function(o) o.Kasse = True).Load()
        Accounts = _context.Accounts.Local


        Me.DataContext = Me

    End Sub

    Private Sub Pay(ByVal param As Object)

        Account = CLng(param)

        Me.DialogResult = True
        Me.Close()
    End Sub
End Class
