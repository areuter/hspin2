﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Input


Namespace ViewModel


    Public Class TagesgastViewModel
        Inherits ViewModelBase

        Public Event RequestClose As EventHandler

        Private _Vorname As String = ""
        Public Property Vorname As String
            Get
                Return _Vorname
            End Get
            Set(value As String)
                _Vorname = value
            End Set
        End Property

        Private _Schluessel As String = ""
        Public Property Schluessel As String
            Get
                Return _Schluessel
            End Get
            Set(value As String)
                _Schluessel = value
                DuplicateKey()
            End Set
        End Property

        Private _CustomerStateLookup As List(Of CustomerState)
        Public Property CustomerStateLookup As List(Of CustomerState)
            Get
                Return _CustomerStateLookup
            End Get
            Set(value As List(Of CustomerState))
                _CustomerStateLookup = value

            End Set
        End Property

        Private _SelectedCustomerState As CustomerState
        Public Property SelectedCustomerState As CustomerState
            Get
                Return _SelectedCustomerState
            End Get
            Set(value As CustomerState)
                _SelectedCustomerState = value
                RaisePropertyChanged("CustomerState")
            End Set
        End Property

        Private _Duplikat As System.Windows.Visibility = System.Windows.Visibility.Hidden
        Public Property Duplikat() As System.Windows.Visibility
            Get
                Return _Duplikat
            End Get
            Set(ByVal value As System.Windows.Visibility)
                _Duplikat = value
                RaisePropertyChanged("Duplikat")
            End Set
        End Property

        Public Property OKCommand As ICommand
        Public Property CancelCommand As ICommand

        Private _schluesselliste As List(Of String)


        Public Sub New()

            OKCommand = New RelayCommand(AddressOf OK, AddressOf CanOK)
            CancelCommand = New RelayCommand(AddressOf Cancel)

            Using con As New in1Entities
                Dim query = From c In con.CustomerStates Select c

                CustomerStateLookup = query.ToList

                If CustomerStateLookup.Count > 0 Then
                    SelectedCustomerState = CustomerStateLookup.First
                End If

                Dim query1 = From c In con.Checkins Select c.Schluessel

                _schluesselliste = query1.ToList()


            End Using

        End Sub
        Private Sub DuplicateKey()
            Duplikat = System.Windows.Visibility.Hidden
            If Not IsNothing(_schluesselliste) Then
                If _schluesselliste.Contains(Schluessel) Then
                    Duplikat = System.Windows.Visibility.Visible
                End If
            End If

        End Sub

        Private Sub OK()
            Dim ch As New Checkin
            ch.CheckinLocation = My.Settings.CheckinLocation
            ch.Checkintime = Now
            ch.Nachname = "Tagesgast"
            ch.Vorname = Vorname
            ch.Schluessel = Schluessel
            ch.CustomerStateID = SelectedCustomerState.CustomerStateID

            Dim _context As New in1Entities
            _context.Checkins.Add(ch)
            _context.SaveChanges()
            RaiseEvent RequestClose(Me, Nothing)

        End Sub



        Private Function CanOK() As Boolean
            If Len(Vorname) < 3 Then Return False
            If Trim(Schluessel) = "" Then Return True
            If _schluesselliste.Contains(Schluessel) Then
                Return False
            End If
            Return True
        End Function

        Private Sub Cancel()
            RaiseEvent RequestClose(Me, Nothing)
        End Sub


    End Class
End Namespace