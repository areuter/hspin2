﻿Imports HSPinOne.Infrastructure
Imports HSPinOne.DAL
Imports HSPinOne.Common
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.ComponentModel.Composition
Imports Prism.Regions

Imports System.Windows.Input
Imports System.Windows.Threading
Imports System.Data.Entity.Infrastructure
Imports System.Data.Entity.Core.Objects
Imports CommonServiceLocator

Namespace ViewModel

    Public Class CheckinViewModel
        Inherits ViewModelBase
        Implements INavigationAware


        Private _context As in1Entities
        Private _regionmanager As IRegionManager
        Private _timer As New DispatcherTimer


#Region "   Properties"


        Private _checkincollection As ObservableCollection(Of Checkin)
        Public Property CheckinCollection As ObservableCollection(Of Checkin)
            Get
                Return _checkincollection
            End Get
            Set(ByVal value As ObservableCollection(Of Checkin))
                _checkincollection = value
                RaisePropertyChanged("CheckinCollection")
            End Set
        End Property



        Private _selectedcustomer As Checkin
        Public Property SelectedCustomer As Checkin
            Get
                Return _selectedcustomer
            End Get
            Set(ByVal value As Checkin)
                _selectedcustomer = value
            End Set
        End Property

        Private _Key As String
        Public Property Key As String
            Get
                Return _Key
            End Get
            Set(value As String)
                _Key = value
                RaisePropertyChanged("Key")
            End Set
        End Property

        Private _CheckinCount As Integer = 0
        Public Property CheckinCount As Integer
            Get
                Return _CheckinCount
            End Get
            Set(value As Integer)
                _CheckinCount = value
                RaisePropertyChanged("CheckinCount")
            End Set
        End Property

        Private _QuioEnabled As Boolean = False
        Public ReadOnly Property QuioEnabled As Boolean
            Get
                Return _QuioEnabled
            End Get

        End Property

        Public Property GetCustomerCommand As ICommand
        Public Property CheckoutCommand As ICommand
        Public Property TagesgastCommand As ICommand
        Public Property ReadCardCommand As ICommand
        Public Property SearchKeyCommand As ICommand
        Public Property LastBonCommand As ICommand



#End Region



        Public Sub New()

            Me._regionmanager = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IRegionManager)

            GetCustomerCommand = New RelayCommand(AddressOf GetCustomer)
            CheckoutCommand = New RelayCommand(AddressOf Verkaufsmaske)
            TagesgastCommand = New RelayCommand(AddressOf Tagesgast)
            ReadCardCommand = New RelayCommand(AddressOf ReadCard)
            SearchKeyCommand = New RelayCommand(AddressOf SearchKey)
            LastBonCommand = New RelayCommand(AddressOf LastBon, AddressOf CanLastBon)

            _QuioEnabled = MyGlobals.QuioEnabled

            _timer.Interval = New TimeSpan(0, 1, 0)
            AddHandler _timer.Tick, AddressOf LoadData
            _timer.Start()

            _context = New in1Entities
            CheckinCollection = _context.Checkins.Local

            LoadData()

        End Sub

        Private Sub LoadData()

            ' Leider nur per ObjectContext, da nur dieser den kompletten Context aktualisieren kann
            Dim oc As ObjectContext = DirectCast(_context, IObjectContextAdapter).ObjectContext

            ' Refresh Existing and add new
            Dim os = oc.CreateObjectSet(Of Checkin)()
            os.MergeOption = MergeOption.OverwriteChanges
            os.Load()

            ' remove from context, what's been removed from database
            oc.Refresh(RefreshMode.StoreWins, _context.Checkins.Local)

            CheckinCount = _context.Checkins.Count

        End Sub


        Private Sub TryCheckin(ByVal cust As Customer)
            'Überprüfen ob bereits eingecheckt
            Dim excust = (From c In _context.Checkins Where c.CustomerID = cust.CustomerID Select c).SingleOrDefault

            If IsNothing(excust) Then


                Dim params As New NavigationParameters From {
                    {"ID", cust.CustomerID}
                }
                _regionmanager.Regions(RegionNames.MainRegion).RequestNavigate(New Uri("CheckinCustomerView", UriKind.Relative), params)



            Else
                SelectedCustomer = excust
                Verkaufsmaske()
                'Dim dlg As New DialogService
                'dlg.ShowAlert("Checkin", "Dieser Kunde ist bereits eingecheckt")
                'dlg = Nothing
                'SelectedCustomer = excust.First
            End If
        End Sub

        Private Sub GetCustomer()
            Dim search = ServiceLocator.Current.GetInstance(Of ISearchCustomer)()

            Dim cust = search.GetCustomer
            If Not IsNothing(cust) Then

                TryCheckin(cust)


            End If
            'LoadData()
        End Sub

        Private Sub ReadCard()
            Dim m As New QuioCV6600
            Dim card = m.Karte_lesen
            If card > 100 Then
                Dim custid = Customer_Bal.CustomerfromCard(card)
                If custid > 0 Then
                    Dim cust = _context.Customers.Find(custid)
                    If Not IsNothing(cust) Then
                        TryCheckin(cust)
                    End If
                End If
            End If

        End Sub

        Private Sub Tagesgast()

            Dim vm As New ViewModel.TagesgastViewModel
            Dim uc As New TagesgastView
            uc.DataContext = vm

            Dim win As New WindowController
            AddHandler vm.RequestClose, AddressOf win.CloseWindow
            AddHandler win.MainWindowClosed, AddressOf LoadData
            win.ShowWindow(uc, "Tagesgast")

            Events.EventInstance.EventAggregator.GetEvent(Of CheckinFocusEvent).Publish(Nothing)

        End Sub



        Private Sub Verkaufsmaske()

            If Not IsNothing(SelectedCustomer) Then
                Dim params As New NavigationParameters()
                params.Add("CheckinID", SelectedCustomer.CheckinID)
                '_regionmanager.RequestNavigate(RegionNames.MainRegion, New Uri("CheckinVerkaufView", UriKind.Relative), params)
                _regionmanager.RequestNavigate(RegionNames.MainRegion, New Uri("CheckinQuickView", UriKind.Relative), params)
            End If

        End Sub

        Private Sub SearchKey()

            If Not IsNothing(Key) AndAlso IsNumeric(Key) Then
                Dim pkey As String = ""
                pkey = Key


                Key = ""
                If Len(pkey) < 6 Then
                    Dim cid = CheckinCollection.Where(Function(o) o.Schluessel = pkey)
                    If cid.Any Then
                        SelectedCustomer = cid.First
                        Verkaufsmaske()
                    End If
                Else

                    If pkey > 0 Then
                        Dim custid = Customer_Bal.CustomerfromCard(pkey)
                        If custid > 0 Then
                            Dim cust = _context.Customers.Find(custid)
                            If Not IsNothing(cust) Then
                                TryCheckin(cust)
                            End If
                        End If
                    End If
                End If

            End If
            Key = ""
        End Sub

        Private Sub LastBon()
            If Not IsNothing(MyGlobals.LastBon) Then
                Dim bon As New HSPinOne.Common.Bondruck
                bon.LastBon(MyGlobals.LastBon)
                bon = Nothing

            End If
        End Sub

        Private Function CanLastBon() As Boolean
            If Not IsNothing(MyGlobals.LastBon) And MyGlobals.LastBon <> Guid.Empty Then Return True
            Return False
        End Function

        Public Overrides Sub OnNavigatedTo(navigationContext As NavigationContext)
            MyBase.OnNavigatedTo(navigationContext)
            LoadData()
        End Sub

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
            _timer.Stop()
            If Not IsNothing(_context) Then _
                _context.Dispose()
        End Sub
    End Class
End Namespace