﻿Imports HSPinOne.Infrastructure
Imports HSPinOne.DAL
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.ComponentModel.Composition
Imports Prism.Regions

Imports System.Windows.Input
Imports System.Windows.Threading
Imports System.Data.Entity.Infrastructure
Imports System.Data.Entity.Core.Objects


Namespace ViewModel

    <Export(GetType(ViewModel.StudioDashboardViewModel)), PartCreationPolicy(CreationPolicy.Shared)> _
    Public Class StudioDashboardViewModel
        Inherits ViewModelBase
        Implements INavigationAware


        Private _context As in1Entities
        Private _regionmanager As IRegionManager
        Private _timer As New DispatcherTimer


#Region "   Properties"

        Private _checkincollection As ObservableCollection(Of Checkin)
        Public Property CheckinCollection As ObservableCollection(Of Checkin)
            Get
                Return _checkincollection
            End Get
            Set(ByVal value As ObservableCollection(Of Checkin))
                _checkincollection = value
                RaisePropertyChanged("CheckinCollection")
            End Set
        End Property

        Private _auslastung As Integer
        Public Property Auslastung As Integer
            Get
                Return _auslastung
            End Get
            Set(ByVal value As Integer)
                _auslastung = value
                RaisePropertyChanged("Auslastung")
            End Set
        End Property

        '  Public Auslastung As Integer
        Public ActualUser As Integer
        Public MaxUser As Integer = 35

#End Region

        Public Sub New(ByVal rm As IRegionManager)

            Me._regionmanager = rm

            _timer.Interval = New TimeSpan(0, 1, 0)
            AddHandler _timer.Tick, AddressOf LoadData
            _timer.Start()

            _context = New in1Entities
            CheckinCollection = _context.Checkins.Local

            LoadData()

        End Sub

        Private Sub LoadData()

            ' Leider nur per ObjectContext, da nur dieser den kompletten Context aktualisieren kann
            Dim oc As ObjectContext = DirectCast(_context, IObjectContextAdapter).ObjectContext

            ' Refresh Existing and add new
            Dim os = oc.CreateObjectSet(Of Checkin)()
            os.MergeOption = MergeOption.OverwriteChanges
            os.Load()

            ' remove from context, what's been removed from database

            oc.Refresh(RefreshMode.StoreWins, _context.Checkins.Local)
            ActualUser = _context.Checkins.Local.Count()
            Auslastung = (ActualUser * 100) / MaxUser


        End Sub


        Protected Overrides Sub Finalize()
            MyBase.Finalize()
            _timer.Stop()
            _context.Dispose()
        End Sub
    End Class
End Namespace