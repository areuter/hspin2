﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Input
Imports System.Windows.Data
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports Prism.Regions
Imports System.ComponentModel.Composition
Imports System.ComponentModel
Imports System.Data.Entity.Validation
Imports HSPinOne.Common

Namespace ViewModel


    Public Class CheckinVerkaufViewModel
        Inherits ViewModelBase
        Implements INavigationAware
        Implements IRegionMemberLifetime




        Public Property LVProducts As ListCollectionView
        Public Property LVBillings As ListCollectionView



        Private _OCProducts As ObservableCollection(Of Package)

        Private _canECCash As Boolean = False

        Private _firstload = True

        Private _CheckinID As Integer
        Public Property CheckinID As Integer
            Get
                Return _CheckinID
            End Get
            Set(value As Integer)
                _CheckinID = value
                Offen = 0
                LoadBillings()
                'LoadProducts()
                Summe()
            End Set
        End Property

        Private _Checkin As Checkin
        Public Property Checkin As Checkin
            Get
                Return _Checkin
            End Get
            Set(value As Checkin)
                _Checkin = value
                RaisePropertyChanged("Checkin")
            End Set
        End Property


        Private _OCBillings As ObservableCollection(Of Billing) = New ObservableCollection(Of Billing)
        Public Property OCBillings As ObservableCollection(Of Billing)
            Get
                Return _OCBillings
            End Get
            Set(value As ObservableCollection(Of Billing))
                _OCBillings = value
            End Set
        End Property

        Public Property BillingsView As ICollectionView

        Private _selectedbill As Billing
        Public Property SelectedBill As Billing
            Get
                Return _selectedbill
            End Get
            Set(value As Billing)
                _selectedbill = value
                RaisePropertyChanged("SelectedBill")
            End Set
        End Property

        Private _Offen As Double = 0
        Public Property Offen As Double
            Get
                Return _Offen
            End Get
            Set(value As Double)
                _Offen = value
                RaisePropertyChanged("Offen")
            End Set
        End Property

        Private _CategoriesLookup As List(Of Category)
        Public Property CategoriesLookup As List(Of Category)
            Get
                Return _CategoriesLookup
            End Get
            Set(value As List(Of Category))
                _CategoriesLookup = value
            End Set
        End Property

        Private _SelectedCategory As Category
        Public Property SelectedCategory As Category
            Get
                Return _SelectedCategory
            End Get
            Set(value As Category)
                _SelectedCategory = value
                If Not IsNothing(LVProducts) Then
                    LVProducts.Refresh()
                End If
                RaisePropertyChanged("SelectedCategory")
            End Set
        End Property

        Private _CustomerStates As ObservableCollection(Of CustomerState)
        Public Property CustomerStates() As ObservableCollection(Of CustomerState)
            Get
                Return _CustomerStates
            End Get
            Set(ByVal value As ObservableCollection(Of CustomerState))
                _CustomerStates = value
            End Set
        End Property

        Private _SelectedState As CustomerState
        Public Property SelectedState() As CustomerState
            Get
                Return _SelectedState
            End Get
            Set(ByVal value As CustomerState)
                _SelectedState = value
                RaisePropertyChanged("SelectedState")
                LoadProducts()
            End Set
        End Property

        Private _PreisEditPopup As Boolean
        Public Property PreisEditPopup() As Boolean
            Get
                Return _PreisEditPopup
            End Get
            Set(ByVal value As Boolean)
                If value = True Then
                    _PreisEditPopup = False
                    _PreisEditPopup = True
                End If
                _PreisEditPopup = value
                RaisePropertyChanged("PreisEditPopup")
            End Set
        End Property

        Private _Preis As Decimal
        Public Property Preis() As Decimal
            Get
                Return _Preis
            End Get
            Set(ByVal value As Decimal)
                _Preis = value
                RaisePropertyChanged("Preis")
            End Set
        End Property



        Private _context As in1Entities
        Private _regionmanager As IRegionManager


        Private _defaultaccount As Integer = 0

        Public Property KassierenCommand As ICommand
        Public Property ECCashCommand As ICommand
        Public Property BackCommand As ICommand
        Public Property AddCommand As ICommand
        Public Property StornoCommand As ICommand
        Public Property CheckoutCommand As ICommand
        Public Property PreisEditCancelCommand As ICommand
        Public Property PreisEditOKCommand As ICommand
        Public Property ShowPreisPopupCommand As ICommand
        Public Property LSVCommand As ICommand

        Private _aktbon As Guid

        Private _kassenr As Integer
        Private _cashclosingid As Integer = 0




        Public Sub New()
            Me._regionmanager = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IRegionManager)

            Init()

            KassierenCommand = New RelayCommand(AddressOf Kassieren, AddressOf CanKassieren)
            ECCashCommand = New RelayCommand(AddressOf KassierenEC, AddressOf CanKassierenECCash)
            LSVCommand = New RelayCommand(AddressOf KassierenLSV, AddressOf CanKassierenLSV)

            BackCommand = New RelayCommand(AddressOf Zurueck)
            AddCommand = New RelayCommand(AddressOf AddProduct)
            StornoCommand = New RelayCommand(AddressOf DelProduct)
            CheckoutCommand = New RelayCommand(AddressOf Checkout)
            PreisEditCancelCommand = New RelayCommand(AddressOf UpdatePreisCancel)
            PreisEditOKCommand = New RelayCommand(AddressOf UpdatePreis)
            ShowPreisPopupCommand = New RelayCommand(AddressOf EditPreis)


            Using con As New in1Entities
                ' Categories Lookup
                Dim cquery = From c In con.Categories Order By c.CategoryID

                CategoriesLookup = cquery.ToList

                Dim cat As New Category
                cat.CategoryID = 9999
                cat.CategoryName = "Alle"
                CategoriesLookup.Insert(0, cat)

                SelectedCategory = CategoriesLookup.First

                con.CustomerStates.Load()
                Me.CustomerStates = con.CustomerStates.Local()


            End Using



        End Sub

        Private Sub Init()
            Dim df = MachineSettingService.GetMachineSetting(MachineSettingService.in1MaschineSettings.Kasse)
            If IsNothing(df) OrElse Trim(df) = "" Then
                Dim dlg As New DialogService
                dlg.ShowError("DefaultAccount", "Es ist kein DefaultAccount für diesen PC festgelegt. Bitte informieren Sie den Administrator")
                Return
            Else
                _defaultaccount = df
            End If

            If Not Trim(MachineSettingService.GetMachineSetting(MachineSettingService.in1MaschineSettings.TerminalIP)) = "" Then
                _canECCash = True
            End If

            _kassenr = MachineSettingService.GetKasseNr()
            _cashclosingid = Repositories.CashClosingRepository.GetCashClosingID(_kassenr)

        End Sub

        Private Sub LoadProducts()

            If Not IsNothing(Checkin) AndAlso Not IsNothing(Me.CustomerStates) Then






                Dim query = From c In _context.Packages Select c

                Dim packages = From c In _context.Packages Join p In _context.PackagePrices On c.PackageID Equals p.PackageID
                               Where p.CustomerStateID = Me.SelectedState.CustomerStateID And c.IsActive = True
                               Select New With {c.PackageID, c.PackageName, p.Preis, p.TaxPercent, c.Position, c.Zutritte,
                                   c.CostcenterID, c.ProductTypeID, c.CategoryID, p.Innenauftrag, p.Sachkonto,
                                   .pCostcenterID = p.CostcenterID, p.Steuerkennzeichen
                                   }

                '_OCProducts = New ObservableCollection(Of Package)(query.ToList)


                LVProducts = CType(CollectionViewSource.GetDefaultView(packages.ToList), ListCollectionView)
                LVProducts.SortDescriptions.Add(New SortDescription("Position", ListSortDirection.Ascending))
                LVProducts.SortDescriptions.Add(New SortDescription("PackageName", ListSortDirection.Ascending))

                LVProducts.Filter = New Predicate(Of Object)(AddressOf Productsfilter)
                RaisePropertyChanged("LVProducts")
            End If
        End Sub

        Private Function Productsfilter(ByVal item As Object) As Boolean
            Dim itm = item

            If SelectedCategory.CategoryID = 9999 Then Return True
            If SelectedCategory.CategoryID = itm.CategoryID Then Return True

            Return False
        End Function

        Private Sub LoadBillings()
            _context = New in1Entities

            If Not IsNothing(CheckinID) Then

                'Me.SelectedState = Me.CustomerStates.Where(Function(o) o.CustomerStateID = Checkin.CustomerStateID).FirstOrDefault()

                Checkin = _context.Checkins.Find(CheckinID)


                If Not IsNothing(Checkin) Then

                    If _firstload Then
                        Me.SelectedState = Me.CustomerStates.Where(Function(o) o.CustomerStateID = Checkin.CustomerStateID).FirstOrDefault()
                    End If
                    _firstload = False

                    OCBillings = Checkin.Billings



                    BillingsView = CType(CollectionViewSource.GetDefaultView(OCBillings), ICollectionView)
                    BillingsView.Filter = New Predicate(Of Object)(AddressOf Billingfilter)

                    Summe()
                    AddHandler OCBillings.CollectionChanged, AddressOf Summe
                    RaisePropertyChanged("OCBillings")



                Else
                    Dim dlg As New DialogService
                    dlg.ShowError("Fehler", "Der Kunde kann in den Checkins nicht gefunden werden")
                    ZumCheckin()
                End If
            End If

        End Sub

        Private Function Billingfilter(ByVal itm As Object) As Boolean
            Dim item = CType(itm, Billing)
            If Not IsNothing(item) Then
                If item.BillingStatus = BillingStates.Offen Then
                    If item.AccountID = _defaultaccount Then
                        Return True
                    End If
                End If
            End If
            Return False
        End Function

        Private Sub AddProduct(ByVal packageid As Integer)
            If Not IsNothing(Checkin) Then


                Dim bill As New Billing

                Dim bal As New Billing_Bal(MyGlobals.CurrentUsername)
                If bal.AddBillingFromPackage(Checkin.CheckinID, packageid, _defaultaccount, 1, _kassenr, SelectedState.CustomerStateID) Then
                    LoadBillings()
                End If

                'Dim sprod = _context.Packages.Find(productid)
                'Dim preisitem = sprod.PackagePrices.Where(Function(o) o.CustomerStateID = SelectedState.CustomerStateID And o.PackageID = productid).First
                'If Not IsNothing(preisitem) Then

                '    If Not IsNothing(sprod) Then
                '        bill.CheckinID = Checkin.CheckinID
                '        bill.CustomerStateID = SelectedState.CustomerStateID
                '        bill.PackageID = sprod.PackageID
                '        bill.Verwendungszweck = sprod.PackageName
                '        bill.Faelligkeit = Now
                '        bill.Rechnungsdatum = Now
                '        bill.Brutto = preisitem.Preis
                '        bill.Steuerprozent = preisitem.TaxPercent
                '        bill.PaymentMethodID = 1
                '        bill.IsStorno = False
                '        bill.Packagecredit = sprod.Zutritte
                '        bill.AccountID = _defaultaccount
                '        bill.ProductTypeID = sprod.ProductTypeID

                '        bill.Innenauftrag = preisitem.Innenauftrag
                '        bill.Sachkonto = preisitem.Sachkonto
                '        bill.KSTextern = preisitem.KSTextern
                '        bill.Steuerkennzeichen = preisitem.Steuerkennzeichen

                '        If Not IsNothing(Checkin.CustomerID) Then
                '            bill.CustomerID = Checkin.CustomerID
                '        End If
                '        bill.BillingStatus = BillingStates.Offen
                '        If (Not IsNothing(preisitem.CostcenterID)) Then
                '            bill.CostcenterID = preisitem.CostcenterID
                '        Else
                '            bill.CostcenterID = sprod.CostcenterID
                '        End If

                '        OCBillings.Add(bill)
                '        Try
                '            _context.SaveChanges(MyGlobals.CurrentUsername)
                '        Catch ex As DbEntityValidationException
                '            Dim err As String = String.Empty
                '            For Each e In ex.EntityValidationErrors
                '                For Each ve In e.ValidationErrors
                '                    err = err & ve.PropertyName & ":" & ve.ErrorMessage & vbNewLine
                '                Next
                '            Next
                '            Dim dlg As New DialogService
                '            dlg.ShowError("Entity", err)
                '        End Try

                '    End If
                'End If
            End If
        End Sub

        Private Sub DelProduct(ByVal param As Object)

            Dim dlg As New DialogService
            If dlg.AskConfirmation("Storno", "Wirklich die Buchung stornieren?") = True Then

                Dim bal As New Billing_Bal(MyGlobals.CurrentUsername)


                Dim bill = CType(param, Billing)

                If bal.Storno(bill.BillingID, _kassenr) Then
                    LoadBillings()
                End If

            End If
        End Sub

        Private Sub Summe()
            Dim lsum As Double = 0
            If Not IsNothing(BillingsView) Then
                For Each posten In BillingsView
                    Dim itm = CType(posten, Billing)
                    If itm.BillingStatus = BillingStates.Offen Then
                        lsum = lsum + itm.Brutto
                    End If
                Next
            End If
            Offen = lsum

        End Sub

        Private Sub Zurueck()
            ZumCheckin()
        End Sub

        Private Sub Kassieren()
            KassierenAction(1)
        End Sub

        Private Sub KassierenEC()
            KassierenAction(4)
        End Sub

        Private Sub KassierenLSV()
            KassierenAction(2)
        End Sub

        Private Sub KassierenAction(Optional method As Integer = 1)
            Dim dlg As New DialogService
            If dlg.AskConfirmation("Kassieren", "Wirklich den Betrag von " & Offen.ToString("c") & " kassieren?") = True Then

                Dim bal As New Billing_Bal(MyGlobals.CurrentUsername)

                Dim paymentinfo As String = String.Empty

                Summe()
                Dim zahlbetrag As Double = Offen

                Dim accountid = 0
                Dim billingstate As String = BillingStates.Offen
                If method = 1 Then ' Wenn Barzahlung Machinesettings holen
                    accountid = MachineSettingService.GetMachineSetting(MachineSettingService.in1MaschineSettings.Kasse)
                    billingstate = BillingStates.Bezahlt
                ElseIf method = 2 Or method = 3 Then ' Wenn LS, Überweisung das standardkonto laden
                    accountid = GlobalSettingService.HoleEinstellung("defaultaccount")
                    billingstate = BillingStates.Offen
                ElseIf method = 4 Then
                    accountid = GlobalSettingService.HoleEinstellung("pos.eccashaccountid")
                    billingstate = BillingStates.Bezahlt
                End If


                If method = 4 Then

                    Dim opiservice As New OPIService
                    Dim res As Boolean = opiservice.Pay(zahlbetrag)

                    If res = True Then
                        paymentinfo = opiservice.Paymentinfo

                    Else
                        ' Abbruch, wenn EC Cash scheitert
                        Return
                    End If

                End If

                '_aktbon = Guid.NewGuid

                Dim kassenr = _kassenr ' MachineSettingService.GetKasseNr()


                Dim list As New List(Of Billing)
                For Each posten In BillingsView ' OCBillings
                    Dim itm = CType(posten, Billing)
                    list.Add(itm)
                Next


                _aktbon = bal.PayList(list, accountid, method, kassenr, paymentinfo)
                MyGlobals.LastBon = _aktbon


                '_context.SaveChanges(MyGlobals.CurrentUsername)



                ' Bon drucken
                Dim customerid As Integer = 0
                If Not IsNothing(Checkin.CustomerID) Then
                    customerid = Checkin.CustomerID
                End If
                Dim bonsvc As New BonService(_aktbon, False, customerid)

                ' Refresh
                LoadBillings()

            End If

        End Sub

        Private Function CanKassieren() As Boolean
            If OCBillings.Any Then Return True
            Return False
        End Function


        Private Function CanKassierenECCash() As Boolean
            If CanKassieren() AndAlso _canECCash _
                AndAlso Offen > 0 Then Return True
            Return False
        End Function

        Private Function CanKassierenLSV()
            If CanKassieren() AndAlso Offen > 0 Then
                If Not IsNothing(Checkin.Customer) Then
                    If Not IsNothing(Checkin.Customer.BIC) AndAlso Not IsNothing(Checkin.Customer.IBAN) And Checkin.Customer.LSVEnabled = True Then
                        If Len(Checkin.Customer.BIC) >= 8 And Len(Checkin.Customer.IBAN) > 10 Then Return True
                    End If
                End If
            End If
            Return False
        End Function


        Private Sub ZumCheckin()
            If Not IsNothing(_context) Then _context.Dispose()
            _regionmanager.RequestNavigate(RegionNames.MainRegion, New Uri("CheckinView", UriKind.Relative))
        End Sub

        Private Sub Checkout()
            If Not IsNothing(Checkin) Then
                Dim dlg As New DialogService

                'Check ob Kunde wirklich noch eingecheckt

                '_context.Entry(Checkin).Reload()
                Dim exists = Nothing
                Using con As New in1Entities
                    exists = con.Checkins.Find(Checkin.CheckinID)
                End Using

                If Not IsNothing(exists) Then

                    ' Check ob Tagesgast
                    If IsNothing(Checkin.CustomerID) Then
                        If Checkin.Billings.Where(Function(o) o.BillingStatus = BillingStates.Offen And o.AccountID = _defaultaccount).Any Then
                            dlg.ShowInfo("Tagesgast", "Checkout von Tagesgästen erst nach Zahlung möglich")
                            Return
                        End If
                    End If

                    'Noch Posten offen?
                    If Checkin.Billings.Where(Function(o) o.BillingStatus = BillingStates.Offen And o.AccountID = _defaultaccount).Any Then
                        Dim ret = dlg.AskCancelConfirmation("Offene Posten", "Es sind noch Rechnungen offen. Kunde trotzdem auschecken?")
                        If ret <> System.Windows.MessageBoxResult.Yes Then Return
                    End If

                    Dim log As New CheckinLog
                    If Not IsNothing(Checkin.CustomerID) Then
                        log.CustomerID = Checkin.CustomerID
                    End If
                    log.Checkin = Checkin.Checkintime
                    log.CheckinLocation = Checkin.CheckinLocation
                    log.Checkout = Now

                    _context.CheckinLogs.Add(log)

                    _context.Checkins.Remove(Checkin)
                    _context.SaveChanges()
                Else
                    dlg.ShowInfo("Checkout", "Der Kunde wurde bereits anderweitig ausgecheckt!")
                End If

                ZumCheckin()

            End If

        End Sub

        Public Sub EditPreis(ByVal param As Object)
            Dim selection = CType(param, Billing)
            Me.SelectedBill = selection
            Me.Preis = SelectedBill.Brutto
            If Not IsNothing(SelectedBill) Then
                Me.PreisEditPopup = True
            End If

        End Sub

        Public Sub UpdatePreis()
            Me.PreisEditPopup = False
            Dim bill = Checkin.Billings.Where(Function(o) o.BillingID = SelectedBill.BillingID).First
            bill.Brutto = Preis
            _context.SaveChanges(MyGlobals.CurrentUsername)
            SelectedBill = Nothing
            Summe
        End Sub

        Public Sub UpdatePreisCancel()
            Me.PreisEditPopup = False
            Me.SelectedBill = Nothing
        End Sub


        Public Overloads Sub OnNavigatedTo(navigationContext As Prism.Regions.NavigationContext) Implements Prism.Regions.INavigationAware.OnNavigatedTo
            _firstload = True
            CheckinID = navigationContext.Parameters("CheckinID")
            Init()
        End Sub

        Public ReadOnly Property KeepAlive As Boolean Implements Prism.Regions.IRegionMemberLifetime.KeepAlive
            Get
                Return False
            End Get
        End Property
    End Class
End Namespace