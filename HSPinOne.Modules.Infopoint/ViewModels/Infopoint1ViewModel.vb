﻿Imports HSPinOne.DAL
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports HSPinOne.Infrastructure
Imports System.Windows.Data
Imports System.Windows.Input
Imports HSPinOne.Common
Imports System.ComponentModel
Imports HSPinOne.Modules.Verwaltung
Imports Prism.Regions

Imports System.ComponentModel.Composition
Imports System.Windows
Imports HSPinOne.UIControls
Imports CommonServiceLocator

Namespace ViewModel

    Public Class Infopoint1ViewModel
        Inherits ViewModelBase
        Implements INavigationAware
        Implements IRegionMemberLifetime


        Private _customer As Customer
        Private _card As Decimal
        Private _lvproducts As ListCollectionView
        Private _selectedbill As Billing
        Private _context As in1Entities

        Private _productlist As ObservableCollection(Of Productlistitem)

        Public Event RequestClear As EventHandler
        Public Event RequestWeiter As EventHandler

        Private WithEvents bgw As New BackgroundWorker
        Private WithEvents bgw_Billings As New BackgroundWorker


        Private _ExtUrl As String
        Private _allowLS As Boolean = False
        Private _canECCash As Boolean = False

        Private _regionmanager As IRegionManager

#Region "   Properties"


        Public Property DefaultCustomers As ObservableCollection(Of In1Button)

        Public ReadOnly Property QuioEnabled As Visibility
            Get
                If Not MyGlobals.QuioEnabled Then Return Visibility.Collapsed
                Return Visibility.Visible
            End Get
        End Property

        Private _SetFocus As Boolean = False
        Public Property SetFocus As Boolean
            Get
                Return _SetFocus
            End Get
            Set(value As Boolean)
                _SetFocus = value
                RaisePropertyChanged("SetFocus")
            End Set
        End Property

        Private _kartenstatus As String = ""
        Public Property Kartenstatus As String
            Get
                Return _kartenstatus
            End Get
            Set(ByVal value As String)
                _kartenstatus = value
                RaisePropertyChanged("Kartenstatus")
            End Set
        End Property

        Public Property Customer As Customer
            Get
                Return _customer
            End Get
            Set(ByVal value As Customer)
                _customer = value
                RaisePropertyChanged("Customer")
            End Set
        End Property

        Public Property LVProducts As ListCollectionView
            Get
                Return _lvproducts
            End Get
            Set(ByVal value As ListCollectionView)
                _lvproducts = value
                RaisePropertyChanged("LVProducts")
            End Set
        End Property

        Private _lvbillings As ListCollectionView
        Public Property LVBillings As ListCollectionView
            Get
                Return _lvbillings
            End Get
            Set(ByVal value As ListCollectionView)
                _lvbillings = value
                RaisePropertyChanged("LVBillings")
            End Set
        End Property

        Private _lvcart As ListCollectionView
        Public Property LVCart As ListCollectionView
            Get
                Return _lvcart
            End Get
            Set(ByVal value As ListCollectionView)
                _lvcart = value
                RaisePropertyChanged("LVCart")
            End Set
        End Property


        Public Property SelectedBill As Billing
            Get
                Return _selectedbill
            End Get
            Set(ByVal value As Billing)
                _selectedbill = value
                RaisePropertyChanged("SelectedBill")
            End Set
        End Property

        Public Property OCBillings As ObservableCollection(Of Billing)

        Public Property Cart As ObservableCollection(Of Billing)

        Public Property OCCourse As ObservableCollection(Of CoursePrice)


        Private _selectedproduct As Productlistitem
        Public Property SelectedProduct As Productlistitem
            Get
                Return _selectedproduct
            End Get
            Set(ByVal value As Productlistitem)
                _selectedproduct = value
                RaisePropertyChanged("SelectedProduct")
            End Set
        End Property

        Public Property SelectedCart As Billing

        Private _iscustomer As Boolean
        Public Property IsCustomer As Boolean
            Get
                Return _iscustomer
            End Get
            Set(ByVal value As Boolean)
                _iscustomer = value
                RaisePropertyChanged("IsCustomer")
            End Set
        End Property

        Private _isverkaufvisible As Visibility = System.Windows.Visibility.Visible ' System.Windows.Visibility.Hidden
        Public Property IsVerkaufVisible As Visibility
            Get
                Return _isverkaufvisible
            End Get
            Set(ByVal value As Visibility)
                _isverkaufvisible = value
                RaisePropertyChanged("IsVerkaufVisible")
            End Set
        End Property

        Private _selectedtabindex As Integer
        Public Property SelectedTabIndex As Integer
            Get
                Return _selectedtabindex
            End Get
            Set(ByVal value As Integer)
                _selectedtabindex = value
                RaisePropertyChanged("SelectedTabIndex")
                If _selectedtabindex = 1 Then
                    LoadProducts()
                End If
            End Set
        End Property

        Private _summe As Decimal
        Public Property Summe As Decimal
            Get
                Return _summe
            End Get
            Set(ByVal value As Decimal)
                _summe = value
                UpdateRueckgeld()
                RaisePropertyChanged("Summe")
            End Set
        End Property

        Private _gegeben As String
        Public Property Gegeben As String
            Get
                Return _gegeben
            End Get
            Set(ByVal value As String)

                _gegeben = value
                UpdateRueckgeld()
                RaisePropertyChanged("Gegeben")
            End Set
        End Property

        Private _differenz As Decimal
        Public Property Differenz As Decimal
            Get
                Return _differenz
            End Get
            Set(ByVal value As Decimal)
                _differenz = value
                RaisePropertyChanged("Differenz")
            End Set
        End Property

        Private _accounts As IEnumerable(Of Account)
        Public Property Accounts As IEnumerable(Of Account)
            Get
                Return _accounts
            End Get
            Set(ByVal value As IEnumerable(Of Account))
                _accounts = value
                RaisePropertyChanged("Accounts")
            End Set
        End Property

        'Private _lvaccounts As ListCollectionView
        'Public Property LVAccounts As ListCollectionView
        '    Get
        '        Return _lvaccounts
        '    End Get
        '    Set(value As ListCollectionView)
        '        _lvaccounts = value
        '        RaisePropertyChanged("LVAccounts")
        '    End Set
        'End Property


        Private _account As Integer
        Public Property Account As Integer
            Get
                Return _account
            End Get
            Set(ByVal value As Integer)
                _account = value
                RaisePropertyChanged("Account")
            End Set
        End Property

        Public Property Packagelogs As List(Of PackageLog)

        Public Property SelectedPackagelog As PackageLog

        Private _aboprotokollVisible As Visibility

        Public Property Warenkorbfilter As Dictionary(Of Boolean, String)

        Private _selectedwarenkorbfilter As Boolean
        Public Property SelectedWarenkorbfilter As Boolean
            Get
                Return _selectedwarenkorbfilter
            End Get
            Set(ByVal value As Boolean)
                _selectedwarenkorbfilter = value
                RaisePropertyChanged("SelectedWarenkorbfilter")
                RefreshCart()
            End Set
        End Property

        Private _termlist As List(Of Term)
        Public Property Termlist As List(Of Term)
            Get
                Return _termlist
            End Get
            Set(ByVal value As List(Of Term))
                _termlist = value
                RaisePropertyChanged("Termlist")
            End Set
        End Property

        Private _selectedterm As Term
        Public Property SelectedTerm As Term
            Get
                Return _selectedterm
            End Get
            Set(ByVal value As Term)
                _selectedterm = value
                RaisePropertyChanged("SelectedTerm")
                If Not IsNothing(_selectedterm) And Not IsNothing(Customer) Then
                    LoadProducts()
                End If
            End Set
        End Property

        Private _CategoriesLookup As List(Of Category)
        Public Property CategoriesLookup As List(Of Category)
            Get
                Return _CategoriesLookup
            End Get
            Set(value As List(Of Category))
                _CategoriesLookup = value
            End Set
        End Property

        Private _SelectedCategory As Category
        Public Property SelectedCategory As Category
            Get
                Return _SelectedCategory
            End Get
            Set(value As Category)
                _SelectedCategory = value
                If Not IsNothing(LVProducts) Then
                    LVProducts.Refresh()
                End If
                RaisePropertyChanged("SelectedCategory")
            End Set
        End Property


        Private _Searchterm As String
        Public Property Searchterm As String
            Get
                Return _Searchterm
            End Get
            Set(value As String)
                _Searchterm = value
                RaisePropertyChanged("Searchterm")
            End Set
        End Property

        Private _PopupOpen As Visibility = Visibility.Hidden
        Public Property PopupOpen As Visibility
            Get
                Return _PopupOpen
            End Get
            Set(value As Visibility)
                _PopupOpen = value
                RaisePropertyChanged("PopupOpen")
            End Set
        End Property



        Private _IsBgwBusy As Boolean = False
        Public Property IsBgwBusy() As Boolean
            Get
                Return _IsBgwBusy
            End Get
            Set(ByVal value As Boolean)
                _IsBgwBusy = value
                RaisePropertyChanged("IsBgwBusy")
            End Set
        End Property


        Public Property Vertragsinfo As ObservableCollection(Of String)

        Private _External As String
        Public Property External As String
            Get
                Return _External
            End Get
            Set(value As String)
                _External = value
                RaisePropertyChanged("External")
            End Set
        End Property

        Private _Uebungsleiter As String
        Public Property Uebungsleiter As String
            Get
                Return _Uebungsleiter
            End Get
            Set(value As String)
                _Uebungsleiter = value
                RaisePropertyChanged("Uebungsleiter")
            End Set
        End Property

        Private _CBFilter As Integer = 0
        Public Property CBFilter As Integer
            Get
                Return _CBFilter
            End Get
            Set(value As Integer)
                _CBFilter = value
                RaisePropertyChanged("CBFilter")
                RefreshBillings()
            End Set
        End Property

        Private _key As String
        Public Property Key As String
            Get
                Return _key
            End Get
            Set(value As String)
                _key = value
                RaisePropertyChanged("Key")
            End Set
        End Property

        Private _KasseNr As Integer
        Public Property KasseNr() As Integer
            Get
                Return _KasseNr
            End Get
            Set(ByVal value As Integer)
                _KasseNr = value
                RaisePropertyChanged("KasseNr")
            End Set
        End Property

        Public Property CBFilterList As Dictionary(Of Integer, String)


        Private _lastbon As Guid
        Private _aktbon As Guid

        Private _sepa As Integer = 1

        Private _accountid As Integer

        Private _defaultaccount As Integer
        Private _defaultpaymentmethod As Integer

        Private _cashclosingid As Integer = 0



        'Tabitem Kunde
        Public Property GetCustomerCommand As ICommand
        Public Property ClearCustomerCommand As ICommand
        Public Property NavigateForwardCommand As ICommand
        Public Property ReadCardCommand As ICommand
        Public Property StornoCommand As ICommand
        Public Property NewCustomerCommand As ICommand
        Public Property CustomerDetailCommand As ICommand
        Public Property NavigateKassierenCommand As ICommand
        Public Property UpdateCommand As ICommand
        Public Property AboabtragenCommand As ICommand
        Public Property AboprotokollCommand As ICommand
        Public Property ClosePopupCommand As ICommand
        Public Property DelTerminCommand As ICommand

        Public Property DefaultCustomerCommand As ICommand

        Public Property LastBonCommand As ICommand
        Public Property SearchKeyCommand As ICommand


        'Tabitem Verkauf
        Public Property CartAddCommand As ICommand
        Public Property CartDelCommand As ICommand
        Public Property CartWaitinglistCommand As ICommand
        Public Property PayCommand As ICommand
        Public Property PayECCommand As ICommand
        Public Property PayLSCommand As ICommand
        Public Property SearchCommand As ICommand

        'Tabitem Kassieren
        Public Property NumberCommand As ICommand

        Public Property GotoOrderCommand As New RelayCommand(AddressOf GotoOrderAction)


#End Region

#Region "   Constructor"


        Public Sub New()

            Me._regionmanager = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IRegionManager)

            'Welche Kasse ist für den PC hinterlegt
            _accountid = CType(MachineSettingService.GetMachineSetting(MachineSettingService.in1MaschineSettings.Kasse), Integer)



            Vertragsinfo = New ObservableCollection(Of String)

            ' Auswahlmenü für Filterliste auf Seite 1
            CBFilterList = New Dictionary(Of Integer, String)
            CBFilterList.Add(0, "Aktuelle")
            CBFilterList.Add(1, "Alle")
            CBFilterList.Add(2, "Vertragszahlungen")


            ' Ist eine externe Datenquelle festgelegt
            _ExtUrl = GlobalSettingService.HoleEinstellung("externalcustomersource", True)


            'Sepa?
            _sepa = GlobalSettingService.HoleEinstellung("SEPA")


            ' Füllen der Kurzwahlbutton in der oberen Leiste
            DefaultCustomers = New ObservableCollection(Of In1Button)
            Using con = New in1Entities
                Dim query = From c In con.CustomerStates Where c.TemplateCustomerID > 0 Select c

                For Each itm In query
                    If Not IsNothing(itm.TemplateCustomerID) Then
                        Dim rb As New In1Button
                        rb.Command = New RelayCommand(AddressOf DefaultCustomer)
                        rb.CommandParameter = itm.TemplateCustomerID
                        rb.Content = itm.CustomerStatename
                        rb.ToolTip = itm.CustomerStatename
                        rb.IconSource = MahApps.Metro.IconPacks.PackIconMaterialKind.Account
                        DefaultCustomers.Add(rb)
                    End If
                Next

                ' Categories Lookup für Verkaufsmaske
                Dim cquery = From c In con.Categories Order By c.CategoryID
                CategoriesLookup = cquery.ToList
                ' Ergänze Alle zu Beginn der Liste
                Dim cat As New Category
                cat.CategoryID = 9999
                cat.CategoryName = "Alle"
                CategoriesLookup.Insert(0, cat)
                ' Erste Kategorie auswählen
                SelectedCategory = CategoriesLookup.First



            End Using





            GetCustomerCommand = New RelayCommand(AddressOf GetCustomer)
            ReadCardCommand = New RelayCommand(AddressOf ReadCard)
            ClearCustomerCommand = New RelayCommand(AddressOf ClearCustomer)
            NavigateForwardCommand = New RelayCommand(AddressOf NavigateForward, AddressOf CanNavigateForward)
            NavigateKassierenCommand = New RelayCommand(AddressOf NavigateKassieren)
            StornoCommand = New RelayCommand(AddressOf Storno, AddressOf CanStorno)
            NewCustomerCommand = New RelayCommand(AddressOf NewCustomer)
            CustomerDetailCommand = New RelayCommand(AddressOf CustomerDetail, AddressOf CanCustomerDetail)
            'UpdateCommand = New RelayCommand(AddressOf Summe_berechnen)
            AboabtragenCommand = New RelayCommand(AddressOf Aboabtragen, AddressOf CanAboabtragen)
            AboprotokollCommand = New RelayCommand(AddressOf Aboprotokoll, AddressOf CanAboprotokoll)
            ClosePopupCommand = New RelayCommand(AddressOf ClosePopup)
            DelTerminCommand = New RelayCommand(AddressOf DelTermin)

            'DefaultCustomerCommand = New RelayCommand(AddressOf DefaultCustomer)


            CartAddCommand = New RelayCommand(AddressOf CartAdd)
            CartDelCommand = New RelayCommand(AddressOf CartDel)
            CartWaitinglistCommand = New RelayCommand(AddressOf CartWaitinglistAction)
            SearchCommand = New RelayCommand(AddressOf RefreshProduct)

            PayCommand = New RelayCommand(AddressOf KassierenBar, AddressOf CanKassierenBar)
            PayECCommand = New RelayCommand(AddressOf KassierenECCash, AddressOf CanKassierenECCash)
            PayLSCommand = New RelayCommand(AddressOf KassierenLS, AddressOf CanKassierenLS)

            NumberCommand = New RelayCommand(AddressOf Number_Pressed)
            LastBonCommand = New RelayCommand(AddressOf LastBon, AddressOf CanLastBon)

            SearchKeyCommand = New RelayCommand(AddressOf SearchKey)

            IsVerkaufVisible = Visibility.Hidden


            _context = New in1Entities

            Warenkorbfilter = New Dictionary(Of Boolean, String)
            Warenkorbfilter.Add(True, "Nur dieser Kassenvorgang")
            Warenkorbfilter.Add(False, "Alle offenen Buchungen des Kunden")
            SelectedWarenkorbfilter = True


            Init()


        End Sub

        Private Sub Init()

            Using con As New in1Entities

                If System.Threading.Thread.CurrentPrincipal.IsInRole("Infopoint Extended") Then
                    'Accounts = (From c In con.Accounts Where c.Kasse = True Select c).ToList
                    'Lastschrift erlauben
                    _allowLS = True
                Else
                    'Accounts = (From c In con.Accounts Where c.Kasse = True And c.AccountID = _accountid Select c).ToList
                End If


                'LVAccounts = CType(CollectionViewSource.GetDefaultView(Accounts.ToList), ListCollectionView)

                'Ist eine Kasse geöffnet?
                'Dim cashopen = Repositories.CashClosingRepository.IsCashOpen(_accountid)


                _defaultpaymentmethod = con.PaymentMethods.FirstOrDefault().PaymentMethodID

                Termlist = (From c In con.Terms Where c.Aktiv = True).OrderByDescending(Function(o) o.Semesterbeginn).ToList
            End Using

            If Termlist.Count = 0 Then
                Dim ds As New DialogService
                ds.ShowAlert("Semester", "Es ist kein Semester auf aktiv geschaltet")
                ds = Nothing

            End If

            SelectedTerm = (From c In Termlist Where DateDiff(DateInterval.Day, c.Semesterbeginn, Now) >= 0 And DateDiff(DateInterval.Day, Now, c.Semesterende) >= 0).FirstOrDefault

            If SelectedTerm Is Nothing Then
                SelectedTerm = Termlist.FirstOrDefault
            End If

            If Not Trim(MachineSettingService.GetMachineSetting(MachineSettingService.in1MaschineSettings.TerminalIP)) = "" Then
                _canECCash = True
            End If

            _defaultaccount = _accountid

            KasseNr = MachineSettingService.GetKasseNr()

            _cashclosingid = Repositories.CashClosingRepository.GetCashClosingID(KasseNr)

        End Sub

#End Region

        Private Sub GetCustomer()
            ClearCustomer()
            Dim search = ServiceLocator.Current.GetInstance(Of ISearchCustomer)()
            Dim cust = search.GetCustomer
            If Not IsNothing(cust) Then
                LoadCustomer(cust.CustomerID)
            End If
        End Sub

        Private Sub DefaultCustomer(ByVal param As Object)
            ClearCustomer()
            Dim prm = CType(param, Integer)
            If Not IsNothing(prm) Then
                LoadCustomer(prm)
            End If
        End Sub

        Private Sub LoadCustomer(ByVal customerID As Long)

            IsBusy = True
            CBFilter = 0
            Dim wc As New WaitCursor
            Try
                _context = New in1Entities


                Customer = _context.Customers.Find(customerID)


                ' Backgroundworker zum laden der externen Customer Infos (JSON)
                'bgw = New BackgroundWorker
                If Not bgw.IsBusy Then
                    bgw.RunWorkerAsync()
                End If


                ' Vertragsinfos abfragen
                GetVertragsinfo()
                GetUebungsleiter()


                LoadBillings()

            Catch ex As Exception
                If Not IsNothing(wc) Then wc.Dispose()
                MsgBox(ex.Message)

            Finally
                IsBusy = False
                If Not IsNothing(wc) Then wc.Dispose()
            End Try
        End Sub

        Private Sub LoadBillings()
            ' Customer Historie laden im Backgroundworker
            If Not IsNothing(Customer) Then
                IsVerkaufVisible = Visibility.Visible
                'bgw_Billings = New BackgroundWorker
                If Not bgw_Billings.IsBusy Then
                    IsBgwBusy = True
                    bgw_Billings.RunWorkerAsync()
                End If
            End If
        End Sub

        Private Sub bgw_Billings_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw_Billings.DoWork
            Try

                Using con As New in1Entities


                    '_context.Billings.Where(Function(o) o.CustomerID = Customer.CustomerID).OrderByDescending(Function(o) o.Rechnungsdatum).Take(200).Load()
                    Dim repo As New Repositories.BillingRepository(con)

                    e.Result = repo.GetAllBillings(Customer.CustomerID, 400)
                End Using
            Catch ex As Exception
                ' Wenn zu schnell Kundenwechsel, dann hier Fehler
                'IsBgwBusy = False
            End Try
        End Sub

        Private Sub bgw_Billings_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles bgw_Billings.RunWorkerCompleted
            If IsNothing(e.Result) Then Return
            OCBillings = e.Result
            LVBillings = New ListCollectionView(OCBillings)


            LVCart = New ListCollectionView(OCBillings)

            For Each bill In OCBillings
                If bill.BillingStatus = BillingStates.Offen Then
                    If bill.KasseNr = KasseNr Then bill.Checked = True

                End If
                AddHandler bill.PropertyChanged, AddressOf Summe_berechnen
            Next

            LVBillings.Filter = New Predicate(Of Object)(AddressOf Billingfilter)
            AddHandler OCBillings.CollectionChanged, AddressOf Summe_berechnen
            LVCart.Filter = New Predicate(Of Object)(AddressOf OpenBillingsfilter)
            IsBgwBusy = False
        End Sub

        Private Sub GetVertragsinfo()
            Dim query = From c In _context.Contracts Where c.CustomerID = Customer.CustomerID Select c

            Vertragsinfo.Clear()
            Dim Vertrag As Contract = Nothing
            Dim str As String = ""
            If query.Any Then
                For Each itm In query
                    If DateDiff(DateInterval.Day, itm.Vertragsbeginn, Now) >= 0 And
                        DateDiff(DateInterval.Day, Now, itm.Vertragsende) >= 0 Then
                        Vertrag = itm
                        str = Vertrag.ContractRate.ContractRateName & " " & Vertrag.Vertragsbeginn.ToShortDateString & " - " & Vertrag.Vertragsende.ToShortDateString
                        If Not IsNothing(Vertrag.Kuendigungzum) Or IsNothing(Vertrag.Verlaengerungsmonate) OrElse Vertrag.Verlaengerungsmonate = 0 Then
                            str = str & " (gekündigt)"
                        End If
                        Vertragsinfo.Add(str)
                    End If
                Next

            End If
        End Sub

        Private Sub GetUebungsleiter()

            Uebungsleiter = ""
            Dim dtfrom = Now.Date


            Dim query = From c In _context.StaffContracts Where c.CustomerID = Customer.CustomerID And c.Beginn <= dtfrom And c.Ende > dtfrom Select c

            If query.Any Then
                Uebungsleiter = "Übungsleiter (Dienstvertrag vom " & query.FirstOrDefault.Beginn.ToShortDateString & " - " & query.FirstOrDefault.Ende.ToShortDateString & ")"
            End If

        End Sub

        Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw.DoWork


            ' External Datenquelle abfragen
            Dim card As String = ""
            If Not IsNothing(Customer.Kartennummer) Then
                card = Customer.Kartennummer
            End If


            Dim ext As New ExternalCustomerSource
            Dim info = ext.GetData(_ExtUrl, "id=" & Customer.CustomerID & "&card=" & card)
            If Not IsNothing(info) Then
                e.Result = info
            End If

        End Sub

        Private Sub bgw_Completed(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles bgw.RunWorkerCompleted
            If Not IsNothing(e.Result) Then

                External = e.Result
            Else
                External = "Problem bei abrufen der externen Datenquelle"
            End If
        End Sub


        Private Sub RefreshCart()
            If Not IsNothing(LVCart) Then
                LVCart.Refresh()
            End If
        End Sub

        Private Sub RefreshBillings()
            If Not IsNothing(LVBillings) Then
                LVBillings.Refresh()
            End If
        End Sub

        Private Sub RefreshProduct()
            SelectedCategory = CategoriesLookup.First
            LVProducts.Refresh()
        End Sub

        Private Sub GotoOrderAction()
            If Not IsNothing(Customer) Then
                Dim params As New NavigationParameters()
                params.Add("Customer", Customer)

                _regionmanager.RequestNavigate(RegionNames.MainRegion, New Uri("OrderView", UriKind.Relative), params)
            End If
        End Sub

        Private Sub LoadProducts()
            _productlist = New ObservableCollection(Of Productlistitem)

            'Dim courses = From c In _context.CoursePrices Where c.CustomerStateID = Customer.CustomerStateID

            Dim courses = From c In _context.Courses Join p In _context.CoursePrices On c.CourseID Equals p.CourseID
                          Where p.CustomerStateID = Customer.CustomerStateID And c.TermID = SelectedTerm.TermID _
                              And c.CourseRegistration.Shop > 0 And c.Aktiv = True
                          Select New With {.CourseID = c.CourseID, .Sportname = c.Sport.Sportname, .Zusatzinfo = c.Zusatzinfo, c.Faelligkeit,
                                          .Zeittext = c.Zeittext, .DtStart = c.DTStart, .maxTN = c.maxTN, .Preis = p.Preis, .CostcenterID = c.CostcenterID,
                                            .ProductTypeID = c.ProductTypeID, .CategoryID = c.CategoryID,
                                            .Anmeldungvon = p.Anmeldungvon,
                                            .Anmeldungbis = p.Anmeldungbis,
                                            .Anmeldungerlaubt = p.Anmeldungerlaubt,
                                            p.TaxPercent,
                                            p.Innenauftrag,
                                            p.Sachkonto,
                                            p.KSTextern,
                                            p.Steuerkennzeichen,
                                            .pCostcenterID = p.CostcenterID,
                                           .Belegt = (From a In _context.Billings Where a.CourseID = c.CourseID And (a.IsStorno = False) Select a.CourseID).Count}

            If courses.Any Then
                For Each itm In courses
                    Dim prod As New Productlistitem
                    prod.Gruppe = "K"
                    prod.Artikelbezeichnung = itm.Sportname & " " & itm.Zusatzinfo
                    prod.Zusatzinfo = itm.Zeittext & " (Start: " & itm.DtStart.ToShortDateString & ")"
                    prod.Preis = itm.Preis
                    prod.TaxPercent = itm.TaxPercent
                    prod.CategoryID = itm.CategoryID
                    prod.CourseID = itm.CourseID
                    If Not IsNothing(itm.pCostcenterID) Then
                        prod.CostcenterID = itm.pCostcenterID
                    Else
                        prod.CostcenterID = itm.CostcenterID
                    End If

                    prod.ProductTypeID = itm.ProductTypeID
                    prod.Innenauftrag = itm.Innenauftrag
                    prod.Sachkonto = itm.Sachkonto
                    prod.KSTextern = itm.KSTextern
                    prod.Steuerkennzeichen = itm.Steuerkennzeichen

                    'Anmeldungerlaubt
                    prod.Showanmeldezeitraum = True
                    prod.Anmeldungvon = itm.Anmeldungvon
                    prod.Anmeldungbis = itm.Anmeldungbis
                    prod.Anmeldungerlaubt = False
                    If itm.Anmeldungvon < DateTime.Now And itm.Anmeldungbis > DateTime.Now Then
                        prod.Anmeldungerlaubt = True
                    End If
                    If itm.Anmeldungerlaubt = False Then prod.Anmeldungerlaubt = False

                    If itm.maxTN > 0 Then
                        prod.Frei = itm.maxTN - itm.Belegt
                    End If

                    If Not IsNothing(itm.Faelligkeit) Then
                        prod.Faelligkeit = itm.Faelligkeit
                    Else
                        prod.Faelligkeit = Now
                    End If
                    _productlist.Add(prod)
                Next
            End If


            Dim packages = From c In _context.Packages Join p In _context.PackagePrices On c.PackageID Equals p.PackageID
                           Where p.CustomerStateID = Customer.CustomerStateID And c.IsActive = True
                           Select New With {c.PackageID, c.PackageName, p.Preis, p.TaxPercent, c.Zutritte,
                               c.CostcenterID, c.ProductTypeID, c.CategoryID, p.Innenauftrag, p.Sachkonto,
                               .pCostcenterID = p.CostcenterID, p.KSTextern, p.Steuerkennzeichen
                               }


            For Each itm In packages
                Dim prod As New Productlistitem
                prod.Gruppe = "P"
                prod.Artikelbezeichnung = itm.PackageName
                prod.Preis = itm.Preis
                prod.TaxPercent = itm.TaxPercent
                prod.Innenauftrag = itm.Innenauftrag
                prod.Sachkonto = itm.Sachkonto
                prod.KSTextern = itm.KSTextern
                prod.Steuerkennzeichen = itm.Steuerkennzeichen

                If Not IsNothing(itm.pCostcenterID) Then
                    prod.CostcenterID = itm.pCostcenterID
                Else
                    prod.CostcenterID = itm.CostcenterID
                End If

                prod.PackageID = itm.PackageID
                prod.Packagecredit = itm.Zutritte
                prod.ProductTypeID = itm.ProductTypeID
                prod.CategoryID = itm.CategoryID
                prod.Faelligkeit = Now
                _productlist.Add(prod)
            Next

            LVProducts = CollectionViewSource.GetDefaultView(_productlist)
            LVProducts.SortDescriptions.Add(New SortDescription("Artikelbezeichnung", ListSortDirection.Ascending))
            LVProducts.Filter = New Predicate(Of Object)(AddressOf Productsfilter)
            RaisePropertyChanged("LVProducts")
            Searchterm = ""
        End Sub

        Private Function Productsfilter(ByVal item As Object) As Boolean
            Dim itm = TryCast(item, Productlistitem)
            If itm IsNot Nothing Then
                If IsNothing(_Searchterm) OrElse Trim(_Searchterm) = "" Then
                    If itm.CategoryID = SelectedCategory.CategoryID Or SelectedCategory.CategoryID = 9999 Then
                        Return True
                    End If
                Else
                    If itm.Artikelbezeichnung.ToLower.Contains(_Searchterm.ToLower) Then Return True
                End If
            End If
            Return False
        End Function

        Private Function Accountsfilter(ByVal item As Object) As Boolean
            Dim itm = TryCast(item, Account)

            If itm IsNot Nothing Then
                If itm.AccountID < 1800 Then Return True
                If Not IsNothing(Customer) Then
                    If _sepa = 1 Then

                        If Not IsNothing(Customer.BIC) And Not IsNothing(Customer.IBAN) And Customer.LSVEnabled = True Then
                            If Len(Customer.BIC) >= 8 And Len(Customer.IBAN) > 10 Then Return True
                        End If
                    Else

                    End If

                End If
                Return False
            End If
            Return False
        End Function


        Private Sub ClearCustomer()
            Try
                'Offene aus dem Kassenvorgang stornieren
                'If Not IsNothing(OCBillings) Then
                '    Dim bal As New Billing_Bal(MyGlobals.CurrentUsername)

                '    For Each itm In OCBillings
                '        If itm.BillingStatus = BillingStates.Offen AndAlso itm.KasseNr = KasseNr Then
                '            bal.Storno(itm.BillingID, KasseNr)
                '        End If
                '    Next
                'End If



                Kartenstatus = ""
                Vertragsinfo.Clear()
                Uebungsleiter = ""
                External = ""
                PopupOpen = Visibility.Hidden
                _context.Dispose()
                Customer = Nothing
                OCBillings = Nothing
                LVBillings = Nothing
                LVCart = Nothing
                SelectedTabIndex = 0
                IsVerkaufVisible = Visibility.Hidden
                SelectedWarenkorbfilter = True
                Summe = 0
                Gegeben = 0
                Differenz = 0
                SetFocus = True
                SetFocus = False
            Catch ex As Exception
                Throw New Exception("Fehler bei ClearCustomer")
            End Try
        End Sub

        ''' <summary>
        ''' Aktualisiert Summe und gibt Anzahl der Posten zurück
        ''' </summary>
        ''' <returns></returns>
        Private Function Summe_berechnen() As Long
            Dim lSumme As Decimal = 0
            Dim itemcount = 0
            If Not IsNothing(OCBillings) Then


                For Each itm In OCBillings
                    If itm.Checked = True Then
                        lSumme = lSumme + itm.Brutto
                        itemcount = itemcount + 1
                    End If
                Next
            End If
            Summe = lSumme
            Return itemcount
        End Function

        Private Sub NavigateForward()
            LoadProducts()
            SelectedTabIndex = 1
            'LoadAccounts()

        End Sub



        Private Function CanNavigateForward(ByVal param As Object) As Boolean
            If Not IsNothing(Customer) Then Return True

            Return False
        End Function

        Private Sub NavigateKassieren()
            SelectedTabIndex = 2
        End Sub

        Private Sub ReadCard()
            ClearCustomer()
            Dim cls As New QuioCV6600
            _card = cls.Karte_lesen
            If _card > 100 Then
                Using con As New in1Entities
                    Dim query = From c In con.Customers Where c.Kartennummer = _card Select c.CustomerID
                    If Not IsNothing(query) Then
                        If query.Count = 1 Then
                            LoadCustomer(query.First())
                        ElseIf query.Count > 1 Then
                            Kartenstatus = "ACHTUNG: Diese Karte ist mehrfach vergeben"
                        Else
                            Kartenstatus = "Kein Kunde mit dieser Karte gefunden"
                        End If
                    End If
                End Using
            Else
                Kartenstatus = cls.Errortext
            End If
            cls = Nothing



        End Sub

        Private Sub SearchKey()
            ClearCustomer()
            If Not IsNothing(Key) AndAlso IsNumeric(Key) Then
                Dim pkey As String = ""
                pkey = Key
                Key = ""
                Using con As New in1Entities

                    Dim cid = con.Customers.Where(Function(o) o.Kartennummer = pkey).FirstOrDefault
                    If Not IsNothing(cid) Then
                        LoadCustomer(cid.CustomerID)
                    Else
                        Kartenstatus = "Kein Kunde mit dieser Karte gefunden"
                    End If


                End Using
            End If
            Key = ""
        End Sub
        ''' <summary>
        '''  Zeigt nur die Rechnungen des Kunden an, denen eine CourseID, eine PackageID zugeordnet ist.
        ''' 
        ''' </summary>
        ''' <param name="item"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function Billingfilter(ByVal item As Object) As Boolean
            Dim itm = TryCast(item, Billing)
            If itm IsNot Nothing Then

                ' Stornierte immer ausblenden
                If itm.IsStorno = True Then Return False


                ' Wenn Filter zwei und es ist eine Vertragszahlung, dann einblenden
                If CBFilter = 2 Then
                    If Not IsNothing(itm.ContractID) Then Return True
                Else
                    'Vetragszahlungen ausblenden
                    If Not IsNothing(itm.ContractID) Then Return False

                    ' Wenn bezahlt innerhalb der letzten 7 Tage anzeigen
                    If Not IsNothing(itm.Buchungsdatum) AndAlso itm.Buchungsdatum > Now.AddDays(-5) Then
                        Return True
                    End If

                    ' Punktekarten mit Guthaben immer anzeigen
                    If itm.Packagecredit > 0 Then
                        Return True
                    End If

                    'Gebuchte Kurse immer anzeigen
                    If Not IsNothing(itm.CourseID) Then Return True

                    ' Nur bei alle Rest anzeigen
                    If CBFilter = 1 Then
                        Return True
                    End If
                End If

            End If
            Return False
        End Function

        ''' <summary>
        ''' Zeigt die offenen Rechnungen im Warenkorb an
        ''' entweder nur die, die soeben in den Warenkorb gelegt wurde
        ''' oder alle offenen, wenn SelectedWarenkorbfilter = false ist
        ''' </summary>
        ''' <param name="item"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function OpenBillingsfilter(ByVal item As Object) As Boolean
            Dim itm = TryCast(item, Billing)
            If itm IsNot Nothing Then
                If SelectedWarenkorbfilter = True Then
                    If itm.KasseNr = KasseNr AndAlso itm.BillingStatus = BillingStates.Offen Then
                        Return True
                    End If
                Else
                    If itm.BillingStatus = BillingStates.Offen Then Return True
                End If


            End If
            Return False
        End Function


        ''' <summary>
        ''' Bei Storno wird bei der gewählten Rechnung die CourseID, die PackageID oder die BillingID gelöscht
        ''' Wurde die Rechnung bereits bezahlt, dann wird eine Minusbuchung erzeugt
        ''' Wurde die Rechnung noch nicht bezahlt/abgebucht, dann wird sie komplett gelöscht
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub Storno()

            Dim dlg As New DialogService

            If Not IsNothing(SelectedBill.PackageID) Then
                Dim query = (From c In _context.PackageLogs Where c.BillingID = SelectedBill.BillingID).Count
                If query > 0 Then
                    dlg.ShowAlert("Abo", "Bei dieser Abokarte wurden bereits Punkte abgetragen. Storno nicht möglich")
                    Exit Sub
                End If

            End If

            If dlg.AskConfirmation("Storno", "Wollen Sie den gewählten Artikel wirklich stornieren?") = True Then
                Dim stornoMail = GlobalSettingService.HoleEinstellung("stornomail")

                'SelectedBill.CourseID = Nothing
                'SelectedBill.ProductID = Nothing
                'SelectedBill.PackageLogs = Nothing



                If (Not IsNothing(stornoMail) And stornoMail) AndAlso dlg.AskConfirmation("Storno Email versenden", "Wollen Sie eine Storno Benachrichtigung per Email versenden?") = True Then
                    SendStornoMail(SelectedBill)
                End If

                Dim bal As New Billing_Bal(MyGlobals.CurrentUsername)

                'Wenn schon bezahlt Minusbuchung
                If SelectedBill.BillingStatus = BillingStates.Bezahlt Then

                    Dim istorno = ServiceLocator.Current.GetInstance(Of IStorno)()
                    Dim reason = istorno.GetReason()

                    Using con As New in1Entities

                        Dim id = bal.Storno(SelectedBill.BillingID, _KasseNr, reason)

                        Dim oldbill = _context.Billings.Find(SelectedBill.BillingID)
                        oldbill.IsStorno = True
                        con.SaveChanges(MyGlobals.CurrentUsername)

                        LoadBillings()

                        'Dim newbill = _context.Billings.AsNoTracking.Where(Function(o) o.BillingID = SelectedBill.BillingID).FirstOrDefault
                        'newbill.StornoID = SelectedBill.BillingID
                        'newbill.IsStorno = True
                        'newbill.Verwendungszweck = "Storno: " & newbill.Verwendungszweck
                        'newbill.CanCartDel = True
                        'newbill.BillingStatus = BillingStates.Offen
                        'newbill.Brutto = newbill.Brutto * -1
                        'newbill.Rechnungsdatum = Now
                        'newbill.Faelligkeit = Now
                        'newbill.Kasse = True
                        'newbill.Checked = True
                        'newbill.Buchungsdatum = Nothing
                        'newbill.Vorgang = _aktbon
                        'newbill.KasseNr = KasseNr
                        'OCBillings.Add(newbill)

                    End Using




                    dlg.ShowInfo("Storno", "Die Stornobuchung wurde zur Auszahlung in den Warenkorb gelegt.")
                    dlg = Nothing
                ElseIf SelectedBill.BillingStatus = BillingStates.Offen Then
                    ' Offene Rechnungen werden direkt in den Status storniert gesetzt
                    Using context As New in1Entities
                        Dim repo As New Repositories.BillingRepository(context)
                        dlg.ShowInfo("Storno", "Diese Buchung wurde noch nicht bezahlt! Es erfolgt keine Auszahlung!")

                        'Original aus der Datenbank holen
                        Dim bill = context.Billings.Find(SelectedBill.BillingID)
                        repo.StornoOpenBilling(bill)
                        bill.KasseNr = MachineSettingService.GetKasseNr()

                        context.SaveChanges(MyGlobals.CurrentUsername)

                    End Using
                    'LoadCustomer(Customer.CustomerID)
                    LoadBillings()
                End If


                LVCart.Refresh()
                LVBillings.Refresh()
            End If
            dlg = Nothing
        End Sub

        Private Function CanStorno(ByVal param As Object) As Boolean
            If Not IsNothing(SelectedBill) Then
                If (SelectedBill.BillingStatus = BillingStates.Bezahlt Or SelectedBill.BillingStatus = BillingStates.Offen) _
                    And (SelectedBill.IsStorno = False Or IsNothing(SelectedBill.IsStorno)) Then
                    'And Not SelectedBill.PaymentMethodID = 4 Then
                    Return True
                End If
            End If


            Return False
        End Function

        Private Sub Aboabtragen()
            If Not IsNothing(SelectedBill) And Not IsNothing(SelectedBill.PackageID) Then
                Dim dlg As New DialogService
                If IsNothing(SelectedBill.Packagecredit) Then
                    dlg.ShowError("Fehler", "Restguthaben NULL. Bitte Administrator informieren")
                    Return
                End If
                If SelectedBill.Packagecredit > 0 Then
                    Using con As New in1Entities


                        Dim pl As New PackageLog
                        pl.BillingID = SelectedBill.BillingID
                        pl.Zeit = Now
                        con.PackageLogs.Add(pl)
                        Dim bill = (From c In con.Billings Where c.BillingID = SelectedBill.BillingID).FirstOrDefault
                        If Not IsNothing(bill) Then
                            If bill.Packagecredit > 0 Then
                                bill.Packagecredit = bill.Packagecredit - 1
                            Else
                                bill.Packagecredit = 0
                            End If
                            con.SaveChanges()
                            dlg.ShowInfo("Abo", "Ein Punkt von der Abokarte abgetragen")
                            LoadBillings()
                        End If
                    End Using
                Else
                    dlg.ShowInfo("Abo", "Kein Guthaben mehr auf der Abokarte")
                End If
            End If
        End Sub

        Private Function CanAboabtragen() As Boolean
            If Not IsNothing(SelectedBill) Then
                If Not IsNothing(SelectedBill.PackageID) Then
                    If Not IsNothing(SelectedBill.Packagecredit) AndAlso SelectedBill.Packagecredit > 0 Then
                        Return True
                    End If
                End If
            End If
            Return False
        End Function

        Private Sub DelTermin()
            If Not IsNothing(SelectedPackagelog) Then
                Dim dlg As New DialogService
                If dlg.AskConfirmation("Termin löschen", "Soll dieser Termin wieder gutgeschrieben werden") = True Then
                    Dim con As New in1Entities
                    Dim p = (From c In con.PackageLogs Where c.PackageLogID = SelectedPackagelog.PackageLogID)
                    If Not IsNothing(p.First) Then
                        con.PackageLogs.Remove(p.First)
                        Dim bill = (From c In con.Billings Where c.BillingID = SelectedBill.BillingID)
                        If bill.Any Then
                            bill.First.Packagecredit = bill.First.Packagecredit + 1
                        End If
                        con.SaveChanges()
                        '_context.Entry(SelectedBill).Reload()
                        ShowPackagelog()
                    End If
                End If
            End If
            LoadBillings()
        End Sub

        Private Sub ShowPackagelog()
            Using con As New in1Entities
                Dim pl = (From c In con.PackageLogs Where c.BillingID = SelectedBill.BillingID).ToList
                Packagelogs = pl
            End Using
            RaisePropertyChanged("PackageLogs")
        End Sub

        Private Sub Aboprotokoll()
            ShowPackagelog()
            PopupOpen = Visibility.Visible
        End Sub

        Private Sub ClosePopup()
            PopupOpen = Visibility.Hidden
        End Sub


        Private Function CanAboprotokoll() As Boolean
            If Not IsNothing(SelectedBill) Then
                If Not IsNothing(SelectedBill.PackageID) Then Return True
            End If

            Return False
        End Function

        Private Sub NewCustomer()
            ClearCustomer()

            Dim cs = ServiceLocator.Current.GetInstance(Of ICustomerService)()
            Dim id = cs.GetNewCustomerID

            If Not id Is Nothing And id > 0 Then
                LoadCustomer(id)
            End If

        End Sub

        Private Sub CustomerDetail()
            Dim cs = ServiceLocator.Current.GetInstance(Of ICustomerService)()
            If cs.ShowCustomer(Customer) = True Then
                LoadCustomer(Customer.CustomerID)
            End If
        End Sub

        Private Function CanCustomerDetail(ByVal param As Object) As Boolean
            If Not IsNothing(Customer) Then Return True
            Return False
        End Function

        '-------------------------------------------
        '   Verkaufsmaske
        Private Sub CartAdd(ByVal selected As Object)

            If IsNothing(selected) Then
                Return
            End If

            SelectedProduct = selected
            If Not IsNothing(SelectedProduct) Then

                Dim bal As New Billing_Bal(MyGlobals.CurrentUsername)
                Dim res As Boolean = False
                If Not IsNothing(SelectedProduct.CourseID) Then
                    res = bal.AddBillingFromCourse(Customer.CustomerID, SelectedProduct.CourseID, _defaultaccount,
                                                   _defaultpaymentmethod, KasseNr)
                ElseIf Not IsNothing(SelectedProduct.PackageID) Then
                    res = bal.AddBillingFromPackage(Customer.CustomerID, SelectedProduct.PackageID, _defaultaccount,
                                                    _defaultpaymentmethod, KasseNr)
                End If

                'Dim bill As New Billing
                'bill.Kasse = True
                'bill.IsStorno = False
                'bill.CanCartDel = True
                'bill.CustomerID = Customer.CustomerID
                'bill.CustomerStateID = Customer.CustomerStateID
                'bill.BillingStatus = "O"
                'bill.Rechnungsdatum = Now
                'bill.ProductTypeID = SelectedProduct.ProductTypeID
                'bill.Faelligkeit = SelectedProduct.Faelligkeit
                'bill.Verwendungszweck = Left(SelectedProduct.Artikelbezeichnung & " " & SelectedProduct.Zusatzinfo, 250)
                'bill.Checked = True

                'bill.CostcenterID = SelectedProduct.CostcenterID
                'bill.Innenauftrag = SelectedProduct.Innenauftrag
                'bill.Sachkonto = SelectedProduct.Sachkonto
                'bill.KSTextern = SelectedProduct.KSTextern
                'bill.Steuerkennzeichen = SelectedProduct.Steuerkennzeichen

                'bill.Packagecredit = SelectedProduct.Packagecredit
                'bill.CourseID = SelectedProduct.CourseID
                'bill.ProductID = SelectedProduct.ProductID
                'bill.PackageID = SelectedProduct.PackageID
                'bill.Brutto = SelectedProduct.Preis
                'bill.Steuerprozent = SelectedProduct.TaxPercent


                'AddHandler bill.PropertyChanged, AddressOf Summe_berechnen

                'OCBillings.Add(bill)
                LoadBillings()
                'LVCart.Refresh()

            End If
        End Sub

        Private Sub CartDel()
            If Not IsNothing(SelectedCart) Then


                Dim bal As New Billing_Bal(MyGlobals.CurrentUsername)

                Dim res = bal.Storno(SelectedCart.BillingID, KasseNr)

                LoadBillings()
            End If
        End Sub

        Private Sub CartWaitinglistAction()
            If Not IsNothing(SelectedCart) AndAlso Not IsNothing(SelectedCart.CourseID) Then
                Dim wl As New Waitinglist
                wl.CourseID = SelectedCart.CourseID
                wl.CustomerID = SelectedCart.CustomerID
                wl.Timestamp = Now
                Using con As New in1Entities
                    con.Waitinglists.Add(wl)
                    con.SaveChanges()
                    Dim dlg As New DialogService
                    dlg.ShowInfo("Warteliste", "Der Kunde wurde zur Warteliste hinzugefügt")
                End Using
                Me.CartDel()
            End If
        End Sub

        '--------- Kassieren Tabitem


        ' Prüfe überbuchung

        Private Function CheckCourseFull() As Boolean
            Dim kursvoll As Boolean = False
            '1. Nochmal prüfen, on wirklich noch Plätze frei
            For Each itm In OCBillings
                If itm.Checked = True Then
                    If Not IsNothing(itm.CourseID) Then
                        Using con As New in1Entities
                            Dim kurs = con.Courses.Find(itm.CourseID)

                            If Not IsNothing(kurs.maxTN) Then
                                If kurs.maxTN > 0 Then

                                    Dim tn = (kurs.Billings.Where(Function(o) o.IsStorno = False)).Count

                                    If tn > kurs.maxTN Then
                                        Dim dlg As New DialogService
                                        If dlg.AskConfirmation("Kurs voll", "Der Kursplatz im Kurs " & kurs.Sport.Sportname & " wurde leider mittlerweile bereits anderweitig vergeben" &
                                                     " Wollen Sie den Kurs wirklich überbuchen? Wenn nicht, entfernen Sie den Kurs aus dem Warenkorb!") = False Then
                                            kursvoll = True
                                        End If
                                    End If

                                End If
                            End If
                        End Using
                    End If
                End If
            Next
            Return kursvoll
        End Function

        Private Sub Kassieren(Optional ByVal method As Integer = 1, Optional ByVal paymentinfo As String = "", Optional receipt As String = "")

            Dim summe As Double = 0
            'Account = CLng(param)
            Dim kursvoll As Boolean = False

            Dim kassenr As Integer = MachineSettingService.GetKasseNr()

            Dim bal As New Billing_Bal(MyGlobals.CurrentUsername)

            Try

                Dim accountid = 0
                If method = 1 Then ' Wenn Barzahlung Machinesettings holen
                    accountid = MachineSettingService.GetMachineSetting(MachineSettingService.in1MaschineSettings.Kasse)
                ElseIf method = 2 Or method = 3 Then ' Wenn LS, Überweisung das standardkonto laden
                    accountid = GlobalSettingService.HoleEinstellung("defaultaccount")
                ElseIf method = 4 Then
                    accountid = GlobalSettingService.HoleEinstellung("pos.eccashaccountid")
                End If

                Dim i As Integer
                ' Guid für den Vorgang
                _aktbon = Guid.NewGuid



                Dim list As New List(Of Billing)


                For i = OCBillings.Count - 1 To 0 Step -1
                    Dim bill = OCBillings(i)
                    If bill.Checked = True Then


                        list.Add(bill)
                        OCBillings.Remove(bill)

                        'bill.AccountID = accountid
                        '    bill.KasseNr = kassenr


                        '    bill.Vorgang = _aktbon
                        '    If Not receipt = "" Then
                        '        bill.Bemerkung = receipt
                        '    End If

                        '    If method = 1 Then 'Barzahlung alles auf bezahlt setzen
                        '        bill.Buchungsdatum = Now
                        '        bill.BillingStatus = BillingStates.Bezahlt
                        '        bill.PaymentMethodID = method
                        '        bill.CashClosingID = _cashclosingid

                        '    ElseIf method = 2 Then ' Lastschrift Then Zahlungsen offen stehen lassen beim Kunden
                        '        bill.BillingStatus = BillingStates.Offen
                        '        bill.PaymentMethodID = method

                        '    ElseIf method = 4 Then 'EC Cash alles auf bezahlt
                        '        bill.Buchungsdatum = Now
                        '        bill.BillingStatus = BillingStates.Bezahlt
                        '        bill.PaymentMethodID = method
                        '        bill.CashClosingID = _cashclosingid

                        '    End If
                        '    bill.PaymentInformation = paymentinfo
                        '    summe = summe + bill.Brutto
                        '    bill.Kasse = False

                    Else
                        'Infopoint hinzugefügte buchungen, die nicht selektiert sind löschen
                        If bill.Kasse Then
                            OCBillings.Remove(bill)
                        End If
                    End If


                Next



                '_context.SaveChanges(MyGlobals.CurrentUsername)



                _lastbon = bal.PayList(list, accountid, method, kassenr, paymentinfo)



                Dim bonsvc As New BonService(_lastbon, False, Customer.CustomerID)


                ClearCustomer()


            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End Sub

        Private Function ConfirmPayment(ByVal zart As String) As Boolean
            Dim dlg As New DialogService
            If dlg.AskConfirmation("Zahlung", "Wirklich Betrag " & zart & " kassieren?") = True Then
                Return True
            End If
            Return False
        End Function

        Private Sub KassierenBar()
            If ConfirmPayment("in BAR") = False Then Return
            If CheckCourseFull() Then Return
            Kassieren(1)
        End Sub

        Private Function CanKassierenBar() As Boolean
            If Not IsNothing(OCBillings) And Summe_berechnen() > 0 Then Return True
            Return False
        End Function

        Private Sub KassierenECCash()

            If ConfirmPayment("per EC Cash") = False Then Return
            If CheckCourseFull() Then Return

            Dim opiservice As New OPIService
            Dim res As Boolean = opiservice.Pay(Summe)

            '#If DEBUG Then
            '            res = True
            '#End If
            If res = True Then
                Kassieren(4, opiservice.Paymentinfo, opiservice.Receipt)
            Else
                If opiservice.ReturnCode = 1 Then
                    Dim dlg As New DialogService
                    Dim ans = dlg.AskConfirmation("EC Cash", "Die EC Buchung wurde mit Fehlern beendet. _
                                Soll die Zahlung trotzdem als bezahlt verbucht werden?")

                    If ans = True Then
                        Kassieren(4, "Fehler EC Gerät", "Returncode " & opiservice.ReturnCode & " " & opiservice.Message)
                    End If

                End If
            End If



        End Sub

        Private Function CanKassierenECCash() As Boolean
            If CanKassierenBar() AndAlso _canECCash _
                AndAlso Summe_berechnen() > 0 AndAlso Summe > 0 Then Return True
            Return False
        End Function

        Private Sub KassierenLS()
            If ConfirmPayment("per Lastschrift") = False Then Return
            If CheckCourseFull() Then Return
            Kassieren(2)
        End Sub

        Private Function CanKassierenLS() As Boolean
            If CanKassierenBar() AndAlso _allowLS = True Then
                If Not IsNothing(Customer.BIC) And Not IsNothing(Customer.IBAN) And Customer.LSVEnabled = True Then
                    If Len(Customer.BIC) >= 8 And Len(Customer.IBAN) > 10 Then Return True
                End If
            End If
            Return False
        End Function

        Private Sub Number_Pressed(ByVal param As String)

            If Not IsNothing(param) Then
                Dim sgeg As String = CStr(Gegeben)
                If sgeg = 0 Then sgeg = ""


                Dim arr = Split(sgeg, ",")
                If UBound(arr) > 0 Then
                    If Len(arr(1)) > 2 Then
                        Return
                    End If
                End If

                'sgeg = Replace(sgeg, ",", ".")
                If param = "C" Then
                    If Len(sgeg) > 0 Then _
                        sgeg = Mid(sgeg, 1, Len(sgeg) - 1)

                ElseIf param = "," Then
                    'Nur wenn noch kein Komma
                    If sgeg.IndexOf(",") = -1 Then _
                        sgeg = sgeg & ","
                Else
                    sgeg = sgeg & param
                End If
                If sgeg <> "" Then
                    Gegeben = sgeg
                Else
                    Gegeben = 0
                End If

            End If
        End Sub


        Private Sub UpdateRueckgeld()
            If Not IsNothing(Gegeben) And Not IsNothing(Summe) And IsNumeric(Gegeben) And IsNumeric(Summe) Then
                Differenz = Gegeben - Summe
            Else
                Differenz = 0
            End If

        End Sub
        Private Sub LastBon()
            If Not IsNothing(_lastbon) Then
                'Dim bon As New Bondruck
                'bon.LastBon(_lastbon)
                Dim bonsvc As New BonService(_lastbon, False, Customer.CustomerID)
                'bon = Nothing

            End If
        End Sub

        Private Function CanLastBon() As Boolean
            If Not IsNothing(_lastbon) And _lastbon <> Guid.Empty Then Return True
            Return False
        End Function

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
            If Not IsNothing(_context) Then
                _context.Dispose()
            End If
        End Sub

        Private Sub SendStornoMail(actBilling As Billing)
            If Not IsNothing(actBilling) And Not IsNothing(actBilling.Course) Then

                Dim mailAccount = New MailAccount()
                mailAccount.Host = GlobalSettingService.HoleEinstellung("smtphost", True)
                mailAccount.Port = GlobalSettingService.HoleEinstellung("smtpport", True)
                mailAccount.Username = GlobalSettingService.HoleEinstellung("smtplogin", True)
                mailAccount.Password = GlobalSettingService.HoleEinstellung("smtppw", True)
                mailAccount.Ssl = GlobalSettingService.HoleEinstellung("smtpssl", True)
                mailAccount.From = GlobalSettingService.HoleEinstellung("smtpfrom", True)
                If Not String.IsNullOrWhiteSpace(GlobalSettingService.HoleEinstellung("smtpbcc", True)) Then
                    mailAccount.Bcc = GlobalSettingService.HoleEinstellung("smtpbcc", True)
                End If

                'Dim mail As New clsMail
                _context.Templates.Load()

                'Dim Templatename As String = "Stornierung"
                Dim template As Template = _context.Templates.First(Function(o) o.TemplatetypeID = actBilling.Course.TemplateStornoID)
                Dim replacer As ReplaceMarker

                'For Each itm In Currentlistmembers
                If Not IsNothing(actBilling.Customer.Mail1) Then

                    Dim rpBilling = New BillingReplaceObject(actBilling).Output
                    Dim rpCourse = New CourseReplaceObject(actBilling.Course).Output
                    Dim rpCustomer = New CustomerReplaceObject(actBilling.Customer).Output

                    'Die Replacerlsiten zu einer zusammen fügen
                    Dim list = rpCourse.Concat(rpCustomer).Concat(rpBilling).ToDictionary(Function(kvp) kvp.Key, Function(kvp) kvp.Value)

                    Dim tText = template.TemplateText
                    replacer = New ReplaceMarker(tText, list)


                    replacer.DoReplacement()
                    Dim body As String = replacer.FinalString

                    replacer.Template = template.Betreff
                    replacer.DoReplacement()
                    Dim subject = replacer.FinalString

                    Dim inOneMail = New InOneEmail(actBilling.Customer.Mail1.ToString(), subject, body)
                    inOneMail.IsHtml = template.IsHtml

                    Dim mailer = New EmailDeliveryHelper(mailAccount, inOneMail)
                    mailer.SendAsync()
                End If
                '  Next
            End If
        End Sub




        Public Overloads Sub OnNavigatedTo(navigationContext As NavigationContext) Implements INavigationAware.OnNavigatedTo
            Init()
        End Sub

        Public ReadOnly Property KeepAlive As Boolean Implements IRegionMemberLifetime.KeepAlive
            Get
                Return False
            End Get
        End Property
    End Class
End Namespace
