﻿
Imports HSPinOne.Common
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports Prism.Regions
Imports System.Collections.ObjectModel
Imports System.Data.Entity

Namespace ViewModel
    Public Class CashZBonViewModel
        Inherits ViewModelBase
        Implements INavigationAware



        Private _from As DateTime
        Private _till As DateTime
        Private _kassenr As Integer

        Private _ccid As Integer

        Private _Billinglist As ObservableCollection(Of Billing)
        Public Property Billinglist() As ObservableCollection(Of Billing)
            Get
                Return _Billinglist
            End Get
            Set(ByVal value As ObservableCollection(Of Billing))
                _Billinglist = value
                RaisePropertyChanged("Billinglist")
            End Set
        End Property

        Private _ZList As ObservableCollection(Of ZBonItem) = New ObservableCollection(Of ZBonItem)
        Public Property ZList() As ObservableCollection(Of ZBonItem)
            Get
                Return _ZList
            End Get
            Set(ByVal value As ObservableCollection(Of ZBonItem))
                _ZList = value
                RaisePropertyChanged("ZList")
            End Set
        End Property

        Private _AbschlussVisible As Boolean = False
        Public Property AbschlussVisible() As Boolean
            Get
                Return _AbschlussVisible
            End Get
            Set(ByVal value As Boolean)
                _AbschlussVisible = value
                RaisePropertyChanged("AbschlussVisible")
            End Set
        End Property


        Property CloseCashCommand As New RelayCommand(AddressOf Abschluss)
        Property XBonCommand As New RelayCommand(AddressOf XBonAction)

        Public Event RequestClose()

        Private _context As in1Entities

        Public Sub New()


        End Sub

        Private Sub Init()
            _kassenr = MachineSettingService.GetKasseNr()
            _ccid = Repositories.CashClosingRepository.GetCashClosingID(_kassenr)
            LoadData()
            'Billinglist = list
            Calculate()
        End Sub

        Public Sub Show(ByVal ccid As Integer)
            _ccid = ccid
            LoadData()
            Calculate()
        End Sub



        Public Sub LoadData()

            If Not IsNothing(_context) Then _context.Dispose()
            _context = New in1Entities

            ZList.Clear()

            Dim cc = _context.CashClosings.Find(_ccid)
            If _kassenr = 0 Then
                _kassenr = cc.KasseNr
            End If
            If Not IsNothing(cc) AndAlso Not cc.Ende.HasValue Then
                AbschlussVisible = True
            Else
                AbschlussVisible = False
            End If

            Dim repo As New HSPinOne.DAL.Repositories.BillingRepository(_context)
            'repo.LoadCashReport(_kassenr)
            repo.LoadJournal(_ccid)

            Billinglist = _context.Billings.Local



        End Sub

        Private Sub Calculate()
            Dim list = Billinglist.ToList()


            'Gesamtumsatz nach Warengruppe und Kundenstatus
            Dim result = From c In list
                         Where c.BillingStatus = BillingStates.Bezahlt
                         Group c By c.CustomerStateID, c.ProductTypeID, c.CustomerState.CustomerStatename, c.ProductType.ProductTypeName Into Group
                         Select CustomerStateID, ProductTypeID, CustomerStatename, ProductTypeName, Summe = Group.Sum(Function(o) o.Brutto)
                         Order By ProductTypeID, CustomerStateID

            ZList.Add(New ZBonItem With {.Bezeichnung = "Gesamtumsatz", .Summe = result.Sum(Function(o) o.Summe)})
            For Each itm In result
                If Not itm.Summe = 0 Then
                    ZList.Add(New ZBonItem With {.Bezeichnung = " -" & itm.CustomerStatename & " " & itm.ProductTypeName, .Summe = itm.Summe})
                End If
            Next


            ' Zahlungsarten
            ZList.Add(New ZBonItem With {.Bezeichnung = "", .Summe = ""})
            Dim zarten = From c In list
                         Where c.BillingStatus = BillingStates.Bezahlt
                         Group c By c.PaymentMethodID, c.PaymentMethod.PaymentMethodname Into Group
                         Select PaymentMethodID, PaymentMethodname, Summe = Group.Sum(Function(o) o.Brutto),
                             Anzahl = Group.Count
                         Order By PaymentMethodID

            ZList.Add(New ZBonItem With {.Bezeichnung = "Zahlungsarten", .Summe = result.Sum(Function(o) o.Summe)})
            For Each itm In zarten
                ZList.Add(New ZBonItem With {.Bezeichnung = " -" & itm.PaymentMethodname, .Anzahl = itm.Anzahl, .Summe = itm.Summe})
            Next


            'Stornierungen
            ZList.Add(New ZBonItem With {.Bezeichnung = "", .Summe = ""})
            Dim stornos = From c In list
                          Where (c.BillingStatus = BillingStates.Storniert) And c.Brutto > 0
                          Group By c.KasseNr Into Group
                          Select KasseNr, Summe = Group.Sum(Function(o) o.Brutto),
                              Anzahl = Group.Count

            For Each itm In stornos
                ZList.Add(New ZBonItem With {.Bezeichnung = "Stornierungen", .Anzahl = itm.Anzahl, .Summe = itm.Summe})
            Next

            'Steuer
            ZList.Add(New ZBonItem With {.Bezeichnung = "", .Summe = ""})
            Dim taxes = From c In list
                        Where c.BillingStatus = BillingStates.Bezahlt
                        Group By c.Steuerprozent Into Group
                        Select Steuerprozent, Summe = Group.Sum(Function(o) o.Brutto)

            ZList.Add(New ZBonItem With {.Bezeichnung = "Steuersummen"})
            For Each itm In taxes
                ZList.Add(New ZBonItem With {.Bezeichnung = itm.Steuerprozent & "%", .Summe = itm.Summe})
            Next
        End Sub

        Private Sub XBonAction()
            Using con As New in1Entities
                Dim cc = con.CashClosings.Find(_ccid)
                PrintZBon(cc, False)
            End Using
        End Sub


        Private Sub Abschluss()

            Dim dlg As New DialogService

            If dlg.AskConfirmation("Abschluss", "Wirklich Kassensbschluss durchführen?") = False Then
                Return
            End If

            Using con As New in1Entities
                ' Wenn Kasse nicht offen, dann Kasse öffnen
                Dim cc = con.CashClosings.Find(_ccid)
                If IsNothing(cc) Then
                    cc = NewCashClosing()
                    con.CashClosings.Add(cc)
                End If

                If Not IsNothing(cc.Ende) Then
                    dlg.ShowInfo("Abschluss", "Kassenschluss nicht möglich, da er bereits durchgeführt wurde")
                    Return
                End If

                Dim pi As String = "" 'Paymentinfo
                Dim res As Boolean = False


                ' Wenn EC Terminal angeschlossen dann abschließen
                If Not Trim(MachineSettingService.GetMachineSetting(MachineSettingService.in1MaschineSettings.TerminalIP)) = "" Then
                    Dim opiservice As New OPIService
                    res = opiservice.Reconciliate()
                    dlg.ShowInfo("Terminal", "EC Abschluss durchgeführt")
                    pi = opiservice.Paymentinfo
                Else
                    res = True
                End If



                If res = True Then


                    cc.Ende = Now
                    cc.ClosedBy = MyGlobals.CurrentUsername
                    cc.ECBon = pi

                    cc.SummeBar = 0
                    cc.SummeKarte = 0

                    'Neue Kasse öffnen
                    Dim newcc = NewCashClosing()
                    con.CashClosings.Add(newcc)
                    con.SaveChanges()

                    'Dim repo As New Repositories.BillingRepository(con)
                    'repo.LoadJournal(SelectedKasse, dtfrom, dttill)
                    PrintZBon(cc, True)

                    Init()

                Else
                    dlg.ShowError("Terminal", "EC Abschluss nicht möglich")
                End If
            End Using

        End Sub

        Private Function NewCashClosing() As CashClosing
            Dim newcc As New CashClosing
            newcc.KasseNr = _kassenr
            newcc.Start = Now
            Return newcc
        End Function

        Private Sub PrintZBon(ByVal cc As CashClosing, Optional ByVal print As Boolean = True)
            Dim bon As New HSPinOne.Common.Bondruck
            Dim list = Billinglist.ToList
            Dim sums As New Dictionary(Of String, Double)

            'Dim kasse As Account = Accountlist.Where(Function(o) o.AccountID = SelectedAccount).FirstOrDefault


            bon.PrintHeader()

            bon.Feed()

            bon.PrintTextLine("Kassenbericht", Alignment.Center, True)

            bon.Feed()

            bon.PrintLeftandRight("Gedruckt am: ", Now.ToString())
            bon.PrintLeftandRight("Bericht von:", cc.Start.ToString())
            bon.PrintLeftandRight("Bericht bis:", cc.Ende.ToString())
            'bon.PrintLeftandRight("Konto:", kasse.AccountID & " " & kasse.AccountName)
            bon.PrintLeftandRight("Kasse: # ", cc.KasseNr.ToString)
            bon.PrintLeftandRight("Abschluss von:", cc.ClosedBy)
            bon.Feed()

            For Each itm In ZList
                Dim anz As String = ""
                If Not Trim(itm.Anzahl) = "" Then
                    anz = itm.Anzahl & " Stk "
                End If
                bon.PrintLeftandRight(itm.Bezeichnung, anz & itm.Summe)
            Next


            'bon.PrintCorrectness()
            Dim bondata = bon.Preview()
            If print Then
                bon.Cut()
                bon.Pulse()
                bon.Print()
            End If


            Templates.CashReportText.CreateReport(bondata)
            'bon.ClosePrinter()


            bon = Nothing
        End Sub


        Public Overrides Sub OnNavigatedTo(navigationContext As NavigationContext) Implements INavigationAware.OnNavigatedTo
            MyBase.OnNavigatedTo(navigationContext)
            Init()
        End Sub

    End Class





    Public Class ZBonItem
        Property Bezeichnung As String
        Property Anzahl As String
        Property Summe As String
    End Class
End Namespace

