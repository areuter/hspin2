﻿Imports HSPinOne.Infrastructure
Imports HSPinOne.DAL
Imports HSPinOne.Common
Imports System.Data.Entity
Imports Prism.Regions
Imports System.ComponentModel.Composition
Imports System.Windows.Input
Imports System.Windows.Media
Imports System.Windows
Imports System.Collections.ObjectModel


Namespace ViewModel

    Public Class CheckinCustomerViewModel
        Inherits ViewModelBase
        Implements INavigationAware


        Private _context As in1Entities

        Private _schluessel As String
        Public Property Schluessel As String
            Get
                Return _schluessel
            End Get
            Set(ByVal value As String)
                _schluessel = value
                RaisePropertyChanged("Schluessel")
                If DuplicateKey() Then
                    Duplikat = Visibility.Visible
                Else
                    Duplikat = Visibility.Collapsed
                End If
            End Set
        End Property

        Private _id As Integer
        Public Property ID() As Integer
            Get
                Return _id
            End Get
            Set(ByVal value As Integer)
                _id = value
                LoadData()
            End Set
        End Property



        Private _Letztercheckin As Date
        Public Property Letztercheckin As Date
            Get
                Return _Letztercheckin
            End Get
            Set(ByVal value As Date)
                _Letztercheckin = value
                RaisePropertyChanged("Letztercheckin")
            End Set
        End Property

        Private _cust As Customer
        Public Property Cust As Customer
            Get
                Return _cust
            End Get
            Set(ByVal value As Customer)
                _cust = value
                RaisePropertyChanged("Cust")

            End Set
        End Property

        Private _vertrag As Contract
        Public Property Vertrag As Contract
            Get
                Return _vertrag
            End Get
            Set(ByVal value As Contract)
                _vertrag = value
                RaisePropertyChanged("Vertrag")
            End Set
        End Property

        Private _Vertragsinfo As ObservableCollection(Of String) = New ObservableCollection(Of String)
        Public Property Vertragsinfo As ObservableCollection(Of String)
            Get
                Return _Vertragsinfo
            End Get
            Set(value As ObservableCollection(Of String))
                _Vertragsinfo = value
            End Set
        End Property


        Private _Foto As Byte()
        Public Property Foto As Byte()
            Get
                Return _Foto
            End Get
            Set(ByVal value As Byte())
                _Foto = value
                RaisePropertyChanged("Foto")
            End Set
        End Property

        Private _Duplikat As Visibility = Visibility.Collapsed
        Public Property Duplikat As Visibility
            Get
                Return _Duplikat
            End Get
            Set(value As Visibility)
                _Duplikat = value
                RaisePropertyChanged("Duplikat")
            End Set
        End Property

        Private _Abokarten As List(Of Billing) = New List(Of Billing)
        Public Property Abokarten() As List(Of Billing)
            Get
                Return _Abokarten
            End Get
            Set(ByVal value As List(Of Billing))
                _Abokarten = value
                RaisePropertyChanged("Abokarten")
            End Set
        End Property



        Private _schluesselliste As List(Of String)



        Public Property EnterCommand As ICommand
        Public Property ExitCommand As ICommand
        Public Property AbtragenCommand = New RelayCommand(AddressOf Aboabtragen)


        Private _regionmanager As IRegionManager


        Public Sub New()
            Me._regionmanager = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IRegionManager)
            EnterCommand = New RelayCommand(AddressOf EnterExecute, AddressOf CanEnterExecute)
            ExitCommand = New RelayCommand(AddressOf ExitExecute)
            _context = New in1Entities
            Vertragsinfo = New ObservableCollection(Of String)


            _context.CustomerStates.Load()
        End Sub

        Private Sub LadeSchluessel()

            Dim query = From c In _context.Checkins Select c.Schluessel

            _schluesselliste = New List(Of String)
            For Each item In query
                _schluesselliste.Add(item)
            Next


        End Sub

        Private Sub ExitExecute()
            _regionmanager.Regions(RegionNames.MainRegion).RequestNavigate(New Uri("CheckinView", UriKind.Relative))
        End Sub

        Private Sub EnterExecute()
            If DuplicateKey() Then Return
            Dim ch As New Checkin
            ch.Vorname = Cust.Vorname
            ch.Nachname = Cust.Nachname
            ch.CustomerID = Cust.CustomerID
            ch.CustomerStateID = Cust.CustomerStateID
            ch.Checkintime = Now
            ch.Schluessel = Schluessel
            ch.CheckinLocation = My.Settings.CheckinLocation
            If Not IsNothing(Vertrag) Then
                ch.Kennzeichen = Vertrag.Kennzeichen
            End If
            If Not IsNothing(Vertragsinfo) Then
                Dim vtext As String = ""
                For Each itm In Vertragsinfo
                    vtext = itm & " " & vtext
                Next
                ch.Vertragsart = Left(vtext, 250)


            End If

            Using context As New in1Entities
                context.Checkins.Add(ch)
                context.SaveChanges()

                'Offene Posten aus der Barkasse der CheckinID zuordnen
                Dim posten = context.Billings.Where(Function(o) o.CustomerID = Cust.CustomerID And o.AccountID < 1800 And o.BillingStatus = BillingStates.Offen)
                If posten.Any Then
                    For Each itm In posten
                        itm.CheckinID = ch.CheckinID
                    Next
                End If
                context.SaveChanges()

            End Using
            ExitExecute()
        End Sub

        Private Function CanEnterExecute() As Boolean
            If Trim(Schluessel) = "" Then Return True
            If Duplikat = Visibility.Collapsed Then Return True
            Return False
        End Function

        Private Function DuplicateKey() As Boolean
            If Trim(Schluessel) = "" Then Return False
            If Not IsNothing(_schluesselliste) Then
                Dim found As Boolean = False
                For Each itm In _schluesselliste
                    If itm = Schluessel Then found = True
                Next
                If found Then Return True
            End If
            Return False
        End Function

        Private Sub LoadData()

            _context = New in1Entities

            Cust = _context.Customers.Find(_id)
            Abokarten = Cust.Billings.Where(Function(o) o.IsStorno = False And o.Packagecredit.HasValue AndAlso o.Packagecredit > 0).ToList()

            LadeSchluessel()
            Schluessel = ""

            'Vertrag
            GetVertragsinfo()




            'Letzter Checkin
            Letztercheckin = Nothing
            Dim qcheck = (From c In _context.CheckinLogs Where c.CustomerID = Cust.CustomerID Select c.Checkin).DefaultIfEmpty.Max



            If Not IsNothing(qcheck) Then
                Letztercheckin = qcheck.ToShortDateString
            End If

            'Foto laden falls existent


            'Dim img As New Imaging.BitmapImage
            'img.BeginInit()
            'If Cust.HasImage Then

            '    Dim strpfad = GlobalSettingService.in1DataFolder(in1Folder.customerphotos) & Cust.CustomerID & ".jpg"

            '    img.UriSource = New Uri(strpfad)
            'Else
            '    img.UriSource = New Uri("pack://application:,,,/HSPinOne.WPF.Shell;component/images/unknown.jpg")
            'End If
            'img.EndInit()
            'Foto = img


        End Sub



        Private Sub Aboabtragen(ByVal param As Object)
            Dim billid = 0
            Dim dlg As New DialogService
            If Not IsNothing(param) Then
                billid = Integer.Parse(param)
                Using con As New in1Entities
                    Dim bill = con.Billings.Find(billid)
                    If bill.Packagecredit > 0 Then
                        If dlg.AskConfirmation("Abokarte", "Wirklich Punkt von Karte " & bill.Verwendungszweck & " abtragen") = True Then
                            Dim bal As New Billing_Bal(MyGlobals.CurrentUsername)
                            Dim result = bal.Aboabtragen(billid)
                            If result >= 0 Then
                                dlg.ShowInfo("Abokarte", "Ein Punkt wurde von der Abokarte abgetragen")
                                Dim vbill = Abokarten.Where(Function(o) o.BillingID = billid).FirstOrDefault
                                vbill.Packagecredit = result
                            End If
                        End If

                    End If
                End Using
            End If
        End Sub

        Private Sub GetVertragsinfo()
            Dim query = From c In _context.Contracts Where c.CustomerID = Cust.CustomerID Select c

            Vertragsinfo.Clear()
            Dim Vertrag As Contract = Nothing
            Dim str As String = ""
            If query.Any Then
                For Each itm In query
                    If DateDiff(DateInterval.Day, itm.Vertragsbeginn, Now) >= 0 And
                        DateDiff(DateInterval.Day, Now, itm.Vertragsende) >= 0 Then
                        Vertrag = itm
                        str = Vertrag.ContractRate.ContractRateName & " " & Vertrag.Vertragsbeginn.ToShortDateString & " - " & Vertrag.Vertragsende.ToShortDateString
                        If Not IsNothing(Vertrag.Kuendigungzum) Or IsNothing(Vertrag.Verlaengerungsmonate) OrElse Vertrag.Verlaengerungsmonate = 0 Then
                            str = str & " (gekündigt)"
                        End If
                        Vertragsinfo.Add(str)
                    End If
                Next

            End If
        End Sub

        Public Overloads Sub OnNavigatedTo(ByVal navigationContext As Prism.Regions.NavigationContext) Implements Prism.Regions.INavigationAware.OnNavigatedTo
            ID = navigationContext.Parameters("ID")
            LadeSchluessel()
        End Sub
    End Class
End Namespace