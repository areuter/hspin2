﻿Imports HSPinOne.Infrastructure
Imports Prism.Regions
Imports HSPinOne.DAL
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports Newtonsoft.Json

Public Class CashClosingsViewModel
    Inherits ViewModelBase
    Implements INavigationAware


    Private _CashClosings As ObservableCollection(Of CashClosing)
    Public Property CashClosings() As ObservableCollection(Of CashClosing)
        Get
            Return _CashClosings
        End Get
        Set(ByVal value As ObservableCollection(Of CashClosing))
            _CashClosings = value
            RaisePropertyChanged("CashClosings")
        End Set
    End Property

    Property From As Date
    Property Till As Date

    Property FilterCommand As New RelayCommand(AddressOf Load)
    Property PrintCommand As New RelayCommand(AddressOf Print)
    Property JournalCommand As New RelayCommand(AddressOf Journal)

    Property TerminalCommand As New RelayCommand(AddressOf Terminal)

    Private _regionmanager As IRegionManager

    Public Sub New()
        Me._regionmanager = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IRegionManager)

        From = Now.AddDays(-14)
        Till = Now
    End Sub

    Private Sub Init()
        Load()
    End Sub

    Private Sub Load()
        Using con As New in1Entities
            Dim bis = Till.Date.AddDays(1)
            con.CashClosings.Where(Function(o) (o.Ende < bis AndAlso o.Ende > From) Or Not o.Ende.HasValue).OrderByDescending(Function(o) o.Ende).Load()
            CashClosings = con.CashClosings.Local()
        End Using
    End Sub

    Private Sub Print(ByVal obj As Object)
        Dim id = 0
        Integer.TryParse(obj, id)
        If id > 0 Then

            Dim win As New WindowController
            Dim uc As New CashZBonView
            Dim vm = New ViewModel.CashZBonViewModel()
            uc.DataContext = vm
            vm.Show(id)
            win.ShowWindow(uc, "Kassenabschluss")


        End If

    End Sub

    Private Sub Terminal(ByVal obj As Object)
        Dim cc = CType(obj, CashClosing)
        If Not IsNothing(cc) Then
            Dim dlg As New DialogService
            Dim json = JsonConvert.DeserializeObject(Of Object)(cc.ECBon)
            Dim jsonstring = json.ToString()
            dlg.ShowInfo("Terminal Report", jsonstring)

        End If
    End Sub

    Private Sub Journal(ByVal obj As Object)

        Dim id = 0
        Integer.TryParse(obj, id)
        If id > 0 Then
            Dim params As New NavigationParameters()
            params.Add("CashClosingID", id)
            _regionmanager.RequestNavigate(RegionNames.MainRegion, New Uri("CashReportView", UriKind.Relative), params)
        End If

    End Sub

    Public Overrides Sub OnNavigatedTo(navigationContext As NavigationContext)

        Init()
        MyBase.OnNavigatedTo(navigationContext)
    End Sub




End Class
