﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports HSPinOne.Common
Imports System.Collections.ObjectModel
Imports System.Data.Entity
Imports System.ComponentModel.Composition
Imports System.Windows.Input
Imports System.ComponentModel
Imports Prism.Regions
Imports System.Linq
Imports System.Windows.Data

Namespace ViewModel

    Public Class CashReportViewModel
        Inherits ViewModelBase
        Implements INavigationAware


#Region "   Properties"

        Private firstload As Boolean = True

        Private WithEvents bgw As New BackgroundWorker



        Private _billinglist As ObservableCollection(Of Billing)
        Public Property Billinglist As ObservableCollection(Of Billing)
            Get
                Return _billinglist
            End Get
            Set(ByVal value As ObservableCollection(Of Billing))
                _billinglist = value
            End Set
        End Property

        Private _billingview As ListCollectionView
        Public Property Billingview() As ListCollectionView
            Get
                Return _billingview
            End Get
            Set(ByVal value As ListCollectionView)
                _billingview = value
                RaisePropertyChanged("Billingview")
            End Set
        End Property


        Private _selectedaccount As Integer
        Public Property SelectedAccount As Integer
            Get
                Return _selectedaccount
            End Get
            Set(ByVal value As Integer)
                _selectedaccount = value
                RaisePropertyChanged("SelectedAccount")
                If Not firstload Then _
                    LoadData()
            End Set
        End Property

        Private _accountlist As List(Of Account)
        Public Property Accountlist As List(Of Account)
            Get
                Return _accountlist
            End Get
            Set(ByVal value As List(Of Account))
                _accountlist = value
            End Set
        End Property

        Private _summe As Decimal
        Public Property Summe As Decimal
            Get
                Return _summe
            End Get
            Set(ByVal value As Decimal)
                _summe = value
                RaisePropertyChanged("Summe")
            End Set
        End Property

        Private _Kassen As List(Of String)
        Public Property Kassen() As List(Of String)
            Get
                Return _Kassen
            End Get
            Set(ByVal value As List(Of String))
                _Kassen = value
            End Set
        End Property

        Private _SelectedKasse As String
        Public Property SelectedKasse() As String
            Get
                Return _SelectedKasse
            End Get
            Set(ByVal value As String)
                _SelectedKasse = value
                RaisePropertyChanged("SelectedKasse")
                If Not firstload Then LoadData()
            End Set
        End Property

        Private _cashclosingid As Integer



        Public Property SelectedItem As Billing

        Public Property PrintCommand As ICommand
        Public Property PrintDetailCommand As ICommand
        Public Property PrintBonCommand As ICommand
        Public Property StornoCommand As ICommand
        Public Property OpendrawerCommand As ICommand
        Public Property CashCloseCommand As ICommand
        Public Property BonduplikatCommand As ICommand
        Public Property EccashcloseCommand As ICommand
        Public Property XBonCommand As ICommand



        Private _bondata As String

        Private _regionmanager As IRegionManager


#End Region

        Public Sub New()

            Me._regionmanager = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IRegionManager)


            Kassen = MachineSettingService.GetKasseAll()

            SelectedKasse = MachineSettingService.GetKasseNr()
            firstload = False

            Me.PrintCommand = New RelayCommand(AddressOf Print)
            'Me.PrintDetailCommand = New RelayCommand(AddressOf PrintDetail)
            Me.StornoCommand = New RelayCommand(AddressOf Storno, AddressOf CanStorno)
            'Me.PrintBonCommand = New RelayCommand(AddressOf PrintZBonAction)
            Me.OpendrawerCommand = New RelayCommand(AddressOf OpenDrawer)
            Me.CashCloseCommand = New RelayCommand(AddressOf CashCloseAction)
            Me.EccashcloseCommand = New RelayCommand(AddressOf ECAbschluss)


            BonduplikatCommand = New RelayCommand(AddressOf Bonduplikat)

            XBonCommand = New RelayCommand(AddressOf XBonAction)




        End Sub
        Private Sub LoadData(Optional ByVal cc As Integer = 0)

            If cc = 0 Then
                Dim kasse = MachineSettingService.GetKasseNr()
                _cashclosingid = Repositories.CashClosingRepository.GetCashClosingID(kasse)
            Else
                _cashclosingid = cc
            End If

            If Not bgw.IsBusy Then
                IsBusy = True
                bgw.RunWorkerAsync()
            End If

        End Sub

        Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw.DoWork
            'Dim context As New in1Entities
            'Dim cc = Repositories.CashClosingRepository.GetCashClosingID(SelectedKasse)

            Using con As New in1Entities
                Dim repo As New Repositories.BillingRepository(con)
                repo.LoadJournal(_cashclosingid)
                'repo.LoadJournal(cc)
                Billinglist = con.Billings.Local

            End Using
            Dim summe As Decimal = 0
            For Each itm In Billinglist
                If itm.BillingStatus = BillingStates.Bezahlt Then _
                    summe = summe + itm.Brutto
            Next
            Me.Summe = summe
        End Sub

        Private Sub bgw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles bgw.RunWorkerCompleted
            'RaisePropertyChanged("Billinglist")
            Billingview = CollectionViewSource.GetDefaultView(Billinglist.ToList())
            Billingview.Filter = AddressOf Billinglistfilter
            IsBusy = False
        End Sub

        Private Function Billinglistfilter(ByVal item As Object) As Boolean
            Try
                Dim itm = TryCast(item, Billing)
                If Not IsNothing(itm) Then
                    If itm.BillingStatus = BillingStates.Bezahlt Then Return True
                End If
            Catch ex As Exception

            End Try
            Return False
        End Function

        Private Sub Storno()
            If Not IsNothing(SelectedItem) Then
                Dim dlg As New DialogService
                If dlg.AskCancelConfirmation("Löschen", "Sind sie sicher, dass diese Buchung gelöscht werden soll?") = System.Windows.MessageBoxResult.Yes Then
                    Using con As New in1Entities
                        Dim itm = con.Billings.Find(SelectedItem.BillingID)
                        Dim repo As New Repositories.BillingRepository(con)
                        repo.StornoBilling(itm, itm.AccountID, itm.PaymentMethodID, MachineSettingService.GetKasseNr(), True)
                        'con.Billings.Remove(itm)
                        con.SaveChanges()
                        LoadData()
                    End Using
                End If
            End If
        End Sub

        Private Function CanStorno() As Boolean
            'If SelectedItem IsNot Nothing Then
            '    If SelectedItem.Customer Is Nothing And SelectedItem.IsStorno = False Then Return True
            'End If
            Return False
        End Function


        Private Sub Bonduplikat()
            If Not IsNothing(SelectedItem) Then

                Dim bonguid = SelectedItem.Vorgang
                Dim bonsvc As New BonService(bonguid, True, SelectedItem.CustomerID)

            End If
        End Sub

        Private Sub ECAbschluss()

            Dim kasse = MachineSettingService.GetKasseNr()
            Dim id = Repositories.CashClosingRepository.GetCashClosingID(kasse)

            Dim win As New WindowController
            Dim uc As New CashZBonView
            Dim vm = New ViewModel.CashZBonViewModel()
            uc.DataContext = vm
            vm.Show(id)
            win.ShowWindow(uc, "Kassenabschluss")

            'Dim opiservice As New OPIService
            'Dim res As Boolean = opiservice.Reconciliate()
            'Dim dlg As New DialogService

            'If res = True Then

            '    dlg.ShowInfo("Terminal", "EC Abschluss durchgeführt")
            '    'Dim receipt = opiservice.Receipt
            '    Dim pi = opiservice.Paymentinfo

            '    ' Wenn Kasse nicht offen, dann Kasse öffnen
            '    Using con As New in1Entities
            '        Dim cc = con.CashClosings.Where(Function(o) o.KasseNr = SelectedKasse And Not o.Ende.HasValue).FirstOrDefault
            '        If IsNothing(cc) Then
            '            cc = NewCashClosing()
            '            con.CashClosings.Add(cc)
            '        End If
            '        cc.Ende = Now
            '        cc.ClosedBy = MyGlobals.CurrentUsername
            '        cc.ECBon = pi

            '        cc.SummeBar = 0
            '        cc.SummeKarte = 0

            '        'Neue Kasse öffnen
            '        Dim newcc = NewCashClosing()
            '        con.CashClosings.Add(newcc)
            '        con.SaveChanges()

            '        'Dim repo As New Repositories.BillingRepository(con)
            '        'repo.LoadJournal(SelectedKasse, dtfrom, dttill)
            '    End Using

            'Else
            '    dlg.ShowError("Terminal", "EC Abschluss nicht möglich")
            'End If

        End Sub

        Private Function NewCashClosing() As CashClosing
            Dim newcc As New CashClosing
            newcc.KasseNr = SelectedKasse
            newcc.Start = Now
            Return newcc
        End Function

        Private Sub OpenDrawer()
            Dim bon As New Bondruck()
            bon.Pulse()
            bon.Print()
            'bon.ClosePrinter()
            bon = Nothing
        End Sub

        Private Sub XBonAction()
            'Dim dtfrom = SelectedDate.Date
            'Dim dttill = SelectedDate2.Date.AddHours(23).AddMinutes(59).AddSeconds(59)
            'Dim win As New WindowController
            'Dim view As New CashZBonView
            'Dim vm As New ViewModel.CashZBonViewModel(SelectedKasse)
            'AddHandler vm.RequestClose, AddressOf win.CloseWindow
            'view.DataContext = vm
            'win.ShowWindow(view, "  X-Bon  ")


        End Sub

        Private Sub CashCloseAction()
            _regionmanager.Regions(RegionNames.MainRegion).RequestNavigate(New Uri("CashClosingView", UriKind.Relative))
        End Sub

        Public Overrides Sub OnNavigatedTo(navigationContext As NavigationContext) Implements INavigationAware.OnNavigatedTo
            Dim cc As Integer = 0
            If Not IsNothing(navigationContext.Parameters("CashClosingID")) Then
                cc = navigationContext.Parameters("CashClosingID")
            End If
            MyBase.OnNavigatedTo(navigationContext)
            LoadData(cc)
        End Sub


    End Class
End Namespace