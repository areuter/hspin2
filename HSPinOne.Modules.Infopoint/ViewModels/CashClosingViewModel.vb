﻿Imports System.Windows.Input
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports Prism.Regions

Namespace ViewModel


    Public Class CashClosingViewModel
        Inherits ViewModelBase
        Implements INavigationAware

        Private _Bestand As List(Of Bestanditem)
        Public Property Bestand() As List(Of Bestanditem)
            Get
                Return _Bestand
            End Get
            Set(ByVal value As List(Of Bestanditem))
                _Bestand = value
            End Set
        End Property

        Private _Summe As Double
        Public Property Summe() As Double
            Get
                Return _Summe
            End Get
            Set(ByVal value As Double)
                _Summe = value
                RaisePropertyChanged("Summe")
            End Set
        End Property

        Private _CashClosing As CashClosing
        Public Property CashClosing() As CashClosing
            Get
                Return _CashClosing
            End Get
            Set(ByVal value As CashClosing)
                _CashClosing = value
                RaisePropertyChanged("CashClosing")
            End Set
        End Property

        Private _Account As Account
        Public Property Account() As Account
            Get
                Return _Account
            End Get
            Set(ByVal value As Account)
                _Account = value
                RaisePropertyChanged("Account")
            End Set
        End Property

        Private _Message As String
        Public Property Message() As String
            Get
                Return _Message
            End Get
            Set(ByVal value As String)
                _Message = value
                RaisePropertyChanged("Message")
            End Set
        End Property


        Private _CashOpen As Boolean = False
        Public Property CashOpen() As Boolean
            Get
                Return _CashOpen
            End Get
            Set(ByVal value As Boolean)
                _CashOpen = value
                RaisePropertyChanged("CashOpen")
            End Set
        End Property

        Private _Anfangsbestand As Double
        Public Property Anfangsbestand() As Double
            Get
                Return _Anfangsbestand
            End Get
            Set(ByVal value As Double)
                _Anfangsbestand = value
                RaisePropertyChanged("Anfangsbestand")
            End Set
        End Property





        Property OpenCommand As ICommand
        Property FinishCommand As ICommand
        Property TakeCommand As ICommand


        Private _context As New in1Entities

        Public Sub New()

            OpenCommand = New RelayCommand(AddressOf OpenCash)
            FinishCommand = New RelayCommand(AddressOf FinishCash)
            TakeCommand = New RelayCommand(AddressOf Take)


            Bestand = New List(Of Bestanditem) From {
                New Bestanditem("1 Cent", 0.01),
                New Bestanditem("2 Cent", 0.02),
                New Bestanditem("5 Cent", 0.05),
                New Bestanditem("10 Cent", 0.1),
                New Bestanditem("20 Cent", 0.2),
                New Bestanditem("50 Cent", 0.5),
                New Bestanditem("1 Euro", 1),
                New Bestanditem("2 Euro", 2),
                New Bestanditem("5 Euro", 5),
                New Bestanditem("10 Euro", 10),
                New Bestanditem("20 Euro", 20),
                New Bestanditem("50 Euro", 50),
                New Bestanditem("100 Euro", 100),
                New Bestanditem("200 Euro", 200),
                New Bestanditem("500 Euro", 500)
            }

            For Each itm In Bestand
                AddHandler itm.AnzahlChanged, AddressOf Berechnen
            Next


            'OnStart()


        End Sub

        Private Sub OnStart()

            _CashOpen = False
            ' Ist dem PC eine Kasse zugeordnet
            Dim pcsetting = CType(MachineSettingService.GetMachineSetting(MachineSettingService.in1MaschineSettings.Kasse), Integer)
            Dim kasse = MachineSettingService.GetKasseNr()


            'Gibt es eine offene Kasse
            Dim cashopen = From c In _context.CashClosings Where c.Ende Is Nothing And c.KasseNr = kasse Select c

            If Not IsNothing(cashopen) AndAlso cashopen.Count = 1 Then
                Me.CashOpen = True
                'CashClosing = Repositories.CashClosingRepository.Calculate(cashopen.FirstOrDefault.CashClosingID)
            Else
                Me.CashOpen = False
            End If
        End Sub

        Private Sub Berechnen()
            Summe = 0
            If Not IsNothing(Bestand) Then
                For Each itm In Bestand
                    If Not IsNothing(itm.Anzahl) AndAlso Not IsNothing(itm.Wert) Then _
                    Summe = Summe + (itm.Wert * itm.Anzahl)
                Next
            End If
        End Sub

        Private Sub Take()
            If CashOpen = True Then
                'Me.CashClosing.Kassenbestand = Summe
            Else
                Me.Anfangsbestand = Summe
            End If

        End Sub

        Private Sub OpenCash()
            'Dim cc As New CashClosing
            'cc.Anfangsbestand = Me.Anfangsbestand
            'cc.AccountID = _Account.AccountID
            'cc.StartDatum = Now
            'Using con As New in1Entities
            '    con.CashClosings.Add(cc)
            '    con.SaveChanges()
            '    Me.CashOpen = True
            '    OnStart()
            'End Using
        End Sub

        Private Sub FinishCash()
            Dim dlg As New DialogService
            If dlg.AskCancelConfirmation("Kasse abschließen", "Wollen Sie wirklich die Kasse abschließen") = System.Windows.MessageBoxResult.Yes Then
                _context.CashClosings.Attach(CashClosing)
                _context.SaveChanges()
                Me.CashOpen = False
            End If

        End Sub


        Public Overrides Sub OnNavigatedTo(navigationContext As NavigationContext)
            MyBase.OnNavigatedTo(navigationContext)
            OnStart()
        End Sub
    End Class

    Public Class Bestanditem
        Property Anzeige As String
        Property Wert As Double
        Private _Anzahl As Nullable(Of Integer)

        Public Event AnzahlChanged()

        Public Property Anzahl() As Nullable(Of Integer)
            Get
                Return _Anzahl
            End Get
            Set(ByVal value As Nullable(Of Integer))
                _Anzahl = value
                RaiseEvent AnzahlChanged()
            End Set
        End Property


        Public Sub New(anz As String, wert As Double)
            Me.Anzeige = anz
            Me.Wert = wert
            Me.Anzahl = Nothing
        End Sub
    End Class

End Namespace