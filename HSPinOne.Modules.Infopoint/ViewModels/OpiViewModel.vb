﻿Imports System.ComponentModel
Imports HSPinOne.Infrastructure

Namespace ViewModel



    Public Class OpiViewModel
        Inherits ViewModelBase



        Private _opi As OPIGateway.OPIAction

        Public Event OpiEvent As EventHandler(Of OPIGateway.CustomEventArgs)

        Public Event RequestClose As EventHandler

        Private WithEvents bgw As BackgroundWorker


        Private _Message As String
        Public Property Message() As String
            Get
                Return _Message
            End Get
            Set(ByVal value As String)
                _Message = value
                RaisePropertyChanged("Message")
            End Set
        End Property

        Private _Betrag As Double
        Public Property Betrag() As Double
            Get
                Return _Betrag
            End Get
            Set(ByVal value As Double)
                _Betrag = value
                RaisePropertyChanged("Betrag")
            End Set
        End Property

        Private _PaymentResult As Boolean = False
        Public ReadOnly Property PaymentResult() As Boolean
            Get
                Return _PaymentResult
            End Get
        End Property

        Private _PaymentInformation As String
        Public ReadOnly Property PaymentInformation() As String
            Get
                Return _PaymentInformation
            End Get
        End Property

        Private _Receipt As String
        Public ReadOnly Property Receipt() As String
            Get
                Return _Receipt
            End Get
        End Property

        Private _Ready As Boolean = False
        Public Property Ready() As Boolean
            Get
                Return _Ready
            End Get
            Set(ByVal value As Boolean)
                _Ready = value
                RaisePropertyChanged("Ready")
            End Set
        End Property

        Private _ReturnCode As Integer = -1
        Public Property ReturnCode() As Integer
            Get
                Return _ReturnCode
            End Get
            Set(ByVal value As Integer)
                _ReturnCode = value
            End Set
        End Property



        Public Property OKCommand As New RelayCommand(AddressOf OKAction)



        Private ip As String
        Private port0 As String
        Private port1 As String
        Private stayloggedin As Boolean = False


        Public Sub New()
            Init()
        End Sub



        Public Sub Init()

            ip = MachineSettingService.GetMachineSetting(MachineSettingService.in1MaschineSettings.TerminalIP)

            port0 = MachineSettingService.GetMachineSetting(MachineSettingService.in1MaschineSettings.TerminalPort0)
            port1 = MachineSettingService.GetMachineSetting(MachineSettingService.in1MaschineSettings.TerminalPort1)
            If (MachineSettingService.GetMachineSetting(MachineSettingService.in1MaschineSettings.TerminalStayLoggedin) = "true") Then
                stayloggedin = True
            End If


        End Sub

        Private Function CheckTerminal(Optional ByVal simulate As Boolean = False) As Boolean

            If Trim(ip) = "" Then
                Message = "Kein EC Terminal installiert"
                Return False
            End If

            Dim opitest = OPIGateway.Helper.TestConnection.TestIt(ip, port0, New TimeSpan(0, 0, 1), simulate)
            If opitest = False Then
                Message = "Terminal nicht erreichbar"
                Return False
            End If
            Return True
        End Function

        Public Sub Pay(ByVal amount As Double)

            Dim simulate As Boolean = False


#If DEBUG Then
            simulate = True
#End If
            If CheckTerminal(simulate) = True Then
                _ReturnCode = 1 ' Payment Running
                _opi = New OPIGateway.OPIAction(ip, CInt(port0), CInt(port1), simulate)
                AddHandler _opi.RaiseCustomEvent, AddressOf OpiHandleEvent

                If stayloggedin = True Then
                    _opi.StayLoggedin = True
                End If

                Betrag = amount
                bgw = New BackgroundWorker()
                'bgw.WorkerReportsProgress = True
                bgw.WorkerSupportsCancellation = False

                If Not IsNothing(bgw) AndAlso Not bgw.IsBusy Then
                    bgw.RunWorkerAsync()
                End If

            Else
                Ready = True
            End If

        End Sub

        Public Sub Cancel()
            If Not IsNothing(bgw) AndAlso bgw.IsBusy Then
                bgw.CancelAsync()
            End If
        End Sub

        Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw.DoWork

            _PaymentResult = _opi.PerformPayment(Betrag)
            If _PaymentResult = True Then
                Message = "Zahlung erfolgreich"
                _ReturnCode = 0 ' Payment successfull

                If Not IsNothing(_opi) Then
                    _PaymentInformation = _opi.PaymentInfo
                    _Receipt = _opi.Receipt
                End If

            Else
                Message = _opi.Message
                ReturnCode = 9 ' Payment Error
            End If


        End Sub



        Private Sub bgw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles bgw.RunWorkerCompleted
            Try
                If Not IsNothing(e.Error) Then
                    _ReturnCode = 8
                    Message = e.Error.ToString()
                End If

                If Not IsNothing(_opi) Then

                    RemoveHandler _opi.RaiseCustomEvent, AddressOf OpiHandleEvent
                    _opi.Close()
                End If
            Catch ex As Exception
                _ReturnCode = 7
                Throw New InvalidOperationException(ex.Message.ToString())
            End Try



            Ready = True
            'OKCommand.RaiseCanExecuteChanged()

        End Sub

        Private Sub OKAction()
            RaiseEvent RequestClose(Me, Nothing)
        End Sub



        Private Sub OpiHandleEvent(ByVal sender As Object, ByVal e As OPIGateway.CustomEventArgs)
            Try


                If Not IsNothing(e) Then
                    Message = e.Message
                    If Not e.Receipt = "" Then
                        _Receipt = e.Receipt
                    End If
                Else
                    Message = "Event Handling fehlgeschlagen (Leeres Event)"
                End If

            Catch ex As Exception
                Throw New InvalidOperationException("OpiHandleEvent - " & ex.Message.ToString())
            End Try
        End Sub

    End Class
End Namespace