﻿

Imports HSPinOne.Infrastructure

Public Class OPIService

    Private _Paymentinfo As String
    Public ReadOnly Property Paymentinfo() As String
        Get
            Return _Paymentinfo
        End Get
    End Property

    Private _Receipt As String
    Public ReadOnly Property Receipt() As String
        Get
            Return _Receipt
        End Get
    End Property

    Private _Message As String
    Public Property Message() As String
        Get
            Return _Message
        End Get
        Set(ByVal value As String)
            _Message = value
        End Set
    End Property

    Private _ReturnCode As Integer = -1
    Public Property ReturnCode() As Integer
        Get
            Return _ReturnCode
        End Get
        Set(ByVal value As Integer)
            _ReturnCode = value
        End Set
    End Property


    Public Sub New()

    End Sub

    Public Function CheckTerminal() As Boolean
        Dim ip As String = MachineSettingService.GetMachineSetting(MachineSettingService.in1MaschineSettings.TerminalIP)

        If Trim(ip) = "" Then
            Message = "Kein EC Terminal installiert"
            Return False
        End If

        Dim port0 As String = MachineSettingService.GetMachineSetting(MachineSettingService.in1MaschineSettings.TerminalPort0)
        Dim port1 As String = MachineSettingService.GetMachineSetting(MachineSettingService.in1MaschineSettings.TerminalPort1)

        Dim opitest = OPIGateway.Helper.TestConnection.TestIt(ip, port0, New TimeSpan(0, 0, 1))
        If opitest = False Then
            Message = "Terminal nicht erreichbar"
            Return False
        End If
        Return True

    End Function


    Public Function Pay(ByVal amount As Double) As Boolean
        Dim vm As New ViewModel.OpiViewModel
        Dim uc As New OpiView

        Dim result As Boolean = False
        uc.DataContext = vm


        vm.Pay(amount)

        Dim win As New WindowController
        win.SetCanClose(False)
        win.HideTitle()
        Dim myeventhandler As New EventHandler(AddressOf win.CloseWindow)
        AddHandler vm.RequestClose, myeventhandler

        win.ShowWindow(uc, "EC Cash")




        result = vm.PaymentResult
        _ReturnCode = vm.ReturnCode
        _Paymentinfo = vm.PaymentInformation
        _Receipt = vm.Receipt

        uc.DataContext = Nothing
        win.CloseWindow()


        Return result
    End Function

    Public Function Reconciliate() As Boolean
        Dim opi As OPIGateway.OPIAction
        Dim dlg As New DialogService

        Dim ip As String = MachineSettingService.GetMachineSetting(MachineSettingService.in1MaschineSettings.TerminalIP)

        If Trim(ip) = "" Then

            dlg.ShowAlert("Terminal", "Kein EC Terminal installiert")
            Return False
        End If

        Dim port0 As String = MachineSettingService.GetMachineSetting(MachineSettingService.in1MaschineSettings.TerminalPort0)
        Dim port1 As String = MachineSettingService.GetMachineSetting(MachineSettingService.in1MaschineSettings.TerminalPort1)

        Dim opitest = OPIGateway.Helper.TestConnection.TestIt(ip, port0, New TimeSpan(0, 0, 1))
        If opitest = False Then
            dlg.ShowAlert("Terminal", "Terminal nicht erreichbar")
            Return False
        End If


        opi = New OPIGateway.OPIAction(ip, CInt(port0), CInt(port1))
        AddHandler opi.RaiseCustomEvent, AddressOf OpiHandleEvent
        Dim res = opi.Reconciliate(True)
        If Not IsNothing(opi) Then
            _Paymentinfo = opi.PaymentInfo
            RemoveHandler opi.RaiseCustomEvent, AddressOf OpiHandleEvent
            opi.Close()
        End If

        Return res

    End Function

    Private Sub OpiHandleEvent(ByVal sender As Object, ByVal e As OPIGateway.CustomEventArgs)

        Message = e.Message
        If Not e.Receipt = "" Then
            _Receipt = e.Receipt
        End If

    End Sub

End Class
