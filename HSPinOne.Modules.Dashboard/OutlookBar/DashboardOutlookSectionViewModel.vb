﻿Imports Prism.Regions
Imports Prism.Events
Imports Prism.Commands
Imports System.Windows.Input
Imports System
Imports System.Collections.ObjectModel
Imports HSPinOne.Common
Imports HSPinOne.Infrastructure
Imports System.ComponentModel.Composition
Imports System.Windows.Controls


<Export(GetType(DashboardOutlookSectionViewModel)), PartCreationPolicy(CreationPolicy.Shared)> _
Public Class DashboardOutlookSectionViewModel


    Private _treeViewMenu As ObservableCollection(Of OutlookSectionMenuTreeTopItemViewModel)
    Public Property TreeViewMenu As ObservableCollection(Of OutlookSectionMenuTreeTopItemViewModel)
        Get
            Return _treeViewMenu
        End Get
        Set(ByVal value As ObservableCollection(Of OutlookSectionMenuTreeTopItemViewModel))
            _treeViewMenu = value
        End Set
    End Property

    Private _regionmanager As IRegionManager
    Private _ea As IEventAggregator



    Private _selected As ListBoxItem
    Public Property Selected As ListBoxItem
        Get
            Return _selected
        End Get
        Set(value As ListBoxItem)
            _selected = value
            If Not IsNothing(value.Tag) Then
                If InStr(value.Tag, "View") > 0 Then
                    ViewNavigation.NavigateViews(CType(value.Tag, String), _regionmanager)
                Else
                    Select Case value.Tag
                        Case "explorer"
                            ExplorerAction()
                        Case "calc"
                            CalculatorAction()
                        Case "www"
                            InternetAction()
                        Case "word"
                            WordAction()
                        Case "mail"
                            MailAction()

                    End Select
                End If
            End If
        End Set
    End Property



    Public Sub New()

        _regionmanager = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IRegionManager)
        _ea = Events.EventInstance.EventAggregator
        _ea.GetEvent(Of OutlookSectionChangedEvent)().Subscribe(AddressOf CheckRegion)






    End Sub

    Private Sub MailAction()
        System.Diagnostics.Process.Start("mailto:")
    End Sub

    Private Sub InternetAction()
        System.Diagnostics.Process.Start("http://www.google.de")
    End Sub

    Private Sub WordAction()
        Try


            Dim oWord = CreateObject("Word.Application")
            oWord.Visible = True
            oWord.Documents.Add()
            oWord = Nothing
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ExplorerAction()
        Process.Start("explorer.exe")
    End Sub

    Private Sub CalculatorAction()
        Process.Start("calc.exe")
    End Sub

    Private Sub CheckRegion(ByVal section As Object)


        Dim viewname As String = "Dashboard"

        If section.Name = viewname Then

            'Dim regionManager As IRegionManager = ServiceLocator.Current.GetInstance(Of IRegionManager)()
            Dim uri As New Uri("/DashboardView", UriKind.Relative)
            _regionmanager.Regions(RegionNames.MainRegion).RequestNavigate(uri)





        End If
    End Sub


End Class
