﻿Imports HSPinOne.Infrastructure
Imports System.Text.RegularExpressions

Public Class LinkViewModel

    Public Property Linkliste As List(Of Linkelement)


    Public Sub New()
        Linkliste = New List(Of Linkelement)
        For i = 1 To 4
            Try


                Dim link = GlobalSettingService.HoleEinstellung("link" & i)
                If Not IsNothing(link) Then
                    If Not Trim(link) = "" Then
                        Dim le As New Linkelement
                        le.Title = link.Split(New Char() {"[", "]"})(1)
                        le.URL = link.Split(New Char() {"(", ")"})(1)
                        Linkliste.Add(le)
                    End If
                End If
            Catch ex As Exception
                Dim dlg As New DialogService
                dlg.ShowError("Fehler", "Fehler bei der Konfiguration der Links in den globalen Einstellungen")
                dlg = Nothing
            End Try
        Next
    End Sub
End Class

Public Class Linkelement
    Public Property URL As String
    Public Property Title As String
End Class
