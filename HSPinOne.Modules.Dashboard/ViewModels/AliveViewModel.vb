﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Threading
Imports System.Windows.Input
Imports System.Collections.ObjectModel
Imports Prism.Events

Public Class AliveViewModel
    Inherits ViewModelBase


    Private _timer As DispatcherTimer
    Private _ea As IEventAggregator



    Private _Userlist As ObservableCollection(Of UserAlive)
    Public Property Userlist As ObservableCollection(Of UserAlive)
        Get
            Return _Userlist
        End Get
        Set(value As ObservableCollection(Of UserAlive))
            _Userlist = value
        End Set
    End Property

    Private _MyAbsence As String
    Public Property MyAbsence As String
        Get
            Return _MyAbsence
        End Get
        Set(value As String)
            _MyAbsence = value
            RaisePropertyChanged("MyAbsence")
        End Set
    End Property



    Public Property SaveAbsenceCommand As ICommand


    Public Sub New()
        Userlist = New ObservableCollection(Of UserAlive)
        Check()
        _timer = New DispatcherTimer
        _timer.Interval = TimeSpan.FromSeconds(90)
        AddHandler _timer.Tick, AddressOf Check
        _timer.Start()
        Events.EventInstance.EventAggregator.GetEvent(Of DashboardRefreshEvent).Subscribe(AddressOf Check)
        SaveAbsenceCommand = New RelayCommand(AddressOf SaveAbsence)
    End Sub

    Private Sub Check()

        Try


            Dim _context As New in1Entities
            Dim query = From c In _context.Users Where c.IsActive = True Order By c.UserName
                        Select c


            Userlist.Clear()
            For Each itm In query
                'Eigenen Status aktualisieren
                If itm.UserName = MyGlobals.CurrentUsername Then
                    itm.LastAliveCheck = Now

                End If

                Dim lastalive As Short = 0
                If Not IsNothing(itm.LastAliveCheck) Then
                    Dim dt = CType(itm.LastAliveCheck, Date)
                    If DateDiff(DateInterval.Minute, dt, Now) < 3 Then lastalive = 1
                End If

                'Abwesenheitsnotiz
                If Not IsNothing(itm.Abwesenheitsnotiz) Then
                    If Trim(itm.Abwesenheitsnotiz) <> "" Then lastalive = 2
                End If
                If itm.UserID = MyGlobals.CurrentUserID Then
                    MyAbsence = itm.Abwesenheitsnotiz
                End If

                Dim user As New UserAlive
                user.Username = itm.UserName
                user.AliveState = lastalive
                user.Abwesenheitsnotiz = Trim(itm.Abwesenheitsnotiz)
                Userlist.Add(user)


            Next
            _context.SaveChanges()
            _context.Dispose()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub SaveAbsence()
        Dim auser As New User() With {.UserID = MyGlobals.CurrentUserID, .Abwesenheitsnotiz = MyAbsence}
        Using con As New in1Entities
            con.Users.Attach(auser)
            con.Entry(auser).Property(Function(o) o.Abwesenheitsnotiz).IsModified = True
            con.SaveChanges()
        End Using
        Check()
    End Sub

    Protected Overrides Sub Finalize()
        _timer.Stop()

        MyBase.Finalize()
    End Sub
End Class

Public Class UserAlive
    Inherits ViewModelBase

    Private _Username As String
    Public Property Username As String
        Get
            Return _Username
        End Get
        Set(value As String)
            _Username = value
            RaisePropertyChanged("Username")
        End Set
    End Property

    Private _AliveState As Short
    Public Property AliveState As Short
        Get
            Return _AliveState
        End Get
        Set(value As Short)
            _AliveState = value
            RaisePropertyChanged("AliveState")
        End Set
    End Property

    Private _Abwesenheitsnotiz As String
    Public Property Abwesenheitsnotiz As String
        Get
            Return _Abwesenheitsnotiz
        End Get
        Set(value As String)
            _Abwesenheitsnotiz = value
            RaisePropertyChanged("Abwesenheitsnotiz")
        End Set
    End Property

End Class

