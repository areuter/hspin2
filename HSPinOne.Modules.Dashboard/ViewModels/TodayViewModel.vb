﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports Prism.Events
Imports System.Collections.ObjectModel
Imports System.ComponentModel
Imports System.Data.Entity
Imports System.Data.Entity.SqlServer

Public Class TodayViewModel
    Inherits ViewModelBase

    Dim WithEvents bgw As New BackgroundWorker

    Private _ea As IEventAggregator


    Private _todayliste As ObservableCollection(Of String)
    Public Property TodayListe As ObservableCollection(Of String)
        Get
            Return _todayliste
        End Get
        Set(ByVal value As ObservableCollection(Of String))
            _todayliste = value

        End Set
    End Property


    Public Sub New()
        _ea = Events.EventInstance.EventAggregator
        Refresh()
        _ea.GetEvent(Of DashboardRefreshEvent).Subscribe(AddressOf Refresh)
    End Sub

    Private Sub Refresh()
        bgw.WorkerReportsProgress = False
        bgw.RunWorkerAsync()
    End Sub


    Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw.DoWork

        Try


            Dim _context As New in1Entities
            Dim von = Now.Date
            Dim bis = Now.Date.AddDays(1)

            Dim query = _context.Appointments _
                        .Include(Function(o) o.Course) _
                        .Include(Function(o) o.Course.Sport) _
                        .Include(Function(o) o.Booking) _
                        .Where(Function(o) o.Start > von And o.Start < bis) _
                        .OrderBy(Function(o) o.Start)




            Dim _liste As New ObservableCollection(Of String)

            For Each itm In query
                'String zusammenfügen
                Dim eventname As String = ""

                If Not IsNothing(itm.Start.ToShortTimeString) Then
                    eventname = eventname & itm.Start.ToShortTimeString
                Else
                    Console.WriteLine()
                End If

                If Not IsNothing(itm.CourseID) Then
                    eventname = eventname & " " & itm.Course.Sport.Sportname & " " & itm.Course.Zusatzinfo
                Else
                    eventname = eventname & " " & itm.Booking.Subject
                End If

                If eventname = "" Then
                    eventname = "Fehler bei der Namenszusammensetzung!"
                End If

                _liste.Add(eventname)
            Next
            e.Result = _liste
            _context.Dispose()

        Catch ex As Exception

        End Try

    End Sub

    Private Sub bgw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles bgw.RunWorkerCompleted
        If e.Error Is Nothing Then
            TodayListe = e.Result
            RaisePropertyChanged("TodayListe")
        End If
    End Sub

End Class
