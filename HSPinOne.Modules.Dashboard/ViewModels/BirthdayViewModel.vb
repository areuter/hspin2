﻿Imports HSPinOne.Infrastructure
Imports HSPinOne.DAL
Imports System.ComponentModel.Composition
Imports System.ComponentModel
Imports Prism.Events

Public Class BirthdayViewModel
    Inherits ViewModelBase

    Dim WithEvents bgw As New BackgroundWorker

    Private _ea As IEventAggregator


    Private _birthdays As Dictionary(Of Long, String)
    Public Property Birthdays As Dictionary(Of Long, String)
        Get
            Return _birthdays
        End Get
        Set(ByVal value As Dictionary(Of Long, String))
            _birthdays = value
        End Set
    End Property


    Public Sub New()
        Refresh()
        _ea = Events.EventInstance.EventAggregator
        _ea.GetEvent(Of DashboardRefreshEvent).Subscribe(AddressOf Refresh)
    End Sub

    Private Sub Refresh()
        bgw.WorkerReportsProgress = False
        bgw.RunWorkerAsync()
    End Sub


    Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw.DoWork
        Dim _context As New in1Entities

        Dim bd = From c In _context.Customers Where Day(c.Geburtstag) = Day(Now) And Month(c.Geburtstag) = Month(Now) And c.Aktiv = True
                 Order By c.Suchname
                 Select c.CustomerID, c.Suchname, c.Geburtstag

        Dim liste As New Dictionary(Of Long, String)

        For Each itm In bd
            Dim cust = itm.Suchname & " (" & DateDiff(DateInterval.Year, CDate(itm.Geburtstag), Now) & " Jahre)"

            liste.Add(itm.CustomerID, cust)
        Next
        e.Result = liste

        _context.Dispose()

    End Sub

    Private Sub bgw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles bgw.RunWorkerCompleted
        If e.Error Is Nothing Then
            Birthdays = e.Result
            RaisePropertyChanged("Birthdays")
        End If
    End Sub


End Class
