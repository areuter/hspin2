﻿Imports Prism.Modularity
Imports Prism.Regions

Imports HSPinOne.Infrastructure
Imports System.Windows.Input
Imports System.ComponentModel.Composition
Imports System.Windows.Threading
Imports Prism.Events

Namespace ViewModels



    Public Class DashboardViewModel
        Implements INavigationAware


        'Public Property ShowDashboardCommand As ICommand
        Private _regionmanager As IRegionManager
        Private _ea As IEventAggregator

        Public Property RefreshCommand As ICommand

        Public Sub ShowDashboard()

            'Dim mainRegion As IRegion = regionManager.Regions(RegionNames.MainRegion)
            'For Each v In mainRegion.Views
            '    mainRegion.Remove(v)
            'Next
            'regionManager.RegisterViewWithRegion(RegionNames.MainRegion, GetType(DashboardView))

        End Sub

        Public Sub New()




            'ShowDashboardCommand = New RelayCommand(AddressOf ShowDashboard)
            Me._regionmanager = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IRegionManager)
            _ea = Events.EventInstance.EventAggregator

            RefreshCommand = New RelayCommand(AddressOf Refresh)

        End Sub

        Private Sub Refresh()
            _ea.GetEvent(Of DashboardRefreshEvent)().Publish(Nothing)
        End Sub

        Public Function IsNavigationTarget(navigationContext As Prism.Regions.NavigationContext) As Boolean Implements Prism.Regions.INavigationAware.IsNavigationTarget
            'Dim rib As New Uri("DashboardRibbonTabView", UriKind.Relative)
            '_regionmanager.RequestNavigate(RegionNames.RibbonRegion, rib)

            Return True
        End Function

        Public Sub OnNavigatedFrom(navigationContext As Prism.Regions.NavigationContext) Implements Prism.Regions.INavigationAware.OnNavigatedFrom

        End Sub

        Public Sub OnNavigatedTo(navigationContext As Prism.Regions.NavigationContext) Implements Prism.Regions.INavigationAware.OnNavigatedTo

        End Sub


    End Class
End Namespace