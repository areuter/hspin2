﻿Imports System.Collections.ObjectModel
Imports System.ComponentModel
Imports HSPinOne.Infrastructure
Imports HSPinOne.DAL
Imports System.Data.Entity
Imports Prism.Events

Public Class LastBillingsViewModel
    Inherits ViewModelBase

    Dim WithEvents bgw As New BackgroundWorker
    Private _ea As IEventAggregator



    Private _Billings As ObservableCollection(Of String)
    Public Property Billings() As ObservableCollection(Of String)
        Get
            Return _Billings
        End Get
        Set(ByVal value As ObservableCollection(Of String))
            _Billings = value
        End Set
    End Property


    Public Sub New()
        Refresh()
        _ea = Events.EventInstance.EventAggregator
        _ea.GetEvent(Of DashboardRefreshEvent).Subscribe(AddressOf Refresh)
    End Sub

    Private Sub Refresh()
        bgw.WorkerReportsProgress = False
        bgw.RunWorkerAsync()
    End Sub


    Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw.DoWork

        Try

            ' Letzte zehn Buchungen heute
            Dim _context As New in1Entities
            Dim von = Now.Date
            Dim bis = Now.Date.AddDays(1)

            Dim query = _context.Billings _
                            .Include(Function(o) o.Customer) _
                            .Where(Function(o) o.Rechnungsdatum > von AndAlso o.Rechnungsdatum < bis _
                            AndAlso Not o.CourseID Is Nothing AndAlso o.IsStorno = False) _
                            .OrderByDescending(Function(o) o.Rechnungsdatum) _
                            .Take(15)


            Dim _liste As New ObservableCollection(Of String)

            For Each itm In query
                'String zusammenfügen
                Dim eventname As String = ""

                eventname = itm.Rechnungsdatum.ToShortDateString & " " & itm.Verwendungszweck & " " & itm.Customer?.Suchname

                _liste.Add(eventname)
            Next
            e.Result = _liste
            _context.Dispose()

        Catch ex As Exception

        End Try

    End Sub

    Private Sub bgw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles bgw.RunWorkerCompleted
        If e.Error Is Nothing Then
            Billings = e.Result
            RaisePropertyChanged("Billings")
        End If
    End Sub

End Class
