﻿Imports HSPinOne.Common
Imports HSPinOne.Infrastructure
Imports System.ComponentModel.Composition
Imports System.ComponentModel
Imports CommonServiceLocator
Imports Prism.Events
Imports Prism.Mvvm

Public Class NewsFeedViewModel
    Inherits BindableBase

    Dim WithEvents bgw As New BackgroundWorker

    Private _ea As IEventAggregator


    Private _news As List(Of NewsArticle)
    Public Property News As List(Of NewsArticle)
        Get
            Return _news
        End Get
        Set(ByVal value As List(Of NewsArticle))
            _news = value
        End Set
    End Property

    Public Sub New()
        Refresh()
        _ea = Events.EventInstance.EventAggregator
        _ea.GetEvent(Of DashboardRefreshEvent).Subscribe(AddressOf Refresh)
    End Sub

    Private Sub Refresh()
        bgw.WorkerReportsProgress = False
        bgw.RunWorkerAsync()
    End Sub


    Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw.DoWork
        Dim ns = ServiceLocator.Current.GetInstance(Of INewsFeedService)()


        Dim feedurl = HSPinOne.Infrastructure.GlobalSettingService.HoleEinstellung("feedurl")
        If Not IsNothing(feedurl) Then
            News = ns.GetNews(feedurl)
            If News.Count = 0 Then
                Dim na As New NewsArticle With {.Title = "Keine News"}
            End If
        End If


    End Sub

    Private Sub bgw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles bgw.RunWorkerCompleted
        If e.Error Is Nothing Then
            RaisePropertyChanged("News")
        End If
    End Sub

End Class
