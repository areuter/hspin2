﻿
Imports Prism.Ioc
Imports Prism.Modularity
Imports Prism.Regions
Imports HSPinOne.Infrastructure

Public Class DashboardModule
    Implements IModule

    Public Sub RegisterTypes(containerRegistry As IContainerRegistry) Implements IModule.RegisterTypes
        containerRegistry.RegisterForNavigation(GetType(DashboardView), "DashboardView")
    End Sub

    Public Sub OnInitialized(containerProvider As IContainerProvider) Implements IModule.OnInitialized

        Dim rm = containerProvider.Resolve(Of IRegionManager)()
        rm.RegisterViewWithRegion(RegionNames.TaskRegion, GetType(DashboardOutlookSection))
    End Sub
End Class
