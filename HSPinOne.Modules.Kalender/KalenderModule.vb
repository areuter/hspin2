﻿Imports HSPinOne.Infrastructure
Imports Prism.Ioc
Imports Prism.Modularity
Imports Prism.Regions

Public Class KalenderModule
    Implements IModule

    Public Sub RegisterTypes(containerRegistry As IContainerRegistry) Implements IModule.RegisterTypes

        containerRegistry.RegisterForNavigation(GetType(KalenderView1), "KalenderView1")
    End Sub

    Public Sub OnInitialized(containerProvider As IContainerProvider) Implements IModule.OnInitialized
        Dim rm = containerProvider.Resolve(Of IRegionManager)()
        rm.RegisterViewWithRegion(RegionNames.TaskRegion, GetType(KalenderOutlookSection))
        'rm.RegisterViewWithRegion(RegionNames.MainRegion, GetType(KalenderView1))
    End Sub
End Class
