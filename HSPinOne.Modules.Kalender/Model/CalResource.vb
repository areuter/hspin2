﻿Public Class CalResource



    Private _ResourceID As Integer
    Public Property ResourceID() As Integer
        Get
            Return _ResourceID
        End Get
        Set(ByVal value As Integer)
            _ResourceID = value
        End Set
    End Property

    Private _ResourceName As String
    Public Property ResourceName() As String
        Get
            Return _ResourceName
        End Get
        Set(ByVal value As String)
            _ResourceName = value
        End Set
    End Property

    Private _Color As Integer
    Public Property Color() As Integer
        Get
            Return _Color
        End Get
        Set(ByVal value As Integer)
            _Color = value
        End Set
    End Property




End Class
