﻿Imports System.Windows.Media

Public Class SchedulerCustomData

    Private _bgcolor As Brush
    Public Property bgcolor() As Brush
        Get
            Return _bgcolor
        End Get
        Set(ByVal value As Brush)
            _bgcolor = value
        End Set
    End Property

End Class
