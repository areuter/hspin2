﻿Public Class DBAppointment



    Private _Start As DateTime
    Public Property Start() As DateTime
        Get
            Return _Start
        End Get
        Set(ByVal value As DateTime)
            _Start = value
        End Set
    End Property

    Private _Ende As DateTime
    Public Property Ende() As DateTime
        Get
            Return _Ende
        End Get
        Set(ByVal value As DateTime)
            _Ende = value
        End Set
    End Property

    Private _AppointmentID As Integer
    Public Property AppointmentID() As Integer
        Get
            Return _AppointmentID
        End Get
        Set(ByVal value As Integer)
            _AppointmentID = value
        End Set
    End Property

    Private _BookingID As Nullable(Of Integer)
    Public Property BookingID() As Nullable(Of Integer)
        Get
            Return _BookingID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _BookingID = value
        End Set
    End Property

    Private _CourseID As Nullable(Of Integer)
    Public Property CourseID() As Nullable(Of Integer)
        Get
            Return _CourseID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _CourseID = value
        End Set
    End Property


    Private _LocationName As String
    Public Property LocationName() As String
        Get
            Return _LocationName
        End Get
        Set(ByVal value As String)
            _LocationName = value
        End Set
    End Property

    Private _LocationID As Integer
    Public Property LocationID() As Integer
        Get
            Return _LocationID
        End Get
        Set(ByVal value As Integer)
            _LocationID = value
        End Set
    End Property

    't.Start, t.Ende, t.AppointmentID, t.BookingID, t.Location.LocationName, t.LocationID, t.CourseID,
    '                        t.Booking.Subject, t.Booking.Color, t.Booking.Description, t.Booking.Category.CategoryName, _
    '                        t.Course.Sport.Sportname, t.Course.Zusatzinfo, t.Canceled, t.Course.Party.Partycolor,
    '                        t.Course.CourseState.CourseStateID
    Private _Subject As String
    Public Property Subject() As String
        Get
            Return _Subject
        End Get
        Set(ByVal value As String)
            _Subject = value
        End Set
    End Property

    Private _Color As String
    Public Property Color() As String
        Get
            Return _Color
        End Get
        Set(ByVal value As String)
            _Color = value
        End Set
    End Property

    Private _Description As String
    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal value As String)
            _Description = value
        End Set
    End Property

    Private _CategoryName As String
    Public Property CategoryName() As String
        Get
            Return _CategoryName
        End Get
        Set(ByVal value As String)
            _CategoryName = value
        End Set
    End Property

    Private _Sportname As String
    Public Property Sportname() As String
        Get
            Return _Sportname
        End Get
        Set(ByVal value As String)
            _Sportname = value
        End Set
    End Property

    Private _Zusatzinfo As String
    Public Property Zusatzinfo() As String
        Get
            Return _Zusatzinfo
        End Get
        Set(ByVal value As String)
            _Zusatzinfo = value
        End Set
    End Property

    Private _CourseStateID As Nullable(Of Integer)
    Public Property CourseStateID() As Nullable(Of Integer)
        Get
            Return _CourseStateID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _CourseStateID = value
        End Set
    End Property


    Private _Canceled As Boolean
    Public Property Canceled() As Boolean
        Get
            Return _Canceled
        End Get
        Set(ByVal value As Boolean)
            _Canceled = value
        End Set
    End Property

    Private _Partycolor As String
    Public Property Partycolor() As String
        Get
            Return _Partycolor
        End Get
        Set(ByVal value As String)
            _Partycolor = value
        End Set
    End Property








End Class
