﻿Imports System.Windows.Media

Public Class CalAppointment

    Private _UniqueID As Integer
    Public Property UniqueID() As Integer
        Get
            Return _UniqueID
        End Get
        Set(ByVal value As Integer)
            _UniqueID = value
        End Set
    End Property

    Private _Type As Integer
    Public Property Type() As Integer
        Get
            Return _Type
        End Get
        Set(ByVal value As Integer)
            _Type = value
        End Set
    End Property

    Private _StartDate As DateTime
    Public Property StartDate() As DateTime
        Get
            Return _StartDate
        End Get
        Set(ByVal value As DateTime)
            _StartDate = value
        End Set
    End Property

    Private _EndDate As DateTime
    Public Property EndDate() As DateTime
        Get
            Return _EndDate
        End Get
        Set(ByVal value As DateTime)
            _EndDate = value
        End Set
    End Property

    Private _AllDay As Boolean
    Public Property AllDay() As Boolean
        Get
            Return _AllDay
        End Get
        Set(ByVal value As Boolean)
            _AllDay = value
        End Set
    End Property

    Private _Subject As String
    Public Property Subject() As String
        Get
            Return _Subject
        End Get
        Set(ByVal value As String)
            _Subject = value
        End Set
    End Property

    Private _Location As String
    Public Property Location() As String
        Get
            Return _Location
        End Get
        Set(ByVal value As String)
            _Location = value
        End Set
    End Property

    Private _Description As String
    Public Property Description() As String
        Get
            Return _Description
        End Get
        Set(ByVal value As String)
            _Description = value
        End Set
    End Property

    Private _Status As Integer = 0
    Public Property Status() As Integer
        Get
            Return _Status
        End Get
        Set(ByVal value As Integer)
            _Status = value
        End Set
    End Property

    Private _Label As Integer
    Public Property Label() As Integer
        Get
            Return _Label
        End Get
        Set(ByVal value As Integer)
            _Label = value
        End Set
    End Property

    Private _LocationID As Integer
    Public Property LocationID() As Integer
        Get
            Return _LocationID
        End Get
        Set(ByVal value As Integer)
            _LocationID = value
        End Set
    End Property

    Private _ReminderInfo As String
    Public Property ReminderInfo() As String
        Get
            Return _ReminderInfo
        End Get
        Set(ByVal value As String)
            _ReminderInfo = value
        End Set
    End Property

    Private _ReccurenceInfo As String
    Public Property RecurrenceInfo() As String
        Get
            Return _ReccurenceInfo
        End Get
        Set(ByVal value As String)
            _ReccurenceInfo = value
        End Set
    End Property

    Private _BookingID As Nullable(Of Integer)
    Public Property BookingID() As Nullable(Of Integer)
        Get
            Return _BookingID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _BookingID = value
        End Set
    End Property


    Private _CourseID As Nullable(Of Integer)
    Public Property CourseID() As Nullable(Of Integer)
        Get
            Return _CourseID
        End Get
        Set(ByVal value As Nullable(Of Integer))
            _CourseID = value
        End Set
    End Property


    Private _BgColor As String
    Public Property BgColor() As String
        Get
            Return _BgColor
        End Get
        Set(ByVal value As String)
            _BgColor = value
        End Set
    End Property









End Class
