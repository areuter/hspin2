﻿
Imports System.Windows.Controls
Imports HSPinOne.DAL
Imports Prism.Regions
Imports HSPinOne.Infrastructure
Imports System.ComponentModel.Composition
Imports System.Diagnostics.CodeAnalysis
Imports System.Windows
Imports System.Printing
Imports Telerik.Windows.Controls
Imports System.Windows.Media.Imaging
Imports Prism.Events


Public Class KalenderView1

    Private _ea As IEventAggregator

    Public Sub New()

        _ea = Events.EventInstance.EventAggregator

        ' This call is required by the designer.
        InitializeComponent()



        _ea.GetEvent(Of PrintScheduleEvent).Subscribe(AddressOf PrintAction)

        Me.DataContext = New KalenderViewModel1()

    End Sub



    Private Sub Scheduler_AppointmentCreating(ByVal sender As Object, ByVal e As AppointmentCreatingEventArgs) Handles Scheduler.AppointmentCreating

        Dim book As New Booking
        'book.LocationID = e.Appointment
        Dim vm = CType(Me.DataContext, KalenderViewModel1)

        e.Cancel = True
    End Sub

    Private Sub Scheduler_AppointmentDeleting(ByVal sender As Object, ByVal e As AppointmentDeletingEventArgs) Handles Scheduler.AppointmentDeleting
        e.Cancel = True
    End Sub

    Private Sub Scheduler_AppointmentEditing(ByVal sender As Object, ByVal e As AppointmentEditingEventArgs) Handles Scheduler.AppointmentEditing
        e.Cancel = True
    End Sub

    Private Sub Scheduler_ShowDialog(ByVal sender As Object, ByVal e As ShowDialogEventArgs) Handles Scheduler.ShowDialog
        e.Cancel = True
    End Sub

    Private Sub PrintActionalt()
        Try

            Dim printdlg As New PrintDialog
            Dim pt As PrintTicket = printdlg.PrintTicket
            pt.PageMediaSize = New PageMediaSize(PageMediaSizeName.Unknown, 1122.24, 793.92)
            pt.PageOrientation = PageOrientation.Landscape

            If printdlg.ShowDialog = True Then


                Dim cal = Me.Scheduler

                Dim v As Media.Visual = Me.ParentGrid
                Dim e As System.Windows.FrameworkElement = Me.ParentGrid

                Dim oldmin = cal.MinTimeRulerExtent
                Dim oldmax = cal.MaxTimeRulerExtent

                ''store original scale
                Dim originalScale As Media.Transform = e.LayoutTransform

                Dim capabilities = printdlg.PrintQueue.GetPrintCapabilities(pt)

                ''get scale of the print wrt to screen of WPF visual
                Dim scale As Double = Math.Min(capabilities.PageImageableArea.ExtentWidth / e.ActualWidth, capabilities.PageImageableArea.ExtentHeight / e.ActualHeight)
                Dim scaleX As Double = capabilities.PageImageableArea.ExtentWidth / e.ActualWidth
                Dim scaleY As Double = capabilities.PageImageableArea.ExtentHeight / e.ActualHeight

                Dim printWidth As Double = capabilities.PageImageableArea.ExtentWidth
                Dim printHeight As Double = capabilities.PageImageableArea.ExtentHeight

                'Transform the Visual to scale
                'e.LayoutTransform = New Media.ScaleTransform(scaleX, scaleY)


                'e.Width = printWidth
                'e.Height = printHeight
                'cal.MinTimeRulerExtent = printHeight - 100
                'cal.MaxTimeRulerExtent = printHeight
                cal.MinTimeRulerExtent = e.RenderSize.Height
                cal.MaxTimeRulerExtent = e.RenderSize.Height

                NavLeft.Visibility = Visibility.Hidden
                NavRight.Visibility = System.Windows.Visibility.Hidden

                'get the size of the printer page
                Dim sz As System.Windows.Size = New System.Windows.Size(printWidth - 80, printHeight - 80)

                'update the layout of the visual to the printer page size.
                e.Measure(sz)
                e.Arrange(New System.Windows.Rect(New System.Windows.Point(40, 40), sz))

                cal.UpdateLayout()

                'now print the visual to printer to fit on the one page.
                printdlg.PrintVisual(v, "My Print")

                'apply the original transform.
                e.LayoutTransform = originalScale

                'printdlg.PrintTicket.PageOrientation = PageOrientation.Landscape


                'Dim size = ParentGrid.RenderSize
                'ParentGrid.Height = printHeight
                'ParentGrid.Width = printWidth

                'ParentGrid.Measure(New Size(printWidth, printHeight))
                'ParentGrid.Arrange(New Rect(20, 20, printWidth - 40, printHeight - 40))
                'cal.UpdateLayout()



                'printdlg.PrintVisual(ParentGrid, "Wochenplan")


                'Aufräumen
                e.Width = Double.NaN
                e.Height = Double.NaN
                'cal.MinTimeRulerExtent = oldmin
                'cal.MaxTimeRulerExtent = oldmax

                cal.UpdateLayout()


                NavLeft.Visibility = Visibility.Visible
                NavRight.Visibility = System.Windows.Visibility.Visible

            End If





        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub PrintCanvasAction()
        Dim scheduleView As RadScheduleView = Me.Scheduler
        Dim canvas As Canvas = Me.printCanvas

        Dim oldmin = scheduleView.MinTimeRulerExtent
        Dim oldmax = scheduleView.MaxTimeRulerExtent
        Dim sc = scheduleView.VerticalScrollBarVisibility

        Dim printDlg = New PrintDialog()
        Dim pt As PrintTicket = printDlg.PrintTicket
        pt.PageMediaSize = New PageMediaSize(PageMediaSizeName.Unknown, 1122.24, 793.92)
        pt.PageOrientation = PageOrientation.Landscape

        If printDlg.ShowDialog() = True Then

            CalGrid.Children.Remove(scheduleView)

            Dim oldbg = canvas.Background
            canvas.Background = Media.Brushes.White

            Dim tb = New TextBlock With {.Text = txtAnzeige.Text, .FontSize = txtAnzeige.FontSize, .Foreground = Media.Brushes.DarkBlue}

            Dim tb1 = New TextBlock With {.Text = GlobalSettingService.GetInstitution(), .FontSize = 12, .HorizontalAlignment = HorizontalAlignment.Right, .Foreground = Media.Brushes.DarkBlue}


            canvas.Children.Add(tb)
            tb.SetValue(Canvas.TopProperty, 10.0)
            tb.SetValue(Canvas.LeftProperty, 10.0)

            canvas.Children.Add(tb1)
            tb1.SetValue(Canvas.RightProperty, 20.0)
            tb1.SetValue(Canvas.TopProperty, 10.0)

            canvas.Children.Add(scheduleView)
            scheduleView.SetValue(Canvas.TopProperty, 50.0)
            scheduleView.SetValue(Canvas.LeftProperty, 10.0)

            'get selected printer capabilities
            Dim capabilities = printDlg.PrintQueue.GetPrintCapabilities(pt)
            Dim printWidth As Double = capabilities.PageImageableArea.ExtentWidth
            Dim printHeight As Double = capabilities.PageImageableArea.ExtentHeight



            Dim pageSize As Size = New Size(printDlg.PrintableAreaWidth, printDlg.PrintableAreaHeight)


            canvas.Height = pageSize.Height
            canvas.Width = pageSize.Width


            'canvas.Measure(New Size(canvas.RenderSize.Width, printHeight))


            scheduleView.MinTimeRulerExtent = canvas.Height - 100
            scheduleView.MaxTimeRulerExtent = canvas.Height - 100
            scheduleView.VerticalScrollBarVisibility = ScrollBarVisibility.Hidden



            'scheduleView.Measure(canvas.RenderSize)
            scheduleView.Width = canvas.Width - 30
            scheduleView.Height = canvas.Height - 70
            scheduleView.UpdateLayout()

            Dim scale As Double = Math.Min(capabilities.PageImageableArea.ExtentWidth / canvas.ActualWidth, capabilities.PageImageableArea.ExtentHeight / canvas.ActualHeight)

            '//Transform the Visual to scale
            'canvas.LayoutTransform = New Media.ScaleTransform(scale, scale)
            canvas.Measure(pageSize)
            canvas.Arrange(New Rect(5, 5, pageSize.Width, pageSize.Height))

            'canvas.UpdateLayout()


            printDlg.PrintVisual(Me.printCanvas, "Kalender")

            scheduleView.MinTimeRulerExtent = oldmin
            scheduleView.MaxTimeRulerExtent = oldmax
            scheduleView.VerticalScrollBarVisibility = sc

            canvas.Children.Remove(scheduleView)
            canvas.Children.Remove(tb)
            canvas.Children.Remove(tb1)
            canvas.Background = oldbg

            CalGrid.Children.Add(scheduleView)
            scheduleView.Height = Double.NaN
            scheduleView.Width = Double.NaN


        End If
    End Sub

    Private Sub PrintAction()


        Dim printDlg = New PrintDialog()
        Dim pt As PrintTicket = printDlg.PrintTicket
        pt.PageMediaSize = New PageMediaSize(PageMediaSizeName.Unknown, 1122.24, 793.92)
        pt.PageOrientation = PageOrientation.Landscape







        If printDlg.ShowDialog() = True Then

            Dim wc As New WaitCursor
            Try
                Dim capabilities = printDlg.PrintQueue.GetPrintCapabilities(pt)

                Dim pageSize = New Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight)

                Dim printWidth As Double = capabilities.PageImageableArea.ExtentWidth
                Dim printHeight As Double = capabilities.PageImageableArea.ExtentHeight


                'because 96 pixels in an inch for WPF window
                Dim marginLeft As Double = 96.0 * 0.5
                Dim marginTop As Double = 96.0 * 0.5
                Dim marginRight As Double = 96.0 * 0.5
                Dim marginBottom As Double = 96.0 * 0.5

                Dim newSize = New Size(printWidth - (marginLeft + marginRight), printHeight - (marginTop + marginBottom))

                Dim scheduleView As RadScheduleView = Me.Scheduler
                Dim canvas As Canvas = Me.printCanvas

                Dim oldmin = scheduleView.MinTimeRulerExtent
                Dim oldmax = scheduleView.MaxTimeRulerExtent
                Dim sc = scheduleView.VerticalScrollBarVisibility


                CalGrid.Children.Remove(scheduleView)

                Dim oldbg = canvas.Background
                canvas.Background = Media.Brushes.White
                canvas.Height = pageSize.Height
                canvas.Width = pageSize.Width

                Dim tb = New TextBlock With {.Text = txtAnzeige.Text, .FontSize = txtAnzeige.FontSize, .Foreground = Media.Brushes.DarkBlue}

                Dim tb1 = New TextBlock With {.Text = GlobalSettingService.GetInstitution(), .FontSize = 12, .HorizontalAlignment = HorizontalAlignment.Right, .Foreground = Media.Brushes.DarkBlue}

                canvas.Children.Add(scheduleView)

                'scheduleView.Measure(canvas.RenderSize)
                scheduleView.Width = newSize.Width
                scheduleView.Height = newSize.Height
                scheduleView.MinTimeRulerExtent = newSize.Height - 100
                scheduleView.MaxTimeRulerExtent = newSize.Height - 100
                scheduleView.VerticalScrollBarVisibility = ScrollBarVisibility.Hidden
                scheduleView.UpdateLayout()

                'Dim scale As Double = Math.Min(capabilities.PageImageableArea.ExtentWidth / canvas.ActualWidth, capabilities.PageImageableArea.ExtentHeight / canvas.ActualHeight)

                '//Transform the Visual to scale
                'canvas.LayoutTransform = New Media.ScaleTransform(scale, scale)

                Const dpi = 300
                Dim bmp As RenderTargetBitmap = New RenderTargetBitmap(CType(scheduleView.Width / 96 * dpi, Integer), CType(scheduleView.Height / 96 * dpi, Integer), dpi, dpi, Media.PixelFormats.Pbgra32)
                bmp.Render(scheduleView)

                canvas.Children.Remove(scheduleView)

                CalGrid.Children.Add(scheduleView)
                scheduleView.MinTimeRulerExtent = oldmin
                scheduleView.MaxTimeRulerExtent = oldmax
                scheduleView.VerticalScrollBarVisibility = sc

                scheduleView.Height = Double.NaN
                scheduleView.Width = Double.NaN

                Dim img As New Image
                img.Source = bmp

                img.Stretch = Media.Stretch.Fill
                img.StretchDirection = StretchDirection.Both






                Dim gr As New Grid

                gr.Width = newSize.Width
                gr.Height = newSize.Height
                gr.RowDefinitions.Add(New RowDefinition With {.Height = GridLength.Auto})
                gr.RowDefinitions.Add(New RowDefinition With {.Height = New GridLength(1, GridUnitType.Star)})
                gr.ColumnDefinitions.Add(New ColumnDefinition With {.Width = New GridLength(1, GridUnitType.Star)})
                gr.ColumnDefinitions.Add(New ColumnDefinition With {.Width = New GridLength(1, GridUnitType.Star)})

                gr.Children.Add(img)
                Grid.SetRow(img, 1)
                Grid.SetColumnSpan(img, 2)

                gr.Children.Add(tb)
                tb1.Margin = New Thickness(0, 0, 0, 15)

                gr.Children.Add(tb1)
                Grid.SetColumn(tb1, 1)
                tb1.TextAlignment = TextAlignment.Right

                ' centralize print area
                Dim xStartPrint As Double = marginLeft
                Dim yStartPrint As Double = marginTop
                Dim xEndPoint As Double = printWidth - marginRight
                Dim yEndPoint As Double = printHeight - marginBottom

                gr.Measure(newSize)

                gr.Arrange(New Rect(New Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight), pageSize))
                'gr.Arrange(New Rect(New Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight), sz))


                'Dim win As New Window
                'win.Content = pCanvas
                'win.ShowDialog()

                printDlg.PrintVisual(gr, "Kalender")


                canvas.Background = oldbg
                wc.Dispose()



            Catch ex As Exception
                If Not IsNothing(wc) Then wc.Dispose()
                Throw New Exception(ex.Message)
            End Try

        End If
    End Sub


    'Private Sub CreatePDF()
    '    Dim dialog = New HSPinOne.Infrastructure.DialogService


    '    If (dialog.SaveFile("*.pdf", "kalender.pdf", "pdf")) Then


    '        Dim Document = CreateDocument()
    '        Document.LayoutMode = DocumentLayoutMode.Flow
    '        Document.Measure(RadDocument.MAX_DOCUMENT_SIZE)
    '        Document.Arrange(New RectangleF(PointF.Empty, Document.DesiredSize))
    '        Document.SectionDefaultPageSize = New Size(1500, 2300)
    '        Dim provider = New Telerik.Windows.Documents.FormatProviders.Pdf.PdfFormatProvider()

    '        Using output As Stream = dialog.OpenFile()

    '            provider.Export(Document, output)
    '        End Using
    '    End If
    'End Sub
    'Private Function CreateDocument() As RadDocument

    '    Dim section = New Telerik.Windows.Documents.Model.Section()
    '    Dim paragraph = New Telerik.Windows.Documents.Model.Paragraph()
    '    Dim bi = New BitmapImage()

    '    Dim image As Telerik.Windows.Documents.Model.ImageInline = Nothing

    '    Using ms As New MemoryStream()

    '        Telerik.Windows.Media.Imaging.ExportExtensions.ExportToImage(Me.Scheduler, ms, New PngBitmapEncoder())
    '        bi.BeginInit()
    '        bi.StreamSource = ms

    '        bi.EndInit()
    '        image = New Telerik.Windows.Documents.Model.ImageInline(New WriteableBitmap(bi))
    '    End Using

    '    paragraph.Inlines.Add(image)
    '    section.Blocks.Add(paragraph)

    '    Dim Document = New RadDocument()
    '    Document.Sections.Add(section)

    '    Return Document
    'End Function


End Class






