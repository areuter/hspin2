﻿
Imports System.Windows.Controls
Imports HSPinOne.DAL
Imports Microsoft.Practices.Prism.Regions
Imports HSPinOne.Infrastructure
Imports System.ComponentModel.Composition
Imports System.Diagnostics.CodeAnalysis
Imports System.Windows
Imports System.Printing
Imports DevExpress.Xpf.Scheduler
Imports DevExpress.Xpf.Scheduler.Drawing
Imports System.Windows.Media




<Export("KalenderView2"), PartCreationPolicy(CreationPolicy.Shared)> _
Public Class KalenderView2



    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        Events.EVAG.GetEvent(Of PrintScheduleEvent).Subscribe(AddressOf PrintAction)


    End Sub

    Private Sub schedulerControl1_EditAppointmentFormShowing(sender As Object, e As EditAppointmentFormEventArgs) Handles schedulerControl1.EditAppointmentFormShowing
        Dim dc = CType(Me.DataContext, KalenderViewModel2)
        If Not IsNothing(dc) Then
            dc.AppDblClickCommand.Execute(Me.schedulerControl1.Storage)
            e.Cancel = True
        End If

    End Sub

    Private Sub schedulerControl1_AppointmentViewInfoCustomizing(ByVal sender As Object, _
                                                                 ByVal e As AppointmentViewInfoCustomizingEventArgs)
        Dim viewInfo As AppointmentViewInfo = e.ViewInfo
        Dim mycolor As String = "#FFFFFF"

        If Not IsNothing(viewInfo.Appointment) Then
            If Not Trim(viewInfo.Appointment.Subject) = "" Then
                If Not IsNothing(viewInfo.Appointment.CustomFields("BgColor")) Then
                    mycolor = viewInfo.Appointment.CustomFields("BgColor")
                End If
            End If

            Dim labels = Me.schedulerControl1.Storage.AppointmentStorage.Labels
            Dim existingid As Integer = 0
            If Trim(mycolor).Length = 0 Then mycolor = "#FFFFFF"
            Dim col = Convert.ToInt32(Right(System.Windows.Media.ColorConverter.ConvertFromString(mycolor).ToString, 6), 16)

            For Each itm In labels
                If Right(itm.DisplayName, 6) = Right(mycolor, 6) Then
                    existingid = itm.Id
                End If
            Next

            If existingid > 0 Then
                viewInfo.Appointment.LabelKey = existingid
            Else
                Dim lbl As New DevExpress.Xpf.Scheduler.AppointmentLabel
                lbl.Color = System.Windows.Media.ColorConverter.ConvertFromString(mycolor)
                lbl.DisplayName = mycolor
                labels.Add(lbl)
                viewInfo.Appointment.LabelKey = lbl.Id
            End If
        End If
    End Sub



    Private Sub PrintAction()
        Try

            Dim printdlg As New PrintDialog
            If printdlg.ShowDialog = True Then


                Dim cal = Me.schedulerControl1

                Dim oldmargin = cal.Margin

                cal.Views.DayView.TimeMarkerVisibility = DevExpress.XtraScheduler.TimeMarkerVisibility.Never
                cal.Views.WorkWeekView.TimeMarkerVisibility = DevExpress.XtraScheduler.TimeMarkerVisibility.Never


                NavLeft.Visibility = Visibility.Hidden
                NavRight.Visibility = Windows.Visibility.Hidden

                printdlg.PrintTicket.PageOrientation = PageOrientation.Landscape

                Dim capabilities = printdlg.PrintQueue.GetPrintCapabilities(printdlg.PrintTicket)



                Dim printWidth As Double = capabilities.PageImageableArea.ExtentWidth - 50
                Dim printHeight As Double = capabilities.PageImageableArea.ExtentHeight - 20
                Dim size = ParentGrid.RenderSize
                ParentGrid.Height = printHeight
                ParentGrid.Width = printWidth


                'cal.MinTimeRulerExtent = printHeight - 100
                'cal.MaxTimeRulerExtent = printHeight
                ParentGrid.Measure(New Size(ParentGrid.RenderSize.Width, printHeight))
                ParentGrid.Arrange(New Rect(60, 20, ParentGrid.RenderSize.Width - 20, printHeight - 20))


                cal.Margin = New Thickness(20, 0, 0, 0)
                cal.UpdateLayout()



                printdlg.PrintVisual(ParentGrid, "Wochenplan")


                'Aufräumen
                ParentGrid.Width = Double.NaN
                ParentGrid.Height = Double.NaN
                'cal.MinTimeRulerExtent = oldmin
                'cal.MaxTimeRulerExtent = oldmax

                NavLeft.Visibility = Visibility.Visible
                NavRight.Visibility = Windows.Visibility.Visible

                cal.Views.DayView.TimeMarkerVisibility = DevExpress.XtraScheduler.TimeMarkerVisibility.Always
                cal.Views.WorkWeekView.TimeMarkerVisibility = DevExpress.XtraScheduler.TimeMarkerVisibility.Always
                cal.Margin = oldmargin

                cal.UpdateLayout()
            End If





        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub





End Class








