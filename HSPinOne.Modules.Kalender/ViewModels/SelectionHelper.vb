﻿Imports System.Windows
Imports DevExpress.Xpf.Scheduler
Imports DevExpress.XtraScheduler

Public Class SelectionHelper
    ' Inherits DependencyObject



    Public Shared ctl As SchedulerControl = Nothing



    Public Shared Function GetSelectedResource(ByVal obj As DependencyObject) As Resource
        Return obj.GetValue(SelectedResourceProperty)
    End Function

    Public Shared Sub SetSelectedResource(ByVal obj As DependencyObject, ByVal value As Resource)
        obj.SetValue(SelectedResourceProperty, value)
    End Sub

    Public Shared ReadOnly SelectedResourceProperty As  _
                            DependencyProperty = DependencyProperty.RegisterAttached("SelectedResource", _
                            GetType(Resource), GetType(SelectionHelper), _
                            New PropertyMetadata(Nothing, AddressOf SelectedResourceChanged))

    Private Shared Sub SelectedResourceChanged(ByVal d As DependencyObject, ByVal e As DependencyPropertyChangedEventArgs)

        If IsNothing(ctl) Then
            ctl = CType(d, SchedulerControl)
            AddHandler ctl.SelectionChanged, AddressOf ctl_SelectionChanged

        End If
    End Sub

    Public Shared Sub ctl_SelectionChanged(ByVal sender As Object, ByVal e As EventArgs)
        If Not IsNothing(ctl.GetValue(SelectionHelper.SelectedResourceProperty)) Then
            If Not CType(ctl.GetValue(SelectionHelper.SelectedResourceProperty), Resource) Is ctl.ActiveView.SelectedResource Then
                ctl.SetValue(SelectionHelper.SelectedResourceProperty, ctl.ActiveView.SelectedResource)
            End If
        Else
            ctl.SetValue(SelectionHelper.SelectedResourceProperty, ctl.ActiveView.SelectedResource)
        End If
        ctl.SetValue(SelectionHelper.SelectedIntervalProperty, ctl.ActiveView.SelectedInterval)
    End Sub



    Public Shared Function GetSelectedInterval(ByVal element As DependencyObject) As TimeInterval
        If element Is Nothing Then
            Throw New ArgumentNullException("element")
        End If

        Return element.GetValue(SelectedIntervalProperty)
    End Function

    Public Shared Sub SetSelectedInterval(ByVal element As DependencyObject, ByVal value As TimeInterval)
        If element Is Nothing Then
            Throw New ArgumentNullException("element")
        End If

        element.SetValue(SelectedIntervalProperty, value)
    End Sub

    Public Shared ReadOnly SelectedIntervalProperty As  _
                           DependencyProperty = DependencyProperty.RegisterAttached("SelectedInterval", _
                           GetType(TimeInterval), GetType(SelectionHelper), _
                           New PropertyMetadata(Nothing, AddressOf SelectedIntervalChanged))


    Private Shared Sub SelectedIntervalChanged(ByVal d As DependencyObject, ByVal e As DependencyPropertyChangedEventArgs)

        If IsNothing(ctl) Then
            ctl = CType(d, SchedulerControl)
            AddHandler ctl.SelectionChanged, AddressOf ctl_SelectionChanged

        End If
    End Sub


End Class
