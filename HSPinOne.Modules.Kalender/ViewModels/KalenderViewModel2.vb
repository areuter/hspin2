﻿Imports System.ComponentModel.Composition
Imports System.ComponentModel
Imports System.Windows.Input
Imports System.Data.Entity
Imports System.Windows.Media
Imports HSPinOne.Infrastructure
Imports HSPinOne.DAL
Imports Microsoft.Practices.Prism.Regions
Imports System.Collections.ObjectModel
Imports HSPinOne.Modules.Verwaltung
Imports HSPinOne.Modules.Verwaltung.ViewModel
Imports DevExpress.XtraScheduler
Imports DevExpress.Xpf.Scheduler


<Export(GetType(KalenderViewModel2)), PartCreationPolicy(CreationPolicy.Shared)>
Public Class KalenderViewModel2
    Inherits Infrastructure.ViewModelBase
    Implements IRegionMemberLifetime

    Private _oldunit As Integer = 0


#Region "   Properties"


    Private viewChanged As Boolean = False
    Private firstload As Boolean = True
    Dim _context As in1Entities

    Private WithEvents bgw As New BackgroundWorker

    Private _pAppointments As New ObservableCollection(Of CalAppointment)
    Private _pResourceTypes As New ObservableCollection(Of CalResource)


    Public ReadOnly Property KeepAlive As Boolean Implements IRegionMemberLifetime.KeepAlive
        Get
            Return False
        End Get
    End Property

    Public Property Context As in1Entities
        Get
            Return _context
        End Get
        Set(ByVal value As in1Entities)
            _context = value
        End Set
    End Property

    Private _Appointments As ObservableCollection(Of CalAppointment) = New ObservableCollection(Of CalAppointment)

    Public Property Appointments As ObservableCollection(Of CalAppointment)
        Get
            Return _Appointments
        End Get
        Set(ByVal value As ObservableCollection(Of CalAppointment))
            _Appointments = value
        End Set
    End Property

    Private _resourceTypes As ObservableCollection(Of CalResource) = New ObservableCollection(Of CalResource)

    Public Property ResourceTypes As ObservableCollection(Of CalResource)
        Get
            Return _resourceTypes
        End Get
        Set(ByVal value As ObservableCollection(Of CalResource))
            _resourceTypes = value
        End Set
    End Property





    Private _SelectedAppointments As ObservableCollection(Of DevExpress.XtraScheduler.Appointment) = New ObservableCollection(Of DevExpress.XtraScheduler.Appointment)
    Public Property SelectedAppointments() As ObservableCollection(Of DevExpress.XtraScheduler.Appointment)
        Get
            Return _SelectedAppointments
        End Get
        Set(ByVal value As ObservableCollection(Of DevExpress.XtraScheduler.Appointment))
            _SelectedAppointments = value
        End Set
    End Property


    Private _SelectedInterval As DevExpress.XtraScheduler.TimeInterval = New TimeInterval
    Public Property SelectedInterval() As DevExpress.XtraScheduler.TimeInterval
        Get
            Return _SelectedInterval
        End Get
        Set(ByVal value As DevExpress.XtraScheduler.TimeInterval)
            _SelectedInterval = value
        End Set
    End Property

    Private _SelectedResource As DevExpress.XtraScheduler.Resource
    Public Property SelectedResource() As DevExpress.XtraScheduler.Resource
        Get
            Return _SelectedResource
        End Get
        Set(ByVal value As DevExpress.XtraScheduler.Resource)
            _SelectedResource = value
        End Set
    End Property


    Private _Labels As AppointmentLabelCollection
    Public Property Labels() As AppointmentLabelCollection
        Get
            Return _Labels
        End Get
        Private Set(ByVal value As AppointmentLabelCollection)
            _Labels = value
        End Set
    End Property

    Private _AppointmentStates As AppointmentStatusCollection
    Public Property AppointmentStates() As AppointmentStatusCollection
        Get
            Return _AppointmentStates
        End Get
        Set(ByVal value As AppointmentStatusCollection)
            _AppointmentStates = value
        End Set
    End Property

    Private _dayDate As DateTime

    Public Property DayDate As DateTime
        Get
            Return _dayDate
        End Get
        Set(ByVal value As DateTime)
            _dayDate = value
            OnPropertyChanged("DayDate")
        End Set
    End Property

    Private _weekstart As DateTime

    Public Property Weekstart As DateTime
        Get
            Return _weekstart
        End Get
        Set(ByVal value As DateTime)
            _weekstart = value
            OnPropertyChanged("Weekstart")
        End Set
    End Property

    Private _weekend As DateTime

    Public Property Weekend As DateTime
        Get
            Return _weekend
        End Get
        Set(ByVal value As DateTime)
            _weekend = value
            OnPropertyChanged("Weekend")
        End Set
    End Property


    Private _ansichtsmodus As String

    Public Property Ansichtsmodus As String
        Get
            Return _ansichtsmodus
        End Get
        Set(ByVal value As String)
            _ansichtsmodus = value
        End Set
    End Property

    Private _ActiveViewType As String = "WorkWeek"
    Public Property ActiveViewType() As String
        Get
            Return _ActiveViewType
        End Get
        Set(ByVal value As String)
            _ActiveViewType = value
            OnPropertyChanged("ActiveViewType")
        End Set
    End Property

    Private _GroupType As String = "Date"
    Public Property GroupType() As String
        Get
            Return _GroupType
        End Get
        Set(ByVal value As String)
            _GroupType = value
            OnPropertyChanged("GroupType")
        End Set
    End Property


    Private _ort As Integer = 0

    Public Property Ort As Integer
        Get
            Return _ort
        End Get
        Set(ByVal value As Integer)
            _ort = value
        End Set
    End Property

    Private _myday As DateTime

    Public Property MyDay As DateTime
        Get
            Return _myday
        End Get
        Set(ByVal value As DateTime)
            _myday = value
            OnPropertyChanged("MyDay")
        End Set
    End Property

    Private _SelectedDate As DateTime = DateSerial(Now.Year, Now.Month, Now.Day)
    Public Property SelectedDate() As DateTime
        Get
            Return _SelectedDate
        End Get
        Set(ByVal value As DateTime)
            _SelectedDate = value
        End Set
    End Property



    Public _anzeige As String

    Public Property Anzeige As String
        Get
            Return _anzeige
        End Get
        Set(ByVal value As String)
            _anzeige = value
            OnPropertyChanged("Anzeige")
        End Set
    End Property

    Private _selectedview As Integer

    Public Property SelectedView As Integer
        Get
            Return _selectedview
        End Get
        Set(ByVal value As Integer)
            _selectedview = value
            OnPropertyChanged("SelectedView")
        End Set
    End Property

    Private _SelectedSlot As String

    Public Property SelectedSlot As String
        Get
            Return _SelectedSlot
        End Get
        Set(value As String)
            _SelectedSlot = value
        End Set
    End Property


    Private _isbusy As Boolean

    Public Property IsBusy As Boolean
        Get
            Return _isbusy
        End Get
        Set(ByVal value As Boolean)
            _isbusy = value
            OnPropertyChanged("IsBusy")
        End Set
    End Property

    Private _showAusfall As Boolean = False
    Public Property ShowAusfall() As Boolean
        Get
            Return _showAusfall
        End Get
        Set(ByVal value As Boolean)
            If _showAusfall <> value Then
                _showAusfall = value
                UpdateSchedule()
                OnPropertyChanged("ShowAusfall")
            End If
        End Set
    End Property





    Public Property AppDblClickCommand As ICommand

    Public Property PrintCommand As ICommand

    Public Property TodayCommand As ICommand
    Public Property AddCourseCommand As ICommand
    Public Property AddBookingCommand As ICommand
    Public Property NextCommand As ICommand
    Public Property PreviousCommand As ICommand


#End Region


#Region "   UpdateSchedule"

    ' Worker Thread für den Kalender


    Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw.DoWork

        MyDay = SelectedDate

        If IsNothing(_myday) Or IsNothing(Ansichtsmodus) Or IsNothing(Ort) Or Ort = 0 Or firstload Then
            Return
        End If
        'IsBusy = True
        'Try
        _pAppointments = New ObservableCollection(Of CalAppointment)
        _pResourceTypes = New ObservableCollection(Of CalResource)

        Dim _anzeige As String = String.Empty

        Dim Termine As List(Of DBAppointment)

        Weekstart = Monday(_myday)
        Weekend = DateAdd(DateInterval.Day, 7, Weekstart)
        DayDate = _myday


        'Model = New CalendarModel


        Dim scontext As New in1Entities




        If Ansichtsmodus = "W" Then 'Wochenansicht für einen Raum

            ActiveViewType = "WorkWeek"
            GroupType = "Date"
            SelectedView = 1


            _oldunit = 0

            Dim Ortsname = scontext.Locations.Where(Function(o) o.LocationID = Ort).Single.LocationName
            _anzeige = Ortsname & " " & Weekstart.ToShortDateString & " - " &
                       DateAdd(DateInterval.Day, 6, Weekstart).ToShortDateString

            Dim loca As New CalResource()
            loca.ResourceID = Ort
            loca.ResourceName = Ortsname
            _pResourceTypes.Add(loca)


            Dim query = From t In scontext.Appointments _
                .Include(Function(o) o.Booking) _
                .Include(Function(o) o.Booking.Category) _
                .Include(Function(o) o.Course) _
                .Include(Function(o) o.Location) _
                .Include(Function(o) o.Course.Sport) _
                .Include(Function(o) o.Course.Party) _
                .Include(Function(o) o.Course.CourseState)
                Where ((t.Start >= Weekstart And t.Start <= Weekend) Or (t.Ende >= Weekstart And t.Ende <= Weekend) Or
                       (t.Start <= Weekstart And t.Ende >= Weekend)) _
                      And t.LocationID = Ort _
                Select New DBAppointment With {
                        .Start = t.Start,
                        .Ende = t.Ende,
                        .AppointmentID = t.AppointmentID,
                        .LocationID = t.LocationID,
                        .BookingID = t.BookingID,
                        .CourseID = t.CourseID,
                        .Subject = t.Booking.Subject,
                        .Color = t.Booking.Color,
                        .Description = t.Booking.Description,
                        .CategoryName = t.Booking.Category.CategoryName,
                        .Sportname = t.Course.Sport.Sportname,
                        .Zusatzinfo = t.Course.Zusatzinfo,
                        .Partycolor = t.Course.Party.Partycolor,
                        .Canceled = t.Canceled,
                        .CourseStateID = t.Course.CourseStateID
                    }


            Dim result = query.ToList()
            Termine = result


        Else


            'Tagesansicht für mehrere Räume
            ActiveViewType = "Day"
            GroupType = "Resource"

            SelectedView = 0
            Dim calStart As DateTime
            Dim calEnd As DateTime
            calStart = DateSerial(_myday.Year, _myday.Month, _myday.Day)
            calEnd = DateAdd(DateInterval.Day, 1, calStart)
            DayDate = calStart

            Dim Ortsgruppe =
                    scontext.Locationgroups.Where(Function(o) o.LocationgroupId = Ort).Single.Locationgroupname
            _anzeige = Ortsgruppe & " " & calStart.ToShortDateString

            Dim Orte =
                    (From o In scontext.LocationLocationgroups Where o.LocationgroupID = Ort
                    Select o.Location.LocationName, o.LocationID Distinct).OrderBy(Function(o) o.LocationName)

            Dim LocationItems As New List(Of Integer)




            For Each Ort1 In Orte
                Dim ort As New CalResource() ' (Ort1.LocationName)
                ort.ResourceID = Ort1.LocationID
                ort.ResourceName = Ort1.LocationName
                LocationItems.Add(Ort1.LocationID)
                _pResourceTypes.Add(ort)
            Next

            _oldunit = Ort

            Dim query = From t In scontext.Appointments _
                .Include(Function(o) o.Booking) _
                .Include(Function(o) o.Booking.Category) _
                .Include(Function(o) o.Course) _
                .Include(Function(o) o.Location) _
                .Include(Function(o) o.Course.Sport) _
                .Include(Function(o) o.Course.Party) _
                Where _
                    ((t.Start <= calStart And t.Ende >= calEnd) Or (t.Start >= calStart And t.Start <= calEnd) Or
                     (t.Ende >= calStart And t.Ende <= calEnd)) _
                    And LocationItems.Contains(t.LocationID) _
                  Select New DBAppointment With {
                        .Start = t.Start,
                        .Ende = t.Ende,
                        .AppointmentID = t.AppointmentID,
                        .LocationID = t.LocationID,
                        .BookingID = t.BookingID,
                        .CourseID = t.CourseID,
                        .Subject = t.Booking.Subject,
                        .Color = t.Booking.Color,
                        .Description = t.Booking.Description,
                        .CategoryName = t.Booking.Category.CategoryName,
                        .Sportname = t.Course.Sport.Sportname,
                        .Zusatzinfo = t.Course.Zusatzinfo,
                        .Partycolor = t.Course.Party.Partycolor,
                        .Canceled = t.Canceled,
                        .CourseStateID = t.Course.CourseStateID
                    }

            Termine = query.ToList()

        End If


        For Each Termin In Termine

            'Dim app As New DevComponents.WpfSchedule.Model.Appointment
            Dim app As New CalAppointment
            'Dim res As New CalResource

            app.StartDate = Termin.Start
            app.EndDate = Termin.Ende
            app.UniqueID = Termin.AppointmentID
            app.BookingID = Termin.BookingID
            app.LocationID = Termin.LocationID
            app.BgColor = "#FFFFFF"


            ' Wenn keine Kursverknüpfung also Booking
            If IsNothing(Termin.CourseID) Then
                app.Subject = IIf(Not IsNothing(Termin.CategoryName), Termin.CategoryName, "ohne Kategorie")
                If Not IsNothing(Termin.Description) Then
                    Dim TextLines() As String = Termin.Description.Split(Environment.NewLine)
                    If Not IsNothing(TextLines(0)) Then _
                        app.Description = TextLines(0)
                Else
                    app.Description = String.Empty
                End If
                app.Description = Termin.Subject & " " & app.Description

                If IsNothing(Termin.Color) Then
                    Dim col = Brushes.AliceBlue
                    col.Freeze()
                    app.BgColor = "#E6E6E6"
                Else
                    Dim col = New SolidColorBrush(ColorConverter.ConvertFromString(Termin.Color.ToString))
                    col.Freeze()
                    app.BgColor = Termin.Color.ToString
                End If

                ' Wen der Termin gestrichen ist
                If Termin.Canceled = True Then
                    Dim col = CType(New BrushConverter().ConvertFrom("#cfcfcf"), SolidColorBrush)
                    col.Freeze()
                    app.BgColor = "#cfcfcf"
                    app.Description = "[Ausfall]" & app.Description
                End If

                ' Ende, wenn keine Kursverknüpfung


            Else ' Wenn Kurstermin
                app.CourseID = Termin.CourseID
                app.Subject = Termin.Sportname
                app.Description = Termin.Zusatzinfo

                ' Wen der Termin gestrichen ist
                If Termin.Canceled = True Then
                    Dim col = CType(New BrushConverter().ConvertFrom("#cfcfcf"), SolidColorBrush)
                    col.Freeze()
                    app.BgColor = "#cfcfcf"
                    app.Description = "[Ausfall]" & app.Description
                Else
                    app.BgColor = Termin.Partycolor
                End If

                ' Markierung geprüft, in Bearbeitung, ungeprüft
                'Dim tmname As String = Termin.CourseStatename
                'app.TimeMarker = TimeMarkers.Where(Function(o) o.TimeMarkerName = tmname).First
                app.Status = Termin.CourseStateID
            End If

            'Workaround, since Devexpress CustomFields do not work properly
            If Trim(app.BgColor.Length) = 0 Then app.BgColor = "#FFFFFF"
            app.Location = Termin.LocationName

            ' Zur Auflistung hinzufügen
            ' Ausfälle anzeigen
            If Termin.Canceled = False Or ShowAusfall = True Then
                _pAppointments.Add(app)
            End If


        Next

        'Feiertage holen
        Dim Feiertage =
                From c In scontext.Holidays Where c.HolidayDate >= Weekstart And c.HolidayDate <= Weekend Select c

        For Each itm In Feiertage
            Dim FT As New CalAppointment
            FT.AllDay = True
            FT.Subject = itm.HolidayName
            FT.StartDate = New Date(Year(itm.HolidayDate), Month(itm.HolidayDate), Day(itm.HolidayDate), 7, 0, 0)
            FT.EndDate = New Date(Year(itm.HolidayDate), Month(itm.HolidayDate), Day(itm.HolidayDate), 7, 59, 59)
            FT.LocationID = Ort
            FT.BgColor = "#cc33ff"
            _pAppointments.Add(FT)
        Next


        'Übergabe des Titels für den Kalender
        e.Result = _anzeige

        'UpdateLabels()

        scontext.Dispose()


        'Catch ex As Exception
        '    Throw New Exception("UpdateSchedule", ex)
        'End Try
    End Sub

    Private Sub bgw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles bgw.RunWorkerCompleted
        MyDay = SelectedDate
        Appointments = _pAppointments
        ResourceTypes = _pResourceTypes
        OnPropertyChanged("ResourceTypes")
        OnPropertyChanged("Appointments")
        'OnPropertyChanged("Labels")
        Anzeige = e.Result
        IsBusy = False
    End Sub


    Private Sub UpdateLabels()

        _Labels.Clear()
        If Not IsNothing(Appointments) Then
            For Each itm In Appointments
                Dim col = Convert.ToInt32(Right(System.Windows.Media.ColorConverter.ConvertFromString(itm.BgColor).ToString, 6), 16)
                Dim exist = (From c In _Labels Where Right(c.DisplayName, 6) = Right(itm.BgColor, 6) Select c.Id).FirstOrDefault
                If IsNothing(exist) Then
                    Dim lbl As New DevExpress.Xpf.Scheduler.AppointmentLabel
                    lbl.Color = System.Windows.Media.ColorConverter.ConvertFromString(itm.BgColor)
                    lbl.DisplayName = itm.BgColor
                    Labels.Add(lbl)
                    itm.Label = lbl.Id
                Else
                    itm.Label = CType(exist, Integer)
                End If
            Next
        End If

    End Sub



    Private Sub UpdateSchedule()


        Appointments.Clear()
        ResourceTypes.Clear()
        'OnPropertyChanged("Appointments")
        'OnPropertyChanged("ResourceTypes")



        If Not bgw.IsBusy Then
            IsBusy = True
            bgw.RunWorkerAsync()
        End If
    End Sub

#End Region



    Private Sub NextAction()
        If Ansichtsmodus = "W" Then
            MyDay = DateAdd(DateInterval.Day, 7, MyDay)
        Else
            MyDay = DateAdd(DateInterval.Day, 1, MyDay)

        End If
        SelectedDate = MyDay
        UpdateSchedule()
    End Sub

    Private Sub PreviousAction()
        If Ansichtsmodus = "W" Then
            MyDay = DateAdd(DateInterval.Day, -7, MyDay)
        Else
            MyDay = DateAdd(DateInterval.Day, -1, MyDay)

        End If
        SelectedDate = MyDay
        UpdateSchedule()
    End Sub


    Private Sub PrintAction()
        Events.EVAG.GetEvent(Of PrintScheduleEvent).Publish(Nothing)
    End Sub


#Region "   Constructor"


    Public Sub New()

        Events.EVAG.GetEvent(Of DateChangedEvent)().Subscribe(AddressOf UpdateDate)
        Events.EVAG.GetEvent(Of ViewChangedEvent)().Subscribe(AddressOf UpdateOrtundModus)
        Events.EVAG.GetEvent(Of RefreshCourseEvent)().Subscribe(AddressOf RefreshSchedule)
        If firstload Then


            Ansichtsmodus = "W"
            Ort = 0
            MyDay = Today
            DayDate = Today
            Weekstart = Monday(Today)
            Weekend = DateAdd(DateInterval.Day, 6, Weekstart)


            'Model = New CalendarModel

            AppDblClickCommand = New RelayCommand(AddressOf AppDblClick)

            'AddCourseCommand = New RelayCommand(AddressOf AddCourse, AddressOf CanAddCourse)
            PrintCommand = New RelayCommand(AddressOf PrintAction)

            TodayCommand = New RelayCommand(AddressOf TodayAction)
            AddCourseCommand = New RelayCommand(AddressOf AddCourseAction)
            AddBookingCommand = New RelayCommand(AddressOf AddBookingAction)
            NextCommand = New RelayCommand(AddressOf NextAction)
            PreviousCommand = New RelayCommand(AddressOf PreviousAction)


            firstload = False

            _context = New in1Entities


            Labels = New DevExpress.Xpf.Scheduler.AppointmentLabelCollection()
            AppointmentStates = New AppointmentStatusCollection()

            Dim query = From c In _context.CourseStates Select c


            For Each itm In query
                Dim tm As New AppointmentStatus
                tm.Brush = New BrushConverter().ConvertFrom(itm.CourseStatecolor)
                tm.Id = itm.CourseStateID
                tm.DisplayName = itm.CourseStatename
                AppointmentStates.Add(tm)
            Next




        End If
    End Sub

#End Region

#Region "   Commands"

    Public Sub UpdateOrtundModus(ByVal view As String)
        Dim modus As String
        modus = Left(view, 1)
        Ort = Mid(view, 2)
        Ansichtsmodus = modus
        UpdateSchedule()
    End Sub

    Public Sub UpdateDate(ByVal tag As DateTime)
        SelectedDate = DateSerial(tag.Year, tag.Month, tag.Day)
        UpdateSchedule()
    End Sub

    Public Sub RefreshSchedule()
        UpdateSchedule()
    End Sub

    Private Function Monday(ByVal datDay As Date) As Date
        Dim dayIndex As Integer = datDay.DayOfWeek
        If dayIndex < DayOfWeek.Monday Then
            dayIndex += 7 'Monday is first day of week, no day of week should have a smaller index
        End If
        Dim dayDiff As Integer = dayIndex - DayOfWeek.Monday
        Return datDay.AddDays(-dayDiff)
    End Function



    Private Sub AppDblClick(ByVal storage As Object)

        If SelectedAppointments.Count > 0 Then
            Dim devapp = SelectedAppointments.FirstOrDefault

            Dim app = CType(devapp.GetSourceObject(storage.GetCoreStorage()), CalAppointment)
            'Dim app = devapp.GetSourceObject()
            'Dim app As New CalAppointment
            If Not IsNothing(app) Then

                If Not IsNothing(app.CourseID) Then 'CourseView öffnen
                    Dim mycontext As New in1Entities
                    Dim scourse = mycontext.Courses.Find(CLng(app.CourseID))

                    Dim cs As New CourseService

                    If cs.ShowCourse(scourse) = True Then
                        UpdateSchedule()
                    End If
                    scourse = Nothing
                    mycontext.Dispose()

                ElseIf Not IsNothing(app.BookingID) Then
                    'sonst nur BookingUserControl öffnen
                    Dim mycontext As New in1Entities
                    Dim book = mycontext.Bookings.Find(app.BookingID)
                    mycontext.Dispose()
                    Dim bs As New BookingService
                    bs.ShowBooking(book)
                    RefreshSchedule()
                    bs.Dispose()
                End If
            End If
        End If
    End Sub




    Private Sub TodayAction()
        UpdateDate(Now)
    End Sub

    Private Sub AddCourseAction()
        Dim cs As New CourseService
        Dim c As New Course
        If Not IsNothing(SelectedInterval) Then
            c.DTStart = SelectedInterval.Start
            c.DTEnd = SelectedInterval.End

            If Ansichtsmodus = "W" Then
                c.LocationID = Ort
            Else
                Dim res = SelectedResource ' SelectedSlot.Resources.First
                Dim room = res.Id

                c.LocationID = room
            End If
        Else
            c.DTStart = New Date(Year(MyDay), Month(MyDay), Day(MyDay), Hour(Now), 0, 0)
            c.DTEnd = DateAdd(DateInterval.Hour, 1, c.DTStart)
        End If

        If cs.GetNewCourse(c) Then
            RefreshSchedule()
        End If
    End Sub

    Private Sub AddBookingAction()
        Dim b As New Booking

        b.UserID = MyGlobals.CurrentUserID
        If Not IsNothing(SelectedInterval) Then
            b.Start = SelectedInterval.Start
            b.Ende = SelectedInterval.End

            If Ansichtsmodus = "W" Then
                b.LocationID = Ort
            Else
                Dim res = SelectedResource
                Dim room = res.Id
                b.LocationID = room
            End If
        Else
            b.Start = New Date(Year(MyDay), Month(MyDay), Day(MyDay), Hour(Now), 0, 0)
            b.Ende = DateAdd(DateInterval.Minute, 60, b.Start)
            If Ansichtsmodus = "W" Then
                b.LocationID = Ort
            End If

        End If
        Dim bs As New BookingService
        bs.ShowBooking(b)
        bs.Dispose()
        RefreshSchedule()
    End Sub

#End Region

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
        _context.Dispose()
    End Sub
End Class

