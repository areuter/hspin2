﻿Imports System.Windows.Media

Public Class HSPAppointment
    Inherits Telerik.Windows.Controls.ScheduleView.Appointment

    Private _bgcolor As Brush
    Public Property Bgcolor As Brush
        Get
            Return _bgcolor
        End Get
        Set(ByVal value As Brush)
            _bgcolor = value
        End Set
    End Property


    Private _BookingID As Nullable(Of Integer)
    Public Property BookingID As Nullable(Of Integer)
        Get
            Return _BookingID
        End Get
        Set(value As Nullable(Of Integer))
            _BookingID = value
        End Set
    End Property

    Private _CourseID As Nullable(Of Integer)
    Public Property CourseID As Nullable(Of Integer)
        Get
            Return _CourseID
        End Get
        Set(value As Nullable(Of Integer))
            _CourseID = value
        End Set
    End Property

End Class
