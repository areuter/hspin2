﻿Imports System.ComponentModel.Composition
Imports System.ComponentModel
Imports System.Windows.Input
Imports System.Data.Entity
Imports System.Windows.Media
Imports HSPinOne.Infrastructure
Imports Telerik.Windows.Controls.ScheduleView
Imports Telerik.Windows.Controls
Imports HSPinOne.DAL
Imports Prism.Regions
Imports System.Collections.ObjectModel
Imports HSPinOne.Modules.Verwaltung
Imports HSPinOne.Modules.Verwaltung.ViewModel
Imports Prism.Events


Public Class KalenderViewModel1
    Inherits Infrastructure.ViewModelBase
    Implements IRegionMemberLifetime

    Private _oldunit As Integer = 0
    Private _ea As IEventAggregator

#Region "   Properties"


    Private viewChanged As Boolean = False
    Private firstload As Boolean = True
    Dim _context As in1Entities

    Private WithEvents bgw As New BackgroundWorker


    Public ReadOnly Property KeepAlive As Boolean Implements IRegionMemberLifetime.KeepAlive
        Get
            Return True
        End Get
    End Property

    Public Property Context As in1Entities
        Get
            Return _context
        End Get
        Set(ByVal value As in1Entities)
            _context = value
        End Set
    End Property


    Private _pAppointments As New ObservableCollection(Of HSPAppointment)

    Private _Appointments As ObservableCollection(Of HSPAppointment)

    Public Property Appointments As ObservableCollection(Of HSPAppointment)
        Get
            Return _Appointments
        End Get
        Set(ByVal value As ObservableCollection(Of HSPAppointment))
            _Appointments = value
        End Set
    End Property

    Private _resourceTypes As ObservableCollection(Of ResourceType)

    Public Property ResourceTypes As ObservableCollection(Of ResourceType)
        Get
            Return _resourceTypes
        End Get
        Set(ByVal value As ObservableCollection(Of ResourceType))
            _resourceTypes = value
        End Set
    End Property

    Private _TimeMarkers As ObservableCollection(Of TimeMarker)

    Public Property TimeMarkers As ObservableCollection(Of TimeMarker)
        Get
            Return _TimeMarkers
        End Get
        Set(value As ObservableCollection(Of TimeMarker))
            _TimeMarkers = value
        End Set
    End Property

    Private _SelectedAppointment As HSPAppointment
    Public Property SelectedAppointment As HSPAppointment
        Get
            Return _SelectedAppointment
        End Get
        Set(value As HSPAppointment)
            _SelectedAppointment = value
            RaisePropertyChanged("SelectedAppointment")
        End Set
    End Property


    Private _dayDate As DateTime

    Public Property DayDate As DateTime
        Get
            Return _dayDate
        End Get
        Set(ByVal value As DateTime)
            _dayDate = value
            RaisePropertyChanged("DayDate")
        End Set
    End Property

    Private _weekstart As DateTime

    Public Property Weekstart As DateTime
        Get
            Return _weekstart
        End Get
        Set(ByVal value As DateTime)
            _weekstart = value
            RaisePropertyChanged("Weekstart")
        End Set
    End Property

    Private _weekend As DateTime

    Public Property Weekend As DateTime
        Get
            Return _weekend
        End Get
        Set(ByVal value As DateTime)
            _weekend = value
            RaisePropertyChanged("Weekend")
        End Set
    End Property


    Private _ansichtsmodus As String

    Public Property Ansichtsmodus As String
        Get
            Return _ansichtsmodus
        End Get
        Set(ByVal value As String)
            _ansichtsmodus = value
        End Set
    End Property


    Private _ort As String

    Public Property Ort As String
        Get
            Return _ort
        End Get
        Set(ByVal value As String)
            _ort = value
        End Set
    End Property

    Private _myday As DateTime

    Public Property MyDay As DateTime
        Get
            Return _myday
        End Get
        Set(ByVal value As DateTime)
            _myday = value
            RaisePropertyChanged("MyDay")
        End Set
    End Property


    Public _anzeige As String

    Public Property Anzeige As String
        Get
            Return _anzeige
        End Get
        Set(ByVal value As String)
            _anzeige = value
            RaisePropertyChanged("Anzeige")
        End Set
    End Property

    Private _selectedview As Integer

    Public Property SelectedView As Integer
        Get
            Return _selectedview
        End Get
        Set(ByVal value As Integer)
            _selectedview = value
            RaisePropertyChanged("SelectedView")
        End Set
    End Property

    Private _SelectedSlot As Slot

    Public Property SelectedSlot As Slot
        Get
            Return _SelectedSlot
        End Get
        Set(value As Slot)
            _SelectedSlot = value
        End Set
    End Property


    Private _showAusfall As Boolean = False
    Public Property ShowAusfall() As Boolean
        Get
            Return _showAusfall
        End Get
        Set(ByVal value As Boolean)
            If _showAusfall <> value Then
                _showAusfall = value
                UpdateSchedule()
                RaisePropertyChanged("ShowAusfall")
            End If
        End Set
    End Property


    Public Property AppDblClickCommand As ICommand

    Public Property PrintCommand As ICommand

    Public Property TodayCommand As ICommand
    Public Property AddCourseCommand As ICommand
    Public Property AddBookingCommand As ICommand
    Public Property NextCommand As ICommand
    Public Property PreviousCommand As ICommand


#End Region


#Region "   UpdateSchedule"

    ' Worker Thread für den Kalender


    Private Sub bgw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs) Handles bgw.DoWork


        If IsNothing(MyDay) Or IsNothing(Ansichtsmodus) Or IsNothing(Ort) Or Ort = 0 Or firstload Then
            Return
        End If
        'IsBusy = True
        Try

            Dim calStart As Date
            Dim calEnd As Date
            Dim _anzeige As String = String.Empty

            Dim Termine As Object

            Weekstart = Monday(MyDay).Date
            Weekend = DateAdd(DateInterval.Day, 7, Weekstart).Date
            DayDate = MyDay.Date


            'Model = New CalendarModel
            _pAppointments = New ObservableCollection(Of HSPAppointment)


            Dim scontext As New in1Entities

            'Feiertage holen
            Dim Feiertage =
                    From c In scontext.Holidays Where c.HolidayDate >= Weekstart And c.HolidayDate <= Weekend Select c

            For Each itm In Feiertage
                Dim FT As New HSPAppointment
                FT.IsAllDayEvent = False
                FT.Subject = itm.HolidayName
                FT.Start = New Date(Year(itm.HolidayDate), Month(itm.HolidayDate), Day(itm.HolidayDate), 7, 0, 0)
                FT.End = New Date(Year(itm.HolidayDate), Month(itm.HolidayDate), Day(itm.HolidayDate), 7, 59, 59)
                FT.Bgcolor = Brushes.Black
                _pAppointments.Add(FT)
            Next


            If Ansichtsmodus = "W" Then 'Wochenansicht für einen Raum


                SelectedView = 1
                calStart = Weekstart
                calEnd = Weekend
                ResourceTypes = New ObservableCollection(Of ResourceType)
                _oldunit = 0

                Dim Ortsname = scontext.Locations.Where(Function(o) o.LocationID = Ort).Single.LocationName
                _anzeige = Ortsname & " " & calStart.ToShortDateString & " - " &
                           DateAdd(DateInterval.Day, 6, calStart).ToShortDateString


                Termine = (From t In scontext.Appointments _
                    .Include(Function(o) o.Booking) _
                    .Include(Function(o) o.Booking.Category) _
                    .Include(Function(o) o.Location) _
                    .Include(Function(o) o.Course.Sport) _
                    .Include(Function(o) o.Course.Party) _
                    .Include(Function(o) o.Course.CourseState)
                           Where ((t.Start >= calStart And t.Start <= calEnd) Or (t.Ende >= calStart And t.Ende <= calEnd) Or
                                  (t.Start <= calStart And t.Ende >= calEnd)) _
                                 And t.LocationID = Ort
                           Select
                               t.Start, t.Ende, t.AppointmentID, t.BookingID, t.Location.LocationName, t.CourseID,
                                   t.Booking.Subject, t.Booking.Color, t.Booking.Description, t.Booking.Category.CategoryName,
                                   t.Course.Sport.Sportname, t.Course.Zusatzinfo, t.Canceled, t.Course.Party.Partycolor,
                                   t.Course.CourseState.CourseStatename).ToList


            Else


                'Tagesansicht für mehrere Räume
                SelectedView = 0
                calStart = DateSerial(MyDay.Year, MyDay.Month, MyDay.Day)
                calEnd = DateAndTime.DateAdd(DateInterval.Day, 1, calStart)
                DayDate = calStart

                Dim Ortsgruppe =
                        scontext.Locationgroups.Where(Function(o) o.LocationgroupId = Ort).Single.Locationgroupname
                _anzeige = Ortsgruppe & " " & calStart.ToShortDateString

                Dim Orte =
                        (From o In scontext.LocationLocationgroups Where o.LocationgroupID = Ort
                         Select o.Location.LocationName, o.LocationID Distinct).OrderBy(Function(o) o.LocationName)

                Dim LocationItems As New List(Of Integer)

                If _oldunit <> Ort Then ResourceTypes = New ObservableCollection(Of ResourceType)


                Dim roomType As New ResourceType("Room")
                For Each Ort1 In Orte
                    'Dim owner1 As New Owner(Ort1.LocationID.ToString, Ort1.LocationName.ToString)
                    'Model.Owners.Add(owner1)
                    'DisplayedOwners.Add(owner1.DisplayName)
                    Dim ort As New Resource(Ort1.LocationName)
                    roomType.Resources.Add(ort)
                    LocationItems.Add(Ort1.LocationID)
                Next
                If _oldunit <> Ort Then

                    ResourceTypes.Add(roomType)

                End If
                _oldunit = Ort

                Termine = (From t In scontext.Appointments _
                    .Include(Function(o) o.Booking) _
                    .Include(Function(o) o.Booking.Category) _
                    .Include(Function(o) o.Course) _
                    .Include(Function(o) o.Location) _
                    .Include(Function(o) o.Course.Sport) _
                    .Include(Function(o) o.Course.Party) _
                    .Include(Function(o) o.Course.CourseState)
                           Where
                               ((t.Start <= calStart And t.Ende >= calEnd) Or (t.Start >= calStart And t.Start <= calEnd) Or
                                (t.Ende >= calStart And t.Ende <= calEnd)) _
                               And LocationItems.Contains(t.LocationID)
                           Select
                               t.Start, t.Ende, t.AppointmentID, t.BookingID, t.Location.LocationName, t.CourseID,
                                   t.Booking.Subject, t.Booking.Color, t.Booking.Description, t.Booking.Category.CategoryName,
                                   t.Course.Sport.Sportname, t.Course.Zusatzinfo, t.Canceled, t.Course.Party.Partycolor,
                                   t.Course.CourseState.CourseStatename).ToList


            End If


            For Each Termin In Termine

                'Dim app As New DevComponents.WpfSchedule.Model.Appointment
                Dim app As New HSPAppointment
                Dim res As New Resource

                app.Start = Termin.Start
                app.End = Termin.Ende
                app.UniqueId = Termin.AppointmentID
                app.BookingID = Termin.BookingID
                app.Location = Termin.LocationName


                res.ResourceName = Termin.LocationName
                res.ResourceType = "Room"


                app.Resources.Add(res)

                ' Wenn keine Kursverknüpfung also Booking
                If IsNothing(Termin.CourseID) Then
                    app.Subject = IIf(Not IsNothing(Termin.CategoryName), Termin.CategoryName, "ohne Kategorie")
                    If Not IsNothing(Termin.Description) Then
                        Dim TextLines() As String = Termin.Description.Split(Environment.NewLine)
                        If Not IsNothing(TextLines(0)) Then _
                            app.Body = TextLines(0)
                    Else
                        app.Body = String.Empty
                    End If
                    app.Body = Termin.Subject & " " & app.Body

                    If IsNothing(Termin.Color) Then
                        Dim col = Brushes.AliceBlue
                        col.Freeze()
                        app.Bgcolor = col
                    Else
                        Dim col = New SolidColorBrush(ColorConverter.ConvertFromString(Termin.Color.ToString))
                        col.Freeze()
                        app.Bgcolor = col
                    End If

                    ' Wen der Termin gestrichen ist
                    If Termin.Canceled = True Then
                        Dim col = CType(New BrushConverter().ConvertFrom("#cfcfcf"), SolidColorBrush)
                        col.Freeze()
                        app.Bgcolor = col
                        app.Body = "[Ausfall]" & app.Body
                    End If

                    ' Ende, wenn keine Kursverknüpfung


                Else ' Wenn Kurstermin
                    app.CourseID = Termin.CourseID
                    app.Subject = Termin.Sportname
                    app.Body = Termin.Zusatzinfo

                    ' Wen der Termin gestrichen ist
                    If Termin.Canceled = True Then
                        Dim col = CType(New BrushConverter().ConvertFrom("#cfcfcf"), SolidColorBrush)
                        col.Freeze()
                        app.Bgcolor = col
                        app.Body = "[Ausfall]" & app.Body
                    Else
                        Dim col = CType(New BrushConverter().ConvertFrom(Termin.Partycolor), SolidColorBrush)
                        col.Freeze()
                        app.Bgcolor = col
                    End If

                    ' Markierung geprüft, in Bearbeitung, ungeprüft
                    Dim tmname As String = Termin.CourseStatename
                    app.TimeMarker = TimeMarkers.Where(Function(o) o.TimeMarkerName = tmname).First
                End If


                'app.CategoryColor = Termin.Partycolor
                'app.TimeMarkedAs = Termin.CourseStatecolor
                'app.OwnerKey = Termin.Locationname
                'app.Locked = True
                If Termin.Canceled = False Or ShowAusfall = True Then
                    _pAppointments.Add(app)
                End If
                '_pAppointments.Add(app)
            Next


            'Übergabe des Titels für den Kalender
            e.Result = _anzeige

            scontext.Dispose()


        Catch ex As Exception
            Throw New Exception
        End Try
    End Sub

    Private Sub bgw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) _
        Handles bgw.RunWorkerCompleted
        IsBusy = False
        Me.Appointments = _pAppointments

        RaisePropertyChanged("ResourceTypes")
        RaisePropertyChanged("Appointments")
        Anzeige = e.Result
        SelectedAppointment = Nothing

    End Sub


    Private Sub UpdateSchedule()


        If Not bgw.IsBusy Then
            'IsBusy = True
            bgw.RunWorkerAsync()
        End If
    End Sub

#End Region


    Private Sub NextAction()
        If Ansichtsmodus = "W" Then
            MyDay = DateAdd(DateInterval.Day, 7, MyDay)
        Else
            MyDay = DateAdd(DateInterval.Day, 1, MyDay)

        End If
        UpdateSchedule()
    End Sub

    Private Sub PreviousAction()
        If Ansichtsmodus = "W" Then
            MyDay = DateAdd(DateInterval.Day, -7, MyDay)
        Else
            MyDay = DateAdd(DateInterval.Day, -1, MyDay)

        End If
        UpdateSchedule()
    End Sub


    Private Sub PrintAction()
        Events.EventInstance.EventAggregator.GetEvent(Of PrintScheduleEvent).Publish(Nothing)
    End Sub


#Region "   Constructor"


    Public Sub New()
        _ea = Events.EventInstance.EventAggregator
        _ea.GetEvent(Of DateChangedEvent)().Subscribe(AddressOf UpdateDate)
        _ea.GetEvent(Of ViewChangedEvent)().Subscribe(AddressOf UpdateOrtundModus)
        _ea.GetEvent(Of RefreshCourseEvent)().Subscribe(AddressOf RefreshSchedule)
        If firstload Then


            Ansichtsmodus = "W"
            Ort = 0
            MyDay = Today
            DayDate = Today
            Weekstart = Monday(Today)
            Weekend = DateAdd(DateInterval.Day, 6, Weekstart)


            'Model = New CalendarModel

            Appointments = New ObservableCollection(Of HSPAppointment)

            Me.TimeMarkers = New ObservableCollection(Of TimeMarker)


            AppDblClickCommand = New RelayCommand(AddressOf AppDblClick)
            'TodayCommand = New RelayCommand(AddressOf TodayExecute)
            'AddCourseCommand = New RelayCommand(AddressOf AddCourse, AddressOf CanAddCourse)
            PrintCommand = New RelayCommand(AddressOf PrintAction)

            TodayCommand = New RelayCommand(AddressOf TodayAction)
            AddCourseCommand = New RelayCommand(AddressOf AddCourseAction)
            AddBookingCommand = New RelayCommand(AddressOf AddBookingAction)
            NextCommand = New RelayCommand(AddressOf NextAction)
            PreviousCommand = New RelayCommand(AddressOf PreviousAction)


            firstload = False

            _context = New in1Entities

            Dim query = From c In _context.CourseStates Select c

            For Each itm In query
                Dim tm As New TimeMarker
                tm.TimeMarkerName = itm.CourseStatename
                tm.TimeMarkerBrush = New BrushConverter().ConvertFrom(itm.CourseStatecolor)
                TimeMarkers.Add(tm)
            Next

        End If
    End Sub

#End Region

#Region "   Commands"

    Public Sub UpdateOrtundModus(ByVal view As String)
        Dim modus As String
        modus = Left(view, 1)
        Ort = Mid(view, 2)
        Ansichtsmodus = modus
        UpdateSchedule()
    End Sub

    Public Sub UpdateDate(ByVal tag As DateTime)
        MyDay = tag
        UpdateSchedule()
    End Sub

    Public Sub RefreshSchedule()
        UpdateSchedule()
    End Sub

    Private Function Monday(ByVal datDay As Date) As Date
        Return _
            DateAdd(DateInterval.Day, -Weekday(datDay, FirstDayOfWeek.Monday) + 1,
                    DateSerial(Year(datDay), Month(datDay), Day(datDay)))
    End Function


    Private Sub TodayExecute()
        MyDay = Today
        UpdateSchedule()
    End Sub


    Private Sub AppDblClick(ByVal obj As Object)

        Dim app = SelectedAppointment
        If Not IsNothing(app) Then

            If Not IsNothing(app.CourseID) Then 'CourseView öffnen
                Dim mycontext As New in1Entities
                Dim scourse = mycontext.Courses.Find(CLng(app.CourseID))

                Dim cs As New CourseService

                If cs.ShowCourse(scourse) = True Then
                    UpdateSchedule()
                End If
                scourse = Nothing
                mycontext.Dispose()

            ElseIf Not IsNothing(app.BookingID) Then
                'sonst nur BookingUserControl öffnen
                Dim mycontext As New in1Entities
                Dim book = mycontext.Bookings.Find(app.BookingID)
                mycontext.Dispose()
                Dim bs As New BookingService
                bs.ShowBooking(book)
                RefreshSchedule()
                bs.Dispose()
            End If
        End If
    End Sub




    Private Sub TodayAction()
        UpdateDate(Now)
    End Sub

    Private Sub AddCourseAction()
        Dim cs As New CourseService
        Dim c As New Course
        If Not IsNothing(SelectedSlot) Then
            c.DTStart = SelectedSlot.Start
            c.DTEnd = SelectedSlot.End

            If Ansichtsmodus = "W" Then
                c.LocationID = Ort
            Else
                Dim res = SelectedSlot.Resources.First
                Dim room = CType(res, Resource).ResourceName
                Dim locid = _context.Locations.Where(Function(o) o.LocationName = room).FirstOrDefault.LocationID
                c.LocationID = locid
            End If
        End If

        If cs.GetNewCourse(c) Then
            RefreshSchedule()
        End If
    End Sub

    Private Sub AddBookingAction()
        Dim b As New Booking

        b.UserID = MyGlobals.CurrentUserID
        If Not IsNothing(SelectedSlot) Then
            b.Start = SelectedSlot.Start
            b.Ende = SelectedSlot.End

            If Ansichtsmodus = "W" Then
                b.LocationID = Ort
            Else
                Dim res = SelectedSlot.Resources.First
                Dim room = CType(res, Resource).ResourceName
                Dim loc = _context.Locations.Where(Function(o) o.LocationName = room).FirstOrDefault.LocationID
                b.LocationID = loc
            End If
        Else
            b.Start = New Date(Year(MyDay), Month(MyDay), Day(MyDay), Hour(Now), 0, 0)
            b.Ende = DateAdd(DateInterval.Minute, 60, b.Start)
            If Ansichtsmodus = "W" Then
                b.LocationID = Ort
            End If

        End If
        Dim bs As New BookingService
        bs.ShowBooking(b)
        bs.Dispose()
        RefreshSchedule()
    End Sub

#End Region

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
        _context.Dispose()
    End Sub
End Class

