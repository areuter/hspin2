﻿Imports Prism.Regions
Imports System.Windows.Input
Imports System
Imports System.Collections.ObjectModel
Imports HSPinOne.Common
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.ComponentModel.Composition
Imports Prism.Events


Public Class KalenderOutlookSectionViewModel

    Private _treeViewMenu As ObservableCollection(Of OutlookSectionMenuTreeTopItemViewModel)
    Public Property TreeViewMenu As ObservableCollection(Of OutlookSectionMenuTreeTopItemViewModel)
        Get
            Return _treeViewMenu
        End Get
        Set(ByVal value As ObservableCollection(Of OutlookSectionMenuTreeTopItemViewModel))
            _treeViewMenu = value
        End Set
    End Property

    Private _selectedDate As DateTime
    Public Property SelectedDate As DateTime
        Get
            Return _selectedDate
        End Get
        Set(ByVal value As DateTime)
            _selectedDate = value
            DayChanged()
        End Set
    End Property

    Private _selectedMode As String
    Public Property SelectedMode As String
        Get
            Return _selectedMode

        End Get
        Set(ByVal value As String)
            _selectedMode = value
        End Set
    End Property

    Private _selectedlocation As Long
    Public Property SelectedLocation As Long
        Get
            Return _selectedlocation
        End Get
        Set(ByVal value As Long)
            _selectedlocation = value
            Locationchanged()
        End Set
    End Property

    Private _SelectedItem As Object
    Public Property SelectedItem As Object
        Get
            Return _SelectedItem
        End Get
        Set(value As Object)
            _SelectedItem = value
            If value.GetType Is GetType(OutlookSectionMenuTreeItemViewModel) Then
                SelectedMode = value.Tag1
                SelectedLocation = value.Tag2
                Locationchanged()
            End If
        End Set
    End Property


    Public Property ClickCommand As ICommand

    Private _regionmanager As IRegionManager
    Private _ea As IEventAggregator



    Public Sub New()

        _regionmanager = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IRegionManager)
        _ea = Events.EventInstance.EventAggregator

        SelectedDate = Now

        _ea.GetEvent(Of OutlookSectionChangedEvent)().Subscribe(AddressOf DefaultNavigation)

        Me.TreeViewMenu = New ObservableCollection(Of OutlookSectionMenuTreeTopItemViewModel)


        Dim _context As New in1Entities



        Dim topMenuitem As New OutlookSectionMenuTreeTopItemViewModel
        topMenuitem.MenuItemName = "Ressourcenübersicht"
        topMenuitem.IsExpanded = True
        Me.TreeViewMenu.Add(topMenuitem)


        Dim Presets = From p In _context.Locationgroups Order By p.Locationgroupname Select p

        For Each Preset In Presets
            Dim menuItem As New OutlookSectionMenuTreeItemViewModel
            menuItem.MenuItemName = Preset.Locationgroupname
            Dim lid As Long = Preset.LocationgroupId
            menuItem.Tag1 = "R"
            menuItem.Tag2 = lid

            topMenuitem.Children.Add(menuItem)
        Next


        topMenuitem = New OutlookSectionMenuTreeTopItemViewModel
        topMenuitem.MenuItemName = "Raumübersicht"
        topMenuitem.IsExpanded = True
        Me.TreeViewMenu.Add(topMenuitem)
        Dim Räume = From r In _context.Locations Where r.ClientVisible = True Order By r.LocationName Select r
        For Each Raum In Räume
            Dim menuItem As New OutlookSectionMenuTreeItemViewModel
            menuItem.MenuItemName = Raum.LocationName
            Dim lid As Long = Raum.LocationID
            menuItem.Tag1 = "W"
            menuItem.Tag2 = lid

            topMenuitem.Children.Add(menuItem)
        Next

        'CheckRegion()
        'SelectedItem = topMenuitem.Children.FirstOrDefault

    End Sub

    Private Sub DayChanged()
        Dim tag As DateTime
        If IsNothing(SelectedDate) Then
            SelectedDate = Now
        End If

        tag = SelectedDate
        'CheckRegion()
        Events.EventInstance.EventAggregator.GetEvent(Of DateChangedEvent)().Publish(tag)


    End Sub

    Private Sub Locationchanged()
        Dim view As String
        view = SelectedMode & SelectedLocation
        'CheckRegion()
        Events.EventInstance.EventAggregator.GetEvent(Of ViewChangedEvent)().Publish(view)
    End Sub

    Private Sub DefaultNavigation(ByVal param As Object)
        If param.Name = "Kalender" Then

            'Dim view = ServiceLocator.Current.GetInstance(Of KalenderView1)()

            Dim wc As New WaitCursor
            Dim uri As New Uri("KalenderView1", UriKind.Relative)
            _regionmanager.RequestNavigate("MainRegion", uri)

            'Dim ruri As New Uri("KalenderRibbonTab", UriKind.Relative)
            '_regionmanager.RequestNavigate("RibbonRegion", ruri)
            wc.Dispose()
        End If
    End Sub


End Class
