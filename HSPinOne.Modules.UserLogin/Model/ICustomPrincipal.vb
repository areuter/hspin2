﻿Imports System.Collections.Generic
Imports System.Security.Principal

Public Interface ICustomPrincipal
    ReadOnly Property Identity As IIdentity
    Property Roles As List(Of String)
    Function IsInRole(role As String) As Boolean
End Interface
