﻿Imports System.Collections.Generic
Imports System.Security.Principal

Public Class CustomPrincipal
    Implements IPrincipal
    Implements ICustomPrincipal

    Private _roles As New List(Of String)()
    Private _identity As CustomIdentity

    Public Property Roles() As List(Of String) Implements ICustomPrincipal.Roles
        Get
            Return Me._roles
        End Get
        Set(ByVal value As List(Of String))
            _roles = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal identity As CustomIdentity, ByVal roles As List(Of String))
        _identity = identity
        _roles = roles
    End Sub

    Public ReadOnly Property Identity() As IIdentity Implements IPrincipal.Identity, ICustomPrincipal.Identity
        Get
            Return _identity
        End Get
    End Property

    Public Function IsInRole(ByVal role As String) As Boolean Implements IPrincipal.IsInRole, ICustomPrincipal.IsInRole
        Return Roles.Contains(role)
    End Function
End Class
