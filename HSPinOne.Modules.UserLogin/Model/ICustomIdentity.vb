﻿Public Interface ICustomIdentity
    ReadOnly Property AuthenticationType As String
    ReadOnly Property InstitutionID As Integer
    ReadOnly Property IsAuthenticated As Boolean
    ReadOnly Property LoginID As String
    ReadOnly Property Name As String
    ReadOnly Property UserId As Integer
End Interface
