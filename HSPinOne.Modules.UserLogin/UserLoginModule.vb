﻿

Imports Prism.Ioc
Imports Prism.Modularity
Imports Prism.Regions
Imports HSPinOne.Infrastructure

Public Class UserLoginModule
    Implements IModule

    Public Sub RegisterTypes(containerRegistry As IContainerRegistry) Implements IModule.RegisterTypes
        'Throw New NotImplementedException()
        containerRegistry.RegisterInstance(Of IUserDataService)(New UserDataService())

        'Dim ci As New CustomIdentity(0, 0, "", "", "")
        'Dim cp = New CustomPrincipal(ci, New Collections.Generic.List(Of String)())
        'containerRegistry.RegisterSingleton(Of ICustomPrincipal, CustomPrincipal)()
    End Sub

    Public Sub OnInitialized(containerProvider As IContainerProvider) Implements IModule.OnInitialized
        'Throw New NotImplementedException()
        Dim rm = containerProvider.Resolve(Of IRegionManager)()
        rm.RegisterViewWithRegion(RegionNames.MainRegion, GetType(UserLoginView))
    End Sub
End Class
