﻿Imports HSPinOne.Infrastructure
Imports System.ComponentModel.Composition


Public Class UserLoginService
    Implements IUserLoginService

    Private win As WindowController
    Private _loginsuccess As Boolean
    Private vm As UserLoginViewModel
    Private user As String

    Public Function ShowLoginWindow() As String Implements Infrastructure.IUserLoginService.ShowLoginWindow

        'win = New WindowController

        'vm = New UserLoginViewModel 'ServiceLocator.Current.GetInstance(Of UserLoginViewModel)()
        'Dim view = New UserLoginView 'ServiceLocator.Current.GetInstance(Of UserLoginView)()
        'view.ViewModel = vm

        'AddHandler vm.RequestClose, AddressOf CloseWindow
        'AddHandler win.MainWindowClosed, AddressOf CloseWindow
        'win.ShowWindow(view, False, System.Windows.ResizeMode.NoResize, 400, 248, "Login")
        'Return user
        Return True
    End Function

    Private Sub CloseWindow()
        If vm.LoginSuccess Then
            win.CloseWindow()
            user = vm.LoginID
            win = Nothing
            vm = Nothing
        Else
            System.Windows.Application.Current.Shutdown()
        End If
    End Sub


End Class
