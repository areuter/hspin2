﻿Imports System.Linq
Imports System.Security.Principal
Imports System.Text.RegularExpressions
Imports System.Data.Entity
Imports HSPinOne.Infrastructure
Imports HSPinOne.DAL
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.ComponentModel.Composition


Public Class UserDataService
    Implements IUserDataService

    Private Shared principleType As String = "Custom"

    Private _context As in1Entities

    Public Function GetUserByLoginID(ByVal LoginID As String) As User Implements IUserDataService.GetUserByLoginID
        Try

            Using con As New in1Entities
                Dim user = (From u In con.Users Where u.LoginID = LoginID Or u.EmailAddress.ToLower = LoginID.ToLower).FirstOrDefault
                Return user
            End Using
            Return Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message, ex.InnerException)
        End Try

    End Function

    Public Function GetUserByID(ByVal UserID As Integer) As User Implements IUserDataService.GetUserByID
        Try
            Using con As New in1Entities
                Dim user = (From u In con.Users.Include("Role") Where u.UserID = UserID).FirstOrDefault
                Return user
            End Using
            Return Nothing

        Catch ex As Exception
            Throw New Exception(ex.Message, ex.InnerException)
        End Try
    End Function
    ''' <summary>
    ''' Login
    ''' </summary>
    ''' <param name="LoginID"></param>
    ''' <param name="LoginPassword"></param>
    ''' <param name="logintype">0=Local, 1=LDAP</param>
    ''' <returns></returns>
    Public Function Login(ByVal LoginID As String,
                          ByVal LoginPassword As String, Optional ByVal logintype As Integer = 0) As PrincipleBusinessObject Implements IUserDataService.Login
        Dim errMsg As String = String.Empty
        Dim user As User = Nothing

        Dim sLoginID As String = LoginID.Trim()
        Dim sLoginPassword As String = LoginPassword.Trim()
        Dim principal As IPrincipal

        Try
            Using con As New in1Entities


                user = GetUserByLoginID(LoginID)
                If user Is Nothing Then
                    errMsg += "Benutzername oder Passwort sind falsch. Bitte versuchen Sie es erneut oder wenden Sie sich an den Administrator."
                    Throw New Exception(errMsg)
                End If

                If user.IsActive = False Then
                    errMsg += "Dieser Benutzer ist deaktiviert. Bitte wenden Sie sich an den Administrator"
                    Throw New Exception(errMsg)
                End If

                'Authenticate
                Dim authenticated As Boolean = False
                If logintype = 1 Then 'LDAP
                    Dim sett = GlobalSettingService.HoleEinstellungen(New List(Of String)({"ldap.host", "ldap.port", "ldap.user", "ldap.pass", "ldap.basedn", "ldap.searchfilter"}))
                    Try
                        Dim ldapprovider = New LDAPProvider(sett.Item("ldap.user"), sett.Item("ldap.pass"), sett.Item("ldap.host"), sett.Item("ldap.port"))
                        If Not ldapprovider.Message = "" Then
                            errMsg = "LDAP Server nicht erreichbar bzw. kein Login möglich"
                        Else
                            ' User DN bestimmen
                            Dim filter As String = String.Format(sett.Item("ldap.searchfilter"), user.EmailAddress)
                            Dim searchresult = ldapprovider.Search(sett.Item("ldap.basedn"), filter)

                            ' Wenn ein Benutzer mit der Email gefunden wird
                            If searchresult.Count = 1 Then
                                Dim first = searchresult(0)
                                Dim userdn = first.FirstOrDefault(Function(o) o.Key = "DN").Value
                                authenticated = ldapprovider.ValidateUserByBind(userdn, sLoginPassword)
                            Else
                                errMsg = "Benutzer nicht im LDAP gefunden"
                            End If

                        End If
                        ldapprovider.Dispose()
                    Catch ex As Exception
                        errMsg = "Unbekanner LDAP Fehler " & ex.Message.ToString()
                    End Try
                Else
                    authenticated = Crypto.Verify(sLoginPassword, user.Password, "SHA1", user.SaltValue)
                End If


                If authenticated Then
                    Dim userID As Integer = user.UserID

                    Dim roleList As New List(Of String)
                    Dim sRoleList() As String = Nothing


                    con.Users.Attach(user)
                    Dim query = From r In con.Roles Select r

                    'For Each item In query.ToList
                    '    Console.WriteLine(item.RoleName)
                    'Next
                    'Dim roles = (From r In MyDataContext.DBEntities.Roles _
                    '            Join ru In MyDataContext.DBEntities.Users _
                    '            On r.RoleID Equals ru.Role.RoleID _
                    '            Where ru.User.UserID = userID _
                    '            Select r)
                    If user.Role Is Nothing Then
                        errMsg += "Diesem Benutzer wurden noch keine Rollen zugewiesen. Bitte wenden sie sich an den Administrator."
                        Throw New Exception(errMsg)
                    Else
                        roleList.Add(user.Role.RoleName)
                        ' Dim lastID As Integer = user.Role.ParentID
                        Dim Rolelist2 As List(Of Role) = query.ToList
                        Dim tempRole As Role = user.Role

                        Do While TypeOf tempRole Is Role And Not IsNothing(tempRole.ParentID)
                            ' If Not IsNothing(tempRole.ParentID) Then

                            tempRole = GetParent(tempRole.ParentID, Rolelist2)
                            ' Console.WriteLine(tempRole.RoleName)
                            roleList.Add(tempRole.RoleName)
                            'Else
                            'tempRole = Nothing
                            'End If

                        Loop


                        'For Each item In query.ToList
                        '    If lastID = item.RoleID Then
                        '        Console.WriteLine(item.RoleName)
                        '    End If
                        'Next
                    End If

                    'Dim i As Integer = 0
                    'For Each r In roles
                    '    roleList.Add(r.RoleName)
                    '    i += 1
                    'Next
                    Dim roleStringArray() As String = roleList.ToArray

                    ' create the Principal and Identity objects
                    If principleType = "Generic" Then
                        Dim identity As New GenericIdentity(user.UserName, "Custom")
                        principal = New GenericPrincipal(identity, roleStringArray)

                    Else
                        Dim identity As New IdentityBusinessObject(user.UserID, user.Institution.InstitutionID, user.UserName, user.LoginID, "Custom")
                        principal = New PrincipleBusinessObject(identity, roleList)
                    End If
                Else
                    ' the credentials were not valid
                    ' so create an unauthenticated Principal/Identity
                    If principleType = "Generic" Then
                        Dim identity As New GenericIdentity("", "")
                        principal = New GenericPrincipal(identity, New String() {})

                    Else
                        Dim identity As New IdentityBusinessObject(0, 0, "", "", "")
                        principal = New PrincipleBusinessObject(identity, Nothing)
                    End If
                    errMsg += "Benutzername oder Passwort sind falsch. Bitte versuchen Sie es erneut oder wenden Sie sich an den Administrator."
                    Throw New Exception(errMsg)
                End If
                Return principal
            End Using
        Catch ex As Exception
            Debug.Print("Exception")
            Throw New Exception(ex.Message, ex.InnerException)
        End Try
    End Function


    Private Function GetParent(ByVal parentID As Integer, ByVal stack As List(Of Role))
        Dim i As Integer
        If Not IsNothing(parentID) Then
            For Each item In stack
                If parentID = item.RoleID Then
                    Return item
                ElseIf i >= stack.Count() - 1 Then
                    Return False
                End If
                i += 1
            Next
            Return False
        Else
            Return False

        End If
    End Function

End Class