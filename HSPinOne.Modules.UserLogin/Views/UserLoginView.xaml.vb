﻿Imports System.ComponentModel.Composition
Imports HSPinOne.Infrastructure
Imports System.Windows.Input
Imports Prism.Regions

Public Class UserLoginView
    Implements INavigationAware


    Public Sub New()

        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()

        ' Fügen Sie Initialisierungen nach dem InitializeComponent()-Aufruf hinzu.
        Me.DataContext = New UserLoginViewModel()

    End Sub

    Public Function IsNavigationTarget(navigationContext As Prism.Regions.NavigationContext) As Boolean Implements Prism.Regions.INavigationAware.IsNavigationTarget
        Keyboard.Focus(Me.LoginIDTextBox)
        Return True

    End Function

    Public Sub OnNavigatedFrom(navigationContext As Prism.Regions.NavigationContext) Implements Prism.Regions.INavigationAware.OnNavigatedFrom

    End Sub

    Public Sub OnNavigatedTo(navigationContext As Prism.Regions.NavigationContext) Implements Prism.Regions.INavigationAware.OnNavigatedTo

    End Sub

    Private Sub UserLoginView_Loaded(sender As Object, e As System.Windows.RoutedEventArgs) Handles Me.Loaded
        Keyboard.Focus(Me.LoginIDTextBox)
    End Sub


    Private Sub PasswordTextBox_PreviewKeyDown(sender As Object, e As KeyEventArgs) Handles PasswordTextBox.PreviewKeyDown
        UpdateCapsLockWarning(e.KeyboardDevice)
    End Sub

    Private Sub PasswordTextBox_GotKeyboardFocus(sender As Object, e As KeyboardFocusChangedEventArgs) Handles PasswordTextBox.GotKeyboardFocus
        UpdateCapsLockWarning(e.KeyboardDevice)
    End Sub

    Private Sub PasswordTextBox_LostKeyboardFocus(sender As Object, e As KeyboardFocusChangedEventArgs) Handles PasswordTextBox.LostKeyboardFocus
        CapsLockWarning.Visibility = System.Windows.Visibility.Hidden
    End Sub

    Private Sub UpdateCapsLockWarning(keyboard As KeyboardDevice)

        CapsLockWarning.Visibility = IIf(keyboard.IsKeyToggled(Key.CapsLock), System.Windows.Visibility.Visible, System.Windows.Visibility.Hidden)
    End Sub
End Class
