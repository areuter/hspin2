﻿Imports System.ComponentModel.Composition
Imports System.Windows
Imports System.Collections.Generic
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Input
Imports Prism.Regions
Imports Prism.Events
Imports System.Security.Principal
Imports System.Threading
Imports CommonServiceLocator
Imports System.ComponentModel
Imports System.Security.Claims
Imports System.Threading.Tasks

Public Class UserLoginViewModel
    Inherits ViewModelBase

#Region "  Declarations"

    Private _user As User
    Private _loginID As String = String.Empty
    Private _password As String = String.Empty
    Private _loginMessage As String = String.Empty
    Private _loginSuccess As Boolean = False


    Private _ea As IEventAggregator

    Private _userAccess As IUserDataService

    Private _regionmanager As IRegionManager

    Private inihandler As Inihandler

    Public Event RequestClose As EventHandler

#End Region

#Region "  Properties"

    Public Property User As User
        Get
            Return _user
        End Get
        Set(ByVal value As User)
            _user = value
            RaisePropertyChanged("User")
        End Set
    End Property

    Public Property LoginID As String
        Get
            Return _loginID
        End Get
        Set(ByVal value As String)
            _loginID = value
            RaisePropertyChanged("LoginID")
        End Set
    End Property

    Public Property Password As String
        Get
            Return _password
        End Get
        Set(ByVal value As String)
            _password = value
            RaisePropertyChanged("Password")
        End Set
    End Property

    Private _Ldap As Boolean
    Public Property Ldap() As Boolean
        Get
            Return _Ldap
        End Get
        Set(ByVal value As Boolean)
            _Ldap = value
            RaisePropertyChanged("Ldap")
        End Set
    End Property


    Private _LoginTypeLocal As Boolean = True
    Public Property LoginTypeLocal() As Boolean
        Get
            Return _LoginTypeLocal
        End Get
        Set(ByVal value As Boolean)
            _LoginTypeLocal = value
            RaisePropertyChanged("LoginTypeLocal")
        End Set
    End Property

    Private _LoginTypeLdap As Boolean
    Public Property LoginTypeLdap() As Boolean
        Get
            Return _LoginTypeLdap
        End Get
        Set(ByVal value As Boolean)
            _LoginTypeLdap = value
            RaisePropertyChanged("LoginTypeLdap")
        End Set
    End Property



    Public Property LoginMessage As String
        Get
            Return _loginMessage
        End Get
        Set(ByVal value As String)
            _loginMessage = value
            RaisePropertyChanged("LoginMessage")
        End Set
    End Property


    Public Property LoginSuccess As Boolean
        Get
            Return _loginSuccess
        End Get
        Set(ByVal value As Boolean)
            _loginSuccess = value
        End Set
    End Property



    Public ReadOnly Property CurrentRoleManager As RoleManager
        Get
            Return Application.Current.FindResource("roleManager")
        End Get
    End Property

    'get { return (RoleManager)this.Resources["roleManager"]; }

    Public Property LoginCommand As ICommand



#End Region

#Region "  Constructors"

    Public Sub New()
        _regionmanager = CommonServiceLocator.ServiceLocator.Current.GetInstance(Of IRegionManager)
        'Me._regionmanager = RegionManager
        _ea = Events.EventInstance.EventAggregator
        _userAccess = ServiceLocator.Current.GetInstance(Of IUserDataService)()

        Ldap = False
        If GlobalSettingService.HoleEinstellung("client.ldap", True) = "true" Then
            Ldap = True
        End If



        Init()



        LoginCommand = New RelayCommand(AddressOf LoginExecute)
    End Sub

#End Region


    Private Sub Init()
        inihandler = New Inihandler(Inihandler.Inifiles.user)

        Dim loginldap = inihandler.GetValue("login", "ldap", "false")

        If loginldap.ToLower = "true" Then
            LoginTypeLocal = False
            _LoginTypeLdap = True
        End If

        Dim ua = inihandler.GetValue("login", "lastuser", "")
        If Not Trim(ua) = "" Then
            LoginID = ua
        End If
    End Sub





    Private Async Function GetLogin(ByVal loginid As String, ByVal password As String, ByVal logintype As Integer) As Tasks.Task(Of PrincipleBusinessObject)
        Return Await Task.Run(Function() _userAccess.Login(loginid, password, logintype))
    End Function

    Private Async Sub LoginActionAsync()
        LoginMessage = ""
        IsBusy = True
        Dim paramLoginID = LoginID
        Dim paramPassword = Password

        Dim logintype As Integer = 0
        If LoginTypeLdap = True Then
            logintype = 1
        End If
        Try


            Dim userPrinciple = Await GetLogin(paramLoginID, paramPassword, logintype)

            'Dim myIdentity As IIdentity = New IdentityBusinessObject(Nothing, Nothing, Nothing, Nothing, Nothing)
            'Dim myRoles As New List(Of String)

            If userPrinciple.Identity.IsAuthenticated Then

                Thread.CurrentPrincipal = userPrinciple
                MyGlobals.CurrentUser = userPrinciple.Identity
                MyGlobals.CurrentUsername = userPrinciple.Identity.Name
                MyGlobals.LoggedOn = True


                _ea.GetEvent(Of LoginEvent).Publish(Nothing)


                LoginSuccess = True
                IsBusy = False
                Dim uri As New Uri("DashboardView", UriKind.Relative)
                _regionmanager.RequestNavigate(RegionNames.MainRegion, uri)

                CurrentRoleManager.ApplyRules()

                SaveSettings()

                LoginID = ""
                Password = ""
            End If
            IsBusy = False
        Catch ex As Exception
            IsBusy = False
            LoginMessage = ex.Message
        End Try
    End Sub

    Private Sub LoginAction()
        LoginMessage = ""

        Dim paramLoginID = LoginID
        Dim paramPassword = Password

        Dim logintype As Integer = 0
        If LoginTypeLdap = True Then
            logintype = 1
        End If
        Try


            Dim userPrinciple = _userAccess.Login(paramLoginID, paramPassword, logintype)

            'Dim myIdentity As IIdentity = New IdentityBusinessObject(Nothing, Nothing, Nothing, Nothing, Nothing)
            'Dim myRoles As New List(Of String)

            If userPrinciple.Identity.IsAuthenticated Then

                Thread.CurrentPrincipal = userPrinciple
                MyGlobals.CurrentUser = userPrinciple.Identity
                MyGlobals.CurrentUsername = userPrinciple.Identity.Name
                MyGlobals.LoggedOn = True



                ' Test CustomPrincipal
                'Dim cp = ServiceLocator.Current.GetInstance(Of ICustomPrincipal)()
                'cp = userPrinciple



                _ea.GetEvent(Of LoginEvent).Publish(Nothing)


                LoginSuccess = True
                IsBusy = False
                Dim uri As New Uri("DashboardView", UriKind.Relative)
                _regionmanager.RequestNavigate(RegionNames.MainRegion, uri)

                CurrentRoleManager.ApplyRules()

                SaveSettings()

                LoginID = ""
                Password = ""
            End If
            IsBusy = False
        Catch ex As Exception
            IsBusy = False
            LoginMessage = ex.Message
        End Try

    End Sub

    Public Overrides Sub OnNavigatedTo(navigationContext As NavigationContext)
        Init()
        MyBase.OnNavigatedTo(navigationContext)
    End Sub

#Region "  Command Methods"

    Private Sub LoginExecute()


        LoginAction()
        'LoginActionAsync()
    End Sub

    Private Sub SaveSettings()
        Dim ldaplogin As String = IIf(LoginTypeLdap, "true", "false")
        inihandler.SetValue("login", "ldap", ldaplogin)
        inihandler.SetValue("login", "lastuser", LoginID)
        inihandler.Save()
    End Sub


    'Private Function CanLoginExecute(ByVal param As Object) As Boolean

    '    Return (Not String.IsNullOrEmpty(LoginID)) AndAlso (Not String.IsNullOrEmpty(Password))

    'End Function

    Private Sub CancelLogin()
        LoginSuccess = False
    End Sub

#End Region
End Class
