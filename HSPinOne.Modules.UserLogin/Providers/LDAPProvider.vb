﻿Imports System.Collections.Generic
Imports System.DirectoryServices.Protocols
Imports System.Net

Public Class LDAPProvider
    Implements IDisposable

    Private _connection As LdapConnection
    Private _url As String
    Private _port As Integer

    Private _Message As String
    Public ReadOnly Property Message() As String
        Get
            Return _Message
        End Get
    End Property

    Public Sub New(ByVal username As String, ByVal password As String, ByVal url As String, ByVal port As String)
        _url = url
        _port = Integer.Parse(port)

        Try


            Dim domain As String = ""
            Dim credentials = New NetworkCredential(username, password, domain)
            Dim serverId = New LdapDirectoryIdentifier(_url, _port)

            _connection = New LdapConnection(serverId, credentials)
            _connection.AuthType = AuthType.Basic
            _connection.SessionOptions.ProtocolVersion = 3
            _connection.Bind()

        Catch ex As Exception
            _Message = ex.Message.ToString
        End Try
    End Sub

    Public Function Search(ByVal basedn As String, ByVal ldapfilter As String) As List(Of Dictionary(Of String, String))
        Try
            Dim request = New SearchRequest(basedn, ldapfilter, SearchScope.Subtree, Nothing)
            Dim response As SearchResponse = _connection.SendRequest(request)

            Dim result = New List(Of Dictionary(Of String, String))

            For Each entry As SearchResultEntry In response.Entries
                Dim dic = New Dictionary(Of String, String)
                dic.Add("DN", entry.DistinguishedName)

                For Each attrName As String In entry.Attributes.AttributeNames
                    dic.Add(attrName, String.Join(",", entry.Attributes(attrName).GetValues(GetType(String))))

                Next
                result.Add(dic)

            Next
            Return result

        Catch ex As Exception
            Throw
        End Try
        Return Nothing
    End Function

    Public Function ValidateUserByBind(ByVal username As String, ByVal password As String) As Boolean
        Dim result As Boolean = False
        Dim credentials = New NetworkCredential(username, password)
        Dim serverId = New LdapDirectoryIdentifier(_url, _port)
        _connection = New LdapConnection(serverId, credentials)
        _connection.AuthType = AuthType.Basic
        _connection.SessionOptions.ProtocolVersion = 3

        Try
            _connection.Bind()
            result = True
        Catch ex As Exception
            _Message = ex.Message.ToString
        End Try
        _connection.Dispose()

        Return result


    End Function

#Region "IDisposable Support"
    Private disposedValue As Boolean ' Dient zur Erkennung redundanter Aufrufe.

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not disposedValue Then
            If disposing Then
                ' TODO: verwalteten Zustand (verwaltete Objekte) entsorgen.
            End If

            If Not IsNothing(_connection) Then _connection.Dispose()
            ' TODO: nicht verwaltete Ressourcen (nicht verwaltete Objekte) freigeben und Finalize() weiter unten überschreiben.
            ' TODO: große Felder auf Null setzen.
        End If
        disposedValue = True
    End Sub

    ' TODO: Finalize() nur überschreiben, wenn Dispose(disposing As Boolean) weiter oben Code zur Bereinigung nicht verwalteter Ressourcen enthält.
    'Protected Overrides Sub Finalize()
    '    ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' Dieser Code wird von Visual Basic hinzugefügt, um das Dispose-Muster richtig zu implementieren.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(disposing As Boolean) weiter oben ein.
        Dispose(True)
        ' TODO: Auskommentierung der folgenden Zeile aufheben, wenn Finalize() oben überschrieben wird.
        ' GC.SuppressFinalize(Me)
    End Sub
#End Region



End Class
