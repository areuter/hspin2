﻿Imports System.ComponentModel.Composition
Imports HSPinOne.Common
Imports HSPinOne.Infrastructure
Imports Microsoft.Practices.Prism.Regions
Imports Microsoft.Practices.ServiceLocation


<Export(GetType(PreferencesOutlookSectionViewModel)), PartCreationPolicy(CreationPolicy.Shared)>
Public Class PreferencesOutlookSectionViewModel
    Private _treeViewMenu As SettingsNavigation

    Public Property TreeViewMenu As SettingsNavigation
        Get
            Return _treeViewMenu
        End Get
        Set(ByVal value As SettingsNavigation)
            _treeViewMenu = value
        End Set
    End Property

    Private _regionmanager As IRegionManager
    Private _servicelocator As IServiceLocator


    Private _SelectedItem As Object

    Public Property SelectedItem As Object
        Get
            Return _SelectedItem
        End Get
        Set(value As Object)
            Try
                If value.GetType Is GetType(SettingsNavigation) Then
                    _SelectedItem = value
                    Navigate()
                End If
            Catch ex As Exception
                MsgBox("Error" & vbCrLf & ex.Message)
            End Try
        End Set
    End Property

    <ImportingConstructor()>
    Public Sub New(ByVal regionmanager As IRegionManager)

        Me._regionmanager = regionmanager
        Me.TreeViewMenu = New SettingsNavigation()


        Events.EVAG.GetEvent (Of OutlookSectionChangedEvent)().Subscribe(AddressOf DefaultNav)
    End Sub


    Private Sub Navigate()

        If Not IsNothing(SelectedItem) Then

            If Not IsNothing(SelectedItem.Tag1) Then
                ViewNavigation.NavigateViews(SelectedItem.Tag1, _regionmanager)
            End If

        End If
    End Sub


    Public Sub DefaultNav(ByVal param As Object)
        If param.Name.ToLower = "preferences" Then

            Navigate()

        End If
    End Sub
End Class
