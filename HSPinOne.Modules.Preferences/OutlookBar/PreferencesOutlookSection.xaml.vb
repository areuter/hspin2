﻿Imports System.ComponentModel.Composition
Imports HSPinOne.Infrastructure

<ViewExport(privateRegionName := RegionNames.TaskRegion), PartCreationPolicy(CreationPolicy.NonShared)>
Public Class PreferencesOutlookSection
    Public Sub New()

        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()

        ' Fügen Sie Initialisierungen nach dem InitializeComponent()-Aufruf hinzu.
    End Sub

    <Import()>
    Public WriteOnly Property ViewModel() As PreferencesOutlookSectionViewModel
        Set(ByVal value As PreferencesOutlookSectionViewModel)
            Me.DataContext = value
        End Set
    End Property
End Class
