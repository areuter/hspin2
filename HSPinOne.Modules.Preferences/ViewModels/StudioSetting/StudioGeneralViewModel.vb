﻿Imports System.ComponentModel.Composition
Imports HSPinOne.Infrastructure

<Export(GetType(StudioGeneralViewModel))> _
<PartCreationPolicy(CreationPolicy.[Shared])>
Public Class StudioGeneralViewModel
    Inherits ViewModelBase
End Class
