﻿Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports Microsoft.Practices.Prism.Regions
Imports HSPinOne.Modules.Preferences.ViewModel

Public Class PreferencesViewModel
    Inherits ViewModelBase
    Implements INavigationAware


    Private _navigationJournal As IRegionNavigationJournal


    ''' <summary>
    '''     Navigate back to last View
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub GoBack()
        If Me._navigationJournal IsNot Nothing Then
            Me._navigationJournal.GoBack()
        End If
    End Sub

    Public Function ShowKeyValueWindow(ByVal dv As KeyValue) As KeyValue

        Dim uc As New EditKeyValueView
        Dim vm As New EditKeyValueViewModel(dv)

        uc.DataContext = vm
        Dim rad As New WindowController

        AddHandler vm.RequestClose, AddressOf rad.CloseWindow
        AddHandler rad.CloseButtonClicked, AddressOf rad.CloseWindow
        rad.ShowWindow(uc, "Bearbeiten")
        Return vm.DialogResult

    End Function

    Public Sub OnNavigatedTo(ByVal navigationContext As NavigationContext) Implements INavigationAware.OnNavigatedTo
        Me._navigationJournal = navigationContext.NavigationService.Journal
    End Sub

    Public Function IsNavigationTarget(ByVal navigationContext As NavigationContext) As Boolean _
        Implements INavigationAware.IsNavigationTarget
        Return True
    End Function

    Public Sub OnNavigatedFrom(ByVal navigationContext As NavigationContext) Implements INavigationAware.OnNavigatedFrom
    End Sub
End Class
