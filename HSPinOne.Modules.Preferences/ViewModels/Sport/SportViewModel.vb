﻿Imports System.ComponentModel.Composition
Imports System.Data.Entity
Imports System.Windows
Imports Microsoft.Practices.Prism.Commands
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Input
Imports Microsoft.Practices.Prism.Regions
Imports Microsoft.Practices.ServiceLocation
Imports Microsoft.Practices.Prism

<Export(GetType(SportViewModel))> _
<PartCreationPolicy(CreationPolicy.[Shared])>
Public Class SportViewModel
    Inherits PreferencesViewModel

#Region "Public Properties"

    Private _filter As String
    Private _context As in1Entities
    Private ReadOnly regionManager As IRegionManager

    Private Const EditView As String = "EditSportView"
    Private Const IdKey As String = "SportId"


    Public ReadOnly Property FilteredSports() As IList(Of Sport)
        Get
            Return GetFilteredSports(_filter)
        End Get
    End Property

    Private _selectedSport As Sport

    Public Property SelectedSport() As Sport
        Get
            Return Me._selectedSport
        End Get
        Set(value As Sport)
            Me._selectedSport = value
            OnPropertyChanged("SelectedSport")
        End Set
    End Property

    Public Property Filter() As String
        Get
            Return Me._filter
        End Get
        Set(value As String)
            Me._filter = value
            OnPropertyChanged("FilteredSports")
        End Set
    End Property

    Public Property DeleteCommand As DelegateCommand(Of Sport)

    Public Property OpenMessageCommand As DelegateCommand(Of Sport)

    Public Property SaveCommand As DelegateCommand

    Public Property CancelCommand As DelegateCommand

    Public Property SportCategories As IList(Of SportCategory)


#End Region

    <ImportingConstructor()>
    Public Sub New(ByVal regionmanager As IRegionManager, ByVal servicelocator As IServiceLocator)
        Me.regionManager = regionmanager

        Me.OpenMessageCommand = New DelegateCommand(Of Sport)(AddressOf Me.OpenMessage)
        Me.SaveCommand = New DelegateCommand(AddressOf Me.Save)
        Me.CancelCommand = New DelegateCommand(AddressOf Me.Cancel)
        Me.DeleteCommand = New DelegateCommand(Of Sport)(AddressOf Me.Delete)

        If IsNothing(_context) Then
            _context = New in1Entities()
        End If

        SelectedSport = Nothing

        If IsNothing(SportCategories) Then
            _context.SportCategories.Load()
            SportCategories = _context.SportCategories.Local
        End If

        Me.Filter = ""
    End Sub

    Private Sub Delete(obj As Sport)
        If _context.Courses.Any(Function(o) o.SportID = obj.SportID) Then
            MessageBox.Show(
                String.Format("Es sind {0} Kurse mit der Sportart verknüpft. Sportart ist nicht löschbar?",
                              Me._context.Courses.Count(Function(o) o.SportID = obj.SportID)),
                "Confirmation", MessageBoxButton.OK)
            Return
        End If
        MessageBox.Show("Sportart wirklich löschen?", "Bestätigen", MessageBoxButton.YesNo, MessageBoxImage.Question)
    End Sub


    ''' <summary>
    ''' Filter Sport list based on Filter
    ''' If Filter is empty then full list
    ''' </summary>
    ''' <param name="filter"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetFilteredSports(ByVal filter As String)
        Return _
            Me._context.Sports.Where(Function(s) s.Sportname.Contains(filter)).OrderBy(Function(o) o.Sportname).ToList()
    End Function

    Private Sub OpenMessage(document As Sport)

        Dim parameters As New UriQuery()
        parameters.Add(IdKey, document.SportID.ToString("N"))

        Me.regionManager.RequestNavigate(RegionNames.MainRegion,
                                         New Uri(EditView + parameters.ToString(), UriKind.Relative))
    End Sub

    
    ''' <summary>
    '''     Save Context and go back to last view
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Save()
        If _context.HasChanges Then
            _context.SaveChanges()
        End If
        Me.GoBack()
    End Sub

    
    ''' <summary>
    '''     Cancelation of Edit Action
    '''     If No changes are made just back to last view
    '''     If changes are made
    '''     1. Dispose and create new context
    '''     2. reload Sportlist
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Cancel()
        If _context.HasChanges Then
            _context.Dispose()
            _context = New in1Entities()
            Filter = Filter
        End If
        Me.GoBack()
    End Sub

    

End Class