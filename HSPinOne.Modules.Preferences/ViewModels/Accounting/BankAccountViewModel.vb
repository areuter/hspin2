﻿Imports System.ComponentModel.Composition
Imports System.Data.Entity
Imports System.Windows
Imports Microsoft.Practices.Prism.Commands
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Input
Imports Microsoft.Practices.Prism.Regions
Imports Microsoft.Practices.ServiceLocation
Imports Microsoft.Practices.Prism

<Export(GetType(BankAccountViewModel))> _
<PartCreationPolicy(CreationPolicy.[Shared])>
Public Class BankAccountViewModel
    Inherits PreferencesViewModel

    Private _filter As String
    Private _context As in1Entities
    Private ReadOnly regionManager As IRegionManager

    Private Const EditView As String = "EditAccountView"
    Private Const IdKey As String = "AccountId"


    Private _accountList As List(Of Account)

    Public Property AccountList() As List(Of Account)
        Get
            Return _accountList
        End Get
        Set(value As List(Of Account))
            Me._accountList = value
            OnPropertyChanged("AccountList")
        End Set
    End Property

    
    Private _selectedAccount As Account

    Public Property SelectedAccount() As Account
        Get
            Return Me._selectedAccount
        End Get
        Set(value As Account)
            Me._selectedAccount = value
            OnPropertyChanged("SelectedAccount")
        End Set
    End Property

    Public Property Filter() As String
        Get
            Return Me._filter
        End Get
        Set(value As String)
            Me._filter = value

            GetFilteredAccounts(value)
            OnPropertyChanged("AccountList")
        End Set
    End Property

    Public ReadOnly Property DeleteCommand() As ICommand
        Get
            Return Me._deleteCommand
        End Get
    End Property
    Public ReadOnly Property OpenCommand() As ICommand
        Get
            Return Me._openCommand
        End Get
    End Property

    Public ReadOnly Property SaveCommand() As ICommand
        Get
            Return Me._saveCommand
        End Get
    End Property

    Public ReadOnly Property CancelCommand() As ICommand
        Get
            Return Me._cancelCommand
        End Get
    End Property
    Public ReadOnly Property NewCommand() As ICommand
        Get
            Return Me._newCommand
        End Get
    End Property

    Private ReadOnly _openCommand As DelegateCommand(Of Account)

    Private ReadOnly _newCommand As DelegateCommand
    Private ReadOnly _saveCommand As DelegateCommand
    Private ReadOnly _cancelCommand As DelegateCommand
    Private ReadOnly _deleteCommand As DelegateCommand(Of Account)



    <ImportingConstructor()>
    Public Sub New(ByVal regionmanager As IRegionManager, ByVal servicelocator As IServiceLocator)
        Me.regionManager = regionmanager

        Me._openCommand = New DelegateCommand(Of Account)(AddressOf Me.OpenMessage)
        Me._saveCommand = New DelegateCommand(AddressOf Me.Save)
        Me._cancelCommand = New DelegateCommand(AddressOf Me.Cancel)
        Me._newCommand = New DelegateCommand(AddressOf Me.NewAccount)
        Me._deleteCommand = New DelegateCommand(Of Account)(AddressOf Me.Delete)

        If IsNothing(_context) Then
            _context = New in1Entities()
        End If

        AccountList = New List(Of Account)()

        'SelectedAccount = Nothing
        
        Me.Filter = ""
        GetFilteredAccounts("")
    End Sub


    Private Sub NewAccount()
        Dim account = New Account
        account.AccountID = 1999
        account.AccountName = "Neuer Account"

        SelectedAccount = account
        _context.Accounts.Add(account)
        OpenMessage(account)

    End Sub

    Private Sub Delete(obj As Account)
        If _context.Billings.Any(Function(o) o.AccountID = obj.AccountID) Then
            MessageBox.Show(
                String.Format("Es sind Rechnungen mit diesem Account verknüpft. Account ist nicht löschbar?"),
                "Confirmation", MessageBoxButton.OK)
            Return
        Else
            If MessageBox.Show(String.Format("Account '{0}' wirklich löschen?", obj.AccountName), "Bestätigen", MessageBoxButton.YesNo, MessageBoxImage.Question) Then
                _context.Accounts.Remove(obj)
                _context.SaveChanges()
                GetFilteredAccounts("")
            Else
                Return
            End If
        End If
    End Sub


    ''' <summary>
    ''' Filter Location list based on Filter
    ''' If Filter is empty then full list
    ''' </summary>
    ''' <param name="filter"></param>
    ''' <remarks></remarks>
    Private Sub GetFilteredAccounts(ByVal filter As String)
        Me.AccountList = Me._context.Accounts.Where(Function(s) s.AccountName.Contains(filter)).OrderBy(Function(o) o.AccountName).ToList()
        '    Me._context.Accounts.Where(Function(s) s.AccountName.Contains(filter)).OrderBy(Function(o) o.AccountName).ToList()
    End Sub

    Private Sub OpenMessage(document As Account)

        'Dim parameters As New UriQuery()
        'parameters.Add(IdKey, document.AccountID.ToString("N"))

        Me.regionManager.RequestNavigate(RegionNames.MainRegion,
                                         New Uri(EditView, UriKind.Relative))
    End Sub


    ''' <summary>
    '''     Save Context and go back to last view
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Save()
        If _context.HasChanges Then
            _context.SaveChanges()
        End If
        Me.GoBack()
    End Sub


    ''' <summary>
    '''     Cancelation of Edit Action
    '''     If No changes are made just back to last view
    '''     If changes are made
    '''     1. Dispose and create new context
    '''     2. reload Locationlist
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Cancel()
        If IsNothing(SelectedAccount.AccountID) Or SelectedAccount.AccountID = 0 Then
            SelectedAccount = Nothing
        End If
        If _context.HasChanges Then
            _context.Dispose()
            _context = New in1Entities()
            Filter = Filter
        End If
        Me.GoBack()
    End Sub


End Class
