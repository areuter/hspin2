﻿Imports System.ComponentModel.Composition
Imports System.Windows
Imports Microsoft.Practices.Prism.Commands
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Input
Imports HSPinOne.Modules.Preferences.ViewModel

<Export(GetType(CostCenterViewModel))> _
<PartCreationPolicy(CreationPolicy.[Shared])>
Public Class CostCenterViewModel
    Inherits PreferencesViewModel

    Private _context As in1Entities

#Region "Public Properties"

    Private _CostcenterList As List(Of Costcenter)

    Public Property CostcenterList() As List(Of Costcenter)
        Get
            Return _CostcenterList
        End Get
        Set(value As List(Of Costcenter))
            Me._CostcenterList = value
            OnPropertyChanged("CostcenterList")
        End Set
    End Property

    Public ReadOnly Property EditCommand() As ICommand
        Get
            Return Me._editCommand
        End Get
    End Property

    Public Property DeleteCommand As ICommand
    Public Property NewCommand As ICommand


    Private ReadOnly _editCommand As DelegateCommand(Of Costcenter)


#End Region

    Public Sub New()
        _context = New in1Entities()
        CostcenterList = New List(Of Costcenter)()
        DeleteCommand = New RelayCommand(AddressOf DeleteExecute, AddressOf CanDeleteExecute)
        NewCommand = New RelayCommand(AddressOf NewExecute, AddressOf CanNewExecute)
        Me._editCommand = New DelegateCommand(Of Costcenter)(AddressOf Me.EditExecute,
                                                                        AddressOf CanEditExecute)
        Me.GetList()

    End Sub

    Private Sub GetList()
        Me.CostcenterList = Me._context.Costcenters.ToList()
    End Sub

    Private Function CanNewExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Function CanDeleteExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Function CanEditExecute(ByVal obj As Object) As Boolean
        Return True
    End Function


    Private Sub DeleteExecute(ByVal obj As Costcenter)
        If _context.Courses.Count(Function(c) c.CostcenterID = obj.CostcenterID) > 0 Then
            MessageBox.Show(String.Format("Die Kosteninformation '{0}' kann nicht gelöscht weren. Es sind Kurse mit der Kosteninformation verknüpft.", obj.Costcentername),
                                     "Löschen nicht möglich", MessageBoxButton.OK)
            Return
        Else
            If MessageBox.Show(String.Format("Eintrag '{0}' wirklich löschen?", obj.Costcentername), "Bestätigen", MessageBoxButton.YesNo, MessageBoxImage.Question) Then
                _context.Costcenters.Remove(obj)
                _context.SaveChanges()
                GetList()
            Else
                Return
            End If
        End If
    End Sub

    Private Sub EditExecute(ByVal ca As Costcenter)
        Dim kv = New KeyValue(ca.CostcenterID, ca.Costcentername)
        Dim result = ShowKeyValueWindow(kv)

        If Not IsNothing(result) Then
            Dim entity = _context.Costcenters.First(Function(a) a.CostcenterID = result.Key)
            If Not IsNothing(entity) Then
                entity.Costcentername = result.Value
            Else
                entity = New Costcenter()
                entity.Costcentername = result.Value
            End If
            _context.SaveChanges()
        End If
    End Sub

    Private Sub NewExecute()
        Dim kv = New KeyValue()
        Dim result = ShowKeyValueWindow(kv)

        If Not IsNothing(result) Then
            Dim entity = New Costcenter()
            entity.Costcentername = result.Value
            _context.Costcenters.Add(entity)
            _context.SaveChanges()
        End If
        GetList()
    End Sub

End Class
