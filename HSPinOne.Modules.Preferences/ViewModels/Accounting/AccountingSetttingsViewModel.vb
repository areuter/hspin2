﻿Imports System.ComponentModel.Composition
Imports HSPinOne.Infrastructure

<Export(GetType(AccountingSettingsViewModel))> _
<PartCreationPolicy(CreationPolicy.[Shared])>
Public Class AccountingSettingsViewModel
    Inherits ViewModelBase
End Class
