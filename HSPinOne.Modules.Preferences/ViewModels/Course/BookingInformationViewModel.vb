﻿
Imports System.ComponentModel.Composition
Imports System.Windows
Imports Microsoft.Practices.Prism.Commands
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Input
Imports Microsoft.Practices.Prism.Regions
Imports Microsoft.Practices.ServiceLocation
Imports Microsoft.Practices.Prism

<Export(GetType(BookingInformationViewModel))> _
<PartCreationPolicy(CreationPolicy.[Shared])>
Public Class BookingInformationViewModel
    Inherits PreferencesViewModel
    Private _context As in1Entities

    Private ReadOnly regionManager As IRegionManager
    Private Const EditView As String = "EditCourseBookingInformationView"
    Private Const IdKey As String = "CouresRegistrationId"

#Region "Public Properties"

    Private _courseRegistrationList As List(Of CourseRegistration)

    Public Property CourseRegistrationList() As List(Of CourseRegistration)
        Get
            Return _courseRegistrationList
        End Get
        Set(value As List(Of CourseRegistration))
            Me._courseRegistrationList = value
            OnPropertyChanged("CourseRegistrationList")
        End Set
    End Property

  

    Private _selectedCourseRegistration As CourseRegistration

    Public Property SelectedCourseRegistration() As CourseRegistration
        Get
            Return Me._selectedCourseRegistration
        End Get
        Set(value As CourseRegistration)
            Me._selectedCourseRegistration = value
            OnPropertyChanged("SelectedCourseRegistration")
        End Set
    End Property


    Public ReadOnly Property BookableStates() As Dictionary(Of Integer, String)
        Get
            Return New BookableStates().States
        End Get

    End Property

#End Region

    Public ReadOnly Property SaveCommand() As ICommand
        Get
            Return Me._saveCommand
        End Get
    End Property

    Public ReadOnly Property CancelCommand() As ICommand
        Get
            Return Me._cancelCommand
        End Get
    End Property
    Public ReadOnly Property EditCommand() As ICommand
        Get
            Return Me._openMessageCommand
        End Get
    End Property

    Private ReadOnly _openMessageCommand As DelegateCommand(Of CourseRegistration)
    Public Property DeleteCommand As ICommand
    Public Property NewCommand As ICommand

    Private ReadOnly _saveCommand As DelegateCommand
    Private ReadOnly _cancelCommand As DelegateCommand

    Private _regionmanager As IRegionManager


    <ImportingConstructor()>
    Public Sub New(ByVal regionmanager As IRegionManager, ByVal servicelocator As IServiceLocator)
        If IsNothing(_context) Then
            _context = New in1Entities()
        End If
        Me.regionManager = regionmanager
        Me._openMessageCommand = New DelegateCommand(Of CourseRegistration)(AddressOf Me.OpenEditView)
        'EditCommand = New RelayCommand(AddressOf EditExecute, AddressOf CanEditExecute)
        DeleteCommand = New RelayCommand(AddressOf DeleteExecute, AddressOf CanDeleteExecute)
        NewCommand = New RelayCommand(AddressOf NewExecute, AddressOf CanNewExecute)
        Me._saveCommand = New DelegateCommand(AddressOf Me.Save)
        Me._cancelCommand = New DelegateCommand(AddressOf Me.Cancel)

        GetCourseRegistrationList()
        SelectedCourseRegistration = Nothing
    End Sub


    Private Sub GetCourseRegistrationList()
        CourseRegistrationList = _context.CourseRegistrations.ToList()
    End Sub

    Private Function CanNewExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Sub NewExecute(ByVal obj As Object)
        MessageBox.Show("Neue Kosteninformation anlegen?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question)
    End Sub
    Private Function CanDeleteExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Sub DeleteExecute(ByVal obj As CourseRegistration)
        If _context.Courses.Any(Function(o) o.CourseRegistrationID = obj.CourseRegistrationID) Then
            MessageBox.Show(String.Format("{0} Kurse verknüpft nicht löschbar?", obj.Courses.Count),
                            "Confirmation", MessageBoxButton.OK)
            Return
        End If
        MessageBox.Show("Löschen?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question)
    End Sub

    Private Function CanEditExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Sub EditExecute(ByVal obj As Object)
        MessageBox.Show("Bearbeiten?", "Confirmation", MessageBoxButton.YesNo,
                                                         MessageBoxImage.Question)
    End Sub


    Private Sub OpenEditView(ByVal obj As CourseRegistration)
        SelectedCourseRegistration = obj
        Dim parameters As New UriQuery()
        parameters.Add(IdKey, obj.CourseRegistrationID.ToString("N"))

        Me.regionManager.RequestNavigate(RegionNames.MainRegion,
                                         New Uri(EditView + parameters.ToString(), UriKind.Relative))
    End Sub


    ''' <summary>
    '''     Save Context and go back to last view
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Save()
        If _context.HasChanges Then
            _context.SaveChanges()
            GetCourseRegistrationList()
        End If
        Me.GoBack()
    End Sub


    ''' <summary>
    '''     Cancelation of Edit Action
    '''     If No changes are made just back to last view
    '''     If changes are made
    '''     1. Dispose and create new context
    '''     2. reload Sportlist
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Cancel()
        If _context.HasChanges Then
            _context.Dispose()
            _context = New in1Entities()
            GetCourseRegistrationList()
        End If
        Me.GoBack()
    End Sub

End Class
