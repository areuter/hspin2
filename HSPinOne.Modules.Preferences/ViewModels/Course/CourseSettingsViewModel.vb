﻿Imports System.ComponentModel.Composition
Imports System.Windows
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Input
Imports Microsoft.Practices.Prism.Regions

<Export(GetType(CourseSettingsViewModel))> _
<PartCreationPolicy(CreationPolicy.[Shared])>
Public Class CourseSettingsViewModel
    Inherits ViewModelBase


    Private _context As in1Entities

#Region "Public Properties"

    Public ReadOnly Property CourseRegistrationList() As List(Of CourseRegistration)
        Get
            Return _
                Me._context.CourseRegistrations.ToList()
        End Get
    End Property

    Private _selectedCourseRegistration As CourseRegistration

    Public Property SelectedCourseRegistration() As CourseRegistration
        Get
            Return Me._selectedCourseRegistration
        End Get
        Set(value As CourseRegistration)
            Me._selectedCourseRegistration = value
            OnPropertyChanged("SelectedCourseRegistration")
        End Set
    End Property

#End Region
    Public Property EditCommand As ICommand
    Public Property DeleteCommand As ICommand
    Public Property NewCommand As ICommand

    Private _regionmanager As IRegionManager

    Public Sub New()
        EditCommand = New RelayCommand(AddressOf EditExecute, AddressOf CanEditExecute)
        DeleteCommand = New RelayCommand(AddressOf DeleteExecute, AddressOf CanDeleteExecute)
        NewCommand = New RelayCommand(AddressOf NewExecute, AddressOf CanNewExecute)

        _context = New in1Entities()
        SelectedCourseRegistration = Nothing
    End Sub



    Private Function CanNewExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Sub NewExecute(ByVal obj As Object)
        MessageBox.Show("Neue Kategorie anlegen?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question)
    End Sub
    Private Function CanDeleteExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Sub DeleteExecute(ByVal obj As CourseRegistration)
        If _context.Courses.Any(Function(o) o.CourseRegistrationID = obj.CourseRegistrationID) Then
            MessageBox.Show(String.Format("{0} Kurse verknüpft nicht löschbar?", obj.Courses.Count),
                            "Confirmation", MessageBoxButton.OK)
            Return
        End If
        MessageBox.Show("Löschen?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question)
    End Sub

    Private Function CanEditExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Sub EditExecute(ByVal obj As Object)
        MessageBox.Show("Bearbeiten?", "Confirmation", MessageBoxButton.YesNo,
                                                         MessageBoxImage.Question)
    End Sub

End Class
