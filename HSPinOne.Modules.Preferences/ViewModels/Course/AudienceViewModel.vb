﻿Imports System.ComponentModel.Composition
Imports System.Windows
Imports HSPinOne.Infrastructure
Imports Microsoft.Practices.Prism.Commands
Imports HSPinOne.DAL
Imports System.Windows.Input


Namespace ViewModel
    <Export(GetType(AudienceViewModel))> _
    <PartCreationPolicy(CreationPolicy.[Shared])>
    Public Class AudienceViewModel
        Inherits PreferencesViewModel

        Private _context As in1Entities

#Region "Public Properties"

        Private _audienceList As List(Of CourseAudience)

        Public Property AudienceList() As List(Of CourseAudience)
            Get
                Return _audienceList
            End Get
            Set(value As List(Of CourseAudience))
                Me._audienceList = value
                OnPropertyChanged("AudienceList")
            End Set
        End Property

        Public ReadOnly Property EditCommand() As ICommand
            Get
                Return Me._editCommand
            End Get
        End Property

        Public Property DeleteCommand As ICommand
        Public Property NewCommand As ICommand


        Private ReadOnly _editCommand As DelegateCommand(Of CourseAudience)


#End Region

        Public Sub New()
            _context = New in1Entities()
            AudienceList = New List(Of CourseAudience)()
            DeleteCommand = New RelayCommand(AddressOf DeleteExecute, AddressOf CanDeleteExecute)
            NewCommand = New RelayCommand(AddressOf NewExecute, AddressOf CanNewExecute)
            Me._editCommand = New DelegateCommand(Of CourseAudience)(AddressOf Me.EditExecute,
                                                                            AddressOf CanEditExecute)
            Me.GetList()

        End Sub

        Private Sub GetList()
            Me.AudienceList = Me._context.CourseAudiences.ToList()
        End Sub

        Private Function CanNewExecute(ByVal obj As Object) As Boolean
            Return True
        End Function
        
        Private Function CanDeleteExecute(ByVal obj As Object) As Boolean
            Return True
        End Function

        Private Function CanEditExecute(ByVal obj As Object) As Boolean
            Return True
        End Function


        Private Sub DeleteExecute(ByVal obj As CourseAudience)
            If _context.Courses.Any(Function(o) o.CourseAudienceID = obj.CourseAudienceID) Then
                MessageBox.Show(String.Format("Die Zielgruppe '{0}' kann nicht gelöscht weren. Es sind {1} Kurse mit der Zielgruppe verknüpft.", obj.CourseAudienceName, obj.Courses.Count),
                                         "Löschen nicht möglich", MessageBoxButton.OK)
                Return
            Else
                If MessageBox.Show(String.Format("Eintrag '{0}' wirklich löschen?", obj.CourseAudienceName), "Bestätigen", MessageBoxButton.YesNo, MessageBoxImage.Question) Then
                    _context.CourseAudiences.Remove(obj)
                    _context.SaveChanges()
                    GetList()
                Else
                    Return
                End If
            End If
        End Sub

        Private Sub EditExecute(ByVal ca As CourseAudience)
            Dim kv = New KeyValue(ca.CourseAudienceID, ca.CourseAudienceName)
            Dim result = ShowKeyValueWindow(kv)

            If Not IsNothing(result) Then
                Dim entity = _context.CourseAudiences.First(Function(a) a.CourseAudienceID = result.Key)
                If Not IsNothing(entity) Then
                    entity.CourseAudienceName = result.Value
                Else
                    entity = New CourseAudience()
                    entity.CourseAudienceName = result.Value
                End If
                _context.SaveChanges()
            End If
        End Sub

        Private Sub NewExecute()
            Dim kv = New KeyValue()
            Dim result = ShowKeyValueWindow(kv)

            If Not IsNothing(result) Then
                Dim entity = New CourseAudience()
                entity.CourseAudienceName = result.Value
                _context.CourseAudiences.Add(entity)
                _context.SaveChanges()
            End If
            GetList()
        End Sub
    End Class
End Namespace
