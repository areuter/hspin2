﻿Imports System.ComponentModel.Composition
Imports System.Windows
Imports HSPinOne.Infrastructure
Imports Microsoft.Practices.Prism.Commands
Imports HSPinOne.DAL
Imports System.Windows.Input
Imports HSPinOne.Modules.Preferences.ViewModel

<Export(GetType(GenderViewModel))> _
<PartCreationPolicy(CreationPolicy.[Shared])>
Public Class GenderViewModel
    Inherits PreferencesViewModel

    Private _context As in1Entities

#Region "Public Properties"

    Private _CourseGenderList As List(Of CourseGender)

    Public Property CourseGenderList() As List(Of CourseGender)
        Get
            Return _CourseGenderList
        End Get
        Set(value As List(Of CourseGender))
            Me._CourseGenderList = value
            OnPropertyChanged("CourseGenderList")
        End Set
    End Property

    Public ReadOnly Property EditCommand() As ICommand
        Get
            Return Me._editCommand
        End Get
    End Property

    Public Property DeleteCommand As ICommand
    Public Property NewCommand As ICommand


    Private ReadOnly _editCommand As DelegateCommand(Of CourseGender)


#End Region

    Public Sub New()
        _context = New in1Entities()
        CourseGenderList = New List(Of CourseGender)()
        DeleteCommand = New RelayCommand(AddressOf DeleteExecute, AddressOf CanDeleteExecute)
        NewCommand = New RelayCommand(AddressOf NewExecute, AddressOf CanNewExecute)
        Me._editCommand = New DelegateCommand(Of CourseGender)(AddressOf Me.EditExecute,
                                                               AddressOf CanEditExecute)
        Me.GetList()
    End Sub

    Private Sub GetList()
        Me.CourseGenderList = Me._context.CourseGenders.ToList()
    End Sub

    Private Function CanNewExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Function CanDeleteExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Function CanEditExecute(ByVal obj As Object) As Boolean
        Return True
    End Function


    Private Sub DeleteExecute(ByVal obj As CourseGender)
        If _context.Courses.Any(Function(o) o.CourseGenderID = obj.CourseGenderID) Then
            MessageBox.Show(
                String.Format(
                    "Die Genderinformation '{0}' kann nicht gelöscht weren. Es sind {1} Kurse mit der Genderinformation verknüpft.",
                    obj.CourseGenderName, obj.Courses.Count),
                "Löschen nicht möglich", MessageBoxButton.OK)
            Return
        Else
            If _
                MessageBox.Show(String.Format("Eintrag '{0}' wirklich löschen?", obj.CourseGenderName), "Bestätigen",
                                MessageBoxButton.YesNo, MessageBoxImage.Question) Then
                _context.CourseGenders.Remove(obj)
                _context.SaveChanges()
                GetList()
            Else
                Return
            End If
        End If
    End Sub

    Private Sub EditExecute(ByVal ca As CourseGender)
        Dim kv = New KeyValue(ca.CourseGenderID, ca.CourseGenderName)
        Dim result = ShowKeyValueWindow(kv)

        If Not IsNothing(result) Then
            Dim entity = _context.CourseGenders.First(Function(a) a.CourseGenderID = result.Key)
            If Not IsNothing(entity) Then
                entity.CourseGenderName = result.Value
            Else
                entity = New CourseGender()
                entity.CourseGenderName = result.Value
            End If
            _context.SaveChanges()
        End If
    End Sub

    Private Sub NewExecute()
        Dim kv = New KeyValue()
        Dim result = ShowKeyValueWindow(kv)

        If Not IsNothing(result) Then
            Dim entity = New CourseGender()
            entity.CourseGenderName = result.Value
            _context.CourseGenders.Add(entity)
            _context.SaveChanges()
        End If
        GetList()
    End Sub
End Class