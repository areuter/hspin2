﻿Imports System.ComponentModel.Composition
Imports System.Windows
Imports HSPinOne.Infrastructure
Imports Microsoft.Practices.Prism.Commands
Imports HSPinOne.DAL
Imports System.Windows.Media
Imports System.Windows.Input
Imports Microsoft.Practices.Prism.Regions
Imports Microsoft.Practices.ServiceLocation
Imports System.Reflection
Imports Microsoft.Practices.Prism

<Export(GetType(CourseCategoryViewModel))> _
<PartCreationPolicy(CreationPolicy.[Shared])>
Public Class CourseCategoryViewModel
    Inherits PreferencesViewModel

    Private _context As in1Entities
    Private ReadOnly regionManager As IRegionManager
    Private Const EditView As String = "EditCourseCategoryView"
    Private Const IdKey As String = "CategoryId"

#Region "Public Properties"

    Private _categoryList As List(Of Category)

    Public Property CategoryList() As List(Of Category)
        Get
            Return _categoryList
        End Get
        Set(value As List(Of Category))
            Me._categoryList = value
            OnPropertyChanged("CategoryList")
        End Set
    End Property


    Private _selectedCategory As Category

    Public Property SelectedCategory() As Category
        Get
            Return Me._selectedCategory
        End Get
        Set(value As Category)
            Me._selectedCategory = value
            OnPropertyChanged("SelectedCategory")
        End Set
    End Property

#End Region
    Public ReadOnly Property SaveCommand() As ICommand
        Get
            Return Me._saveCommand
        End Get
    End Property

    Public ReadOnly Property CancelCommand() As ICommand
        Get
            Return Me._cancelCommand
        End Get
    End Property
    Public ReadOnly Property EditCommand() As ICommand
        Get
            Return Me._openMessageCommand
        End Get
    End Property

    Private ReadOnly _openMessageCommand As DelegateCommand(Of Category)
    Public Property DeleteCommand As ICommand
    Public Property NewCommand As ICommand

    Private ReadOnly _saveCommand As DelegateCommand
    Private ReadOnly _cancelCommand As DelegateCommand

    Private _regionmanager As IRegionManager


    <ImportingConstructor()>
    Public Sub New(ByVal regionmanager As IRegionManager, ByVal servicelocator As IServiceLocator)
        If IsNothing(_context) Then
            _context = New in1Entities()
        End If
        Me.regionManager = regionmanager
        Me._openMessageCommand = New DelegateCommand(Of Category)(AddressOf Me.OpenEditView)
        'EditCommand = New RelayCommand(AddressOf EditExecute, AddressOf CanEditExecute)
        DeleteCommand = New RelayCommand(AddressOf DeleteExecute, AddressOf CanDeleteExecute)
        NewCommand = New RelayCommand(AddressOf NewExecute, AddressOf CanNewExecute)
        Me._saveCommand = New DelegateCommand(AddressOf Me.Save)
        Me._cancelCommand = New DelegateCommand(AddressOf Me.Cancel)
        GetCategoryList()
        SelectedCategory = Nothing
    End Sub

    Private Sub GetCategoryList()
        CategoryList = _context.Categories.ToList()
    End Sub
    Private Sub OpenEditView(ByVal obj As Category)
        SelectedCategory = obj
        Dim parameters As New UriQuery()
        parameters.Add(IdKey, obj.CategoryID.ToString("N"))

        Me.regionManager.RequestNavigate(RegionNames.MainRegion,
                                         New Uri(EditView + parameters.ToString(), UriKind.Relative))
    End Sub


    Private Function CanNewExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Sub NewExecute(ByVal obj As Object)
        MessageBox.Show("Neue Kategorie anlegen?", "Bestätigen", MessageBoxButton.YesNo, MessageBoxImage.Question)
    End Sub

    Private Function CanDeleteExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Sub DeleteExecute(ByVal obj As Category)
        If Me._context.Courses.Any(Function(o) o.CategoryID = obj.CategoryID) Then
            MessageBox.Show(
                String.Format("{0} Kurse verknüpft nicht löschbar?",
                              Me._context.Courses.Count(Function(o) o.CategoryID = obj.CategoryID)),
                "Bestätigen", MessageBoxButton.OK)
            Return
        End If
        MessageBox.Show("Kurskategorie wirklich löshen?", "Bestätigen", MessageBoxButton.YesNo, MessageBoxImage.Question)
    End Sub

    ''' <summary>
    '''     Save Context and go back to last view
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Save()
        If _context.HasChanges Then
            _context.SaveChanges()
        End If
        Me.GoBack()
    End Sub


    ''' <summary>
    '''     Cancelation of Edit Action
    '''     If No changes are made just back to last view
    '''     If changes are made
    '''     1. Dispose and create new context
    '''     2. reload Sportlist
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Cancel()
        If _context.HasChanges Then
            _context.Dispose()
            _context = New in1Entities()
            GetCategoryList()
        End If
        Me.GoBack()
    End Sub
End Class