﻿Imports System.ComponentModel.Composition
Imports System.Windows
Imports Microsoft.Practices.Prism.Commands
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Input
Imports Microsoft.Practices.Prism.Regions
Imports HSPinOne.Modules.Preferences.ViewModel

<Export(GetType(CostInformationViewModel))> _
<PartCreationPolicy(CreationPolicy.[Shared])>
Public Class CostInformationViewModel
    Inherits PreferencesViewModel

    Private _context As in1Entities

#Region "Public Properties"

    Private _courseCostList As List(Of CourseCost)

    Public Property CourseCostList() As List(Of CourseCost)
        Get
            Return _courseCostList
        End Get
        Set(value As List(Of CourseCost))
            Me._courseCostList = value
            OnPropertyChanged("CourseCostList")
        End Set
    End Property

    Public ReadOnly Property EditCommand() As ICommand
        Get
            Return Me._editCommand
        End Get
    End Property

    Public Property DeleteCommand As ICommand
    Public Property NewCommand As ICommand


    Private ReadOnly _editCommand As DelegateCommand(Of CourseCost)


#End Region

    Public Sub New()
        _context = New in1Entities()
        CourseCostList = New List(Of CourseCost)()
        DeleteCommand = New RelayCommand(AddressOf DeleteExecute, AddressOf CanDeleteExecute)
        NewCommand = New RelayCommand(AddressOf NewExecute, AddressOf CanNewExecute)
        Me._editCommand = New DelegateCommand(Of CourseCost)(AddressOf Me.EditExecute,
                                                                        AddressOf CanEditExecute)
        Me.GetList()

    End Sub

    Private Sub GetList()
        Me.CourseCostList = Me._context.CourseCosts.ToList()
    End Sub

    Private Function CanNewExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Function CanDeleteExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Function CanEditExecute(ByVal obj As Object) As Boolean
        Return True
    End Function


    Private Sub DeleteExecute(ByVal obj As CourseCost)
        If _context.Courses.Any(Function(o) o.CourseCostID = obj.CourseCostID) Then
            MessageBox.Show(String.Format("Die Kosteninformation '{0}' kann nicht gelöscht weren. Es sind {1} Kurse mit der Kosteninformation verknüpft.", obj.CourseCostName, obj.Courses.Count),
                                     "Löschen nicht möglich", MessageBoxButton.OK)
            Return
        Else
            If MessageBox.Show(String.Format("Eintrag '{0}' wirklich löschen?", obj.CourseCostName), "Bestätigen", MessageBoxButton.YesNo, MessageBoxImage.Question) Then
                _context.CourseCosts.Remove(obj)
                _context.SaveChanges()
                GetList()
            Else
                Return
            End If
        End If
    End Sub

    Private Sub EditExecute(ByVal ca As CourseCost)
        Dim kv = New KeyValue(ca.CourseCostID, ca.CourseCostName)
        Dim result = ShowKeyValueWindow(kv)

        If Not IsNothing(result) Then
            Dim entity = _context.CourseCosts.First(Function(a) a.CourseCostID = result.Key)
            If Not IsNothing(entity) Then
                entity.CourseCostName = result.Value
            Else
                entity = New CourseCost()
                entity.CourseCostName = result.Value
            End If
            _context.SaveChanges()
        End If
    End Sub

    Private Sub NewExecute()
        Dim kv = New KeyValue()
        Dim result = ShowKeyValueWindow(kv)

        If Not IsNothing(result) Then
            Dim entity = New CourseCost()
            entity.CourseCostName = result.Value
            _context.CourseCosts.Add(entity)
            _context.SaveChanges()
        End If
        GetList()
    End Sub

End Class
