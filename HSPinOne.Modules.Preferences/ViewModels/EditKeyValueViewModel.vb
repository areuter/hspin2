﻿Imports System.ComponentModel.Composition
Imports System.ComponentModel
Imports HSPinOne.Infrastructure
Imports System.Windows.Input

Namespace ViewModel
    <Export(GetType(EditKeyValueViewModel))> _
    <PartCreationPolicy(CreationPolicy.[Shared])>
    Public Class EditKeyValueViewModel
        Inherits ViewModelBase
        Property Content() As KeyValue

        Public Property SaveCommand As ICommand
        Public Property CancelCommand As ICommand

        Sub New(ca As KeyValue)
            ' TODO: Complete member initialization 
            Content = ca

            SaveCommand = New RelayCommand(AddressOf Save, AddressOf CanSave)
            CancelCommand = New RelayCommand(AddressOf Cancel)
        End Sub

        Private Sub Cancel(ByVal obj As Object)
            DialogResult = Nothing
            RaiseEvent RequestClose(Me, Nothing)
        End Sub

        Private Sub Save(ByVal obj As Object)
            DialogResult = Content
            RaiseEvent RequestClose(Me, Nothing)
        End Sub


        Private Function CanSave(ByVal obj As Object) As Boolean
            Return True
        End Function


        Public Event RequestClose As EventHandler

        Private _dialogresult As KeyValue

        Public Property DialogResult As KeyValue
            Get
                Return _dialogresult
            End Get
            Set(ByVal value As KeyValue)
                _dialogresult = value
            End Set
        End Property
    End Class

    Public Class KeyValue 
        Property Key() As Integer
        Property Value As String
        Property Color() As String
        Property [Date]() As DateTime
        Property Bool() As Boolean

        Public Sub New()
        End Sub

        Public Sub New(ByVal key As Integer, ByVal value As String
                       )
            Me.Key = key
            Me.Value = value
        End Sub
    End Class
End Namespace
