﻿Imports System.ComponentModel.Composition
Imports System.Data.Entity
Imports System.Windows
Imports Microsoft.Practices.Prism.Commands
Imports HSPinOne.DAL
Imports HSPinOne.Infrastructure
Imports System.Windows.Input
Imports Microsoft.Practices.Prism.Regions
Imports Microsoft.Practices.ServiceLocation
Imports Microsoft.Practices.Prism
Imports System.Collections.ObjectModel

<Export(GetType(LocationViewModel))> _
<PartCreationPolicy(CreationPolicy.[Shared])>
Public Class LocationViewModel
    Inherits PreferencesViewModel

#Region "Public Properties"

    Private _filter As String
    Private _context As in1Entities
    Private ReadOnly regionManager As IRegionManager

    Private Const EditView As String = "EditLocationView"
    Private Const IdKey As String = "LocationId"


    Public ReadOnly Property FilteredLocations() As IList(Of Location)
        Get
            Return GetFilteredLocations(_filter)
        End Get
    End Property
    'Public ReadOnly Property LocationGroups() As IList(Of LocationLocationgroup)
    '    Get
    '        If Not IsNothing(SelectedLocation) Then
    '            Return Me._context.LocationLocationgroups.Where(Function(s) s.LocationID = SelectedLocation.LocationID).OrderBy(Function(o) o.Locationgroup.Locationgroupname).ToList()
    '        End If
    '        Return New List(Of LocationLocationgroup)()
    '    End Get
    'End Property


    Private _selectedLocation As Location

    Public Property SelectedLocation() As Location
        Get
            Return Me._selectedLocation
        End Get
        Set(value As Location)
            Me._selectedLocation = value
            OnPropertyChanged("SelectedLocation")
            'OnPropertyChanged("LocationGroups")
        End Set
    End Property

    Public Property Filter() As String
        Get
            Return Me._filter
        End Get
        Set(value As String)
            Me._filter = value
            OnPropertyChanged("FilteredLocations")
        End Set
    End Property

    Public ReadOnly Property DeleteCommand() As ICommand
        Get
            Return Me._deleteCommand
        End Get
    End Property
    Public ReadOnly Property OpenCommand() As ICommand
        Get
            Return Me._openCommand
        End Get
    End Property

    Public ReadOnly Property SaveCommand() As ICommand
        Get
            Return Me._saveCommand
        End Get
    End Property

    Public ReadOnly Property CancelCommand() As ICommand
        Get
            Return Me._cancelCommand
        End Get
    End Property

    Public Property Campuses As IList(Of Campus)

    Private ReadOnly _openCommand As DelegateCommand(Of Location)

    Private ReadOnly _saveCommand As DelegateCommand
    Private ReadOnly _cancelCommand As DelegateCommand
    Private ReadOnly _deleteCommand As DelegateCommand(Of Location)


#End Region

    <ImportingConstructor()>
    Public Sub New(ByVal regionmanager As IRegionManager, ByVal servicelocator As IServiceLocator)
        Me.regionManager = regionmanager

        Me._openCommand = New DelegateCommand(Of Location)(AddressOf Me.OpenMessage)
        Me._saveCommand = New DelegateCommand(AddressOf Me.Save)
        Me._cancelCommand = New DelegateCommand(AddressOf Me.Cancel)
        Me._deleteCommand = New DelegateCommand(Of Location)(AddressOf Me.Delete)

        If IsNothing(_context) Then
            _context = New in1Entities()
        End If

        SelectedLocation = Nothing

        If IsNothing(Campuses) Then
            _context.Campuss.Load()
            Campuses = _context.Campuss.Local
        End If

        Me.Filter = ""
    End Sub

    Private Sub Delete(obj As Location)
        If _context.Appointments.Any(Function(o) o.LocationID = obj.LocationID) Then
            MessageBox.Show(
                String.Format("Es sind {0} Kurse mit der Locationart verknüpft. Locationart ist nicht löschbar?",
                              Me._context.Courses.Count(Function(o) o.LocationID = obj.LocationID)),
                "Confirmation", MessageBoxButton.OK)
            Return
        End If
        MessageBox.Show("Locationart wirklich löschen?", "Bestätigen", MessageBoxButton.YesNo, MessageBoxImage.Question)
    End Sub


    ''' <summary>
    ''' Filter Location list based on Filter
    ''' If Filter is empty then full list
    ''' </summary>
    ''' <param name="filter"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetFilteredLocations(ByVal filter As String)
        Return _
            Me._context.Locations.Where(Function(s) s.LocationName.Contains(filter)).OrderBy(Function(o) o.LocationName).ToList()
    End Function

    Private Sub OpenMessage(document As Location)

        Dim parameters As New UriQuery()
        parameters.Add(IdKey, document.LocationID.ToString("N"))

        Me.regionManager.RequestNavigate(RegionNames.MainRegion,
                                         New Uri(EditView + parameters.ToString(), UriKind.Relative))
    End Sub


    ''' <summary>
    '''     Save Context and go back to last view
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Save()
        If _context.HasChanges Then
            _context.SaveChanges()
        End If
        Me.GoBack()
    End Sub


    ''' <summary>
    '''     Cancelation of Edit Action
    '''     If No changes are made just back to last view
    '''     If changes are made
    '''     1. Dispose and create new context
    '''     2. reload Locationlist
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Cancel()
        If _context.HasChanges Then
            _context.Dispose()
            _context = New in1Entities()
            Filter = Filter
        End If
        Me.GoBack()
    End Sub



End Class