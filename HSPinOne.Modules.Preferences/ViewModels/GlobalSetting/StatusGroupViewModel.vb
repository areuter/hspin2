﻿Imports System.ComponentModel.Composition
Imports System.Windows
Imports HSPinOne.DAL
Imports System.Windows.Input
Imports HSPinOne.Infrastructure
Imports Microsoft.Practices.Prism.Regions

<Export(GetType(StatusGroupViewModel))> _
<PartCreationPolicy(CreationPolicy.[Shared])>
Public Class StatusGroupViewModel
    Inherits ViewModelBase

    Private _context As in1Entities

#Region "Public Properties"

    Public ReadOnly Property CustomerStateList() As List(Of CustomerState)
        Get
            Return _
                Me._context.CustomerStates.ToList()
        End Get
    End Property

    Private _selectedCustomerState As CustomerState

    Public Property SelectedCustomerState() As CustomerState
        Get
            Return Me._selectedCustomerState
        End Get
        Set(value As CustomerState)
            Me._selectedCustomerState = value
            OnPropertyChanged("SelectedCustomerState")
        End Set
    End Property

#End Region

    Public Property EditCommand As ICommand
    Public Property DeleteCommand As ICommand
    Public Property NewCommand As ICommand

    Private _regionmanager As IRegionManager

    Public Sub New()
        EditCommand = New RelayCommand(AddressOf EditExecute, AddressOf CanEditExecute)
        DeleteCommand = New RelayCommand(AddressOf DeleteExecute, AddressOf CanDeleteExecute)
        NewCommand = New RelayCommand(AddressOf NewExecute, AddressOf CanNewExecute)

        Me._regionmanager = New RegionManager()
        _context = New in1Entities()
        SelectedCustomerState = Nothing
    End Sub

    Private Function CanNewExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Sub NewExecute(ByVal obj As Object)
        MessageBox.Show("Neue Statusgruppe anlegen?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question)
    End Sub
    Private Function CanDeleteExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Sub DeleteExecute(ByVal obj As CustomerState)
        Dim result As MessageBoxResult
        If obj.Customers.Any() Then
            result = MessageBox.Show(String.Format("{0} Kunden verknüpft nicht löschbar?", obj.Customers.Count),
                                     "Confirmation", MessageBoxButton.OK)
            Return
        End If
        result = MessageBox.Show("Löschen?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question)
    End Sub

    Private Function CanEditExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Sub EditExecute(ByVal obj As Object)
        MessageBox.Show("Bearbeiten?", "Confirmation", MessageBoxButton.YesNo,
                                                         MessageBoxImage.Question)
    End Sub

End Class
