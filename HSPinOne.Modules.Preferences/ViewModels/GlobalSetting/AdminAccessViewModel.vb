﻿Imports System.ComponentModel.Composition
Imports System.Windows
Imports HSPinOne.DAL
Imports System.Windows.Input
Imports HSPinOne.Infrastructure
Imports Microsoft.Practices.Prism.Regions

<Export(GetType(AdminAccessViewModel))> _
<PartCreationPolicy(CreationPolicy.[Shared])>
Public Class AdminAccessViewModel
    Inherits ViewModelBase

    Private _context As in1Entities

#Region "Public Properties"

    Public ReadOnly Property AdminAccessList() As List(Of AdminAccess)
        Get
            Return _
                Me._context.AdminAccesss.OrderBy(Function(o) o.Customer.Suchname).ToList()
        End Get
    End Property

    Private _selectedAdminAccess As AdminAccess

    Public Property SelectedAdminAccess() As AdminAccess
        Get
            Return Me._selectedAdminAccess
        End Get
        Set(value As AdminAccess)
            Me._selectedAdminAccess = value
            OnPropertyChanged("SelectedAdminAccess")
        End Set
    End Property

#End Region

    Public Property EditCommand As ICommand
    Public Property DeleteCommand As ICommand
    Public Property NewCommand As ICommand

    Private _regionmanager As IRegionManager

    Public Sub New()
        EditCommand = New RelayCommand(AddressOf EditExecute, AddressOf CanEditExecute)
        DeleteCommand = New RelayCommand(AddressOf DeleteExecute, AddressOf CanDeleteExecute)
        NewCommand = New RelayCommand(AddressOf NewExecute, AddressOf CanNewExecute)

        Me._regionmanager = New RegionManager()
        _context = New in1Entities()
        SelectedAdminAccess = Nothing
    End Sub

    Private Function CanNewExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Sub NewExecute(ByVal obj As Object)
        MessageBox.Show("Neue Adminzutritt anlegen?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question)
    End Sub
    Private Function CanDeleteExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Sub DeleteExecute(ByVal obj As AdminAccess)
        Dim result As MessageBoxResult
        result = MessageBox.Show("Löschen?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question)
    End Sub

    Private Function CanEditExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Sub EditExecute(ByVal obj As Object)
        MessageBox.Show("Bearbeiten?", "Confirmation", MessageBoxButton.YesNo,
                                                         MessageBoxImage.Question)
    End Sub

End Class
