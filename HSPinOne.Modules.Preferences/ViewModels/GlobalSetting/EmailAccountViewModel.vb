﻿Imports System.ComponentModel.Composition
Imports System.Windows
Imports HSPinOne.DAL
Imports System.Windows.Input
Imports HSPinOne.Infrastructure
Imports Microsoft.Practices.Prism.Regions

<Export(GetType(EmailAccountViewModel))> _
<PartCreationPolicy(CreationPolicy.[Shared])>
Public Class EmailAccountViewModel
    Inherits PreferencesViewModel


    Private _filter As String
    Private _context As in1Entities
    Private ReadOnly regionManager As IRegionManager

#Region "Public Properties"

    Private Property StubAccounts() As List(Of MailAccount)

    Private _mailAccountList As List(Of MailAccount)

    Public Property MailAccountList() As List(Of MailAccount)
        Get
            Return _mailAccountList
        End Get
        Set(value As List(Of MailAccount))
            Me._mailAccountList = value
            OnPropertyChanged("MailAccountList")
        End Set
    End Property
    Private _selectedMailAccount As MailAccount

    Public Property SelectedMailAccount() As MailAccount
        Get
            Return Me._selectedMailAccount
        End Get
        Set(value As MailAccount)
            Me._selectedMailAccount = value
            OnPropertyChanged("SelectedMailAccount")
        End Set
    End Property

    Public Property Filter() As String
        Get
            Return Me._filter
        End Get
        Set(value As String)
            Me._filter = value

            GetFilteredMailAccounts(value)
            OnPropertyChanged("AccountList")
        End Set
    End Property
#End Region

    Public Property EditCommand As ICommand
    Public Property DeleteCommand As ICommand
    Public Property NewCommand As ICommand

    Private _regionmanager As IRegionManager

    Public Sub New()
        StubAccounts = New List(Of MailAccount)

        Dim account = New MailAccount()
        account.Name = "Test Account 1"
        account.Cc = "medien@adh.de"
        account.Bcc = "info@adh.de"
        account.ReplyTo = "beckmann@hochschulsportmarketing.de"
        account.From = "Dirk Beckmann"
        account.Username = "dbeckmann"
        account.Ssl = True
        account.Active = False
        StubAccounts.Add(account)

        Dim account2 = New MailAccount()
        account.Name = "Test Account 2"
        StubAccounts.Add(account2)



        EditCommand = New RelayCommand(AddressOf EditExecute, AddressOf CanEditExecute)
        DeleteCommand = New RelayCommand(AddressOf DeleteExecute, AddressOf CanDeleteExecute)
        NewCommand = New RelayCommand(AddressOf NewExecute, AddressOf CanNewExecute)

        Me._regionmanager = New RegionManager()
        _context = New in1Entities()

        Me.Filter = ""

        SelectedMailAccount = Nothing
    End Sub

    Private Function CanNewExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Sub NewExecute(ByVal obj As Object)
        MessageBox.Show("Neue Statusgruppe anlegen?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question)
    End Sub
    Private Function CanDeleteExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Sub DeleteExecute(ByVal obj As CustomerState)
        Dim result As MessageBoxResult
        If obj.Customers.Any() Then
            result = MessageBox.Show(String.Format("{0} Kunden verknüpft nicht löschbar?", obj.Customers.Count),
                                     "Confirmation", MessageBoxButton.OK)
            Return
        End If
        result = MessageBox.Show("Löschen?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question)
    End Sub

    Private Function CanEditExecute(ByVal obj As Object) As Boolean
        Return True
    End Function

    Private Sub EditExecute(ByVal obj As Object)
        MessageBox.Show("Bearbeiten?", "Confirmation", MessageBoxButton.YesNo,
                                                         MessageBoxImage.Question)
    End Sub

    Private Sub GetFilteredMailAccounts(ByVal filter As String)
        Me.MailAccountList = Me.StubAccounts.Where(Function(s) s.Name.Contains(filter)).OrderBy(Function(o) o.Name).ToList()
        '     Me.MailAccountList = Me._context.Accounts.Where(Function(s) s.AccountName.Contains(filter)).OrderBy(Function(o) o.AccountName).ToList()
    End Sub


End Class


