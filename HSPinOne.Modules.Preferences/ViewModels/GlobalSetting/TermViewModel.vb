﻿Imports System.ComponentModel.Composition

<Export(GetType(TermViewModel))> _
<PartCreationPolicy(CreationPolicy.[Shared])>
Public Class TermViewModel
    Inherits PreferencesViewModel

    Property TestDate() As DateTime

    Sub New ()
        TestDate = DateTime.Now
    End Sub
End Class
