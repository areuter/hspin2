﻿Imports System.ComponentModel.Composition

<Export(GetType(GlobalViewModel))> _
<PartCreationPolicy(CreationPolicy.[Shared])>
Public Class GlobalViewModel
    Inherits PreferencesViewModel

    Private _viewData As Object

    Property ViewData() As Object
        Get
            Return _viewData
        End Get
        Set(value As Object)
            _viewData = value
            OnPropertyChanged("ViewData")
        End Set
    End Property

    Sub New()
        Dim einrichtung =
                New _
                With {.Name = "Test Hochschulsport", .Telefon = "+49 6457 4154", .Email = "info@adh.de",
                .Strasse = "Max Planck Str. 2", .Ort = "Dieburg", .PLZ = "64289", .Bundesland = "Hessen",
                .Land = "Deutschland",
                .Homepage = "www.adh.de",
                      .Latitude = Nothing,
                      .Longitude = Nothing,
                .Facebook = "https://www.facebook.com/hochschulsportverband"}

        Dim externalLinks = New Dictionary(Of String, String)
        externalLinks.Add("Infosys", "http://www.sport.uni-goettingen.de/infosys")
        externalLinks.Add("Interner Kalender", "http://www.sport.uni-goettingen.de/infosys/scheduler")

        ViewData =
            New _
                With {.ExternalLinks = externalLinks, .NewsFeed = "http://www.sport.uni-goettingen.de/infosys/rss.php",
                    .Einrichtung = einrichtung, .TemporaryFolder = "c:\Temp"}
    End Sub
End Class
