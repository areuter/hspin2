﻿Imports System.ComponentModel.Composition
Imports HSPinOne.Infrastructure

<Export(GetType(TemplateWordViewModel))> _
<PartCreationPolicy(CreationPolicy.[Shared])>
Public Class TemplateWordViewModel
    Inherits ViewModelBase
End Class
