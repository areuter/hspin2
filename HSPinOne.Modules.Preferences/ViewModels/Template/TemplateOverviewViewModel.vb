﻿Imports System.ComponentModel.Composition
Imports HSPinOne.Infrastructure

<Export(GetType(TemplateOverviewViewModel))> _
<PartCreationPolicy(CreationPolicy.[Shared])>
Public Class TemplateOverviewViewModel
    Inherits ViewModelBase
End Class
