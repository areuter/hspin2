﻿Imports System.ComponentModel.Composition
Imports HSPinOne.Infrastructure

<Export(GetType(TemplatePdfViewModel))> _
<PartCreationPolicy(CreationPolicy.[Shared])>
Public Class TemplatePdfViewModel
    Inherits ViewModelBase
End Class
