﻿
Imports System.ComponentModel.Composition
Imports System.Windows
Imports System.Data.Entity
Imports HSPinOne.Infrastructure
Imports Microsoft.Practices.Prism.Commands
Imports HSPinOne.DAL
Imports System.Windows.Input
Imports Microsoft.Practices.Prism.Regions
Imports Microsoft.Practices.ServiceLocation

<Export(GetType(AdministrationUserViewModel))> _
<PartCreationPolicy(CreationPolicy.[Shared])>
Public Class AdministrationUserViewModel
    Inherits PreferencesViewModel

    Private _filter As String
    Private _context As in1Entities
    Private ReadOnly regionManager As IRegionManager

    Private Const EditView As String = "EditAdministrationUserView"
    Private Const IdKey As String = "UserId"


    Private _UserList As List(Of User)

    Public Property UserList() As List(Of User)
        Get
            Return _UserList
        End Get
        Set(value As List(Of User))
            Me._UserList = value
            OnPropertyChanged("UserList")
        End Set
    End Property


    Private _selectedUser As User

    Public Property SelectedUser() As User
        Get
            Return Me._selectedUser
        End Get
        Set(value As User)
            Me._selectedUser = value
            OnPropertyChanged("SelectedUser")
        End Set
    End Property

    Public Property Filter() As String
        Get
            Return Me._filter
        End Get
        Set(value As String)
            Me._filter = value

            GetFilteredUsers(value)
            OnPropertyChanged("UserList")
        End Set
    End Property

    Private _pass As String
    Public Property Pass As String
        Get
            Return _pass
        End Get
        Set(ByVal value As String)
            _pass = value
            OnPropertyChanged("Pass")
        End Set
    End Property

    Public ReadOnly Property DeleteCommand() As ICommand
        Get
            Return Me._deleteCommand
        End Get
    End Property

    Public ReadOnly Property OpenCommand() As ICommand
        Get
            Return Me._openCommand
        End Get
    End Property

    Public ReadOnly Property SaveCommand() As ICommand
        Get
            Return Me._saveCommand
        End Get
    End Property

    Public ReadOnly Property CancelCommand() As ICommand
        Get
            Return Me._cancelCommand
        End Get
    End Property

    Public ReadOnly Property NewCommand() As ICommand
        Get
            Return Me._newCommand
        End Get
    End Property

    Public Property Institutions As IList(Of Institution)

    Public Property Roles As IList(Of Role)

    Private ReadOnly _openCommand As DelegateCommand(Of User)

    Private ReadOnly _newCommand As DelegateCommand
    Private ReadOnly _saveCommand As DelegateCommand
    Private ReadOnly _cancelCommand As DelegateCommand
    Private ReadOnly _deleteCommand As DelegateCommand(Of User)


    <ImportingConstructor()>
    Public Sub New(ByVal regionmanager As IRegionManager, ByVal servicelocator As IServiceLocator)
        Me.regionManager = regionmanager

        Me._openCommand = New DelegateCommand(Of User)(AddressOf Me.OpenMessage)
        Me._saveCommand = New DelegateCommand(AddressOf Me.Save)
        Me._cancelCommand = New DelegateCommand(AddressOf Me.Cancel)
        Me._newCommand = New DelegateCommand(AddressOf Me.NewUser)
        Me._deleteCommand = New DelegateCommand(Of User)(AddressOf Me.Delete)

        If IsNothing(_context) Then
            _context = New in1Entities()
        End If

        UserList = New List(Of User)()

        If IsNothing(Institutions) Then
            _context.Institutions.Load()
            Institutions = _context.Institutions.Local
        End If
        If IsNothing(Roles) Then
            _context.Roles.Load()
            Roles = _context.Roles.Local
        End If

        SelectedUser = Nothing

        Me.Filter = ""
        GetFilteredUsers("")
    End Sub

    Private Sub NewUser()
        Dim User = New User
        User.UserName = "Neuer User"

        SelectedUser = User
        _context.Users.Add(User)
        OpenMessage(User)
    End Sub


    Private Sub Delete(obj As User)
        If _context.Users.Any(Function(o) o.UserID = obj.UserID) Then
            MessageBox.Show(
                String.Format("Es sind Rechnungen mit diesem User verknüpft. User ist nicht löschbar?"),
                "Confirmation", MessageBoxButton.OK)
            Return
        Else
            If _
                MessageBox.Show(String.Format("User '{0}' wirklich löschen?", obj.UserName), "Bestätigen",
                                MessageBoxButton.YesNo, MessageBoxImage.Question) Then
                _context.Users.Remove(obj)
                _context.SaveChanges()
                GetFilteredUsers("")
            Else
                Return
            End If
        End If
    End Sub

    
    ''' <summary>
    '''     Filter Location list based on Filter
    '''     If Filter is empty then full list
    ''' </summary>
    ''' <param name="filter"></param>
    ''' <remarks></remarks>
    Private Sub GetFilteredUsers(ByVal filter As String)
        Me.UserList =
            Me._context.Users.Where(Function(s) s.UserName.Contains(filter)).OrderBy(Function(o) o.UserName).ToList()
        '    Me._context.Users.Where(Function(s) s.UserName.Contains(filter)).OrderBy(Function(o) o.UserName).ToList()
    End Sub

    Private Sub OpenMessage(document As User)

        'Dim parameters As New UriQuery()
        'parameters.Add(IdKey, document.UserID.ToString("N"))

        Me.regionManager.RequestNavigate(RegionNames.MainRegion,
                                         New Uri(EditView, UriKind.Relative))
    End Sub

    
    ''' <summary>
    '''     Save Context and go back to last view
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Save()
        If Not IsNothing(Pass) Then

            Dim salt As Byte()
            salt = GenSalt()
            SelectedUser.SaltValue = Convert.ToBase64String(salt)
            SelectedUser.Password = GenHash(Pass, salt)
       
        End If

        If _context.HasChanges Then
            _context.SaveChanges()
        End If
        Me.GoBack()
    End Sub

    
    ''' <summary>
    '''     Cancelation of Edit Action
    '''     If No changes are made just back to last view
    '''     If changes are made
    '''     1. Dispose and create new context
    '''     2. reload Locationlist
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Cancel()
        If IsNothing(SelectedUser.UserID) Or SelectedUser.UserID = 0 Then
            SelectedUser = Nothing
        End If
        If _context.HasChanges Then
            _context.Dispose()
            _context = New in1Entities()
            Filter = Filter
        End If
        Me.GoBack()
    End Sub

    Public Function GenHash(ByVal LoginPassword As String, ByVal salt As Byte())
        Try

            Return Crypto.ComputeHash(LoginPassword, "SHA1", salt)

        Catch ex As Exception
            Debug.Print("Exception")
            Throw New Exception(ex.Message, ex.InnerException)
        End Try
    End Function

    Public Function GenSalt()
        Return Crypto.generateRadomSaltvalue()
    End Function

End Class
