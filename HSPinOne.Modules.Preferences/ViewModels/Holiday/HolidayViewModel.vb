﻿Imports System.ComponentModel.Composition
Imports HSPinOne.Infrastructure

<Export(GetType(HolidayViewModel))> _
<PartCreationPolicy(CreationPolicy.[Shared])>
Public Class HolidayViewModel
    Inherits ViewModelBase
End Class
