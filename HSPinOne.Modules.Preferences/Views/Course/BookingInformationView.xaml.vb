﻿Imports System.ComponentModel.Composition
Imports System.Diagnostics.CodeAnalysis
Imports HSPinOne.Modules.Preferences.ViewModel
Imports Microsoft.Practices.Prism.Regions

<Export("BookingInformationView"), PartCreationPolicy(CreationPolicy.Shared)>
Public Class BookingInformationView

#Region "Constructors and Destructors"

    Public Sub New()
        Me.InitializeComponent()
    End Sub

#End Region

#Region "Properties"

    <Import> _
    <
        SuppressMessage _
            ("Microsoft.Design", "CA1044:PropertiesShouldNotBeWriteOnly",
             Justification:="Needs to be a property to be composed by MEF")>
    Private WriteOnly Property ViewModel() As BookingInformationViewModel
        Set(value As BookingInformationViewModel)
            Me.DataContext = value
        End Set
    End Property

#End Region


End Class
