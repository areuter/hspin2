﻿Imports System.ComponentModel.Composition
Imports System.Diagnostics.CodeAnalysis
Imports HSPinOne.Modules.Preferences.ViewModel
Imports Microsoft.Practices.Prism.Regions

<Export("StudioGeneralView"), PartCreationPolicy(CreationPolicy.Shared)>
Public Class StudioGeneralView

#Region "Constructors and Destructors"

    Public Sub New()
        Me.InitializeComponent()
    End Sub

#End Region

#Region "Properties"

    <Import> _
    <
        SuppressMessage _
            ("Microsoft.Design", "CA1044:PropertiesShouldNotBeWriteOnly",
             Justification:="Needs to be a property to be composed by MEF")>
    Private WriteOnly Property ViewModel() As StudioGeneralViewModel
        Set(value As StudioGeneralViewModel)
            Me.DataContext = value
        End Set
    End Property

#End Region

#Region "Public Methods and Operators"

    Public Function IsNavigationTarget(navigationContext As NavigationContext) As Boolean
        Return True
    End Function

    Public Sub OnNavigatedFrom(navigationContext As NavigationContext)
    End Sub

    Public Sub OnNavigatedTo(navigationContext As NavigationContext)
    End Sub

#End Region
End Class