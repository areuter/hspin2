﻿
Imports System.ComponentModel.Composition
Imports System.Diagnostics.CodeAnalysis

<Export("TermView"), PartCreationPolicy(CreationPolicy.Shared)>
Public Class TermView

#Region "Constructors and Destructors"

    Public Sub New()
        Me.InitializeComponent()
    End Sub

#End Region

#Region "Properties"

    <Import> _
    <
        SuppressMessage _
            ("Microsoft.Design", "CA1044:PropertiesShouldNotBeWriteOnly",
             Justification := "Needs to be a property to be composed by MEF")>
    Private WriteOnly Property ViewModel() As TermViewModel
        Set(value As TermViewModel)
            Me.DataContext = value
        End Set
    End Property

#End Region
End Class