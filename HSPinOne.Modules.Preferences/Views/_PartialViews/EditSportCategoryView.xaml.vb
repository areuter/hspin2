﻿
Imports System.ComponentModel.Composition
Imports System.Diagnostics.CodeAnalysis

<Export("EditSportCategoryView"), PartCreationPolicy(CreationPolicy.NonShared)>
Public Class EditSportCategoryView
#Region "Constructors and Destructors"

    Public Sub New()
        Me.InitializeComponent()
    End Sub

#End Region

#Region "Properties"

    <Import> _
    <
        SuppressMessage _
            ("Microsoft.Design", "CA1044:PropertiesShouldNotBeWriteOnly",
             Justification:="Needs to be a property to be composed by MEF")>
    Private WriteOnly Property ViewModel() As SportCategoryViewModel
        Set(value As SportCategoryViewModel)
            Me.DataContext = value
        End Set
    End Property

#End Region

#Region "Public Methods and Operators"


#End Region
End Class
